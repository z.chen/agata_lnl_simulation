#ifndef __EmitterData__
#define __EmitterData__
#include <iostream>
#include <TObject.h>
#include <TVector3.h>
#include <TClass.h>
#include <vector>

class EmitterData : public TObject
{
 public:
  EmitterData(){;}
  ~EmitterData(){;}

 public:
  //Emitter
  inline void SetBeta (float b) {fBeta =b;};
  inline void SetPosition (float x, float y, float z) {fPosition.SetXYZ(x,y,z);};
  inline void SetVelocity (float x, float y, float z) {fVelocity.SetXYZ(x,y,z);};

  inline float    GetBeta ()      {return fBeta;};
  inline TVector3 GetPosition(){return fPosition;};
  inline TVector3 GetVelocity(){return fVelocity;};

    
 private:
  float    fBeta;
  TVector3 fPosition;
  TVector3 fVelocity;


  ClassDef(EmitterData,1);

};


#endif
