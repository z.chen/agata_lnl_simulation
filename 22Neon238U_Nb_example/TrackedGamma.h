#ifndef __TrackedGamma__
#define __TrackedGamma__
#include <iostream>
#include <TObject.h>
#include <TVector3.h>
#include <TClass.h>
#include <vector>

class TrackedGamma  : public TObject
{
 public:
  TrackedGamma(){;}
  ~TrackedGamma(){;}

 public:
  //Gamma
  inline void SetEnergy (float e) {Energy =e;};
  inline void SetEnergyDC (float e) {EnergyDC =e;};
  inline void SetPosition(float x, float y, float z){fPos.SetXYZ(x,y,z);}

  inline float GetEnergy () {return Energy;};
  inline float GetEnergyDC () {return EnergyDC;};
  inline TVector3 GetPosition(){return fPos;};
    
 private:

  float Energy;
  float EnergyDC;
  TVector3 fPos;

  ClassDef(TrackedGamma,3);

};
#pragma link C++ class vector<TrackedGamma>+;

#endif
