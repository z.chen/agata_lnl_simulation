//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Mar  7 08:54:39 2019 by ROOT version 6.14/04
// from TTree SimulatedAgata/SimulatedAgata
// found on file: tree.root
//////////////////////////////////////////////////////////

#ifndef SimuAnalysis_h
#define SimuAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TF1.h>
#include <TF2.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TCutG.h>
#include <TCanvas.h>



// Headers needed by this particular selector
#include "TrackedGamma.h"

#include <vector>

#include "CoreEnergy.h"
#include "EmitterData.h"

Double_t fun2stopped(Double_t *x, Double_t *par){
	return (par[2]*TMath::Exp(-TMath::Power((x[1]-par[0])/(par[3]),2)))*(par[0+7]+par[1+7]*TMath::Power(x[0],1)+par[2+7]*TMath::Power(x[0],2)+par[3+7]*TMath::Power(x[0],3)+par[4+7]*TMath::Power(x[0],1)+par[5+7]*TMath::Power(x[0],5)+par[6+7]*TMath::Power(x[0],6)+par[7+7]*TMath::Power(x[0],7)+par[8+7]*TMath::Power(x[0],8));
};

Double_t fun2(Double_t *x, Double_t *par){
	return (par[2]*TMath::Exp(-TMath::Power((x[1]-par[0])/(par[3]),2))+par[4]*TMath::Exp(-TMath::Power((x[1]-par[0]*(1+par[1]*TMath::Cos(x[0])))/(par[5]),2))+par[6])*(par[0+7]+par[1+7]*TMath::Power(x[0],1)+par[2+7]*TMath::Power(x[0],2)+par[3+7]*TMath::Power(x[0],3)+par[4+7]*TMath::Power(x[0],1)+par[5+7]*TMath::Power(x[0],5)+par[6+7]*TMath::Power(x[0],6)+par[7+7]*TMath::Power(x[0],7)+par[8+7]*TMath::Power(x[0],8));
};

Double_t fun1(Double_t *x, Double_t *par){
	return par[0]+par[1]*TMath::Power(x[0],1)+par[2]*TMath::Power(x[0],2)+par[3]*TMath::Power(x[0],3)+par[4]*TMath::Power(x[0],1)+par[5]*TMath::Power(x[0],5)+par[6]*TMath::Power(x[0],6)+par[7]*TMath::Power(x[0],7)+par[8]*TMath::Power(x[0],8);
};

class SimuAnalysis : public TSelector {
	public :
		TTreeReader     fReader;  //!the tree reader
		TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

		// Readers to access the data (delete the ones you do not need).
		TTreeReaderArray<TrackedGamma> tracked = {fReader, "tracked"};
		TTreeReaderArray<CoreEnergy>   cores   = {fReader, "cores"};
		TTreeReaderValue<EmitterData>  emitter = {fReader, "emitter"};


		SimuAnalysis(TTree * /*tree*/ =0) { }
		virtual ~SimuAnalysis() { }
		virtual Int_t   Version() const { return 2; }
		virtual void    Begin(TTree *tree);
		virtual void    SlaveBegin(TTree *tree);
		virtual void    Init(TTree *tree);
		virtual Bool_t  Notify();
		virtual Bool_t  Process(Long64_t entry);
		virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
		virtual void    SetOption(const char *option) { fOption = option; }
		virtual void    SetObject(TObject *obj) { fObject = obj; }
		virtual void    SetInputList(TList *input) { fInput = input; }
		virtual TList  *GetOutputList() const { return fOutput; }
		virtual void    SlaveTerminate();
		virtual void    Terminate();
		
		//Kinematics
		Double_t Gamma(Double_t beta){return 1./TMath::Sqrt(1-beta*beta);};
		const TVector3 RotateBeam(TVector3 pp1){ pp1.RotateY(-40.*TMath::DegToRad()); return pp1;};

		Double_t amuTOkev = 931493.6148385;
		Double_t M1 = 136*amuTOkev;
		Double_t M2 = 198*amuTOkev;
		Double_t M3 = 138*amuTOkev;
		Double_t M4 = 196*amuTOkev;
		
		Double_t Ek = 850000.;
		Double_t E1 = M1+Ek;

		TVector3 pp1lab = {0,0,TMath::Sqrt(E1*E1-M1*M1)};
		TLorentzVector p1 = {RotateBeam(pp1lab), E1};
		TLorentzVector p2 = {0,0,0, M2};

		struct BMean {
			Double_t mean;
			Int_t samples;
			BMean() : mean(0.), samples(0){};
		};
		BMean betaBP;
		BMean beta;

		std::string GetDopplerFunction(Double_t en,Double_t beta){
			return Form("%f*(1.+%f*TMath::Cos(x))",en,beta);
		};

		//std::vector<Double_t> energies={324.4, 467.0,639.2};
		std::vector<Double_t> energies={};

		//Fit
		Int_t rebin = 4;
		TF2 * GetFit(const TH2D * histo_in, Double_t energy, Double_t beta);

		//Histograms
		TH1D* hThetaEmitter3;
		TH1D* hThetaEmitter4;
		TH2D* mThetaBetaEmitter;
		TH1D * hTracked_DC;
		TH1D * hTracked;
		TH2D * mAngleE4;
		TH2D * mAngleE3;
		TH2D * mAngleEDC4;
		TH2D * mAngleEDC3;
		TH2D * mA3A4;

		
		//Other
		std::string out_file;

		ClassDef(SimuAnalysis,0);

};

#endif

#ifdef SimuAnalysis_cxx
void SimuAnalysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t SimuAnalysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef SimuAnalysis_cxx
