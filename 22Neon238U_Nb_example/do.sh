#!/bin/bash
#source /opt/agataSim.env

simufolder=`pwd`

## to create .so files ##
root -l -b << end
.L TrackedGamma.h+
.L CoreEnergy.h+
.L EmitterData.h+
.L readsimu.C+
end


Abeam=22
Zbeam=10
Atarget=238
Ztarget=92

Abeamlike=22
Zbeamlike=10
Atargetlike=238
Ztargetlike=92

DAbeam=$(( Abeamlike - Abeam ))
DZbeam=$(( Zbeamlike - Zbeam ))

runnumber='2210'


export G4AGATAVACUUMINWORLD=yes 
export G4VRMLFILE_MAX_FILE_NUM=1000 
Agata -seed -Gen -SN -n -b ExperimentSetup.mac | tee Agata.log 
tracking_simulated "GammaEvents.${runnumber}" 1 30000000 1 1 0 1 1 -8
mv tracked_energies GammaEvents.${runnumber}-tracked

root -l -b << end
.L TrackedGamma.h+
.L CoreEnergy.h+
.L EmitterData.h+
.L readsimu.C+
.! echo  'running readsimu("GammaEvents.${runnumber}-tracked","GammaEvents.${runnumber}-tracked.root")'
readsimu("GammaEvents.${runnumber}-tracked","GammaEvents.${runnumber}-tracked.root")
.! echo " readsimu done "
end


root -l -b << endselector
.L TrackedGamma.h+
.L CoreEnergy.h+
.L EmitterData.h+
.L RunSelector.C
.! echo  'running RunSelector("GammaEvents.${runnumber}-tracked.root","GammaEvents.${runnumber}-tracked-spec.root ${Abeam} ${Atarget} ${Abeamlike} ${Atargetlike} ${Energy} 1577 2296")'
RunSelector("GammaEvents.${runnumber}-tracked.root","GammaEvents.${runnumber}-tracked-spec.root ${Abeam} ${Atarget} ${Abeamlike} ${Atargetlike} ${Energy} 1577 2296")
.! echo " RunSelector done"
endselector



cd $simufolder
