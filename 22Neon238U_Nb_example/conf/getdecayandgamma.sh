#!/bin/bash


cp /opt/geant/geant4.9/install/share/Geant4-9.6.4/data/PhotonEvaporation2.3/z10.a26 gamma26Ne_original


cp /opt/geant/geant4.9/build/data/RadioactiveDecay3.6/z10.a26 decay26Ne_original




#
#The photo-evaporation database contains nuclear deexcitation data starting 
#from a given nuclear level. Each file contains data for a given isotope, 
#identified by Z and A.
#
#The database must first be downloaded from
#
#http://geant4.web.cern.ch/geant4/support/download.shtml
#
#and stored in a local directory.  The environment variable 
#G4LEVELGAMMADATA must then be set to point to this directory.
# 
#**************************************************
#
#Each line contains a de-excitation *step* from a given energy level to a lower 
#one (which might be the ground state). Notice that if multiple de-excitation 
#channels are allowed for the starting energy level, these channels will be 
#described in more lines (all having the same starting level).
#
#
#Each line contains 17 columns:
#
#1) Energy of the starting nuclear level (keV)
#   As mentioned before, it is possible to have more lines describing the same 
#   starting level, in the case where multiple de-excitation schemes are
#   allowed.
#	
#2) Energy of the transition (keV)
#   This is the energy difference between the initial and the final level.
#
#3) Transition probability (%)
#   Note1: if the probability is less than  minProbability = 1e-8%, it is forced
#          to be 1e-8%.
#   Note2: transition probabilities from a given level do not always sum up to 
#	  100% exactly. They are re-normalized internally.
#
#4) Polarity 
#   Spin-parity variation in the transition
#   [never used in real simulation]
#
#5) Level half-life (s)
#
#6) Angular Momentum
#   Spin of the initial level
#   [never used in real simulation]
#
#7) Total internal conversion probability 
#   Note: the value in item (3) is the *transition* probability, so the 
#         actual *gamma* probability is calculated taking into account the 
#         probability of internal conversion.
#
#8-17) Partial conversion probabilities for
#	K-shell
#	L1-3 shells
#	M1-5 shells
#	Outer shells (shellID = 9 is used, when applicable)
#
#Note: if the nuclear excitation energy does not match any of the known levels,
#      the *nearest* level is always considered.  In G4RadioactiveDecay, 
#      metastable states are treated correctly if the excitation energy is 
#      within 2.0 keV of the values in $G4RADIOACTIVEDATA.
#
#      For instance: take file $G4LEVELGAMMADATA/z28.a60 (Ni-60)
#      Co-60 radioactive decay populates the 1332.5080-keV level of 
#      Ni-60 (0.12%) or the 2505.7480-keV level of Ni-60 (99.88%).
#
#      Deexcitation from the 2505.7480-keV level is described in lines 
#      6-8 of $G4LEVELGAMMADATA/z28.a60 (Ni-60)
#      The nucleus will release
#      1) 347 keV with 7.6e-3% probability, ending up in the 2158-keV level 
#      (following de-excitation hence takes place, lines 2-4 of the file)
#      2) 1173 keV with 100% probability, ending up in the 1332-keV 
#      excited state (following de-excitation hence takes place, line 1)
#      2) 2505 keV with 2e-6% probability ending up in the ground state.
#
#
#
############################################################################
############################################################################
############################################################################
#This README file provides instructions for reading the radioactive decay 
#schemes in the files zXX.aYYY found in the directory RadioactiveDecay5.0
#(most recent version).  This directory, as well as previous versions, is
#available from the Geant4 download page 
#
#http://cern.ch/geant4/support/download.shtml
#
#under the Data files heading. In order to use the radioactive decay module
#correctly, you must download and unpack the above directory in your local
#area, and set the environment variable
#
#G4RADIOACTIVEDATA
#
#to point to it. 
#
#**************************************************************************
#In each of the files, lines beginning with '#' are comments.
#
#Lines beginning with a 'P' are headers, indicating metastable states of the
#nucleus which may be parent nuclei for further decays.  The first number is
#the excitation energy in keV, followed by a character representing the floating
#level.  If this is "-", the level is assumed to be fixed.  If it is
#+X, +Y, +Z, +W, +U or +V, the level is floating, or undetermined.  The number 
#following this is the half life in seconds.  Indented lines under the 'P' lines 
#are decay modes for that excitation.  There are two types of records here, 
#distinguished by the number of columns.
#
#If there are four columns only, this is a header describing the total strength
#of each type of decay from this level.  The first column is the decay mode,
#the second column is always zero, the third column indicates whether the level 
#is floating or not, as mentioned above, and the fourth column is the total 
#branching ratio going through this mode, as a fraction of unity.  Therefore,
#the fourth column of all the 4-column records under a given 'P' line should sum
#to 1.0.
#
#If there are five columns, this is a record which describes a subset of the 
#decay type, specific to a given lower level in the daughter species.  The 
#first column is the decay type, the second and third columns are the excitation
#level (keV) of the daughter reached after the decay and its floating level 
#indicator, and the fourth column is the branching ratio as a percentage
#(! note... not a fraction of unity) of the total branching ratio which came 
#from the 4-column record for this decay.
#
#In the case of beta- or beta+ decay, there will often be a sixth column
#describing the "forbiddenness" of the particular decay channel.  This is
#indicated by the tags firstForbidden, secondForbidden, thirdForbidden,
#uniqueFirstForbidden, uniqueSecondForbidden, uniqueThirdForbidden, which
#determine the shape of the beta spectrum.
#**************************************************************************
#
