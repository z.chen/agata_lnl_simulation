#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class TVector3+;
#pragma link C++ class TrackedGamma+;
#pragma link C++ class std::vector<TrackedGamma>+;
#endif

