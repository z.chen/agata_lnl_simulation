// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME EmitterDataDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "EmitterData.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_TVector3(void *p = 0);
   static void *newArray_TVector3(Long_t size, void *p);
   static void delete_TVector3(void *p);
   static void deleteArray_TVector3(void *p);
   static void destruct_TVector3(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TVector3*)
   {
      ::TVector3 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TVector3 >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TVector3", ::TVector3::Class_Version(), "TVector3.h", 22,
                  typeid(::TVector3), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TVector3::Dictionary, isa_proxy, 4,
                  sizeof(::TVector3) );
      instance.SetNew(&new_TVector3);
      instance.SetNewArray(&newArray_TVector3);
      instance.SetDelete(&delete_TVector3);
      instance.SetDeleteArray(&deleteArray_TVector3);
      instance.SetDestructor(&destruct_TVector3);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TVector3*)
   {
      return GenerateInitInstanceLocal((::TVector3*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TVector3*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_EmitterData(void *p = 0);
   static void *newArray_EmitterData(Long_t size, void *p);
   static void delete_EmitterData(void *p);
   static void deleteArray_EmitterData(void *p);
   static void destruct_EmitterData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::EmitterData*)
   {
      ::EmitterData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::EmitterData >(0);
      static ::ROOT::TGenericClassInfo 
         instance("EmitterData", ::EmitterData::Class_Version(), "EmitterData.h", 9,
                  typeid(::EmitterData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::EmitterData::Dictionary, isa_proxy, 4,
                  sizeof(::EmitterData) );
      instance.SetNew(&new_EmitterData);
      instance.SetNewArray(&newArray_EmitterData);
      instance.SetDelete(&delete_EmitterData);
      instance.SetDeleteArray(&deleteArray_EmitterData);
      instance.SetDestructor(&destruct_EmitterData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::EmitterData*)
   {
      return GenerateInitInstanceLocal((::EmitterData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::EmitterData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr TVector3::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TVector3::Class_Name()
{
   return "TVector3";
}

//______________________________________________________________________________
const char *TVector3::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TVector3*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TVector3::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TVector3*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TVector3::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TVector3*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TVector3::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TVector3*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr EmitterData::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *EmitterData::Class_Name()
{
   return "EmitterData";
}

//______________________________________________________________________________
const char *EmitterData::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EmitterData*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int EmitterData::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::EmitterData*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *EmitterData::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EmitterData*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *EmitterData::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::EmitterData*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void TVector3::Streamer(TBuffer &R__b)
{
   // Stream an object of class TVector3.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TVector3::Class(),this);
   } else {
      R__b.WriteClassBuffer(TVector3::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TVector3(void *p) {
      return  p ? new(p) ::TVector3 : new ::TVector3;
   }
   static void *newArray_TVector3(Long_t nElements, void *p) {
      return p ? new(p) ::TVector3[nElements] : new ::TVector3[nElements];
   }
   // Wrapper around operator delete
   static void delete_TVector3(void *p) {
      delete ((::TVector3*)p);
   }
   static void deleteArray_TVector3(void *p) {
      delete [] ((::TVector3*)p);
   }
   static void destruct_TVector3(void *p) {
      typedef ::TVector3 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TVector3

//______________________________________________________________________________
void EmitterData::Streamer(TBuffer &R__b)
{
   // Stream an object of class EmitterData.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(EmitterData::Class(),this);
   } else {
      R__b.WriteClassBuffer(EmitterData::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_EmitterData(void *p) {
      return  p ? new(p) ::EmitterData : new ::EmitterData;
   }
   static void *newArray_EmitterData(Long_t nElements, void *p) {
      return p ? new(p) ::EmitterData[nElements] : new ::EmitterData[nElements];
   }
   // Wrapper around operator delete
   static void delete_EmitterData(void *p) {
      delete ((::EmitterData*)p);
   }
   static void deleteArray_EmitterData(void *p) {
      delete [] ((::EmitterData*)p);
   }
   static void destruct_EmitterData(void *p) {
      typedef ::EmitterData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::EmitterData

namespace {
  void TriggerDictionaryInitialization_EmitterDataDict_Impl() {
    static const char* headers[] = {
"EmitterData.h",
0
    };
    static const char* includePaths[] = {
"/opt/root/install/include",
"/opt/root/install/include/",
"/agata07_data8/aguilera/simu/26Mgon238U/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "EmitterDataDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate(R"ATTRDUMP(A 3D physics vector)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$TVector3.h")))  __attribute__((annotate("$clingAutoload$EmitterData.h")))  TVector3;
class __attribute__((annotate("$clingAutoload$EmitterData.h")))  EmitterData;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "EmitterDataDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "EmitterData.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"EmitterData", payloadCode, "@",
"TVector3", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("EmitterDataDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_EmitterDataDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_EmitterDataDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_EmitterDataDict() {
  TriggerDictionaryInitialization_EmitterDataDict_Impl();
}
