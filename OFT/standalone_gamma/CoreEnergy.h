#ifndef __CoreEnergy__
#define __CoreEnergy__
#include <iostream>
#include <TClass.h>
#include <vector>

class CoreEnergy
{
 public:
  CoreEnergy(){;}
  ~CoreEnergy(){;}


 public: 
  inline void SetEnergy (float e) {Energy=e;};
  inline void SetDetector(int d)  {Detector=d;};

  inline float GetEnergy() {return Energy;}
  inline int   GetDetector() {return Detector;}
 private:

  float Energy;
  int   Detector;

  ClassDef(CoreEnergy,1);

};


#endif
