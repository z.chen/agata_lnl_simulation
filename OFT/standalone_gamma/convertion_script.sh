FILES="GammaEvents.0000 GammaEvents.0001 GammaEvents.0002 GammaEvents.0003 GammaEvents.0004"
#FILES="GammaEvents.1000" # GammaEvents.0001 GammaEvents.0002 GammaEvents.0003 GammaEvents.0004"

if test -f list_of_files.txt
 then      
 rm list_of_files.txt
fi 

if test $1 != 1
then
for f in $FILES
do
 if test -f $f
 then 
  rm $f
 fi
done
fi

for f in $FILES
do
  if test -f $f
  then 
    echo $f >> list_of_files.txt
  else
    echo "Converting $f"
    MINPROBSING=0.015 ./tracking_simulated /home/galileo/simu_daniele/$f 1 30000000 1 1 0 1 1 -8
    mv tracked_energies $f
#    grep " AGATA" tracked_energies > $f
    #grep " AGATA_BF" tracked_energies > BF_$f
    echo $f >> list_of_files.txt
  fi
done

#root -l -b -q 'plot_simu.C("list_of_files.txt","simu_112te.root")' 
