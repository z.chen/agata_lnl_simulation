// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME RecoilInfoDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "RecoilInfo.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_RecoilInfo(void *p = 0);
   static void *newArray_RecoilInfo(Long_t size, void *p);
   static void delete_RecoilInfo(void *p);
   static void deleteArray_RecoilInfo(void *p);
   static void destruct_RecoilInfo(void *p);
   static void streamer_RecoilInfo(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RecoilInfo*)
   {
      ::RecoilInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RecoilInfo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RecoilInfo", ::RecoilInfo::Class_Version(), "RecoilInfo.h", 8,
                  typeid(::RecoilInfo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RecoilInfo::Dictionary, isa_proxy, 16,
                  sizeof(::RecoilInfo) );
      instance.SetNew(&new_RecoilInfo);
      instance.SetNewArray(&newArray_RecoilInfo);
      instance.SetDelete(&delete_RecoilInfo);
      instance.SetDeleteArray(&deleteArray_RecoilInfo);
      instance.SetDestructor(&destruct_RecoilInfo);
      instance.SetStreamerFunc(&streamer_RecoilInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RecoilInfo*)
   {
      return GenerateInitInstanceLocal((::RecoilInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RecoilInfo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr RecoilInfo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RecoilInfo::Class_Name()
{
   return "RecoilInfo";
}

//______________________________________________________________________________
const char *RecoilInfo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RecoilInfo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RecoilInfo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RecoilInfo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RecoilInfo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RecoilInfo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RecoilInfo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RecoilInfo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void RecoilInfo::Streamer(TBuffer &R__b)
{
   // Stream an object of class RecoilInfo.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      R__b >> Beta;
      fPos.Streamer(R__b);
      R__b.CheckByteCount(R__s, R__c, RecoilInfo::IsA());
   } else {
      R__c = R__b.WriteVersion(RecoilInfo::IsA(), kTRUE);
      R__b << Beta;
      fPos.Streamer(R__b);
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RecoilInfo(void *p) {
      return  p ? new(p) ::RecoilInfo : new ::RecoilInfo;
   }
   static void *newArray_RecoilInfo(Long_t nElements, void *p) {
      return p ? new(p) ::RecoilInfo[nElements] : new ::RecoilInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_RecoilInfo(void *p) {
      delete ((::RecoilInfo*)p);
   }
   static void deleteArray_RecoilInfo(void *p) {
      delete [] ((::RecoilInfo*)p);
   }
   static void destruct_RecoilInfo(void *p) {
      typedef ::RecoilInfo current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_RecoilInfo(TBuffer &buf, void *obj) {
      ((::RecoilInfo*)obj)->::RecoilInfo::Streamer(buf);
   }
} // end of namespace ROOT for class ::RecoilInfo

namespace {
  void TriggerDictionaryInitialization_RecoilInfoDict_Impl() {
    static const char* headers[] = {
"RecoilInfo.h",
0
    };
    static const char* includePaths[] = {
"/opt/root/install/include",
"/opt/root/install/include/",
"/opt/OFT/standalone_gamma/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "RecoilInfoDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$RecoilInfo.h")))  RecoilInfo;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "RecoilInfoDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "RecoilInfo.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"RecoilInfo", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("RecoilInfoDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_RecoilInfoDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_RecoilInfoDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_RecoilInfoDict() {
  TriggerDictionaryInitialization_RecoilInfoDict_Impl();
}
