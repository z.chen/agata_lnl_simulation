#ifndef __RecoilInfo__
#define __RecoilInfo__
#include <iostream>
#include <TVector3.h>
#include <TClass.h>
#include <vector>

class RecoilInfo
{
 public:
  RecoilInfo(){;}
  ~RecoilInfo(){;}

 public:
  inline void SetBeta    (float e) {Beta =e;};
  inline void SetPosition(float x, float y, float z)
  {
    fPos.SetXYZ(x,y,z);
  }

  inline float GetBeta () {return Beta;};
  inline TVector3 GetPosition(){return fPos;};
    
 private:

  float Beta;
  TVector3 fPos;

  ClassDef(RecoilInfo,1);

};


#endif
