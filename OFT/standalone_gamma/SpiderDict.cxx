// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME SpiderDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "Spider.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_Spider(void *p = 0);
   static void *newArray_Spider(Long_t size, void *p);
   static void delete_Spider(void *p);
   static void deleteArray_Spider(void *p);
   static void destruct_Spider(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Spider*)
   {
      ::Spider *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Spider >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Spider", ::Spider::Class_Version(), "Spider.h", 8,
                  typeid(::Spider), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Spider::Dictionary, isa_proxy, 4,
                  sizeof(::Spider) );
      instance.SetNew(&new_Spider);
      instance.SetNewArray(&newArray_Spider);
      instance.SetDelete(&delete_Spider);
      instance.SetDeleteArray(&deleteArray_Spider);
      instance.SetDestructor(&destruct_Spider);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Spider*)
   {
      return GenerateInitInstanceLocal((::Spider*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Spider*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr Spider::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Spider::Class_Name()
{
   return "Spider";
}

//______________________________________________________________________________
const char *Spider::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Spider*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Spider::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Spider*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Spider::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Spider*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Spider::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Spider*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void Spider::Streamer(TBuffer &R__b)
{
   // Stream an object of class Spider.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Spider::Class(),this);
   } else {
      R__b.WriteClassBuffer(Spider::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_Spider(void *p) {
      return  p ? new(p) ::Spider : new ::Spider;
   }
   static void *newArray_Spider(Long_t nElements, void *p) {
      return p ? new(p) ::Spider[nElements] : new ::Spider[nElements];
   }
   // Wrapper around operator delete
   static void delete_Spider(void *p) {
      delete ((::Spider*)p);
   }
   static void deleteArray_Spider(void *p) {
      delete [] ((::Spider*)p);
   }
   static void destruct_Spider(void *p) {
      typedef ::Spider current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Spider

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 339,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));

      ::ROOT::AddClassAlternate("vector<float>","std::vector<float, std::allocator<float> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlESpidergR_Dictionary();
   static void vectorlESpidergR_TClassManip(TClass*);
   static void *new_vectorlESpidergR(void *p = 0);
   static void *newArray_vectorlESpidergR(Long_t size, void *p);
   static void delete_vectorlESpidergR(void *p);
   static void deleteArray_vectorlESpidergR(void *p);
   static void destruct_vectorlESpidergR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Spider>*)
   {
      vector<Spider> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Spider>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Spider>", -2, "vector", 339,
                  typeid(vector<Spider>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlESpidergR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Spider>) );
      instance.SetNew(&new_vectorlESpidergR);
      instance.SetNewArray(&newArray_vectorlESpidergR);
      instance.SetDelete(&delete_vectorlESpidergR);
      instance.SetDeleteArray(&deleteArray_vectorlESpidergR);
      instance.SetDestructor(&destruct_vectorlESpidergR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Spider> >()));

      ::ROOT::AddClassAlternate("vector<Spider>","std::vector<Spider, std::allocator<Spider> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Spider>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlESpidergR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Spider>*)0x0)->GetClass();
      vectorlESpidergR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlESpidergR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlESpidergR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Spider> : new vector<Spider>;
   }
   static void *newArray_vectorlESpidergR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Spider>[nElements] : new vector<Spider>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlESpidergR(void *p) {
      delete ((vector<Spider>*)p);
   }
   static void deleteArray_vectorlESpidergR(void *p) {
      delete [] ((vector<Spider>*)p);
   }
   static void destruct_vectorlESpidergR(void *p) {
      typedef vector<Spider> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Spider>

namespace {
  void TriggerDictionaryInitialization_SpiderDict_Impl() {
    static const char* headers[] = {
"Spider.h",
0
    };
    static const char* includePaths[] = {
"/opt/root/install/include",
"/opt/root/install/include/",
"/opt/OFT/standalone_gamma/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "SpiderDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$Spider.h")))  Spider;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "SpiderDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "Spider.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"Spider", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("SpiderDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_SpiderDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_SpiderDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_SpiderDict() {
  TriggerDictionaryInitialization_SpiderDict_Impl();
}
