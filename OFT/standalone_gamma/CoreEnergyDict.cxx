// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME CoreEnergyDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "CoreEnergy.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_CoreEnergy(void *p = 0);
   static void *newArray_CoreEnergy(Long_t size, void *p);
   static void delete_CoreEnergy(void *p);
   static void deleteArray_CoreEnergy(void *p);
   static void destruct_CoreEnergy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::CoreEnergy*)
   {
      ::CoreEnergy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::CoreEnergy >(0);
      static ::ROOT::TGenericClassInfo 
         instance("CoreEnergy", ::CoreEnergy::Class_Version(), "CoreEnergy.h", 7,
                  typeid(::CoreEnergy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::CoreEnergy::Dictionary, isa_proxy, 4,
                  sizeof(::CoreEnergy) );
      instance.SetNew(&new_CoreEnergy);
      instance.SetNewArray(&newArray_CoreEnergy);
      instance.SetDelete(&delete_CoreEnergy);
      instance.SetDeleteArray(&deleteArray_CoreEnergy);
      instance.SetDestructor(&destruct_CoreEnergy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::CoreEnergy*)
   {
      return GenerateInitInstanceLocal((::CoreEnergy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::CoreEnergy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr CoreEnergy::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *CoreEnergy::Class_Name()
{
   return "CoreEnergy";
}

//______________________________________________________________________________
const char *CoreEnergy::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CoreEnergy*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int CoreEnergy::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::CoreEnergy*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *CoreEnergy::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CoreEnergy*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *CoreEnergy::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::CoreEnergy*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void CoreEnergy::Streamer(TBuffer &R__b)
{
   // Stream an object of class CoreEnergy.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(CoreEnergy::Class(),this);
   } else {
      R__b.WriteClassBuffer(CoreEnergy::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_CoreEnergy(void *p) {
      return  p ? new(p) ::CoreEnergy : new ::CoreEnergy;
   }
   static void *newArray_CoreEnergy(Long_t nElements, void *p) {
      return p ? new(p) ::CoreEnergy[nElements] : new ::CoreEnergy[nElements];
   }
   // Wrapper around operator delete
   static void delete_CoreEnergy(void *p) {
      delete ((::CoreEnergy*)p);
   }
   static void deleteArray_CoreEnergy(void *p) {
      delete [] ((::CoreEnergy*)p);
   }
   static void destruct_CoreEnergy(void *p) {
      typedef ::CoreEnergy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::CoreEnergy

namespace ROOT {
   static TClass *vectorlECoreEnergygR_Dictionary();
   static void vectorlECoreEnergygR_TClassManip(TClass*);
   static void *new_vectorlECoreEnergygR(void *p = 0);
   static void *newArray_vectorlECoreEnergygR(Long_t size, void *p);
   static void delete_vectorlECoreEnergygR(void *p);
   static void deleteArray_vectorlECoreEnergygR(void *p);
   static void destruct_vectorlECoreEnergygR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<CoreEnergy>*)
   {
      vector<CoreEnergy> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<CoreEnergy>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<CoreEnergy>", -2, "vector", 339,
                  typeid(vector<CoreEnergy>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlECoreEnergygR_Dictionary, isa_proxy, 4,
                  sizeof(vector<CoreEnergy>) );
      instance.SetNew(&new_vectorlECoreEnergygR);
      instance.SetNewArray(&newArray_vectorlECoreEnergygR);
      instance.SetDelete(&delete_vectorlECoreEnergygR);
      instance.SetDeleteArray(&deleteArray_vectorlECoreEnergygR);
      instance.SetDestructor(&destruct_vectorlECoreEnergygR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<CoreEnergy> >()));

      ::ROOT::AddClassAlternate("vector<CoreEnergy>","std::vector<CoreEnergy, std::allocator<CoreEnergy> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<CoreEnergy>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlECoreEnergygR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<CoreEnergy>*)0x0)->GetClass();
      vectorlECoreEnergygR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlECoreEnergygR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlECoreEnergygR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<CoreEnergy> : new vector<CoreEnergy>;
   }
   static void *newArray_vectorlECoreEnergygR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<CoreEnergy>[nElements] : new vector<CoreEnergy>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlECoreEnergygR(void *p) {
      delete ((vector<CoreEnergy>*)p);
   }
   static void deleteArray_vectorlECoreEnergygR(void *p) {
      delete [] ((vector<CoreEnergy>*)p);
   }
   static void destruct_vectorlECoreEnergygR(void *p) {
      typedef vector<CoreEnergy> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<CoreEnergy>

namespace {
  void TriggerDictionaryInitialization_CoreEnergyDict_Impl() {
    static const char* headers[] = {
"CoreEnergy.h",
0
    };
    static const char* includePaths[] = {
"/opt/root/install/include",
"/opt/root/install/include/",
"/opt/OFT/standalone_gamma/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "CoreEnergyDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$CoreEnergy.h")))  CoreEnergy;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "CoreEnergyDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "CoreEnergy.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"CoreEnergy", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("CoreEnergyDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_CoreEnergyDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_CoreEnergyDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_CoreEnergyDict() {
  TriggerDictionaryInitialization_CoreEnergyDict_Impl();
}
