#ifndef __TrackedGamma__
#define __TrackedGamma__
#include <iostream>
#include <TVector3.h>
#include <TClass.h>
#include <vector>

class TrackedGamma
{
 public:
  TrackedGamma(){;}
  ~TrackedGamma(){;}

 public:
  inline void SetEnergy (float e) {Energy =e;};
  inline void SetEnergyDC (float e) {EnergyDC =e;};
  inline void SetPosition(float x, float y, float z)
  {
    fPos.SetXYZ(x,y,z);
  }

  inline float GetEnergy () {return Energy;};
  inline float GetEnergyDC () {return EnergyDC;};
  inline TVector3 GetPosition(){return fPos;};
  inline double GetTheta(){return fPos.Theta()*TMath::RadToDeg();}
 private:

  float Energy;
  float EnergyDC;
  TVector3 fPos;

  ClassDef(TrackedGamma,1);

};


#endif
