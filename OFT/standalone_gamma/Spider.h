#ifndef __Spider__
#define __Spider__
#include <iostream>
#include <TVector3.h>
#include <TClass.h>
#include <vector>

class Spider
{
 public:
  Spider(){;}
  ~Spider(){;}
  void reset(){
    gammaDC.clear();
    gammaDC_binary.clear();
    thetaGS.clear();
    thetaGS_binary.clear();
    energy      = 0.;
    beta        = -10.;
    thetaCM     = 0.;
    betaBinary  = 0.;
    eBinary     = 0.;
    betaBinary  = 0.;
    thetaBinary = 0.;
    phiBinary   = 0.;
    fPos.SetMagThetaPhi(1,0,0);
  }
 public:
  inline void SetReaction(int aT, int aP, float eB){
    massT = 931.494*aT;
    massP = 931.494*aP;
    eBeam = eB;
    tau_kin=massT/massP;
  }
  inline void SetEnergy (float e) {energy =e;};
  inline void SetPosition(float x, float y, float z)
  {
    fPos.SetXYZ(x,y,z);
  }
  inline void SetPosition(TVector3 vec)
  {
    fPos = vec;
  }

  inline void AddGamma(float e, TVector3 gPos)
  {
    if(beta<0){
      beta    = TMath::Sqrt(1-TMath::Power(massP/(massP+energy),2));
      thetaCM = 2*TMath::ATan((TMath::Cos(fPos.Theta())-
			       TMath::Sqrt(1-TMath::Sin(fPos.Theta())*
					   TMath::Sin(fPos.Theta())
					   *tau_kin*tau_kin))/
			      (TMath::Sin(fPos.Theta())*(tau_kin-1)));
      thetaBinary = TMath::ATan((TMath::Sin(thetaCM))/(1-TMath::Cos(thetaCM)));
      phiBinary   = fPos.Phi()+TMath::Pi();
      fPosBP.SetMagThetaPhi(1,thetaBinary,phiBinary);
      eBinary     = massP*massT/((massP+massT)*(massP+massT))*(2-2*TMath::Cos(thetaCM))*eBeam;
      betaBinary  =  TMath::Sqrt(1-TMath::Power(massT/(massT+eBinary),2));
      std::cout << "beta = " << beta << " beta binary = " << betaBinary 
		<< " thetaCM " << thetaCM 
		<< " thetaBinary " << thetaBinary 
		<< " eBinary " << eBinary
		<< " tau_kin = "<< tau_kin 
		<< " TMath::Sin(fPos.Theta())" <<  TMath::Sin(fPos.Theta()) << std::endl; 
    }
  
    thetaGS.push_back(gPos.Angle(fPos));
    

    gammaDC.push_back(e*(1/TMath::Sqrt(1-beta*beta)*(1-beta*TMath::Cos(thetaGS.back()))));
    thetaGS_binary.push_back(gPos.Angle(fPosBP));
    gammaDC_binary.push_back(e*(1/TMath::Sqrt(1-betaBinary*betaBinary)*(1-betaBinary*TMath::Cos(thetaGS_binary.back()))));
  }

  inline float GetEnergy () {return energy;};
  inline TVector3 GetPosition(){return fPos;};
  inline TVector3 GetPositionBP() { return fPosBP; }
  inline std::vector<float> GetGammaDC()    {return gammaDC;};
  inline std::vector<float> GetGammaDCBin() {return gammaDC_binary;};

 private:

  float energy;
  float eBeam;
  float tau_kin;
  float massT;
  float massP;
  float thetaCM;
  float beta;
  float eBinary;
  float betaBinary;
  float thetaBinary;
  float phiBinary;
  std::vector<float> gammaDC;
  std::vector<float> gammaDC_binary;
  TVector3 fPos;
  TVector3 fPosBP;
  std::vector<float> thetaGS;
  std::vector<float> thetaGS_binary;
  ClassDef(Spider,1);

};


#endif
