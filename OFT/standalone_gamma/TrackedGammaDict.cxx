// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TrackedGammaDict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "TrackedGamma.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_TrackedGamma(void *p = 0);
   static void *newArray_TrackedGamma(Long_t size, void *p);
   static void delete_TrackedGamma(void *p);
   static void deleteArray_TrackedGamma(void *p);
   static void destruct_TrackedGamma(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackedGamma*)
   {
      ::TrackedGamma *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::TrackedGamma >(0);
      static ::ROOT::TGenericClassInfo 
         instance("TrackedGamma", ::TrackedGamma::Class_Version(), "TrackedGamma.h", 8,
                  typeid(::TrackedGamma), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::TrackedGamma::Dictionary, isa_proxy, 4,
                  sizeof(::TrackedGamma) );
      instance.SetNew(&new_TrackedGamma);
      instance.SetNewArray(&newArray_TrackedGamma);
      instance.SetDelete(&delete_TrackedGamma);
      instance.SetDeleteArray(&deleteArray_TrackedGamma);
      instance.SetDestructor(&destruct_TrackedGamma);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackedGamma*)
   {
      return GenerateInitInstanceLocal((::TrackedGamma*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TrackedGamma*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr TrackedGamma::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TrackedGamma::Class_Name()
{
   return "TrackedGamma";
}

//______________________________________________________________________________
const char *TrackedGamma::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackedGamma*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TrackedGamma::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::TrackedGamma*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TrackedGamma::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackedGamma*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TrackedGamma::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::TrackedGamma*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void TrackedGamma::Streamer(TBuffer &R__b)
{
   // Stream an object of class TrackedGamma.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(TrackedGamma::Class(),this);
   } else {
      R__b.WriteClassBuffer(TrackedGamma::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_TrackedGamma(void *p) {
      return  p ? new(p) ::TrackedGamma : new ::TrackedGamma;
   }
   static void *newArray_TrackedGamma(Long_t nElements, void *p) {
      return p ? new(p) ::TrackedGamma[nElements] : new ::TrackedGamma[nElements];
   }
   // Wrapper around operator delete
   static void delete_TrackedGamma(void *p) {
      delete ((::TrackedGamma*)p);
   }
   static void deleteArray_TrackedGamma(void *p) {
      delete [] ((::TrackedGamma*)p);
   }
   static void destruct_TrackedGamma(void *p) {
      typedef ::TrackedGamma current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackedGamma

namespace ROOT {
   static TClass *vectorlETrackedGammagR_Dictionary();
   static void vectorlETrackedGammagR_TClassManip(TClass*);
   static void *new_vectorlETrackedGammagR(void *p = 0);
   static void *newArray_vectorlETrackedGammagR(Long_t size, void *p);
   static void delete_vectorlETrackedGammagR(void *p);
   static void deleteArray_vectorlETrackedGammagR(void *p);
   static void destruct_vectorlETrackedGammagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TrackedGamma>*)
   {
      vector<TrackedGamma> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TrackedGamma>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TrackedGamma>", -2, "vector", 339,
                  typeid(vector<TrackedGamma>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETrackedGammagR_Dictionary, isa_proxy, 4,
                  sizeof(vector<TrackedGamma>) );
      instance.SetNew(&new_vectorlETrackedGammagR);
      instance.SetNewArray(&newArray_vectorlETrackedGammagR);
      instance.SetDelete(&delete_vectorlETrackedGammagR);
      instance.SetDeleteArray(&deleteArray_vectorlETrackedGammagR);
      instance.SetDestructor(&destruct_vectorlETrackedGammagR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TrackedGamma> >()));

      ::ROOT::AddClassAlternate("vector<TrackedGamma>","std::vector<TrackedGamma, std::allocator<TrackedGamma> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TrackedGamma>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETrackedGammagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TrackedGamma>*)0x0)->GetClass();
      vectorlETrackedGammagR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETrackedGammagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETrackedGammagR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TrackedGamma> : new vector<TrackedGamma>;
   }
   static void *newArray_vectorlETrackedGammagR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TrackedGamma>[nElements] : new vector<TrackedGamma>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETrackedGammagR(void *p) {
      delete ((vector<TrackedGamma>*)p);
   }
   static void deleteArray_vectorlETrackedGammagR(void *p) {
      delete [] ((vector<TrackedGamma>*)p);
   }
   static void destruct_vectorlETrackedGammagR(void *p) {
      typedef vector<TrackedGamma> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TrackedGamma>

namespace {
  void TriggerDictionaryInitialization_TrackedGammaDict_Impl() {
    static const char* headers[] = {
"TrackedGamma.h",
0
    };
    static const char* includePaths[] = {
"/opt/root/install/include",
"/opt/root/install/include/",
"/opt/OFT/standalone_gamma/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "TrackedGammaDict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$TrackedGamma.h")))  TrackedGamma;
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "TrackedGammaDict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "TrackedGamma.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"TrackedGamma", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TrackedGammaDict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TrackedGammaDict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TrackedGammaDict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TrackedGammaDict() {
  TriggerDictionaryInitialization_TrackedGammaDict_Impl();
}
