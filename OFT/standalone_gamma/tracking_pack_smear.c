#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "tracking_define.h"
#include "tracking_utilitaires.h"

double drand48(void);

int packpoints(int number, double posx[], double posy[], double posz[], double energy[], int detnumb[], int segnumb[])
{
    int i, j, n, l, jp[5000], jjp[5000], ip[5000], iip[5000];
    double rpack, esum;
    l = 0;

    /* check which interactions are within precision of each other in the same segment */
    for (i = 0; i < number; i++) {
	for (j = i + 1; j < number; j++) {
	    rpack = sqrt(SQ(posx[i] - posx[j]) + SQ(posy[i] - posy[j]) + SQ(posz[i] - posz[j]));
	    if (rpack < resolution && segnumb[i]==segnumb[j] && detnumb[i]==detnumb[j]) {
		l++;
		jp[l] = j;
		jjp[l] = j;
		ip[l] = i;
		iip[l] = i;
	    }
	}
    }

    /* check if couples have already been packed by previous couples */
    for (i = 1; i <= l; i++) {
	for (j = i + 1; j <= l; j++) {
	    if (ip[i] == ip[j]) {	/*&& ip[i] != -1) { */
		for (n = j + 1; n <= l; n++) {
		    if (ip[n] == jp[i] && jp[n] == jp[j]) {
			iip[n] = -1;
			jjp[n] = -1;
		    }
		    if (ip[n] == jp[j] && jp[n] == jp[i]) {
			iip[n] = -1;
			jjp[n] = -1;
		    }
		}
	    }
	}
    }

    for (n = 1; n <= l; n++) {
	if (iip[n] == -1)
	    ip[n] = -1;
	if (jjp[n] == -1)
	    jp[n] = -1;
    }
    
    for (j = 1; j <= l; j++) {
	for (i = 0; i < number; i++) {
	    if (ip[j] == i && jp[j] != i) {
		esum = energy[i] + energy[jp[j]];

		/* new position pondered by energie */
		posx[i] = ((posx[i] * energy[i]) + (posx[jp[j]] * energy[jp[j]])) / esum;
		posy[i] = ((posy[i] * energy[i]) + (posy[jp[j]] * energy[jp[j]])) / esum;
		posz[i] = ((posz[i] * energy[i]) + (posz[jp[j]] * energy[jp[j]])) / esum;
		energy[i] = esum;	/* put 2 energies into 1 */
		swap(energy, number - 1, jp[j]);	/* put unused energy at the end of the list */
		swap(posx, number - 1, jp[j]);
		swap(posy, number - 1, jp[j]);
		swap(posz, number - 1, jp[j]);
		swapi(detnumb, number - 1, jp[j]);
		swapi(segnumb, number - 1, jp[j]);
		for (n = j + 1; n <= l; n++) {
		    if (ip[n] == jp[j]) {	/* if the one just packed needs to be packed */
			ip[n] = i;
		    }
		    if (jp[n] == jp[j]) {	/* if the one just packed needs to be packed */
			jp[n] = i;
		    }
		    if (ip[n] == number - 1) {	/* if end of list needs to be packed */
			ip[n] = jp[j];
		    }
		    if (jp[n] == number - 1) {	/* if end of list needs to be packed */
			jp[n] = jp[j];
		    }
		}
		number -= 1;	/* decrement the number of interactions */
	    }
	}
    }
    return number;		/* return new number of interactions */
}

int packpointsbarycenter(int number, double posx[], double posy[], double posz[], double energy[], int detnumb[], int segnumb[])
{
	int i,j;
	double en[800];
	
	for(i=0;i<number;i++){
		
		
	    for(j=i+1;j<number;j++){
			if(energy[i]!=0 && detnumb[i]==detnumb[j] && segnumb[i]==segnumb[j]){
				
				en[i]=energy[i]+energy[j];
				posx[i]=((energy[i]*posx[i])+(energy[j]*posx[j]))/en[i];
				posy[i]=((energy[i]*posy[i])+(energy[j]*posy[j]))/en[i];
				posz[i]=((energy[i]*posz[i])+(energy[j]*posz[j]))/en[i];
				
				
				energy[j]=0;
				
				energy[i]=en[i];
				
			}
			
		}
		
	}
	
	
	j=0;
	for(i=0;i<number;i++){
		if(energy[i]!=0){
			energy[j]=energy[i];
			posx[j]=posx[i];
			posy[j]=posy[i];
			posz[j]=posz[i];
			detnumb[j]=detnumb[i];
			segnumb[j]=segnumb[i];
			j++;
		}
		
	}
	number = j;
	
	
    return number;		/* return new number of interactions */
}

int packpointscenter(int number, double posx[], double posy[], double posz[], double energy[], int detnumb[], int segnumb[], double segx[180][60], double segy[180][60], double segz[180][60])
{
	int i, j;
	double en[800];
	
	for(i=0;i<number;i++){
		
		
	    for(j=i+1;j<number;j++){
			if(energy[i]!=0 && detnumb[i]==detnumb[j] && segnumb[i]==segnumb[j]){
				
				en[i]=energy[i]+energy[j];
				posx[i]=segx[detnumb[i]][segnumb[i]]/10.; /* in cm */
				posy[i]=segy[detnumb[i]][segnumb[i]]/10.;
				posz[i]=segz[detnumb[i]][segnumb[i]]/10.;
							
				energy[j]=0;
				
				energy[i]=en[i];
				
			}
			
		}
		
	}
	
	
	j=0;
	for(i=0;i<number;i++){
		if(energy[i]!=0){
			energy[j]=energy[i];
			posx[j]=posx[i];
			posy[j]=posy[i];
			posz[j]=posz[i];
			detnumb[j]=detnumb[i];
			segnumb[j]=segnumb[i];
			j++;
		}
		
	}
	number = j;
	
	
    return number;		/* return new number of interactions */
}




void smearpoints(int number, double energy[], double posx[], double posy[], double posz[], double errorfc[], double step, int nbsteps)
{

    int i, j;
    double tru, err,error_p;
	
    err = 0;

    for (i = 0; i < number; i++) {
	
      /*error_p = 0.5*sqrt(0.1/energy[i])/2.35;*/ /* fwhm/2.35 in cm */
      /*this is from NIM A 638 PA Söderström et al.*/
      error_p = (.27+.62*sqrt(0.1/energy[i]))/2.35/sqrt(3.); 
    
		
		tru = drand48();

	/* printf("random = %f \n",tru); */
	if (tru < errorfc[0])
	    err = step;
	for (j = 0; j < nbsteps; j++) {
	    if (errorfc[j] > tru) {
		err = j * step;
		break;
	    }
	}
	tru = drand48();
	if (tru > 0.5)		/* if random > 0.5 dx=>dx */
	    err = err;
	else			/* if random <=0.5 dx=>-dx */
	    err = -err;
		
	posx[i] = posx[i] + error_p * err;
		
		
	tru = drand48();
	if (tru < errorfc[0])
	    err = step;
	for (j = 0; j < nbsteps; j++) {
	    if (errorfc[j] > tru) {
		err = j * step;
		break;
	    }
	}

	tru = drand48();
	if (tru > 0.5)
	    err = err;
	else
	    err = -err;
		
	posy[i] = posy[i] + error_p * err;
		
		
	tru = drand48();
	if (tru < errorfc[0])
	    err = step;
	for (j = 0; j < nbsteps; j++) {
	    if (errorfc[j] > tru) {
		err = j * step;
		break;
	    }
	}
	tru = drand48();
	if (tru > 0.5)
	    err = err;
	else
	    err = -err;
		
	posz[i] = posz[i] + error_p * err;
		
    }
}

void smearenergies(int number, double energy[], double errorfc[], double step, int nbsteps)
{
	
    int i, j;
    double tru, err,error_e;
	
    err = 0;
	
    for (i = 0; i < number; i++) {
		
		error_e = sqrt(1 + energy[i]*3.7)/2.35; /* fwhm/2.35 in keV  */
		
		tru = drand48();
		
		/* printf("random = %f \n",tru); */
		if (tru < errorfc[0])
			err = step;
		for (j = 0; j < nbsteps; j++) {
			if (errorfc[j] > tru) {
				err = j * step;
				break;
			}
		}
		tru = drand48();
		if (tru > 0.5)		/* if random > 0.5 dx=>dx */
			err = err;
		else			/* if random <=0.5 dx=>-dx */
			err = -err;
		
		energy[i] = energy[i]+(err*error_e)/1000; /* add error in MeV */
		if(energy[i]<0)energy[i] = fabs((err*error_e)/1000);
		
    }
}


int energy_thresh(int number, double posx[], double posy[], double posz[], double energy[], int detnumb[], int segnumb[])
{

    int i, j, newnumber, detnumbn[800], segnumbn[800];

    double en[800], xn[800], yn[800], zn[800];

    j = 0;
    for (i = 0; i < number; i++) {
	if (energy[i] > threshold) {
	    en[j] = energy[i];
	    xn[j] = posx[i];
	    yn[j] = posy[i];
	    zn[j] = posz[i];
	    detnumbn[j] = detnumb[i];
	    segnumbn[j] = segnumb[i];
	    j++;
	}
    }

    newnumber = j;
    for (i = 0; i < newnumber; i++) {
	energy[i] = en[i];
	posx[i] = xn[i];
	posy[i] = yn[i];
	posz[i] = zn[i];
	detnumb[i] = detnumbn[i];
	segnumb[i] = segnumbn[i];
    }
    return newnumber;
}

