#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
/*#ifdef OPTIMACML
#include "acml_mv.h"
#endif*/
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
/* #include <unistd.h> */

#include "tracking_define.h"
#include "tracking_data_manip.h"
#include "tracking_utilitaires.h"
#include "tracking_physics.h"

/*
* Recherche des groupements d interractions
* Retourne nb de clusters trouves
*/
int cluster_search(int intnumber[intmax],	/* Nbre interractions par cluster */
                   double anglecluster[intmax],	/*Angle alpha du cluster */
                   double ecluster[intmax],	/* E totale du cluster */
                   int id[intmax*kmax],	/* [Numero de cluster][Numero d interraction dans le cluster] = Numero interraction dans fichier */
                   int number,	/* Nb total interraction dans detecteur */
                   double energy[intmax], 
                   double thetaangle[intmax], 
                   double phiangle[intmax])
{
  int i, j, n, k;
  double costest1, max_opening_angle, opening_angle, power, flagu[intmax];
#ifdef OPTIM_SINCOS
  double cosangth, sinangth, costhetaang, sinthetaang;
#endif
  double angth, angph;
  n = 0;			/* number of clusters */
  power = pow(((number + 2) / 3), 0.9);
  if (number > 0)
    max_opening_angle = acos(1 - 2 / power);
  else
    max_opening_angle = 0;		/* max value of angle computed as a function of number of interactions in the event */
  opening_angle = min_opening_angle - step_in_opening_angle;
  do {
    opening_angle = opening_angle + step_in_opening_angle;

    for (i = 0; i < number; i++)
      flagu[i] = 0;	/* flagging interaction points to use once per alfa value */
    for (i = 0; i < number; i++) {
      ecluster[n] = 0;
      k = 0;
      if (flagu[i] == 0) {
        id[n*kmax+k] = i;
        ecluster[n] = ecluster[n] + energy[i];
        angth = thetaangle[id[n*kmax+k]];
        angph = phiangle[id[n*kmax+k]];
        k++;
        flagu[i] = 1;
        for (j = 0; ((j < number) && (k < kmax)); j++) {
          if (j != i && flagu[j] == 0) {
#ifdef OPTIM_SINCOS
            sincos(angth, &cosangth, &sinangth);
            sincos(thetaangle[j], &costhetaang, &sinthetaang);
            costest1 = acos((cosangth * costhetaang) + ((sinangth * sinthetaang) * cos(phiangle[j] - angph)));
#else
            costest1 = (cos(angth) * cos(thetaangle[j]));
            costest1 = costest1 + ((sin(angth) * sin(thetaangle[j])) * cos(phiangle[j] - angph));
            costest1 = acos(costest1);
#endif 
            if ((fabs(costest1) <= opening_angle)) {
              ecluster[n] = ecluster[n] + energy[j];
              id[n*kmax+k] = j;
              k++;
              flagu[j] = 1;
              angth = thetaangle[j];
              angph = phiangle[j];
            }
          }
        }
        intnumber[n] = k;
        anglecluster[n] = opening_angle;
        n++;
      }
    }

    /**************************************************/
    /*********** loop on alpha values *****************/
    /**************************************************/
  } while (opening_angle < max_opening_angle);
  return n;
}


/*
* Compare clusters trouves a la Physique
* Calcule toutes les permutations possibles des interractions dans un cluster 
* Pour tous les clusters
*/
void cluster_evaluation(int number,
                        double ecluster[intmax],
                        int intnumber[intmax],
                        int id[intmax*kmax],
                        double probability[intmax],
                        int ordering[intmax*kmax],
                        double energy[intmax],
                        double posx[intmax],
                        double posy[intmax],
                        double posz[intmax],
                        double r[intmax*intmax],
                        double r_ge[intmax*intmax],
                        int clusterflag[intmax],
                        double source[3])
{
  int cluster_number, hit0, hit1, hit2, hit3, hit4, hit5, hit6;
  double interaction_sequence_prob;
  double scalarproduct_s01, scalarproduct_012, scalarproduct_123,scalarproduct_234,scalarproduct_345,scalarproduct_456;
  double total_cluster_energy, costheta, ercos;
  double cross0_compt,cross0_photo,cross0_pair,cross0_tot;
  double cross1_compt,cross1_photo,cross1_pair,cross1_tot;
  double cross2_compt,cross2_photo,cross2_pair,cross2_tot;
  double cross3_compt,cross3_photo,cross3_pair,cross3_tot;
  double cross4_compt,cross4_photo,cross4_pair,cross4_tot;
  double cross5_compt,cross5_photo,cross5_pair,cross5_tot;
  double cross6_compt,cross6_photo,cross6_pair,cross6_tot;
  double prob_for_range0,prob_for_range1, prob_for_range2,prob_for_range3,prob_for_range4,prob_for_range5,prob_for_range6;
  double prob_for_interaction0,prob_for_interaction1,prob_for_interaction2,prob_for_interaction3,prob_for_interaction4;
  double prob_for_interaction5,prob_for_interaction6;
  double test1,prob1,test2,prob2,test3,prob3,test4,prob4,test5,prob5,test6,prob6;
  double escatter1,escatter1_geom, escatter2, escatter2_geom, escatter3, escatter3_geom, escatter4, escatter4_geom;
  double escatter5, escatter5_geom,escatter6,escatter6_geom;
  double deltaesc1,deltaesc1_geom, deltaesc2, deltaesc2_geom, deltaesc3, deltaesc3_geom, deltaesc4, deltaesc4_geom;
  double deltaesc5, deltaesc5_geom,deltaesc6,deltaesc6_geom;
  double xsource,ysource,zsource;

  int number_of_interactions_in_cluster;

  xsource = source[0];
  ysource = source[1];
  zsource = source[2];

  for (cluster_number = 0; cluster_number < number; cluster_number++) {	/* Pour chaque cluster */

    total_cluster_energy = ecluster[cluster_number];
    clusterflag[cluster_number] = 0;
    number_of_interactions_in_cluster = intnumber[cluster_number];

    if (number_of_interactions_in_cluster == 1) {
      if(getenv("MINPROBTRACK")) {
	interaction_sequence_prob = atof(getenv("MINPROBTRACK"));
      }  else {
	interaction_sequence_prob = minprobtrack;
      }
      ordering[cluster_number*kmax+0] = id[cluster_number*kmax+0];
    }
    else {
      interaction_sequence_prob = 0;

      cross0_photo = sig_abs(total_cluster_energy);
      cross0_compt = sig_compt(total_cluster_energy);
      cross0_pair = sig_pair(total_cluster_energy);
      cross0_tot = cross0_compt + cross0_photo + cross0_pair;
      prob_for_interaction0 = cross0_compt /cross0_tot;
      prob_for_range0 = range_process(cross0_compt);

      for (hit0 = 0; hit0 < number_of_interactions_in_cluster; hit0++) {	/* Pas plus de 7 interractions */

        escatter1 = total_cluster_energy - energy[id[cluster_number*kmax+hit0]];
        cross1_photo = sig_abs(escatter1);
        cross1_compt = sig_compt(escatter1);
        cross1_pair = sig_pair(escatter1);
        cross1_tot =cross1_photo + cross1_compt + cross1_pair;

        if (number_of_interactions_in_cluster == 2) {
          prob_for_interaction1 = cross1_photo / cross1_tot ;
          prob_for_range1 = range_process(cross1_photo);
        }
        else {
          prob_for_interaction1 = cross1_compt /cross1_tot;
          prob_for_range1 = range_process(cross1_compt);
        }

        for (hit1 = 0; hit1 < number_of_interactions_in_cluster; hit1++) {
#ifdef DEBUG
          printf ("hit1 = %d, ",hit1);
#endif
          if (hit1 != hit0) {
            scalarproduct_s01 = (posx[id[cluster_number*kmax+hit0]] - xsource) * (posx[id[cluster_number*kmax+hit1]] - posx[id[cluster_number*kmax+hit0]])
              + (posy[id[cluster_number*kmax+hit0]] - ysource) * (posy[id[cluster_number*kmax+hit1]] - posy[id[cluster_number*kmax+hit0]])
              + (posz[id[cluster_number*kmax+hit0]] - zsource) * (posz[id[cluster_number*kmax+hit1]] - posz[id[cluster_number*kmax+hit0]]);

            costheta = scalarproduct_s01 / (r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]] * r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]);
            ercos = err_cos(xsource, posx[id[cluster_number*kmax+hit0]], posx[id[cluster_number*kmax+hit1]], ysource, posy[id[cluster_number*kmax+hit0]], posy[id[cluster_number*kmax+hit1]], zsource, posz[id[cluster_number*kmax+hit0]], posz[id[cluster_number*kmax+hit1]], r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]], r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]);
            escatter1_geom = total_cluster_energy / (1 + (total_cluster_energy / mec2) * (1 - costheta));

            deltaesc1_geom = SQ(escatter1_geom) * ercos / mec2;
            deltaesc1 = (number_of_interactions_in_cluster + 1) * eresSQ;

            test1 = -2. * SQ(escatter1 - escatter1_geom) / (SQ(deltaesc1_geom) + deltaesc1);

            if (test1 >= -9.903487552536127225) {

              if (number_of_interactions_in_cluster > 2) {
                escatter2 = escatter1 - energy[id[cluster_number*kmax+hit1]];
                cross2_photo = sig_abs(escatter2);
                cross2_compt = sig_compt(escatter2);
                cross2_pair = sig_pair(escatter2);
                cross2_tot = cross2_photo + cross2_compt + cross2_pair ;
                if (number_of_interactions_in_cluster == 3) {
                  prob_for_interaction2 = cross2_photo / cross2_tot ;
                  prob_for_range2 = range_process(cross2_photo);
                }
                else {
                  prob_for_interaction2 = cross2_compt / cross2_tot ;
                  prob_for_range2 = range_process(cross2_compt);
                }

                for (hit2 = 0; hit2 < number_of_interactions_in_cluster; hit2++) {
#ifdef DEBUG
                  printf ("hit2 = %d, ",hit2);
#endif
                  if (hit2 != hit0 && hit2 != hit1) {
                    scalarproduct_012 = (posx[id[cluster_number*kmax+hit1]] - posx[id[cluster_number*kmax+hit0]]) * (posx[id[cluster_number*kmax+hit2]] - posx[id[cluster_number*kmax+hit1]])
                      + (posy[id[cluster_number*kmax+hit1]] - posy[id[cluster_number*kmax+hit0]]) * (posy[id[cluster_number*kmax+hit2]] - posy[id[cluster_number*kmax+hit1]])
                      + (posz[id[cluster_number*kmax+hit1]] - posz[id[cluster_number*kmax+hit0]]) * (posz[id[cluster_number*kmax+hit2]] - posz[id[cluster_number*kmax+hit1]]);
                    costheta = scalarproduct_012 / (r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]] * r[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]);
                    ercos = err_cos(posx[id[cluster_number*kmax+hit0]], posx[id[cluster_number*kmax+hit1]], posx[id[cluster_number*kmax+hit2]], posy[id[cluster_number*kmax+hit0]], posy[id[cluster_number*kmax+hit1]], posy[id[cluster_number*kmax+hit2]], posz[id[cluster_number*kmax+hit0]], posz[id[cluster_number*kmax+hit1]], posz[id[cluster_number*kmax+hit2]], r[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]], r[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]);
                    escatter2_geom = escatter1 / (1 + (escatter1 / mec2) * (1 - costheta));
                    deltaesc2_geom = SQ(escatter2_geom) * ercos / mec2;
                    deltaesc2 = (number_of_interactions_in_cluster + 2) * eresSQ;

                    test2 = -SQ(escatter2 - escatter2_geom) / (SQ(deltaesc2_geom) + deltaesc2);

                    if (test2 >= -9.903487552536127225) {

                      if (number_of_interactions_in_cluster > 3) {

                        escatter3 = escatter2 - energy[id[cluster_number*kmax+hit2]];
                        cross3_photo=sig_abs(escatter3);
                        cross3_compt = sig_compt(escatter3);
                        cross3_pair = sig_pair(escatter3);
                        cross3_tot = cross3_photo + cross3_compt + cross3_pair ;

                        if (number_of_interactions_in_cluster == 4) {
                          prob_for_interaction3 = cross3_photo / cross3_tot ;
                          prob_for_range3=range_process(cross3_photo);
                        }
                        else {
                          prob_for_interaction3 = cross3_compt /cross3_tot ;
                          prob_for_range3 = range_process(cross3_compt);
                        }

                        for (hit3 = 0; hit3 < number_of_interactions_in_cluster; hit3++) {
#ifdef DEBUG
                          printf ("hit3 = %d, ",hit3);
#endif
                          if (hit3 != hit0 && hit3 != hit1 && hit3 != hit2) {
                            scalarproduct_123 = (posx[id[cluster_number*kmax+hit2]] - posx[id[cluster_number*kmax+hit1]]) * (posx[id[cluster_number*kmax+hit3]] - posx[id[cluster_number*kmax+hit2]])
                              + (posy[id[cluster_number*kmax+hit2]] - posy[id[cluster_number*kmax+hit1]]) * (posy[id[cluster_number*kmax+hit3]] - posy[id[cluster_number*kmax+hit2]])
                              + (posz[id[cluster_number*kmax+hit2]] - posz[id[cluster_number*kmax+hit1]]) * (posz[id[cluster_number*kmax+hit3]] - posz[id[cluster_number*kmax+hit2]]);
                            costheta = scalarproduct_123 / (r[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]] * r[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]);
                            ercos = err_cos(posx[id[cluster_number*kmax+hit1]], posx[id[cluster_number*kmax+hit2]], posx[id[cluster_number*kmax+hit3]], posy[id[cluster_number*kmax+hit1]], posy[id[cluster_number*kmax+hit2]], posy[id[cluster_number*kmax+hit3]], posz[id[cluster_number*kmax+hit1]], posz[id[cluster_number*kmax+hit2]], posz[id[cluster_number*kmax+hit3]], r[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]], r[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]);
                            escatter3_geom = escatter2 / (1 + (escatter2 / mec2) * (1 - costheta));
                            deltaesc3_geom = SQ(escatter3_geom) * ercos / mec2;
                            deltaesc3 = (number_of_interactions_in_cluster + 3) * eresSQ;

                            test3 = (-SQ(escatter3 - escatter3_geom) / (SQ(deltaesc3_geom) + deltaesc3));

                            if (test3 >= -9.903487552536127225) {

                              if (number_of_interactions_in_cluster > 4) {

                                escatter4 = escatter3 - energy[id[cluster_number*kmax+hit3]];
                                cross4_photo=sig_abs(escatter4);
                                cross4_compt = sig_compt(escatter4);
                                cross4_pair = sig_pair(escatter4);
                                cross4_tot = cross4_photo + cross4_compt + cross4_pair;

                                if (number_of_interactions_in_cluster == 5) {
                                  prob_for_interaction4 = cross4_photo / cross4_tot ;
                                  prob_for_range4 = range_process(cross4_photo);
                                }
                                else {
                                  prob_for_interaction4 = cross4_compt / cross4_tot ;
                                  prob_for_range4 = range_process(cross4_compt);
                                }

                                for (hit4 = 0; hit4 < number_of_interactions_in_cluster; hit4++) {
#ifdef DEBUG
                                  printf ("hit4 = %d, ",hit4);
#endif
                                  if (hit4 != hit3 && hit4 != hit2 && hit4 != hit1 && hit4 != hit0) {
                                    scalarproduct_234 = (posx[id[cluster_number*kmax+hit3]] - posx[id[cluster_number*kmax+hit2]]) * (posx[id[cluster_number*kmax+hit4]] - posx[id[cluster_number*kmax+hit3]])
                                      + (posy[id[cluster_number*kmax+hit3]] - posy[id[cluster_number*kmax+hit2]]) * (posy[id[cluster_number*kmax+hit4]] - posy[id[cluster_number*kmax+hit3]])
                                      + (posz[id[cluster_number*kmax+hit3]] - posz[id[cluster_number*kmax+hit2]]) * (posz[id[cluster_number*kmax+hit4]] - posz[id[cluster_number*kmax+hit3]]);
                                    costheta = scalarproduct_234 / (r[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]] * r[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]]);
                                    ercos = err_cos(posx[id[cluster_number*kmax+hit2]], posx[id[cluster_number*kmax+hit3]], posx[id[cluster_number*kmax+hit4]], posy[id[cluster_number*kmax+hit2]], posy[id[cluster_number*kmax+hit3]], posy[id[cluster_number*kmax+hit4]], posz[id[cluster_number*kmax+hit2]], posz[id[cluster_number*kmax+hit3]], posz[id[cluster_number*kmax+hit4]], r[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]], r[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]]);
                                    escatter4_geom = escatter3 / (1 + (escatter3 / mec2) * (1 - costheta));
                                    deltaesc4_geom = SQ(escatter4_geom) * ercos / mec2;

                                    deltaesc4 = (number_of_interactions_in_cluster + 4) * eresSQ;

                                    test4 = (-SQ(escatter4_geom - escatter4) / (SQ(deltaesc4_geom) + deltaesc4));
                                    if (test4 >= -9.903487552536127225) {

                                      if (number_of_interactions_in_cluster > 5) {

                                        escatter5 = escatter4 - energy[id[cluster_number*kmax+hit4]];
                                        cross5_photo=sig_abs(escatter5);
                                        cross5_compt = sig_compt(escatter5);
                                        cross5_pair = sig_pair(escatter5);
                                        cross5_tot = cross5_photo + cross5_compt + cross5_pair ;

                                        if (number_of_interactions_in_cluster == 6) {
                                          prob_for_interaction5 = cross5_photo / cross5_tot ;
                                          prob_for_range5 = range_process(cross5_photo);
                                        }
                                        else {
                                          prob_for_interaction5 = cross5_photo / cross5_tot ;
                                          prob_for_range5 = range_process(cross5_photo);
                                        }

                                        for (hit5 = 0; hit5 < number_of_interactions_in_cluster; hit5++) {
#ifdef DEBUG
                                          printf ("hit5 = %d, ",hit5);
#endif
                                          if (hit5 != hit0 && hit5 != hit1 && hit5 != hit2 && hit5 != hit3 && hit5 != hit4) {
                                            scalarproduct_345 = (posx[id[cluster_number*kmax+hit4]] - posx[id[cluster_number*kmax+hit3]]) * (posx[id[cluster_number*kmax+hit5]] - posx[id[cluster_number*kmax+hit4]])
                                              + (posy[id[cluster_number*kmax+hit4]] - posy[id[cluster_number*kmax+hit3]]) * (posy[id[cluster_number*kmax+hit5]] - posy[id[cluster_number*kmax+hit4]])
                                              + (posz[id[cluster_number*kmax+hit4]] - posz[id[cluster_number*kmax+hit3]]) * (posz[id[cluster_number*kmax+hit5]] - posz[id[cluster_number*kmax+hit4]]);
                                            costheta = scalarproduct_345 / (r[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]] * r[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]]);
                                            ercos = err_cos(posx[id[cluster_number*kmax+hit3]], posx[id[cluster_number*kmax+hit4]], posx[id[cluster_number*kmax+hit5]], posy[id[cluster_number*kmax+hit3]], posy[id[cluster_number*kmax+hit4]], posy[id[cluster_number*kmax+hit5]], posz[id[cluster_number*kmax+hit3]], posz[id[cluster_number*kmax+hit4]], posz[id[cluster_number*kmax+hit5]], r[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]], r[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]]);
                                            escatter5_geom = escatter4 / (1 + (escatter4 / mec2) * (1 - costheta));
                                            deltaesc5_geom = SQ(escatter5_geom) * ercos / mec2;
                                            deltaesc5 = (number_of_interactions_in_cluster + 5) * eresSQ;

                                            test5 = -SQ(escatter5_geom - escatter5) / (SQ(deltaesc5_geom) + deltaesc5);

                                            if (number_of_interactions_in_cluster > 6) {

                                              escatter6 = escatter5 - energy[id[cluster_number*kmax+hit5]];
                                              cross6_photo = sig_abs(escatter6);
                                              cross6_compt = sig_compt(escatter6);
                                              cross6_pair = sig_pair(escatter6);
                                              cross6_tot = cross6_photo + cross6_compt + cross6_pair;
                                              prob_for_interaction6 = cross6_photo / cross6_tot;
                                              prob_for_range6 = range_process(cross6_photo);

                                              for (hit6 = 0; hit6 < number_of_interactions_in_cluster; hit6++) {
#ifdef DEBUG
                                                printf ("hit6 = %d, ",hit6);
#endif
                                                if (hit6 != hit0 && hit6 != hit1 && hit6 != hit2 && hit6 != hit3 && hit6 != hit4 && hit6 != hit5) {
                                                  scalarproduct_456 = (posx[id[cluster_number*kmax+hit5]] - posx[id[cluster_number*kmax+hit4]]) * (posx[id[cluster_number*kmax+hit6]] - posx[id[cluster_number*kmax+hit5]])
                                                    + (posy[id[cluster_number*kmax+hit5]] - posy[id[cluster_number*kmax+hit4]]) * (posy[id[cluster_number*kmax+hit6]] - posy[id[cluster_number*kmax+hit5]])
                                                    + (posz[id[cluster_number*kmax+hit5]] - posz[id[cluster_number*kmax+hit4]]) * (posz[id[cluster_number*kmax+hit6]] - posz[id[cluster_number*kmax+hit5]]);
                                                  costheta = scalarproduct_456 / (r[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]] * r[id[cluster_number*kmax+hit5]*intmax+id[cluster_number*kmax+hit6]]);
                                                  ercos =
                                                    err_cos(posx[id[cluster_number*kmax+hit4]], posx[id[cluster_number*kmax+hit5]], posx[id[cluster_number*kmax+hit6]], posy[id[cluster_number*kmax+hit4]], posy[id[cluster_number*kmax+hit5]], posy[id[cluster_number*kmax+hit6]], posz[id[cluster_number*kmax+hit4]], posz[id[cluster_number*kmax+hit5]], posz[id[cluster_number*kmax+hit6]], r[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]],
                                                    r[id[cluster_number*kmax+hit5]*intmax+id[cluster_number*kmax+hit6]]);
                                                  escatter6_geom = escatter5 / (1 + (escatter5 / mec2) * (1 - costheta));
                                                  deltaesc6_geom = SQ(escatter6_geom) * ercos / mec2;
                                                  deltaesc6 = (number_of_interactions_in_cluster + 6) * eresSQ;

                                                  test6 = -SQ(escatter6_geom - escatter6) / (SQ(deltaesc6_geom) + deltaesc6);
                                                  prob6 = SQ(prob_for_interaction0) * prob_for_interaction1 * prob_for_interaction2 * 
                                                    prob_for_interaction3 * prob_for_interaction4 * prob_for_interaction5 * prob_for_interaction6 * 
                                                    exp (test1 +
                                                    (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                                                    (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1)+ test2 + 
                                                    (-r_ge[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]/prob_for_range2)+ test3 + 
                                                    (-r_ge[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]/prob_for_range3)+ test4 + 
                                                    (-r_ge[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]]/prob_for_range4)+ test5 + 
                                                    (-r_ge[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]]/prob_for_range5)+ test6 + 
                                                    (-r_ge[id[cluster_number*kmax+hit5]*intmax+id[cluster_number*kmax+hit6]]/prob_for_range6));
                                                  prob6 = pow(prob6, 1 / 13.);
                                                  if (prob6 > interaction_sequence_prob) {
                                                    interaction_sequence_prob = prob6;
                                                    ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                                                    ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                                                    ordering[cluster_number*kmax+2] = id[cluster_number*kmax+hit2];
                                                    ordering[cluster_number*kmax+3] = id[cluster_number*kmax+hit3];
                                                    ordering[cluster_number*kmax+4] = id[cluster_number*kmax+hit4];
                                                    ordering[cluster_number*kmax+5] = id[cluster_number*kmax+hit5];
                                                    ordering[cluster_number*kmax+6] = id[cluster_number*kmax+hit6];
                                                  }
                                                }
                                              }
                                            }
                                            else {	/* number_of_interactions_in_cluster == 6 */
                                              prob5 = SQ(prob_for_interaction0) * prob_for_interaction1 * prob_for_interaction2 * 
                                                prob_for_interaction3 * prob_for_interaction4 * prob_for_interaction5 * 
                                                exp (test1 +
                                                (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                                                (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1)+ test2 + 
                                                (-r_ge[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]/prob_for_range2)+ test3 + 
                                                (-r_ge[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]/prob_for_range3)+ test4 + 
                                                (-r_ge[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]]/prob_for_range4)+ test5 + 
                                                (-r_ge[id[cluster_number*kmax+hit4]*intmax+id[cluster_number*kmax+hit5]]/prob_for_range5));
                                              prob5 = pow(prob5, 1 / 11.);
                                              if (prob5 > interaction_sequence_prob) {
                                                interaction_sequence_prob = prob5;
                                                ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                                                ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                                                ordering[cluster_number*kmax+2] = id[cluster_number*kmax+hit2];
                                                ordering[cluster_number*kmax+3] = id[cluster_number*kmax+hit3];
                                                ordering[cluster_number*kmax+4] = id[cluster_number*kmax+hit4];
                                                ordering[cluster_number*kmax+5] = id[cluster_number*kmax+hit5];
                                              }
                                            }
                                          }
                                        }
                                      }
                                      else {	/* number_of_interactions_in_cluster == 5 */
                                        prob4 =  SQ(prob_for_interaction0) * prob_for_interaction1 * prob_for_interaction2 * 
                                          prob_for_interaction3 * prob_for_interaction4 * 
                                          exp(test1 +
                                          (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                                          (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1)+ test2 + 
                                          (-r_ge[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]/prob_for_range2)+ test3 + 
                                          (-r_ge[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]/prob_for_range3)+ test4 + 
                                          (-r_ge[id[cluster_number*kmax+hit3]*intmax+id[cluster_number*kmax+hit4]]/prob_for_range4));
                                        prob4 = pow(prob4, 1 / 9.);
                                        if (prob4 > interaction_sequence_prob) {
                                          interaction_sequence_prob = prob4;
                                          ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                                          ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                                          ordering[cluster_number*kmax+2] = id[cluster_number*kmax+hit2];
                                          ordering[cluster_number*kmax+3] = id[cluster_number*kmax+hit3];
                                          ordering[cluster_number*kmax+4] = id[cluster_number*kmax+hit4];
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                              else {	/* number_of_interactions_in_cluster == 4 */
                                prob3 =  SQ(prob_for_interaction0) * prob_for_interaction1 * prob_for_interaction2 * prob_for_interaction3 * 
                                  exp(test1 +
                                  (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                                  (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1)+ test2 + 
                                  (-r_ge[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]/prob_for_range2)+ test3 + 
                                  (-r_ge[id[cluster_number*kmax+hit2]*intmax+id[cluster_number*kmax+hit3]]/prob_for_range3));
                                prob3 = pow(prob3, 1 / 7.);
                                if (prob3 > interaction_sequence_prob) {
                                  interaction_sequence_prob = prob3;
                                  ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                                  ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                                  ordering[cluster_number*kmax+2] = id[cluster_number*kmax+hit2];
                                  ordering[cluster_number*kmax+3] = id[cluster_number*kmax+hit3];
                                }
                              }
                            }
                          }
                        }
                      }
                      else {	/* number_of_interactions_in_cluster == 3 */
                        prob2 = SQ(prob_for_interaction0)*prob_for_interaction1*prob_for_interaction2 * 
                          exp(test1 +
                          (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                          (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1)+ test2 + 
                          (-r_ge[id[cluster_number*kmax+hit1]*intmax+id[cluster_number*kmax+hit2]]/prob_for_range2));

                        prob2 = pow(prob2, 1 / 5.);

                        if (prob2 > interaction_sequence_prob) {
                          interaction_sequence_prob = prob2;
                          ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                          ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                          ordering[cluster_number*kmax+2] = id[cluster_number*kmax+hit2];
                        }
                      }
                    }
                  }
                }
              }
              else {	/* number_of_interactions_in_cluster == 2 */
                prob1 = SQ(prob_for_interaction0) * prob_for_interaction1 * 
                  exp(test1 +
                  (2 * (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit0]]/prob_for_range0)) +
                  (-r_ge[id[cluster_number*kmax+hit0]*intmax+id[cluster_number*kmax+hit1]]/prob_for_range1));
                prob1 = pow(prob1, 1 / 3.);

                if (prob1 > interaction_sequence_prob) {
                  interaction_sequence_prob = prob1;
                  ordering[cluster_number*kmax+0] = id[cluster_number*kmax+hit0];
                  ordering[cluster_number*kmax+1] = id[cluster_number*kmax+hit1];
                }
              }
            }
          }
        }
      }
    }
    probability[cluster_number] = interaction_sequence_prob;

  }
}


/*
* Rearrange les clusters par ordre decroissant de probabilite
* Il peut y avoir deux clusters avec les memes interractions
*/
void cluster_sort_flag(int number,
                       double ecluster[intmax],
                       int intnumber[intmax],
                       int id[intmax*kmax],
                       double clusterang[intmax],
                       double probability[intmax],
                       int ordering[intmax*kmax],
                       int clusterflag[intmax])
{
  int i, j, l, m, k, temp1[kmax];

  /* single interactions are awarded for the time being the minimum figure of merit */
  for (i = 0; i < number; i++) {
    for (j = i + 1; j < number; j++) {
      if (probability[i] < probability[j]) {
        swap(probability, i, j);
        swap(clusterang, i, j);
        swap(ecluster, i, j);
        swapi(clusterflag, i, j);
        for (l = 0; l < intnumber[i]; l++)
          temp1[l] = ordering[i*kmax+l];
        for (l = 0; l < intnumber[j]; l++)
          ordering[i*kmax+l] = ordering[j*kmax+l];
        for (l = 0; l < intnumber[i]; l++)
          ordering[j*kmax+l] = temp1[l];
        for (l = 0; l < intnumber[i]; l++)
          temp1[l] = id[i*kmax+l];
        for (l = 0; l < intnumber[j]; l++)
          id[i*kmax+l] = id[j*kmax+l];
        for (l = 0; l < intnumber[i]; l++)
          id[j*kmax+l] = temp1[l];
        swapi(intnumber, i, j);
      }
    }
  }

  /* flag clusters with lower figure of merit wich have common interactions with cluster with higher figure of merit */
  for (i = 0; i < number; i++) {
    if (clusterflag[i] == 0) {
      for (k = 0; k < intnumber[i]; k++) {
        for (l = i + 1; l < number; l++) {
          for (m = 0; m < intnumber[l]; m++) {
            if (id[i*kmax+k] == id[l*kmax+m]) {
              clusterflag[l] = 1;	/* Interaction deja trouve dans un cluster plus probable */
              break;
            }
          }
        }
      }
    }
  }
}


/*
* Probabilite pour les Interraction photoelectrique simple
*/
void single_interaction(int nbint,
                        double r[intmax*intmax],
                        double r_ge[intmax*intmax],
                        int number,
                        double ecluster[intmax],
                        int intnumber[intmax],
                        int id[intmax*kmax],
                        double probability[intmax],
                        int clusterflag[intmax])
{
  int l, m;
  double rmin, cross_photo, cross_compt, cross_pair, cross_tot, range, test;
  for (l = 0; l < number; l++) {
    if (intnumber[l] == 1 && clusterflag[l] == 0) {
      rmin = 100;
      if (nbint == 1)
        rmin = photodistmax + 1;
      for (m = 0; m < nbint; m++) {
        if (m != id[l*kmax+0]) {
          if (r[id[l*kmax+0]*intmax+m] < rmin)
            rmin = r[id[l*kmax+0]*intmax+m];
        }
      }
      if (rmin > photodistmax) {
        cross_photo = sig_abs(ecluster[l]);
        cross_compt = sig_compt(ecluster[l]);
        cross_pair = sig_pair(ecluster[l]);
        cross_tot = cross_photo + cross_compt + cross_pair;
        range = range_process(cross_tot);
        test = proba(range, r_ge[id[l*kmax+0]*intmax+id[l*kmax+0]]) * cross_photo / cross_tot;
        probability[l] = sqrt(test);
        /*    	printf("single interaction prob for cluster %d = %f \n",l,probability[l]); */
      }
    }
  }
}

/*
* Verifie les probabilites pour Compton et single.
* Retourne le nombre de photons (=clusters) valides (=trackes)
*/
int cluster_validation(double etot[intmax],	/* Energie corrige de l effet Dopler */
                       int nbtot[intmax],	/* Nb interraction dans cluster */
                       int number, 
                       double probability[intmax], 
                       int intnumber[intmax], 
                       int ordering[intmax*kmax], 
                       double ecluster[intmax], 
                       int clusterflag[intmax],
                       double x[intmax], 
                       double y[intmax], 
                       double z[intmax], 
                       double r[intmax*intmax], 
                       double xfirst[intmax],
                       double yfirst[intmax], 
                       double zfirst[intmax],
                       double xsecond[intmax],
                       double ysecond[intmax],
                       double zsecond[intmax])
{
  int j, i;
  double mprobsing = 1.,mprobtrack=1.;

  i = 0;
  for(j = 0; j < number; j++) {
    if(clusterflag[j] != 0)
      continue;
    if(getenv("MINPROBTRACK")) {
      mprobtrack = atof(getenv("MINPROBTRACK"));
    }  else {
      mprobtrack = minprobtrack;
    }

    if((intnumber[j] > 1) && (probability[j] >= mprobtrack)) {
      etot[i]    = ecluster[j];
      nbtot[i]   = intnumber[j];
      xfirst[i]  = x[ordering[j*kmax+0]];
      yfirst[i]  = y[ordering[j*kmax+0]];
      zfirst[i]  = z[ordering[j*kmax+0]];
      xsecond[i] = x[ordering[j*kmax+1]];
      ysecond[i] = y[ordering[j*kmax+1]];
      zsecond[i] = z[ordering[j*kmax+1]];
      i++;
    }
    if(getenv("MINPROBSING")) {
      mprobsing = atof(getenv("MINPROBSING"));
    }  else {
      mprobsing = minprobsing;
    }
    if((intnumber[j] == 1) && (probability[j] > mprobsing)) {
      etot[i]    = ecluster[j];
      nbtot[i]   = intnumber[j];
      xfirst[i]  = x[ordering[j*kmax+0]];
      yfirst[i]  = y[ordering[j*kmax+0]];
      zfirst[i]  = z[ordering[j*kmax+0]];
      xsecond[i] = 0;
      ysecond[i] = 0;
      zsecond[i] = 0;
      i++;
    }
  }
  return i;
}
