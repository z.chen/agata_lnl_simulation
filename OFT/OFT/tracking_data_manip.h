#ifndef DATA_MANIP_H
#define DATA_MANIP_H

#include"tracking_define.h"

void distances (double r[intmax],
                double r_ge[intmax],
                int number,
                double posx[intmax],
                double posy[intmax],
                double posz[intmax],
                double radius,
                double radius_out,
                double source[3]);

double err_cos (double xa,
                double xb,
                double xc,
                double ya,
                double yb,
                double yc,
                double za,
                double zb,
                double zc,
                double rab,
                double rbc);
#endif
