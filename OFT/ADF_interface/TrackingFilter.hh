#ifndef ADF_TRACKINGFILTER_H
#define ADF_TRACKINGFILTER_H

/* PSA-actor base class, J. Ljungvall 2008,
based on implementation by Olivier Stezowski*/

#ifdef _FromGRU_
#include <iostream>
#include <TSystem.h>
#include <GAcq.h>
#include <GNetServerRoot.h>
#include <GSpectra.h>
#include <TROOT.h>
#include <TApplication.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#endif

#include "AgataKeyFactory.h"
#include "AgataFrameFactory.h"
#include "NarvalInterface.h"
#include "PSAFrame.h"
#include "TrackedFrame.h"
#include "AgataCompositeFrame.h"
#include "Trigger.h"
#include "spectra.h"

#define WRITE_INPUT_HITS  // uncomment to write input hits in AgataMC/mgt format

//! Base class for Tracking filters
class TrackingFilter : public ADF::NarvalFilter 
{
private:
#ifdef _FromGRU_
  GNetServerRoot *NetworkRoot;
  GSpectra *SpectraDB;
  TH1I *HitDistrib;
  TH1F *TrackedEnergies;
  TH1F *CE_Tracking, *TrackedEnergiesCorr;
  TH3F *CC3D_BefAftRT;
#endif

protected:
  // General tracking structures
  typedef struct {
    double E;
    double X;
    double Y;
    double Z;
    int    Id;
    int    Sg;
  } exyzHit;
  typedef struct {
    double rXX;
    double rXY;
    double rXZ;
    double rYX;
    double rYY;
    double rYZ;
    double rZX;
    double rZY;
    double rZZ;
    double TrX;
    double TrY;
    double TrZ;
  } rotTr3D; 
  typedef struct {
    double E;
    double X1;
    double Y1;
    double Z1;
    double X2;
    double Y2;
    double Z2;
    int    nH;
  } trGamma;

  rotTr3D  *rotTr;

  exyzHit *pEXYZ;
  int number_of_hits;

  trGamma *pGams;
  int number_of_gammas;

  int number_of_psa_frames;
  int number_of_event_frames;
  int number_of_tracked_gammas;

  std::string conf_file;
  std::string event_file;
  FILE *fp_conf;
  FILE *fp_event;

  std::string hits_file;
  FILE *fp_hits;

  UInt_t trackCount;
  spec1D<int> *OftSpec_EE;

  double RecoilVc;    // recoil velocity v/c (0--1)
  double RecoilGm;    // 1/(1-RecoilVc**2)
  double RecoilCx;    // direction cosines of recoil vector
  double RecoilCy;
  double RecoilCz;
  double DopplerCorrect(trGamma *pg);

  void  GetDataPSA();

protected:
  //! to get frames that are needed for that algorithm
  //  ADF::NarvalIO *fNarvalIO; 

  //! input and ouput frames
  ADF::PSAFrame     *fFramePSA;
  ADF::TrackedFrame *fFrameTRACK;

  //! trigger frames
  ADF::AgataFrameTrigger fTrigger;
  ADF::AgataFrameTrigger fTriggerEvent;
  ADF::AgataFrameTrigger fTriggerPSA;

  UShort_t  crystal_id;
  UShort_t  crystal_status;
  Float_t   CoreE[2];
  UInt_t    evnumber; 
  ULong64_t timestamp;

  static std::string fConfPath;
  static bool fVerbose;

  void InitGenStructures();
  int  GetRotoTranslations(FILE *fp_conf);
  void Transform(exyzHit *pt, int nb_det) {    // transformation to global frame
    double Xr = pt->X; 
    double Yr = pt->Y;
    double Zr = pt->Z;
    rotTr3D *prt = rotTr + nb_det;
    pt->X = prt->rXX*Xr + prt->rXY*Yr + prt->rXZ*Zr + prt->TrX;
    pt->Y = prt->rYX*Xr + prt->rYY*Yr + prt->rYZ*Zr + prt->TrY;
    pt->Z = prt->rZX*Xr + prt->rZY*Yr + prt->rZZ*Zr + prt->TrZ; 
  }

  //!Here a pointer to whatever you have to initialize
  void *fDataContainer; 

  //!In here your open your data file etc;
  virtual Int_t InitDataContainer() {return 0;}
  //!Here you close data file etc.
  virtual Int_t ResetDataContainer(){return 0;}
  /** Key version numbers crystal frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMajorKey;
  /** Key version numbers crystal frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMinorKey;
  /** Frame version numbers crystal frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMajorFrame;
  /** Frame version numbers crystal frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMinorFrame;
  /** Key version numbers PSA frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMajorPSAKey;
  /** Key version numbers PSA frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMinorPSAKey;
  /** Frame version numbers PSA frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMajorPSAFrame;
  /** Frame version numbers PSA frame, can be set in configuration file, 
  or with time, using service frame
  */
  static Int_t fMinorPSAFrame;

public:
  /** To choose which Tracking algo */
  static std::string fWhichTrackingFilter;
  static std::string fWhichRotoTranslations;
  static std::string fOdirPrefix;
  static double fMGS000[3];
  static double fSource[3]; // x y z in mm
  static double fRecoil[4]; // cx, cy, cz, v/c
  TrackingFilter();
  virtual ~TrackingFilter(); 

  void WriteSpectra();

  //! to init your local variables with the ones from the buffers
  virtual Int_t SetInput();
  //! to copy the result of the algrithm into the frame through ADFObjects
  virtual Int_t SetOutput();
  //! Overload with yout own PSA algo.
  virtual Int_t Process();
  //! to init globals (static) from a directory
  static void process_config (const Char_t *, UInt_t *) ;

  //! Constructor implementation 	
  virtual void process_initialise (UInt_t *error_code);

  //! Destructor implementation  
  virtual void process_reset (UInt_t *error_code);

  //! To process just a block of data
  virtual void process_block( void   *input_buffer,  UInt_t   size_of_input_buffer,
                              void   *output_buffer, UInt_t   size_of_output_buffer,
                              UInt_t *used_size_of_output_buffer,
                              UInt_t *error_code);	

  virtual UInt_t ProcessBlock(ADF::FrameBlock &, ADF::FrameBlock &);

  virtual void process_start (UInt_t *error_code);
  virtual void process_stop  (UInt_t *error_code);
  virtual void process_pause (UInt_t *error_code);
  virtual void process_resume(UInt_t *error_code);

  const std::string &GetConfPath() { return fConfPath;}
};

#endif //ADF_TRACKINGFILTER_H
