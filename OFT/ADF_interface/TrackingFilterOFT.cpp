/** \file TrackingFilterOFT.cpp compiled in libADF.so
     !! only in standalone mode !! */

#include "TrackingFilterOFT.h"
#include "ada_parameters.h"

using namespace ADF;
using namespace std;

#ifdef _WIN32
extern "C" {
  void process_event(CntlStruct *mydata, unsigned int *error_code);
}
#else
  void process_event(CntlStruct *mydata, unsigned int *error_code);
#endif

TrackingFilterOFT::TrackingFilterOFT()
{	
  Log.GetProcessName() = "TrackingFilterOFT";
  fFrameIO.SetName("Tracking");

}

//TrackingFilterOFT::~TrackingFilterOFT() 
//{  
//  // in principle not needed ... just in case reset has not been called by narval
//  //UInt_t error = 0u;
//  //process_reset(&error) ;
//} 

Int_t TrackingFilterOFT::Process()
{
  unsigned int error_code;

  error_code = 0;

  FillOFTStructures();

  process_event(local_ptr, &error_code);

  MoveOFTStructures();

  return 0;
}

Int_t TrackingFilterOFT::InitDataContainer()
{
  local_ptr = (CntlStruct *) malloc( sizeof(CntlStruct) );

  local_ptr->etot = (double *)calloc(intmax, sizeof(double));
  local_ptr->xpos = (double *)calloc(intmax, sizeof(double));
  local_ptr->ypos = (double *)calloc(intmax, sizeof(double));
  local_ptr->zpos = (double *)calloc(intmax, sizeof(double));

  local_ptr->xfirst  = (double *)calloc(intmax, sizeof(double));
  local_ptr->yfirst  = (double *)calloc(intmax, sizeof(double));
  local_ptr->zfirst  = (double *)calloc(intmax, sizeof(double));

  local_ptr->xsecond = (double *)calloc(intmax, sizeof(double));
  local_ptr->ysecond = (double *)calloc(intmax, sizeof(double));
  local_ptr->zsecond = (double *)calloc(intmax, sizeof(double));

  local_ptr->probtot = (double *)calloc(intmax, sizeof(double));

  local_ptr->flagu = (int *)calloc(intmax, sizeof(int));
  local_ptr->nbtot = (int *)calloc(intmax, sizeof(int));

  local_ptr->e    = (double *)calloc(intmax, sizeof(double));
  local_ptr->et   = (double *)calloc(intmax, sizeof(double));

  local_ptr->r    = (double *)calloc(intmax*intmax, sizeof(double));
  local_ptr->r_ge = (double *)calloc(intmax*intmax, sizeof(double));

  local_ptr->sn          = (int *)calloc(kmax*intmax, sizeof(int));
  local_ptr->interaction = (int *)calloc(kmax*intmax, sizeof(int));

  local_ptr->seg = (int *)calloc(intmax, sizeof(int));
  local_ptr->det = (int *)calloc(intmax, sizeof(int));

  local_ptr->angtheta = (double *)calloc(intmax, sizeof(double));
  local_ptr->angphi   = (double *)calloc(intmax, sizeof(double));
  local_ptr->angn     = (double *)calloc(intmax, sizeof(double));

  local_ptr->numn = (int *)calloc(intmax, sizeof(int));

  local_ptr->source = (double *)calloc(3, sizeof(double));
  local_ptr->source[0] = fSource[0]/10.;
  local_ptr->source[1] = fSource[1]/10;
  local_ptr->source[2] = fSource[2]/10;

  local_ptr->inner_r = 23.5;  // inner radius of AGATA or AGATA demonstrator 
  local_ptr->outer_r = 32.9;  // outer radius of AGATA or AGATA demonstrator 
  local_ptr->mult   = 0;
  local_ptr->nb_int = 0;

  return 0;
}

void TrackingFilterOFT::FillOFTStructures()
{
  char tag1[] = "hit#";
  char tag2[] = "    ";
  char * tag = tag1;

  local_ptr->nb_int = number_of_hits;
  exyzHit * pt = pEXYZ;
  for(int nn = 0; nn < number_of_hits; nn++, pt++) {
    local_ptr->e[nn]    = pt->E * 0.001;  // Mev 
    local_ptr->xpos[nn] = pt->X * 0.1;    // cm 
    local_ptr->ypos[nn] = pt->Y * 0.1;    // cm
    local_ptr->zpos[nn] = pt->Z * 0.1;    // cm
    if(fVerbose) {
      if(nn==1) tag = tag2;
      printf("%s%3d %2d %2d  E x y z       = %7.1f %6.1f %6.1f %6.1f\n",
        tag, nn, pt->Id, pt->Sg, pt->E, pt->X, pt->Y, pt->Z);
    }
  }

#ifdef WRITE_INPUT_HITS
  if(fp_hits) {
    double etot = 0;
    for(int ii = 0; ii < number_of_hits; ii++)
      etot += pEXYZ[ii].E;
    fprintf(fp_hits, "-1 %.1f 0 0 0 0\n", etot);
    exyzHit * pt = pEXYZ;
    for(int nn = 0; nn < number_of_hits; nn++, pt++) {
      fprintf(fp_hits, "%d %.1f %.1f %.1f %.1f %d\n", pt->Id, pt->E, pt->X, pt->Y, pt->Z, pt->Sg);
    }
  }
#endif
}

void TrackingFilterOFT::MoveOFTStructures()
{
  char tag1[] = "gam#";
  char tag2[] = "    ";
  char * tag = tag1;
  number_of_gammas = local_ptr->mult;
  trGamma * pg = pGams;
  for(Int_t nn = 0; nn < number_of_gammas; nn++, pg++) {
    pg->E  = local_ptr->etot[nn]    * 1000.;  // keV
    pg->X1 = local_ptr->xfirst[nn]  * 10.;    // mm
    pg->Y1 = local_ptr->yfirst[nn]  * 10.;
    pg->Z1 = local_ptr->zfirst[nn]  * 10.;
    pg->X2 = local_ptr->xsecond[nn] * 10.;
    pg->Y2 = local_ptr->ysecond[nn] * 10.;
    pg->Z2 = local_ptr->zsecond[nn] * 10.;
    pg->nH = local_ptr->nbtot[nn];
    if(fVerbose) {
      if(nn==1) tag = tag2;
      printf("%s%3d        E x y z x y z = %7.1f %6.1f %6.1f %6.1f %6.1f %6.1f %6.1f\n",
        tag, nn, pg->E, pg->X1, pg->Y1, pg->Z1, pg->X2, pg->Y2, pg->Z2);
    }
    trackCount++;
  }
}

TrackingFilterOFT *process_register(UInt_t *error_code)
{
  //Int_t loc_error_code1,loc_error_code2,loc_error_code3,loc_error_code4,loc_error_code5;

  //ada_new_parameter("number_of_event_frames;integer_type;read_write;0",&loc_error_code1);
  //ada_new_parameter("number_of_psa_frames;integer_type;read_write;0",&loc_error_code2);
  //ada_new_parameter("input_number_of_hits;integer_type;read_write;0",&loc_error_code3);
  //ada_new_parameter("number_of_tracked_frames;integer_type;read_write;0",&loc_error_code4);
  //ada_new_parameter("output_number_of_gammas;integer_type;read_write;0",&loc_error_code5);
  //cout << "*********** added new parameters      "  << endl;

  return new TrackingFilterOFT();
}
