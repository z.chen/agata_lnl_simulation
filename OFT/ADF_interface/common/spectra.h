#ifndef SPECTRA_H_INCLUDED
#define SPECTRA_H_INCLUDED

// Classes to generate simple 1D- and 2D- spectra 
// Should be completed adding destructors, more controls and improving I/O
// Should also move part of the definitions to a cpp file

#include <iostream>
#include <string>

template <class T>
class spec1D
{
  std::string fname;
  int  numspec1;
  int  numspec2;
  int  lenspec;
  int  numtot;
  int  lentot;
  T   *data;
  int *count;
  int  counts;
public:
  spec1D(int len, int num1 = 1, int num2 = 1) {
    lenspec  = len;
    numspec1 = (num1>0) ? num1 : 1;
    numspec2 = (num2>0) ? num2 : 1;
    numtot   = numspec1*numspec2;
    lentot   = lenspec*numtot;
    data     = new T[lentot];
    count    = new int[numtot];
    reset();
  }
  spec1D(std::string fn, int len, int num1 = 1, int num2 = 1) {
    fname = fn;
    lenspec  = len;
    numspec1 = (num1>0) ? num1 : 1;
    numspec2 = (num2>0) ? num2 : 1;
    numtot   = numspec1*numspec2;
    lentot   = lenspec*numtot;
    data     = new T[lentot];
    count    = new int[numtot];
    reset();
  }
  void setFileName(std::string fn) {
    if(fn.length() > 0)
      fname = fn;
  }
  std::string getFileName() {
    return fname;
  }
  int getLenSpec() {
    return lenspec;
  }
  int getNumSpec1() {
    return numspec1;
  }
  int getNumSpec2() {
    return numspec2;
  }
  void reset() {
    int ii;
    for(ii=0; ii < lentot; ) data [ii++] = 0;
    for(ii=0; ii < numtot; ) count[ii++] = 0;
    counts = 0;
  }
  // off-range moved to borders
  void incr(int ii) {
    if(ii < 0)
      ii = 0;
    else if(ii >= lenspec)
      ii = lenspec-1;
    data [ii]++;
    count[ 0]++;
    counts++;
  }
  // off-range moved to borders
  void incr(int ii, int n1) {
    if(n1 < 0 || n1 >= numspec1)
      return;
    if(ii < 0)
      ii = 0;
    else if(ii >= lenspec)
      ii = lenspec-1;
    int jj = n1*lenspec  + ii;
    data [jj]++;
    count[n1]++;
    counts++;
  }
  // off-range moved to borders
  void incr(int ii, int n1, int n2) {
    if(n1 < 0 || n1 >= numspec1)
      return;
    if(n2 < 0 || n2 >= numspec2)
      return;
    if(ii < 0)
      ii = 0;
    else if(ii >= lenspec)
      ii = lenspec-1;
    int nn = n1*numspec2 + n2;
    int jj = nn*lenspec  + ii;
    data [jj]++;
    count[nn]++;
    counts++;
  }
  // off-range discarded
  void Incr(int ii) {
    if(ii < 0)
      return;
    else if(ii >= lenspec)
      return;
    data [ii]++;
    count[ 0]++;
    counts++;
  }
  // off-range discarded
  void Incr(int ii, int n1) {
    if(n1 < 0 || n1 >= numspec1)
      return;
    if(ii < 0)
      return;
    else if(ii >= lenspec)
      return;
    int jj = n1*lenspec  + ii;
    data [jj]++;
    count[n1]++;
    counts++;
  }
  // off-range discarded
  void Incr(int ii, int n1, int n2) {
    if(n1 < 0 || n1 >= numspec1)
      return;
    if(n2 < 0 || n2 >= numspec2)
      return;
    if(ii < 0)
      return;
    else if(ii >= lenspec)
      return;
    int nn = n1*numspec2 + n2;
    int jj = nn*lenspec  + ii;
    data [jj]++;
    count[nn]++;
    counts++;
  }
  T *getData(int n1 = 0, int n2 = 0) {
    if(n1 < 0 || n1 >= numspec1) n1 = 0;
    if(n2 < 0 || n2 >= numspec2) n2 = 0;
    int nn = n1*numspec2 + n2;
    return data + nn*lenspec;
  }
  int getCounts() {
    return counts;
  }
  int getCounts(int n1, int n2 = 0) {
    if(n1 < 0 || n1 > numspec1) n1 = 0;
    if(n2 < 0 || n2 > numspec2) n2 = 0;
    int nn = n1*numspec2 + n2;
    return count[nn];
  }
  bool write() {
    if(fname.length() < 1) {
      std::cout << "\nFile name not given" << std::endl;
      return false;
    }
    FILE *sFILE = fopen(fname.c_str(), "wb");
    if (sFILE==0) {
      std::cout << "\nBad fopen on " << fname << std::endl;
      return false;
    }
    bool stat = write(sFILE);
    fclose(sFILE);
    return stat;
 }
  bool write(const std::string fname) {
    FILE *sFILE = fopen(fname.c_str(), "wb");
    if (sFILE==0) {
      std::cout << "\nBad fopen on " << fname << std::endl;
      return false;
    }
    bool stat = write(sFILE);
    fclose(sFILE);
    return stat;
 }
  bool write(FILE *fp) {
    int ii = (int)fwrite(data, sizeof(T), lentot, fp);
    return (ii == lentot) ? true : false;
  }
  bool write(FILE *fp, int n1, int n2 = 0) {
    if(n1 < 0 || n1 >= numspec1) n1 = 0;
    if(n2 < 0 || n2 >= numspec2) n2 = 0;
    int nn = n1*numspec2 + n2;
    int ii = (int)fwrite(data + nn*lenspec, sizeof(T), lenspec, fp);
    return (ii == lenspec) ? true : false;
  }
};

// da completare per matrici multiple
template <class T>
class spec2D
{
  std::string fname;
  int   len1;
  int   len2;
  int   lentot;
  T    *data;
  int   counts;
public:
  spec2D(std::string fn, int l1, int l2) {
    fname = fn;
    len1   = l1;
    len2   = l2;
    lentot = len1*len2;
    data   = new T[lentot];
    reset();
  }
  spec2D(int l1, int l2) {
    len1   = l1;
    len2   = l2;
    lentot = len1*len2;
    data   = new T[lentot];
    reset();
  }
  void setFileName(std::string fn) {
    if(fn.length() > 0)
      fname = fn;
  }
  std::string getFileName() {
    return fname;
  }
  int getDim1() {
    return len1;
  }
  int getDim2() {
    return len2;
  }
  void reset() {
    int ii;
    for(ii=0; ii < lentot; ) data [ii++] = 0;
    counts = 0;
  }
  // off-range moved to borders
  void incr(int i1, int i2) {
    if(i1 < 0)
      i1 = 0;
    else if(i1 >= len1)
      i1 = len1-1;
    if(i2 < 0)
      i2 = 0;
    else if(i2 >= len2)
      i2 = len2-1;
    int ii = i1*len2 + i2;
    data[ii]++;
    counts++;
  }
  // off-range discarded
  void Incr(int i1, int i2) {
    if(i1 < 0)
      return;
    else if(i1 >= len1)
      return;
    if(i2 < 0)
      return;
    else if(i2 >= len2)
      return;
    int ii = i1*len2 + i2;
    data[ii]++;
    counts++;
  }
  T *getData() {
    return data;
  }
  int getCounts() {
    return counts;
  }
  bool write() {
    if(fname.length() < 1) {
      std::cout << "\nFile name not given" << std::endl;
      return false;
    }
    FILE *sFILE = fopen(fname.c_str(), "wb");
    if (sFILE==0) {
      std::cout << "\nBad fopen on " << fname << std::endl;
      return false;
    }
    bool stat = write(sFILE);
    fclose(sFILE);
    return stat;
  }
  bool write(const std::string fname) {
    FILE *sFILE = fopen(fname.c_str(), "wb");
    if (sFILE==0) {
      std::cout << "\nBad fopen on " << fname << std::endl;
      return false;
    }
    bool stat = write(sFILE);
    fclose(sFILE);
    return stat;
  }
  bool write(FILE *fp) {
    int ii = (int)fwrite(data, sizeof(T), lentot, fp);
    return (ii == lentot) ? true : false;
  }
};
#endif // SPECTRA_H_INCLUDED
