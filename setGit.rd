#if this is an empty folder:

git init #always init first before download anything
git pull git@git.gsi.de:z.chen/anatraces.git #before this step, make sure you have added you ssh-key to this git account
git config pull.rebase true ##if the folder is not empty, you need to rebase the folder

#if this is your first time, before submit your updated files
git add .
git commit -m YOUR_COMMENTS
git remote add origin git@git.gsi.de:z.chen/anatraces.git
git branch -M main
git push -uf origin main

#if it's not the first time to upload files in this folder'
git add .
git commit -m YOUR_COMMENTS
git push
