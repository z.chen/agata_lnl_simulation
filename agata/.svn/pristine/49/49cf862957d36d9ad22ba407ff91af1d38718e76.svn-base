//*************************************************************
//-------AGATA+LYCCA-0 SIMULATION (EVENT GENERATOR)------------
//*************************************************************
#include <fstream>
#include "Riostream.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <string>
#include "TRandom3.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include <time.h>

using namespace std;
//*************************************************************
//-----------------------USAGE---------------------------------
//*************************************************************
// root[0] : .L MERGE.cpp++
// root[1] : MERGE(3,10000,100000,20,"merged.events")
//*************************************************************
//-------------------------------------------------------------
//*************************************************************
Int_t MERGE(Int_t background,Int_t nIons,Int_t abkgmult,Int_t highemult,Char_t outfilename[]){
//*************************************************************
//-----------------------INPUT FILES---------------------------
//*************************************************************
  Char_t mocadi_file[]="/mnt/hgfs/Echanges/MOCADI/4GEANT4/DanEvtGene/output/mocadi_templatenoback.events"; //"/PATH TO...";//path to mocadi outgoing ions
  Char_t abkg_file[]="abkg/output/abkg.events"; //"/PATH TO ...";//path to sampled abkg file
  Char_t dweiko_file[]="dweiko_gfortran/output/dweiko_SampledDummy.out";//"/PATH TO ..."; //path to sampled ang. distribution from dweiko_gam.out
//*************************************************************
//-------------------------------------------------------------
//*************************************************************

//*************************************************************
//-----------------BACKGROUND FLAG SETTINGS--------------------
//*********************(Int_t background)**********************
//1 for abkg only
//2 for high E only
//3 for both
//4 for gamma-rays of interest only
//5 for gamma-rays of interest + abkg
//6 for gamma-rays of interest + high E
//*************************************************************
//-------------------------------------------------------------
//*************************************************************
Int_t A, line, dummy1, dummy2,  numen;
Float_t x, xprime, y, yprime, ener, t, amu, Z, dummy3, dummy4, dummy5, TOF, dummy6, a, b, c, vect_len, dx, dy, dz, dummy7;
Double_t hEdx=0., hEdy=0., hEdz=0.;
Double_t Eabkg=0., theta=0., beta=0., lab=0.,lab_en=0., pi=3.14159265359;
Double_t phi=0., xdx=0., xdy=0., xdz=0., energy=0., coul_theta=0., coul_phi=0. , coul_dx=0., coul_dy=0., coul_dz=0.,hEtheta=0.,hEphi=0.;
string gam, moc, first;
Double_t highE[21];

ifstream mocadi(mocadi_file);
ifstream abkg(abkg_file);
ifstream dweiko(dweiko_file);
TRandom3 r(0);
TRandom3 r1(0);
TRandom3 r2(0);
TRandom3 gf(0);

if(!mocadi){
	cout << "Error! Cannot find Mocadi outgoing ions file: " << mocadi_file << endl;
	return(1);
}

if(!abkg){
	cout << "Error! Cannot find sampled abkg file: " << abkg_file << endl;
	return(1);
}

if(!dweiko){
	cout << "Error! Cannot find sampled dweiko file: " << dweiko_file << endl;
	return(1);
}

ofstream agata(outfilename);   //merged file for agata input
ofstream info("recon.info"); //mocadi info file needed for event reconstruction
//*************************************************************
//---------------WRITE MERGED INPUT FILE HEADER----------------
//*************************************************************
agata << "FORMAT 0 2" << endl;
agata << "#" << endl;
agata << "REACTION 1 1 6 12 0.0" << endl;
agata << "#" << endl;
agata << "EMITTED 2 1 8" << endl;
agata << "#" << endl;
//*************************************************************
//-------------------------------------------------------------
//*************************************************************

//*************************************************************
//------------------ROOT TREE BRANCHES-------------------------
//*************************************************************
TFile *f1 = new TFile("MERGE.root","RECREATE");
TTree *t1 = new TTree("t1","MERGED INPUT FILE:AGATA SIM.");
t1->Branch("coul_theta",&coul_theta,"coul_theta/D");
t1->Branch("coul_phi",&coul_phi,"coul_phi/D");
t1->Branch("coul_dx",&coul_dx,"coul_dx/D");
t1->Branch("coul_dy",&coul_dy,"coul_dy/D");
t1->Branch("coul_dz",&coul_dz,"coul_dz/D");
t1->Branch("beta",&beta,"beta/D");
t1->Branch("lab_en",&lab_en,"lab_en/D");
t1->Branch("abkg_theta",&theta,"abkg_theta/D");
t1->Branch("abkg_en",&Eabkg,"abkg_en/D");
t1->Branch("abkg_phi",&phi,"abkg_phi/D");
t1->Branch("abkg_dx",&xdx,"abkg_dx/D");
t1->Branch("abkg_dy",&xdy,"abkg_dy/D");
t1->Branch("abkg_dz",&xdz,"abkg_dz/D");
t1->Branch("high_theta",&hEtheta,"high_theta/D");
t1->Branch("high_phi",&hEphi,"high_phi/D");
t1->Branch("high_numen",&numen,"high_numen/I");
t1->Branch("high_en",&highE[numen],"high_en/D");
t1->Branch("high_dx",&hEdx,"high_dx/D");
t1->Branch("high_dy",&hEdy,"high_dy/D");
t1->Branch("high_dz",&hEdz,"high_dz/D");
//*************************************************************
//-------------------------------------------------------------
//*************************************************************

//*************************************************************
//---------------------MAIN LOOP-------------------------------
//*************************************************************
for(Int_t i=0;i<nIons;i++){ 
//	cout << "Events processed: " << i << " " << endl;
	getline(mocadi,moc);
	line++;
	stringstream mc(moc);
	mc >> dummy1 >> dummy2 >> x >> xprime >> y >> yprime >> ener >> t >> amu >> Z >> dummy3 >> dummy4 >> dummy5 >> TOF >> dummy6 >> dummy7;

	info << " " << TOF*1000 << " " << dummy6 << " " << x << " " << y << endl;

	A=amu+0.5;	
	a=tan(xprime*0.001); 
	b=tan(yprime*0.001); 	
	c=1.;
	vect_len=sqrt(a*a+b*b+c*c);
	dx=a/vect_len;
	dy=b/vect_len;
	dz=c/vect_len;

	agata << "$" << endl;
	agata << "-101 " << Z << " " << A << " " << ener*A << " " << dx << " " << dy << " " << dz << " " << x << " " << y << " " << "0." << endl;
//*************************************************************
//---------------Heavy Ion             ------------------------
//*************************************************************
//	agata << " 8" << " " <<  ener*A << " " << dx << " " << dy << " " << dz << endl;


//*************************************************************
//---------------GAMMA-RAYS OF INTEREST------------------------
//*************************************************************
//If more transitions are required, add additional code similar to below >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	energy=1000.; //keV

//	dweiko >> coul_theta;
    coul_theta=acos(r1.Uniform(-1,1)) *180./pi;  // replace dweiko >> coul_theta; untill DWEIKO is being checked !!

	coul_phi=r2.Uniform(0,360);

	coul_dx=sin(coul_theta*(pi/180.))*cos(coul_phi*(pi/180.));
	coul_dy=sin(coul_theta*(pi/180.))*sin(coul_phi*(pi/180.));
	coul_dz=cos(coul_theta*(pi/180.));
	
	beta=sqrt(pow(ener,2)+1863*(ener))/(931.5+(ener));
	lab=(((energy*1e3*1.602176487e-19)*sqrt(1-(beta*beta)))/(1-(beta*cos(coul_theta*(pi/180.)))));
	lab_en=lab/(1.602176487e-19*1000);
		if(background==3||background==4||background==5||background==6){
			agata << " 1" << " " <<  lab_en << " " << coul_dx << " " << coul_dy << " " << coul_dz << endl;
		}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

//*************************************************************
//--------------------ATOMIC BACKGROUND------------------------
//*************************************************************
		if(background==1||background==3||background==5){
			for(Int_t ii=0;ii<abkgmult;ii++){
				abkg >>  theta >> Eabkg ;
				phi=r2.Uniform(0,360);
				xdx=sin(theta*(pi/180.)*cos(phi*(pi/180.)));
				xdy=sin(theta*(pi/180.)*sin(phi*(pi/180.)));
				xdz=cos(theta*(pi/180.));
				agata << " 1 " << Eabkg << " " << xdx << " " << xdy << " " << xdz << endl;
			}
		}
//*************************************************************
//-------------------------------------------------------------
//*************************************************************

//*************************************************************
//----------------HIGH ENERGY BACKGROUND-----------------------
//*************************************************************
//		if(background==2||background==3||background==6){
		if(background==6){
			for(Int_t iii=0;iii<highemult;iii++){
				 highE[0] = abs(gf.Gaus(1000,425.532));
				 highE[1] = abs(gf.Gaus(1250,531.915));
 				 highE[2] = abs(gf.Gaus(1500,638.298));
				 highE[3] = abs(gf.Gaus(1750,744.681));
				 highE[4] = abs(gf.Gaus(2000,851.064)); 
				 highE[5] = abs(gf.Gaus(2250,957.447));
				 highE[6] = abs(gf.Gaus(2500,1063.83));
				 highE[7] = abs(gf.Gaus(2750,1170.21));
				 highE[8] = abs(gf.Gaus(3000,1276.60)); 
				 highE[9] = abs(gf.Gaus(3250,1382.98));
				highE[10] = abs(gf.Gaus(3500,1489.36));
				highE[11] = abs(gf.Gaus(3750,1595.74));
				highE[12] = abs(gf.Gaus(4000,1702.13));
				highE[13] = abs(gf.Gaus(4250,1808.51));
				highE[14] = abs(gf.Gaus(4500,1914.89));
				highE[15] = abs(gf.Gaus(4750,2021.28));
				highE[16] = abs(gf.Gaus(5000,2127.66));
				highE[17] = abs(gf.Gaus(5250,2234.04));
				highE[18] = abs(gf.Gaus(5500,2340.43));
				highE[19] = abs(gf.Gaus(5750,2446.81));
				highE[20] = abs(gf.Gaus(6000,2553.19));

				hEtheta=r2.Uniform(0,180);
				hEphi=r2.Uniform(0,360);
				hEdx=sin(hEtheta*(pi/180.))*cos(hEphi*(pi/180.));
				hEdy=sin(hEtheta*(pi/180.))*sin(hEphi*(pi/180.));
				hEdz=cos(hEtheta*(pi/180.));

				numen=r2.Uniform(0,20);
				agata << " 1" << " " << highE[numen] << " " << hEdx << " " << hEdy << " " << hEdz << endl;
			}
		}
//*************************************************************
//-------------------------------------------------------------
//*************************************************************
t1->Fill();
}
//*************************************************************
//-------------------------------------------------------------
//*************************************************************
mocadi.close();
agata.close();
abkg.close();
info.close();
dweiko.close();
t1->Write();
return 0;
}
