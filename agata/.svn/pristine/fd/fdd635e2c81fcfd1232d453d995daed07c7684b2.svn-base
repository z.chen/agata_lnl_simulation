#ifdef ANCIL
#ifndef DiamantDetectorConstruction_h
#define DiamantDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4Material.hh"
#include "G4ThreeVector.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;


class AgataAncillaryDiamantMessenger;


class AgataAncillaryDiamant : public AgataAncillaryScheme
{
public:
  AgataAncillaryDiamant(G4String,G4String);
  ~AgataAncillaryDiamant();

private:
  G4bool useAbsorbers;
  G4bool useDelrin;

public:
  G4int FindMaterials();
  void GetDetectorConstruction();
  void InitSensitiveDetector();
  void Placement();
  void ShowStatus();
  void WriteHeader (std::ofstream &outFileLMD, G4double=1.0*mm);

public:
  G4int GetSegmentNumber(G4int,G4int,G4ThreeVector);
  inline G4int GetCrystalType(G4int){return -1;};

public:
  void SetUseAbsorbers    ( G4bool );
  void SetUseDelrin       ( G4bool );

private:
  void PlaceQuads();
  void PlaceBackwardTriangles();
  void PlaceForwardTriangles();
  void PlaceForwardWall();
  void PlaceGeCrystals();

  void PlaceFWAbsorbers();
  void PlaceQuadsAbsorbers();
  void PlaceBackwardTrianglesAbsorbers();
  void PlaceForwardTrianglesAbsorbers();

  void PlaceFWDelrin();
  void PlaceQuadsDelrin();
  void PlaceForwardTrianglesDelrin();
  void PlaceBackwardTrianglesDelrin();
  
  AgataAncillaryDiamantMessenger* myMessenger;

private:
  G4String     nameCsI;
  G4Material*  matCsI;
  G4String     nameTa;
  G4Material*  matTa;
  G4String     nameDelrin;
  G4Material*  matDelrin;
  
  G4Trap*            DiamantElement_sd;
  G4LogicalVolume*   DiamantElement_log;

  G4Box*             DiamantBox;
  G4LogicalVolume*   DiamantBox_log;
  G4VPhysicalVolume*  DiamantBox_phys;
  
private:
  G4ThreeVector CentresOfSolids[76];
  G4RotationMatrix DiaRotMatrix[76];
  G4String name[80];

  G4VPhysicalVolume* DiamantElement_phys[80];

  G4double FWFoilThickness;
  G4double AbsorbersOn45degThickness;
  G4double AbsorbersOn90degThickness;
  G4double AbsorbersOn135degThickness;
  G4double FoilDistanceToCsI;

  G4double DelrinThickness;
  G4double DelrinTrianglesThickness;
  G4double DelrinDistanceToCsI;

};

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
 
class AgataAncillaryDiamantMessenger: public G4UImessenger
{
public:
  AgataAncillaryDiamantMessenger(AgataAncillaryDiamant*,G4String);
  ~AgataAncillaryDiamantMessenger();

private:
  AgataAncillaryDiamant*       myTarget;
  G4UIdirectory*            myDirectory;

  G4UIcmdWithABool*   enableAbsorbers;
  G4UIcmdWithABool*   disableAbsorbers;
  G4UIcmdWithABool*   enableDelrin;
  G4UIcmdWithABool*   disableDelrin;



public:
  void SetNewValue(G4UIcommand * command,G4String newValues);
};

#endif
#endif
