#ifdef ANCIL
#include "AgataAncillaryBrick.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryBrick::AgataAncillaryBrick(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  dirName     = name;
  
  matShell    = NULL;
  matName     = "Steel";
  
  ancSD       = NULL;
  ancName     = G4String("BRICK");
  ancOffset   = 6000;
  
  numAncSd = 0;
}

AgataAncillaryBrick::~AgataAncillaryBrick()
{}

G4int AgataAncillaryBrick::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryBrick::InitSensitiveDetector()
{}


void AgataAncillaryBrick::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryBrick::Placement()
{
  ///////////////////////
  /// construct a shell
  ///////////////////////
  G4double size_x = 40.*cm;
  G4double size_y = 40.*cm;
  G4double size_z = 10.*cm;
  
  G4Box        *solidShell = new G4Box( "brick", size_x, size_y, size_z );
  G4LogicalVolume *logicShell = new G4LogicalVolume( solidShell, matShell, "Brick", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(0., 0., size_x), "Shell", logicShell, theDetector->HallPhys(), false, 0 );

  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  logicShell->SetVisAttributes( pVA );

  return;
}


void AgataAncillaryBrick::ShowStatus()
{
  G4cout << " ANCILLARY BRICK has been constructed." << G4endl;
  G4cout << " Ancillary brick material is " << matShell->GetName() << G4endl;
}

void AgataAncillaryBrick::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
