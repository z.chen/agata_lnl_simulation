////////////////////////////////////////////////////
/////////// GEOMETRY: AGATA ////////////////////////
////////////////////////////////////////////////////

// TEMPORARILY TREATED LIKE A SHELL (MOSTLY)

#include "mgt.h"

double  rsh_int;              // inner radius of shell
double  rsh_int2;             // inner radius of shell squared
double  rsh_ext;              // outer radius of shell
double  rsh_ext2;             // outer radius of shell squared
double  rsh_thick;            // thickness    of shell

double  shell_dd, shell_dp, shell_d1;   // distanza12^2, p1*dir, p1^2 (A, B, C+rr^2 dell'equazione dell'intercetta)
double  shell_t1, shell_t2;             // le due soluzioni dell'equazione di secondo grado

int     nshapes;              // number of different crystals
int     nsegsZ;               // number of segments along  detector axis
int     nsegsP;               // number of segments around detector axis
int     nsegsR;               // number of segments along  detector radius
int     detposOK = 0;         // to know if initialized from file
int     segposOK = 0;         //      "

void    initdetcenter_agata  ();
void    initsegcenter_agata  ();
int     insidegedet_agata    (point *);
void    findsegment_agata    (point *);
void    setsegcenter_agata   (point *);
double  gedistance_agata     (point *, point *);
int     hitdet_agata         (double);

void
geominit_agata()
{
  char  line[256];
  char *token;
  double dim1, dim2;
  int ii, nn, ns, nsp, nsz;

  insidegedet  = insidegedet_agata;
  findsegment  = findsegment_agata;
  setsegcenter = setsegcenter_agata;
  gedistance   = gedistance_agata;
  
  // to get rid of previous cr/lf
  do {
    if (!fgets(line, 255, ifp)) {
      printf("Error reading %s \n", ifname);
      errexit("geominit_agata");
    }
  }
  while (line[0] < ' ');
  
  nn = strlen(line);
  if(line[nn-1] == 10) line[nn-1] = 0;
  printf("%s\n", line);
  
  token = strtok(line, " ");

  nn = strlen(token);
  for(ii = 0; ii < nn; ii++)
    token[ii] = toupper(token[ii]);

  if(strcmp(token, "SUMMARY")) {
    printf("Error reading %s\n", ifname);
    printf("Keyword SUMMARY expected\n");
    errexit("geominit_agata");
  }

  token = strtok(NULL, " ");
  if(!token) {
    printf("Error reading summary description of AGATA geometry\n");
    errexit("geominit_agata");
  }
  dim1 = atof(token)*distfactor;
  token = strtok(NULL, " ");
  if(!token) {
    printf("Error reading summary description of AGATA geometry\n");
    errexit("geominit_agata");
  }
  dim2 = atof(token)*distfactor;

  rsh_int   = dim1 - 1.;      // safety 1 mm
  rsh_ext   = dim2 + 1.;
  rsh_int2  = rsh_int * rsh_int;
  rsh_ext2  = rsh_ext * rsh_ext;
  rsh_thick = rsh_ext - rsh_int;

  token = strtok(NULL, " ");
  if(!token) {
    printf("Error reading summary description of AGATA geometry\n");
    errexit("geominit_agata");
  }
  ngedets = atoi(token);



  // To deal with double clusters:
  int nATC, nADC, nASC;

  printf("\nThere is %i crystals in total, but how many double cluster ? ", ngedets);
  printf("\nPlease indicate the number of Double Clusters: ");
  scanf("%i", &nADC);
  printf("\nPlease indicate the number of Cluster with 1 crystal: ");
  scanf("%i", &nASC);

  nATC=(ngedets-2*nADC)/3;
  nATC=(ngedets-2*nADC-nASC)/3;


  if(nASC==0)
    {
      if((ngedets-2*nADC)%3){
	printf("\n Attention, the entered ADC number doesn't match any combination of (ATC*3 +ADC*2) to reach %i \n\n", ngedets);
    
	//exit(1);
    
      }else
	{
	  ngedets=3*(nATC+nADC); // resetting  ngedets to ensure that mgt does not miss any crystals with hits.
	  // ngedet is the maximum number of crystals if all ATC cluster are complete
	  printf("\n Reset crystal numbers to: %i to make sure mgt doesn't miss any crystals with hits \n\n", ngedets);
	}
    }
  else{

      if((ngedets-2*nADC-nASC)%3){
	printf("\n Attention, the entered ADC number doesn't match any combination of (ATC*3 +ADC*2) to reach %i \n\n", ngedets);
    
	//exit(1);
    
      }else
	{
	  ngedets=3*(nATC+nADC+nASC); // resetting  ngedets to ensure that mgt does not miss any crystals with hits.
	  printf("\n Reset crystal numbers to: %i to make sure mgt doesn't miss any crystals with hits \n\n", ngedets);
	}

  }


  token = strtok(NULL, " ");
  if(!token) {
    printf("Error reading summary description of AGATA geometry\n");
    errexit("geominit_agata");
  }
  nshapes = atoi(token);

  for(ns = 0; ns < nshapes; ns++) {
    token = strtok(NULL, " ");
    if(!token) {
      printf("Error reading description description of segments\n");
      errexit("geominit_agata");
    }
    nsp = atoi(token);
    token = strtok(NULL, " ");
    if(!token) {
      printf("Error reading description description of segments\n");
      errexit("geominit_agata");
    }
    nsz = atoi(token);
    nsegsP = MAX(nsegsP, nsp);
    nsegsZ = MAX(nsegsZ, nsz);
  }
  nsegsR  = 1;
  nsegdet = nsegsZ * nsegsP * nsegsR;
  nsegtot = nsegdet * ngedets;
  
  if(nsegdet > 1) {
    if(nsegsR <2)
      sprintf(segmentstring, "(Phi=%d Z=%d)", nsegsP, nsegsZ);
    else
      sprintf(segmentstring, "(Phi=%d Z=%d R=%d)", nsegsP, nsegsZ, nsegsR);
  }

  // allocate structures for geometrical centers of detectors and segments
  CDet = (coors *)calloc(ngedets, sizeof(coors));
  CSeg = (coors *)calloc(nsegtot, sizeof(coors));

  GEOMETRY   = AGATA;

  while(1) {
    if (!fgets(line, 255, ifp)) {
      printf("Error reading %s while skipping AGATA geometry data\n", ifname);
      errexit("geominit_agata");
    }
    nn = strlen(line);
    if(line[nn-1] == 10) line[nn-1] = 0;
    if(!strcmp(line, "POSITION_CRYSTALS")) {
      initdetcenter_agata();
      detposOK = 1;
    }
    if(!strcmp(line, "POSITION_SEGMENTS")) {
      initsegcenter_agata();
      segposOK = 1;
    }
    else if(!strcmp(line, "ENDGEOMETRY")) {
      if(!detposOK)
        printf("detectors positions not found in %s\n", ifname);
      if(!segposOK) {
        printf("segments  positions not found in %s\n", ifname);
#if SEGCENTER==1 && SEGPACK==1
        printf("PACKED POINTS CANNOT BE MOVED TO SEGMENTS CENTERS !!!!\n");
#endif
      }
      return;
    }
  }
}

void
initdetcenter_agata()
{
  char   line[100];
  int    nn, ii, detector;
  double xx, yy, zz;

  printf("Reading detectors centres from %s\n", ifname);

  for(nn = 0; nn < ngedets; nn++)
    CDet[nn].nd = CDet[nn].ns = -1;  // mark all as invalid

  while(1) {
    nn = fscanf(ifp, "%s", line);
    if(nn != 1) {
      printf("error reading detectors centres from %s\n", ifname);
      errexit("initdetcenter_agata");
    }
    if(!strcmp(line, "ENDPOSITION_CRYSTALS")) {
      return;
    }
    detector = atoi(line);
    nn = fscanf(ifp, "%d %lf %lf %lf", &ii, &xx, &yy, &zz);
    xx *= distfactor;
    yy *= distfactor;
    zz *= distfactor;
    if(nn != 4 || detector < 0 || detector >= ngedets || ii != 0) {
      printf("error reading detectors centres from %s\n", ifname);
      errexit("initdetcenter_agata");
    }
    CDet[detector].nd = detector;
    CDet[detector].ns = 0;
    CDet[detector].xx = xx;
    CDet[detector].yy = yy;
    CDet[detector].zz = zz;
    nn = fscanf(ifp, "%d %f %f %f", &ii, &xx, &yy, &zz);
    nn = fscanf(ifp, "%d %f %f %f", &ii, &xx, &yy, &zz);
    nn = fscanf(ifp, "%d %f %f %f", &ii, &xx, &yy, &zz);
  }

  for(nn = 0; nn < ngedets; nn++) {
    if(CDet[nn].nd < 0 || CDet[nn].ns < 0) {
      printf("ununitialized detector centre %d\n", nn);
      errexit("initdetcenter_agata");
    }
  }

}

void
initsegcenter_agata()
{
  char   line[100];
  int    nn, detector, sector, slice;
  double xx, yy, zz, vol;

  printf("Reading segments  centres from %s\n", ifname);

  for(nn = 0; nn < nsegtot; nn++) CSeg[nn].nd = CSeg[nn].ns = -1;  // mark all as invalid

  while(1) {
    nn = fscanf(ifp, "%s", line);
    if(nn != 1) {
      printf("error reading segments  centres from %s\n", ifname);
      errexit("initsegcenter_agata");
    }
    if(!strcmp(line, "ENDPOSITION_SEGMENTS")) {
      return;
    }
    detector = atoi(line);
    nn = fscanf(ifp, "%d %d %lf %lf %lf %lf", &sector, &slice, &xx, &yy, &zz, &vol);
    xx *= distfactor;
    yy *= distfactor;
    zz *= distfactor;
    if(nn != 6) {
      printf("error reading segments  centres from %s\n", ifname);
      errexit("initsegcenter_agata");
    }
    nn = detector*nsegdet + sector*nsegsP + slice;
    CSeg[nn].nd = detector;
    CSeg[nn].ns = sector*nsegsP + slice;
    CSeg[nn].xx = xx;
    CSeg[nn].yy = yy;
    CSeg[nn].zz = zz;
  }

  for(nn = 0; nn < nsegtot; nn++) {
    if(CSeg[nn].nd < 0 || CSeg[nn].ns < 0) {
      printf("ununitialized segment centre %d\n", nn);
      errexit("initsegcenter_agata");
    }
  }

}

int
insidegedet_agata(point *pp)
{
  return (pp->rr >= rsh_int) && (pp->rr <= rsh_ext);
}

void
findsegment_agata(point *pp)
{
  pp->seg = pp->ns + nsegdet * pp->nd;

#if STATINSIDESEG == 1
  specIncr(pp->nd         , 19, 0);    // detector number
  specIncr(pp->ns%6 + 1000, 19, 0);    // phi-sector
  specIncr(pp->ns/6 + 1500, 19, 0);    // z-slice
#endif

  return;
}

void
setsegcenter_agata(point *pp)
{
  double dist = 0.;
  coors *csg;

  if(segposOK) {
    csg = CSeg + pp->seg;
#if STATINSIDESEG == 1
    dist = sqrt(pow(pp->xx - csg->xx, 2.) + pow(pp->yy - csg->yy, 2.)+pow(pp->zz - csg->zz, 2.));
    specIncr((int)dist+3000, 19, 0);
#endif
    pp->xx = csg->xx;
    pp->yy = csg->yy;
    pp->zz = csg->zz;
    setcosdir(pp);
  }
  return;
}

double
gedistance_agata(point *p1, point *p2)
{
  point *pt;
  int nn;
  double dx, dy, dz;   // numeri direttori 1->2
  double d12, dist;

  if(p1->rr > p2->rr)
    SWAP(p1, p2, pt)

// casi particolari
  if(p1->rr <= rsh_int) {
    if(p2->rr <= rsh_int) 
      return 0.0;         // (p1, p2) < Rint
    if(p1->rr == 0.0) {   // p1 al centro della shell
      if(p2->rr < rsh_ext) return p2->rr - rsh_int;
      return rsh_ext - rsh_int;
    }
  }

  dx = p2->xx - p1->xx;                   // i numeri direttori della retta da p1 a p2
  dy = p2->yy - p1->yy;
  dz = p2->zz - p1->zz;

  shell_dd = dx*dx + dy*dy + dz*dz;       // distanza tra i due punti
  if(shell_dd == 0.) return 0;
  d12 = sqrt(shell_dd);

  shell_dp = p1->xx*dx     + p1->yy*dy     + p1->zz*dz;
  shell_d1 = p1->xx*p1->xx + p1->yy*p1->yy + p1->zz*p1->zz;

// il primo punto e' nello spazio vuoto interno
  if(p1->rr <= rsh_int) {
    dist = 1.;    // distanza normalizzata da cui sottrarre i pezzi in vuoto
    if(p2->rr >= rsh_ext) {     // il secondo punto e' esterno
      nn = hitdet_agata(rsh_ext);
      if(nn != 1) {
        printf("Does not hit outer surface (1)\n");
        errexit("getdistance_agata");
      }
      dist -= shell_t1;
    }
    nn = hitdet_agata(rsh_int);
    if(nn != 1) {
        printf("Does not hit inner surface (1)\n");
        errexit("getdistance_agata");
//    return 0.;     // da mettere in alternativa alla precedente
    }
    dist -= shell_t1;
    return dist * d12;
  }

// il primo punto e' tra le due superfici sferiche
  if(p1->rr <= rsh_ext) {
    dist = 1.;    // distanza normalizzata da cui sottrarre i pezzi in vuoto
    if(p2->rr >= rsh_ext) {       // il secondo punto e' fuori
      nn = hitdet_agata(rsh_ext);
      if(nn != 1) {
        printf("Does not hit outer surface (2)\n");
        errexit("getdistance_agata");
      }
      dist = shell_t1;
    }
    nn = hitdet_agata(rsh_int);
    if(nn > 1) dist -= shell_t2 - shell_t1;
    return dist * d12;
  }
  
// entrambi i punti sono esterni
  nn = hitdet_agata(rsh_ext);
  if(nn < 2) return 0.0;
  dist = shell_t2 - shell_t1;
  nn = hitdet_agata(rsh_int);
  if(nn > 1) dist -= shell_t2 - shell_t1;
  return dist * d12;

}

int
hitdet_agata(double rr)
{
  double A, B, C, sq;

  A = shell_dd;
  B = shell_dp;
  C = shell_d1 - rr*rr;
  
  sq = B*B - A*C;
  if(sq < 0.) {
    return 0;
  }
  if(sq == 0.) {
    shell_t1 = -B / A;
    return (shell_t1 >= 0.) ? (1) : (0);
  }

  sq = sqrt(sq);
  shell_t1 = (-B + sq) / A;
  if(shell_t1 < 0.)
    return 0;
  shell_t2 = (-B - sq) / A;
  if(shell_t2 < 0.)
    return 1;
  if(shell_t1 > shell_t2) SWAP(shell_t1, shell_t2, sq)
  return 2;
}
