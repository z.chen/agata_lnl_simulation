////////////////////////////////////////////////////
/////////// GEOMETRY: MXXX /////////////////////////
////////////////////////////////////////////////////

//#include "mgt.h"
#include "mgt.extern.h"
#include "mgt_geomxxx.h"

#define MAXFACES  8           // Max number of faces (front+back+side) for MXXX detectors

typedef struct {
  int     nfaces;             // numero di facce del solido mxxx
  double  rbsph;              // raggio della bounding sphere
  double  rbsph2;             // raggio^2 della bounding sphere
  double  risph;              // raggio della inner sphere
  double  risph2;             // raggio^2 della inner sphere
  double  ssegz;              // segmentazione lungo z
  double  ssegr;              // segmentazione radiale;
  double  ffr;                // raggio della faccia frontale
  double  rfr;                // raggio della faccia posteriore
  double  vmat[MAXFACES][4];  // matrice dei piani  (Ax+By+Cz+D)
  coors   cge;                // il centro geometrico del solido
  coors   ffc;                // il centro della faccia anteriore
  coors   rfc;                // il centro della faccia posteriore
  coors   dax;                // versore dell'asse del rivelatore (ffc->rfc)
  coors   ffv;                // versore dal centro della prima faccia al punto medio dell' ultimo lato (==> phi = 0)
} mxxx;
mxxx    *geomxxx;

/*

double  rsh_int;              // inner radius of shell
double  rsh_int2;             // inner radius of shell squared
double  rsh_out;              // outer radius of shell

int     nsegsZ;               // number of segments along  detector axis
int     nsegsP;               // number of segments around detector axis
int     nsegsR;               // number of segments along  detector radius
*/

int     insidegedet_mxxx    (point *);               //
void    findsegment_mxxx    (point *);               //
void    setsegcenter_mxxx   (point *);               //
double  gedistance_mxxx     (point *, point *);      //

double  gedistance_mxxx_shell(point *p1, point *p2);
void    makegeomxxx          (int, coors *, int);               // genera la matrice delle facce dei solidi MXXX
int     hitdet_mxxx          (int nn, coors *pi, coors *cd);    // verifica se il raggio da pi in direzione cd colpisce nn
void    mccalc_mxxx          ();                                // calcolo MC di volume e angolo solido

void
geominit_mxxx()
{
  int    ii, jj, nn, np;
  double dd, dim1, dim2;
  char   dummy_line[128];
  coors  pmx[2*MAXFACES];
  
  insidegedet  = insidegedet_mxxx;
  findsegment  = findsegment_mxxx;
  setsegcenter = setsegcenter_mxxx;
  gedistance   = gedistance_mxxx;
  if(fscanf(ifp, "%d", &ngedets) != 1) {
    printf("Error reading %s\n", ifname);
    errexit("geominit_mxxx");
  }
  if( dataFileVersion == FAGATA ) {
    fscanf( ifp, "\n%s", dummy_line );  // ENDGEOMETRY line
  }
  printf("Initializing geometry for the MXXX configuration\n");
  geomxxx  = (mxxx  *)calloc(ngedets, sizeof(mxxx));
  CDet     = (coors *)calloc(ngedets, sizeof(coors));
  nsegsZ   = 6;
  nsegsP   = 6;       // in practice this is the number of side faces of the crystal (5 or 6)
  nsegsR   = 1;
  nsegdet  = nsegsZ * nsegsP * nsegsR;
  rsh_int  = 1.e20;
  rsh_int2 = rsh_int * rsh_int;
  rsh_out  = 0.;
  for(nn = 0; nn < ngedets; nn++) {
    if(fscanf(ifp, "%d %d", &ii, &np)  != 2) {
      printf("Error reading geometry data from %s\n", ifname);
      errexit("geominit_mxxx");
    }
    if (ii != nn || np < 0 || np%2) {
      printf("Problem reading geometry data from %s\n", ifname);
      errexit("geominit_mxxx");
    }
    for(jj = 0; jj < np; jj++) {
      if (fscanf(ifp, "%lf %lf %lf", &pmx[jj].xx, &pmx[jj].yy, &pmx[jj].zz)  != 3) {
        printf("Error reading geometry data from %s\n", ifname);
        errexit("geominit_mxxx");
      }
      pmx[jj].xx *= distfactor;
      pmx[jj].yy *= distfactor;
      pmx[jj].zz *= distfactor;
      dd = sqrt(pmx[jj].xx*pmx[jj].xx + pmx[jj].yy*pmx[jj].yy + pmx[jj].zz*pmx[jj].zz);
      rsh_int = MIN(rsh_int, dd);
      rsh_out = MAX(rsh_out, dd);
    }
    makegeomxxx(np, pmx, nn);
  }
  
  dim1   = 0.;
  dim2   = 0.;
  for(nn = 0; nn < ngedets; nn++) {
    dd = geomxxx[nn].vmat[0][3];
    dd = ABS(dd);
    dim1 = MAX(dim1, dd);
    dd = geomxxx[nn].vmat[1][3];
    dd = ABS(dd);
    dim2 = MAX(dim2, dd);
  }
  rsh_int  = MIN(dim1, rsh_int) - .2;
  rsh_out  = MAX(dim2, rsh_out);
  nsegtot  = nsegdet * ngedets;
  GEOMETRY = MXXX;
  if(nsegsR <2)
    sprintf(segmentstring, "(Phi=%d Z=%d)", nsegsP, nsegsZ);
  else
    sprintf(segmentstring, "(Phi=#edges Z=%d R=%d)", nsegsZ, nsegsR);
  
//mccalc_mxxx();
  
}

int
insidegedet_mxxx(point *pp)
{
  int     nd, nf, nn;
  double  dist, dx, dy, dz;
  double *pvm;
  coors  *cge;
  mxxx   *pmxxx;

  nd    = pp->nd;
  pmxxx = &geomxxx[nd];
  cge   = &pmxxx->cge;
  dx = pp->xx - cge->xx;
  dy = pp->yy - cge->yy;
  dz = pp->zz - cge->zz;
  dist  = dx*dx + dy*dy + dz*dz;
  if(dist > pmxxx->rbsph2) return 0;
  if(dist < pmxxx->risph2) return 1;

  nf  = pmxxx->nfaces;
  pvm = pmxxx->vmat[0];
  for(nn = 0; nn < nf; nn++) {
   dist = pp->xx * pvm[0] + pp->yy * pvm[1] + pp->zz * pvm[2] + pvm[3];
   if(dist < 0.) return 0;
   pvm += 4;
  }
  return 1;
}

void
findsegment_mxxx(point *pp)
{
  int    nd, nf, nz, np, nr, ns;
  double tt, pz, dd, cosph, ph;
  double *pvm;
  coors   ppf, ppv;
  coors  *ffc, *ffv;
  mxxx   *pmxxx;

//pp->ns = 0;
//pp->seg = pp->nd;
//return;

  nd = pp->nd;

  pmxxx = geomxxx + nd;
  pvm   = pmxxx->vmat[0];
  nf    = pmxxx->nfaces - 2;

  tt = -(pvm[0]*pp->xx + pvm[1]*pp->yy + pvm[2]*pp->zz + pvm[3]); // parametro dell'intercetta pp-->FacciaFrontale
  pz = ABS(tt);                                                   // distance from the first face
  nz = (int)(pz / pmxxx->ssegz);
  nz = MAX(0, nz);
  nz = MIN(nsegsZ-1, nz);

  ppf.xx = pp->xx + pvm[0] * tt;    // punto proiettato sulla prima faccia
  ppf.yy = pp->yy + pvm[1] * tt;    // andrebbe proiettato puntando al vertice del rivelatore
  ppf.zz = pp->zz + pvm[2] * tt;

  ffc = &pmxxx->ffc;                // lo trasforma in versore ffc->ppf
  ppf.xx -= ffc->xx;
  ppf.yy -= ffc->yy;
  ppf.zz -= ffc->zz;
  dd      = ppf.xx*ppf.xx + ppf.yy*ppf.yy + ppf.zz*ppf.zz;
  if(dd > 0) {
    dd = sqrt(dd);
    nr = (int)(dd / pmxxx->ssegr);
    if(nr >= nsegsR) nr = nsegsR-1;
    dd = 1./ dd;
    ppf.xx *= dd;
    ppf.yy *= dd;
    ppf.zz *= dd;
    ffv = &pmxxx->ffv;
    cosph = ppf.xx * ffv->xx + ppf.yy * ffv->yy + ppf.zz * ffv->zz;
    cosph = MAX(-1., cosph);
    cosph = MIN( 1., cosph);
    ph = acos(cosph);
    ppv.xx = ffv->yy*ppf.zz - ffv->zz*ppf.yy;   // ffv cross ppf
    ppv.yy = ffv->zz*ppf.xx - ffv->xx*ppf.zz;
    ppv.zz = ffv->xx*ppf.yy - ffv->yy*ppf.xx;
    if((ppv.xx*pvm[0] + ppv.yy*pvm[1] + ppv.zz*pvm[2]) < 0.) ph = 2*M_PI-ph;
    np =  (int)(ph / (2.*M_PI/nf));             // 60 or 72 deg slices
  }
  else {
    nr = 0;
    np = rand() % nf;
  }
  ns = nr + nsegsR*(np + nsegsP*nz);

#if STATINSIDESEG == 1
  specIncr(nd       , 19, 0);
  specIncr(np + 1000, 19, 0);
  specIncr(nz + 1500, 19, 0);
  specIncr(nr + 2000, 19, 0);
#endif

  pp->ns  = ns;
  pp->seg = ns + nsegdet * pp->nd;
}

void
setsegcenter_mxxx(point *pp)
{
  double  dist = 0.;
  coors  *csg;

  csg = CSeg + pp->ns;
#if STATINSIDESEG == 1
  dist = sqrt(pow(pp->xx - csg->xx, 2.) + pow(pp->yy - csg->yy, 2.) +pow(pp->zz - csg->zz, 2.));
  specIncr((int)dist+3000, 19, 0);
#endif
  pp->xx = csg->xx;
  pp->yy = csg->yy;
  pp->zz = csg->zz;
  setcosdir(pp);
  return;
}

double
gedistance_mxxx(point *p1, point *p2)
{
  double xx, yy, zz;
  
  if(p1->nd == p2->nd) {
    xx = p2->xx - p1->xx;
    yy = p2->yy - p1->yy;
    zz = p2->zz - p1->zz;
    return sqrt(xx*xx + yy*yy + zz*zz);
  }
  return gedistance_mxxx_shell(p1, p2);    // per adesso considera come se fosse una shell
}

void
makegeomxxx(int np, coors *pc, int detn)
{
  int     nf, nft, nn, np2, jj, kk, ll;
  int     nzz, nph, nrr, nss;
  int     ip[MAXFACES+1];
  double  aa, bb, cc, xcf, ycf, zcf, dist, rbs, ris, dfr;
  double  dx, dy, dz, frr, fzz;
  double *vmat;
  mxxx   *pmxxx;
  coors  *tpc, *cge;
  coors   ffc, ffp, rfc, csgf, csgr;

  pmxxx = geomxxx + detn;
  np2   = np / 2;

  xcf = ycf = zcf = 0;                      // trova il centro della faccia frontale
  tpc = pc;
  for(nn = 0; nn < np2; nn++, tpc++) {
    xcf += tpc->xx; ycf += tpc->yy; zcf += tpc->zz;
  }
  pmxxx->ffc.xx = ffc.xx = xcf/np2;
  pmxxx->ffc.yy = ffc.yy = ycf/np2;
  pmxxx->ffc.zz = ffc.zz = zcf/np2;
  rbs = 0.;                                 // e il raggio della faccia frontale
  tpc = pc;
  for(nn = 0; nn < np2; nn++, tpc++) {
    dist = pow(tpc->xx - ffc.xx, 2.) + pow(tpc->yy - ffc.yy, 2.) + pow(tpc->zz - ffc.zz, 2.);
    rbs = MAX(rbs, dist);
  }
  pmxxx->ffr = sqrt(rbs);

  tpc = pc + np2;
  xcf = ycf = zcf = 0;                      // trova il centro della faccia posteriore
  for(nn = 0; nn < np2; nn++, tpc++) {
    xcf += tpc->xx; ycf += tpc->yy; zcf += tpc->zz;
  }
  pmxxx->rfc.xx = rfc.xx = xcf/np2;
  pmxxx->rfc.yy = rfc.yy = ycf/np2;
  pmxxx->rfc.zz = rfc.zz = zcf/np2;
  rbs = 0.;                                 // e il raggio della faccia posteriore
  tpc = pc + np2;
  for(nn = 0; nn < np2; nn++, tpc++) {
    dist = pow(tpc->xx - rfc.xx, 2.) + pow(tpc->yy - rfc.yy, 2.) + pow(tpc->zz - rfc.zz, 2.);
    rbs = MAX(rbs, dist);
  }
  pmxxx->rfr = sqrt(rbs);

  dfr = sqrt(pow(rfc.xx - ffc.xx, 2.) + pow(rfc.yy - ffc.yy, 2.) + pow(rfc.zz - ffc.zz, 2.));
  pmxxx->dax.xx = (rfc.xx - ffc.xx) / dfr;
  pmxxx->dax.yy = (rfc.yy - ffc.yy) / dfr;
  pmxxx->dax.zz = (rfc.zz - ffc.zz) / dfr;

  pmxxx->ssegz = dfr / nsegsZ;
  pmxxx->ssegr = pmxxx->rfr / nsegsR;

  tpc = pc;
  ffp.xx = (tpc[0].xx + tpc[np2-1].xx) / 2.;    // il punto medio dell'ultimo lato
  ffp.yy = (tpc[0].yy + tpc[np2-1].yy) / 2.;    // cosi' che il primo punto sia al
  ffp.zz = (tpc[0].zz + tpc[np2-1].zz) / 2.;    // centro del primo segmento
  dx = ffp.xx - ffc.xx;
  dy = ffp.yy - ffc.yy;
  dz = ffp.zz - ffc.zz;
  dist   = sqrt(dx*dx + dy*dy + dz*dz);
  pmxxx->ffv.xx = dx / dist;                // versore dal centro della prima faccia al punto medio
  pmxxx->ffv.yy = dy / dist;
  pmxxx->ffv.zz = dz / dist;

  xcf = ycf = zcf = 0;                      // trova il centro del rivelatore
  tpc = pc;
  for(nn = 0; nn < np; nn++, tpc++) {
    xcf += tpc->xx; ycf += tpc->yy; zcf += tpc->zz;
  }
  cge = &pmxxx->cge;
  cge->xx = xcf/np;
  cge->yy = ycf/np;
  cge->zz = zcf/np;
  memcpy(CDet+detn, cge, sizeof(coors));     // riporta il centro in Cge

  rbs = 0.;                                 // trova il raggio della bounding sphere
  tpc = pc;
  for(nn = 0; nn < np; nn++, tpc++) {
    dx = tpc->xx - cge->xx;
    dy = tpc->yy - cge->yy;
    dz = tpc->zz - cge->zz;
    dist = dx*dx + dy*dy + dz*dz;
    rbs  = MAX(rbs, dist);
  }
	rbs = sqrt(rbs) + 1.0;                    // aggiunto 1 mm per sicurezza
	pmxxx->rbsph  = rbs;
	pmxxx->rbsph2 = rbs * rbs;

  vmat = pmxxx->vmat[0];                    // calcola la matrice di volume del poliedro
  pmxxx->nfaces = nft = np2 + 2;
  for(nf = 0; nf < nft; nf++) {
     if(nf==0 || nf==1) {
       nn = np2;
       for(ll = 0; ll < nn; ll++) ip[ll] = ll + nn*(nf==1);
       ip[nn] = ip[0];
     }
     else {
       nn = 4;
       ip[0] = nf-2;       ip[1] = (nf-2+1)%(np2);
       ip[3] = nf-2 + np2; ip[2] = (nf-2+1)%(np2) + np2;
       ip[4] = ip[0];
     }
	   aa  = 0;
	   bb  = 0;
	   cc  = 0;
	   xcf = 0;
	   ycf = 0;
	   zcf = 0;
     for(ll = 0; ll < nn; ll++) {
       jj = ip[ll]; kk = ip[ll+1];
       aa  += (pc[jj].yy - pc[kk].yy) * (pc[jj].zz + pc[kk].zz);
       bb  += (pc[jj].zz - pc[kk].zz) * (pc[jj].xx + pc[kk].xx);
       cc  += (pc[jj].xx - pc[kk].xx) * (pc[jj].yy + pc[kk].yy);
       xcf +=  pc[jj].xx;
       ycf +=  pc[jj].yy;
       zcf +=  pc[jj].zz;
     }
	   xcf /= nn;             // il punto medio della faccia
	   ycf /= nn;
	   zcf /= nn;
	   dist = sqrt(aa*aa + bb*bb + cc*cc);
	   vmat[0] = aa/dist;		  // normalizzatzione dei coseni direttori della retta normale al piano
	   vmat[1] = bb/dist;
	   vmat[2] = cc/dist;
	   vmat[3] = -(vmat[0]*xcf + vmat[1]*ycf + vmat[2]*zcf); // il quarto coefficiente da un punto sicuramente nel piano
	   dist =	cge->xx*vmat[0] + cge->yy*vmat[1] + cge->zz*vmat[2] + vmat[3];  // distanza del centro rivelatore dal piano
     if(dist < 0.) {          // aggiusta il segno della faccia in modo che internamente la distanza sia positiva 
	    vmat[0] *= -1.;
	    vmat[1] *= -1.;
	    vmat[2] *= -1.;
	    vmat[3] *= -1.;
     }
     vmat += 4;
  }

  ris = 0.;                                   // raggio della inner sphere
  vmat = pmxxx->vmat[0];
  for(nf = 0; nf < nft; nf++) {
	   dist =	cge->xx*vmat[0] + cge->yy*vmat[1] + cge->zz*vmat[2] + vmat[3];  // distanza del piano dal centro rivelatore 
     ris = MAX(ris, dist);
     vmat += 4;
  }
	ris = sqrt(ris) - 1.0;                    // tolto 1 mm per sicurezza
  ris = MAX(ris, 0.);
	pmxxx->risph  = ris;
	pmxxx->risph2 = ris * ris;

// calcola i centri dei segmenti
  CSeg = (coors *)calloc(nsegdet, sizeof(coors));
  for(nrr = 0; nrr < nsegsR; nrr++) {
    frr = (1.+2.*nrr)/(2*nsegsR);
    for(nph = 0; nph < np2; nph++) {
      csgf.xx = ffc.xx +(pc[nph    ].xx - ffc.xx)*frr;  // centro dei segmenti sulla faccia frontale
      csgf.yy = ffc.yy +(pc[nph    ].yy - ffc.yy)*frr;
      csgf.zz = ffc.zz +(pc[nph    ].zz - ffc.zz)*frr;
      csgr.xx = rfc.xx +(pc[nph+np2].xx - rfc.xx)*frr;  // centro dei segmenti sulla faccia posteriore
      csgr.yy = rfc.yy +(pc[nph+np2].yy - rfc.yy)*frr;
      csgr.zz = rfc.zz +(pc[nph+np2].zz - rfc.zz)*frr;
      for(nzz = 0; nzz < nsegsZ; nzz++) {
        nss = nrr + nsegsR*(nph + nzz*nsegsP);
        fzz = (1.+2.*nzz)/(2.*nsegsZ);
        CSeg[nss].xx = csgf.xx + (csgr.xx - csgf.xx)*fzz;
        CSeg[nss].yy = csgf.yy + (csgr.yy - csgf.yy)*fzz;
        CSeg[nss].zz = csgf.zz + (csgr.zz - csgf.zz)*fzz;
        CSeg[nss].nd = detn;
        CSeg[nss].ns = nss;
      }
    }
  }
}

void
mccalc_mxxx()
{
  int    ii, jj, iseen, mctot;
  double dim1, dim2, axl, xy, th, ph;
  coors  cpos, cdir;
  point  ppos;
  coors *cge;

  mctot = 10000;

  dim2    = 0.;
  for(ii = 0; ii < ngedets; ii++) {
    cge = &geomxxx[ii].cge;
    axl = 2.* geomxxx[ii].rbsph;
    ppos.nd = ii;
    iseen = 0;
    for(jj = 0; jj < mctot; jj++) {
      ppos.xx = (RAND()-0.5)*axl + cge->xx;
      ppos.yy = (RAND()-0.5)*axl + cge->yy;
      ppos.zz = (RAND()-0.5)*axl + cge->zz;
      if(insidegedet_mxxx(&ppos)) iseen++;
    }
    dim1 = iseen*axl*axl*axl/(double)mctot;
//  printf("Volume(%4d) = %10.1f cc\n", ii, dim1);
    dim2 += dim1;
  }
  printf("Total Volume %12.1f cc\n", dim2);
  
  iseen = 0;
  cpos.xx = origin.xx;
  cpos.yy = origin.yy;
  cpos.zz = origin.zz;
  for(ii = 0; ii < mctot; ii++) {
    cdir.zz = 1.0 - 2.0 * RAND();
    th = acos(cdir.zz);
    xy = sin(th);
    ph = 2.0 * M_PI * RAND();
    cdir.xx = cos(ph) * xy;
    cdir.yy = sin(ph) * xy;
    for(jj = 0; jj < ngedets; jj++) {
      if(hitdet_mxxx(jj, &cpos, &cdir)) {
        iseen++;
	printf("\n jj=%i \n",jj);
        specIncr(jj+6000, 17, 0);     // distribuzione angolare dei rivelatori
        break;
      }
    }
  }
  printf("Total Solid Angle %7.1f %%\n", 100.*iseen/(double)mctot);
}

int
hitdet_mxxx(int nn, coors *pi, coors *cd)
{
  int     ii, jj, nf;
  double  tt, td, xh, yh, zh, dd;
  double *pvm, *pvmii, *pvmjj;

  nf  = geomxxx[nn].nfaces;
  pvm =  pvmii = geomxxx[nn].vmat[0];
  for(ii = 0; ii < nf; ii++) {
    tt = cd->xx*pvmii[0] + cd->yy*pvmii[1] + cd->zz*pvmii[2]; 
    if(ABS(tt) < 1.e-6) continue;   // parallelo alla faccia
    td = pi->xx*pvmii[0] + pi->yy*pvmii[1] + pi->zz*pvmii[2] + pvmii[3];
    tt = -td / tt;
    if(tt < -1.e-4) continue;
    xh = pi->xx + cd->xx*tt;
    yh = pi->yy + cd->yy*tt;
    zh = pi->zz + cd->zz*tt;
    pvmjj  = pvm;
    for(jj = 0; jj < nf; jj++) {
      if(jj != ii) {
        dd = xh*pvmjj[0] + yh*pvmjj[1] + zh*pvmjj[2] + pvmjj[3];
        if(dd < 0.) break;
      }
      pvmjj += 4;
    }
    if(dd > 0) return 1;
    pvmii += 4;
  }
  return 0;
}

double
gedistance_mxxx_shell(point *p1, point *p2)
{
  double a, b, c, d, d2, t1, t2, dx, dy, dz, dd;
  point *pt;

  if(p1->rr > p2->rr) SWAP(p1, p2, pt)

  if(p1->rr <= rsh_int) {
    if(p2->rr <= rsh_int) {
      return 0.0;
    }
    if(p1->rr == 0.0) return p2->rr - rsh_int;
    dx = p2->xx - p1->xx;
    dy = p2->yy - p1->yy;
    dz = p2->zz - p1->zz;
    a  = dx*dx + dy*dy + dz*dz;
    b  = (p1->xx * p2->xx + p1->yy * p2->yy + p1->zz * p2->zz);
    dd = p1->rr*p1->rr;
    b -= dd;
    c  = dd - rsh_int2;
    d2 = b*b - a*c;
    if(d2 <= 0.0)  return  0.0;
    d  = sqrt(d2);
    t1 = (- b + d) / a;
    t2 = (- b - d) / a;
    if (t1 <= 0.) {
      if(t2 <= 0. || t2 > 1.)   return  0.0;
      return (1.0 - t2) * sqrt(a);
    }
    if (t1 > 1.)   return  0.0;
    return (1.0 - t1) * sqrt(a);
    
  }
  
  dx = p2->xx - p1->xx;
  dy = p2->yy - p1->yy;
  dz = p2->zz - p1->zz;
  a  = dx*dx + dy*dy + dz*dz;
  b  = (p1->xx * p2->xx + p1->yy * p2->yy + p1->zz * p2->zz);
  dd = p1->rr*p1->rr;
  b -= dd;
  c  = dd - rsh_int2;
  d2 = b * b - a * c;
  if(d2 <= 0.0)  return  sqrt(a);
  d  = sqrt(d2);
  t1 = (- b + d) / a;
  if (t1 <= 0. || t1 > 1.)   return  sqrt(a); 
  t2 = (- b - d) / a;
  if (t2 <= 0. || t2 > 1.)   return  sqrt(a); 
//return -(1.0 - fabs(t2 - t1)) * sqrt(a); // negative sign to indicate scattering across center
  return (1.0 - fabs(t2 - t1)) * sqrt(a);
}
