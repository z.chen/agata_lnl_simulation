
/////////////////////////////////////////////////////////////////////////
// The Messenger
////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataDetectorConstructionMessenger::AgataDetectorConstructionMessenger(AgataDetectorConstruction* pTarget, G4bool vol, G4String name)
:myTarget(pTarget)
{ 
  volume = vol;
  
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of detector construction.");
  
  commandName = directoryName + "update";
  aLine = commandName.c_str();
  UpdateCmd = new G4UIcmdWithoutParameter(aLine, this);
  UpdateCmd->SetGuidance("Update array geometry (MANDATORY in case of changes).");
  UpdateCmd->SetGuidance("This command MUST be applied before \"beamOn\" if any geometrical value(s) was changed.");
  UpdateCmd->SetGuidance("Required parameters: none.");
  UpdateCmd->AvailableForStates(G4State_Idle);
  
  commandName = directoryName + "targetMaterial";
  aLine = commandName.c_str();
  TarMatCmd = new G4UIcmdWithAString(aLine, this);
  TarMatCmd->SetGuidance("Select Material of the target.");
  TarMatCmd->SetGuidance("Required parameters: 1 string.");
  TarMatCmd->SetParameterName("choice",false);
  TarMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "targetSize";
  aLine = commandName.c_str();
  SetSizeCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetSizeCmd->SetGuidance("Define size of target.");
  SetSizeCmd->SetGuidance("Required parameters: 3 double (x y in mm, z in mg/cm2).");
  SetSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "targetPosition";
  aLine = commandName.c_str();
  SetPositionCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetPositionCmd->SetGuidance("Define position of the target.");
  SetPositionCmd->SetGuidance("Required parameters: 3 double (target position in mm).");
  SetPositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "wallThickness";
  aLine = commandName.c_str();
  WalThickCmd = new G4UIcmdWithADouble(aLine, this);  
  WalThickCmd->SetGuidance("Define thickness of the chamber.");
  WalThickCmd->SetGuidance("Required parameters: 1 double (thickness of the chamber in mm).");
  WalThickCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "chamberRadius";
  aLine = commandName.c_str();
  ChamRadiusCmd = new G4UIcmdWithADouble(aLine, this);  
  ChamRadiusCmd->SetGuidance("Define radius of the chamber.");
  ChamRadiusCmd->SetGuidance("Required parameters: 1 double (radius of the chamber in mm).");
  ChamRadiusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "pipeRadius";
  aLine = commandName.c_str();
  PipeRadiusCmd = new G4UIcmdWithADouble(aLine, this);  
  PipeRadiusCmd->SetGuidance("Define radius of the beam pipe.");
  PipeRadiusCmd->SetGuidance("Required parameters: 1 double (radius of the pipe in mm).");
  PipeRadiusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "chamberMaterial";
  aLine = commandName.c_str();
  ChamMatCmd = new G4UIcmdWithAString(aLine, this);
  ChamMatCmd->SetGuidance("Select Material of the chamber.");
  ChamMatCmd->SetGuidance("Required parameters: 1 string.");
  ChamMatCmd->SetParameterName("choice",false);
  ChamMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "rotateChamber";
  aLine = commandName.c_str();
  RotateChamberCmd = new G4UIcmdWithAString(aLine, this);
  RotateChamberCmd->SetGuidance("Select rotation of the scattering chamber.");
  RotateChamberCmd->SetGuidance("Required parameters: 2 double (rotation angles in degrees).");
  RotateChamberCmd->SetParameterName("choice",false);
  RotateChamberCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters.");
  StatusCmd->SetGuidance("Required parameters: none.");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "DistInGe";
  aLine = commandName.c_str();
  CalcDistGeCmd = new G4UIcmdWithAString(aLine, this);
  CalcDistGeCmd->SetGuidance("Calculates equivalent distance in Germanium between two points.");
  CalcDistGeCmd->SetGuidance("Required parameters: 6 double, 1 integer");
  CalcDistGeCmd->SetGuidance("(coordinates in mm of the two points to be tested and the number of steps needed for the calculation.");
  CalcDistGeCmd->SetParameterName("choice",false);
  CalcDistGeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  if( volume ) {
    commandName = directoryName + "volume";
    aLine = commandName.c_str();
    CalcVolCmd = new G4UIcmdWithAString(aLine, this);
    CalcVolCmd->SetGuidance("Calculates volume of the germanium detectors.");
    CalcVolCmd->SetGuidance("Required parameters: 1 integer, 3 double");
    CalcVolCmd->SetGuidance("(number of test points and size in mm of a box containing the detectors.");
    CalcVolCmd->SetParameterName("choice",false);
    CalcVolCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName + "omega";
    aLine = commandName.c_str();
    CalcOmegaCmd = new G4UIcmdWithAnInteger(aLine, this);
    CalcOmegaCmd->SetGuidance("Calculates solid angle covered by the germanium detectors.");
    CalcOmegaCmd->SetGuidance("Required parameters: 1 integer.");
    CalcOmegaCmd->SetParameterName("choice",false);
    CalcOmegaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
  
  commandName = directoryName + "ancillary/";
  myADirectory = new G4UIdirectory(commandName);
  commandName = G4String("Control of the ancillary detectors for ") + name + G4String(".");
  myADirectory->SetGuidance(commandName);
  
}

AgataDetectorConstructionMessenger::~AgataDetectorConstructionMessenger()
{
  delete myDirectory;
  delete myADirectory;
  delete UpdateCmd;
  delete TarMatCmd;
  delete ChamMatCmd;
  delete ChamRadiusCmd;
  delete PipeRadiusCmd;
  delete SetSizeCmd;
  delete WalThickCmd;
  delete RotateChamberCmd;
  delete SetPositionCmd;
  delete StatusCmd;
  delete CalcDistGeCmd;
  if(volume) {
    delete CalcVolCmd;
    delete CalcOmegaCmd;
  }
}

void AgataDetectorConstructionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == TarMatCmd ) {
    myTarget->SetTargMate(newValue);
  }
  if( command == ChamMatCmd ) {
    myTarget->SetChamMate(newValue);
  }
  if( command == SetSizeCmd ) {
    myTarget->SetTargetSize(SetSizeCmd->GetNew3VectorValue(newValue));
  }
  if( command == WalThickCmd ) {
    myTarget->SetWallThickness(WalThickCmd->GetNewDoubleValue(newValue));
  }
  if( command == ChamRadiusCmd ) {
    myTarget->SetChamberRadius(ChamRadiusCmd->GetNewDoubleValue(newValue));
  }
  if( command == PipeRadiusCmd ) {
    myTarget->SetPipeRadius(PipeRadiusCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositionCmd ) {
    myTarget->SetTargetPosition(SetPositionCmd->GetNew3VectorValue(newValue));
  }
  if( command == UpdateCmd ) {
    myTarget->UpdateGeometry();
  } 
  if( command == StatusCmd ) {
    myTarget->ShowStatus( );
  }
  if( command == RotateChamberCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetThetaChamb( e1*deg );
    myTarget->SetPhiChamb  ( e2*deg );
  }
  if( command == CalcDistGeCmd ) {
    G4int n;
    G4double x, y, z, X, Y, Z;
    if( sscanf( newValue, "%lf %lf %lf %lf %lf %lf %d", &x, &y, &z, &X, &Y, &Z, &n ) != 7 )
      G4cout << " Wrong number of parameters (seven numbers required)." << G4endl;
    else
      myTarget->GetDistInGe(G4ThreeVector(x,y,z)*mm, G4ThreeVector(X,Y,Z)*mm, n);
  }
  if( volume ){
    if( command == CalcOmegaCmd ) {
      myTarget->CalculateGeSolidAngle(CalcOmegaCmd->GetNewIntValue(newValue));
    }
    if( command == CalcVolCmd ) {
      G4int n;
      G4double x, y, z;
      if( sscanf( newValue, "%d %lf %lf %lf", &n, &x, &y, &z ) != 4 )
        G4cout << " Wrong number of parameters (four numbers required)." << G4endl;
      else {
        myTarget->CalculateGeVolume( n, G4ThreeVector(x,y,z)*mm );
      }  
    }
  }
}

///////////////////////////////////////////////////////////////////////
// Methods common to Agata, Gasp and Clara
//////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////
/// The materials used by Agata are defined here
////////////////////////////////////////////////////////////
void AgataDetectorConstruction::DefineMaterials()
{

  G4double a, z, density, temperature, pressure;
  G4double fractionmass;
  G4String name, symbol;
  G4int    nelements, natoms, nprot, nnucl, ncomponents;

  std::vector<G4Element*>  myElements;    // save pointers here to avoid
  std::vector<G4Material*> myMaterials;   // warnings of unused variables
  
  G4Material* Vacuum = new G4Material(name="Vacuum", z=1., a= 1.01*g/mole, density= universe_mean_density,
                                      kStateGas, temperature = 0.1*kelvin, pressure=1.0e-19*pascal);
  myMaterials.push_back(Vacuum);

  
  G4Element* elN  = new G4Element(name="Nitrogen", symbol="N",  z=7.,  a= 14.00674*g/mole);
  myElements.push_back(elN);

  G4Element* elO  = new G4Element(name="Oxigen",   symbol="O",  z=8.,  a= 15.9994 *g/mole);
  myElements.push_back(elO);

  G4Material* Air = new G4Material(name="Air", density=1.29*mg/cm3, nelements=2);
  Air->AddElement(elN, .7);
  Air->AddElement(elO, .3);
  myMaterials.push_back(Air);

  G4Material* Al = new G4Material(name="Aluminium", z=13., a= 26.98154*g/mole, density= 2.70  *g/cm3);
  myMaterials.push_back(Al);

  G4Material* Si = new G4Material(name="Silicon", z=14., a= 28.0855*g/mole, density= 2.330  *g/cm3);
  myMaterials.push_back(Si);

  // Germanium isotopes
  G4Isotope* Ge70 = new G4Isotope(name="Ge70", nprot=32, nnucl=70, a=69.9242*g/mole);
  G4Isotope* Ge72 = new G4Isotope(name="Ge72", nprot=32, nnucl=72, a=71.9221*g/mole);
  G4Isotope* Ge73 = new G4Isotope(name="Ge73", nprot=32, nnucl=73, a=72.9235*g/mole);
  G4Isotope* Ge74 = new G4Isotope(name="Ge74", nprot=32, nnucl=74, a=73.9212*g/mole);
  G4Isotope* Ge76 = new G4Isotope(name="Ge76", nprot=32, nnucl=76, a=75.9214*g/mole);
  // germanium defined via its isotopes
  G4Element* elGe = new G4Element(name="Germanium",symbol="Ge", nelements=5);
  elGe->AddIsotope(Ge70, 0.2123);
  elGe->AddIsotope(Ge72, 0.2766);
  elGe->AddIsotope(Ge73, 0.0773);
  elGe->AddIsotope(Ge74, 0.3594);
  elGe->AddIsotope(Ge76, 0.0744);
  myElements.push_back(elGe);

  G4Material* Ge = new G4Material(name="Germanium", density=5.323 *g/cm3, nelements=1);
  Ge->AddElement(elGe, natoms=1);
  myMaterials.push_back(Ge);
  
  G4Element* elCu = new G4Element(name="Copper",   symbol="Cu", z=29., a=63.546    *g/mole);
  myElements.push_back(elCu);

  G4Element* elAl = new G4Element(name="Aluminum", symbol="Al", z=13., a= 26.98154*g/mole);
  myElements.push_back(elAl);

  G4Element* elMg = new G4Element(name="Magnesium",     symbol="Mg", z=12., a=24.305    *g/mole);
  myElements.push_back(elMg);

  G4Element* elMn = new G4Element(name="Manganese", symbol="Mn", z=25., a=54.938    *g/mole);
  myElements.push_back(elMn);
  
 //------------------------------------------------------------------
  //DurAl [3-4% Cu, 0.5% Mg, 0.25-1%Mn, remainder Al] : use average
  //                                                    values
  G4Material* durAl = new G4Material(name="DurAluminium",
                                        density = 2.8*g/cm3, ncomponents=4);
  durAl->AddElement(elCu, fractionmass= 3.5*perCent);
  durAl->AddElement(elMg, fractionmass= 0.5*perCent);
  durAl->AddElement(elMn, fractionmass= 0.6*perCent);
  durAl->AddElement(elAl, fractionmass=95.4*perCent);
  myMaterials.push_back(durAl);
  
  DefineAdditionalMaterials();
 
  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}


void AgataDetectorConstruction::ConstructHall()
{
  //G4String matWorldName = "Air";
  G4String matWorldName = "Vacuum";
  
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matWorldName);
  if (ptMaterial) {
    matWorld = ptMaterial;
    G4cout << "\n ----> The world material is "
          << matWorld->GetName() << G4endl;
  }
  else {
    G4cout << " ----> Material not found, cannot build the experimental hall! " << G4endl;
    return;
  }
  ///////////////////////////////////
  // experimental hall (world volume)
  ///////////////////////////////////

  G4double hall_x = 10.0*m;
  G4double hall_y = 10.0*m;
  G4double hall_z = 10.0*m;
  
  if( hallPhys )
    delete hallLog;
  
  hallBox  = new G4Box("hallBox", hall_x, hall_y, hall_z);
  hallLog  = new G4LogicalVolume(hallBox, matWorld, "hallLog", 0, 0, 0);
  hallPhys = new G4PVPlacement(0, G4ThreeVector(), "hallPhys", hallLog, 0, false, 0);
  
  if( matTargetName != "Vacuum" )
    ConstructTarget();  

  if( matChamberName != "Vacuum" )
    ConstructChamber();  


  G4VisAttributes*
  hallVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,1.0));
  hallVisAtt->SetForceWireframe(true);
  hallVisAtt->SetVisibility(false);
  hallLog->SetVisAttributes(hallVisAtt);
}

///////////////////////////////////////////////////////////////////////////////////////
/// This method handles the placement of the target.
/// At class initialization, target material is vacuum --> target is not constructed
/// Setting the target material initializes matTarget to non-NULL values
///////////////////////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::ConstructTarget( )
{
  if( matTargetName == "Vacuum" ) return;
  
  targetSize.setZ( targetThickness / matTarget->GetDensity() ); //> convert to linear dimensions!
  
  G4Box* solidTarget = new G4Box( "target", targetSize.x()/2, targetSize.y()/2, targetSize.z()/2 );
  G4LogicalVolume* logicTarget = new G4LogicalVolume( solidTarget, matTarget, "Target", 0, 0, 0 );
  new G4PVPlacement(0, targetPosition, "Target", logicTarget, hallPhys, false, 0 );
                                                           
  G4VisAttributes* targetVisAtt = new G4VisAttributes( G4Colour(0.5, 0.5, 0.5) );
  targetVisAtt -> SetForceWireframe(true);
  logicTarget -> SetVisAttributes( targetVisAtt );                                                                   

}

///////////////////////////////////////////////////////////////////////////////////////
/// This method handles the placement of the chamber.
/// At class initialization, chamber material is vacuum --> chamber is not constructed
/// Setting the chember material initializes matChamber to non-NULL values
/// The chamber is made of a spherical part, with two entrance/exit pipes. If the
/// radius is smaller than the pipe diameter, only a cylinder is built.
/// Setting the pipe radius smaller than the thickness
///////////////////////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::ConstructChamber( )
{
  /// First check that the geometry parameters are consistent
  G4bool noPipe = false;
  G4bool noSphe = false;
  if( pipeRadius < wallThickness )
    noPipe = true;
  if( (outRadius <= pipeRadius) || (outRadius < wallThickness) )
    noSphe = true;
  
  if( noPipe && noSphe ) {
    G4cout << "---> Warning! Cannot build the scattering chamber!" << G4endl;
    return;
  } 
  if( noPipe ) 
    G4cout << "---> Warning! No beam pipe will be built." << G4endl;
  if( noSphe ) 
    G4cout << "---> Warning! Only the beam pipe will be built." << G4endl;
  
  G4RotationMatrix rm;
  rm.set(0,0,0);
  rm.rotateY(thetaChamb);
  rm.rotateZ(phiChamb);
  
  G4VisAttributes* ChamberVisAtt = new G4VisAttributes( G4Colour(0.5, 0.5, 0.5) );
  ChamberVisAtt -> SetForceWireframe(true);
  
  G4double minZ = 50.*cm;
  if( noPipe )
    minZ = outRadius;
  
  G4double pipeIRadius = pipeRadius - wallThickness;
  
  G4double *zPipe;
  G4double *outPipe;
  G4double *innPipe;
  
  G4Polycone* solidPipe = NULL;
  
  if( noSphe ) {
    // simply a cylinder
    // G4cout << " Warning! Forcing chamber to be a simple tube." << G4endl;
    zPipe   = new G4double[2];
    outPipe = new G4double[2];
    innPipe = new G4double[2];
    
    zPipe[0] = -minZ;
    zPipe[1] =  minZ;
    
    outPipe[0] = pipeRadius;
    outPipe[1] = pipeRadius;
    
    innPipe[0] = pipeIRadius;
    innPipe[1] = pipeIRadius;
    
    solidPipe = new G4Polycone( "pipe", 0.*deg, 360.*deg, 2, zPipe, innPipe, outPipe );
  }
  else {
    G4int nSteps = 100;
    G4int size   = nSteps - 1 + 6;
    
    zPipe   = new G4double[size];
    outPipe = new G4double[size];
    innPipe = new G4double[size];
    
    G4double innRadius = outRadius  - wallThickness;
    G4double sinTheta;
    G4double cosTheta;
    if( noPipe ) {
      sinTheta = 0.;
      cosTheta = 1.;
    }
    else {
      sinTheta  = pipeRadius / outRadius;
      cosTheta  = sqrt( 1. - sinTheta*sinTheta );
    }
  
    // first we fill the z coordinates
    zPipe[0] = -minZ;
    zPipe[1] = -outRadius * cosTheta;
    zPipe[2] = -innRadius * cosTheta;

    zPipe[size-1] = -zPipe[0];
    zPipe[size-2] = -zPipe[1];
    zPipe[size-3] = -zPipe[2];
    
    G4double deltaZ = ((zPipe[size-3]-zPipe[2])/(G4double)nSteps);
    for( G4int ii=1; ii<nSteps; ii++ )
      zPipe[ii+2] = zPipe[2] + ii * deltaZ;
    
    // Inner radius
    if( noPipe ) {
      innPipe[0] = 0.;
      innPipe[1] = 0.;
      innPipe[2] = 0.;
    }
    else {
      innPipe[0] = pipeIRadius;
      innPipe[1] = pipeIRadius;
      innPipe[2] = pipeIRadius;
    }
    
    innPipe[size-1] = innPipe[0];
    innPipe[size-2] = innPipe[1];
    innPipe[size-3] = innPipe[2];
    
    for( G4int ij=1; ij<nSteps; ij++ )
      innPipe[ij+2] = sqrt( innRadius*innRadius - zPipe[ij+2]*zPipe[ij+2] );
    
    // Outer radius
    if( noPipe ) {
      outPipe[0] = 0.;
      outPipe[1] = 0.;
    }
    else {
      outPipe[0] = pipeRadius;
      outPipe[1] = pipeRadius;
    }
    
    outPipe[size-1] = pipeRadius;
    outPipe[size-2] = pipeRadius;
    
    for( G4int ik=0; ik<nSteps+1; ik++ )
      outPipe[ik+2] = sqrt( outRadius*outRadius - zPipe[ik+2]*zPipe[ik+2] );
    
    // Finally, the polycone!
    solidPipe = new G4Polycone( "pipe", 0.*deg, 360.*deg, size, zPipe, innPipe, outPipe );
  
  } 
  
  G4LogicalVolume* logicPipe = new G4LogicalVolume( solidPipe, matChamber, "Pipe", 0, 0, 0 );
  logicPipe    -> SetVisAttributes( ChamberVisAtt );
  new G4PVPlacement( G4Transform3D(rm,G4ThreeVector()), "Pipe", logicPipe, hallPhys, false, 0 );
}


/////////////////////////////////////////////////////////////////////////
// Methods for the Messenger
/////////////////////////////////////////////////////////////////////////
#include "AgataPhysicsList.hh"

void AgataDetectorConstruction::SetTargMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matTargetName = materialName;
    matTarget     = ptMaterial;
    G4cout << "\n ----> The target material is "
          << matTarget->GetName() << G4endl;
  }
  else {
    G4cout << " Material not found! " << G4endl;
    // First time matTarget is still NULL
    if( matTargetName == "Vacuum" )
      G4cout << " Cannot build target. " << G4endl;
    else  
      G4cout << " ----> Keeping previously set target material ("
             << matTarget->GetName() << ")" << G4endl;
  }
}

void AgataDetectorConstruction::SetChamMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matChamberName = materialName;
    matChamber     = ptMaterial;
    G4cout << "\n ----> The chamber material is "
          << matChamber->GetName() << G4endl;
  }
  else {
    G4cout << " Material not found! " << G4endl;
    if( matChamberName == "Vacuum" )
      G4cout << " Cannot build chamber. " << G4endl;
    else  
      G4cout << " ----> Keeping previously set chamber material ("
             << matChamber->GetName() << ")" << G4endl;
  }
}

void AgataDetectorConstruction::SetWallThickness( G4double thick )
{
  if( thick < 0. ) {
    G4cout << " ----> Invalid value, keeping previous value " << wallThickness/mm << " mm" << G4endl;
  }
  else {
    wallThickness = thick * mm;
    G4cout << " ----> The wall thickness is now " << wallThickness/mm << " mm" << G4endl;
  }
}

void AgataDetectorConstruction::SetChamberRadius( G4double radius )
{
  if( radius < 0. ) {
    G4cout << " ----> Invalid value, keeping previous value " << outRadius/mm << " mm" << G4endl;
  }
  else {
    outRadius = radius * mm;
    G4cout << " ----> The chamber radius is now " << outRadius/mm << " mm" << G4endl;
  }
}

void AgataDetectorConstruction::SetPipeRadius( G4double radius )
{
  if( radius < 0. ) {
    G4cout << " ----> Invalid value, keeping previous value " << pipeRadius/mm << " mm" << G4endl;
  }
  else {
    pipeRadius = radius * mm;
    G4cout << " ----> The pipe radius is now " << pipeRadius/mm << " mm" << G4endl;
  }
}

void AgataDetectorConstruction::SetTargetSize( G4ThreeVector size )
{
  if( size.x() > 0. &&  size.y() > 0. && size.z() > 0.) {
    targetThickness = size.z()*mg/cm2;
    targetSize = size * mm;
  }  
  else
    G4cout << " Could not change target size (lengths must be > 0) !" << G4endl;

  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Target size set to ("
              << std::setw(8) << targetSize.x()/mm << " x " 
              << std::setw(8) << targetSize.y()/mm << ") mm x "
              << std::setw(8) << targetThickness/(mg/cm2) << " mg/cm2" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataDetectorConstruction::SetTargetPosition( G4ThreeVector pos )
{
  targetPosition = pos * mm;

  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Target position set to ("
              << std::setw(8) << targetPosition.x()/mm
              << std::setw(8) << targetPosition.y()/mm
              << std::setw(8) << targetPosition.z()/mm << ") mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataDetectorConstruction::ShowCommonStatus()
{
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  if( matTargetName != "Vacuum" ) {
    G4cout << " The target material is " << matTargetName << G4endl;
    G4cout << " The target position is ("
           << std::setw(8) << targetPosition.x()/cm
           << std::setw(8) << targetPosition.y()/cm
           << std::setw(8) << targetPosition.z()/cm << ") cm" << G4endl;
    G4cout << " The target size is "
           << std::setw(8) << targetSize.x()/cm << "cm x"
           << std::setw(8) << targetSize.y()/cm << "cm x"
           << std::setw(8) << targetSize.z()*matTarget->GetDensity()/(mg/cm2) << "mg/cm2"  << G4endl;
  }
  if( matChamberName != "Vacuum" ) {
    G4cout << " The chamber material is " << matChamberName << G4endl;
  }
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataDetectorConstruction::SetThetaChamb( G4double angle )
{
  thetaChamb = angle;
  G4cout << " ----> The chamber is rotated by an angle theta = " << thetaChamb/deg << " degrees" << G4endl;
}

void AgataDetectorConstruction::SetPhiChamb( G4double angle )
{
  phiChamb = angle;
  G4cout << " ----> The chamber is rotated by an angle phi = " << phiChamb/deg << " degrees" << G4endl;
}

////////////////////////////////////////////////////////////////////////////////////////////
/// Special methods to calculate the volume and the geometrical coverage of the germanium
///  plus equivalent distance in germanium between any two points
///////////////////////////////////////////////////////////////////////////////////////////
#include "G4TransportationManager.hh"
#include "G4Navigator.hh"
#include "Randomize.hh"
#include "AgataRunAction.hh"
#include "CLHEP/Random/RandomEngine.h"
#include "AgataSteppingOmega.hh"
#include "AgataGeneratorOmega.hh"
#include "AgataGeneratorAction.hh"
#include "AgataAnalysis.hh"

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// The equivalent distance in germanium is calculated as a summation, normalized via the
/// radiation lengths of the materials.
/////////////////////////////////////////////////////////////////////////////////////////////////////
G4double AgataDetectorConstruction::DistanceInGe( G4ThreeVector point1, G4ThreeVector point2, G4int nSteps )
{
  G4int nSt = nSteps;
    
  if( nSt < 0 )
    nSt = 1000;

  if( (point1-point2).mag() < 0.001*mm )
    return 0.;
  
    
  G4ThreeVector step = (point2-point1)/nSt;   

  G4Navigator* theNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  theNavigator->SetWorldVolume(hallPhys);       

  G4ThreeVector      myPoint = point1;
  G4VPhysicalVolume* myVolume;
  
  G4double distInGe = 0.;
  G4double radLenGe = 0.;
  G4double stepSize = step.mag();
  
  // search the Germanium material by its name
  G4Material* ptMaterial = G4Material::GetMaterial("Germanium");
  if (ptMaterial) 
    radLenGe = ptMaterial->GetRadlen();
    
  for( G4int ii=0; ii<nSt; ii++ ) {
    myVolume = theNavigator->LocateGlobalPointAndSetup(myPoint);
    if( myVolume->GetLogicalVolume()->GetMaterial()->GetName() != "Vacuum" )
      distInGe += stepSize * radLenGe / myVolume->GetLogicalVolume()->GetMaterial()->GetRadlen();
    myPoint += step;
  }
  return distInGe;
}

void AgataDetectorConstruction::GetDistInGe( G4ThreeVector point1, G4ThreeVector point2, G4int nSteps )
{
  G4cout << " Equivalent distance in Ge between the selected points is " << 
            DistanceInGe(point1, point2, nSteps)/mm << " mm." << G4endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// This method calculates the volume of the germanium detectors with a Monte Carlo technique.
//////////////////////////////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::CalculateGeVolume( G4int numberOfEvent, G4ThreeVector size )
{
  numHits = new G4int[theConstructed->GetMaxDetectorIndex()+1];
  for( G4int ij=0; ij<theConstructed->GetMaxDetectorIndex()+1; ij++ )
    numHits[ij] = 0;

  G4int  totalHits = 0;
  
  G4double xSize = size.x();
  G4double ySize = size.y();
  G4double zSize = size.z();
  
  G4double boxVolume = xSize * ySize * zSize / numberOfEvent;
  
  G4ThreeVector      myPoint;
  G4VPhysicalVolume* myVolume;
  G4int              detCode;
  G4VPhysicalVolume* topVolume;
  
  G4TouchableHistoryHandle theTouchable;
  
  
  G4RunManager*         runManager = G4RunManager::GetRunManager();
  AgataRunAction*       theRun     = (AgataRunAction *)      runManager->GetUserRunAction();

  G4bool                doWrite      = theRun->DoWrite();
  G4bool                doAnalysis   = theRun->GetTheAnalysis()->IsEnabled();
  // to avoid conflicts, disable write to lmd file and spectra
  if( doWrite )
    theRun->EnableWrite( false );
  if( doAnalysis )
    theRun->GetTheAnalysis()-> EnableAnalysis( false ); 
  
  runManager->BeamOn(0);  // this is needed to make this action "equivalent" to a normal run
                          // and to avoid overwriting the output file

  G4int                 runNumber    = theRun->GetRunNumber();
  theRun->SetRunNumber( runNumber ); // this is needed to make this action "equivalent" to a normal run
                                     // and to avoid overwriting the output file
                                     // the method takes care of disabling the output of list mode
                                     // and spectra while making the fake run!
  
  G4Navigator* theNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  
  theNavigator->SetWorldVolume(hallPhys);       
  
  G4int depth = geSD->GetDepth();

  G4int prec1 = 1;
  char filename[32];
  std::ofstream outFile;
  if( doWrite ) {
    sprintf(filename, "GammaVolume.%4.4d", runNumber);
    outFile.open(filename);
    G4cout << " File " << filename << " opened" << G4endl;
    WriteHeader(outFile);
    prec1 = outFile.precision(4);
    outFile.setf(ios::fixed);
  }  
  
  for( G4int i=0; i<numberOfEvent; i++ ) {
    // periodic printing
    if ( i < 100 || i%500 == 0)  {
      printf( " >>> Ev.# %6d\r", i );
      G4cout.flush(); 
    }
    
    myPoint.setX( size.x() * ( 1.0*G4UniformRand() - 0.5 ) );
    myPoint.setY( size.y() * ( 1.0*G4UniformRand() - 0.5 ) );
    myPoint.setZ( size.z() * ( 1.0*G4UniformRand() - 0.5 ) );
    
    myVolume = theNavigator->LocateGlobalPointAndSetup(myPoint);
    
    if( myVolume->GetLogicalVolume()->GetMaterial()->GetName() == "Germanium" 
        && myVolume->GetLogicalVolume()->GetSensitiveDetector() != NULL ) {  // must consider only the active parts!
    
      theTouchable = theNavigator->CreateTouchableHistoryHandle();
    
      topVolume = theTouchable->GetVolume(depth);
      
      detCode = topVolume->GetCopyNo();	
	
      numHits[detCode%1000]++;
      totalHits++;
    }
  }    
  G4cout << G4endl;
    
  G4double totDet = totalHits * boxVolume;

  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);

  for( G4int ii=0; ii<theConstructed->GetMaxDetectorIndex()+1; ii++ ) {
    G4cout << " Detector# " << std::setw(4) << ii
                            << std::setw(12) << boxVolume * numHits[ii]/cm3 << " cm3" << G4endl;
                            
    if( doWrite )
      outFile << " Detector# " << std::setw(4) << ii
                               << std::setw(12) << boxVolume * numHits[ii]/cm3 << " cm3" << G4endl;
  }
  G4cout << " Total volume of the detectors is " << std::setw(8) << totDet/cm3 << " cm3 " << G4endl;
  if( doWrite )
    outFile << " Total volume of the detectors is " << std::setw(8) << totDet/cm3 << " cm3 " << G4endl;
    
  G4double density;
  G4Material* ptMaterial = G4Material::GetMaterial("Germanium");  
  
  if( ptMaterial )
    density = ptMaterial->GetDensity();
   
  
  G4cout << " Total mass of the detectors is " << std::setw(8) << totDet * density / kg << " kg " << G4endl;   
  if( doWrite )
    outFile << " Total mass of the detectors is " << std::setw(8) << totDet * density / kg << " kg " << G4endl;   
  
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);

  if( doWrite ) {
    outFile.unsetf(ios::fixed);
    outFile.precision(prec1);
    outFile.close();
    G4cout << " File " << filename << " closed" << G4endl;
  }
  theRun->SetRunNumber( runNumber + 1 ); 
  if( doWrite )
    theRun->EnableWrite( true );
  if( doAnalysis )
    theRun->GetTheAnalysis()-> EnableAnalysis( true ); 
}

void AgataDetectorConstruction::IncrementHits( G4int index )
{
  if( index > theConstructed->GetMaxDetectorIndex() )
    return;
  if( index < theConstructed->GetMinDetectorIndex() )
    return;
  numHits[index]++;
  return;      
}

// uncomment if AgataSteppingAction is being used
// #include "AgataSteppingAction.hh"

/////////////////////////////////////////////////////////////////////////////////////
/// This method calculates the solid angle covered by germanium detectors.
/// Geantinos are used for that!
/////////////////////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::CalculateGeSolidAngle( G4int numberOfEvent )
{
  calcOmega = true;

  numHits = new G4int[theConstructed->GetMaxDetectorIndex()+1];
  for( G4int ij=0; ij<theConstructed->GetMaxDetectorIndex()+1; ij++ )
    numHits[ij] = 0;


  G4RunManager * runManager = G4RunManager::GetRunManager();
  AgataGeneratorAction* theGenerator = (AgataGeneratorAction*) runManager->GetUserPrimaryGeneratorAction();
  AgataRunAction*       theRun       = (AgataRunAction *)      runManager->GetUserRunAction();

// uncomment if AgataSteppingAction is being used
//  AgataSteppingAction* theStepping = (AgataSteppingAction*) runManager->GetUserSteppingAction();
  G4int                 runNumber    = theRun->GetRunNumber();
  G4bool                doWrite      = theRun->DoWrite();
  G4bool                doAnalysis   = theRun->GetTheAnalysis()->IsEnabled();
  // to avoid conflicts, disable write to lmd file and spectra
  if( doWrite )
    theRun->EnableWrite( false );
  if( doAnalysis )
    theRun->GetTheAnalysis()-> EnableAnalysis( false ); 
  

  AgataGeneratorOmega* newGenerator = new AgataGeneratorOmega();
  runManager->SetUserAction( newGenerator );
  AgataSteppingOmega* newStepping = new AgataSteppingOmega();
  runManager->SetUserAction( newStepping );
  

  G4int prec1 = 1;
  char filename[32];
  std::ofstream outFile;
  if( doWrite ) {
    sprintf(filename, "GammaSolidAngle.%4.4d", runNumber);
    outFile.open(filename);
    G4cout << " File " << filename << " opened" << G4endl;
    WriteHeader(outFile);
    prec1 = outFile.precision(4);
    outFile.setf(ios::fixed);
  }  

  runManager->BeamOn(numberOfEvent);
  
  G4double*  omega;
  omega = new G4double[theConstructed->GetMaxDetectorIndex()+1];
  G4double omegaTot = 0.;
  
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  
  for( G4int ii=0; ii<theConstructed->GetMaxDetectorIndex()+1; ii++ ) {
    omega[ii] = 100. * (G4double) numHits[ii] / (G4double)numberOfEvent;
    omegaTot += omega[ii];
    G4cout << " Detector# " << std::setw(4) << ii
                            << std::setw(12) << omega[ii] << " %" << G4endl;
    if( doWrite )
      outFile << " Detector# " << std::setw(4) << ii
                               << std::setw(12) << omega[ii] << " %" << G4endl;
  }
  G4cout << " Total solid angle: " << std::setw(10) << omegaTot << " %" << G4endl;
  if( doWrite )
    outFile << " Total solid angle: " << std::setw(10) << omegaTot << " %" << G4endl;

  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);

  if( doWrite ) {
    outFile.unsetf(ios::fixed);
    outFile.precision(prec1);
    outFile.close();
    G4cout << " File " << filename << " closed" << G4endl;
  }
  
  theRun->SetRunNumber( runNumber+1 );
  if( doWrite )
    theRun->EnableWrite( true );
  if( doAnalysis )
    theRun->GetTheAnalysis()-> EnableAnalysis( true ); 
  delete newGenerator;
  runManager->SetUserAction( theGenerator );  
// uncomment if AgataSteppingAction is being used
// runManager->SetUserAction( theStepping );
// delete newStepping;
  calcOmega = false;
}

////////////////////////////////////////////////
//// Management of the Offset Lookup Table 
////////////////////////////////////////////////
G4int AgataDetectorConstruction::GetOffset( G4int index )
{
  if( (index < 0) || (index >  (G4int)offsetLut.size()) )
    return -1;
  else
    return offsetLut[index];  
}

void AgataDetectorConstruction::SetOffset( G4int index, G4int value )
{
  if( (index < 0) || (index >= (G4int)offsetLut.size()) )
    offsetLut.push_back(value);
  else
    offsetLut[index] = value;  
}

void AgataDetectorConstruction::AddOffset( G4int value )
{
  offsetLut.push_back(value);
}

void AgataDetectorConstruction::CopyOffset( std::vector<G4int>& origValue )
{
  offsetLut.resize( origValue.size() );
  for( G4int ii=0; ii<(G4int)(G4int)offsetLut.size(); ii++ )
    offsetLut[ii] = origValue[ii];
}

void AgataDetectorConstruction::ResetOffset()
{
  offsetLut.clear();
}

void AgataDetectorConstruction::PrintOffset()
{
  for( G4int ii=0; ii<(G4int)offsetLut.size(); ii++ )
    G4cout << " Offset[" << ii << "] = " << offsetLut[ii] << G4endl;
}

G4int AgataDetectorConstruction::HowManyOffset()
{
  return (G4int)offsetLut.size();
}

