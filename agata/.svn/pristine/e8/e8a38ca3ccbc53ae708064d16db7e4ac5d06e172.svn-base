#include <fstream>
#include <iomanip>
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include "Decay.hpp"

using namespace std;

int main(int argc, char **argv)
{
  int NumOfFragments;
  char incoming_infile[] = "../IncomingFragments.asc";
  char outgoing_infile[] = "../OutgoingFragments.asc";
  char crosssectionFile[] = "../CrossSections.dat";
  char incoming_outfile[] = "IncomingAGATAEventFile.asc";
  char outgoing_outfile[] = "OutgoingAGATAEventFile.asc";
   int primaryEvents;
  double rannum, rannumTrg;
  int SavePoint = 0;
  int FragmentNum = 0;
  float FragmentXpos = 0.0;         // in cm
  float FragmentXangle = 0.0;      // in mrad
  float FragmentYpos = 0.0;         // in cm
  float FragmentYangle = 0.0;      // in mrad
  float FragmentEnergy = 0.0;     // in AMeV
  float FragmentTime = 0.0;         // in microseconds
  float FragmentMass = 0.0;       // in amu
  float FragmentCharge = 0.0;     // Z
  float FragmentElectrons = 0.0;
  float FragmentNSF = 0.0;
  float Dummy = 0.0;

  float FragmentP2tot=0.0;
  float FragmentPx=0.0;
  float FragmentPy=0.0;
  float FragmentPz=0.0;
  float amu=931.494;
  float FragmentZpos=0.0;
  float TrgThickness=0.0;


  time_t tt;
  int i,j,k,m,q;
  int counter = 0;
  int frag_counter = 0;
  double sum;

  struct tm *timeinfo;

  if(argc !=4 ) {
    cout << "usage: applyx_v2 <Number of fragments species> <Number incoming fragments> <Target thickness (in micrometer) >" << endl;
    return 0;
  }

  NumOfFragments = atoi(argv[1]);
  primaryEvents = atoi(argv[2]);
  TrgThickness = atof(argv[3]);

  
  double crossSection[NumOfFragments];

  // Read in cross sections from a file
  ifstream crossfile(crosssectionFile, ios::in);
  if(!crossfile){
      cout << "Can't open " << crosssectionFile << " for reading !" << endl;
      return 0;
    } else {
	sum = 0;
	for(k=0;k<NumOfFragments;k++) {
		crossfile >> crossSection[k];
		sum += crossSection[k];
	}
	cout << k << " Cross sections read in" << endl;
}
  crossfile.close();

 // cout << "Sum = " << sum << endl;

  ifstream ifile_outgoing(outgoing_infile, ios::in);
  ifstream ifile_incoming(incoming_infile, ios::in);
  ofstream ofile_incoming(incoming_outfile, ios::out);
  ofstream ofile_outgoing(outgoing_outfile, ios::out);


  if(!ifile_outgoing || !ifile_incoming){
      cout << "Can't open input files for reading !" << endl;
      return 0;
    }

  // Choose the seed of random number generator as the running time 

  time(&tt);
  srand((unsigned) tt);

  timeinfo = localtime (&tt);

  counter = 0;

  // setup GSL random number generator	
  gsl_rng_env_setup();
  gsl_rng *rng = gsl_rng_alloc(gsl_rng_default);

 // Open decay list file:
   ifstream decay_list("list.decay", ios::in);
   string decay_fname;


  // Header for the AGATA external event file  
  ofile_outgoing << "FORMAT 0 4" << "\n" ;  
  ofile_outgoing << "#_____________________________________________________________________\n";
  ofile_outgoing << "#\n";
  ofile_outgoing << "#              Ascii Events File with AGATA format                    \n";
  ofile_outgoing << "#\n";
  ofile_outgoing << "# File generated on " << asctime(timeinfo) << "#\n"; 
  ofile_outgoing << "# via XsecApply_Agata.cpp which couples MOCADI outputs with           \n";
  ofile_outgoing << "# production X-sections external file\n";
  ofile_outgoing << "#_____________________________________________________________________\n";
  ofile_outgoing << "EMITTED 1 1\n" ;  // only the Gamma will be transported by Geant4
  ofile_outgoing << "$\n" ;  // only the Gamma will be transported by Geant4

  // Loop over the different fragments in the input file
  for(j=0;j<NumOfFragments;j++)
    {
     frag_counter = 0;

     // Read the decay file associated to this fragment 		
		decay_list >> decay_fname;
		cout << "j= " << j << " decay file=" <<  decay_fname << endl;

		ifstream decay_in( decay_fname.c_str(), ios::in);

		//Decay *decay= new Decay(decay_in);					
		Decay decay(decay_in);					
		decay_in.close();
		

     for(i=0;i<primaryEvents;i++)   // number of event for each fragment (same for all fragment)
	{
	  // Read the line content of file ifile_outgoing
	  ifile_outgoing >> SavePoint >> FragmentNum >> FragmentXpos >> FragmentXangle >> FragmentYpos >> FragmentYangle >> FragmentEnergy >> FragmentTime >> FragmentMass >> FragmentCharge >> FragmentElectrons >> FragmentNSF >> Dummy >> Dummy >> Dummy >> Dummy ;

	  // Calculate total momentum:

	  FragmentP2tot= pow((FragmentEnergy+FragmentMass*amu),2) - pow(FragmentMass*amu,2);

	  FragmentPz= sqrt(FragmentP2tot)*cos(FragmentXangle/1000.)*cos(FragmentYangle/1000.);  // angle unit are changed from mrad to rad

	  // unitary vector momentum:
	  FragmentPx= sqrt( (FragmentP2tot*pow(cos(FragmentXangle/1000.),2)) - pow(FragmentPz,2)) / sqrt(FragmentP2tot);
	  FragmentPy= sqrt( (FragmentP2tot*pow(cos(FragmentYangle/1000.),2)) - pow(FragmentPz,2)) / sqrt(FragmentP2tot);
	  FragmentPz= FragmentPz/sqrt(FragmentP2tot);


	 rannumTrg = (double) (rand()/(RAND_MAX+1.0));
	 FragmentZpos= (TrgThickness*(rannumTrg-0.5))/10000; // Z Position of interaction in target (/10000 to convert in centimeter)

	 rannum = (double) (rand()/(RAND_MAX+1.0));
  
	
 	 if(rannum < (crossSection[j]/sum))
	  {
	   frag_counter++;

	   ofile_outgoing << "-101 " << FragmentCharge << "  " << int(FragmentMass+0.5) << " " << FragmentEnergy*int(FragmentMass+0.5) << " " << FragmentPx << "  " << FragmentPy << "  " << FragmentPz << "  " << FragmentXpos << "  " << FragmentYpos << "  " << FragmentZpos << std::endl;


		decay.excite("exciter");
		// choose a decaytime randomly according to the lifetime of the state !
		int ID;
		double dt, Tau, Ecm;
		decay.wait_decay(dt,Ecm, ID, rng); // first time is for the exciter
		//decay.wait_decay_tau(Tau,Ecm, ID); // first time is for the exciter

//			while( decay.wait_decay_tau(Tau,Ecm, ID) ) // and again for the excited states
			while( decay.wait_decay(dt,Ecm, ID, rng) ) // and again for the excited states
			{
			  ofile_outgoing << "1 " <<  Ecm << " " << dt << "\n"; // Format 0 4 -> gamma energy in CM  and 
		    }
			ofile_outgoing << "$\n";
    	               

	   for(q=0;q<11;q++) {
	     ifile_incoming >> SavePoint >> FragmentNum >> FragmentXpos >> FragmentXangle >> FragmentPx >> FragmentYangle >> FragmentEnergy >> FragmentTime >> FragmentMass >> FragmentCharge >> FragmentElectrons >> FragmentNSF >> Dummy >> Dummy >> Dummy >> Dummy;

	     ofile_incoming << SavePoint << "  " << FragmentNum << "  " << FragmentXpos << "  " << FragmentXangle << "  " << FragmentYpos << "  " << FragmentYangle << "  " << FragmentEnergy << "  " << FragmentTime << "  " << FragmentMass << "  " << FragmentCharge << "  " << FragmentElectrons << "  " << FragmentNSF << "  " << Dummy << "  " << Dummy << "  " << Dummy << "  " << Dummy << "\n";
	   } 
	    counter++;
	  }
	  else {
	   for(q=0;q<11;q++) {
	     // Read the next 11 line content of file ifile_incoming
	     ifile_incoming >> SavePoint >> FragmentNum >> FragmentXpos >> FragmentXangle >> FragmentYpos >> FragmentYangle >> FragmentEnergy >> FragmentTime >> FragmentMass >> FragmentCharge >> FragmentElectrons >> FragmentNSF >> Dummy >> Dummy >> Dummy >> Dummy;
	   }
	  }
      	 }
	 cout << "Fragment " << FragmentNum << " = " << frag_counter << " events" << endl;
    } 


  decay_list.close();


  cout << " A total of " << counter << " events have been generated for the input of the AGATA code" << endl;

  ifile_outgoing.close();
  ifile_incoming.close();

  //ofile_incoming.close();
  ofile_outgoing.close();

  return 0;
}
