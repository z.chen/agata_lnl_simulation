////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the generation of random directions (in the centre of mass)
/// according to the calculations of G.Pollarolo
////////////////////////////////////////////////////////////////////////////////////////

#ifndef AngDistHandler_h
#define AngDistHandler_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>
//#include <ios>
//#include <iostream>
//#include <istream>
#include <fstream>

using namespace std;

class AngDistHandler
{
  public:
    AngDistHandler( G4String );
    ~AngDistHandler();
    
  private:
    void ReadDistribution(std::ifstream& inFile);  
    
  public:
    void ReNew           (G4String);  
    
  public:
    G4double      GetEnergy   ( );
    G4ThreeVector GetDirection( G4double );
    
  public:
    void          SetPhiRange ( G4double, G4double );
    void          SetThRange  ( G4double, G4double );
    
  private:
    void InitData( G4double, G4double, G4double, G4double, G4double, G4double );    

  // range in energy
  private:
    double E_min;
    double E_max;
    double E_dif;
    
  // range in angle
  private:
    double th_min;
    double th_max;
    double th_dif;
    
  // phi range for emission
  private:
    double phi_min;
    double phi_dif;
    
  // theta range for emission
  private:
    double the_min;
    double the_dif;

  private:
    G4bool goodDistribution;

  private:
    G4bool fixedTheta;

  private:
    std::vector<G4double> refSigma;
    std::vector<G4double> refSigIn;
    G4double maxSigma;
    
  // angular distribution data in differential and integral form  
  private:
    std::vector<std::vector<double> > difAngDist;  
    std::vector<std::vector<double> > intAngDist; 
    std::vector<double>               eneDifDist; 
    std::vector<double>               eneIntDist; 

  private:
    std::vector<G4double> rawEner;
    std::vector<G4double> rawThet;
    std::vector<G4double> rawSigm;
    std::vector<G4double> mulEner;
    std::vector<G4double> mulThet;
    std::vector<G4double> mulSigm;
    std::vector<G4double> intSigm;

  private:
    G4double  chosenTheta;
    G4double  chosenEnergy;

  public:
    void      SelectTheta();

    
  public:
    inline G4bool IsGoodDistribution() { return goodDistribution; };
    inline G4double GetTheta()  { return chosenTheta; };
    inline G4double GetSigma(G4int ii)  { return mulSigm[ii]; };    
    inline G4double GetSize()  { return mulSigm.size(); };    


};

#endif
