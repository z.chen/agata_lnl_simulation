#ifndef PHYSICS_GEOMETRY_VECTOR_HPP
#define PHYSICS_GEOMETRY_VECTOR_HPP

#include <iostream>
#include <iomanip>
#include <cmath>

//#include <boost/static_assert.hpp>

namespace math
{

namespace geometry
{

// N dimensional vector
template<unsigned N, class T = double>
class vector
{
	public:
		// Constructor creates an unity vector in direction n
		// if (n == -1) the 0-vector will be created
		explicit vector(int n = -1);
		vector(const vector<N,T> &v);
		explicit vector(T x0);
		vector(T x0, T x1);
		vector(T x0, T x1, T x2);
		vector(T x0, T x1, T x2, T x3);
		vector(T x0, T x1, T x2, T x3, T x4);
		
		T* ptr();
		const T* prt() const;

		// index access
		T& operator[](size_t n);
		const T& operator[](size_t n) const;
		
		// scalar product
		T operator*(const vector<N,T> &w);
		vector<N,T>& operator*=(const T &f);
		vector<N,T>& operator/=(const T &f);
		vector<N,T>& operator+=(const vector<N,T> &v2);
		vector<N,T>& operator-=(const vector<N,T> &v2);
		
		// vector product in 3 dimensions
		vector<3,T>& operator^=(const vector<3,T> &w)
		{
			vector<3,T> v_(*this);
			int i = 0, j = 1, k = 2;
			for (unsigned i = 0; i < 3; ++i, ++j, ++k)
				v[i] = v_.v[j%3]*w.v[k%3] - v_.v[k%3]*w.v[j%3];
			return *this;	
		}


	private:
		T v[N];
};

template<unsigned N, class T>
vector<N,T> operator+(const vector<N,T> &v1, const vector<N,T> &v2);
template<unsigned N, class T>
vector<N,T> operator-(const vector<N,T> &v1, const vector<N,T> &v2);
template<unsigned N, class T>
vector<N,T> operator*(const vector<N,T> &v, const T &f);
template<unsigned N, class T>
vector<N,T> operator*(const T &f, const vector<N,T> &v);
template<unsigned N, class T>
vector<N,T> operator/(const vector<N,T> &v, const T &f);

template<class T>
vector<3,T> operator^(const vector<3,T> &v1, const vector<3,T> &v2);
template<unsigned N, class T>
T operator*(const vector<N,T> &v1, const vector<N,T> &v2);

template<unsigned N, class T>
std::ostream& operator<<(std::ostream &out, const vector<N,T> &v);


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


template<unsigned N, class T>
vector<N,T>::vector(int n)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
			
	if (n >= 0 && static_cast<unsigned>(n) < N)
		v[n] = T(1);
}

template<unsigned N, class T>
vector<N,T>::vector(const vector<N,T> &v2)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] = v2.v[i];
}

template<unsigned N, class T>
vector<N,T>::vector(T x0)
{
//	BOOST_STATIC_ASSERT(N == 1);
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
	v[0] = x0;
}
template<unsigned N, class T>
vector<N,T>::vector(T x0, T x1)
{
//	BOOST_STATIC_ASSERT(N == 2);
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
	v[0] = x0;
	v[1] = x1;
}
template<unsigned N, class T>
vector<N,T>::vector(T x0, T x1, T x2)
{
//	BOOST_STATIC_ASSERT(N == 3);
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
	v[0] = x0;
	v[1] = x1;
	v[2] = x2;
}
template<unsigned N, class T>
vector<N,T>::vector(T x0, T x1, T x2, T x3)
{
//	BOOST_STATIC_ASSERT(N == 4);
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
	v[0] = x0;
	v[1] = x1;
	v[2] = x2;
	v[3] = x3;
}
template<unsigned N, class T>
vector<N,T>::vector(T x0, T x1, T x2, T x3, T x4)
{
//	BOOST_STATIC_ASSERT(N == 5);
	for (unsigned i = 0; i < N; ++i)
		v[i] = T();	
	v[0] = x0;
	v[1] = x1;
	v[2] = x2;
	v[3] = x3;
	v[4] = x4;
}


template<unsigned N, class T>
T* vector<N,T>::ptr()
{
	return v;
}

template<unsigned N, class T>
const T* vector<N,T>::prt() const
{
	return v;
}

template<unsigned N, class T>
T& vector<N,T>::operator[](size_t n)
{
	return v[n];
}

template<unsigned N, class T>
const T& vector<N,T>::operator[](size_t n) const
{
	return v[n];
}

template<unsigned N, class T>
T vector<N,T>::operator*(const vector<N,T> &w)
{
	T result = T();
	for (size_t i = 0; i < N; ++i)
		result += v[i] * w[i];
	return result;
}

template<unsigned N, class T>
vector<N,T>& vector<N,T>::operator*=(const T &f)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] *= f;
	return *this;
}

template<unsigned N, class T>
vector<N,T>& vector<N,T>::operator/=(const T &f)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] /= f;
	return *this;
}

template<unsigned N, class T>
vector<N,T>& vector<N,T>::operator+=(const vector<N,T> &v2)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] += v2[i];
	return *this;
}

template<unsigned N, class T>
vector<N,T>& vector<N,T>::operator-=(const vector<N,T> &v2)
{
	for (unsigned i = 0; i < N; ++i)
		v[i] -= v2[i];
	return *this;
}



template<unsigned N, class T>
vector<N,T> operator+(const vector<N,T> &v1, const vector<N,T> &v2)
{
	vector<N,T> ret(v1);
	ret += v2;
	return ret;
}

template<unsigned N, class T>
vector<N,T> operator-(const vector<N,T> &v1, const vector<N,T> &v2)
{
	vector<N,T> ret(v1);
	ret -= v2;
	return ret;
}

template<unsigned N, class T>
vector<N,T> operator*(const vector<N,T> &v, const T &f)
{
	vector<N,T> ret(v);
	ret *= f;
	return ret;
}

template<unsigned N, class T>
vector<N,T> operator*(const T &f, const vector<N,T> &v)
{
	vector<N,T> ret(v);
	ret *= f;
	return ret;
}

template<unsigned N, class T>
vector<N,T> operator/(const vector<N,T> &v, const T &f)
{
	vector<N,T> ret(v);
	ret /= f;
	return ret;
}

template<class T>
vector<3,T> operator^(const vector<3,T> &v1, const vector<3,T> &v2)
{
	vector<3,T> ret(v1);
	ret ^= v2;
	return ret;
}

template<unsigned N, class T>
T operator*(const vector<N,T> &v1, const vector<N,T> &v2)
{
	T ret = T();
	for (unsigned i = 0; i < N; ++i)
		ret += v1[i] * v2[i];
	return ret;	
}


template<unsigned N, class T>
std::ostream& operator<<(std::ostream &out, const vector<N,T> &v)
{
	const int precision = 4;
	const int power = pow(10,precision);
	const int width = precision + 3;

	out << "(";
	for (unsigned i = 0; i < N-1; ++i)
		out << std::setw(width) << std::setprecision(precision) << round(power*v[i])/power << ",";
	out << std::setw(width) << std::setprecision(precision) << round(power*v[N-1])/power << ")";
	return out;
}


} // namespace geometry

} // namespace math

#endif

