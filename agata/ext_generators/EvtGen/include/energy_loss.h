#ifndef ENERGY_LOSS_H
#define ENERGY_LOSS_H

#include <vector>

#include "ExpSetup.hpp"

extern std::vector<int> beta_histogram;

void energy_loss(ExpSetup &setup, double beta0, double &z, double &t, double z_max, double t_max, double &beta_final);

#endif

