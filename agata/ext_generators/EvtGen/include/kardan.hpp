#ifndef PHYSICS_GEOMETRY_KARDAN_HPP
#define PHYSICS_GEOMETRY_KARDAN_HPP

// generalized Kardan angles
//
// konversion from a set of N*(N-1)/2 angles to a SO(N) matrix 
// konversion from a SO(N) matrix to a set of N*(N-1)/2 angles
//

#include <iostream>
#include <iomanip>

#include "vector.hpp"
#include "matrix.hpp"

namespace math
{

namespace geometry
{

template<unsigned N>
geometry::matrix<N,N> kardan_matrix(const geometry::vector<N*(N-1)/2> &phi);

template<unsigned N>
geometry::matrix<N,N> kardan_matrix_inverse(const geometry::vector<N*(N-1)/2> &phi);

template<unsigned N>
geometry::vector<N*(N-1)/2> kardan_angles(geometry::matrix<N,N> m);






template<unsigned N>
geometry::matrix<N,N> kardan_matrix(const geometry::vector<N*(N-1)/2> &phi)
{
	geometry::matrix<N,N> m;
	
	unsigned idx = 0;
	geometry::matrix<N,N> d;
	
	for (unsigned i = 0; i < N-1; ++i)
		for (unsigned j = i+1; j < N; ++j)
			m = m * d.rotation(i,j, phi[idx++]);
	
	return m;	
}

template<unsigned N>
geometry::matrix<N,N> kardan_matrix_inverse(const geometry::vector<N*(N-1)/2> &phi)
{
	geometry::matrix<N,N> m;
	
	unsigned idx = 0;
	geometry::matrix<N,N> d;
	
	for (unsigned i = 0; i < N-1; ++i)
		for (unsigned j = i+1; j < N; ++j)
			m = d.rotation(i,j, -phi[idx++]) * m;
	
	return m;	
}

template<unsigned N>
geometry::vector<N*(N-1)/2> kardan_angles(geometry::matrix<N,N> m)
{
	geometry::vector<N*(N-1)/2> phi;

	int idx = N*(N-1)/2 - 1;
	geometry::matrix<N,N> d;
	
	for (unsigned i = N-2; i >= 0; --i)
		for (int j = N-1; j >= i+1; --j)
		{
			phi[idx] = atan2(m[i][j], m[j][j]);
			m = m * d.rotation(j,i, phi[idx--]);
		}	
	
	return phi;		
}



} // namespace geometry


} // namespace math

#endif


