#ifndef PHYSICS_GEOMETRY_MATRIX_HPP
#define PHYSICS_GEOMETRY_MATRIX_HPP

#include <iostream>
#include <iomanip>
#include <cmath>
#include "vector.hpp"


namespace math
{

namespace geometry
{

// matrix with M rows
//         and N columns
//
// index operator(i) returns the i-th row vector
// index operator[j] returns the j-th column vector
//
template<unsigned M, unsigned N, class T = double>
class matrix
{
	public:
		explicit matrix(int m = -1, int n = -1);
		matrix(const matrix<M,N,T> &m);
		
		T* ptr();
		const T* prt() const;

		unsigned rows();
		unsigned columns();

		// index access to rows
		vector<M,T>& operator[](size_t j);
		const vector<M,T>& operator[](size_t j) const;

		// index access to columns
		vector<N,T> operator()(size_t j) const;
		
		template<unsigned N2>
		matrix<M,N2,T> operator*(matrix<N,N2,T> m2);
		
		matrix<M,N,T> &operator=(const matrix<M,N,T> &m);
		matrix<M,N,T> &operator*=(const T &f);
		matrix<M,N,T> &operator/=(const T &f);
		matrix<M,N,T> &operator+=(const matrix<M,N,T> &m2);
		
		matrix<N,M,T> transpose() const;
		
		matrix<M,N,T> &rotation(unsigned a, unsigned b, const double &phi);
		matrix<M,N,T> &rotation(vector<N,T> r);

	private:
		vector<M,T> mat[N]; // N row vectors
};

template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator+(const matrix<M,N,T> &m1, const T &f);
template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator*(const matrix<M,N,T> &m, const T &f);
template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator*(const T &f, const matrix<M,N,T> &m);
template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator/(const matrix<M,N,T> &m, const T &f);

template<unsigned M, unsigned N, class T>
std::ostream& operator<<(std::ostream &out, const matrix<M,N,T> &mat);

template<unsigned M, unsigned N, class T>
vector<M,T> operator*(const matrix<M,N,T> &m, const vector<N,T> &v);
//template<unsigned M, unsigned N, class T>
//vector<N,T> operator*(const vector<N,T> &v, const matrix<M,N,T> &m);

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////


template<unsigned M, unsigned N, class T>
matrix<M,N,T>::matrix(int m, int n)
{
	for (size_t j = 0; j < N; ++j)
		mat[j] = vector<M>();
			
	if (m < 0 && n < 0)
	{
		for (size_t i = 0; i < std::min(M,N); ++i)
			mat[i][i] = T(1);
	}
	else if (m >= 0 && n < 0)
	{
		for (size_t j = 0; j < N; ++j)
			mat[j][m] = T(1);
	}
	else if (m < 0 && n >= 0)
	{
		for (size_t i = 0; i < M; ++i)
			mat[n][i] = T(1);
	}
	else 
		mat[n][m] = T(1);
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T>::matrix(const matrix<M,N,T> &m)
{
	for (unsigned j = 0; j < N; ++j)
		mat[j] = m.mat[j];
}

template<unsigned M, unsigned N, class T>
T* matrix<M,N,T>::ptr()
{
	return &(mat[0][0]);
}
template<unsigned M, unsigned N, class T>
const T* matrix<M,N,T>::prt() const
{
	return &(mat[0][0]);
}

template<unsigned M, unsigned N, class T>
unsigned matrix<M,N,T>::rows()
{
	return M;
}
template<unsigned M, unsigned N, class T>
unsigned matrix<M,N,T>::columns()
{
	return N;
}
		
template<unsigned M, unsigned N, class T>
vector<M,T>& matrix<M,N,T>::operator[](size_t j)
{
	return mat[j];
}

template<unsigned M, unsigned N, class T>
const vector<M,T>& matrix<M,N,T>::operator[](size_t j) const
{
	return mat[j];
}

template<unsigned M, unsigned N, class T>
vector<N,T> matrix<M,N,T>::operator()(size_t i) const
{
	vector<N,T> ret;
	for (unsigned j = 0; j < N; ++j)
		ret[j] = mat[j][i];
	return ret;	
}


template<unsigned M, unsigned N, class T>
template<unsigned N2>
matrix<M,N2,T> matrix<M,N,T>::operator*(matrix<N,N2,T> m2)
{
	matrix<M,N2,T> res;
	for (unsigned i = 0; i < M; ++i)
		for (unsigned j = 0; j < N2; ++j)
		{
			res[j][i] = 0;
			for (unsigned n = 0; n < N; ++n)
				res[j][i] += (*this)[n][i] * m2[j][n];
		}
	return res;		
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::operator=(const matrix<M,N,T> &m)
{
	for (unsigned j = 0; j < N; ++j)
		mat[j] = m.mat[j];
	return *this;
}
template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::operator*=(const T &f)
{
	for (unsigned j = 0; j < N; ++j)
		mat[j] *= f;
	return *this;
}
template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::operator/=(const T &f)
{
	for (unsigned j = 0; j < N; ++j)
		mat[j] /= f;
	return *this;
}
template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::operator+=(const matrix<M,N,T> &m2)
{
	for (unsigned j = 0; j < N; ++j)
		mat[j] += m2.mat[j];
	return *this;
}

template<unsigned M, unsigned N, class T>
matrix<N,M,T> matrix<M,N,T>::transpose() const
{
	matrix<N,M,T> ret;
	for (unsigned i = 0; i < M; ++i)
		for (unsigned j = 0; j < N; ++j)
			ret.mat[j][i] = mat[i][j];
	return ret;			
}


template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::rotation(unsigned a, unsigned b, const double &phi)
{
//	BOOST_STATIC_ASSERT(M == N);
	
	double cos_phi = cos(phi);
	double sin_phi = sin(phi);
	
	for (unsigned i = 0; i < M; ++i)
		for (unsigned j = 0; j < N; ++j)
			mat[j][i] = (i==j)?T(1):(0);
			
	mat[a][a] =  cos_phi;	mat[b][a] = -sin_phi;
	mat[a][b] =  sin_phi;	mat[b][b] =  cos_phi;
	
	return *this;
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T> &matrix<M,N,T>::rotation(vector<N,T> r)
{
//	BOOST_STATIC_ASSERT(M == N);
//	BOOST_STATIC_ASSERT(M == 3);

	T phi = sqrt(r*r);
	if (phi > 1e-10)
	{
		r /= phi;
		T cp = cos(phi);
		T c = 1 - cp;
		T s = sin(phi);
		T &n0 = r[0];
		T &n1 = r[1];
		T &n2 = r[2];
		mat[0][0] = n0*n0*c + cp;	mat[1][0] = n0*n1*c - n2*s;	mat[2][0] = n0*n2*c + n1*s;
		mat[0][1] = n1*n0*c + n2*s;	mat[1][1] = n1*n1*c + cp;	mat[2][1] = n1*n2*c - n0*s;
		mat[0][2] = n2*n0*c - n1*s;	mat[1][2] = n2*n1*c + n0*s;	mat[2][2] = n2*n2*c + cp;
	}
	else	
	{
		for (size_t i = 0; i < 3; ++i)
			for (size_t j = 0; j < 3; ++j)
				mat[j][i] = 0;
				
		for (size_t i = 0; i < 3; ++i)
			mat[i][i] = 1;
	}
	return *this;
}




template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator+(const matrix<M,N,T> &m1, const matrix<M,N,T> &m2)
{
	matrix<M,N,T> ret(m1);
	ret += m2;
	return ret;
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator*(const matrix<M,N,T> &m, const T &f)
{
	matrix<M,N,T> ret(m);
	ret *= f;
	return ret;
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator*(const T &f, const matrix<M,N,T> &m)
{
	matrix<M,N,T> ret(m);
	ret *= f;
	return ret;
}

template<unsigned M, unsigned N, class T>
matrix<M,N,T> operator/(const matrix<M,N,T> &m, const T &f)
{
	matrix<M,N,T> ret(m);
	ret /= f;
	return ret;
}


template<unsigned M, unsigned N, class T>
std::ostream& operator<<(std::ostream &out, const matrix<M,N,T> &mat)
{
	for (size_t j = 0; j < N; ++j)
		out << '[' << j << ": " << mat(j) << ']' << std::endl;
	return out;
}


template<unsigned M, unsigned N, class T>
vector<M,T> operator*(const matrix<M,N,T> &m, const vector<N,T> &v)
{
	vector<M,T> ret;
	for (unsigned i = 0; i < M; ++i)
	{
		ret[i] = 0;
		for (unsigned j = 0; j < N; ++j)
			ret[i] += m[j][i]*v[j];
	}		
	return ret;
}

/*template<unsigned M, unsigned N, class T>
vector<N,T> operator*(const vector<M,T> &v, const matrix<M,N,T> &m)
{
	vector<N,T> ret;
	for (unsigned i = 0; i < M; ++i)
		ret += v[i] * m[i] ;
	return ret;
}
*/


} // namespace geometry

} // namespace math


#endif

