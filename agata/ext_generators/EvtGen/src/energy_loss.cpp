#include "energy_loss.h"

#include <cmath>

#include <gsl/gsl_odeiv.h>
#include <gsl/gsl_errno.h>

#include "ExpSetup.hpp"

double min(double a, double b)
{
	if (a < b)
		return a;
	return b;
}

// stuff for the beta histogramming
std::vector<int> beta_histogram(1000);
bool near(double a, double b, double eps=1)
{
	return abs(a-b) < eps;
}
// end of stuff for the beta histogramming

struct Params
{
	enum{t_integration, z_integration} mode;
	int Ap, Zp; 
	ExpSetup *setup;
};

// z is the depth in the target ( in um )
// y[0] is beta
// y[1] is time in the nuclear rest system or the position z of the nucleus (depending on params->mode)
int bethe_bloch_func (double z, const double y[], double f[], void *params)
{
	Params p = *((Params*)params);
	
	const double c = 299.792458; // speed of light in micrometer per picosecond
	const double m_e = 0.511 ; // electon mass times c^2 in MeV
	const double Na = 6.022e23; // mol^(-1)
	const double alpha = 7.297e-3; // fine structure constant
	const double hbarc = 197.0e-9; // hbar * c  (in MeV * um)
	
	int ln;
	if (p.mode == Params::z_integration)
		ln = p.setup->layer_number(z);
	else
		ln = p.setup->layer_number(y[1]); // in case of time integration y[1] contians the z-position	
	
	int At, Zt;
	double rho_t;
	if (ln == -1)
	{
		// if we are beyond all the defined layers, assume to be in vacuum
		At = Zt = 1;
		rho_t = 0;
	}
	else
	{
		// else read layer properties
		At = p.setup->layers[ln].A;
		Zt = p.setup->layers[ln].Z;
		rho_t = p.setup->layers[ln].density;
	}
	
	double I = 16e-6 * pow(Zt, 0.9); // mean excitation potential in MeV
	double beta = y[0];
	double beta2 = beta*beta;
	double ombeta2 = 1.0 - beta2;
	double gamma = 1.0 / sqrt(1.0 - beta*beta);
	double m_p = p.Ap * 931.0; // projectile mass times c^2 in MeV
	

	// electron density n in target (in um^(-3)). 
	// Conversion of mass density by multiplying: 1 = 1e-12 cm^3/um^3
	double n = Zt * rho_t * 1e-12 / At * Na; 
	
	// Bethe-Bloch formula
	double dEdx = - (4.0*M_PI / m_e) * n * (p.Zp*p.Zp / beta2) * alpha*alpha * hbarc * hbarc
			      * ( log((2.0 * m_e * beta2)/(I*(1-beta2))) - beta2 );
	if (dEdx > 0)
		dEdx = 0;		      

	switch(p.mode)
	{
		case Params::z_integration:
			// conversion to a ODE in beta ( i.e.  dbeta/dx )
			// change in beta ( in um^(-1) )
			f[0] = dEdx * sqrt( ombeta2 * ombeta2 * ombeta2 ) / (beta*m_p);
			f[1] = 1.0 / (c*beta*gamma); // f[1] contains d t / d x
			break;
		case Params::t_integration:
			// conversion to a ODE in beta ( i.e.  dbeta/dt )
			// change in beta ( in um^(-1) )
			f[0] = dEdx * gamma * c * sqrt( ombeta2 * ombeta2 * ombeta2 ) / m_p ;
			f[1] = c*beta*gamma; // f[1] contains d x / d t
			break;
	}		


	if (f[0] > 0) 
		f[0] = 0;
		
	if (f[1] < 0)
		f[1] = 0;

//	if (beta <= 0)
//		std::cerr << " beta = " << beta << " f[0] = " << f[0] << " f[1] = " << f[1] << std::endl;
	
	return GSL_SUCCESS;
}

void energy_loss(ExpSetup &setup, double beta0, double &z, double &t, double z_max, double t_max, double &beta_final)
{
	bool beta_written[3] = {false,false,false};
	Params params;
	params.setup = &setup;
	// choose the correct projectile parameters
	// from the setup structure
	if (setup.fragmented)
	{
		params.Ap = setup.Ae;
		params.Zp = setup.Ze;
	}
	else
	{
		params.Ap = setup.Ap;
		params.Zp = setup.Zp;
	}
	
	const gsl_odeiv_step_type * T = gsl_odeiv_step_rk2;
	static gsl_odeiv_step * s = gsl_odeiv_step_alloc (T, 2);
	static gsl_odeiv_control * c = gsl_odeiv_control_y_new (0.0, 1e-4);
	static gsl_odeiv_evolve * e = gsl_odeiv_evolve_alloc (2);
	
	if (t_max < 0 && z_max > 0) // integrate over z
	{
		// std::cerr << "z integration" << std::endl;
		params.mode = Params::z_integration;
		gsl_odeiv_system sys = {bethe_bloch_func, 0, 2, &params};
		double y[2] = {beta0, t};
		double delta_z = 10; // um
		unsigned ln = setup.layer_number(z);
		while (z < z_max)
		{
			// beta histogramming
			if (near(z,0) && !beta_written[0]) {++beta_histogram[y[0]*1000]; beta_written[0] = true;}
			if (near(z,108) && !beta_written[1]) {++beta_histogram[y[0]*1000]; beta_written[1] = true;}
			if (near(z,108+6000+503) && !beta_written[2]) {++beta_histogram[y[0]*1000]; beta_written[2] = true;}
			// end of beta histogramming

			// z_next contains the end of the current layer or z_max.
			// If we are already behind the last layer, it contains z_max.
			double z_next = z_max;
			if (ln < setup.layers.size())
				z_next = min(z_max, setup.layers[ln].integrated_thickness);

			while (z < z_next)
				gsl_odeiv_evolve_apply(e, c, s, &sys, &z, z_next, &delta_z, y);

			++ln;
		}	
		beta_final = y[0];
		t = y[1];
	}
	else if (z_max < 0 && t_max > 0) // integrate over t
	{
		// std::cerr << "t integration" << std::endl;
		params.mode = Params::t_integration;
		gsl_odeiv_system sys = {bethe_bloch_func, 0, 2, &params};
		double y[2] = {beta0, z}; // y[1] contains z position!!!
		double delta_t = 0.033; // ps
		int current_ln = setup.layer_number(z);
		while (t < t_max)
		{
			// beta histogramming
			if (near(y[1],0) && !beta_written[0]) {++beta_histogram[y[0]*1000]; beta_written[0] = true;}
			if (near(y[1],108) && !beta_written[1]) {++beta_histogram[y[0]*1000]; beta_written[1] = true;}
			if (near(y[1],108+6000+503) && !beta_written[2]) {++beta_histogram[y[0]*1000]; beta_written[2] = true;}
			// end of beta histogramming
			double y0_old = y[0];
			double y1_old = y[1];
			double delta_t_old = delta_t;
			double t_old = t;
			gsl_odeiv_evolve_apply(e, c, s, &sys, &t, t_max, &delta_t, y);
			if (current_ln < setup.layer_number(y[1]) && delta_t_old > 0.3)
			{
				// if the algorithm juped over the end of a layer, set it back
				// to the last point and a small step size
				y[0] = y0_old;
				y[1] = y1_old;
				t = t_old;
//				if (setup.layers[current_ln].density == 0)
//				{
//					const double c = 299.792458; // speed of light in micrometer per picosecond
//					double gamma = 1.0/sqrt(1.0-y[0]*y[0]);
//					delta_t = (setup.layers[current_ln].integrated_thickness - y1_old)/(gamma * y0_old * c);
//				}	
//				else
				delta_t = 0.033;
			}
//			std::cout << " ---> " << y[1] << " " << y[0] << std::endl;
		}	
		beta_final = y[0];
		z = y[1];
	}
	else
	{
		std::cerr << "error: either z_max or t_max has to be < 0" << std::endl;
		std::cerr << "       and either z_max or t_max has to be > 0" << std::endl;
		return;
	}
	
	if (beta_final < 0)
		beta_final = 0;
}


