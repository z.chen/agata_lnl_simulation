#include "Layer.hpp"

Layer::Layer(std::istream &in) 
	: integrated_thickness(0), integrated_fragmentation_prob(0) 
{
	in >> A >> Z >> density >> thickness >> fragmentation_prob >> angular_straggling >> reaction_type;
	
	if (!in)
	{
		std::cerr << "could not read layer" << std::endl;
	}
}

Layer::Layer() 
	: integrated_thickness(0), integrated_fragmentation_prob(0)
{
	A = Z = 1;
	density = 0;
	thickness = 100;
	fragmentation_prob = 0;
	angular_straggling = 0;
	reaction_type = 'C';
}

