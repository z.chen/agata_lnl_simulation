#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include "Decay.hpp"
#include "ExpSetup.hpp"
#include "energy_loss.h"
#include "kardan.hpp"


using namespace std;


void transform(double &phi, double &theta, double phi_p, double theta_p, double phi_g, double theta_g)
{
	math::geometry::vector<3> v_g(0,0,1);

	math::geometry::matrix<3,3> M_y, M_z;
	M_y.rotation(0,2, -theta_g);
	M_z.rotation(0,1, phi_g);
	
	math::geometry::matrix<3,3> M_y2;
	math::geometry::matrix<3,3> M_z2;
	M_y2.rotation(0,2, -theta_p);
	M_z2.rotation(0,1, phi_p);

	math::geometry::vector<3> v_lab = M_z2 * M_y2 * M_z * M_y * v_g;
	
	phi = atan2(v_lab[1],v_lab[0]);
	theta = acos(v_lab[2]);
}

int main(int argc, char *argv[])
{
	if (argc != 3)
	{
		std::cerr << "usage: " << argv[0] << " experimental_setup_file  output_file  " << std::endl;
		return 1;
	}
	
	ExpSetup setup(argv[1]);
	setup.layers.push_back(Layer());

	
	if (setup.layers.size() < 2)
	{	
		std::cerr << "error: no layers defined" << std::endl;
		return 1;
	}
	
/*	int Np;
	std::istringstream Np_in(argv[3]);
	Np_in >> Np;
	if (!Np_in)
	{
		std::cerr << "error: cannot read number of projectiles from 2nd command line argument: " << argv[2] << std::endl;
		return 1;
	}
*/
	// setup GSL random number generator	
	gsl_rng_env_setup();
	gsl_rng *rng = gsl_rng_alloc(gsl_rng_default);


	// added for tranfer reactions :
	bool ReadXsec=false;
	int XsecSize = 1;
	double *CrossSection;
	double *Ang;

	try
	{
		
	  // Read decay file:
		std::cout << setup.decay_filename << std::endl;
		std::ifstream decay_in(setup.decay_filename.c_str());
		Decay decay(decay_in);
		decay_in.close();
	
		std::ofstream out(argv[2]);
		if (!out)
		{
			std::cerr << "error: cannot create file " << argv[2] << std::endl;
			return 1;
		}

		int loop_i=0;

		out << "FORMAT 0 0\n";
		out << "REACTION     "<< setup.Zp << " " << setup.Ap << " " << setup.Ztg << " " << setup.Atg << " " << setup.E0*setup.Ap  << "\n";
		//out << "REACTION     29    75    26    56 166.2\n";
		out << "EMITTED 1 1 \n";
		
		std::vector<int> beta_hist(100000,0);
		std::vector<int> theta_hist(100000,0);	
		std::vector<int> ChargeState_hist(100000,0);

		double vx_3 = 0.;
		double vy_3 = 0.;
		double vz_3 = 0.;
		double vx_4 = 0.;
		double vy_4 = 0.;
		double vz_4 = 1.;
		double vx_4s = 0.; // if smear
		double vy_4s = 0.; // if smear
		double vz_4s = 1.; // if smear
		double EnergieLab3 = 0.;
		double EnergieLab4 = 0.;
		double EnergieLab3_smear = 0.;
		double EnergieLab4_smear = 0.;


		// start simulation
		for (int n = 0; n < setup.NumberProjectiles; ++n)
		{
			if (n%1000 == 0){
				std::cout << n << std::endl;
				std::cout << "//////////////" << std::endl;
			}
			setup.fragmented = false;

			// choose starting position and energy 
			double x0 = setup.x0 + gsl_ran_gaussian(rng, setup.deltax);
			double y0 = setup.y0 + gsl_ran_gaussian(rng, setup.deltay);
			double E0 = setup.E0 + gsl_ran_gaussian(rng, setup.deltaE);

			// determine the layer where the fragmentation takes place;
			double x = gsl_rng_uniform(rng);
			unsigned reaction_layer; // the index to the layer where the fragmentation takes place
			for (reaction_layer = 0; reaction_layer < setup.layers.size(); ++reaction_layer)
				if (x < setup.layers[reaction_layer].integrated_fragmentation_prob)
					break;
		
		
			// calculate the relativ fragmentation position in the fragmentation layer
			// (relativ to the layer thickness)
			double depth_fraction = x;
			if (reaction_layer > 0)
			{
				depth_fraction -= setup.layers[reaction_layer-1].integrated_fragmentation_prob;
				depth_fraction /= (setup.layers[reaction_layer].integrated_fragmentation_prob - setup.layers[reaction_layer-1].integrated_fragmentation_prob);
			}
			else
				depth_fraction /= setup.layers[reaction_layer].integrated_fragmentation_prob;

			// calculate abolute position of the fragmentation process
			double fragmentation_position = setup.layers[reaction_layer].integrated_thickness - setup.layers[reaction_layer].thickness * (1-depth_fraction);

			cout <<"Beam energy: " << E0*setup.Ap << endl;
		
			//double mass = setup.Ap * 930.0;  // Why 930 ? -> now 931.5
			double mass = setup.Ap * 931.5;
			double gamma0 = (E0 * setup.Ap)/mass + 1.0; // pour particules emise au seuil.
			double beta0 = sqrt(1.0-1.0/(gamma0*gamma0));
			cout << "beta0: "<< beta0 << endl;
			double beta;		

			double z = 0; // projectile position along the beam direction
			double t = 0; // proper time of the projectile (time in rest frame of the projectile)
		
			// integrate energy loss formula forward in space to the fragmentation position
			energy_loss(setup, beta0, z,t, fragmentation_position, -1.0, beta);
			//energy_loss(setup, beta0, z,t, fragmentation_position, -1.0, beta_smear);
			cout << "beta0_energyloss: "<< beta << endl;

			// Let's recalculate beam energy from new beta at reation point in target:
			gamma0=1/sqrt(1-beta*beta);
			E0=(gamma0-1)*mass/setup.Ap;
			cout <<"New Beam energy: " << E0*setup.Ap << endl;


			// now we have the fragmentation position (= position of excitation)
			// do the reaction!
			// calculate collision kinematics
			switch (setup.layers[reaction_layer].reaction_type)
			{
				case 'c': // coulex
					 // do nothing ( which is the wrong thing )
					 // (not yet implemented)
				break;

				case 'C': // inverse coulex 
					// do nothing : ejectile flys with velocity of projectile
					// maybe one could implement a small angular distribution in the future
				break;

				case 'k': // knock out : ejectile flys with velocity of projectile
				{
					double mp = 931.5*setup.Ap; // projectile mass *c^2
					double me = 931.5*setup.Ae; // ejectile mass 
//					double mt = 931.5*setup.layers[reaction_layer].A; // target mass
					double gamma = 1.0/sqrt(1.0-beta*beta);
					double Ef = mp * (gamma-1.0) * me/mp;

					// Relative change of velocity (Borrel et al.)
					double beta_change = sqrt(1.0 - ((setup.Bn*(setup.Ap-setup.Ae))/Ef));
					beta *= beta_change;
				}
				break;

				case 'f': // fusion evaporation : ejectile flys with center of mass velocity of 
						  // projectile and target.
				{
					double mp = 931.5*setup.Ap;
					double mt = 931.5*setup.layers[reaction_layer].A;
					double mpt = mp+mt;
	
					double beta_p2 = beta*beta; // projektile velocity
					double beta_pt2 = mp*mp * beta_p2/(mpt*mpt*(1-beta_p2) + mp*mp * beta_p2); // CM velocity squared

					beta = sqrt(beta_pt2); // CM velocity
				}	
				break;

				case 'T': // Transfer reaction in inverse kinematics: ejectile (recoil) energy depens on ejectile angle 
						
				{
				  //double mp = 931.5*setup.Ap;
				  //double mt = 931.5*setup.layers[reaction_layer].A;

				  // Here we assume flat distribution for cos(theta):
				  // ThetaCM= acos(gsl_rng_uniform(rng)*2.0 - 1.0);
					
				  // Here we randomly choose ThetaCM from the DWBA cross section distribution:
				  // Read (only once) the Cross-section file for transfer reaction:
				  if(!ReadXsec)
				    {
				      ifstream Xsec_in;
				      Xsec_in.open(setup.Xsec_filename.c_str());
				      if(Xsec_in.is_open()) 
					{ cout << "Reading Cross Section File " <<  setup.Xsec_filename << endl;
					ReadXsec=true;
					}
				      else {cout << "Cross Section File " <<  setup.Xsec_filename << " not found" << endl;}

				      double AngleCM, Xsec;
				      double XsecFinal;
				      vector<double> XsecBuffer;
				      vector<double> AngleBuffer;
				      while(!Xsec_in.eof())
					{
					  Xsec_in >> AngleCM;
					  Xsec_in >> Xsec;
					  XsecFinal=Xsec*sin(AngleCM*M_PI/180.);
					  XsecBuffer.push_back(XsecFinal);
					  AngleBuffer.push_back(AngleCM);
					}
				      Xsec_in.close();

				      XsecSize = XsecBuffer.size();
				      cout << "XSecSize= " << XsecSize << endl;
				      Ang = new double[XsecSize] ;
				      CrossSection = new double[XsecSize] ;
				      for(int i = 0 ; i <XsecSize ; i++ ) 
					{
					  CrossSection[i] = XsecBuffer[i];
					  Ang[i]= AngleBuffer[i];
					}
				    }
				      

					gsl_ran_discrete_t *g=gsl_ran_discrete_preproc(XsecSize,CrossSection);
					double ThetaCM= gsl_ran_discrete(rng,g);
					double ThetaCM_smear= 0.;
					//cout << " gsl_ran_discrete " << ThetaCM << endl;
					//cout << " last angle (max) " <<  Ang[XsecSize-1] << endl;
					ThetaCM= ThetaCM*Ang[XsecSize-1]/(XsecSize-1);
					cout << "ThetaCM1="<< ThetaCM<< endl;
					ThetaCM= ThetaCM*M_PI/180.; // in radians

					double BeamEnergy= E0*setup.Ap; // E0 has been recalculated taking energy loss in target into account
					double Excitation= setup.Ex;
				
					//
					//Taken from NPTool:
					//

				        double  m1 = 931.5*setup.Ap + setup.Xsp; // projectile
					double  m2 = 931.5*setup.Atg + setup.Xstg; // target
					double  m3 = 931.5*setup.Ale + setup.Xsle; // light ejectile
					double  m4 = 931.5*setup.Ae + setup.Xse + Excitation; // Recoil

					//cout<< "m1=" << m1 << " m2=" << m2 << " m3=" << m3 << " m4=" << m4 << endl;
					//cout<< "BeamEnergy=" << BeamEnergy << endl;

					// center-of-mass velocity
					double WtotLab = (BeamEnergy + m1) + m2;
					double WtotLab_smear = 0.;

					double P1 = sqrt(pow(BeamEnergy,2) + 2*m1*BeamEnergy);
					double B = P1 / WtotLab;
					cout <<"B=P1/WtotLab =" << B << endl;
					double G = 1 / sqrt(1 - pow(B,2));
					double B_smear = 0.;
					double G_smear = 0.;

					// total energy of the ejectiles in the center-of-mass
					double W3cm = (pow(WtotLab,2) + pow(G,2)*(pow(m3,2) - pow(m4,2)))
					       / (2 * G * WtotLab);
					double W4cm = (pow(WtotLab,2) + pow(G,2)*(pow(m4,2) - pow(m3,2)))
					       / (2 * G * WtotLab);
					double W3cm_smear = 0.;
					double W4cm_smear = 0.;

					// velocity of the ejectiles in the center-of-mass
					double beta3cm  = sqrt(1 - pow(m3,2)/pow(W3cm,2));
					double beta4cm  = sqrt(1 - pow(m4,2)/pow(W4cm,2));
					double beta3cm_smear  = 0.;
					double beta4cm_smear  = 0.;

					//cout << "W3cm=" << W3cm << " W4cm=" << W4cm << endl;

					//cout << "beta3cm=" << beta3cm << " beta4cm=" << beta4cm << endl;

					// Constants of the kinematics
					double K3 = B / beta3cm;
					double K4 = B / beta4cm;
					double K3_smear = 0.;
					double K4_smear = 0.;


					 //Added by Marc for Carl, assuming resolution is dominated by TRACE
					 if(setup.AddRes){					

					  WtotLab_smear= WtotLab+gsl_ran_gaussian(rng, (setup.NrjRes/2.35)/1000.); // This has no impact
			
					  B_smear = P1 / WtotLab_smear;
					  G_smear = 1 / sqrt(1 - pow(B_smear,2));
						cout << "WtotLab: "<< WtotLab<< endl;	
						cout << "WtotLab_smear: "<< WtotLab_smear<< endl;	
						cout << "B: "<< B << endl;	
						cout << "B_smear: "<< B_smear<< endl;	
						cout << "G: "<< G << endl;	
						cout << "G_smear: "<< G_smear<< endl;	
					  // total energy of the ejectiles in the center-of-mass
					  W3cm_smear = (pow(WtotLab_smear,2) + pow(G_smear,2)*(pow(m3,2) - pow(m4,2)))
					       / (2 * G * WtotLab_smear);
					  W4cm_smear = (pow(WtotLab_smear,2) + pow(G_smear,2)*(pow(m4,2) - pow(m3,2)))
					       / (2 * G * WtotLab_smear);

					  // velocity of the ejectiles in the center-of-mass
					  beta3cm_smear  = sqrt(1 - pow(m3,2)/pow(W3cm_smear,2));
					  beta4cm_smear  = sqrt(1 - pow(m4,2)/pow(W4cm_smear,2));

					  //cout << "beta3cm=" << beta3cm << " beta4cm=" << beta4cm << endl;

					  // Constants of the kinematics
					  K3_smear = B_smear / beta3cm_smear;
					  K4_smear = B_smear / beta4cm_smear;

					 } 


					// 2-body relativistic kinematics: direct + inverse
					// EnergieLab3,4 : lab energy in MeV of the 2 ejectiles
					// ThetaLab3,4   : angles in rad

					// case of inverse kinematics
					if (m1 > m2) ThetaCM = M_PI - ThetaCM;

					// lab angles
					double PhiLab3 = 2*M_PI * gsl_rng_uniform(rng);
					double PhiLab4 = PhiLab3 + M_PI;
					double ThetaLab3 = atan(sin(ThetaCM) / (cos(ThetaCM) + K3) / G);
					double ThetaLab3_smear =0.;

					 //Added by Marc for Carl, assuming resolution is dominated by TRACE
					 // In order to calculate the impact of the resolution in TRACE: 
					 if(setup.AddRes){	
					   ThetaLab3_smear =  ThetaLab3 +  gsl_ran_gaussian(rng, (setup.AngRes/2.35)*M_PI/180.);
					   cout << "ThetaLab3: "<< ThetaLab3*180/M_PI << endl;
					   cout << "ThetaLab3_smear: "<< ThetaLab3_smear*180/M_PI << endl;
					   // recalculate thetaCM after smearing so that one can calculate the inpact on the recoil
					   ThetaCM_smear= atan(sin(ThetaLab3_smear) / (cos(ThetaLab3_smear)- K3_smear) /G_smear); // This works (has been checked)
					   cout << "ThetaCM_smear: "<< ThetaCM_smear*180/M_PI << endl;
					   // case of inverse kinematics
					   if (m1 > m2) ThetaCM_smear = M_PI + ThetaCM_smear; // This works
					   // recalculate thetaLab3 with
					   ThetaLab3_smear = atan(sin(ThetaCM_smear) / (cos(ThetaCM_smear) + K3_smear) / G_smear);
					   cout << "ThetaCM: "<< ThetaCM*180/M_PI << endl;
					   cout << "ThetaCM_smear: "<< ThetaCM_smear*180/M_PI << endl;
					   cout << "ThetaLab3_smear: "<< ThetaLab3_smear*180/M_PI << endl;
					   cout << "K3_smear: "<< K3_smear << endl;
					   cout << "K4_smear: "<< K4_smear << endl;
					   cout << "G_smear: "<< G_smear << endl;
					 }
 
					//cout << "ThetaLab3:" << ThetaLab3 << " K3=" << K3 << endl;
					if (ThetaLab3 < 0) ThetaLab3 += M_PI;
					double ThetaLab4 = atan(sin(M_PI + ThetaCM) / (cos(M_PI + ThetaCM) + K4) / G);
					double ThetaLab4_smear = 0.;
					   cout << "ThetaLab4: "<< ThetaLab4*180/M_PI << endl;

					 // Added by Marc for Carl  !! 
					 if(setup.AddRes){
					  if (ThetaLab3_smear < 0) ThetaLab3_smear += M_PI;
					  //ThetaLab4_smear= atan(sin(M_PI + ThetaCM_smear) / (cos(M_PI + ThetaCM_smear) + K4_smear) / G_smear); //
					    ThetaLab4_smear= ThetaLab4 + gsl_ran_gaussian(rng, (setup.AngRes/2.35)*M_PI/180.); // add resolution directly to ThetaLab4
					   cout << "ThetaLab4_smear: "<< ThetaLab4_smear*180/M_PI << endl;
					 }

					if (fabs(ThetaLab3) < 1e-6) ThetaLab3 = 0;
					ThetaLab4 = fabs(ThetaLab4);
					if (fabs(ThetaLab4) < 1e-6) ThetaLab4 = 0;
					vx_3 = sin(ThetaLab3)*cos(PhiLab3);
				        vy_3 = sin(ThetaLab3)*sin(PhiLab3);
				        vz_3 = cos(ThetaLab3);
				        vx_4 = sin(ThetaLab4)*cos(PhiLab4);
				        vy_4 = sin(ThetaLab4)*sin(PhiLab4);
				        vz_4 = cos(ThetaLab4);

					 // Added by Marc for CarlW  !! 
					 if(setup.AddRes){

					  if (fabs(ThetaLab3_smear) < 1e-6) ThetaLab3 = 0;
					  ThetaLab4_smear = fabs(ThetaLab4_smear);
					  if (fabs(ThetaLab4_smear) < 1e-6) ThetaLab4_smear = 0;

 				          vx_4s = sin(ThetaLab4_smear)*cos(PhiLab4);
				          vy_4s = sin(ThetaLab4_smear)*sin(PhiLab4);
				          vz_4s = cos(ThetaLab4_smear);
 					 } 

					// total and kinetic energies in the lab
					double W3lab = W3cm * G * (1 + B*beta3cm*cos(ThetaCM));
					double W4lab = W4cm * G * (1 + B*beta4cm*cos(ThetaCM + M_PI));
					// test for total energy conversion
					if (fabs(WtotLab - (W3lab+W4lab)) > 1e-6) 
					  std::cout << "Problem for energy conservation" << endl;
					EnergieLab3 = W3lab - m3;
					EnergieLab4 = W4lab - m4;


					//cout <<"XXXXXXXXXXXXXXXXXXXXXX" << endl;
					//cout <<"ThetaCM:" << ThetaCM*180/M_PI << endl;
					//cout <<"EnergieLab3:" << EnergieLab3 << endl;
					//cout <<"EnergieLab4:" << EnergieLab4 << endl;
					//cout <<"W4lab:" << W4lab << endl;
					double beta4lab= sqrt(W4lab*W4lab - m4*m4)/W4lab; // the recoiling heavy ion 
					beta=beta4lab;
					
					 // Added by Marc for Carl  !! 
					 if(setup.AddRes){
					  double W3lab_smear = W3cm_smear * G * (1 + B_smear*beta3cm_smear*cos(ThetaCM_smear));
					  double W4lab_smear = W4cm_smear * G * (1 + B_smear*beta4cm_smear*cos(ThetaCM_smear + M_PI));
					  // test for total energy conversion
					  if (fabs(WtotLab_smear - (W3lab_smear+W4lab_smear)) > 1e-3) 
					     std::cout << "Problem for energy conservation: "  << fabs(WtotLab_smear - (W3lab_smear+W4lab_smear)) << ">1e-3" << endl;
					  EnergieLab3_smear = W3lab_smear - m3;
					  EnergieLab4_smear = W4lab_smear- m4;
					  double beta4lab_smear= sqrt(W4lab_smear*W4lab_smear - m4*m4)/W4lab_smear; // the recoiling heavy ion 
					  //beta_smear=beta4lab_smear;
					  //cout <<"beta1:" << beta << endl;
					 }

				}
	
				break;

				default:
					std::cerr << "unknown reaction type" << std::endl;	
			} // end of switch



			// excite the fragment!
			decay.excite("exciter");
			setup.fragmented = true; // this is for the energy loss formula to 
						 // recognize that the flying particle is now the fragment
		
			double dt, dE;
			int ID;
			decay.wait_decay(dt,dE, ID, rng);

			bool loop= false;

			//std::cerr << dt << " " << dE << " " << ID << std::endl;
			while(decay.wait_decay(dt,dE, ID, rng))
			{
			     //cout << "dt: " << dt << " dE: " << dE <<  " Id: " << ID << endl;
			     loop_i++;

				// integrate the energy loss formula in time
				// to the deexcitation time (determined randomly by wait_decay)
				t = 0;
				energy_loss(setup, beta, z,t, -1.0, dt, beta);
				
				// calculate the amount of angular straggling caused by the targets
				double straggling_theta = 0;
				int ln = setup.layer_number(z);
				double sum_z = 0;
				for (int i = 0; i < ln; ++i)
				{
					straggling_theta += setup.layers[i].thickness*setup.layers[i].angular_straggling;
					sum_z += setup.layers[i].thickness;
				}
				straggling_theta += (z - sum_z) * setup.layers[ln].angular_straggling;
				straggling_theta = gsl_ran_gaussian(rng, straggling_theta*0.001); // 0.001 to go from rad to millirad
				double straggling_phi = rand()*2.0*M_PI/RAND_MAX;
				int theta_pos = abs(straggling_theta*theta_hist.size()/M_PI);
				//std::cerr << "theta = " << theta_pos << std::endl;				
				if (theta_pos < theta_hist.size())
					++theta_hist[theta_pos];
				

//				std::cout << fragmentation_position << "  " <<  z << "  " << beta << std::endl;
				// if something went wrong during integration of stopping
				// omit this nucleus and go to the next one
				if (beta < 0. || beta >= 1.)
				{
					std::cout << "wrong" << std::endl; 
					break;
				}
				// keep track of the velocities at emission point
				//std::cerr << "fill beta histogram " << beta << std::endl;
				++beta_hist[beta*beta_hist.size()];
				//std::cerr << beta_hist[beta*beta_hist.size()] << std::endl;
			
				// caluclate kinetic energy of the fragment at deexcitation point
				double gamma = 1.0/sqrt(1-beta*beta);
				double Eemit = 931.5 * setup.Ae * (gamma - 1); // 930 -> 931.5
				//cout <<"Eemit:" << Eemit << endl;
			
				// create uniform distribution of gammas in space 
				double phi = 2*M_PI * gsl_rng_uniform(rng);
				double theta = acos(gsl_rng_uniform(rng)*2.0 - 1.0);
			
				// Lorentz boost!!
				double theta_boosted = acos((cos(theta)+beta)/(1.0+beta*cos(theta)));
				double dE_boosted = dE / (gamma*(1.0 - beta*cos(theta_boosted)));


				// add angular straggling from the projectile to the emitted gamma
				transform(phi,theta_boosted,   straggling_phi,straggling_theta,  phi,theta_boosted);

			
				// direction of the emitted gamma in cartesian coordinates
				double vx_gamma = sin(theta_boosted)*cos(phi);
				double vy_gamma = sin(theta_boosted)*sin(phi);
				double vz_gamma = cos(theta_boosted);
			
				// "emit" gamma (i.e. write position, velocity etc. to file)
				if(!loop)
				  {
				    loop=true;

				    if(loop_i==1) // to write info of charged jectile only once per events 
				    {
				      //cout << "EnergieLab3:"<< EnergieLab3<< endl;
				      // for transfer reactions: the light ejectile should haveenergy !=0
				      if(EnergieLab3!=0)
				       {
					 if(!setup.AddRes)
					 {
					  out<<"$\n" ; // << endl;
					  // To write info of light ejectile uncomment the lines below: 
					  //    out << "-101 " << setup.Zle << " " <<  setup.Ale << " " << EnergieLab3 << " "  
					  //    << vx_3 << " " << vy_3 << " " << vz_3 << " " 
					  //    << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << std::endl;
					  // To write info of heavy ejectile uncomment the lines below: 
					      out << "-101 " << setup.Ze << " " <<  setup.Ae << " " << EnergieLab4 << " "  
					      << vx_4 << " " << vy_4 << " " << vz_4 << " " 
					      << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << std::endl;
					 }else
					  {
					   // No need to simulate light as detection resolution are already added to calulation: 
					   out<<"$\n"; // << endl;

					  //}
				            // for the heavy fragment and gamma emitter
				            if(EnergieLab4!=0)
				            {

					      if(!setup.Prisma){
					        out << "-101 " << setup.Ze << " " <<  setup.Ae << " " << Eemit << " " 
					        <<  vx_4s << " " <<  vy_4s << " " <<  vz_4s << " " // AddRes added to heavy ejectile
					        << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << "\n";
				              }else  // one add the charge state information
					       {
					         // first we choose randomly a charge state	
			                         int Zeq = setup.MeanZeq + int(gsl_ran_gaussian(rng, setup.deltaZeq));
					         // in case MeanZeq is close to setup.Ze and deltaZe is large we added the while loop below 
					         while(Zeq>setup.Ze)
					         { 
					           Zeq = setup.MeanZeq + int(gsl_ran_gaussian(rng, setup.deltaZeq));
					         }
					
					       out << "-101 " << setup.Ze << " " <<  setup.Ae << " " << Zeq << " " << Eemit << " " 
					       <<  vx_4 << " " <<  vy_4 << " " <<  vz_4 << " "
					       << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << "\n";
					       if(Zeq<ChargeState_hist.size()) ++ChargeState_hist[Zeq];
					      } //end of !setup.prisma
					   
				             } // end of EnergieLab4!=0  (tobe check if one need to close bracket here
				          } // end of AddRes

				    }else  // means Energie3Lab =0 <=> not Transfer reaction -> writing the heavy ion gamma emitter information only  
				     {
					if(!setup.Prisma){  
					  out << "$\n-101 " << setup.Ze << " " <<  setup.Ae << " " << Eemit << " " 
					      << vx_4 << " " << vy_4 << " " << vz_4 << " "
					      //<< 0.0 << " " << 0.0 << " " << 1.0 << " "
					      << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << "\n";
					}else
					 {
					  // first we choose randomly a charge state
			                  int Zeq = setup.MeanZeq + int(gsl_ran_gaussian(rng, setup.deltaZeq)); 
					  // in case MeanZeq is close to setup.Ze and deltaZe is large we added the while loop below 
					   while(Zeq>setup.Ze)
					   { 
				             Zeq = setup.MeanZeq + int(gsl_ran_gaussian(rng, setup.deltaZeq));
					   }

					  cout << "Zeq: " << Zeq << endl;
					  out << "$\n-101 " << setup.Ze << " " <<  setup.Ae << " " << Zeq << " " << Eemit << " " 
					   <<  vx_4 << " " <<  vy_4 << " " <<  vz_4 << " "
					   << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << "\n";
 
					    if(Zeq<ChargeState_hist.size()) ++ChargeState_hist[Zeq];	
                                         } // end of !setup.Prisma
					  
 				       } // end of EnergieLab3 condition

  				   } // end of loop_i==1 condition

				} // end of !loop

				// For gammas:
				out << "1 " << dE_boosted << " " 
				    << vx_gamma << " " << vy_gamma << " " << vz_gamma << " " 
				    << x0*1e-4 << " " << y0*1e-4 << " " << z*1e-4 << std::endl;
				//std::cout << "Writting in file" << std::endl;
				//		std::cout << "emission in layer number " << setup.layer_number(z) << std::endl;

			} // end of while(decay...)

			loop_i=0; //reset 		
		} // end of event main loop

		// write beta_histogram to file;
		std::ofstream bout("beta_hist.dat");
		if (!bout)
		{
			std::cerr << "cannot open file: beta_hist.dat . No beta histogram will be written" << std::endl;
		}
		else
		{
			std::cerr << "writing beta histogram " << std::endl;
			for (unsigned i = 0; i < beta_hist.size(); ++i)
				bout << i/(double)beta_hist.size() << " " << beta_hist[i] << std::endl;
		}
		
		// write theta_histogram to file;
		std::ofstream thout("theta_hist.dat");
		if (!bout)
		{
			std::cerr << "cannot open file: theta_hist.dat . No beta histogram will be written" << std::endl;
		}
		else
		{
			std::cerr << "writing theta histogram " << std::endl;
			for (unsigned i = 0; i < theta_hist.size(); ++i)
				thout << i*M_PI/(double)theta_hist.size() << " " << theta_hist[i] << std::endl;
		}

		// write ChargeState_histogram to file;
		std::ofstream qsout("qState_hist.dat");
		if (!qsout)
		{
			std::cerr << "cannot open file: qState_hist.dat . No Charge State histogram will be written" << std::endl;
		}
		else
		{
			std::cerr << "writing qState histogram " << std::endl;
			for (unsigned i = 0; i < ChargeState_hist.size(); ++i)
				qsout << i << " " << ChargeState_hist[i] << std::endl;
		}

		
	}
	catch (const std::string &message)
	{
		std::cerr << message << std::endl;
	}
	

	return 0;
}

