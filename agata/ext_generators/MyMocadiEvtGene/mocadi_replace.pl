#!/usr/bin/perl
# Program to replace the fragment parameters
# in a mocadi input file and run multiple times
# to get a correlation between the incoming
# and outgoing fragments

# TO RUN type: perl mocadi_replace.pl

# first need define an array that will hold our fragment parameters
#@fragZ = (28,26,27,16,21,20,22,19,17,18,23,21,18,17,19,20,16,24,25,16,26,17,22,18,21,19,20,20,21,19,18,22,17,16,27,23,16,24,17,23,18,25,22,19,21,20,19,20,18,17,21,16,16,26,28,22,17,23,24,18,19,23,24,20,22,21,16,16,17,18,19,17,20,25,21,18,19,22,20,27,21,23,22,24,23,25,24,26,25,26,27);
#@fragA = (53,49,51,38,40,38,42,36,40,34,44,48,42,32,44,46,30,46,48,37,50,39,49,41,47,43,45,39,41,37,35,43,33,31,52,45,36,47,38,50,40,49,48,42,46,44,38,40,36,34,42,32,35,51,54,44,37,46,51,39,41,49,48,43,47,45,34,33,35,37,39,36,41,50,43,38,40,45,42,53,44,47,46,50,48,52,49,52,51,53,54);
@fragZ = (28,26);
@fragA = (53,49);

#@fragZ = ( 24,26,25,26,27 );
#@fragA = ( 49,52,51,53,54 );

$number_of_frags = $#fragZ;    # determine how many fragments we are using

$real_frag_num = $number_of_frags + 1;

print "\n$real_frag_num fragments being generated\n\n";

system('cp TotalForG4.in mocadi_orig.in');
print "\nmocadi input file copied to mocadi_orig.in\n";

for $i ( 0 .. $number_of_frags ) {    # loop over the fragments listed

    # open the mocadi input file with original source code
    open( moc_in, "mocadi_orig.in" )
      or die "Can't open mocadi_orig.in";    # open file in read mode
    @buffer = <moc_in>;                      # read file into an array
    $lines  = $#buffer;                      # determine no of lines in buffer
    close(moc_in);                           # close input file

    # open a mocadi input file which will contain the modified source code
    open( moc_temp, ">mocadi_template.in" )
      or die
      "Can't open mocadi_template.in";    # open file in overwrite mode using >

    for $j ( 0 .. $lines ) {              # loop over the lines of code
        $buffer[$j] =~ s/A1/$fragA[$i]/;  # replace A1 with next fragment mass
        $buffer[$j] =~ s/Z1/$fragZ[$i]/;  # replace Z1 with next fragment charge
        print moc_temp ( $buffer[$j] );   # write out lines to the new file
    }
    close(moc_temp);                      # close the modified file

    system('/home/mala/MOCADI/mocadi mocadi_template.in');  # run the mocadi program
    rename( "mocadi_template.out", "mocadi_template.out$i" ); # rename the output files
    rename( "mocadi_template.asc", "mocadi_template.asc$i" );
}

print "\n\n$real_frag_num fragments have been generated\n\n";
