#ifndef DECAY_HPP
#define DECAY_HPP

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <gsl/gsl_rng.h>

class Decay
{
	struct Level
	{
		std::string name;
		double E;
		double inversetau;
		std::vector<int> decay; // destination level 
		std::vector<int> decayID;
		std::vector<double> lambda;
	};
	
	public:
		Decay(std::istream &in);
		
		// excite the level with name "name". returns true on success
		bool excite(const std::string &name);
		// return  the time from last decay to the next decay,
		//         the energy difference between initial and final level
		//         the decay ID of chosen the decay path
		// needs a gsl random number generator
		bool wait_decay(double &dt, double &E, int &ID, gsl_rng *rng);


		// return  the time of life as in input file from last decay to the next decay,
		//         the energy difference between initial and final level
		//         the decay ID of chosen the decay path
		// needs a gsl random number generator
		bool wait_decay_tau(double &dt, double &E, int &ID);

		
		// returns the name of the currently populated level
		std::string get_current();
	private:
		std::map<std::string, int> levelnames;
		std::vector<Level> levels;
		
		int groundstate;
		int currentstate;
};

#endif
