#ifndef DECAY_CPP
#define DECAY_CPP

#include <sstream>
#include <cmath>
#include <gsl/gsl_rng.h>

#include "Decay.hpp"


Decay::Decay(std::istream &in)
{
	int linenumber = 0;
	int number_decays = 0;
	
	std::string line;
	levels.push_back(Level()); // the 0th level is a dummy
	
	int cl = 0; // current level
	
	while (in)
	{
		++linenumber;
		
		std::getline(in, line);
		std::istringstream lin(line);

		std::string key;
		lin >> key;
		
		if (key == "level:")
		{
			if (cl != 0 && cl != 1 && !levels[cl].decay.size())
			{
				std::cerr << "warning: no decays defined for level "
						  << levels[cl].name << "\n";
			}
			std::string name;
			//std::cout << name << std::endl;
			double E;
			double tau;
			lin >> name >> E;
			if (E != 0.0)
				lin >> tau;
			if (!lin)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
				    << "expecting: \"level: E name tau\"" << std::endl
					<< "tau is optional for the ground state only";
				throw std::string(eout.str());
			}
			if (cl != 0 && E <= levels[cl].E)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
				    << "energy does not increase from " << levels[cl].E
				    << " to " << E;
				throw std::string(eout.str());
			}
			levels.push_back(Level());
			cl = levels.size()-1;
			levels[cl].name = name;
			levels[cl].E = E;
			levels[cl].inversetau = 1.0/tau;
			levelnames[name] = cl;
//			std::cerr << "name " << name << " " << levels[levelnames[name]].name << std::endl;
		}
		if (key == "decay:")
		{
			if (cl == 0)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
				    << "definition of a decay but no levels defined yet";
				throw std::string(eout.str());
			}
			std::string dest;
			//std::cout << dest << std::endl;
			double branching = -1;
			int ID = 0;
			lin >> dest >> branching >> ID;
			if (!lin)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": "
					 << "expecting: \"decay: destination branching decayID\"";
				throw std::string(eout.str());
			}
			if (levelnames[dest] == cl)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
					 << "level " << levels[cl].name 
					 << " cannot decay to " << dest;
				throw std::string(eout.str());
			}
			if (levelnames[dest] == 0)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
					 << "level " << dest << " unknown ";
				throw std::string(eout.str());
			}
			if (branching <= 0)
			{
				std::ostringstream eout;
				eout << "error: line " << linenumber << ": " 
					 << "bad branching ratio";
				throw std::string(eout.str());
			}
			
			
//			levels[cl].inversetau += lambda;
			levels[cl].decay.push_back(levelnames[dest]);
			levels[cl].lambda.push_back(branching);
			levels[cl].decayID.push_back(ID);
			
			//std::cerr << levels[levelnames[dest]].name << " =? " << levels[levels[cl].decay.back()].name << std::endl;
			
			++number_decays;
		}
	}

	// calculate the decay constant for each transition.
	// first step: normalize all the branchings
	// second step: divide the normalized branchings by the level lifetime
	for (unsigned l = 0; l < levels.size(); ++l)
	{
		double sum_of_branchings = 0;
		for (unsigned d = 0; d < levels[l].decay.size(); ++d)
			sum_of_branchings += levels[l].lambda[d];
		for (unsigned d = 0; d < levels[l].decay.size(); ++d)
		{
			levels[l].lambda[d] *= levels[l].inversetau / sum_of_branchings;
//			std::cerr << levels[l].decayID[d] << " " << levels[l].lambda[d] << std::endl;
		}	
	}

	if (!number_decays)
		std::cerr << "warning: no decays defined at all\n";
	
	groundstate = 1;		
	currentstate = groundstate;
}

bool Decay::excite(const std::string &name)
{
	if (levelnames[name] == 0)
	{
		//std::cout << "levelname " << name << " not found" << std::endl;
		return false;
	}
	currentstate = levelnames[name];
	return true;		
}

bool Decay::wait_decay(double &dt, double &E, int &ID, gsl_rng *rng)
{
	if (currentstate == groundstate ||
		levels[currentstate].inversetau == 0)
		return false;
		
	double r = 1.0-gsl_rng_uniform(rng);//double(rand())/double(RAND_MAX);
	dt = -log(r)/levels[currentstate].inversetau;
	double x = levels[currentstate].inversetau 
				* double(rand())/double(RAND_MAX);
	double sum_lambda = 0;
	for (unsigned int i = 0; i < levels[currentstate].lambda.size(); ++i)
	{
		sum_lambda += levels[currentstate].lambda[i];
		if (sum_lambda > x)
		{
			E = levels[currentstate].E - levels[levels[currentstate].decay[i]].E;
			ID = levels[currentstate].decayID[i];
			currentstate = levels[currentstate].decay[i];
			return true;
		}
	}
	std::cerr << "Decay::wait_decay: shouldn't happen\n";
	return false; // sollte nicht passieren;
}

bool Decay::wait_decay_tau(double &dt, double &E, int &ID)
{

	if (currentstate == groundstate ||
		levels[currentstate].inversetau == 0)
		return false;


// dt For AGATA output Format 0 0		
//	 double r = 1.0-gsl_rng_uniform(rng);//double(rand())/double(RAND_MAX);
//   dt=-log(r)/levels[currentstate].inversetau;	
// dt For AGATA output Format 0 4		
    dt = 1.0/levels[currentstate].inversetau; 
	
	double x = levels[currentstate].inversetau 
				* double(rand())/double(RAND_MAX);

	double sum_lambda = 0;
	
	for (unsigned int i = 0; i < levels[currentstate].lambda.size(); ++i)
	{
		sum_lambda += levels[currentstate].lambda[i];


		if (sum_lambda > x)
		{
			E = levels[currentstate].E - levels[levels[currentstate].decay[i]].E;
			ID = levels[currentstate].decayID[i];
			currentstate = levels[currentstate].decay[i];
			return true;
		}
	}
	
	std::cerr << "Decay::wait_decay: shouldn't happen\n";
	return true; // sollte nicht passieren;
}



std::string Decay::get_current()
{
	return levels[currentstate].name;
}

#endif
