#!/usr/bin/perl
# Program to merge mocadi output ascii files into
# incoming and outgoing fragment ascii files

# This version incorporates the fact that the number of fragments
# at the S2 position could be different to that at the S4 position

# TO RUN type: perl mocadi_merge.pl

#$last_file_index = 90;	 # set the number of files we are using 91
#$number_of_frags = 965689;  # set number of generated fragments for each species reaching PAST S2 !
$last_file_index = 1;	 # set the number of files we are using (ie:2) -1 =1
$number_of_frags = 90000;  # set number of generated fragments for each species reaching PAST S2 ( 104x2 out of 112x2 )!

$num_of_incom_sp = 11;   # number of savepoints in the mocadi input TotalForG4.in file before the last savepoint. 

# need to output files one for incoming fragments and one for outgoing

open( incoming, ">IncomingFragments.asc" )
  or die "Can't open IncomingFragments.asc";    # open in overwrite mode with >
open( outgoing, ">OutgoingFragments.asc" )
  or die "Can't open OutgoingFragments.asc";    # open in overwrite mode with >
#open ( logfile, ">Logfile.txt" )
#  or die "Can't create a log file";

#$i = 0;
for $i ( 0 .. $last_file_index ) {              # loop over each file
    print "\nProcessing file index $i\n";
    
    $inputfilename = "mocadi_template.asc$i";    # change input file name
    open( input, $inputfilename )
      or die "Can't open $inputfilename";        # open current input file

    # need to remove the header from the asc files (4 lines)
    for $j ( 0 .. 3 ) {
        $line = <input>;                         # read a line in from file
    }

    for $k ( 1 .. $number_of_frags ) {    # loop over the number of fragments
    	#print "\nprocessing fragment number $k\n";
	
        if ( $i == 0 ) {    # if on first file with fragment number 1
	# print "Using first file index\n";
	
            $badevent = 0;  # set event flag: Definition of a bad event is one that
			    # reaches SC21 but not SC41
          STARTLOOP:        # goto label here
            if ( $badevent == 1 ) {	# test to see if last event was bad
                $loopfrom = 2;		# change how many lines to read in from file
#		print logfile ("Reading next 2 lines only due to bad event\n");
            }
            else {
                $loopfrom = 1;
            }
            for $m ( $loopfrom .. 3 ) {
                $lines[$m] = <input>;	# read in 2 or 3 lines depending if last event was bad
            }
            @savepoint = split( / /, $lines[3] ); # split the 3rd line into parts

            # 	for $m (0 .. $#savepoint){
            # 	print "\n savep = $savepoint[2]\n";
            # 	}
            if ( $savepoint[2] == "3" ) {	# test if 3rd line savepoint is 3
                for $m ( 1, 2, 3 ) {
                    print incoming ( $lines[$m] );  # print the good first 3 lines to incoming file
                }
                for $m ( 4 .. $num_of_incom_sp ) {
                    $line = <input>;		# read rest of event from file
                    print incoming ($line);	# print good lines to incoming file
                }
                $line = <input>;    # read outgoing fragment parameters
                print outgoing ($line);    # write outgoing fragment parameters
#		print logfile (" ge ");

            }
            else {

                $badevent = 1;		# got a bad event
#		print logfile ("Bad event detected\n");
                goto STARTLOOP;		# redo the loop and read the next 2 lines

            }
        }
        else {  # Now that the first file is read, the next files can be read
	    #print "\nFirst file processed, onto next file index\n";
            $badevent = 0;
	    
          SL:
            if ( $badevent == 1 ) {
                $loopfrom = 2;
            }
            else {
                $loopfrom = 1;
            }
            for $m ( $loopfrom .. 3 ) {
                $line = <input>;
                substr( $line, 6, 1 ) =
                  ( $i + 1 ) . " ";    # change default fragment number
                $lines[$m] = $line;
            }
            @savepoint = split( / /, $lines[3] );
            if ( $savepoint[2] == "3" ) {
                for $m ( 1, 2, 3 ) {
                    print incoming ( $lines[$m] );
                }
                for $m ( 4 .. $num_of_incom_sp ) {
                    $line = <input>;
                    substr( $line, 6, 1 ) =
                      ( $i + 1 ) . " ";    # change default fragment number
                    print incoming ($line);  # write incoming fragment parameter
                }
                $line = <input>;             # read outgoing fragment parameters
                substr( $line, 6, 1 ) =
                  ( $i + 1 ) . " ";          # change default fragment number
                print outgoing ($line);    # write outgoing fragment parameters

            }
            else {
                $badevent = 1;
                goto SL;
            }
        }

        # 	if($k == 1)
        # 		{
        # 		print "\n$line";
        # 		}
    }
    $t = $i + 1;
    print "\nfragment $t done";
#    print logfile ("\nfragment $t done");

    close(input);
}
print "\n$t fragment species merged\n";
print "for $number_of_frags fragments\n";

close(incoming);
close(outgoing);
close(logfile);
