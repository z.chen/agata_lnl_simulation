/*
 * [bash] $ root 
 * root[0]$ .L dweikoplot.cpp+
 * root[1]$ dweikoplot()
 */  

#include "Riostream.h"
#include "TGraph.h"
#include <fstream>
#include "TAxis.h"
#include <cmath>

using namespace std;

void dweikoplot()
{
string dummy1, dummy2, data;
Int_t i=0;
Int_t start;

 ifstream dweiko_gam_out("output/dweiko_gam.out"); // ("/.../dweiko_gam.out"); //path to dweiko_gam.out file
getline(dweiko_gam_out,dummy1);
getline(dweiko_gam_out,dummy2);
start=dweiko_gam_out.tellg();

while(getline(dweiko_gam_out,data))
{
i++;
}

dweiko_gam_out.clear();
dweiko_gam_out.seekg(start);

Double_t theta[i], ww[i];
Double_t theta_max=0;

for(Int_t j=0;j<i;j++)
{
dweiko_gam_out >> theta[j] >> ww[j];
if(theta[j]>theta_max) theta_max=theta[j];
}

 cout << theta_max << endl;

 cout << int(theta_max) << endl;

Double_t x[180],y[180];

for(Int_t j=0; j<theta_max;j++)
{
x[j]=j;
y[j]=0;
}

//TGraph *gr=new TGraph(i,theta,ww);
 TGraph *gr=new TGraph(i,theta,ww);

TGraph *gr2=new TGraph(180,x,y);
gr->SetLineWidth(2);
gr->SetTitle("dweiko_gam.out");
gr->GetXaxis()->SetTitle("theta [degrees]");
gr->GetYaxis()->SetTitle("W(theta)");
gr2->SetLineWidth(0.5);
gr->Draw("AC");
gr2->Draw("P");

//for(Int_t j=0; j<i;j++) cout << theta[j] << "         " << ww[j] << endl;

dweiko_gam_out.close();
}
