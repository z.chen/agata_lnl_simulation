\documentclass[12pt,a4paper]{report}
\usepackage{graphicx}

\addtolength{\textwidth}{1cm}
\title{\textbf{MC Code for the simulation of experiments with \\AGATA at GSI}}

\begin{document}

\date{\today}


\maketitle

\tableofcontents

\thispagestyle{empty}

\chapter*{Introduction}
The full simulation is divided into three steps, \textbf{a) event
  generation}, \textbf{b) event building} or detector response simulation and
  \textbf{c) event reconstruction}. 

In the first step, a heavy ion beam
  strikes on a target and emits gamma rays. These $\gamma$-rays are emitted
  according to a certain levels and half-life scheme. 

In the second step the
  response of the detection system (for the moment only AGATA) to those gamma
  rays is simulated. 

In the third step, a very basic and simplified analysis
  is made. Here Doppler corrections are calculated, an ideal event-wise
  tracking or add-back is performed, the resolution function (in energy and
  position) of the several detectors is roughly included and raw and Doppler
  corrected spectra and other histograms are generated.

To split the simulation into these three parts has the advantage that one
can e.g. re-evaluate the response of the $\gamma$-ray detection system
(i.e. AGATA) for different geometric configurations of it, without need to
re-generate every time all the primary events. Similarly, the effect of a worse or a
better position resolution can be evaluated in the third stage, without need
to re-simulate the detector response each time.


\section*{Getting started}
To run the simulation you will need \textsc{root}~\cite{root},
\textsc{Geant4}~\cite{Geant4} and the CLHEP library~\cite{clhep} installed in
your computer.

Create a directory for the simulation work, save there the compressed file
and unpack it via \verb*+tar -xzvf mc_agata_gsi_v0.tgz+

You should obtain then a directory structure similar to this,
\begin{verbatim}
drwxr-xr-x   7 cdomingo kp2 4,0K 2009-09-21 13:01 a_event_generator
drwxr-xr-x  21 cdomingo kp2 4,0K 2009-09-22 14:00 b_event_builder
drwxr-xr-x   3 cdomingo kp2   63 2009-09-22 10:48 c_event_reconstruction
drwxr-xr-x   2 cdomingo kp2  107 2009-09-22 14:00 manual
drwxr-xr-x   5 cdomingo kp2   56 2009-09-21 13:02 SimulationResults
drwxr-xr-x   4 cdomingo kp2   26 2009-09-22 13:55 work
\end{verbatim}

The first three directories contain the three parts of the simulation code,
splitted as mentioned above. 

The directory \verb+manual+ contains this manual, both the PDF and the \LaTeX-
 versions. The idea is to update/extend the code and update this changes also
 in this manual.

\verb+SimulationResults+ will contain the results or the output from each
part of the simulation. Therefore, it is recommended to create and use a sub-directory
structure like the following,

\begin{verbatim}
cdomingo:~/simulations/agata_gsi/SimulationResults$ ls -lhtr
drwxr-xr-x  3 cdomingo kp2 17 2009-09-21 13:01 Generator
drwxr-xr-x  3 cdomingo kp2 17 2009-09-21 13:02 Builder
drwxr-xr-x  3 cdomingo kp2 17 2009-09-21 13:02 Reconstructor
\end{verbatim}

In this way, the output of each simulation step, EventGenerator, EventBuilder or
Event Reconstructor, can be saved in the corresponding directory. At the
moment only the \verb+Generator+ and \verb+Builder+ directories are being
used, thus the third stage of the simulation does not save any file.

After compilation, the executable of each \textsc{Geant4} part will be stored
in the \verb+$G4WORKDIR\bin\Linux-g+++ directory. I defined my
\verb+$G4WORKDIR+ equal to the \verb+work+ directory (see above), but this
depends on yourself.

It is convenient to include the \verb+$G4WORKDIR\bin\Linux-g+++ in the \verb+$PATH+
variable, so that the executables are accessible from any place.


\chapter{Event Generation}

\framebox[\textwidth]{This part of the code has been kindly provided by Pieter Doornenbal
(RIKEN).}\\

This chapter has been copied from Ref.~\cite{Doornenbal09}.

The Event Generator is located in the directory
\verb+b_event_generator+. Compile the Event Generator with:

\verb+make all+

It will create an executable file called  \verb+EventGenerator+ in your
\verb+$G4WORKDIR/bin/Linux-g+++ directory. Please ignore all the warning
messages.

\section{The Input File(s)}
To change the parameters of the Event Generator, open the file

\verb+./input/EventGenerator.in+

The file contains keywords, which can be
used in the free format and change the default values of the simulation. The
available keywords and their parameters are listed in
Tab.~\ref{tab:generator}. In this table, $s$, $i$, and $f$ are used for input
string, integer, and floating point values, respectively. Note that in the
current version the simulation code is very restrictive, that is, comments
and not existing input keywords in this file will cause the program to
terminate. 
The parameters are now discussed subsequently (if not specified differently,
the units are cm and degrees):

\begin{table}
\begin{center}
\begin{tabular}{|lc|}
\hline
Keyword & Parameter(s)\\
\hline
BEAMISOTOPE & $i \; i \; i$\\
BEAMENERGY & $f \; f$\\
BEAMPOSITION & $f \; f \; f \; f$\\
BEAMANGLE &$f \; f \; f \; f$\\
TARGET & $i \; f \; f \; f$\\
TARGETANGULARBROADENING & $i \; f$\\
MASSCHANGE & $i \; i$ \\
BORREL & $i \; f$ \\
GOLDHABER & $i \; f$ \\
THETARANGE & $f \; f$ \\
NUMBEROFEVENTS & $i$\\
DEFAULTCUTVALUE & $f$\\
OUTPUTFILE & $s$ \\
DEDXTABLE & $i \; s \; s$ \\
END&\\
\hline
\end{tabular}
\end{center}
\caption{Parameters of the input file EventGenerator.in.}
\end{table}\label{tab:generator}


\begin{itemize}
\item BEAMISOTOPE $A_P$ $Z_P$ $Q_P$

Contains the information on the type of projectile $P$ in the order mass
$A_P$, the element number $Z_P$ and the charge state $Q_P$.

\item BEAMENERGY $E_P$ $\Delta E(FWHM)_P$

Gives the total energy of the projectile ($E_P$ before striking on the
target) and the width of the energy distribution ($\Delta E(FWHM)_P$) in
MeV/$u$.

\item BEAMPOSITION X FWHM$_X$ Y FWHM$_Y$

Position of the projectile before impinging on the target.

\item BEAMANGLE $\vartheta_P$ $\Delta (FWHM) \vartheta_P$ $\phi_{Pmin}$
  $\phi_{Pmax}$ 

Angle of the incoming projectile. The distribution for $\Delta (FWHM)
\vartheta_P$ is Gaussian, while the distribution between $\phi_{Pmin}$ and
$\phi_{Pmax}$ is flat.

\item TARGET Type Size$_X$ Size$_Y$ Thickness$_Z$

This line specifies the target material and its dimensions. The Type is 1 for
Au, 2 for Be, 3 for C, 4 for Fe, 5 for Pb and 6 for LH$_2$. As density always
the standard value for a solid state is given. The value for Thickness$_Z$ is
given in mg/cm$^2$.

\item TARGETANGULARBROADENING Option$_{ang}$ $\Delta (FWHM)\vartheta_{target}$

Angular Broadening caused by the reaction in the target. If Option$_{ang}$ =
1, this option is considered and the broadening is Gaussian distribution
defined by $\Delta (FWHM) \vartheta_{target}$.

\item BORREL Option$_{Borrel}$ $B_n$

Velocity shift for a fragmentation process. If Option$_{Borrel}$=1, the
velocity shift is calculated according to the formula from Ref.~\cite{Borrel83}.
\begin{equation}
\frac{v_f}{v_p} = \sqrt{1-\frac{B_n (A_P - A_F)}{A_P E_F}}
\end{equation}

where the index $F$ stands for the fragment, $v$ is the velocity, and $B_n$
the binding energy (in MeV) per ablated nucleon.

\item GOLDHABER Option$_{Goldhaber}$ $\sigma_0$

Parallel momentum distribution produced in a fragmentation process. If
Option$_{Goldhaber} = 1$, the momentum distribution is calculated according
to:

\begin{equation}
\sigma_{||} = \sigma_0^2\frac{A_F (A_P - A_F)}{A_P - 1}
\end{equation}

where $\sigma_0$ is given in MeV/$c$.


\item GAMMAINPUT File$_{\gamma-in}$

The filename specifies the location of the level and decay scheme to be
simulated. See section~\ref{sec:level scheme} for more details.

\item THETARANGE $\vartheta_{\gamma min}$ $\vartheta_{\gamma max}$

Polar angular range in the moving frame of the $\gamma$-rays that should be
included in the simulation. If your detectors cover only extreme forward
angles, $\vartheta_{\gamma min}$ can be set to 0 and $\vartheta_{\gamma max}$
to 90, thereby reducing the simulation time and file size by a factor of two.

\item NUMBEROFEVENTS N$_{events}$

N$_{events}$ gives the number of reactions to be simulated.

\item DEFAULTCUTVALUE $L_{cut}$

  $L_{cut}$ specifies the default cut-in-range value in mm used in the
  simulation. Visit the \textsc{Geant4} web pages for more information.

\item OUTPUTFILENAME $File_{out}$

Specifies the location of the output root file-name. Additionally an ASCII
file with the same name and the \verb+.txt+ extension will be created. The
latter is formatted as an external event generator for AGATA (next step).

\item DEDXTABLE Option$_{dEdX}$ File$_P$ File$_E$

Instead of letting \textsc{Geant4} calculate the energy loss of projectile
and ejectile (fragment), if Option$_{dEdX} = 1$ the energy loss can be
calculated much faster (for thick targets) according to energy loss
tables. $File_P$ and $File_E$ specify the location of these tables for
projectile and ejectile, respectively.

\item END

This keyword will end the scan of the input file and can be set at any line
within the input file.

\end{itemize}


\section{Input of Level Scheme}\label{sec:level scheme}

The variable File$_{\gamma-in}$ specifies the location of the decay scheme of
the fragment nucleus to be simulated. It contains two keywords, \verb+LEVEl+
and \verb+DECAY+. They are defined as follows:
\begin{itemize}
\item LEVEL N$_{Level}$ $P_i$ $E_{Level}$ $T_{1/2}$

N$_{Level}$ is the identifier of the level, $P_i$ is the relative initial
probability of the ejectile (fragment) to end in that state after the initial
reaction, $E_{Level}$ is the energy of the level above the ground state in
keV, and $T_{1/2}$ is the half-life of the level given in ps.

\item DECAY $L_i$ $L_f$ $P_{\gamma}$

This keyword controls the decay scheme of a initial level $L_i$ into the
final level $L_f$ via the relative probability $P_\gamma$. Note that any
given level may possess up to a maximum five levels that it decays into.
\end{itemize}

A possible level scheme with two excited states at $L_1 = 1000$~keV and $L_2
= 1500$~keV and an initial level probability of $P_1 = 67$ and $P_2 = 33$
would look as:
\begin{verbatim}
LEVEL 0 0. 0. 0.
LEVEL 1 67. 1000. 10.
LEVEL 2 33. 1500. 20.
DECAY 1 0 100.
DECAY 2 1 100.
DECAY 2 0 1.
\end{verbatim}

In the above example, $L_1$ has a half-life of $T_{1/2} = 10$~ps, while $L_2$
has 20~ps. Furthermore, $L_2$ decays into the ground state (LEVEL~0) with a
relative probability of 1/1000 compared to the decay to the first excited
state (LEVEL~1).

\section{Input of the dEdX Tables}

The energy loss tables have the structure:

\verb+dEdX Energy SpecificEnergyLoss+

The energy is given in MeV/$u$ and the specific energy loss is given in
MeV/(mg/cm$^2$).

\section{Starting the Simulation}

To start the simulation in the batch mode, type:

\verb*+EventGenerator run_nothing.mac+

Omitting a filename will bring the Event Generator into the ``standard''
\textsc{Geant4} interactive session, which is not intended to be used in this
step.

After running the executable, both a root and an ascii files will be created
in the specified directory. One can easily display histograms with the generated events
opening a root-session via the TBrowser class. The ascii file will be the
input data needed at the next stage of the simulation.

\chapter{Event Builder}
The event builder is located in the \verb*+b_event_builder+ directory. Basically it
contains the Agata code of E.~Farnea plus some macros and geometry files to define a
possible geometry of 8-10 AGATA clusters for experiments at GSI. For more
details check the Agata code manual of E.~Farnea.

To compile it type:

\verb*+make+

the executable file \verb+Agata+ will be created in your
\verb+$G4WORKDIR/bin/Linux-g+++ directory.

\section{Starting the simulation}

Once the code is compiled, one can run it in order to visualize the geometry
or to run a MC simulation of the events simulated in the previous step.

Macros describing the geometry of AGATA at GSI are placed in the directory

\verb*+b_event_builder/macros_prespec+

which is sub-divided into two directories. Visualization macros are placed in

\verb*+b_event_builder/macros_prespec/vis+

and macros for running the MC-simulation are placed in

\verb*+b_event_builder/macros_prespec/mc+

To visualize the geometry of the AGATA S2 configuration type

\verb*+Agata -b macros_prespec/vis/agataS2.mac+

To run a MC simulation of the $\gamma$-ray events generated in the previous step

\verb*+Agata -Ext -b macros_prespec/mc/agataS2.mac+

The latter macro is described in more detail below.

\section{Input file configuration}


\begin{verbatim*}
/Agata/file/workingPath ../SimulationResults/Builder/74ni
/Agata/file/enableLM
/Agata/file/verbose 1
/Agata/file/info/outputMask 11100110
/Agata/detector/traslateArray 0. 0. 0.
/Agata/detector/solidFile ./A180/A180solid.list
/Agata/detector/angleFile ./A180/A180eulerS2.list 
/Agata/detector/wallsFile ./A180/A180walls.list
/Agata/detector/clustFile ./A180/A180clust.list
/Agata/detector/sliceFile ./A180/A180slice.list
/Agata/detector/enableCapsules
/Agata/detector/targetMaterial Iron
/Agata/detector/targetSize 62.25 62.25 0.5
/Agata/detector/targetPosition 0 0 0
/Agata/detector/update
/Agata/generator/emitter/eventFile ../SimulationResults/Generator/74ni/75cu_74ni_200mev_500mgfe_1024kev0.5ps.txt
/Agata/run/beamOn 50000
\end{verbatim*}

In this macro the main commands to change from one simulation to another are
the following,

\begin{itemize}
\item directory path where the AGATA output ascii file (by default \verb+GammaEvents.0000+) containing all the
$\gamma$-ray hits and other information will be saved

\verb+/Agata/file/workingPath ../SimulationResults/Builder/74ni+

\item position of AGATA with respect to the target

\verb+/Agata/detector/traslateArray 0. 0. 0.+

\item AGATA geometry (Shell S1, S2, S3 or Cylindrical C1, C2, C3)

\verb+/Agata/detector/angleFile ./A180/A180eulerS2.list+

\item target material

\verb+/Agata/detector/targetMaterial Iron+

\item target size (mm) and thickness ($g/cm^2$)

\verb+/Agata/detector/targetSize 62.25 62.25 0.5+

\item events file simulated in the previous step

\verb+/Agata/generator/emitter/eventFile ../SimulationResults/Generator/74ni/75cu_74ni_200mev_500mgfe_1024kev0.5ps.txt+

\item number of events to simulate

\verb+/Agata/run/beamOn 50000+
\end{itemize}

After running the simulation, an ascii file with name \verb+GammaEvents.0000+
will be written in the \verb+workingPath+ directory specified in the last
macro. This file should be appropriately renamed and it will be the basic
input data for the next step, the Event Reconstruction or analysis.

\chapter{Event Reconstruction}

At this stage a simplified reconstruction of the events is
performed. Add-back and Doppler-corrections are applied and the resolutions
of the detectors (spatial, energy) are implemented.

The reconstruction program is placed in the \verb+c_event_reconstruction+
directory.

\section{The input file}
The input file, where the information related to the event reconstruction or
analysis is specified, is the following

\verb+ c_event_reconstruction/input/info_reconstruction.txt+


An example of this file is described in the following.
\begin{verbatim}
#AGATA Output Ascii File
../SimulationResults/Builder/74ni/g50kEvts0.5ps500mgFe.txt
# Number of Simulated Events or Events to process
50000
#FWHM Spatial Resolution of the AGATA detectors (mm)
5
#FWHM Spatial Resolution of DSSSD (mm)
0.7
#Time Resolution for fragment detection (LYCCA) in (ps)
80
#lower Energy Range for HPGe Pulse Height Spectrum (eV)
500
#high Energy Range for HPGe Pulse Height Spectrum (eV)
2000
\end{verbatim}

Every line starting with \verb+#+ will be considered a comment and hence,
ignored by the program.
The first line specifies the name and location of the gamma events obtained
in the previous step. In this example GammaEvents.0000 has been moved to
\verb+g50kEvts0.5ps500mgFe.txt+
The rest of lines are self-explicative via the preceding comment lines. No
output will be saved, only the spectra should be displayed on the screen when
the program is executed.

\section{To run the program}
Open a root session (the program has been tested with root 5.12) and type
\begin{verbatim}
root[0].L analysis.hh
root[1]plot()
\end{verbatim}

after some seconds, depending on the number of events to process,
several spectra should be displayed on the screen. In
Table~\ref{tab:analysis} a summary of some relevant spectra/histograms
generated by default is given.

\begin{table}
\begin{center}
\begin{tabular}{|ccl|}
\hline
Canvas & Pad & Content\\
\hline
7 & 1 & g/e-hits multiplicity\\
&2& Segments multiplicity\\
&3& Crystals multiplicity\\
\hline
6 & &Beam Profile \\
\hline
5 & 1 & Raw AGATA Spectrum \\
& 2 & Doppler-Corrected AGATA Spectrum \\
\hline
4 && Momentum distributions \\
\hline
3 && Doppler-shifted $\gamma$-ray energy\\
\hline
1 && Implemented Energy Resolution for HPGe\\
\hline
\end{tabular}
\end{center}
\caption{Some of the canvas/histograms created by default in the event reconstruction.}
\end{table}\label{tab:analysis}

\chapter{A simplified illustrative example}

I want to test the performance of AGATA for line-shape analysis, in
particular the AGATA S2 geometry (a symmetric ring of 10 cluster detectors),
in a fragmentation experiment where $^{74}$Ni is produced in an excited state
at 1024~keV, which decays to the ground state with an unknown halflife, let's say between 0.05~ps and 5~ps.

\section*{Event Generation}
First I generate the primary events. I go to the \verb+a_event_generator+
directory, I compile the code (only once) and I check that in my \verb+$G4WORKDIR\bin\Linux-g+++ the \verb+EventGenerator+
executable was created.  I specify the following input parameters in the
\verb+input/EventGenerator.in+ file, which describes a secondary beam of $^{75}$Cu at 165~MeV/$u$, with an spread of 6~cm
FWHM$_X$ and 4~cm FWHM$_Y$, impinging onto an Iron target 500~mg/cm$^2$ thick
and producing $^{74}$Ni with a certain level scheme as described in the
second file (see below).

\begin{verbatim}
BEAMISOTOPE 75 29 29
BEAMENERGY 165   3
BEAMPOSITION 0.0 6.0 0.0 4.0
BEAMANGLE 0.0 0.65 0.0 360.0
TARGET 4 6.25 6.25 500
TARGETANGULARBROADENING 1 0.6
MASSCHANGE 1 1
BORREL 1 8.
GOLDHABER 1 90.
GAMMAINPUT ./input/74Ni.in
NUMBEROFEVENTS 50000
DEFAULTCUTVALUE 0.001
OUTPUTFILE ../SimulationResults/Generator/74ni/75cu_74ni_165mev_500mgfe_1024kev0.5ps.root
DEDXTABLE 1 ./dEdXTables/CuOnFe.in ./dEdXTables/NiOnFe.in
END
\end{verbatim}

The level scheme of $^{74}$Ni is specified in \verb+input/74Ni.in+
\begin{verbatim}
LEVEL 0 0.  0. 0.
LEVEL 1 100.0 1024.0 0.5
DECAY 1 0 100.
END
\end{verbatim}

I repeat this stage for different values of the half-life (given in 74Ni.in), between 0.05~ps
and 5~ps, and I store the corresponding primary events in the
\verb+../SimulationResults/Generator/74ni+ as shown above.

Opening a root session in the latter directory, via the TBrowser one can
quickly check some aspects as the beta distributions before and after the target, etc.

\begin{figure}[!htbp]
\centering
\includegraphics{./figures/rootbrowser.eps}
\end{figure}

By double-click on the leaves above, one can display e.g. the following
histograms,

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.4\textwidth]{./figures/betabeforetarget.eps}
\includegraphics[width=0.4\textwidth]{./figures/betaaftertarget.eps}
\includegraphics[width=0.4\textwidth]{./figures/decaytimeafterfrag.eps}
\includegraphics[width=0.4\textwidth]{./figures/egammadoppler.eps}
\includegraphics[width=0.4\textwidth]{./figures/egammarest.eps}
\includegraphics[width=0.4\textwidth]{figures/energyprojectile.eps}
\caption{Some histograms of the primary events generated via EventGenerator.}
\end{figure}

\newpage

\section*{Event Builder/AGATA Response}

I go to the \verb+b_event_builder+ directory, I compile the code (only once),
and I check that in my \verb+$G4WORKDIR\bin\Linux-g+++ the \verb+Agata+
executable was created.

I visualize first the geometry of the AGATA array via (see also Fig.~\ref{fig:agataS2}):

\begin{verbatim}
Agata -b macros_prespec/vis/agataS2.mac
\end{verbatim}

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.4\textwidth]{./figures/agataS2fig1.eps}
\includegraphics[width=0.4\textwidth]{./figures/agataS2fig2.eps}
\caption{AGATA geometry to simulate in this example.}
\end{figure}\label{fig:agataS2}

Once I have checked that this is the geometry that I want to simulate, I run
the mc-simulation via:

\begin{verbatim}
Agata -Ext -b macros_prespec/mc/agataS2.mac
\end{verbatim}

where the \verb+mc/agataS2.mac+ macro contains the following lines,

\begin{verbatim}
/Agata/file/workingPath ../SimulationResults/Builder/74ni
/Agata/file/enableLM
/Agata/file/verbose 1
/Agata/file/info/outputMask 11100110
/Agata/detector/traslateArray 0. 0. 0.
/Agata/detector/solidFile ./A180/A180solid.list
/Agata/detector/angleFile ./A180/A180eulerS2.list
/Agata/detector/wallsFile ./A180/A180walls.list
/Agata/detector/clustFile ./A180/A180clust.list
/Agata/detector/sliceFile ./A180/A180slice.list
/Agata/detector/enableCapsules
/Agata/detector/targetMaterial Iron
/Agata/detector/targetSize 62.25 62.25 0.5
/Agata/detector/targetPosition 0 0 0
/Agata/detector/update
/Agata/generator/emitter/eventFile ../SimulationResults/Generator/74ni/75cu_74ni_165mev_500mgfe_1024kev0.5ps.txt
/Agata/run/beamOn 50000
\end{verbatim}

\textbf{It is important to take care that the target material, thickness and size are the same
as the target used in the previous Event-Generation stage.}

I run the MC-simulation three times, one for each half life which I want to
simulate, 0.05~ps, 0.5~ps and 5~ps. The corresponding primary event files
were stored in the previous stage, inside
\verb+../SimulationResults/EventGenerator/74ni/+). After each run, I move the AGATA output
file \verb+GammaEvents.0000+ saved in

\verb+../SimulationResults/Builder/74ni/GammaEvents.0000+ 

to another name, e.g.

\verb+../SimulationResults/Builder/74ni/g50kEvts0.5ps500mgFe.txt+


\section*{Event Reconstruction}

I go to the \verb+c_event_reconstruction+ directory and specify the following
parameters in the \verb+input\info_reconstruction.txt+ file,

\begin{verbatim}
#AGATA Output Ascii File
../SimulationResults/Builder/74ni/g50kEvts0.5ps500mgFe.txt
# Number of Simulated Events or Events to process
50000
#FWHM Spatial Resolution of the AGATA detectors (mm)
5
#FWHM Spatial Resolution of DSSSD (mm)
0.7
#Time Resolution for fragment detection (LYCCA) in (ps)
80
#lower Energy Range for HPGe Pulse Height Spectrum (eV)
500
#high Energy Range for HPGe Pulse Height Spectrum (eV)
2000
\end{verbatim}

After running the plot() method for each one of the files in

\verb+../SimulationResults/Builder/74ni/+ 

one obtains some spectra like those shown in Fig.~\ref{fig:reconstruction}
for the primary $\gamma$-rays spatial distribution and the AGATA
reconstructed pulse height spectra.

\begin{figure}[!htbp]
\centering
\includegraphics[width=\textwidth]{./figures/rec_beam_profile.eps}
\includegraphics[width=\textwidth]{./figures/rec_gamma_spectrazoom.eps}
\caption{Some spectra obtained via analysis.hh$\rightarrow$plot().}
\end{figure}\label{fig:reconstruction}

One can compare now the Doppler-Corrected lines for the three half-life cases
simulated, 0.05~ps, 0.5~ps and 5~ps, thus obtaining the following effect in
the line-shape,

\begin{figure}[!htbp]
\centering
\includegraphics[width=0.8\textwidth]{./figures/example1.eps}
\includegraphics[width=0.8\textwidth]{./figures/example1zoom.eps}
\caption{Blue, red and black lines correspond to level half-lifes of 5~ps, 0.5~ps
  and 0.05~ps, respectively.}
\end{figure}\label{fig:reconstruction}

\bibliographystyle{unsrt}
\bibliography{agata_gsi}

\end{document}

