#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "Randomize.hh"

#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"

#include "RunAction.hh"
#include "EventAction.hh"
#include "TrackingAction.hh"
#include "SteppingAction.hh"
#include "SteppingVerbose.hh"

#ifdef G4VIS_USE
  #include "G4VisExecutive.hh"
#endif

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TTree.h"

#include <iostream>
#include "G4ios.hh"

using namespace std;

#ifdef G4V10
using namespace CLHEP;
#endif

//Defining all the variables
//Initialzing the variables that need an initial value
int mass                = 1;
int z                   = 1;
int charge              = 1;
int numevent_total      = 10000;
int mass_f              = 1;
int z_f                 = 1;
int charge_f            = 1; 
int kind_target         = 2;            // kind_target 1 Au 2 Be 
int deltamass           = 0;
int deltazet            = 0;            

float energy_beam_mean  = 0.;
float energy_beam_sigma = 0.;           // Energy_of_beam A MeV
float target_X          = 1.;           // dimension of the target in cm for X,Y and mg/cm^2 for Z
float target_Y          = 1.;
float thickness         = 0.001;   

char temp[200];
char gamma_in[200]      = "dummy.in";
char root_out[200]      = "dummy.root";
char tempparticle[200];

float theta_low         = 0.;
float theta_high        = 180.;           // Angle covered
float x_beam            = 0.; 
float y_beam            = 0.; 
float x_beam_sigma      = 0.0;
float y_beam_sigma      = 0.0;
float theta_beam        = 0.;
float theta_beam_sigma  = 0.0;
float phi_beam_min      = 0.;
float phi_beam_max      = 360.;
int target_broadening_option = 0;        // Option to take also the angular broadening of the beam due to fragmentation
float theta_target_broadening_sigma = 0.0;

int borrel_option       = 0;
int goldhaber_option    = 0;
float sigma0_goldhaber  = 90.;
float defaultCutValue   = 0.001;
//_______________________________________________
double sum_dist;
double sum_dist_uptonow[18001];
//_______________________________________________
float evnum;
float decayIDevnum;
float p0[3],pvb[3],pva[3];
float energy_p;
float beta_b,beta_r,beta_a,halflife,decay_time_after_interaction;
float g0[3],gv[3];
float e_rest,e_doppler;
float theta_gamma_rest, theta_gamma_lab;
float energy_vertex_stored;
float binding_energy_nucleon;
double density[7];
//_______________________________________________
int dEdXTableOption          = 0;
char dEdXTableInputBeam[200] = "./dEdXTables/dummy.in";
char dEdXTableInputFragment[200] = "./dEdXTables/dummy.in";
const int n_dEdX_values = 83;
//const int n_dEdX_values = 72;
//const int n_dEdX_values = 56;
float beamdEdX[2][n_dEdX_values]        = {{0.0}}; // The first value gives the energy, the second the actual value;
float fragmentdEdX[2][n_dEdX_values]    = {{0.0}};
int flag_add_bkg = 1;//0 no bkg, 1 bs bkg, 2 ...
float bkg_multiplicity = 0;

//_______________________________________________
struct Level{
  int ID;
  float excitationProbability;
  float beginOfTotalExcitationProbability;
  float endOfTotalExcitationProbability;
  float energy;
  G4double halflife;
  int numberOfDecayBranches;
  int decayIntoLevelId[5];
  float decayIntoLevelProbability[5];
  float totalDecayProbability;
  float beginOfDecayProbability[5];
  float endOfDecayProbability[5];
} SLevel[100];
//_______________________________________________
float gTotalLevelExcitationProbability = 0.;
int gNumberOfLevels                    = 0; 
int gNumberOfExcitedLevels             = 0; 
//_______________________________________________
int fDistributionType = 0;
//_______________________________________________
// The ROOT-Tree
TTree *t;
TTree *tHeader;  // The header for information that doesn't change.
//_______________________________________________
void ReadInputFile();
void ReadGammaFile();
void RunSimulation();
void ReaddEdXTable();
void SaveAgataAsciiFile();

G4ThreeVector GetNextVector(G4ThreeVector vector_in, float theta);
//_______________________________________________
G4RunManager           *runManager;
RunAction              *run;
DetectorConstruction   *det;
PrimaryGeneratorAction *kin;
SteppingAction         *steppingaction;
G4UImanager            *UI;
PhysicsList            *phys;
//_______________________________________________


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
int main(int argc,char** argv) {

  // Reading the input file 
  ReadInputFile();
  // Reading the gamma-ray decay file
  ReadGammaFile();

  // Calculating the fragment mass and charge:
  z_f      = z      - deltazet;
  mass_f   = mass   - deltamass;
  charge_f = charge - deltazet;

  density[0] = 0.0;     // Vacuum
  density[1] = 19.3;    // Au
  density[2] = 1.848;   // Be
  density[3] = 1.82;/// 2009.3.11: density changed 2.2;     // C
  density[4] = 7.874;   // Fe
  density[5] = 11.35;   // Pb
  density[6] = 0.07099; // LH2
  
  // Calculating the target thickness in cm!
  thickness = thickness/density[kind_target]/1000.0;

  // Making a uniform theta distribution
  ///
  sum_dist = 0.0;
  //distribution 0: uniform; 2200: J,M 2,2-> 0,0; 2100: J,M 2,1->0,0 2000: J,M 2,0 -> 0,0
  // The zet axis is the beam axis
  for(int m2=(int)(theta_low*100);m2<(int)(theta_high*100+1);m2++){					
    G4double angle  = m2/100.0*degree;
    if(fDistributionType == 2000)  
      sum_dist = sum_dist + 1.0*sin(angle/rad)
        *( sin(angle/rad)*sin(angle/rad)*cos(angle/rad)*cos(angle/rad));
    else if(fDistributionType == 2100)
      sum_dist = sum_dist + 1.0*sin(angle/rad)
        *(1 - 3*cos(angle/rad)*cos(angle/rad) + 4*cos(angle/rad)*cos(angle/rad)*cos(angle/rad)*cos(angle/rad));
    else if (fDistributionType == 2200)
      sum_dist = sum_dist + 1.0*sin(angle/rad)
        *(1 - cos(angle/rad)*cos(angle/rad)*cos(angle/rad)*cos(angle/rad));
    // Making a uniform theta distribution:
    else sum_dist = sum_dist + 1.0*sin(angle/rad);
    sum_dist_uptonow[m2] = sum_dist;
  }
  
  // Choose the Random engine
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  
  // My Verbose output class
  G4VSteppingVerbose::SetInstance(new SteppingVerbose);
    
  // Construct the default run manager
  runManager = new G4RunManager;

#ifdef G4VIS_USE
   G4VisManager* visManager = new G4VisExecutive;
   visManager->Initialize();
#endif   

  runManager->SetUserInitialization(det  = new DetectorConstruction);
  det       ->UpdateGeometry();
  runManager->SetUserInitialization(phys = new PhysicsList(defaultCutValue));
  runManager->SetUserAction(kin = new PrimaryGeneratorAction(det));
  
  // Set user action classes
  runManager->SetUserAction(run = new RunAction(det,phys,kin)); 
  runManager->SetUserAction(new EventAction);
  runManager->SetUserAction(new TrackingAction(run));
  runManager->SetUserAction(steppingaction = new SteppingAction(det,run));

  // Get the pointer to the User Interface manager 
  // G4UImanager *UI = G4UImanager::GetUIpointer();
  UI = G4UImanager::GetUIpointer();
 
  // Setting target material and thickness in cm
  det->SetTargetSize(target_X,target_Y,thickness);
  if(kind_target==1) det->SetTargetMaterial("Au");
  if(kind_target==2) det->SetTargetMaterial("Be");
  if(kind_target==3) det->SetTargetMaterial("C");
  if(kind_target==4) det->SetTargetMaterial("Fe");
  if(kind_target==5) det->SetTargetMaterial("Pb");
  if(kind_target==6) det->SetTargetMaterial("LH2");
  
  det->UpdateGeometry();

  if(argc==1){
  // Define (G)UI terminal for interactive mode  
    cout<<"Interactive mode"<<endl;
    // G4UIterminal is a (dumb) terminal.
    G4UIsession * session = 0; 
#ifdef G4UI_USE_TCSH
      session = new G4UIterminal(new G4UItcsh);      
#else
      session = new G4UIterminal();
#endif    

    UI->ApplyCommand("/control/execute vis.mac");    
    session->SessionStart();
    delete session;
  }
  else{
  // Batch mode
    cout<<"Batch mode"<<endl;
    G4String command  = "/control/execute ";
    G4String fileName = argv[1];
    UI->ApplyCommand(command+fileName);
  }

  UI->ApplyCommand("/vis/scene/notifyHandlers");
  UI->ApplyCommand("/run/verbose 0");
  UI->ApplyCommand("/event/verbose 0");
  UI->ApplyCommand("/tracking/verbose 0");

  TFile *rootfile = new TFile(root_out,"RECREATE");
  rootfile->cd();

  t = new TTree("Events","Events");
  t->Branch("EventNumber",&evnum,"evnum/F");
  t->Branch("DecayIDOfEventNumber",&decayIDevnum,"decayIDevnum/F");
  t->Branch("ProjectileVertex",p0,"p0[3]/F");                                   // Position at the fragmentation point
  t->Branch("VProjectileBeforeTarget",pvb,"pvb[3]/F");                          // Normalized Vector of beam before the target
  t->Branch("VProjectileAfterTarget",pva,"pva[3]/F");                           // Normalized Vector of beam after the target
  t->Branch("EnergyProjectile",&energy_p,"energy_p/F");                         // Energy of beam before the target in MeV/u
  t->Branch("BetaBeforeTarget",&beta_b,"beta_b/F");                             // Beta before the target
  t->Branch("BetaReal",&beta_r,"beta_r/F");                                     // Beta at deexcitation	
  t->Branch("BetaAfterTarget",&beta_a,"beta_a/F");                              // Beta After Target
  t->Branch("Halflife",&halflife,"halflife/F");                                 // Halflife
  t->Branch("DecayTimeAfterInteraction",&decay_time_after_interaction,"decay_time_after_interaction/F");
  t->Branch("VertexGamma",g0,"g0[3]/F");                                        // Position at the gamma emmittance point
  t->Branch("VGamma",gv,"gv[3]/F");		                                // Gamma vector
  t->Branch("EGammaRest",&e_rest,"e_rest/F");		                        // Energy at rest
  t->Branch("EGammaDoppler",&e_doppler,"e_doppler/F");                          // Theta of doppler boosted gamma
  t->Branch("ThetaGammaRest",&theta_gamma_rest,"theta_gamma_rest/F");
  t->Branch("ThetaGammaLab",&theta_gamma_lab,"theta_gamma_lab/F");
  t->Branch("EnergyVertex",&energy_vertex_stored,"energy_vertex_stored/F");     // Energy of fragment at fragmentation
  // The header tree gets the information that is not changed.
  tHeader  = new TTree("Header","Header");
  tHeader->Branch("Mass",&mass,"mass/I");                                       // Beam mass
  tHeader->Branch("Z",&z,"z/I");                                                // Beam z	
  tHeader->Branch("Charge",&charge,"charge/I");                                 // Beam charge	
  tHeader->Branch("MassFragment",&mass_f,"mass_f/I");                           // Fragment mass
  tHeader->Branch("ZFragment",&z_f,"z_f/I");	                                // Fragment z
  tHeader->Branch("ChargeFragment",&charge_f,"charge_f/I");                     // Fragment charge
  tHeader->Branch("TargetKind",&kind_target,"kind_target/I");
  tHeader->Branch("TargetThicknessCM",&thickness,"thickness/F");
  tHeader->Branch("ThetaLow",&theta_low,"theta_low/F");                         // Theta angle covered by simulation
  tHeader->Branch("ThetaHigh",&theta_high,"theta_high/F");
  tHeader->Fill();

  UI->ApplyCommand("/testem/phys/addPhysics standard");
  UI->ApplyCommand("/run/initialize");

  sprintf(tempparticle,"/gun/ion %i %i %i",z,mass,charge);
  UI->ApplyCommand("/gun/particle ion");
  UI->ApplyCommand(tempparticle);
 
  // Starting the simulation:
  RunSimulation();
  // Writing stuff to file and spectra:
  tHeader->Write();
  t->Write();

  UI->ApplyCommand("/vis/viewer/update");
#ifdef G4VIS_USE
  delete visManager;
#endif
  delete runManager; 
  
  SaveAgataAsciiFile();


  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo...... 
void ReadInputFile()
{
  // The Input files are of free type.
  FILE *fin = fopen("./input/EventGenerator.in","r");
  while(!feof(fin)){
    fscanf(fin,"%s ",temp); 
    if(strcmp(temp,"BEAMISOTOPE")==0){
      fscanf(fin,"%i %i %i ",&mass,&z,&charge); 
      printf("%s %i %i %i\n",temp,mass,z,charge);
    }
    else if(strcmp(temp,"BEAMENERGY")==0){
      fscanf(fin,"%f %f",&energy_beam_mean,&energy_beam_sigma); 
      energy_beam_sigma = energy_beam_sigma/2.35;
      printf("%s %f %f \n",temp,energy_beam_mean,energy_beam_sigma);
    }
    else if(strcmp(temp,"BEAMPOSITION")==0){
      fscanf(fin,"%f %f %f %f",&x_beam,&x_beam_sigma,&y_beam,&y_beam_sigma);
      x_beam_sigma = x_beam_sigma/2.35;y_beam_sigma = y_beam_sigma/2.35;
      printf("%s %f %f %f %f \n",temp,x_beam,x_beam_sigma,y_beam,y_beam_sigma);
    }
    else if(strcmp(temp,"BEAMANGLE")==0){
      fscanf(fin,"%f %f %f %f",&theta_beam,&theta_beam_sigma,&phi_beam_min,&phi_beam_max);
      theta_beam_sigma=theta_beam_sigma/2.35;
      printf("%s %f %f %f %f \n",temp,theta_beam,theta_beam_sigma,phi_beam_min,phi_beam_max);
    }
    else if(strcmp(temp,"TARGET")==0){
      fscanf(fin,"%i %f %f %f",&kind_target,&target_X,&target_Y,&thickness);
      if(kind_target<0 && kind_target>6){
        cout<<"Could not read your input keyword. Aborting program."<<endl; 
        abort();
      }
      printf("%s %i %f %f %f \n",temp,kind_target,target_X,target_Y,thickness);
    }   
    else if(strcmp(temp,"TARGETANGULARBROADENING")==0){
      fscanf(fin,"%i %f",&target_broadening_option,&theta_target_broadening_sigma);
      theta_target_broadening_sigma = theta_target_broadening_sigma/2.35;
      printf("%s %i %f \n",temp,target_broadening_option,theta_target_broadening_sigma);
    }
    else if(strcmp(temp,"MASSCHANGE")==0){
      fscanf(fin,"%i %i",&deltamass,&deltazet);
      printf("%s %i %i \n",temp,deltamass,deltazet);
    }
    else if(strcmp(temp,"BORREL")==0){
      fscanf(fin,"%i %f",&borrel_option,&binding_energy_nucleon);
      printf("%s %i %f \n",temp,borrel_option,binding_energy_nucleon);
    }
    else if (strcmp(temp,"GOLDHABER")==0){
      fscanf(fin,"%i %f",&goldhaber_option,&sigma0_goldhaber);
      printf("%s %i %f \n",temp,goldhaber_option,sigma0_goldhaber);
    }
    else if(strcmp(temp,"GAMMAINPUT")==0){
      fscanf(fin,"%s",&gamma_in);
      printf("%s %s \n",temp,gamma_in);
    }
    else if(strcmp(temp,"THETARANGE")==0){
      fscanf(fin,"%f %f",&theta_low,&theta_high);
      printf("%s %f %f \n",temp,theta_low,theta_high);
    }
    else if(strcmp(temp,"NUMBEROFEVENTS")==0){
      fscanf(fin,"%i ",&numevent_total);
      printf("%s %i\n",temp,numevent_total);
    }
    else if(strcmp(temp,"DEFAULTCUTVALUE")==0){
      fscanf(fin,"%f ",&defaultCutValue);
      printf("%s %f\n",temp,defaultCutValue);
    }
    else if(strcmp(temp,"OUTPUTFILE")==0){
      fscanf(fin,"%s ",&root_out); 
      printf("%s %s \n",temp,root_out);
    }
    else if(strcmp(temp,"DEDXTABLE")==0){
      fscanf(fin,"%i %s %s",&dEdXTableOption,&dEdXTableInputBeam,&dEdXTableInputFragment); 
      printf("%s %i %s %s\n",temp,dEdXTableOption,dEdXTableInputBeam,dEdXTableInputFragment);
      if(dEdXTableOption==1) ReaddEdXTable();
    }
    else if(strcmp(temp,"DISTRIBUTIONTYPE")==0){
      fscanf(fin,"%i ",&fDistributionType); 
      printf("%s %i \n",temp,fDistributionType);
    }
    else if(strcmp(temp,"ADDBACKGROUND")==0){
      fscanf(fin,"%i ",&flag_add_bkg); 
      printf("%s %i \n",temp,flag_add_bkg);
    }
    else if(strcmp(temp,"BKGMULTIPLICITY")==0){
      fscanf(fin,"%f ",&bkg_multiplicity); 
      printf("%s %f \n",temp,bkg_multiplicity);
    }
    else if(strcmp(temp,"END")==0) break;
    else {
      cout<<"Could not read your input keyword. Aborting program."<<endl; 
      abort();
    }
  }
  fclose(fin);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void ReadGammaFile(){
  // At first, initializing the struct of SLevel[100]
  cout<<"Reading Gamma input file"<<endl;
  for(int i=0;i<100;i++){
    SLevel[i].ID = -1;
    SLevel[i].excitationProbability             = 0.;
    SLevel[i].beginOfTotalExcitationProbability = 0.;
    SLevel[i].endOfTotalExcitationProbability   = 0.;
    SLevel[i].energy                            = 0.;
    SLevel[i].halflife                          = 0.;
    SLevel[i].numberOfDecayBranches             = 0;
    SLevel[i].totalDecayProbability             = 0.;
    for(int j=0;j<5;j++){
      SLevel[i].decayIntoLevelId[j]             = 0;
      SLevel[i].decayIntoLevelProbability[j]    = 0.;
      SLevel[i].beginOfDecayProbability[j]      = 0.;
      SLevel[i].endOfDecayProbability[j]        = 0.;
    }
  }

  int levelID,decayLevelID; 
  float levelExcitationProbability, levelEnergy, levelHalflife;
  float branchRatio;
  
  FILE *gammaIn = fopen(gamma_in,"r");

  if(gammaIn == NULL)
    {
      cout << "Problems opening file with g-levels, "<< gamma_in << " check if file exists, exiting... " <<endl;
      exit(-1);
    }

  while(!feof(gammaIn)){
    fscanf(gammaIn,"%s ",&temp);
    if(strcmp(temp,"LEVEL")==0){
      fscanf(gammaIn,"%i %f %f %f",&levelID,&levelExcitationProbability,&levelEnergy,&levelHalflife);
      // Check if the input values are greater than zero
      if(levelID < 0 || levelExcitationProbability < 0. || levelEnergy < 0.){
        cout<<"At least one of your LEVEL input values is smaller than zero. Aborting program."<<endl; 
        abort();
      }
      // Check if the level has been assigned already
      if(SLevel[levelID].ID != -1){
        cout<<"This LEVEL has been assigned already. Aborting program."<<endl; 
        abort();
      }
      SLevel[levelID].ID=levelID;
      SLevel[levelID].excitationProbability=levelExcitationProbability;
      // Determine the range of this level within the total excitation probabilty
      // To be used later for the determination of the initial state of excitation
      SLevel[levelID].beginOfTotalExcitationProbability = gTotalLevelExcitationProbability;
      gTotalLevelExcitationProbability = gTotalLevelExcitationProbability + levelExcitationProbability;
      
      cout<<"gTotalLevelExcitationProbability: "<<gTotalLevelExcitationProbability<<endl;
      
      SLevel[levelID].endOfTotalExcitationProbability = gTotalLevelExcitationProbability;
      SLevel[levelID].energy=levelEnergy;
      SLevel[levelID].halflife=levelHalflife;
      gNumberOfLevels++;
      if(levelEnergy>0.) gNumberOfExcitedLevels++;
    } 
    else if(strcmp(temp,"DECAY")==0){
      fscanf(gammaIn,"%i %i %f",&levelID,&decayLevelID,&branchRatio);
      // Setting the maximum of decay branches to five:
      int branchID = SLevel[levelID].numberOfDecayBranches;
      if(branchID>4){
        cout<<"This LEVEL has already five decay branches. Aborting program."<<endl; 
        abort();
      }
      SLevel[levelID].decayIntoLevelId[branchID] = decayLevelID;
      SLevel[levelID].decayIntoLevelProbability[branchID] = branchRatio;
      // Determine the range of this decay within all decay branches
      // To be used later for the determination of the decay branch
      SLevel[levelID].beginOfDecayProbability[branchID] = SLevel[levelID].totalDecayProbability;
      SLevel[levelID].totalDecayProbability = SLevel[levelID].totalDecayProbability + branchRatio;
      cout<<" Total Decay Probability of Level "<<SLevel[levelID].ID<<": "<< SLevel[levelID].totalDecayProbability<<endl;
      SLevel[levelID].endOfDecayProbability[branchID] = SLevel[levelID].totalDecayProbability;
      SLevel[levelID].numberOfDecayBranches++;
    }
    else if(strcmp(temp,"END")==0) break;
    else{
      cout<<"Could not read your input keyword of the gamma-ray decay file. Aborting program."<<endl; 
      abort();
    }
  }
  fclose(gammaIn);
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void ReaddEdXTable(){
  G4cout<<"Building the energy loss table..."<<G4endl;
  float dummy1,dummy2;
  int i=0,j=0;
  FILE *tableInBeam = fopen(dEdXTableInputBeam,"r");
  while(!feof(tableInBeam)&&j<n_dEdX_values){
    fscanf(tableInBeam,"%s ",temp); 
    if(strcmp(temp,"dEdX")==0){
      fscanf(tableInBeam,"%f %f",&dummy1,&dummy2);
      beamdEdX[0][i] = dummy1;
      beamdEdX[1][i] = dummy2;
      cout<< dummy1<<" "<<dummy2<<endl;
      i++;
    }
    if(strcmp(temp,"END")==0)  break;
  }
  fclose(tableInBeam);
  
  if(i !=  n_dEdX_values)
    cout << " Warning: change number of dEdX values ?! " << endl;

  FILE *tableInFragment = fopen(dEdXTableInputFragment,"r");
  while(!feof(tableInFragment)&&j<n_dEdX_values){
    fscanf(tableInFragment,"%s ",temp); 
    if(strcmp(temp,"dEdX")==0){
      fscanf(tableInFragment,"%f %f",&dummy1,&dummy2);
      fragmentdEdX[0][j] = dummy1;
      fragmentdEdX[1][j] = dummy2;
      cout<< dummy1<<" "<<dummy2<<endl;
      j++;
    }
    if(strcmp(temp,"END")==0)  break;  
  }
  fclose(tableInFragment);

  if(j !=  n_dEdX_values)
    cout << " Warning: change number of dEdX values ?! " << endl;
}
                            
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void RunSimulation(){
  G4cout<<"Running the simulation"<<G4endl;

  double gamma_temp;
  int i=0;
  while(i<numevent_total){
    if(i%1000==0)
      {
	printf("%d Events Processed of %d \r",i,numevent_total);
	cout.flush();	      
      }

    int restart_loop = 0;

    // Reseting the decay ID and the time of the event to zero;
    decay_time_after_interaction = 0.;
    decayIDevnum                 = 0.;
    G4double beam_origin_x       = 999.9;
    G4double beam_origin_y       = 999.9;
    G4double beam_origin_z       = 999.9;

    // Defining the beam's starting position
    while(beam_origin_x>target_X || beam_origin_x<(-1.0*target_X) || beam_origin_y>target_Y || beam_origin_y<(-1.*target_Y)){
      beam_origin_x = G4RandGauss::shoot(x_beam,x_beam_sigma);
      beam_origin_y = G4RandGauss::shoot(y_beam,y_beam_sigma);
      beam_origin_z = -1.0*thickness/2.0;                      // cout<<"Beam origin Z: "<<beam_origin_z<<endl;
    }
    // Defining the beam's starting vector.
    G4double ftheta = -10.0;
    G4double fphi   = -10.0;	
    while(ftheta< 0.0 || ftheta > 180.0 || fphi < 0.0 || fphi>360.0){  
      // Gaussian distribution from input values
      ftheta = G4RandGauss::shoot(theta_beam, theta_beam_sigma);
      fphi   = CLHEP::RandFlat::shoot(phi_beam_min, phi_beam_max);  
    }
    fphi   = fphi*degree;           // cout<<"fphi = "<<fphi<<endl;
    ftheta = ftheta*degree;         // cout<<"ftheta = "<<ftheta<<endl;
    
    // Normalized vector before the target					
    pvb[0] = sin(ftheta)*cos(fphi);  // cout<<"x_pvb: "<<x_pvb<<endl;
    pvb[1] = sin(ftheta)*sin(fphi);  // cout<<"y_pvb: "<<y_pvb<<endl;
    pvb[2] = cos(ftheta);            // cout<<"z_pvb: "<<z_pvb<<endl;
    
    // Energy of the projectile
    energy_p   = G4RandGauss::shoot(energy_beam_mean,energy_beam_sigma); 
    gamma_temp = (931.494+energy_p)/931.494;            // Gamma before the target
    beta_b     = sqrt(1.0 - 1.0/gamma_temp/gamma_temp); // Beta before the target
    
    // Total beam energy before the target
    double energy_total = energy_p * (double)mass;        

    // Assigning the initial beam values
    kin->GetParticleGun()->SetParticleEnergy(energy_total*MeV);
    kin->GetParticleGun()->SetParticleMomentumDirection(G4ThreeVector(pvb[0]*m,pvb[1]*m,pvb[2]*m));
    kin->GetParticleGun()->SetParticlePosition(G4ThreeVector(beam_origin_x*cm,beam_origin_y*cm,beam_origin_z*cm));
   
    //------------------------------
    // ****Starting Beam*************
    //------------------------------ 
    // First, getting the fragmentation point to know how far the energy loss has
    // to be calculated for the secondary beam
    p0[2] = CLHEP::RandFlat::shoot(-1.0*thickness/2.0,1.0*thickness/2.0);  // G4cout<<"Z_Point of fragmentation: "<<z_p0<<endl;
    p0[0] = beam_origin_x;	
    p0[1] = beam_origin_y;
    steppingaction->SetInteractionPoint(p0[0]*cm,p0[1]*cm,p0[2]*cm);
   
    // Reseting the target and gamma excitation flag 
    steppingaction->StartBeam();
    // Shooting the beam;
    if(dEdXTableOption==0 && energy_total>0.) 
      runManager->BeamOn(1);  // Let GEANT4 only simulate if no dEdX tables are provided

    if(steppingaction->GetFlagTarget()==1 || dEdXTableOption==1 || energy_beam_mean==0){
      double energy_vertex=0.;
      if(dEdXTableOption==0 && energy_total>0.) energy_vertex = steppingaction->GetTotalEnergy(); 
      //___________________________________________________________________________________________
      else if(dEdXTableOption==1){
        float energyHalfStep = (beamdEdX[0][0] - beamdEdX[0][1])/2;
        // Have to calculate the energy loss myself:
        for(int j=0;j<n_dEdX_values;j++){
          if(beamdEdX[0][j]+energyHalfStep>energy_p && 
             beamdEdX[0][j]-energyHalfStep<energy_p){
            //cout<<"condition fullfilled: "<<j<<endl;
            float dummyEnergy1  = energy_total;
            float dummyEnergy2  = beamdEdX[0][j+1] + energyHalfStep;
            float dummyPosition = beam_origin_z;
            int dummyCounter    = j;
            while(dummyPosition<p0[2]){
              // cout<<"Dummy position: "<<dummyPosition<<endl;
              dummyPosition = dummyPosition + 0.1*defaultCutValue;  // Convert into cm because beamdEdX is given in cm
              dummyEnergy1  = dummyEnergy1 - beamdEdX[1][dummyCounter]*0.1*defaultCutValue * density[kind_target]*1000;
              // cout<<"dummyEnergy1: "<<dummyEnergy1<<endl;
              if(dummyEnergy1/(double)mass < dummyEnergy2){
                dummyCounter++;
                dummyEnergy2 = beamdEdX[0][dummyCounter+1] + energyHalfStep;
              }
            }
	    energy_vertex = dummyEnergy1;
	  }
        }
      }
      else energy_vertex = 0.0;
      //___________________________________________________________________________________________
      // Energy of fragment at fragmentation point:
      // Calculating the energy spread due to goldhaber
      if(goldhaber_option==1 && energy_vertex >0.){	  
        double sigma_goldhaber = sqrt(sigma0_goldhaber * sigma0_goldhaber * mass_f * (mass-mass_f)/(mass-1)); 
        double sigma_momentum  = G4RandGauss::shoot(0,sigma_goldhaber); 	
        
        gamma_temp             = (931.494+energy_vertex/mass) / 931.494;
        float beta_temp        = sqrt(1.0-1.0/gamma_temp/gamma_temp);
        double sigma_energy    = sigma_momentum * gamma_temp * beta_temp;
        
        energy_vertex_stored   = energy_vertex * mass_f/mass + sigma_energy; // Energy of fragment at fragmentation
        energy_vertex          = energy_vertex_stored;                       // cout<<"Energy vertex of fragment: "<<energy_vertex<<endl;
      }
      else{
        energy_vertex_stored   = energy_vertex * mass_f/mass;
        energy_vertex          = energy_vertex_stored;
      }
      
      double beta_vertex;
      if(borrel_option==1 && energy_vertex>0.){
        // Fragment velocity change
	double VelocityChange = sqrt(1 - ((binding_energy_nucleon*(mass-mass_f))/(energy_vertex)));  // Relative change of velocity (Borrel et al.)
	gamma_temp            = (931.494 + energy_vertex/mass_f)/931.494;	                     // Initial gamma value
	beta_vertex           = VelocityChange * sqrt(1.0 - 1.0/gamma_temp/gamma_temp);              // Changed absolute beta
        gamma_temp            = sqrt(1/(1 - beta_vertex * beta_vertex));                             // Changed gamma value
	energy_vertex         = (931.494 * gamma_temp - 931.494)*mass_f;	                     // Changed energy from gamma value
	energy_vertex_stored  = energy_vertex;                                                       // cout<<"Energy vertex including Borrel: "<<energy_vertex<<endl;  
      }
      else{
        gamma_temp  = (931.494 + energy_vertex/mass_f)/931.494;
	beta_vertex = sqrt(1.0 - 1.0/gamma_temp/gamma_temp);
      }
      //End energy at fragmentation point
      //---------------------------------
      //---------------------------------------------------------
      // Broadening in angles due to fragmentation in the target:
      G4double ftheta2 = -10.0;
      G4double fphi2   = -10.0;  
      if(target_broadening_option==1 && energy_vertex>0.) {
        while(ftheta2< 0.0 || ftheta2>180.0 || fphi2<0.0 || fphi2>360.0){  
          ftheta2 = G4RandGauss::shoot(0.0,theta_target_broadening_sigma); // cout<<"ftheta2 = "<<ftheta2<<endl;
          fphi2   = CLHEP::RandFlat::shoot(0.0,360.0);                     // cout<<"fphi2 = "<<fphi2<<endl;
        }
        
	fphi2   = fphi2*degree;   // cout<<"fphi2 = "<<fphi2<<endl;
	ftheta2 = ftheta2*degree; // cout<<"ftheta2 = "<<ftheta2<<endl;
        
        pva[0] = cos(fphi2) * tan(ftheta2) + pvb[0];   // vector after the fragmentation					
        pva[1] = sin(fphi2) * tan(ftheta2) + pvb[1];
        pva[2] = cos(ftheta2) * tan(ftheta2) + pvb[2]; // cout<<"z_pva = "<<z_pva<<endl;
        
        double length1 = sqrt(pva[0]*pva[0] + pva[1]*pva[1] + pva[2]*pva[2]); // cout<<"length1 = "<<length1<<endl;
        
        for(int j=0;j<3;j++) pva[j] = pva[j]/length1; // cout<<"x_pva = "<<x_pva<<endl;
        // End Broadening in angles due to fragmentation in the target
        //-----------------------------------------------------------
      }
      else {
        for(int j=0;j<3;j++){ pva[j] = pvb[j];}       
      }
      // Changing tempparticle to the fragment!!!!!!!!!!!!!!!!!!!
      if(deltazet!=0  || deltamass!=0){
        sprintf(tempparticle,"/gun/ion %i %i %i",z_f,mass_f,charge_f);  
        UI->ApplyCommand("/gun/particle ion");
        UI->ApplyCommand(tempparticle);
      } 	 
      kin->GetParticleGun()->SetParticleEnergy(energy_vertex*MeV);
      kin->GetParticleGun()->SetParticleMomentumDirection(G4ThreeVector(pva[0]*m,pva[1]*m,pva[2]*m));
      kin->GetParticleGun()->SetParticlePosition(G4ThreeVector(p0[0]*cm,p0[1]*cm,p0[2]*cm));		
      
      // Not really necessary
      steppingaction->SetInteractionPoint(9999.0,9999.0,9999.0);
      
      // Moving the decay point already to the point of fragmentation
      // the later "real" decay point will be added by the time they need to decay times velocity and vector
      for(int j=0;j<3;j++){ g0[j] = p0[j];}       
      
      
      // Selecting the populated excitation level
      float randNumber       = CLHEP::RandFlat::shoot(0.0,gTotalLevelExcitationProbability);
      int populatedLevelID   = 0;
      int decayIntoLevelID   = 0;
      float excitationEnergy = 1.;
      float decayEnergy      = 0.;
      
      G4double meanlife, lambda, decayTime;
      for(int j=0;j<gNumberOfLevels;j++){
        if(randNumber>=SLevel[j].beginOfTotalExcitationProbability && 
           randNumber<SLevel[j].endOfTotalExcitationProbability)
          populatedLevelID = j; 
      }
      // cout<<"Populated Level:"<<populatedLevelID<<endl;
      //Ok, we have the initialy excited level. Now we have to determine the decay pattern
      while(excitationEnergy != 0. && SLevel[populatedLevelID].numberOfDecayBranches >0 && 
            SLevel[populatedLevelID].totalDecayProbability>0.){
        randNumber = CLHEP::RandFlat::shoot(0.0,SLevel[populatedLevelID].totalDecayProbability);
        for(int j=0;j<SLevel[populatedLevelID].numberOfDecayBranches;j++){  
          if(randNumber>=SLevel[populatedLevelID].beginOfDecayProbability[j] && randNumber<SLevel[populatedLevelID].endOfDecayProbability[j]){ 
            decayIntoLevelID = SLevel[populatedLevelID].decayIntoLevelId[j];
            decayEnergy      = SLevel[populatedLevelID].energy - SLevel[decayIntoLevelID].energy;
            if(decayEnergy<0.){cout<<"Decay energy smaller than zero. Aborting program."<<endl; abort();}
          }
        }
        
        // The beam has to be shot up to the decay point. For simplicity, I assume that gamma_temp is constant during that time,
        // which causes errors of about 1% or so, depending on the beta spread in the target.
        halflife                     = SLevel[populatedLevelID].halflife;
        meanlife                     = halflife*0.001/log(2.0)*gamma_temp*ns;
        lambda                       = 1.0/(meanlife/ns);
        decayTime                    = CLHEP::RandExponential::shoot(1/lambda)*ns; // Only the decay time for the level!!!
        decay_time_after_interaction = decay_time_after_interaction + decayTime;   // Decay time from reaction point
        
        steppingaction->SetTime(0.);
        steppingaction->SetDecayTime(decayTime);
        steppingaction->SetAfterGammaExcitation(TRUE);
        // Setting the position before starting the beam to get the moved position within the target
        steppingaction->InitialParticlePosition(G4ThreeVector(g0[0]*cm,g0[1]*cm,g0[2]*cm));
        
        float energy_deexcite=0.;
        // cout<<"Starting Beam"<<endl;
        if(dEdXTableOption==0 && energy_vertex>0.){
          runManager->BeamOn(1);
          // Getting energy at deexcitation time.

          if(steppingaction->GetFlagDecayWithinTarget()==TRUE) 
            energy_deexcite = steppingaction->GetTotalEnergyAtPointOfGammaDecay()/MeV;
          else energy_deexcite = steppingaction->GetTotalEnergyAfterTarget()/MeV;

          // Reseting the hi energy for the next gamma decay:
          kin->GetParticleGun()->SetParticleEnergy(energy_deexcite*MeV);

          // Getting beta at deexcitation time.
          gamma_temp = (931.494+energy_deexcite/mass_f)/931.494;
          beta_r     = sqrt(1.0 - 1.0/gamma_temp/gamma_temp);

          // Getting position at deexcitation time.
          g0[0] = g0[0] + steppingaction->GetMovedTrackInTarget().getX()/cm; // cout<<"x_g0 :"<<x_g0<<endl;
          g0[1] = g0[1] + steppingaction->GetMovedTrackInTarget().getY()/cm; // cout<<"y_g0 :"<<y_g0<<endl;
          g0[2] = g0[2] + steppingaction->GetMovedTrackInTarget().getZ()/cm; // cout<<"z_g0 :"<<z_g0<<endl;
   
          if(steppingaction->GetFlagDecayWithinTarget()==FALSE){
            G4double timeThrougTarget = steppingaction->GetTime();
            g0[0] = g0[0] + (decayTime-timeThrougTarget)/ns*29.97925*beta_r*pva[0];
            g0[1] = g0[1] + (decayTime-timeThrougTarget)/ns*29.97925*beta_r*pva[1];
            g0[2] = g0[2] + (decayTime-timeThrougTarget)/ns*29.97925*beta_r*pva[2];
          }

          // Reseting the hi position for the next gamma decay:
          kin->GetParticleGun()->SetParticlePosition(G4ThreeVector(g0[0]*cm,g0[1]*cm,g0[2]*cm));
        }
        //_________________________________________________________________________________________
	//        else if(dEdXTableOption==1 && energy_vertex>0.){
        else if(dEdXTableOption==1){
	  if(energy_vertex <= 0)
	    energy_vertex = 0.1;
          float energyHalfStep = (fragmentdEdX[0][0] - fragmentdEdX[0][1])/2;
          // Have to calculate the energy loss myself:
          for(int j=0;j<n_dEdX_values;j++){
            if(fragmentdEdX[0][j]+ energyHalfStep >energy_vertex/mass_f &&  
               fragmentdEdX[0][j]- energyHalfStep <energy_vertex/mass_f){
              float dummyEnergy1  = energy_vertex;
              float dummyEnergy2  = fragmentdEdX[0][j+1] + energyHalfStep;
              float dummyTime     = 0.;
              float dummyPosition = g0[2];
              int dummyCounter    = j;
              double dummyGamma   = 0.;
              double dummyBeta    = 0.;
              while(dummyTime<decay_time_after_interaction){
                if(dummyPosition>thickness/2.0) break;
                dummyGamma    = (931.494 + dummyEnergy1/mass_f)/931.494;
                dummyBeta     = sqrt(1.0 - 1.0/dummyGamma/dummyGamma);
                dummyTime     = dummyTime + 0.1*defaultCutValue/(dummyBeta*29.97925);  //have to convert into cm because beamdEdX is given in MeV/cm**2
                dummyEnergy1  = dummyEnergy1 - fragmentdEdX[1][dummyCounter]*0.1*defaultCutValue * density[kind_target]*1000;
                dummyPosition = dummyPosition + 0.1*defaultCutValue;            
                g0[2]          = dummyPosition;
                if(dummyEnergy1/mass_f < dummyEnergy2){
                  dummyCounter++;
                  dummyEnergy2 = fragmentdEdX[0][dummyCounter+1] + energyHalfStep;
                }
              }
              if(dummyTime<decay_time_after_interaction){
                g0[2] = g0[2] + (decay_time_after_interaction - dummyTime) * (dummyBeta*29.97925);
              }
              energy_deexcite = dummyEnergy1;
            }
          }
          // Getting beta at deexcitation time.
          gamma_temp = (931.494+energy_deexcite/mass_f)/931.494;
          beta_r     = sqrt(1.0 - 1.0/gamma_temp/gamma_temp);
        }
        else beta_r = 0.;
	
	if(beta_r == 0)
	  cout << " Warning beta_r = 0 " << endl;
        //_________________________________________________________________________________________
        

        //------------------------------------------ 
        //Now comes the deexcitation part!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //------------------------------------------
      
        // Determine the theta distribution!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        double ran_homo = CLHEP::RandFlat::shoot(0.0,sum_dist);
        for(int mmm=1;mmm<18001;mmm++){
	  if(ran_homo>=sum_dist_uptonow[mmm-1] && ran_homo<=sum_dist_uptonow[mmm]){
            theta_gamma_rest = mmm/100.0/180.0*3.14159;
          }
        }
    
        theta_gamma_lab       = acos((cos(theta_gamma_rest)+beta_r)/(1.0+beta_r*cos(theta_gamma_rest)));
        G4ThreeVector vec_in  = G4ThreeVector(pva[0], pva[1], pva[2]);
        G4ThreeVector vec_out = GetNextVector(vec_in, theta_gamma_lab);

        if(vec_out.getX()==-999.9 && vec_out.getY()==-999.9 && vec_out.getZ()==-999.9){
          restart_loop = 1; 
          continue;
        }
        gv[0] = vec_out.getX();
        gv[1] = vec_out.getY();
        gv[2] = vec_out.getZ();

        // The energy after the target		
        double energy_after_target = 0;
        if(dEdXTableOption==0) energy_after_target = steppingaction->GetTotalEnergyAfterTarget()/MeV;
        //_________________________________________________________________________________________
        else if(dEdXTableOption==1){
          float energyHalfStep = (fragmentdEdX[0][0] - fragmentdEdX[0][1])/2;
          // Have to calculate the energy loss myself:

          for(int j=0;j<n_dEdX_values;j++){
            if(fragmentdEdX[0][j] + energyHalfStep >energy_vertex/mass_f && 
               fragmentdEdX[0][j] - energyHalfStep <energy_vertex/mass_f){
              // cout<<"condition fullfilled: "<<j<<endl;
              float dummyEnergy1  = energy_vertex;
              float dummyEnergy2  = fragmentdEdX[0][j+1] + energyHalfStep;
              float dummyPosition = p0[2];
              int dummyCounter    = j;
              while(dummyPosition<(thickness/2.0)){
                dummyEnergy1 = dummyEnergy1 - fragmentdEdX[1][dummyCounter]*0.1*defaultCutValue * density[kind_target]*1000;
                dummyPosition = dummyPosition + 0.1*defaultCutValue;            
                if(dummyEnergy1/mass_f < dummyEnergy2){
                  dummyCounter++;
                  dummyEnergy2 = fragmentdEdX[0][dummyCounter+1] + energyHalfStep;
                }
	      }
              energy_after_target = dummyEnergy1;
            }
          }
        }
	
	// if(energy_after_target == 0)
// 	  {
// 	    cout << " Warning! energy after target = 0 , stopping simulation " << endl;
// 	    exit(0);
// 	  }
	
	
        //_________________________________________________________________________________________

        gamma_temp                 = (931.494+energy_after_target/mass_f)/931.494;
        beta_a                     = sqrt(1.0 - 1.0/gamma_temp/gamma_temp);
	    
        e_rest    = decayEnergy;
        e_doppler = e_rest*(sqrt(1.0-beta_r*beta_r)/(1.0-beta_r*cos(theta_gamma_lab)));
	        
        evnum = i;
	
        t->Fill(); // cout<<"Filling root tree"<<endl;

        //Setting the new excitation energy and populated level:
        excitationEnergy = SLevel[decayIntoLevelID].energy;
        populatedLevelID = SLevel[decayIntoLevelID].ID;                   
        decayIDevnum++;   
      }// end while loop throug decay scheme
    }// end if
    if(restart_loop==1) continue; 
    i = i + 1;
  } // end while loop through events  
}

G4ThreeVector GetNextVector(G4ThreeVector vector_in, float theta){
  double x1     = vector_in.getX();
  double y1     = vector_in.getY();
  double z1     = vector_in.getZ();
  double l1     = sqrt(x1*x1 + y1*y1 + z1*z1);
  double theta1 = acos(z1/l1);
  double phi1   = 0.0;

  if(sqrt((x1/l1/sin(theta1))*(x1/l1/sin(theta1)))>1.0) {
    x1     = 0.0;
    y1     = 0.0;
    z1     = 1.0;
    theta1 = 0.0;
    phi1   = 0.0;
  }
  if(sqrt((x1/l1/sin(theta1))*(x1/l1/sin(theta1)))<=1.0) {
    phi1 = acos(x1/l1/sin(theta1));
  }

  G4RotationMatrix rot3D;
  G4ThreeVector rotatedPos;

  G4double g4phi    = CLHEP::RandFlat::shoot(0.0,360.0)*degree;
  G4double g4theta  = theta*rad;
  G4double g4theta1 = theta1*rad;
  G4double g4phi1   = phi1*rad;

  rot3D.set(0, 0, 0);
  rot3D.rotateY(g4theta);  
  rot3D.rotateZ(g4phi);
  rot3D.rotateY(g4theta1);  
  rot3D.rotateZ(g4phi1);
	      
  rotatedPos = rot3D(G4ThreeVector(0.0,0.0,1.0));
  return rotatedPos;
}


void SaveAgataAsciiFile(void)
{
  Int_t filenamelength = strlen(root_out);
  char ascii_out[200];
  if(filenamelength > 200)
    {
      cout << " ERROR Increase size of ascii_out, exiting..."  << endl;
      exit(-1);
    }

  sprintf(ascii_out,"%s",root_out);

  char * pch;
  pch = strstr (ascii_out,".root");
  strncpy (pch,".txt",5);


  int z_tar = 0, mass_tar = 0;
  switch(kind_target)
    {
    case 1:
      z_tar = 79;
      mass_tar = 197;
      break;
    case 2:
      z_tar = 4;
      mass_tar = 9;
      break;
    case 3:
      z_tar = 6;
      mass_tar = 12;
      break;
    case 4:
      z_tar = 26;
      mass_tar = 56;
      break;
    case 5:
      z_tar = 82;
      mass_tar = 208;
      break;
    case 6:
      z_tar = 1;
      mass_tar = 1;
      break;
    }


  FILE *fout = fopen(ascii_out,"w");
  Int_t nentries = (Int_t)t->GetEntries();
  cout << " number of entries: " << nentries << endl;

  float eEmi = 0, gamma = 0;
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );

  fprintf(fout,"FORMAT 0 0\n");
  fprintf(fout,"#____________________________________________________________________\n");
  fprintf(fout,"#\n");
  fprintf(fout,"#              Ascii Events File with AGATA format                   \n");
  fprintf(fout,"#\n");
  fprintf(fout,"# File automatically generated via SaveAgataAsciiFile() on %s",asctime (timeinfo));
  fprintf(fout,"# from the tree saved in root-file:\n#    %s\n",root_out);
  fprintf(fout,"# with %.0e events. \n",(float) nentries);
  fprintf(fout,"#\n");
  fprintf(fout,"#_____________________________________________________________________\n");
  fprintf(fout,"REACTION %5d %5d %5d %5d %5.1f\n",z,mass,z_tar,mass_tar,energy_p);// ???
  fprintf(fout,"EMITTED 1 1\n");// gamma + fragment1 (proton) + fragment2 ???
  //  for(int i=0;i<nentries;i++)


  int bs_event_nr=0, gbsMg = 0;
  float gbsdx=0, gbsdy=0, gbsdz=0, gbsEg=0;
  float gbsposx = 0, gbsposy = 0, gbsposz = 0;

  TFile* file;
  TH1F *hMg;
  TH1F *hEg;
  TH1F *hdx;
  TH1F *hdy;
  TH1F *hdz;
  TH1F *hz;

  if(flag_add_bkg)
    {
      file = TFile::Open("./bkg_tests/bkg_model.root");
      hMg = (TH1F*) file->Get("hMg");
      hEg = (TH1F*) file->Get("hEg");
      hdx = (TH1F*) file->Get("hdx");
      hdy = (TH1F*) file->Get("hdy");
      hdz = (TH1F*) file->Get("hdz");
      hz = (TH1F*) file->Get("hz");
    }

  for(int i=0;i<nentries;i++) // at each event EMITTER:= FRAGMENT and EMITTED:=g-ray after fragment de-excitation
    {
      t->GetEntry(i);
      //      fprintf(fout,"REACTION %5d %5d %5d %5d %5.1f\n",z,mass,z_tar,mass_tar,energy_p);// ???
      fprintf(fout,"$\n");
      gamma = 1/sqrt(1-beta_a*beta_a);
      eEmi = 931.494*(gamma - 1)*mass_f;
      fprintf(fout,"-101 %5d %5d %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \n",//fragment
	     z_f,mass_f,eEmi,pva[0],pva[1],pva[2],p0[0],p0[1],p0[2]);
      fprintf(fout,"1 %5.1f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f\n",//gamma
	     e_doppler,gv[0],gv[1],gv[2],g0[0],g0[1],g0[2]);

      int myj = 0;
      int ngoverthr = 0;
      if(flag_add_bkg)
	{
	  if(((int) bkg_multiplicity) == 0)
	    gbsMg = (int) hMg->GetRandom();//bkgrandom multiplicity
	  else
	    gbsMg = (int) bkg_multiplicity;//bkgrandom multiplicity

	  for(myj=0;myj<gbsMg;myj++)
	    {
	      gbsEg = hEg->GetRandom(); // bs g-energy (keV)
	      gbsposx = g0[0]; // bs g-position xy=fragment z=bkgrandom
	      gbsposy = g0[1];
	      gbsposz = hz->GetRandom();
	      gbsdx = hdx->GetRandom(); // direction NEEDS TO BE NORMALIZED GENERATED!!!! (not yet made)
	      gbsdy = hdy->GetRandom();
	      gbsdz = hdz->GetRandom();
	  
	      if(gbsEg>100)//100keV Threshold for Bkg
		{
		  ngoverthr++;
		  
		  fprintf(fout,"1 %5.1f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f\n",//BS gamma
			  gbsEg,gbsdx,gbsdy,gbsdz,gbsposx,gbsposy,gbsposz);
		  
		  //		  printf("DEBUG writing BS Event %d  bs bkg gamma nr %d of %d with Eg = %5.1f \n",//BS gamma
		  //i,ngoverthr,gbsMg,gbsEg);
		}
	    }
	}
    }
}



// void SaveAgataAsciiFilePlusBSBkg(void)
// {
//   Int_t filenamelength = strlen(root_out);
//   char ascii_out[200];
//   if(filenamelength > 200)
//     {
//       cout << " ERROR Increase size of ascii_out, exiting..."  << endl;
//       exit(-1);
//     }

//   sprintf(ascii_out,"%s",root_out);

//   char * pch;
//   pch = strstr (ascii_out,".root");
//   strncpy (pch,".txt",5);


//   int z_tar = 0, mass_tar = 0;
//   switch(kind_target)
//     {
//     case 1:
//       z_tar = 79;
//       mass_tar = 197;
//       break;
//     case 2:
//       z_tar = 4;
//       mass_tar = 9;
//       break;
//     case 3:
//       z_tar = 6;
//       mass_tar = 12;
//       break;
//     case 4:
//       z_tar = 26;
//       mass_tar = 56;
//       break;
//     case 5:
//       z_tar = 82;
//       mass_tar = 208;
//       break;
//     case 6:
//       z_tar = 1;
//       mass_tar = 1;
//       break;
//     }


//   FILE *fout = fopen(ascii_out,"w");
//   Int_t nentries = (Int_t)t->GetEntries();
//   cout << " number of entries: " << nentries << endl;

//   float eEmi = 0, gamma = 0;
//   time_t rawtime;
//   struct tm * timeinfo;

//   //---------------------
//   // Background Part
//   //---------------------
//   const float mean_BremBkgMult = 2340;//mean at. bkg multiplicity
//   const float sigma_BremBkgMult = 50;//mean at. bkg multiplicity sigma
//   //---------------------


//   time ( &rawtime );
//   timeinfo = localtime ( &rawtime );

//   fprintf(fout,"FORMAT 0 0\n");
//   fprintf(fout,"#____________________________________________________________________\n");
//   fprintf(fout,"#\n");
//   fprintf(fout,"#              Ascii Events File with AGATA format                   \n");
//   fprintf(fout,"#\n");
//   fprintf(fout,"# File automatically generated via SaveAgataAsciiFile() on %s",asctime (timeinfo));
//   fprintf(fout,"# from the tree saved in root-file:\n#    %s\n",root_out);
//   fprintf(fout,"# with %.0e events. \n",(float) nentries);
//   fprintf(fout,"#\n");
//   fprintf(fout,"#_____________________________________________________________________\n");
//   fprintf(fout,"REACTION %5d %5d %5d %5d %5.1f\n",z,mass,z_tar,mass_tar,energy_p);// ???
//   fprintf(fout,"EMITTED 1 1\n");// gamma + fragment1 (proton) + fragment2 ???
//   //  for(int i=0;i<nentries;i++)

//   int bs_event_nr=0, gbsMg = 0;
//   float gbsdx=0, gbsdy=0, gbsdz=0, gbsEg=0;
//   float gbsposx = 0, gbsposy = 0, gbsposz = 0;
//   int ngoverthr = 0;

//   TFile* file;
//   TH1F *hMg;
//   TH1F *hEg;
//   TH1F *hdx;
//   TH1F *hdy;
//   TH1F *hdz;
//   TH1F *hz;

//   if(flag_add_bkg)
//     {
//       file = TFile::Open("./bkg_tests/bkg_model.root");
//       hMg = (TH1F*) file->Get("hMg");
//       hEg = (TH1F*) file->Get("hEg");
//       hdx = (TH1F*) file->Get("hdx");
//       hdy = (TH1F*) file->Get("hdy");
//       hdz = (TH1F*) file->Get("hdz");
//       hz = (TH1F*) file->Get("hz");
//     }

//   for(int i=0;i<nentries;i++) // at each event EMITTER:= FRAGMENT and EMITTED:=g-ray after fragment de-excitation
//     {
//       t->GetEntry(i);
//       //      fprintf(fout,"REACTION %5d %5d %5d %5d %5.1f\n",z,mass,z_tar,mass_tar,energy_p);// ???
//       fprintf(fout,"$\n");
//       gamma = 1/sqrt(1-beta_a*beta_a);
//       eEmi = 931.494*(gamma - 1)*mass_f;
//       fprintf(fout,"-101 %5d %5d %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f %5.3f \n",//fragment
// 	     z_f,mass_f,eEmi,pva[0],pva[1],pva[2],p0[0],p0[1],p0[2]);
//       fprintf(fout,"1 %5.1f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f\n",//gamma
// 	     e_doppler,gv[0],gv[1],gv[2],g0[0],g0[1],g0[2]);


//       if(flag_add_bkg)
// 	{
// 	  gbsMg = (int) hMg->GetRandom();//bkgrandom multiplicity
// 	  for(int jc=0;j<gbsMg;jc++)
// 	    {
// 	      gbsEg = hEg->GetRandom(); // bs g-energy (keV)
// 	      gbsposx = g0[0]; // bs g-position xy=fragment z=bkgrandom
// 	      gbsposy = g0[1];
// 	      gbsposz = hz->GetRandom();
// 	      gbsdx = hdx->GetRandom(); // direction NEEDS TO BE NORMALIZED GENERATED!!!! (not yet made)
// 	      gbsdy = hdy->GetRandom();
// 	      gbsdz = hdz->GetRandom();
// 	    }
// 	  if(bsEg>100)
// 	    {
// 	      ngoverthr++;
// 	      fprintf(fout,"1 %5.1f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f  %5.3f\n",//BS gamma
// 		      gbsEg,gbsdx,gbsdy,gbsdz,gbsposx,gbsposy,gbsposz);

// 	      printf("DEBUG Event %d  bs bkg gamma nr %d Eg = %5.1f  dirx= %5.3f  diry= %5.3f  dirz= %5.3f  posx= %5.3f  posy= %5.3f  posz= %5.3f\n",//BS gamma
// 		      i,j,ngoverthr,gbsEg,gbsdx,gbsdy,gbsdz,gbsposx,gbsposy,gbsposz);
// 	    }
// 	}
//     }
// }

