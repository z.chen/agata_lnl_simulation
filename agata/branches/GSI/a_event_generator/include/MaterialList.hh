#ifndef MaterialList_h
#define MaterialList_h 1

#include "G4ios.hh"
#include <iostream>
#include "G4Material.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class MaterialList
{
  public:
  
    MaterialList();
   ~MaterialList();

    G4Material *GetMaterial(G4String materialName);
  private:
    //All defined materials:
    G4Material* H2O,*Air,*vacuum,*Be,*C,*Al,*Si,*Fe,*Ge,*Sn,*Au,*Pb,*Sci,*MgO,*BaF2,*NaI,*CsI,*LaBr,*LH2;
};
#endif
