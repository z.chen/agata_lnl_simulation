#ifndef PhysListEmLivermore_h
#define PhysListEmLivermore_h 1

#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
class PhysListEmLivermore : public G4VPhysicsConstructor
{
  public: 
    PhysListEmLivermore(const G4String& name = "Livermore");
   ~PhysListEmLivermore();

  public: 
    // This method is dummy for physics
    void ConstructParticle() {};
 
    // This method will be invoked in the Construct() method.
    // each physics process will be instantiated and
    // registered to the process manager of each particle type 
    void ConstructProcess();
};
#endif








