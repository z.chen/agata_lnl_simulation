
int plot(void)//DRAWS BKG AND CREATES BKG MODEL IN ROOT FILE
{
  // specify name of ASCII file from Pavel's simulation containing bkg events
  ifstream finp("/data.local/cdomingo/simulations/brem_bkg_files_pavel/fragment_Z23_A46.txt");
  // set this flag for saving the root file with the histograms
  bool flag_save_file = false;

  const int tot_events = 50;
  int ev_cr = 0;

  if(flag_save_file)
    TFile* file = TFile::Open("bkg_model.root", "NEW");  // my output file (no owrite)

  TH1F *hMg = new TH1F("hMg","Gamma Multiplicity",10000,0,10000);
  TH1F *hEg = new TH1F("hEg","Gamma Energy Distribution",1000,0,5000);//5keV/ch
  TH1F *hdx = new TH1F("hdx","Gamma X Direction",100,-1,1);//
  TH1F *hdy = new TH1F("hdy","Gamma Y Direction",100,-1,1);//
  TH1F *hdz = new TH1F("hdz","Gamma Z Direction",100,-1,1);//
  TH1F *hz = new TH1F("hz","Gamma Z Position",3000,-2000,1000);//

  //  TH1F *hrndEg = new TH1F("hrndEg","",1000,0,5000);//5keV/ch
  //  setStyle(hrndEg);
  setStyle(hMg);
  setStyle(hEg);
  setStyle(hdx);
  setStyle(hdy);
  setStyle(hdz);
  setStyle(hz);


  int event_nr=0;
  float firstvalue=0, px=0, py=0, pz=0, dx=0, dy=0, dz=0, Eg=0, tg=0;

  int Mg = 0;

  

  while(!finp.eof())
    //  while(ev_cr <= tot_events)
    {
      finp >> firstvalue;
      if((int) firstvalue == -30)
	{
	  finp >> event_nr;
	  ev_cr++;
	  if((ev_cr)%1000==0)
	    cout <<  ev_cr << " Events Processed " << endl;
	  hMg->Fill(Mg);
	  Mg = 0;
	  
	}
      else
	{
	  Mg++;
	  Eg = firstvalue;
	  finp >> px >> py >> pz >> dx >> dy >> dz >> tg;
	  
	  hEg->Fill(Eg);
	  hdx->Fill(dx);
	  hdy->Fill(dy);
	  hdz->Fill(dz);
	  hz->Fill(pz);
	}
    }
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  TCanvas *c = new TCanvas("c","",1);
  c->SetFillColor(10);
  c->Divide(2,3);
  c->cd(1);
  hEg->GetXaxis()->SetTitle("E_{#gamma} (keV)");
  hEg->GetYaxis()->SetTitle("Counts");
  hEg->DrawCopy();
  c->cd(2);
  hdx->GetXaxis()->SetTitle("direction x");
  hdx->GetYaxis()->SetTitle("Counts");
  hdx->DrawCopy();
  c->cd(3);
  hdy->GetXaxis()->SetTitle("direction y");
  hdy->GetYaxis()->SetTitle("Counts");
  hdy->DrawCopy();
  c->cd(4);
  hdz->GetXaxis()->SetTitle("direction z");
  hdz->GetYaxis()->SetTitle("Counts");
  hdz->DrawCopy();
  c->cd(5);
  hMg->GetXaxis()->SetTitle("M_{#gamma} (keV)");
  hMg->GetYaxis()->SetTitle("Counts");
  hMg->DrawCopy();
  c->cd(6);
  hz->GetXaxis()->SetTitle("position z");
  hz->GetYaxis()->SetTitle("Counts");
  hz->DrawCopy();

//   //  for(int i=0;i<100;i++)
//   hrndEg->FillRandom(hEg,1);

//   TCanvas *c2 = new TCanvas("c2","",1);
//   c2->SetFillColor(10);
//   hrndEg->DrawCopy();

  if(flag_save_file)
    {
      hMg->Write();
      hEg->Write();
      hdx->Write();
      hdy->Write();
      hdz->Write();
      hz->Write();  
      file->Write();
      file->Close();
    }

  return 0;
}

void setStyle(TH1F *h)
{
  h->SetLineColor(2);
  h->SetLineWidth(2);
  h->GetXaxis()->SetTitleSize(0.06);
  h->GetYaxis()->SetTitleSize(0.06);
  h->GetXaxis()->SetLabelSize(0.06);
  h->GetYaxis()->SetLabelSize(0.06);
}

void readBkgModel()
{
  TFile* file = TFile::Open("./bkg_model.root");  // my output file

  TH1F *hMg = (TH1F*) file->Get("hMg");
  TH1F *hEg = (TH1F*) file->Get("hEg");
  TH1F *hdx = (TH1F*) file->Get("hdx");
  TH1F *hdy = (TH1F*) file->Get("hdy");
  TH1F *hdz = (TH1F*) file->Get("hdz");
  TH1F *hz = (TH1F*) file->Get("hz");

  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);

  TCanvas *c = new TCanvas("c","",1);
  c->SetFillColor(10);
  c->Divide(2,3);
  c->cd(1);
  hEg->DrawCopy();
  c->cd(2);
  hdx->DrawCopy();
  c->cd(3);
  hdy->DrawCopy();
  c->cd(4);
  hdz->DrawCopy();
  c->cd(5);
  hMg->DrawCopy();
  c->cd(6);
  hz->DrawCopy();  
}
