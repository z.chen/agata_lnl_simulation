#include "PhysListEmPenelope.hh"
#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"

#ifdef G4V495
#include "G4PhysicsListHelper.hh"
#endif

// gamma
#ifdef G4V495
#include "G4ComptonScattering.hh"
#include "G4PenelopeComptonModel.hh"
#include "G4GammaConversion.hh"
#include "G4PenelopeGammaConversionModel.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4PenelopePhotoElectricModel.hh"
#include "G4RayleighScattering.hh" 
#include "G4PenelopeRayleighModel.hh"
#else
#include "G4PenelopeCompton.hh"
#include "G4PenelopeGammaConversion.hh"
#include "G4PenelopePhotoElectric.hh"
#include "G4PenelopeRayleigh.hh" 
#endif


// e- and e+
#if defined G4V494 || G4V495
#include "G4eMultipleScattering.hh"
#include "G4UniversalFluctuation.hh"
#else
#include "G4MultipleScattering.hh"
#endif

#ifdef G4V495
#include "G4eIonisation.hh"
#include "G4PenelopeIonisationModel.hh"
#include "G4eBremsstrahlung.hh"
#include "G4PenelopeBremsstrahlungModel.hh"
#else
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#endif


// e+ only
#ifdef G4V495
#include "G4eplusAnnihilation.hh"
#include "G4PenelopeAnnihilationModel.hh"
#else
#include "G4eplusAnnihilation.hh"
#endif

#ifdef G4V10
#include "G4PenelopePhotoElectricModel.hh"   
#endif

// Muons
#ifndef G4V47
#include "G4MuMultipleScattering.hh"
#endif
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

// hadrons
#ifndef G4V47
#include "G4hMultipleScattering.hh"
#endif
#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

#ifdef G4V10
using namespace CLHEP;
#endif


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmPenelope::PhysListEmPenelope(const G4String& name)
  :  G4VPhysicsConstructor(name)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmPenelope::~PhysListEmPenelope()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void PhysListEmPenelope::ConstructProcess()
{
  // Add Penelope or standard EM Processes
#ifdef G4V495 
 G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
#endif
#ifdef G4V10
  auto theParticleIterator=GetParticleIterator();
#endif

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
     
    if (particleName == "gamma") {
      // gamma
#ifdef G4V495       
      G4PhotoElectricEffect* thePhotoElectricEffect = new G4PhotoElectricEffect();
#ifndef G4V10
      thePhotoElectricEffect->SetModel(new G4PenelopePhotoElectricModel());
      ph->RegisterProcess(thePhotoElectricEffect, particle);
#else
      G4VEmModel* thePenelopePEModel = new G4PenelopePhotoElectricModel();
      thePenelopePEModel->SetHighEnergyLimit(10*GeV);
      thePhotoElectricEffect->SetEmModel(thePenelopePEModel,1);
#endif

      G4ComptonScattering* theComptonScattering = new G4ComptonScattering();
#ifndef G4V10
      theComptonScattering->SetModel(new G4PenelopeComptonModel());
      ph->RegisterProcess(theComptonScattering, particle);
#else
      G4VEmModel* thePenelopeCompModel = new G4PenelopeComptonModel();
      thePenelopeCompModel->SetHighEnergyLimit(1*GeV);
      theComptonScattering->SetEmModel(thePenelopeCompModel,1);

#endif

      G4GammaConversion* theGammaConversion = new G4GammaConversion();
#ifndef G4V10
      theGammaConversion->SetModel(new G4PenelopeGammaConversionModel());
      ph->RegisterProcess(theGammaConversion, particle);
#else
      G4VEmModel* thePenelopeGCModel = new G4PenelopeGammaConversionModel();
      thePenelopeGCModel->SetHighEnergyLimit(1*GeV);
      theGammaConversion->SetEmModel(thePenelopeGCModel,1);

#endif

      G4RayleighScattering* theRayleigh = new G4RayleighScattering();
#ifndef G4V10
      theRayleigh->SetModel(new G4PenelopeRayleighModel());
      ph->RegisterProcess(theRayleigh, particle);
#else
      G4VEmModel* thePenelopeRayleighModel = new G4PenelopeRayleighModel();
      theRayleigh->SetEmModel(thePenelopeRayleighModel,1);

#endif


#else

      pmanager->AddDiscreteProcess(new G4PenelopePhotoElectric);
      pmanager->AddDiscreteProcess(new G4PenelopeCompton);
      pmanager->AddDiscreteProcess(new G4PenelopeGammaConversion);
      pmanager->AddDiscreteProcess(new G4PenelopeRayleigh);
#endif
     
    } else if (particleName == "e-") {

      //electron
#if defined G4V47 || G4V48
      pmanager->AddProcess(new G4MultipleScattering,      -1, 1, 1);
#else
      pmanager->AddProcess(new G4eMultipleScattering,      -1, 1, 1);
#endif

      pmanager->AddProcess(new G4eIonisation,     -1, 2, 2);
      pmanager->AddProcess(new G4eBremsstrahlung, -1,-1, 3);


#ifdef G4V495
      G4eMultipleScattering* msc = new G4eMultipleScattering();
      ph->RegisterProcess(msc, particle);
      
      // Ionisation
      G4eIonisation* eIoni = new G4eIonisation();
      eIoni->SetEmModel(new G4PenelopeIonisationModel());
      eIoni->SetFluctModel(new G4UniversalFluctuation() );
      ph->RegisterProcess(eIoni, particle);
      
      // Bremsstrahlung
      G4eBremsstrahlung* eBrem = new G4eBremsstrahlung();
      eBrem->SetEmModel(new G4PenelopeBremsstrahlungModel());
      ph->RegisterProcess(eBrem, particle);
#endif
    
    } else if (particleName == "e+") {
      //positron      
#if defined  G4V47 || G4V48
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4eMultipleScattering, -1, 1, 1);
#endif

      pmanager->AddProcess(new G4eIonisation,     -1, 2, 2);
      pmanager->AddProcess(new G4eBremsstrahlung, -1,-1, 3);      
      pmanager->AddProcess(new G4eplusAnnihilation,    0,-1, 4);


    } else if( particleName == "mu+" || 
               particleName == "mu-"    ) {
      //muon  
#ifdef G4V47
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4MuMultipleScattering, -1, 1, 1);
#endif

      pmanager->AddProcess(new G4MuIonisation,       -1, 2, 2);
      pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 3, 3);
      pmanager->AddProcess(new G4MuPairProduction,   -1, 4, 4);       
     
    } else if( particleName == "alpha" || particleName == "GenericIon" ) { 
#ifdef G4V47
     pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4hMultipleScattering, -1, 1,1);
#endif

      pmanager->AddProcess(new G4ionIonisation,      -1, 2,2);
     
    } else if ((!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) && 
	       (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
#ifdef G4V47
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4hMultipleScattering, -1, 1, 1);
#endif
      pmanager->AddProcess(new G4hIonisation,        -1, 2, 2);
    }
  }
}
