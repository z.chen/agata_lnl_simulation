#include "PhysListEmStandard.hh"
#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "G4hMultipleScattering.hh"
#if defined G4V494 || G4V495
#include "G4eMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#else
#include "G4MultipleScattering.hh"
#endif



#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

#include "G4EmProcessOptions.hh"


#ifdef G4V10
using namespace CLHEP;
#endif



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmStandard::PhysListEmStandard(const G4String& name)
   :  G4VPhysicsConstructor(name)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmStandard::~PhysListEmStandard()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void PhysListEmStandard::ConstructProcess()
{
  // Add standard EM Processes
#ifdef G4V10
  auto theParticleIterator=GetParticleIterator();
#endif

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
    //G4cout<<"The name of the particle is "<<particleName<<G4endl;
     
    if (particleName == "gamma") {
      // gamma         
      pmanager->AddDiscreteProcess(new G4PhotoElectricEffect);
      pmanager->AddDiscreteProcess(new G4ComptonScattering);
      pmanager->AddDiscreteProcess(new G4GammaConversion);
      
    } else if (particleName == "e-") {
      //electron
#if defined G4V494 || G4V495
      pmanager->AddProcess(new G4eMultipleScattering, -1, 1,1);
#else
      pmanager->AddProcess(new G4MultipleScattering, -1, 1,1);
#endif
      pmanager->AddProcess(new G4eIonisation,        -1, 2,2);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 3,3);
	    
    } else if (particleName == "e+") {
      //positron
#if defined G4V494 || G4V495
      pmanager->AddProcess(new G4eMultipleScattering, -1, 1,1);
#else
      pmanager->AddProcess(new G4MultipleScattering, -1, 1,1);
#endif
      pmanager->AddProcess(new G4eIonisation,        -1, 2,2);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 3,3);
      pmanager->AddProcess(new G4eplusAnnihilation,   0,-1,4);
      
    } else if( particleName == "mu+" || 
               particleName == "mu-"    ) {
      //muon  
#if defined G4V494 || G4V495
      pmanager->AddProcess(new G4MuMultipleScattering,-1, 1,1);
#else
      pmanager->AddProcess(new G4MultipleScattering,-1, 1,1);
#endif
      pmanager->AddProcess(new G4MuIonisation,       -1, 2,2);
      pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 3,3);
      pmanager->AddProcess(new G4MuPairProduction,   -1, 4,4);       
     
    } else if( particleName == "alpha" || particleName == "GenericIon" ) { 
      pmanager->AddProcess(new G4hMultipleScattering,-1, 1,1);
      pmanager->AddProcess(new G4ionIonisation,      -1, 2,2);

    } else if ((!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) && 
	       (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
#if defined G4V494 || G4V495
      pmanager->AddProcess(new G4hMultipleScattering,-1,1,1);
#else
      pmanager->AddProcess(new G4MultipleScattering,-1,1,1);
#endif
      //pieter: commented the following and replaced by G4ionIonisation
      pmanager->AddProcess(new G4hIonisation,        -1,2,2);
      // pmanager->AddProcess(new G4ionIonisation,      -1, 2,2);
    }
  }
  G4EmProcessOptions opt;
  opt.SetStepFunction(0.2, 100*um);
  opt.SetSkin(1.);
}
