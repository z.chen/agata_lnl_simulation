#include "PhysListEmLivermore.hh"
#include "G4ParticleDefinition.hh"
#include "G4ProcessManager.hh"


#ifdef G4V495
#include "G4PhysicsListHelper.hh"
#else
#include "G4LowEnergyCompton.hh"
#include "G4LowEnergyGammaConversion.hh"
#include "G4LowEnergyPhotoElectric.hh"
#include "G4LowEnergyRayleigh.hh"
#endif


// gamma
#include "G4PhotoElectricEffect.hh"
#ifdef G4V495
#include "G4LivermorePhotoElectricModel.hh"
#endif

#include "G4ComptonScattering.hh"
#ifdef G4V495
#include "G4LivermoreComptonModel.hh"
#endif

#include "G4GammaConversion.hh"
#ifdef G4V495
#include "G4LivermoreGammaConversionModel.hh"
#endif
 
#if defined G4V494 || G4V495
#include "G4RayleighScattering.hh"
#include "G4LivermoreRayleighModel.hh"
#endif

// e-
#if defined G4V494 || G4V495
#include "G4eMultipleScattering.hh"
#include "G4UniversalFluctuation.hh"
#else
#include "G4MultipleScattering.hh"
#endif

#include "G4eIonisation.hh"
#ifdef G4V495
#include "G4LivermoreIonisationModel.hh"
#endif

#include "G4eBremsstrahlung.hh"
#ifdef G4V495
#include "G4LivermoreBremsstrahlungModel.hh"
#endif


#ifndef G4V495
#include "G4LowEnergyIonisation.hh"
#include "G4LowEnergyBremsstrahlung.hh"
#endif

// e+
#include "G4eplusAnnihilation.hh"

// muons
#include "G4MuMultipleScattering.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"

// hadrons
#include "G4hMultipleScattering.hh"
#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"
#ifndef G4V495
#include "G4hLowEnergyIonisation.hh"
#endif


#ifdef G4V10
using namespace CLHEP;
#endif

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmLivermore::PhysListEmLivermore(const G4String& name)
  :  G4VPhysicsConstructor(name)
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
PhysListEmLivermore::~PhysListEmLivermore()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void PhysListEmLivermore::ConstructProcess()
{
  // Add LowEn or standard EM Processes
#ifdef G4V495
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
#endif
#ifdef G4V10
  auto theParticleIterator=GetParticleIterator();
#endif

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
     
    if (particleName == "gamma") {
      // gamma         
#ifndef G4V495
      pmanager->AddDiscreteProcess(new G4LowEnergyPhotoElectric);
      pmanager->AddDiscreteProcess(new G4LowEnergyCompton);
      pmanager->AddDiscreteProcess(new G4LowEnergyGammaConversion);
      pmanager->AddDiscreteProcess(new G4LowEnergyRayleigh);
#else
      G4PhotoElectricEffect* thePhotoElectricEffect = new G4PhotoElectricEffect();

#ifndef G4V10
      thePhotoElectricEffect->SetModel(new G4LivermorePhotoElectricModel());
      ph->RegisterProcess(thePhotoElectricEffect, particle);
#else
      G4VEmModel* theLivermorePEModel = new G4LivermorePhotoElectricModel();
      theLivermorePEModel->SetHighEnergyLimit(10*GeV);
      thePhotoElectricEffect->SetEmModel(theLivermorePEModel,1);
#endif

      G4ComptonScattering* theComptonScattering = new G4ComptonScattering();
#ifndef G4V10
      theComptonScattering->SetModel(new G4LivermoreComptonModel());
      ph->RegisterProcess(theComptonScattering, particle);
#else
      G4VEmModel* theLivermoreComptonModel = new G4LivermoreComptonModel();
      theLivermoreComptonModel->SetHighEnergyLimit(1*GeV);
      theComptonScattering->SetEmModel(theLivermoreComptonModel,1);
#endif

      G4GammaConversion* theGammaConversion = new G4GammaConversion();
#ifndef G4V10
      theGammaConversion->SetModel(new G4LivermoreGammaConversionModel());
      ph->RegisterProcess(theGammaConversion, particle);
#else
      G4VEmModel* theLivermoreGCModel = new G4LivermoreGammaConversionModel();
      theLivermoreGCModel->SetHighEnergyLimit(1*GeV);
      theGammaConversion->SetEmModel(theLivermoreGCModel,1);
#endif

      G4RayleighScattering* theRayleigh = new G4RayleighScattering();
#ifndef G4V10
      theRayleigh->SetModel(new G4LivermoreRayleighModel());
      ph->RegisterProcess(theRayleigh, particle);
#else
      G4VEmModel* theLivermoreRayleighModel = new G4LivermoreRayleighModel();
      theLivermoreRayleighModel->SetHighEnergyLimit(10*GeV);
      theRayleigh->SetEmModel(theLivermoreRayleighModel,1);
#endif



#endif
      
    } else if (particleName == "e-") {
      //electron
#ifndef G4V495     
#ifdef G4V494
      pmanager->AddProcess(new G4eMultipleScattering,      -1, 1, 1);
#else
      pmanager->AddProcess(new G4MultipleScattering,      -1, 1, 1);
#endif
      pmanager->AddProcess(new G4LowEnergyIonisation,     -1, 2, 2);
      pmanager->AddProcess(new G4LowEnergyBremsstrahlung, -1,-1, 3);
#else
      G4eMultipleScattering* msc = new G4eMultipleScattering();
      ph->RegisterProcess(msc, particle);
      
      // Ionisation
      G4eIonisation* eIoni = new G4eIonisation();
      eIoni->SetEmModel(new G4LivermoreIonisationModel());
      eIoni->SetFluctModel(new G4UniversalFluctuation() );
      ph->RegisterProcess(eIoni, particle);
      
      // Bremsstrahlung
      G4eBremsstrahlung* eBrem = new G4eBremsstrahlung();
      eBrem->SetEmModel(new G4LivermoreBremsstrahlungModel());
      ph->RegisterProcess(eBrem, particle);
#endif
	    
    } else if (particleName == "e+") {
      //positron
#ifndef G4V495    
#ifdef G4V494
      pmanager->AddProcess(new G4eMultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#endif

#else
      pmanager->AddProcess(new G4eMultipleScattering, -1, 1, 1);
#endif
      pmanager->AddProcess(new G4eIonisation,        -1, 2, 2);
      pmanager->AddProcess(new G4eBremsstrahlung,    -1, 3, 3);
      pmanager->AddProcess(new G4eplusAnnihilation,   0,-1, 4);
      
    } else if( particleName == "mu+" || 
               particleName == "mu-"    ) {
      //muon  
#ifdef G4V47
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4MuMultipleScattering, -1, 1, 1);
#endif
      pmanager->AddProcess(new G4MuIonisation,       -1, 2, 2);
      pmanager->AddProcess(new G4MuBremsstrahlung,   -1, 3, 3);
      pmanager->AddProcess(new G4MuPairProduction,   -1, 4, 4);       
     
    } else if ((!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) && 
	       (particle->GetParticleName() != "chargedgeantino")) {
      //all others charged particles except geantino
#ifdef G4V497   
      pmanager->AddProcess(new G4MultipleScattering, -1, 1, 1);
#else
      pmanager->AddProcess(new G4hMultipleScattering, -1, 1, 1);
#endif

      pmanager->AddProcess(new G4hIonisation, -1, 2, 2);
    }
  }
}
