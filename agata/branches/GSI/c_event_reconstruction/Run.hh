#include <iostream>

class Setup 
// in principle this class is not yet used for anything, just stores geomtry from agata file
{
public:

  virtual void setposxyz_crincl(int cr_i,float posx,float posy,float posz)
  {
    crinclpos_x[cr_i] = posx;
    crinclpos_y[cr_i] = posy;
    crinclpos_z[cr_i] = posz;
  }

  virtual float getposx_det(int det_i){return detpos_x[det_i];}
  virtual float getposy_det(int det_i){return detpos_y[det_i];}
  virtual float getposz_det(int det_i){return detpos_z[det_i];}
  virtual float getposx_cl(int det_i){return clpos_x[det_i];}
  virtual float getposy_cl(int det_i){return clpos_y[det_i];}
  virtual float getposz_cl(int det_i){return clpos_z[det_i];}
  virtual float getposx_crincl(int det_i){return crinclpos_x[det_i];}
  virtual float getposy_crincl(int det_i){return crinclpos_y[det_i];}
  virtual float getposz_crincl(int det_i){return crinclpos_z[det_i];}
  virtual int getcrystalclindex(int crystal_nr){return crystalclindex[crystal_nr];}
  virtual void setposxyz_det(int det_i,float posx,float posy,float posz)
  {
    detpos_x[det_i] = posx;
    detpos_y[det_i] = posy;
    detpos_z[det_i] = posz;
  }

  virtual void setposxyz_cl(int cl_i,float posx,float posy,float posz)
  {
    clpos_x[cl_i] = posx;
    clpos_y[cl_i] = posy;
    clpos_z[cl_i] = posz;
  }

  virtual void setcrystalclindex(int crystal_nr,int cl_index)
  {
    crystalclindex[crystal_nr] = cl_index;// contains in which cluster a certain crystal is placed.
  }

private:
  static const int max_detectors = 300;
  static const int max_clusters = 100;
  static float crinclpos_x[3];
  static float crinclpos_y[3];
  static float crinclpos_z[3];  
  static float detpos_x[max_detectors];
  static float detpos_y[max_detectors];
  static float detpos_z[max_detectors];
  static float clpos_x[max_clusters];
  static float clpos_y[max_clusters];
  static float clpos_z[max_clusters];
  static int crystalclindex[max_detectors];
};


class Event 
// Event Class contains all variables related to one single event
// filled/cleared at/after each event
{
public:
  //  Event(){this->clean();}
  Event(){n_int=0; int_index = 0;flag_badevent = 0;ndetectorshitted=0;}
  ~Event();

  // set methods
  void seteventnumber(int eventnumber){ev_number = eventnumber;}
  void setfragmentbeta(float betafragment){frag_beta = betafragment;}
  void setegamma(float energy){g_energy = energy;}
  void setgraydirection(float x, float y, float z){g_dx = x; g_dy=y; g_dz=z;}
  void setgrayorigin(float x, float y, float z){g_x = x; g_y=y; g_z=z;}
  void setintflag(bool flag){interaction_flag = flag;}
  void increasendethitted(void){ndetectorshitted++;}
  void increase_nint(void){n_int++;}
  void increase_int_index(void){int_index++;}
  void set_edepint(float edep){edep_int[int_index] = edep;}
  void set_edepintc(float edep){edep_intc[int_index] = edep;}
  void setintcoor(float x, float y, float z){int_x[int_index] = x; int_y[int_index]=y; int_z[int_index]=z;}
  void setintcoorc(float x, float y, float z){int_xc[int_index] = x; int_yc[int_index]=y; int_zc[int_index]=z;}
  void increase_edep(float edep){total_edep += edep;}
  //  void increase_edep(float edep, float edepc){total_edep += edep; total_edepc += edepc;}
  void increase_edep(float edep, float totedepc){total_edep += edep; total_edepc += totedepc;}
  void set_edepc(float totedepc){total_edepc = totedepc;}
  void increase_accedepint(void){accedep_int[int_index] = total_edep;}
  void clean(void);
  //  void increase_ev_nr(void){ev_number++;}
  void setxint(float x){int_x[int_index] = x;}
  void setyint(float y){int_y[int_index] = y;}
  void setzint(float z){int_z[int_index] = z;}
  void setclnhit(int cln){cln_hit[int_index] = cln;}
  void setdetnhit(int detn){detn_hit[int_index] = detn;}//crystal fired at this hit
  void setnseghit(int nseg){nseg_hit[int_index] = nseg;}
  void sethitlargestedep(int index){hit_largestEdep_i = index;}
  void setfirsthit(int index){hit_first = index;}
  void settimeint(float time){time_int[int_index] = time;}
  void resetnint(void){n_int = 1;int_index=0;}
  void flagbadevent(void){flag_badevent = 1;}

  //access methods
  int geteventnumber(void){return ev_number;}
  float getedep(int interaction){return edep_int[interaction];}
  float getedepc(int interaction){return edep_intc[interaction];}
  float getfragmentbeta(void){return frag_beta;}
  float getaccedep(int interaction){return accedep_int[interaction];}
  float gettotedep(void){return total_edep;}
  float gettotedepc(void){return total_edepc;}
  float getegamma(void){return g_energy;}
  bool getintflag(void){return interaction_flag;}
  float getgxdir(void){return g_dx;}
  float getgydir(void){return g_dy;}
  float getgzdir(void){return g_dz;}
  float getgsourcex(void){return g_x;}
  float getgsourcey(void){return g_y;}
  float getgsourcez(void){return g_z;}
  float getxint(int intn){return int_x[intn];}
  float getyint(int intn){return int_y[intn];}
  float getzint(int intn){return int_z[intn];}
  float getxintc(int intn){return int_xc[intn];}
  float getyintc(int intn){return int_yc[intn];}
  float getzintc(int intn){return int_zc[intn];}
  int getndetectorshitted(void){return ndetectorshitted;}
  int getninteractions(void){return n_int;}

  int getdetnhit(int int_index){return detn_hit[int_index];}
  int getclnhit(int int_index){return cln_hit[int_index];}
  int getnseghit(int int_index){return nseg_hit[int_index];}



  virtual float getthresholdEdep(){return threshold_Edep;}
  int gethitlargestedep(void){return hit_largestEdep_i;}
  int getfirsthit(void){return hit_first;}
  float gettimeint(int intn){return time_int[intn];}
  int getintindex(void){return int_index;}
 
  bool getflagbadevent(void){return flag_badevent;}


private:
  static const int max_interactions = 200;


  int ev_number;
  float g_energy;

  float frag_beta;
  
  float g_dx;
  float g_dy;
  float g_dz;

  float g_x;
  float g_y;
  float g_z;

  bool interaction_flag;
  int n_int;
  int int_index;
  int det_int[max_interactions];

  float edep_int[max_interactions];
  float edep_intc[max_interactions];

  float accedep_int[max_interactions];
  float time_int[max_interactions];
  float int_x[max_interactions];
  float int_y[max_interactions];
  float int_z[max_interactions];
  float int_xc[max_interactions];
  float int_yc[max_interactions];
  float int_zc[max_interactions];

  int detn_hit[max_interactions];
  int cln_hit[max_interactions];
  int nseg_hit[max_interactions];



  static float threshold_Edep;

  float total_edep;
  float total_edepc;

  int hit_largestEdep_i;
  int hit_first;
  bool flag_badevent;
  int ndetectorshitted;
};


Event::~Event()
{
}

void Event::clean()
{
  flag_badevent = 0;

  n_int = 0;

  ev_number = 0;
  g_energy = 0;
  g_x = 0;
  g_y = 0;
  g_z = 0;
  interaction_flag = 0;
  
  for(int i=0;i<max_interactions;i++)
    det_int[i] = 0;
  /*
  edep_int[0] = {0};
  int_x[0] = {0};
  int_y[0] = {0};
  int_z[0] = {0};
  */
  total_edep = 0;
  hit_largestEdep_i = 0;
  hit_first = 0;
  int_index = 0;
}


class Run // Run class contains all general histograms filled at each event and global counters
{
public:

  static Event event_i;
  static Setup setup_i;

  static float energy_resolution_fwhmat1MeV;
  static float lycca_time_resolution_fwhm;
  static float position_resolution_fwhm;
  static float position_resolution_dsssd;//fwhm
  static int SimEvents;
  static int flag_reconstruction;
  static int flag_activetarget;
  static int flag_plot;
  static int flag_plot_Eresol;
  static int rebin_factor;
  static Char_t file1[500]; 
  static const bool flagnotracking = 0;
  static const int MaxNint = 99;
  static float lowerE;
  static float higherE;
  //  static const int maxE = 15000;//gammas
  static const int maxE = 100000;//muons 100 MeV
  static float cutTheta;

  static  TH1F *hgs;
  static  TH1F *hgdirx;
  static  TH1F *hgdiry;
  static  TH1F *hgdirz;
  static TH1F *hfwhmx;
  static TH1F *hfwhmy;
  static TH1F *hfwhmz;
  static TH1F *hfwhme;

  static TH2F *hgxy;
  static TH1F *hgx;
  static TH1F *hgy;
  static TH1F *hgz;

  static TH1F *hEdep;
  static const int nhistos = 4;
  static TH1F *hEdepdc[nhistos];

  static TH1F* hAvgX;// = new TH1F("hAvgX","",1000,-50,50);
  static TH1F* hAvgY;// = new TH1F("hAvgY","",1000,-50,50);
  static TH1I* hEvents;

  //  next:
  //  static Event *event_i;
  //  static Setup *setup;
  
  // new functions to be called at each step of the main readinputfile loop
  //----------------
  void fillVertexPosEventi(void);
  void fillGammaHisto(void){hgs->Fill(event_i.getegamma());}
  void fillGDirXHisto(void){hgdirx->Fill(event_i.getgxdir());}
  void fillGDirYHisto(void){hgdiry->Fill(event_i.getgydir());}
  void fillGDirZHisto(void){hgdirz->Fill(event_i.getgzdir());}
  void fillDCHistos(void);
  void fillPHEventi(void);
  void clearEventi(void);
  float GetDopplerCorrectedEnergy(const int flag);
  
  //----------------
private:
  void ReadInfoFile(void);
};


Run::Run()
{
  ReadInfoFile();
}

Run::~Run()
{
  delete hgs;
  delete hgdirx;
  delete hgdiry;
  delete hgdirz;
  delete hfwhmx;
  delete hfwhmy;
  delete hfwhmz;
  delete hgx;
  delete hgy;
  delete hgz;
  delete hEdep;
  delete [] hEdepdc;
}


void Run::fillPHEventi(void)
{
  //  cout << " DEBUG: intflag = " << Run::event_i.getintflag() << endl;
  if(Run::event_i.getintflag()) //there was >0 hits in this event
    {
      int ndethit = event_i.getndetectorshitted();
      float theta = acos(event_i.getgzdir())*360.0/(2.0*3.141592);
      //	  cout << " Event " << i << " theta = " << theta << endl;
      if(Run::flagnotracking)//usually not the user-case
	{
	  if(ndethit == 0)
	    {
	      Run::hEdep->Fill((double) Run::event_i.gettotedep());
	      Run::hEdepdc[0]->Fill((double) GetDopplerCorrectedEnergy(0));//ideal
	      Run::hEdepdc[1]->Fill((double) GetDopplerCorrectedEnergy(1));//positon resolution
	      Run::hEdepdc[2]->Fill((double) GetDopplerCorrectedEnergy(2));//energy resolution
	      Run::hEdepdc[3]->Fill((double) GetDopplerCorrectedEnergy(3));//pos+energy resolution
	    }
	  else
	    cout << " Event = ??? Number of detectors hitted " << ndethit << " excluded" << endl;
	}
      else if(cutTheta > 0)
	{
	  if(theta < (cutTheta + 5) && (cutTheta - 5) < theta )
	    {
	      //	      cout << " Event Nr. = " << i+1 << endl;
	      //	  cout << " DEBUG Event Edep = " << Run::event_i.gettotedep() << endl;
	      Run::hEdep->Fill((double) Run::event_i.gettotedep());
	      //	  cout << "         DEBUG hEdep integral = " << Run::hEdep->Integral() << endl;
	      Run::hEdepdc[0]->Fill((double) GetDopplerCorrectedEnergy(0));//ideal
	      Run::hEdepdc[1]->Fill((double) GetDopplerCorrectedEnergy(1));//positon resolution
	      Run::hEdepdc[2]->Fill((double) GetDopplerCorrectedEnergy(2));//energy resolution
	      Run::hEdepdc[3]->Fill((double) GetDopplerCorrectedEnergy(flag_reconstruction));//pos+energy r
	    }
	}
      else if(cutTheta == 0)
	{
	  //	      cout << " Event Nr. = " << i+1 << endl;
	  //	  cout << " DEBUG Event Edep = " << Run::event_i.gettotedep() << endl;
	  Run::hEdep->Fill((double) Run::event_i.gettotedep());
	  //	  cout << "         DEBUG hEdep integral = " << Run::hEdep->Integral() << endl;
	  Run::hEdepdc[0]->Fill((double) GetDopplerCorrectedEnergy(0));//ideal
	  Run::hEdepdc[1]->Fill((double) GetDopplerCorrectedEnergy(1));//positon resolution
	  Run::hEdepdc[2]->Fill((double) GetDopplerCorrectedEnergy(2));//energy resolution
	  Run::hEdepdc[3]->Fill((double) GetDopplerCorrectedEnergy(flag_reconstruction));//pos+energy r
	} 
    }
}


float Run::GetDopplerCorrectedEnergy(const int flag)  
{

  TRandom *rnd = new TRandom();
  
  //int first_interaction = eventi.getfirsthit();
  //  int first_interaction = 0;

  int first_interaction = Run::event_i.gethitlargestedep();

  if(flag == 0)// ideal position & energy resolutions
    {
      float gamma_det_x = Run::event_i.getxint(first_interaction);
      float gamma_det_y = Run::event_i.getyint(first_interaction);
      float gamma_det_z = Run::event_i.getzint(first_interaction);
      float gamma_energy = Run::event_i.gettotedep();
    }
  else if(flag == 1)// include position resolution
    {
      float gamma_det_x = Run::event_i.getxintc(first_interaction);
      float gamma_det_y = Run::event_i.getyintc(first_interaction);
      float gamma_det_z = Run::event_i.getzintc(first_interaction);
      float gamma_energy = Run::event_i.gettotedep();
    }
  else if(flag == 2)// include energy resolution
    {
      float gamma_det_x = Run::event_i.getxint(first_interaction);
      float gamma_det_y = Run::event_i.getyint(first_interaction);
      float gamma_det_z = Run::event_i.getzint(first_interaction);
      float gamma_energy = Run::event_i.gettotedepc();
    }
  else if(flag == 3)// include position & energy resolution
    {
      float gamma_det_x = Run::event_i.getxintc(first_interaction);
      float gamma_det_y = Run::event_i.getyintc(first_interaction);
      float gamma_det_z = Run::event_i.getzintc(first_interaction);
      float gamma_energy = Run::event_i.gettotedepc();
      Run::hAvgX->Fill(gamma_det_x);
      Run::hAvgY->Fill(gamma_det_y);
    }
  else if(flag == 4)// g-position resolution = crystal size
    {
      int g_det = Run::event_i.getdetnhit(first_interaction);
      float gamma_det_x = Run::setup_i.getposx_det(g_det);
      float gamma_det_y = Run::setup_i.getposy_det(g_det);
      float gamma_det_z = Run::setup_i.getposz_det(g_det);
      float gamma_energy = Run::event_i.gettotedepc();

      //      cout << " hitted detector " << g_det << " Edep = " << eventi.getedep(first_interaction) << "  pos det " << gamma_det_x << " , " << gamma_det_y << " , " << gamma_det_z << endl;
    }

  const float SigmaX_DSSSD = position_resolution_dsssd/2.35;

  float target_x = rnd->Gaus(Run::event_i.getgsourcex(),SigmaX_DSSSD);
  float target_y = rnd->Gaus(Run::event_i.getgsourcey(),SigmaX_DSSSD);

  float pos_det_x = target_x;
  float pos_det_y = target_y;
  float pos_det_z = 0;

  float decay_z = Run::event_i.getgsourcez();// 0 anyway

  float vg_x = gamma_det_x;
  float vg_y = gamma_det_y;
  float vg_z = gamma_det_z - decay_z;

  float vt_x = pos_det_x;
  float vt_y = pos_det_y;
  float vt_z = pos_det_z;

  float vgt_x = vg_x - vt_x;
  float vgt_y = vg_y - vt_y;
  float vgt_z = vg_z - vt_z;


  Double_t theta_gamma = acos(vg_z/sqrt(vg_x*vg_x + vg_y*vg_y + vg_z*vg_z));

  Double_t theta_lab = acos(vgt_z/sqrt(vgt_x*vgt_x + vgt_y*vgt_y + vgt_z*vgt_z));

  float beta = Run::event_i.getfragmentbeta();

  if(beta < 0 || beta > 1)
    {
      cout << " Problem with the beta-value beta = " << beta << " exiting..." << endl;
      exit(-1);
    }

  // beta broadening
  float c_light = 3E10;//cm/s
  float beta2 = beta*beta;
  float lycca_d = 340.0;//cm distance of fragment flight

  //  cout << " DEBUG perfect beta = " << beta << endl;
  beta = rnd->Gaus(beta,c_light*beta2*(1E-12*lycca_time_resolution_fwhm/2.35)/lycca_d);
  //  cout << " DEBUG real beta = " << beta << endl;

  float doppler_corrected_energy_lab = gamma_energy*(1-beta*cos(theta_lab))/sqrt(1.0-beta*beta);

  float doppler_corrected_energy = gamma_energy*(1-beta*cos(theta_gamma))/sqrt(1.0-beta*beta);

  if(flag == 63)// include position & energy resolution
    {
      cout << "+++++++++++++++++++++++++++++++++++++++++" << endl;
      cout << " pos_det_x = " << pos_det_x << endl;
      cout << " pos_det_y = " << pos_det_y << endl;
      cout << endl;
      cout << " Edep(largest) = " << Run::event_i.getedep(first_interaction)<< endl;
      cout << " n interactions = " << Run::event_i.getninteractions()<< endl;
      cout << " Hit largest Edep = " << Run::event_i.gethitlargestedep() << " " << first_interaction << endl;
      cout << " gamma_det_x = " << gamma_det_x << endl;
      cout << " gamma_det_y = " << gamma_det_y << endl;
      cout << " gamma_det_z = " << gamma_det_z << endl;
      cout << " theta_lab = " << theta_lab << " doppler_corrected_energy_lab = " << doppler_corrected_energy_lab << endl;
      cout << " theta_gamma = " << theta_gamma << " doppler_corrected_energy = " << doppler_corrected_energy << endl;
      cout << "+++++++++++++++++++++++++++++++++++++++++" << endl;
    }

  //  double theta_lab = acos(sqrt(vz*vz)/l);
  if(flag_activetarget)
    return doppler_corrected_energy_lab; // ACTIVE Sec. Target
  else
    return doppler_corrected_energy; // NO active sec. target
}


void Run::fillVertexPosEventi(void)
{
  Run::hgx->Fill(Run::event_i.getgsourcex()); // for all events
  Run::hgy->Fill(Run::event_i.getgsourcex());
  Run::hgz->Fill(Run::event_i.getgsourcez());
  Run::hgxy->Fill(Run::event_i.getgsourcex(),Run::event_i.getgsourcey());
}



void Run::ReadInfoFile()
{
  char info_file[256];
  sprintf(info_file,"input/info_reconstruction.txt");
    
  int i = 0;
  char w[512];
  char directory[512];
  int flag = 0;

  FILE	*datafile;
  
  /* read info from file */
  datafile = fopen(info_file, "r");	/* check if file exists and open */
  if (datafile==NULL)	
    {
      printf("cannot find file %s\n exit... \n", fname);
      exit(0);
    }

  //reading file name
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%s",file1);

  // number of simulated events or events to process
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&SimEvents);

  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&position_resolution_fwhm);

  position_resolution_fwhm /= sqrt(3.0);// linear spatial resolution along x, y, z -> /sqrt(3)

 fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&position_resolution_dsssd);

  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&lycca_time_resolution_fwhm);
  
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&lowerE);

  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&higherE);

  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&flag_reconstruction);
  
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&flag_activetarget);
 
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&flag_plot);
  
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&flag_plot_Eresol);
  
  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%d",&rebin_factor);

  fgets(w,512,datafile); 		
  while ( strstr(w,"#")!=NULL) 
    fgets(w,512,datafile);
  sscanf(w,"%f",&cutTheta);

  cout << " Info for Event Reconstruction read from file " << info_file << endl;
  cout << " g-ray events will be read from " << file1 << endl;
  cout << " number of events to process = " << SimEvents << endl;
  cout << " position resolution of HPGe = " << position_resolution_fwhm*sqrt(3) << " mm " << endl;
  cout << " position resolution of DSSSD = " << position_resolution_dsssd << " mm " << endl;
  cout << " time resolution to determine beta of fragment = " << lycca_time_resolution_fwhm << " (ps)" << endl;
  cout << " Energy range in pulse height spectra = " << lowerE << "-"<< higherE << " (eV)" << endl;
  cout << " Rebin Factor = " << rebin_factor << endl;
  if(cutTheta>0)
    {
      cout << endl;
      cout << " Choosing events with theta in the angular range [" << cutTheta-5 <<","<<cutTheta+5 <<"]"<<endl;
      cout << endl;
    }

}

