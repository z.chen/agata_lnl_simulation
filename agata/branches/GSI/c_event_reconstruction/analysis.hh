#include <fstream>
#include "TRandom.h"
#include "TSpectrum.h"
#include "TVirtualFitter.h"

#include "Run.hh"

TH1F* Run::hgs =  new TH1F("hgs","",1000,0,10000);
TH1F* Run::hgdirx = new TH1F("hgdirx","",100,-1,1);
TH1F* Run::hgdiry = new TH1F("hgdiry","",100,-1,1);
TH1F* Run::hgdirz = new TH1F("hgdirz","",100,-1,1);
TH1F* Run::hfwhmx = new TH1F("hfwhmx","Resolution X",100,-10,10);
TH1F* Run::hfwhmy = new TH1F("hfwhmy","Resolution Y",100,-10,10);
TH1F* Run::hfwhmz = new TH1F("hfwhmz","Resolution Z",100,-10,10);
TH1F* Run::hfwhme = new TH1F("hfwhme","Energy Resolution",100,-10,10);
TH1F* Run::hgx = new TH1F("hgx","g-Ray Source Pos. X (mm)",100,-100,100);
TH1F* Run::hgy = new TH1F("hgy","g-Ray Source Pos. Y (mm)",100,-100,100);
TH2F* Run::hgxy = new TH2F("hgxy","g-Ray Source Pos. X/Y (mm)",100,-100,100,100,-100,100);
TH1F* Run::hgz = new TH1F("hgz","g-Ray Source Pos. Z (mm)",100,-50,50);

TH1F *Run::hEdep = new TH1F("hEdep","Raw Pulse Height Spectrum",5*Run::maxE,0,Run::maxE);
TH1F *Run::hEdepdc[0] = new TH1F("hEdepdc[0]","Ideal Pos./E resolution",5*Run::maxE,0,Run::maxE);
TH1F *Run::hEdepdc[1] = new TH1F("hEdepdc[1]","",5*Run::maxE,0,Run::maxE);
TH1F *Run::hEdepdc[2] = new TH1F("hEdepdc[2]","",5*Run::maxE,0,Run::maxE);
TH1F *Run::hEdepdc[3] = new TH1F("hEdepdc[3]","",5*Run::maxE,0,Run::maxE);
TH1F *Run::hAvgX = new TH1F("hAvgX","",1000,-50,50);
TH1F *Run::hAvgY = new TH1F("hAvgY","",1000,-50,50);
TH1I *Run::hEvents = new TH1I("hEvents","",1E6,0,1E6);

float Event::threshold_Edep = 0;//keV
float Setup::detpos_x[0] = {0};
float Setup::detpos_y[0] = {0};
float Setup::detpos_z[0] = {0};

// globals, for debug only, can be removed in future
//TH1F* hDB_Theta_l = new TH1F("hDB_Theta_l","",100,0,2);
//TH1F* hDB_Theta_l1 = new TH1F("hDB_Theta_l1","",100,0,2);
//TH1F* hDB_DeltaTheta = new TH1F("hDB_DeltaTheta","",100,-1,1);


Double_t fpeaksbkg(Double_t *x, Double_t *par);

int plot()
{
  gStyle->SetOptStat(111111);
  gStyle->SetOptFit(111111);
  gStyle->SetOptStat(0000);
  //------------------------------
  //    final geometry
  //------------------------------

  float beta = 0.0; // not used

 // Reading Input File

  Run run;// contains general run, event, setup reads info_file

  const int MaxEvents = Run::SimEvents;

  cout << " Processing " << Run::SimEvents << " events " << endl;

  // Setting/drawing the energy resolution
  TCanvas *cEResol;
  TF1 *fr = GetEResol(cEResol);

  Run::energy_resolution_fwhmat1MeV = fr->Eval(1E3);

  int nevents1 = 0;

  ReadInputFile(&run,nevents1,fr);//call run fill-methods here inside!

  printGeneralSettings(run.event_i);

  if(Run::flag_plot)
    {
      cout << "Plotting general histograms " << endl;

      Char_t histotitle[100];
      Run::hEdepdc[0]->SetLineStyle(3);
      Run::hEdepdc[0]->SetLineWidth(1);

      sprintf(histotitle,"#DeltaX_{i} = %.1f mm (fwhm)",Run::position_resolution_fwhm);
      Run::hEdepdc[1]->SetTitle(histotitle);
      Run::hEdepdc[1]->SetLineWidth(1);
      Run::hEdepdc[1]->SetLineStyle(2);

      sprintf(histotitle,"#DeltaE = %.0f keV (fwhm)",Run::position_resolution_fwhm);
      Run::hEdepdc[2]->SetTitle(histotitle);
      Run::hEdepdc[2]->SetLineWidth(1);
      Run::hEdepdc[2]->SetLineStyle(1);

      sprintf(histotitle,"#DeltaX_{i} = %.1f mm; #DeltaE = %.1f keV",Run::position_resolution_fwhm, Run::energy_resolution_fwhmat1MeV);
      Run::hEdepdc[3]->SetTitle(histotitle);
      Run::hEdepdc[3]->SetLineWidth(2);
      Run::hEdepdc[3]->SetLineColor(1);


      //      getPH(event1,nevents1,hEdep1,hEdep1dc,beta);

      gStyle->SetPadTickX(1);
      gStyle->SetPadTickY(1);

      //-----------------------------------------------------------------
     //  TCanvas *cdb = new TCanvas("cdb","",1);
//       cdb->Divide(2,2);
//       cdb->cd(1);
//       hDB_Theta_l->Draw();
//       hDB_Theta_l1->SetLineColor(2);
//       hDB_Theta_l1->Draw("same");
//       cdb->cd(2);
//       hDB_DeltaTheta->SetLineColor(4);
//       hDB_DeltaTheta->Draw();
//       cdb->cd(3);
//       Run::hAvgX->Draw();
//       cdb->cd(4);
//       Run::hAvgY->Draw();

      //      cout << "Efficiency = " << GetEfficiency(Run::hEdep,nevents1) << endl;
      //      cout << "Photopeak Efficiency = " << GetPhotopeakEfficiency(Run::hEdepdc[0],nevents1,1000) << endl;
      
      // Drawing g-ray source energy distribution
      gStyle->SetOptStat(0000);
      //-----------------------------------------------------------------
      TCanvas *cg = new TCanvas("cg","",1);
      cg->SetFillColor(10);
      Run::hgs->GetXaxis()->SetTitle("E_{#gamma} (keV)");
      Run::hgs->GetYaxis()->SetTitle("Events");
      Run::hgs->DrawCopy();
      
      // Drawing g-ray momentum distributions on x,y,z
      //-----------------------------------------------------------------
      TCanvas *cgdir = new TCanvas("cgdir","",1);
      cgdir->SetFillColor(10);
      cgdir->Divide(1,3);
      cgdir->cd(1);
      SetStyle(Run::hgdirx);
      Run::hgdirx->GetXaxis()->SetTitle("direction vector x");
      Run::hgdirx->GetYaxis()->SetTitle("Events");
      Run::hgdirx->DrawCopy();
      cgdir->cd(2);
      SetStyle(Run::hgdiry);
      Run::hgdiry->GetXaxis()->SetTitle("direction vector y");
      Run::hgdiry->GetYaxis()->SetTitle("Events");
      Run::hgdiry->DrawCopy();
      cgdir->cd(3);
      SetStyle(hgdirz);
      Run::hgdirz->GetXaxis()->SetTitle("direction vector z");
      Run::hgdirz->GetYaxis()->SetTitle("Events");
      Run::hgdirz->DrawCopy();

      // Drawing pulse height spectra with add-back
      //-----------------------------------------------------------------
      TCanvas *c = new TCanvas("c","",1);
      gStyle->SetOptTitle(0000);
      gStyle->SetOptFit(0000);
      c->SetFillColor(10);
 //      c->Divide(1,2);
//       c->cd(1);
      //      SetStyle(Run::hEdep);

      Run::hEdep->GetXaxis()->SetTitle("E^{addback}_{dep} (keV)");
      Run::hEdep->GetYaxis()->SetTitle("Counts");
      Run::hEdep->SetLineStyle(1);
      Run::hEdep->SetLineColor(1);
      Run::hEdep->SetLineWidth(2);
      Run::hEdep->GetXaxis()->SetRangeUser(Run::lowerE,Run::higherE);
      Run::hEdep->Rebin(Run::rebin_factor);
      SetStyle(Run::hEdep);
      Run::hEdep->DrawCopy();
      TCanvas *c2 = new TCanvas("c2","",1);
      c2->SetFillColor(10);

      for(int i=0;i<Run::nhistos;i++)
	{
	  SetStyle(Run::hEdepdc[i]);
	  Run::hEdepdc[i]->Rebin(Run::rebin_factor);
      	  Run::hEdepdc[i]->GetXaxis()->SetRangeUser(Run::lowerE,Run::higherE);
	  Run::hEdepdc[i]->GetXaxis()->SetTitle("E^{addback+DopplerCorr.}_{dep} (keV)");
	  Run::hEdepdc[i]->GetYaxis()->SetTitle("Counts");
	  if(i==0)
	      Run::hEdepdc[i]->DrawCopy();
	  else
	      Run::hEdepdc[i]->DrawCopy("same");
	}
      
      
      TF1 *myfit = FitSpectrum(Run::hEdepdc[3]);
      Run::hEdepdc[2]->DrawCopy("same");
      Run::hEdepdc[1]->DrawCopy("same");
      Run::hEdepdc[0]->DrawCopy("same");

      leg = new TLegend(0.6,0.7,1.0,1.0);
      char legtitle[100];
      leg->AddEntry(Run::hEdepdc[0],"Ideal case","l");
      sprintf(legtitle,"#DeltaX = %.1f mm (fwhm)",Run::position_resolution_fwhm);
      leg->AddEntry(Run::hEdepdc[1],legtitle,"l");
      sprintf(legtitle,"#DeltaE = %.0f keV (fwhm)",Run::energy_resolution_fwhmat1MeV);
      leg->AddEntry(Run::hEdepdc[2],legtitle,"l");
      sprintf(legtitle,"#DeltaX = %.1f mm; #DeltaE = %.0f keV (fwhm)",Run::position_resolution_fwhm,Run::energy_resolution_fwhmat1MeV);
      leg->AddEntry(Run::hEdepdc[3],legtitle,"l");

      sprintf(legtitle,"Fit fwhm = %.1f (keV)",2.35*(myfit->GetParameter(4)));
      leg->AddEntry(myfit,legtitle,"l");

      leg->Draw();

      //--------------------------------------------------------------------------------
      TCanvas *cResol = new TCanvas("cResol","",1);
      cResol->SetFillColor(10);
      cResol->Divide(1,4);
      DrawResolutions(cResol);
      
      TCanvas *cGSource = new TCanvas("cGSource","",1);
      cGSource->SetFillColor(10);
      cGSource->Divide(4,1);
      DrawGammaSource(cGSource);

      gStyle->SetOptStat(1111);
      TCanvas *cEvents = new TCanvas("cEvents","",1);
      cEvents->SetFillColor(10);
      hEvents->SetLineColor(2);
      hEvents->SetLineWidth(2);
      hEvents->Draw("H");
      
      savePHRootFile(Run::hEdepdc[3],"../SimulationResults/Reconstructor/74ni/DC74Ni500mgFe1.2ps25deg150MeV.root");

      return 0;

      // Drawing hit distributions
      //-------------------------------------------
      TCanvas *chd = new TCanvas("chd","",1);
      chd->SetFillColor(10);
      chd->Divide(1,4);
      char titlehits[200];
      sprintf(titlehits,"Hits Distribution, Threshold = %.0f keV",event1[0].getthresholdEdep());
      TH1I *h_hd = new TH1I("h_hd",titlehits,30,0.5,30.5);
      TH1I *h_sd = new TH1I("h_sd","Segments (10*slice + sector)",66,0.5,66.5);
      TH1I *h_dd = new TH1I("h_dd","Detectors",180,0,180);
      TH1I *h_cd = new TH1I("h_cd","Clusters",60,0.5,60.5);
      SetStyle(h_hd);
      SetStyle(h_sd);
      SetStyle(h_dd);
      SetStyle(h_cd);
      
      getHitsDistribution(event1, nevents1, h_hd, h_sd, h_dd, h_cd);

      chd->cd(1);
      h_hd->Draw();

      chd->cd(2);
      h_sd->Draw();
      
      chd->cd(3);
      h_dd->Draw();
      
      chd->cd(4);
      h_cd->Draw();
    }

  // Drawing histo first hit vs. largest Edep hit
  //-------------------------------------------
  TCanvas *ct = new TCanvas("ct","",1);
  ct->SetFillColor(10);
  ct->Divide(1,2);
  TH1I *ht = new TH1I("ht","",20,0,20);
  TH1F *htd = new TH1F("htd","",150,0,150);
  CheckHitLargestEdep(event1, nevents1,ht,htd);
  SetStyle(ht);
  SetStyle(htd);
  ct->cd(1);
  ht->Draw();
  ct->cd(2);
  htd->Draw();
  
  if(0)
    {
  // Draw Eg/Eg0
  //-----------------------
  TH1F *hcostheta = new TH1F("hcostheta","",100,-1,1);
  TH1F *hEg = new TH1F("hEg","",2000,0,2000);
  TH1F *htheta = new TH1F("htheta","",180,0,180);
  //TH1F *htheta = new TH1F("htheta","",100,0,2);
  TH2F *hEgvsCT = new TH2F("hEgvsCT","",100,-1,1,2000,0,2000);
  TH2F *hEgvsT = new TH2F("hEgvsT","",180,0,180,2000,0,2000);
  SetStyle(hcostheta);
  SetStyle(hEg);
  SetStyle(htheta);
  SetStyle(hEgvsCT);
  SetStyle(hEgvsT);

  CheckEgtoEg0(event1,nevents1,hcostheta,hEg,htheta,hEgvsCT,hEgvsT,beta);
  gStyle->SetOptStat(0000);
  TCanvas *ctheta = new TCanvas("ctheta","",1);
  ctheta->SetFillColor(10);
  ctheta->Divide(1,5);  
  ctheta->cd(1);
  hcostheta->Draw();
  ctheta->cd(2);
  hEg->Draw();
  ctheta->cd(3);
  htheta->Draw();
  ctheta->cd(4);
  hEgvsCT->Draw("H");
  ctheta->cd(5);
  hEgvsT->Draw("H");

  /*
  //  TH3F *h3d = new TH3F("h3d","Lorentz Boost",180,0,180,180,0,180,180,0,180);
  //  TH2F *h2d = new TH2F("h2d","Lorentz Boost",180,0,180,180,0,180);
  TH2F *h2d = new TH2F("h2d","Lorentz Boost",100,-1,1,100,-1,1);
  //  CheckLorentzBoost(event1,nevents1,h2d);
  CheckLorentzBoost(event1,nevents1,h2d);
  TCanvas *clb = new TCanvas("clb","",1);
  clb->SetFillColor(10);
  h2d->Draw("");
  //  h2d->Draw("lego");
  */
    }

  if(0)
    {  
      // Drawing 3D-hits
      //----------------------
      gStyle->SetPalette(1);
      TCanvas *chits = new TCanvas("chits","",1);
      chits->SetFillColor(10);
      
      TH3F *h3d = getHits3DHisto(event1, nevents1);
      h3d->DrawCopy("P");

      //      TH2F *h2d = getHits2DHisto(event1, nevents1);
      //      h2d->DrawCopy("zcol");
   
      /*
      TCanvas *chits = new TCanvas("chits","",1);
      chits->SetFillColor(10);
  
      //  TH3F *h3d = new TH3F("h3d","",400,-400,400,400,-400,400,400,-400,400);// 2mm binning
      TH3F *h3d = new TH3F("h3d","",300,-300,300,300,-300,300,150,100,400);// 2mm binning
      h3d->GetXaxis()->SetTitle("X (mm)");
      h3d->GetYaxis()->SetTitle("Y (mm)");
      h3d->GetZaxis()->SetTitle("Z (mm)");
      h3d->SetMarkerSize(0.4);
      h3d->SetMarkerStyle(20);

      //  for(int i=0;i<nevents1;i++)
      int color_index = 0;
      for(int i=0;i<1000;i++)
	{
	  if(event1[i].getintflag())
	    {
	      h3d->Reset();
	      getHits3DHisto(event1, i, h3d);
	      color_index++;
	      if(color_index>50)
		color_index = 1;
	      h3d->SetMarkerColor(color_index);
	      h3d->DrawCopy("P");
	      chits->Modified();
	      chits->Update();
	    }
	    }
      */
      
    }

  return 0;
}

void drawPhotoPeakEffvsBeta()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  const int ndata = 7;
  const float beta[ndata] = {0,0.1,0.2,0.3,0.4,0.5,0.6};
  const float pheff[ndata] = {6.53,7.19,8.32,9.45,10.62,11.81,13.29};
  TGraph *gr = new TGraph(ndata,beta,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("#beta = v/c");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs #beta, geometry: prespecv0");
  gr->Draw("ALP");
}

void comparePerformance()
{
  char *lab0 = "2 rings, 6 clusters/ring";
  char *lab1 = "1 ring, 6 clusters/ring";
  char *lab2 = "asymmetric ring 8 clusters";

  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  const int ndata = 7;
  //  const int ndata1 = 11;
  const int ndata1 = 9;
  //  const float d[ndata] = {0,5,8,10,12,15,20};
  //  float d[ndata1] = {-15,-10,-5,-2,0,5,8,10,12,15,20};
  float d[ndata1] = {-15,-10,-5,-2,0,5,10,15,20};
   //float d[ndata] = {0,5,8,10,12,15,20};
  for(int i=0;i<ndata1;i++)
    d[i] += 15.0;

  float d2[ndata] = {0,5,8,10,12,15,20};
  for(int i=0;i<ndata;i++)
    d2[i] = 23.5 - d2[i];

  // puntual source
  //  const float pheffd1[ndata] = {7.21,10.06,11.81,11.73,11.32,10.15,7.65}; // aganta v0
  //  const float pheffd2[ndata] = {6.3,7.42,7.85,7.97,8.12,7.13,3.51}; // agata v2
  
  // diffuse source
  //  const float pheffd1[ndata] = {7.13,10.17,11.85,11.99,11.09,10.29,7.88}; // aganta v0
  //  const float pheffd1[ndata] = {11.85,12.94,12.37,12.01,11.56,10.03,7.80}; // aganta v0
  //  const float pheffd1[ndata] = {11.11,12.84,13.31,13.56,13.95,14.33,11.83}; // aganta v1
  //  const float pheffd1[ndata] = {16.20,16.88,16.83,16.48,16.05,14.63,11.65}; // aganta v1_ds (corr.)
  // const float pheffd1[ndata1] = {13.92,15.96,16.50,14.96,13.05,9.8,7.4,6.55,5.55,4.39,3.28};//aganta v2
  //  const float pheffd1[ndata1] = {14.32,15.94,16.8,14.81,12.98,9.44,6.45,4.41,3.24};//agantav2_ds (corr)
  //  const float pheffd12[ndata1] = {14.16,16.21,16.97,14.96,13.38,9.48,6.52,4.59,3.32};//agantav3_ds 
  const float pheffd1[ndata1] = {6.01,7.88,8.80,8.56,7.99,6.43,5.19,3.82,3.02};//agantav4_ds 
  const float pheffd12[ndata1] = {13.43,14.06,13.55,12.27,11.61,9.25,7.04,5.49,4.11};//agantav5_ds 
  //const float pheffd2[ndata] = {6.43,7.53,8.01,8.14,7.36,7.21,3.20}; // agata v2
  //  const float pheffd2[ndata] = {6.59,7.60,7.98,8.21,8.20,7.84,6.71}; // agata v2
  //  const float pheffd2[ndata] = {5.7,7.46,8.41,9.02,9.50,10.0,9.91}; // agata v4 sbp
  const float pheffd2[ndata] = {5.67, 7.24,8.14,8.88,9.37, 10.13, 9.95}; // agata v4 sbp ds (corr)

  TGraph *gr = new TGraph(ndata1,d,pheffd1);
  TGraph *gr12 = new TGraph(ndata1,d,pheffd12);
  TGraph *grd2 = new TGraph(ndata,d2,pheffd2);
  gr->SetMarkerStyle(21);
  gr12->SetMarkerStyle(22);
  grd2->SetMarkerStyle(25);

  gr->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs offset from Sec. Target");
  gr->Draw("ALP");
  gr12->Draw("LP");
  grd2->Draw("LP");


  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12,lab0,"lp");
  leg->AddEntry(gr,lab1,"lp");
  leg->AddEntry(grd2,lab2,"lp");

  leg->Draw();


  TCanvas *cr = new TCanvas("cr","",1);
  cr->SetFillColor(10);
  // puntual source
  //  const float resol1[ndata] = {11.9,11.2,10.8,10.4,9.6,8.6,6.9}; // agantav0
  //  const float resol2[ndata] = {6.2,7.2,7.8,8.0,8.6,8.7,8.9};//agatav2

  // diffuse source
  //  const float resol1[ndata] = {12.3,11.0,10.7,10.1,9.74,8.76,6.80}; // agantav0
  //  const float resol1[ndata] = {11.78,11.4,11.15,10.49,9.94,8.53,7.06}; // agantav0_ds (corrected)
  //  const float resol1[ndata] = {11.11,10.09,9.46,9.04,9.26,9.11,7.84}; // agantav1
  //  const float resol1[ndata] = {10.99,10.27,10.08,9.83,9.55,9.06,7.94}; // agantav1_ds (corr.)
  //  const float resol1[ndata1] = {11.66,11.90,13.33,13.06,12.09,9.5,7.63,6.94,6.25,5.26,4.41};//agantav2spb
  //  const float resol1[ndata1] = {11.69,12.43,13.97,13.41,12.45,9.41,7.05,5.22,4.37};//agantav2spb_ds (corr)
  //const float resol12[ndata1] = {12.34,12.98,14.39,13.53,12.32,9.34,6.86,5.20,4.16};//agantav3spb_ds (corr)
  const float resol1[ndata1] = {9.85,11.70,11.47,10.66,9.77,7.89,6.53,5.20,4.26};//agantav4spb_ds
  const float resol12[ndata1] = {9.07,9.58,9.48,8.74,8.31,6.98,5.78,4.84,4.14};//agantav5spb_ds
  //  const float resol2[ndata] = {6.3,7.22,7.89,8.16,8.46,8.82,8.87};//agatav2
  //  const float resol2[ndata] = {6.27,7.28,7.79,8.23,8.69,8.79,8.86};//agatav2_ds (corrected)
  // const float resol2[ndata] = {5.56,6.75,7.7,8.36,8.99,10.26,11.97};//agatav4sbp
 const float resol2[ndata] = {5.66,6.79,7.58,8.17,9.02,10.21,12.30};//agatav4sbp ds (corr.)

  TGraph *gr1 = new TGraph(ndata1,d,resol1);
  TGraph *gr12 = new TGraph(ndata1,d,resol12);
  TGraph *gr2 = new TGraph(ndata,d2,resol2);
  gr1->SetMarkerStyle(21);
  gr12->SetMarkerStyle(22);
  gr2->SetMarkerStyle(25);
  gr2->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr2->GetYaxis()->SetTitle("FWHM (keV)");
  gr2->SetTitle("FWHM vs distance from Sec. Target");
  gr2->Draw("ALP");
  gr1->Draw("LP");
  gr12->Draw("LP");
  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12,lab0,"lp");
  leg->AddEntry(gr1,lab1,"lp");
  leg->AddEntry(gr2,lab2,"lp");
  leg->Draw();


  TCanvas *cs = new TCanvas("cs","",1);
  cs->SetFillColor(10);

  float gs1[ndata1];
  float gs12[ndata1];
  float gs2[ndata];

  for(int i=0;i<ndata1;i++)
    {
      gs1[i] = pheffd1[i]/resol1[i];
      gs12[i] = pheffd12[i]/resol12[i];
    }

  for(int i=0;i<ndata;i++)
    {
      gs2[i] = pheffd2[i]/resol2[i];
    }

  TGraph *grgs1 = new TGraph(ndata,d,gs1);
  TGraph *grgs12 = new TGraph(ndata,d,gs12);
  TGraph *grgs2 = new TGraph(ndata,d2,gs2);
  grgs1->SetMarkerStyle(21);
  grgs12->SetMarkerStyle(22);
  grgs2->SetMarkerStyle(25);
  grgs1->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  grgs1->GetYaxis()->SetTitle("#gamma-ray sensitivity (a.u.)");
  grgs1->SetTitle("#gamma-ray sensitivity vs distance from Sec. Target");
  grgs1->Draw("ALP");
  grgs2->Draw("LP");
  grgs12->Draw("LP");

  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(grgs12,lab0,"lp");
  leg->AddEntry(grgs1,lab1,"lp");
  leg->AddEntry(grgs2,lab2,"lp");
  leg->Draw();
}


void drawPhotoPeakEffvsDistance()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  // geometry prespec v0
//   const int ndata = 9;
//   const float d[ndata] = {0,1,2,3,5,8,10,12,15};
//   const float pheff[ndata] = {11.8,12.4,13.3,13.72,15.32,17.7,19.72,21.37,23.43};

  const int ndata = 7;
  //  const float d[ndata] = {0,5,8,10,15,20,22};
  float d[ndata] = {0,5,8,10,12,15,20};
  for(int i=0;i<ndata;i++)
    d[i] = 23.5 - d[i];
  //  const float pheff[ndata] = {10.27,13.29,15.21,16.81,19.90,18.69,16.12};
  //  const float pheff[ndata] = {7.41,9.08,10.61,11.56,13.49,13.61,12.28};
  //  const float pheff[ndata] = {6.47,7.11,7.27,7.41,6.30,3.10,1.76};
  //  const float pheff[ndata] = {7.41,7.42,8.45,8.82,10.20,9.9,8.8};
  //  const float pheff[ndata] = {6.2,7.4,8.4,8.8,10.2,10.2,8.8};
  //  const float pheff[ndata] = {6.88,8.53,9.37,9.95,11.5,11.0,9.18};
  //  const float pheffnotrack[ndata] = {5.82,7.24,8.02,8.50,9.70,8.98,7.29};
  //  const float pheff[ndata] = {6.6,8.2,9.15,9.76,11.3,10.8,9.45};
  //  const float pheff[ndata] = {6.2,7.73,8.85,9.63,11.27,11.04,9.72};
  // latest:
  //  const float pheff[ndata] = {7.21,10.06,11.81,11.73,11.32,10.15,7.65}; // 8 clu. close geo.
  const float pheff[ndata] = {6.3,7.42,7.85,7.97,8.12,7.13,3.51}; // 10 clu. ring

  //  TGraph *grnt = new TGraph(ndata,d,pheffnotrack);
  //  grnt->SetLineStyle(2);
  //  grnt->SetMarkerStyle(22);
  TGraph *gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  //  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv0, #beta = 0.5");
  //  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv1, #beta = 0.5");
  // gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv1, #beta = 0.5, #theta from E^{max}_{dep,hit}");
  // gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv3, #beta = 0.5, #theta from E^{max}_{dep,hit}");
  //  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv4 (Asy.Ring), #beta = 0.43, #theta from E^{max}_{dep,hit}");
  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: sym. ring 10 clusters, #beta = 0.43, #theta from E^{max}_{dep,hit}");
  // gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv4 (Asy.Ring), #beta = 0.5, #theta from E^{max}_{dep,hit}, No Tracking btw. Clusters");
  gr->Draw("ALP");
  //  grnt->Draw("LP");
  /*
  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr,"With tracking","lp");
  leg->AddEntry(grnt,"No tracking","lp");
  leg->Draw();
  */
}

void drawP2TvsDistance()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  const int ndata = 7;
  const float d[ndata] = {0,5,8,10,15,20,22};
  const float p2tt[ndata] = {0.747718,0.746664,0.740075,0.745955,0.740992,0.689305,0.669761};
  const float p2tnt[ndata] = {0.755583,0.757876,0.746773,0.754568,0.748075,0.7096,0.675739};

  TGraph *gr = new TGraph(ndata,d,p2tt);
  TGraph *grnt = new TGraph(ndata,d,p2tnt);
  gr->SetMarkerStyle(21);
  grnt->SetMarkerStyle(22);
  gr->GetXaxis()->SetTitle("distance from centre (cm)");
  gr->GetYaxis()->SetTitle("P/T Ratio");
  gr->SetTitle("P/T vs offset from centre, geometry: prespecv4 (Asy.Ring), #beta = 0.5, #theta from E^{max}_{dep,hit}");
  gr->Draw("ALP");
  grnt->Draw("LP");

  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr,"With tracking","lp");
  leg->AddEntry(grnt,"No tracking","lp");
  leg->Draw();
}


void drawPhotoPeakEffvsTheta()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  // geometry prespec v0
//   const int ndata = 9;
//   const float d[ndata] = {0,1,2,3,5,8,10,12,15};
//   const float pheff[ndata] = {11.8,12.4,13.3,13.72,15.32,17.7,19.72,21.37,23.43};

  const int ndata = 6;
  const float d[ndata] = {20,30,40,50,60,70};
  //  const float pheff[ndata] = {0.15,1.86,4.67,6.51,6.87,6.88};
  const float pheff[ndata] = {0.84,2.4,2.3,0.72,0.02,0.0};

  TGraph *gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("#theta (deg)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
 gr->SetTitle("Photopeak Efficiency vs #theta, geometry: prespecv4 (Asy.Ring), #beta = 0.5, d = 0 mm");
  gr->Draw("ACP");
}


void drawFWHMvsDistance()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  // geometry prespec v0
//   const int ndata = 9;
//   const float d[ndata] = {0,1,2,3,5,8,10,12,15};
//   const float pheff[ndata] = {11.8,12.4,13.3,13.72,15.32,17.7,19.72,21.37,23.43};

  const int ndata = 7;
  //  const float d[ndata] = {0,5,8,10,15,20,22};
  float d[ndata] = {0,5,8,10,12,15,20};
  for(int i=0;i<ndata;i++)
    d[i] = 23.5 - d[i];

  //  const float pheff[ndata] = {10.27,13.29,15.21,16.81,19.90,18.69,16.12};
  //  const float pheff[ndata] = {0.82,1.03,1.13,1.21,1.52,2.14,2.60};
  //  const float pheff[ndata] = {0.91,1.0,1.0,1.01,0.96,0.88,0.85};
  //  const float pheff[ndata] = {1.00,1.03,1.29,1.20,1.44,1.98,2.4};
  //  const float pheff[ndata] = {8.5,10.6,11.9,12.96,16.26,21.92,23.95};
  //  const float pheff[ndata] = {5.44,6.65,7.30,7.98,9.86,13.9,15.7};
  //  const float pheff[ndata] = {4.76,5.58,6.22,6.73,8.39,11.7,13.4};
  // latest
  //  const float pheff[ndata] = {11.9,11.2,10.8,10.4,9.6,8.6,6.9};
  const float pheff[ndata] = {6.2,7.2,7.8,8.0,8.6,8.7,8.9}; // 10 clu. ring

  TGraph *gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr->GetYaxis()->SetTitle("FWHM (keV)");
  //  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv0, #beta = 0.5");
  //  gr->SetTitle("Photopeak Efficiency vs offset from centre, geometry: prespecv1, #beta = 0.5");
  // gr->SetTitle("FWHM vs offset from centre, geometry: prespecv1, #beta = 0.5, #theta from E^{max}_{dep,hit}");
  // gr->SetTitle("FWHM vs offset from centre, geometry: prespecv3 (1 Ring), #beta = 0.5, #theta from E^{max}_{dep,hit}");
  // gr->SetTitle("FWHM vs offset from centre, geometry: sep. 8 clusters, #beta = 0.43, #theta from E^{max}_{dep,hit}");
 gr->SetTitle("FWHM vs offset from centre, geometry: sym. ring 10 clusters, #beta = 0.43, #theta from E^{max}_{dep,hit}");
  gr->Draw("ALP");
}

TH3F* getHits3DHisto(Event *event, int nevents)
{
  h3d = new TH3F("h3d","",150,100,400,300,-300,300,300,-300,300);
  h3d->GetXaxis()->SetTitle("Z (mm)");
  h3d->GetYaxis()->SetTitle("Y (mm)");
  h3d->GetZaxis()->SetTitle("X (mm)");
  //h3d->SetMarkerStyle(1);
  //  h3d->SetMarkerSize(0.2);
  h3d->SetMarkerColor(2);
  for(int i=0;i<nevents;i++)
    {
      if(event[i].getintflag())
	for(int j=0;j<event[i].getninteractions();j++)
	  h3d->Fill(event[i].getzint(j),event[i].getyint(j),event[i].getxint(j));
    }
  return h3d;
}


TH2F* getHits2DHisto(Event *event, int nevents)
{
  h2d = new TH2F("h2d","",300,-300,300,300,-300,300);
  h2d->GetXaxis()->SetTitle("X (mm)");
  h2d->GetYaxis()->SetTitle("Y (mm)");
  //  h2d->GetZaxis()->SetTitle("Z (mm)");
  //  h2d->SetMarkerStyle(1);
  //  h3d->SetMarkerSize(0.2);
  //  h2d->SetMarkerColor(2);

  for(int i=0;i<nevents;i++)
    {
      if(event[i].getintflag())
	for(int j=0;j<event[i].getninteractions();j++)
	  h2d->Fill(event[i].getxint(j),event[i].getyint(j));
    }
  return h2d;
}



void getHits3DHisto(Event *event, int event_number, TH3F *h3d)
{
  for(int j=0;j<event[event_number].getninteractions();j++)
    h3d->Fill(event[event_number].getxint(j),event[event_number].getyint(j),event[event_number].getzint(j));
}



TH1F* getGammaSpectrum(Event *event, int nevents)
{

  hg = new TH1F("hg","",1000,0,10000);

  for(int i=0;i<nevents;i++)
    {
      hg->Fill((double) event[i].getegamma());
    }
  return hg;
}


TH1F* getGammaDirXSpectrum(Event *event, int nevents)
{

  hdx = new TH1F("hdx","",100,-1,1);

  for(int i=0;i<nevents;i++)
    {
      hdx->Fill((double) event[i].getgxdir());
    }
  return hdx;
}

TH1F* getGammaDirYSpectrum(Event *event, int nevents)
{

  hdy = new TH1F("hdy","",100,-1,1);

  for(int i=0;i<nevents;i++)
    {
      hdy->Fill((double) event[i].getgydir());
    }
  return hdy;
}

TH1F* getGammaDirZSpectrum(Event *event, int nevents)
{

  hdz = new TH1F("hdz","",100,-1,1);

  for(int i=0;i<nevents;i++)
    {
      hdz->Fill((double) event[i].getgzdir());
    }
  return hdz;
}


float GetEfficiency(TH1F *hEdep, int nevents)
{
  return hEdep->Integral()/((float) nevents);
}



float GetPhotopeakEfficiency(TH1F *hEdep, int nevents, float Eg)
{
  float binning = 1/((float)hEdep->GetXaxis()->GetBinWidth(1));
  //  int Egi = (Eg-50)*binning;
  //  int Ege = (Eg+50)*binning;
  int Egi = (Eg)*binning - 4;
  int Ege = (Eg)*binning + 4;
  //  cout << " binning = " << binning << " bini = " << Egi << " bine = " << Ege << endl;

  return hEdep->Integral(Egi,Ege)/((float) nevents);
}




void ReadInputFile(Run *run, int &nevents, TF1 *fresol)
{  
  float energy_resolution = Run::energy_resolution_fwhmat1MeV/2.35;
  const float position_resolution = Run::position_resolution_fwhm/2.35;

  TRandom *rnd = new TRandom();

  cout << endl;
  cout << "Reading Input File " << Run::file1 << endl;
  cout << "Folding with g-Ray Detection Position Resolution FWHM = " << Run::position_resolution_fwhm << " (mm) -> sigma = " << position_resolution << " (mm) " << endl;
  cout << "Folding with E Resolution FWHM = " << Run::energy_resolution_fwhmat1MeV << " (keV) -> sigma = " << energy_resolution << " (keV) " << endl;
  cout << "Folding with particle Detection (DSSSD) Position Resolution FWHM = " << Run::position_resolution_dsssd << " (mm) -> sigma = " << Run::position_resolution_dsssd/2.35 << " (mm) " << endl;
  cout << "Folding with LYCCA time resolution FWHM = " <<Run::lycca_time_resolution_fwhm << " (ps) " << endl;
  cout << endl;

  char temp[500], lmin[200];
  char masklabel[20];
  int mask = 0;
  int int_i = 0;
  int event_index = 0;

  FILE *fin = fopen(Run::file1,"r");
  int par0 = 0, par5 = 0;
  float par1 = 0, par2=0, par3=0, par4=0, par6=0;
  int cl, cln;
  float betafrag = 0;
  float psi, theta, phi, dx,dy,dz;
  float tmp_x = 0, tmp_y = 0, tmp_z = 0;

  //  fscanf(fin,"%s ",temp); 

  fgets (temp , 500 , fin); // AGATA 7.3.0
  fgets (temp , 500 , fin); // MASK

  sscanf(temp,"%s %d",masklabel,&mask);
  
  cout << masklabel << " " << mask << endl;

  if(mask == 11100110)
    cout<< "Reading input file with time saved " << endl;
  else if(mask == 11100100)
    cout<< "Reading input file (no event time saved) " << endl;
  else
    {
      cout << " Input format not recongised " << endl; 
      exit(0);
    }

  while((strstr(temp,"CLUSTER")==0))
    {
      fgets (temp , 500 , fin);
      //      printf("%s\n",temp);
    }

  fgets (temp , 500 , fin);// two header lines
  fgets (temp , 500 , fin);

 while((strstr(temp,"ENDCLUSTER")==0))  
    {
      fgets (temp , 500 , fin);

      if((strstr(temp,"ENDCLUSTER")==0))
	{
	  sscanf(temp,"%d  %d %d  %f  %f  %f  %f  %f  %f",&cl, &cln, &cln, &psi, &theta, &phi, &dx,&dy,&dz); 
	  Run::setup_i.setposxyz_crincl(cln,dx,dy,dz);
	}
    }


  while((strstr(temp,"EULER")==0))
    {
      fgets (temp , 500 , fin);
      //      printf("%s\n",temp);
    }

  fgets (temp , 500 , fin);// two header-comment lines
  fgets (temp , 500 , fin);

  int tot_n_cluster = 0;
 
  while((strstr(temp,"ENDEULER")==0))  
    {
      fgets (temp , 500 , fin);

      if((strstr(temp,"ENDEULER")==0))
	{
	  if(strlen(temp)>1){//there is one empty line before endeuler
	  sscanf(temp,"%d  %d  %f  %f  %f  %f  %f  %f",&cl, &cln, &psi, &theta, &phi, &dx,&dy,&dz); 
	  Run::setup_i.setposxyz_cl(cl,dx,dy,dz);
	  tot_n_cluster++;
	  }
	}
    }
  
  int det_i = 0;
  
  cout << " Total number of cluster detectors: " << tot_n_cluster << endl;

  for(int cl_i = 0; cl_i < tot_n_cluster; cl_i++)
    {
      for(int j=0;j<3;j++)
	{
	  Run::setup_i.setposxyz_det(det_i,
				     Run::setup_i.getposx_cl(cl_i) + Run::setup_i.getposx_crincl(j),
				     Run::setup_i.getposy_cl(cl_i) + Run::setup_i.getposy_crincl(j),
				     Run::setup_i.getposz_cl(cl_i) + Run::setup_i.getposz_crincl(j));
	  
	  cout << " Setting Detector # " << det_i+1 << " at position : " << endl;
	  cout << " x = " << Run::setup_i.getposx_det(det_i) << " mm " << endl;
	  cout << " y = " << Run::setup_i.getposy_det(det_i) << " mm " << endl;
	  cout << " z = " << Run::setup_i.getposz_det(det_i) << " mm " << endl;
	  det_i++;
	}
    }
  
  fgets (temp , 500 , fin);
 
  while((strstr(temp,"CRYSTAL_LUT")==0))  
    {
      fgets (temp , 500 , fin);
    }

  int crystal_nr=0, lut=0, tot_cl_index = -1;
  int ref_lut_nr = 0;

  /*
  while((strstr(temp,"ENDCRYSTAL_LUT")==0))  
    {
      fgets (temp , 500 , fin);
      if((strstr(temp,"ENDCRYSTAL_LUT")==0))
	{
	  sscanf(temp,"%d %d",&crystal_nr,&lut);
	  if(crystal_nr == 0 && lut ==1)// double cluster geometry, first crystal is 1
	    {
	      ref_lut_nr = 1;
	    }
	  else if(crystal_nr == 0 && lut ==0) // triple cluster geometry, first crystal is 0
	    ref_lut_nr = 0;

	  if(lut==ref_lut_nr)
	    tot_cl_index++;
	  if(lut >=0)
	    Run::setup_i.setcrystalclindex(crystal_nr,tot_cl_index);
	}
    }

  if( ref_lut_nr == 1) // next will come the conventional triple cluster
    {
      fgets (temp , 500 , fin);

      while((strstr(temp,"EULER")==0)) //next euler corresponds to the triple clusters
	{
	  fgets (temp , 500 , fin);
	  //      printf("%s\n",temp);
	}

      fgets (temp , 500 , fin);// two header-comment lines
      fgets (temp , 500 , fin);

      
      while((strstr(temp,"ENDEULER")==0))  
	{
	  fgets (temp , 500 , fin);
	  
	  if((strstr(temp,"ENDEULER")==0) && (strstr(temp,"#")==0))
	    {
	      if(strlen(temp)>1){
		sscanf(temp,"%d  %d  %f  %f  %f  %f  %f  %f",&cl, &cln, &psi, &theta, &phi, &dx,&dy,&dz); 
		Run::setup_i.setposxyz_cl(cl,dx,dy,dz);
		tot_n_cluster++;
	      }
	    }
	}
  
  cout << " Total  number of ATC+ADC clusters: " << tot_n_cluster << endl;

  for(cl_i; cl_i < tot_n_cluster; cl_i++)
    {
      for(int j=0;j<3;j++)
	{
	  Run::setup_i.setposxyz_det(det_i,
				     Run::setup_i.getposx_cl(cl_i) + Run::setup_i.getposx_crincl(j),
				     Run::setup_i.getposy_cl(cl_i) + Run::setup_i.getposy_crincl(j),
				     Run::setup_i.getposz_cl(cl_i) + Run::setup_i.getposz_crincl(j));
	  
	  cout << " Setting Detector # " << det_i+1 << " at position : " << endl;
	  cout << " x = " << Run::setup_i.getposx_det(det_i) << " mm " << endl;
	  cout << " y = " << Run::setup_i.getposy_det(det_i) << " mm " << endl;
	  cout << " z = " << Run::setup_i.getposz_det(det_i) << " mm " << endl;
	  det_i++;
	}
    }
  

  while((strstr(temp,"CRYSTAL_LUT")==0))  
    {
      fgets (temp , 500 , fin);
    }

  while((strstr(temp,"ENDCRYSTAL_LUT")==0))  
    {
      fgets (temp , 500 , fin);
      if((strstr(temp,"ENDCRYSTAL_LUT")==0) && (strstr(temp,"-1")==0))
	{
	  sscanf(temp,"%d %d",&crystal_nr,&lut);
	  
	  if(lut==0)
	    tot_cl_index++;
	  if(lut >=0)
	    Run::setup_i.setcrystalclindex(crystal_nr,tot_cl_index);
	  cout << " crystal nr = " << crystal_nr << " tot_cl_index " << tot_cl_index << endl;
	}
    }
  
    }
  */

  int gen_type = -1;
  char word[30];

  int dummy_evnr = 0;
  float dumvar = 0;

  while((strstr(temp,"GENERATOR")==0))  
    {
      fgets (temp , 500 , fin);
    }
  sscanf(temp,"%s %d",word,&gen_type);
  
  if(gen_type == 0)
    {
      cout << " READING EVENTS FILE GENERATED WITH AGATA BUILT-IN GENERATOR " << endl;
      
      fgets (temp , 500 , fin);
      while((strstr(temp,"RECOIL")==0))  
	{
	  fgets (temp , 500 , fin);
	}
      
      sscanf(temp,"%s %f %f %f %f %f %f",word,&betafrag,&dumvar,&dumvar,&dumvar,&dumvar,&dumvar);
      cout << " Built-In Generator with Fixed Beta Value= " << betafrag << endl;


    while(!(strcmp(temp,"$")==0))  // list of events starts here
	{
	  fscanf(fin,"%s ",temp); 
	}
      
      //  while(!feof(fin) && event_index < 100)  
      while(!feof(fin)  && Run::event_i.geteventnumber() < Run::SimEvents)  
	{
	  fscanf(fin,"%d",&par0);

	  if(par0 == -102)
	    {
	      fscanf(fin,"%f %f %f",&tmp_x,&tmp_y,&tmp_z);

	      fscanf(fin,"%d %f %f %f %f %d",&par0,&par1,&par2,&par3,&par4,&par5);

	      dummy_evnr = par5 + 1;

	      if(dummy_evnr > 1)
		{
		  Run::fillPHEventi();// fill pulse height spectra from previous event in case of g-hits
		  Run::fillGammaHisto();
		  Run::event_i.clean();// clear info previous event
		}

	      Run::event_i.seteventnumber(dummy_evnr);

	      if((Run::event_i.geteventnumber())%10000==0)
		{
		  //		  cout <<  Run::event_i.geteventnumber() << " Events Processed of " << Run::SimEvents;
		  printf("%d Events Processed of %d (%.0f%)\r",Run::event_i.geteventnumber(),Run::SimEvents,100.0*((float)Run::event_i.geteventnumber())/((float)Run::SimEvents));
		  cout.flush();	      
		}

	      hEvents->Fill(Run::event_i.geteventnumber());
	    }
	  else if(par0 >= 0 && mask == 11100100)//no time
	    fscanf(fin,"%f %f %f %f %d",&par1,&par2,&par3,&par4,&par5);
	  else if(par0 >= 0 && mask == 11100110)//time
	    fscanf(fin,"%f %f %f %f %d  %f",&par1,&par2,&par3,&par4,&par5,&par6);
	  
	  if(par0 == -1)
	    {
	      event_index = par5;
	      int_i = 0;

	      if(event_index != (Run::event_i.geteventnumber() - 1))
		{ 
		  cout << " ERROR: inconsistent gamma-number and event-number, exiting... " << endl;
		  exit(-1);
		}
	      
	      Run::event_i.setegamma(par1);
	      Run::event_i.setgraydirection(par2,par3,par4);
	      Run::event_i.setfragmentbeta(betafrag);

	      Run::fillGDirXHisto();
	      Run::fillGDirYHisto();
	      Run::fillGDirZHisto();
	    }
	  else if(par0 >= 0) // there was an interacction
	    {
	      Run::event_i.setintflag(1);
	      
	      if(Run::event_i.getninteractions() == 0)// the first interaction
		{
		  Run::event_i.setgrayorigin(tmp_x,tmp_y,tmp_z);
		  Run::fillVertexPosEventi();//fills hgx, hgy, hgxz, etc at each event
		}
	      
	      int crystal_nr = par0;
	      int cl_nr = Run::setup_i.getcrystalclindex(crystal_nr);// my absolute cluster number for that crystal fired
	      
	      if(Run::event_i.getintindex() >= Run::MaxNint)
		{
		  if(Run::event_i.getflagbadevent() != 0)
		    {
		      cout << "WARNING AT EVENT " << event_index <<" with more than " << Run::MaxNint << " Interactions" << endl;
		      Run::event_i.flagbadevent();
		    }
		}
	      else
		{
		  Run::event_i.setdetnhit(crystal_nr);
		  Run::event_i.setnseghit(par5);
		  Run::event_i.setclnhit(cl_nr);
		  
		  if(Run::energy_resolution_fwhmat1MeV != 0)
		    energy_resolution = (fresol->Eval(par1))/2.35;
		  else
		    energy_resolution = 0;

		  float edepc = rnd->Gaus(par1,energy_resolution);
		  Run::hfwhme->Fill((par1 - edepc));
		  float xc = rnd->Gaus(par2,position_resolution);
		  Run::hfwhmx->Fill((par2 - xc));
		  float yc = rnd->Gaus(par3,position_resolution);
		  Run::hfwhmy->Fill((par3 - yc));
		  float zc = rnd->Gaus(par4,position_resolution);
		  Run::hfwhmz->Fill((par4 - zc));
		  
		  Run::event_i.setintflag(1);// there has been at least one interaction in this event
		  Run::event_i.set_edepint(par1);//raw
		  Run::event_i.set_edepintc(edepc);//conv
		  Run::event_i.setintcoor(par2,par3,par4);//raw
		  Run::event_i.setintcoorc(xc,yc,zc);//conv
		  Run::event_i.settimeint(par6);
		  Run::event_i.increase_edep(par1);
		  float e_gamma = Run::event_i.gettotedep();
		  //		  cout << " Event Nr. = " << run.event_i.geteventnumber() << " Eg = " << e_gamma << endl;
		  if(e_gamma == 0.0)
		    {
		  cout << "Error Eg = 0, exiting... " << endl;
		  //		  exit(-1);
		}
	      energy_resolution = (fresol->Eval(e_gamma))/2.35;
	      //	      Run::event_i.increase_edep(par1,edepc);
	      Run::event_i.set_edepc(rnd->Gaus(e_gamma,energy_resolution));
	      Run::event_i.increase_accedepint();
	      //	      if(edepc >= (Run::event_i.getthresholdEdep()))
	      Run::event_i.increase_nint();//tot number of interactions
	      
	      if( Run::event_i.getninteractions() > 1) // more than one interaction in this event with E>threshold
		{
		  if(Run::event_i.getdetnhit(Run::event_i.getintindex()) 
		     != 
		     Run::event_i.getdetnhit(Run::event_i.getintindex()-1))
		    {
		      Run::event_i.increasendethitted();// total number of crystals fired/event
		    }

		  int index_prev_int = Run::event_i.getintindex() - 1;
		  
		  if((Run::event_i.getedep(Run::event_i.gethitlargestedep())) < par1)
		    Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		  if((Run::event_i.gettimeint(Run::event_i.getfirsthit())) > par6)
		    Run::event_i.setfirsthit(Run::event_i.getintindex());
		}
	      else // first interaction n_int = 1
		{
		  // 	      Run::event_i.resetnint();
		  Run::event_i.setfirsthit(Run::event_i.getintindex());
		  Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		}
	      Run::event_i.increase_int_index();
		}
	    }
	}
      
      /*
      while(!(strcmp(temp,"$")==0))  
	{
	  fscanf(fin,"%s ",temp); 
	}
      
      //  while(!feof(fin) && event_index < 100)  
      while(!feof(fin)  && Run::event_i.geteventnumber() < Run::SimEvents)  
	{
	  fscanf(fin,"%d",&par0);

	  if(par0 < 0)
	    {
	      if(par0 == -1)
		fscanf(fin,"%f %f %f %f %d",&par1,&par2,&par3,&par4,&par5);
	      else if(par0 == -102)
		fscanf(fin,"%f %f %f",&tmp_x,&tmp_y,&tmp_z);
	    }
	  else if(par0 >= 0 && mask == 11100100)
	    fscanf(fin,"%f %f %f %f %d",&par1,&par2,&par3,&par4,&par5);
	  else if(par0 >= 0 && mask == 11100110)
	    fscanf(fin,"%f %f %f %f %d  %f",&par1,&par2,&par3,&par4,&par5,&par6);
	  
	  if(par0 == -1)
	    {
	      event_index = par5;
	      int_i = 0;
	      	      
	      if(event_index > 1)
		{
		  Run::fillPHEventi();// fill pulse height spectra from previous event in case of g-hits
		  Run::fillGammaHisto();
		  Run::event_i.clean();// clear info previous event
		}

	      Run::event_i.seteventnumber(event_index+1);

	      //printf("%d %f %f %f %f %d\n",par0,par1,par2,par3,par4,par5); 

	      Run::event_i.setegamma(par1);
	      Run::event_i.setgraydirection(par2,par3,par4);
	      Run::event_i.setfragmentbeta(0.43);
	      Run::fillGDirXHisto();
	      Run::fillGDirYHisto();
	      Run::fillGDirZHisto();
	    } 
	  else if(par0 >= 0) // there was an interacction
	    {
	      Run::event_i.setintflag(1);
	      
	      if(Run::event_i.getninteractions() == 0)// the first interaction
		{
		  Run::event_i.setgrayorigin(tmp_x,tmp_y,tmp_z);
		  Run::fillVertexPosEventi();//fills hgx, hgy, hgxz, etc at each event
		}
	      
	      int crystal_nr = par0;
	      int cl_nr = Run::setup_i.getcrystalclindex(crystal_nr);// my absolute cluster number for that crystal fired
	      
	      if(Run::event_i.getintindex() >= Run::MaxNint)
		{
		  if(Run::event_i.getflagbadevent() != 0)
		{
		  cout << "WARNING AT EVENT " << event_index <<" with more than " << Run::MaxNint << " Interactions" << endl;
		  Run::event_i.flagbadevent();
		}

		}
	      else
		{
		  Run::event_i.setdetnhit(crystal_nr);
		  Run::event_i.setnseghit(par5);
		  
		  Run::event_i.setclnhit(cl_nr);
		  
		  if(Run::energy_resolution_fwhmat1MeV != 0)
		    energy_resolution = (fresol->Eval(par1))/2.35;
		  else
		    energy_resolution = 0;
		  float edepc = rnd->Gaus(par1,energy_resolution);
		  Run::event_i.hfwhme->Fill((par1 - edepc));
		  float xc = rnd->Gaus(par2,position_resolution);
		  Run::event_i.hfwhmx->Fill((par2 - xc));
		  float yc = rnd->Gaus(par3,position_resolution);
		  Run::event_i.hfwhmy->Fill((par3 - yc));
		  float zc = rnd->Gaus(par4,position_resolution);
		  Run::event_i.hfwhmz->Fill((par4 - zc));
		  
		  Run::event_i.setintflag(1);
		  Run::event_i.set_edepint(par1);
		  Run::event_i.set_edepintc(edepc);
		  Run::event_i.setintcoor(par2,par3,par4);
		  Run::event_i.setintcoorc(xc,yc,zc);
		  Run::event_i.settimeint(par6);
		  Run::event_i.increase_edep(par1);
		  float e_gamma = Run::event_i.gettotedep();
		  if(e_gamma == 0.0)
		    {
		      cout << "Error Eg = 0, (not) exiting... " << endl;
		      //		  exit(-1);
		    }
		  energy_resolution = (fresol->Eval(e_gamma))/2.35;
		  //	      Run::event_i.increase_edep(par1,edepc);
		  Run::event_i.set_edepc(rnd->Gaus(e_gamma,energy_resolution));
		  Run::event_i.increase_accedepint();
		  //	      if(edepc >= (Run::event_i.getthresholdEdep()))
		  Run::event_i.increase_nint();//tot number of interactions
		  
		  if( Run::event_i.getninteractions() > 1) // more than one interaction in this event with E>threshold
		    {
		      if(Run::event_i.getdetnhit(Run::event_i.getintindex()) 
			 != 
			 Run::event_i.getdetnhit(Run::event_i.getintindex()-1))
			{
			  Run::event_i.increasendethitted();// total number of crystals fired/event
			}

		      int index_prev_int = Run::event_i.getintindex() - 1;
		      
		      if((Run::event_i.getedep(Run::event_i.gethitlargestedep())) < par1)
			Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		      if((Run::event_i.gettimeint(Run::event_i.getfirsthit())) > par6)
			Run::event_i.setfirsthit(Run::event_i.getintindex());
		    }
		  else // first interaction n_int = 1
		    {
		      // 	      Run::event_i.resetnint();
		      Run::event_i.setfirsthit(Run::event_i.getintindex());
		      Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		    }
		  
		  Run::event_i.increase_int_index();
		}
	    }
	  //	  cout << " event " << event_index << " nr. hit " << Run::event_i.getninteractions() << endl;
	}
      */
    }
  else if(gen_type == 1)
    {

      while(!(strcmp(temp,"$")==0))  // list of events starts here
	{
	  fscanf(fin,"%s ",temp); 
	}
      
      //  while(!feof(fin) && event_index < 100)  
      while(!feof(fin)  && Run::event_i.geteventnumber() < Run::SimEvents)  
	{
	  fscanf(fin,"%d",&par0);

	  if(par0 == -100)
	    {
	      fscanf(fin,"%d",&dummy_evnr);

	      if(dummy_evnr > 1)
		{
		  Run::fillPHEventi();// fill pulse height spectra from previous event in case of g-hits
		  Run::fillGammaHisto();
		  Run::event_i.clean();// clear info previous event
		}

	      Run::event_i.seteventnumber(dummy_evnr);

	      if((Run::event_i.geteventnumber())%1000==0)
		{
		  //		  cout <<  Run::event_i.geteventnumber() << " Events Processed of " << Run::SimEvents;
		  printf("%d Events Processed of %d (%.0f%)\r",Run::event_i.geteventnumber(),Run::SimEvents,100.0*((float)Run::event_i.geteventnumber())/((float)Run::SimEvents));
		  cout.flush();	      
		}

	      hEvents->Fill(Run::event_i.geteventnumber());

	      fscanf(fin,"%d %f %f %f %f",&par0,&par2,&par3,&par4,&par6);

	      betafrag = par2;

	      if(par0 != -101)
		{
		  cout << " Wrong event header " << par0<< " instead of -101 , exiting... " << endl;
		  exit(-1);
		}
	      fscanf(fin,"%d %f %f %f",&par0,&tmp_x,&tmp_y,&tmp_z);
	      //	      printf("DEBUG %d %f %f %f \n",par0,tmp_x,tmp_y,tmp_z);
	      if(par0 != -102)
		{
		  cout << " Wrong event header " << par0 << " instead of -102 , exiting... " << endl;
		  exit(-1);
		}

	      fscanf(fin,"%d %f %f %f %f %d",&par0,&par1,&par2,&par3,&par4,&par5);

	      //	      if(par0 != -1) // no gamma or muon
	      //		{
		  //		  cout << " Wrong event header " << par0<< " instead of -1 , exiting... " << endl;
		  //		  exit(-1);
	      //		}
	    }
	  else if(par0 >= 0 && mask == 11100100)//no time
	    fscanf(fin,"%f %f %f %f %d",&par1,&par2,&par3,&par4,&par5);
	  else if(par0 >= 0 && mask == 11100110)//time
	    fscanf(fin,"%f %f %f %f %d  %f",&par1,&par2,&par3,&par4,&par5,&par6);
	  
	  if(par0 == -1 || par0 == -96)
	    {
	      event_index = par5;
	      int_i = 0;

	      if(event_index != (Run::event_i.geteventnumber() - 1))
		{ 
		  cout << " ERROR: inconsistent gamma-number and event-number, exiting... " << endl;
		  exit(-1);
		}
	      
	      Run::event_i.setegamma(par1);
	      Run::event_i.setgraydirection(par2,par3,par4);
	      Run::event_i.setfragmentbeta(betafrag);

	      Run::fillGDirXHisto();
	      Run::fillGDirYHisto();
	      Run::fillGDirZHisto();
	    }
	  else if(par0 >= 0) // there was an interacction
	    {
	      Run::event_i.setintflag(1);
	      
	      if(Run::event_i.getninteractions() == 0)// the first interaction
		{
		  Run::event_i.setgrayorigin(tmp_x,tmp_y,tmp_z);
		  Run::fillVertexPosEventi();//fills hgx, hgy, hgxz, etc at each event
		}
	      
	      int crystal_nr = par0;
	      int cl_nr = Run::setup_i.getcrystalclindex(crystal_nr);// my absolute cluster number for that crystal fired
	      
	      if(Run::event_i.getintindex() >= Run::MaxNint)
		{
		  if(Run::event_i.getflagbadevent() != 0)
		{
		  cout << "WARNING AT EVENT " << event_index <<" with more than " << Run::MaxNint << " Interactions" << endl;
		  Run::event_i.flagbadevent();
		}
	    }
	  else
	    {
	      Run::event_i.setdetnhit(crystal_nr);
	      Run::event_i.setnseghit(par5);
	      Run::event_i.setclnhit(cl_nr);

	      if(Run::energy_resolution_fwhmat1MeV != 0)
		energy_resolution = (fresol->Eval(par1))/2.35;
	      else
		energy_resolution = 0;

	      float edepc = rnd->Gaus(par1,energy_resolution);
	      Run::hfwhme->Fill((par1 - edepc));
	      float xc = rnd->Gaus(par2,position_resolution);
	      Run::hfwhmx->Fill((par2 - xc));
	      float yc = rnd->Gaus(par3,position_resolution);
	      Run::hfwhmy->Fill((par3 - yc));
	      float zc = rnd->Gaus(par4,position_resolution);
	      Run::hfwhmz->Fill((par4 - zc));

	      Run::event_i.setintflag(1);// there has been at least one interaction in this event
	      Run::event_i.set_edepint(par1);//raw
	      Run::event_i.set_edepintc(edepc);//conv
	      Run::event_i.setintcoor(par2,par3,par4);//raw
	      Run::event_i.setintcoorc(xc,yc,zc);//conv
	      Run::event_i.settimeint(par6);
	      Run::event_i.increase_edep(par1);
	      float e_gamma = Run::event_i.gettotedep();
	      //	      cout << " Event Nr. = " << run.event_i.geteventnumber() << " Eg = " << e_gamma << endl;
	      if(e_gamma == 0.0)
		{
		  cout << "Error Eg = 0, exiting... " << endl;
		  //		  exit(-1);
		}
	      energy_resolution = (fresol->Eval(e_gamma))/2.35;
	      //	      Run::event_i.increase_edep(par1,edepc);
	      Run::event_i.set_edepc(rnd->Gaus(e_gamma,energy_resolution));
	      Run::event_i.increase_accedepint();
	      //	      if(edepc >= (Run::event_i.getthresholdEdep()))
	      Run::event_i.increase_nint();//tot number of interactions
	      
	      if( Run::event_i.getninteractions() > 1) // more than one interaction in this event with E>threshold
		{
		  if(Run::event_i.getdetnhit(Run::event_i.getintindex()) 
		     != 
		     Run::event_i.getdetnhit(Run::event_i.getintindex()-1))
		    {
		      Run::event_i.increasendethitted();// total number of crystals fired/event
		    }

		  int index_prev_int = Run::event_i.getintindex() - 1;
		  
		  if((Run::event_i.getedep(Run::event_i.gethitlargestedep())) < par1)
		    Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		  if((Run::event_i.gettimeint(Run::event_i.getfirsthit())) > par6)
		    Run::event_i.setfirsthit(Run::event_i.getintindex());
		}
	      else // first interaction n_int = 1
		{
		  // 	      Run::event_i.resetnint();
		  Run::event_i.setfirsthit(Run::event_i.getintindex());
		  Run::event_i.sethitlargestedep(Run::event_i.getintindex());
		}
	      Run::event_i.increase_int_index();
	    }
	    }
	}
      
    }
  else
    {
      cout << " GENERATOR type not recognised, exiting ... " << endl;
      exit(-1);
    }
  
  nevents = event_index + 1; // total number of events from AGATA file (not calculated/nor cross-checked)

  /*
    else if(strcmp(temp,"OUTPUTFILE")==0)  {
    fscanf(fin,"%s ",&root_out); 
	printf("%s %s \n",temp,root_out);
	}
	else if(strcmp(temp,"CLUSTERPBTHICKNESS")==0)  {
	fscanf(fin,"%f ",&cluster_thickness_Pb); 
	printf("%s %f \n",temp,cluster_thickness_Pb);
	}
	
	}
      */
}


void CalcNewCoordinates(const float newdistance, char *filename = "../prespec_geom/A180eulerPRESPEC.list")
{
  ifstream finp(filename);
  char temp[500];
  finp.getline(temp,90);
  finp.getline(temp,90);
  float dumy1, dumy2, dumy3, dumy4, dumy5, x, y , z;
  float distance = 0;
  float factor = 0, factor2 = 0;

  while(!finp.eof())
    {
      finp >> dumy1  >> dumy2 >> dumy3 >> dumy4 >> dumy5 >> x >> y >> z;
      distance = sqrt(pow(x,2) + pow(y,2) + pow(z,2) );
      factor = newdistance/distance;
      cout << " factor = " << factor << endl;

     //      cout << " d = " << sqrt(pow(x,2) + pow(y,2) + pow(z,2) ) << " mm" <<  endl;
      //      printf("   %d  %d   %12.6f  %12.6f  %12.6f %12.6f  %12.6f  %12.6f\n",dumy1,dumy2,dumy3,dumy4,dumy5,x,y,z);
      printf("   %d  %d   %12.6f  %12.6f  %12.6f %12.6f  %12.6f  %12.6f\n",dumy1,dumy2,dumy3,dumy4,dumy5,factor*x,factor*y,factor*z);
    }

}


Double_t fpeaksbkg(Double_t *x, Double_t *par) {
  const int npeaks = 1;
  //  Double_t result = par[0]*TMath::Exp(par[1]*x[0]);
  Double_t result = par[0]+par[1]*x[0];
   for (Int_t p=0;p<npeaks;p++) {
      Double_t norm  = par[3*p+2];
      Double_t mean  = par[3*p+3];
      Double_t sigma = par[3*p+4];
      result += norm*TMath::Gaus(x[0],mean,sigma);
   }
   return result;
}


TF1* FitSpectrum(TH1F *hEdep1dc)
{
  //  hEdep1dc->Rebin(4);

  Int_t npeaks = 4;
  float threshold = 0.40;// amplitude_smallest_peak/amplitude_highest_peak
  float resolution = 2;
 
  if(Run::flag_activetarget)
    float default_sigma = 2; // approx
  else
    float default_sigma = 30; // approx


  TSpectrum *s = new TSpectrum(2*npeaks,resolution);
  Int_t nfound = s->Search(hEdep1dc,default_sigma,"goff",threshold);
  Float_t *xpeaks = s->GetPositionX();
  Float_t firstpeak = xpeaks[0];
  for(int j=0;j<nfound;j++)
    if(xpeaks[j]<firstpeak)
      firstpeak = xpeaks[j];

  cout << " peaks found = "<< nfound << endl;
  cout << " First peak at = "<< firstpeak << endl;

  if(nfound == 0)
    {
      firstpeak = (Run::higherE + Run::lowerE)/2.0;
      cout << " WARNING:" << endl;
      cout << " Assigning manually peak at " << firstpeak << endl;
    }

  Double_t par[10];
  par[0] = 0;
  par[1] = 0;
  par[2] = hEdep1dc->GetBinContent(hEdep1dc->FindBin(firstpeak));
  par[3] = firstpeak;
  par[4] = default_sigma;  

  Char_t fitname[100];
  sprintf(fitname,"fit");
  fit = new TF1(fitname,fpeaksbkg,firstpeak*0.5,firstpeak*1.5,5);
  fit->SetLineColor(2);
  h2 = (TH1I*)hEdep1dc->Clone("h2");
  TVirtualFitter::Fitter(h2,10); //we may have more than the default 25 parameters
  fit->SetParameters(par);
  fit->SetNpx(1000);
  fit->SetLineWidth(1);
  h2->Fit(fitname,"","",firstpeak*0.5,firstpeak*1.5);  
  
  double a = fit->GetParameter(3);
  double sigma = fit->GetParameter(4);
  if(sigma<0)
    sigma *= -1;

  double t = fit->GetParameter(3)-fit->GetParameter(1);
  double b = fit->GetParameter(2);
  int peak_bini = hEdep1dc->FindBin(firstpeak - 5*(sigma));
  int peak_binf = hEdep1dc->FindBin(firstpeak + 5*(sigma));
  double tot_counts_peak = hEdep1dc->Integral(peak_bini,peak_binf);// NO BKG Substracted!!!

  //estimating background
  TF1 *fbkg = new TF1("fbkg","[0]+[1]*(x)",0,10000);//0-10MeV
  fbkg->SetParameter(0,fit->GetParameter(0));
  fbkg->SetParameter(1,fit->GetParameter(1));

  TH1F *hbkg = (TH1F*) hEdep1dc->Clone("hbkg");
  hbkg->Reset("ICE");

  for(int i = hEdep1dc->FindBin(firstpeak - 5*(sigma)); i < hEdep1dc->FindBin(firstpeak + 5*(sigma));i++)
    {
      hbkg->SetBinContent(i,fbkg->Eval(hEdep1dc->GetBinCenter(i)));
    }

  double bkg = hbkg->Integral();

  cout << "Photopeak Efficiency (Fit) = " << (tot_counts_peak - bkg)/((float) Run::SimEvents) << endl;
  cout << " Peak = " << tot_counts_peak << " Counts" <<endl;
  cout << " Bkg Level = " << bkg << " Counts"<< endl;
  cout << " P/T  = " << (tot_counts_peak - bkg)/ hEdep1dc->Integral() << endl;

  cout << "Integral between bin = " << peak_bini << " and bin = " << peak_binf << endl;
  cout << " FWHM = " << sigma*2.35 << " keV" << endl;
  return fit;
}



void getHitsDistribution(Event *event, int nevents, TH1I *hh, TH1I *hs, TH1I *hd, TH1I *hc)
{
  hh->GetXaxis()->SetTitle("Number of Hits per Event");
  hs->GetXaxis()->SetTitle("Segments ");
  hd->GetXaxis()->SetTitle("Crystals ");
  hc->GetXaxis()->SetTitle("Cluster # ");
  hc->GetYaxis()->SetTitle("Hits (%) ");

  for(int i=0;i<nevents;i++)
    {
      if(event[i].getintflag())
	{
	  int nrofint =  event[i].getninteractions();
	  hh->Fill(nrofint);

	  for(int j=0;j<event[i].getninteractions();j++)
	    {
	      if(event[i].getedep(j) > event[i].getthresholdEdep())
		{
		  hs->Fill(event[i].getnseghit(j));
		  hd->Fill(event[i].getdetnhit(j));
		  hc->Fill(1+event[i].getdetnhit(j));
		}
	    }
	}
    }
  float max = hs->GetMaximum();
  if(max>0)
    hs->Scale(100/max);
  max = hd->GetMaximum();
  if(max>0)
    hd->Scale(100/max);
  max = hc->GetMaximum();
  if(max>0)
  hc->Scale(100/max);

  hs->GetYaxis()->SetRangeUser(0,110);
  hd->GetYaxis()->SetRangeUser(0,110);
  hc->GetYaxis()->SetRangeUser(0,110);
}


void SetStyle(TH1 *h)
{
  h->GetXaxis()->SetTitleSize(0.08);
  h->GetYaxis()->SetTitleSize(0.08);
  h->GetXaxis()->SetTitleOffset(1.03);
  h->GetYaxis()->SetTitleOffset(0.7);
  h->GetXaxis()->SetLabelSize(0.08);
  h->GetYaxis()->SetLabelSize(0.08);
}


void printGeneralSettings(Event event)
{
  cout << " ****************************************** " << endl;
  cout << " Edep Threshold = " << event.getthresholdEdep() << " (keV)" << endl;
  
  cout << " ****************************************** " << endl;
}


void CheckHitLargestEdep(Event *event, int nevents, TH1I *h, TH1F *hd)
{
  h->GetXaxis()->SetTitle("|First Hit - Max. Edep Hit|");
  h->GetXaxis()->SetTitleOffset(0.8);
  hd->GetXaxis()->SetTitle("distance_{First Hit - Max. Edep Hit} (mm)");
  hd->GetXaxis()->SetTitleOffset(0.8);
  for(int i=0;i<nevents;i++)
    {
      if(event[i].getintflag())
	{
	  //	  int nrofint =  event[i].getninteractions();
	  //	  cout << " Event index " << i << " Max. Edep " << event[i].getedep(event[i].gethitlargestedep()) << " (keV)    Hit index = " <<  event[i].gethitlargestedep() << " First Hit index = " << event[i].getfirsthit() << endl;
// 	  for(int j=0;j<event[i].getninteractions();j++)
// 	    {
// 	      cout << "        Hit index " << j << " Edep = " << event[i].getedep(j) << " (keV) " << endl;
// 	    }
	  int hitlargestedep = event[i].gethitlargestedep();
	  int firsthit = event[i].getfirsthit();
	  h->Fill(abs( hitlargestedep - firsthit));
	  float distance = sqrt(pow((event[i].getxint(hitlargestedep) - event[i].getxint(firsthit)),2)
				+ pow((event[i].getyint(hitlargestedep) - event[i].getyint(firsthit)),2)
				+ pow((event[i].getzint(hitlargestedep) - event[i].getzint(firsthit)),2));
	  
	  hd->Fill(distance);
	}
    }
}
				
	

void CheckEgtoEg0(Event *event, int nevents, TH1F *hct, TH1F *hEg, TH1F *htheta, TH2F*hEgvsCT,TH2F *hEgvsT,float beta)
{
  hct->GetXaxis()->SetTitle("cos(#theta)");
  hct->GetYaxis()->SetTitle("Events ");
  hct->GetXaxis()->SetTitleOffset(0.8);
  hEg->GetXaxis()->SetTitle("E_{#gamma} (keV)");
  hEg->GetXaxis()->SetTitleOffset(0.8);
  hEg->GetYaxis()->SetTitle("Events ");
  htheta->GetXaxis()->SetTitle(" #theta (deg)");
  htheta->GetXaxis()->SetTitleOffset(0.8);
  htheta->GetYaxis()->SetTitle("Events ");
  hEgvsCT->GetXaxis()->SetTitle("cos(#theta^{Lab})");
  hEgvsCT->GetYaxis()->SetTitle("E_{#gamma} (keV)");
  hEgvsCT->GetXaxis()->SetTitleOffset(0.8);
  hEgvsT->GetXaxis()->SetTitle("#theta^{Lab} (deg)");
  hEgvsT->GetYaxis()->SetTitle("E_{#gamma} (keV)");
  hEgvsT->GetXaxis()->SetTitleOffset(0.8);

  const float Ego = 1000.0;

  for(int i=0;i<nevents;i++)
    {
      float costheta = event[i].getgzdir();
      //      float theta = 2*3.141592*acos(event[i].getgzdir())/360.0;
      float theta = acos(costheta)*360.0/(2.0*3.141592);
      float Eg = Ego*sqrt(1-beta*beta)/(1-beta*costheta);
      hct->Fill(costheta);
      htheta->Fill(theta);
      //htheta->Fill(Eg/Ego);
      hEg->Fill(Eg);
      hEgvsCT->Fill(costheta,Eg);
      hEgvsT->Fill(theta,Eg);
    }
}
					
				

void  CheckLorentzBoost(Event *event, int nevents, TH2F *h2d)
{

  h2d->GetXaxis()->SetTitle("Z");
  h2d->GetYaxis()->SetTitle("X");
  
  for(int i=0;i<nevents;i++)
    {
      float x = event[i].getgxdir();
      float z = event[i].getgzdir();

      //      h2d->Fill(acos(z)*360.0/(2.0*3.141592),acos(x)*360.0/(2.0*3.141592));
      h2d->Fill(z,x);
    }
}


void DrawResolutions(TCanvas *crs)
{
  SetStyle(Run::hfwhmx);
  Run::hfwhmx->GetXaxis()->SetTitle("#DeltaX (mm)");
  Run::hfwhmx->GetYaxis()->SetTitle("Events");
  SetStyle(Run::hfwhmy);
  Run::hfwhmy->GetXaxis()->SetTitle("#DeltaY (mm)");
  Run::hfwhmy->GetYaxis()->SetTitle("Events");
  SetStyle(Run::hfwhmz);
  Run::hfwhmz->GetXaxis()->SetTitle("#DeltaZ (mm)");
  Run::hfwhmz->GetYaxis()->SetTitle("Events");
  SetStyle(Run::hfwhme);
  Run::hfwhme->GetXaxis()->SetTitle("#DeltaE (keV)");
  Run::hfwhme->GetYaxis()->SetTitle("Events");
  Run::hfwhme->GetYaxis()->SetNdivisions(3);

  crs->cd(1);
  Run::hfwhmx->DrawCopy();
  crs->cd(2);
  Run::hfwhmy->DrawCopy();
  crs->cd(3);
  Run::hfwhmz->DrawCopy();
  crs->cd(4);
  Run::hfwhme->DrawCopy();
}


void DrawGammaSource(TCanvas *crs)
{
  gStyle->SetPalette(1);

  SetStyle(Run::hgx);
  Run::hgx->GetXaxis()->SetTitle("#gamma-source Position X (mm)");
  Run::hgx->GetYaxis()->SetTitle("Events");

  SetStyle(Run::hgy);
  Run::hgy->GetXaxis()->SetTitle("#gamma-source Position Y (mm)");
  Run::hgy->GetYaxis()->SetTitle("Events");

  SetStyle(Run::hgz);
  Run::hgz->GetXaxis()->SetTitle("#gamma-source Position Z (mm)");
  Run::hgz->GetYaxis()->SetTitle("Events");

  Run::hgxy->GetXaxis()->SetTitle("#gamma-source Position X (mm)");
  Run::hgxy->GetYaxis()->SetTitle("#gamma-source Position Y (mm)");

  //  crs = new TCanvas("crs","",1);


  crs->cd(1);
  Run::hgxy->DrawCopy("colz");
  crs->cd(2);
  Run::hgx->DrawCopy();
  crs->cd(3);
  Run::hgy->DrawCopy();
  crs->cd(4);
  Run::hgz->DrawCopy();
}


TF1 *GetEResol(TCanvas *c)// returns E_resol (fwhm) for a certain Eg
{


  TH2F *h = new TH2F("h","Energy Resolution #Delta E (fwhm)",100,0,10000,100,0,10);
  TF1 *fr = new TF1("fr","[0]+[1]*sqrt(x)",0,10000);//0-10MeV
  h->GetXaxis()->SetTitle("E_{#gamma} (keV)");
  h->GetYaxis()->SetTitle("FWHM (keV)");
  fr->SetParameter(0,0.537);
  fr->SetParameter(1,4.625E-2);
  if(Run::flag_plot_Eresol)
    {
      cer = new TCanvas("cer","",1);
      cer->SetFillColor(10);
      h->Draw("");
      fr->Draw("Csame");
    }

  return fr;
}



int compareDemonstrator()
{
  char *lab0 = "#beta = 0";
  char *lab1 = "#beta = 30%";
  char *lab2 = "#beta = 50%";

  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  const int ndata = 4;
  float d[ndata] = {0,5,10,15};

  // my results for photopeak counts in +/- 2*sigma
  //  const float effb1[ndata] = {3,4.38,6.83,10.96};
  //  const float effb2[ndata] = {4.03,5.52,7.96,11.89};
  //  const float effb3[ndata] = {5.18,7.13,10.15,13.89};

//   // my results for photopeak counts in +/- 5*sigma
//   const float effb1[ndata] = {3.10,4.56,7.12,11.40};
//   const float effb2[ndata] = {4.4,6.08,8.82,13.0};
//   const float effb3[ndata] = {5.67,7.83,11.10,15.05};

  // my results for photopeak counts in +/- 5*sigma and correct E_resol
  const float effb1[ndata] = {3.12,4.57,7.12,11.43};
  const float effb2[ndata] = {4.32,6.03,8.85,13.09};
  const float effb3[ndata] = {5.71,7.89,11.26,15.25};

//   // assuming g_Delta_r = 5 mm
//   const float fwhmb1[ndata] = {3.04,3.09,3.09,3.06};
//   const float fwhmb2[ndata] = {3.65,4.23,5.17,6.94};
//   const float fwhmb3[ndata] = {4.93,6.30,8.53,12.23};

//   // assuming g_Delta_r = 4 mm
//   const float fwhmb1[ndata] = {3.04,3.09,3.09,3.06};
//   const float fwhmb2[ndata] = {3.47,3.88,4.56,5.86};
//   const float fwhmb3[ndata] = {4.29,5.30,7.08,9.96};

  // g_Delta_r = 5 mm and correct E_resol

  const float fwhmb1[ndata] = {1.98,2.00,1.98,2.00};
  const float fwhmb2[ndata] = {2.75,3.51,4.57,6.42};
  const float fwhmb3[ndata] = {4.59,5.90,8.32,12.04};

  // results from Enrico Farnea (ef)
  const float effb1ef[ndata] = {2.8,4.1,6.6,10.5};
  const float effb2ef[ndata] = {4.5,6,9.5,13.8};
  const float effb3ef[ndata] = {6,8,11.8,16.1};

  const float fwhmb1ef[ndata] = {2, 2, 2, 2};
  const float fwhmb2ef[ndata] = {2.6, 3.2, 4, 6.2};
  const float fwhmb3ef[ndata] = {4.3, 5.5, 8, 12};

  TGraph *greffb1 = new TGraph(ndata,d,effb1);
  TGraph *greffb2 = new TGraph(ndata,d,effb2);
  TGraph *greffb3 = new TGraph(ndata,d,effb3);

  TGraph *greffb1ef = new TGraph(ndata,d,effb1ef);
  TGraph *greffb2ef = new TGraph(ndata,d,effb2ef);
  TGraph *greffb3ef = new TGraph(ndata,d,effb3ef);

  greffb1->SetMarkerStyle(20);
  greffb2->SetMarkerStyle(21);
  greffb3->SetMarkerStyle(22);
  greffb1ef->SetMarkerStyle(24);
  greffb2ef->SetMarkerStyle(25);
  greffb3ef->SetMarkerStyle(26);

  greffb1->GetYaxis()->SetRangeUser(0,20);
  greffb1->GetXaxis()->SetTitle("Shift from center (cm)");
  greffb1->GetYaxis()->SetTitle("Photopeak Efficiency (%)");
  greffb1->SetTitle("Efficiency");
  greffb1->Draw("ALP");
  greffb2->Draw("LP");
  greffb3->Draw("LP");
  greffb1ef->Draw("LP");
  greffb2ef->Draw("LP");
  greffb3ef->Draw("LP");

  leg = new TLegend(0.7,0.85,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(greffb1,lab0,"lp");
  leg->AddEntry(greffb2,lab1,"lp");
  leg->AddEntry(greffb3,lab2,"lp");

  leg->Draw();


  TCanvas *cr = new TCanvas("cr","",1);
  cr->SetFillColor(10);

  TGraph *grfwhmb1 = new TGraph(ndata,d,fwhmb1);
  TGraph *grfwhmb2 = new TGraph(ndata,d,fwhmb2);
  TGraph *grfwhmb3 = new TGraph(ndata,d,fwhmb3);
  TGraph *grfwhmb1ef = new TGraph(ndata,d,fwhmb1ef);
  TGraph *grfwhmb2ef = new TGraph(ndata,d,fwhmb2ef);
  TGraph *grfwhmb3ef = new TGraph(ndata,d,fwhmb3ef);

  grfwhmb1->SetMarkerStyle(20);
  grfwhmb2->SetMarkerStyle(21);
  grfwhmb3->SetMarkerStyle(22);
  grfwhmb1ef->SetMarkerStyle(24);
  grfwhmb2ef->SetMarkerStyle(25);
  grfwhmb3ef->SetMarkerStyle(26);

  grfwhmb1->GetYaxis()->SetRangeUser(0,15);
  grfwhmb1->GetXaxis()->SetTitle("Shift from center (cm)");
  grfwhmb1->GetYaxis()->SetTitle("FWHM (keV)");
  grfwhmb1->SetTitle("Resolution");
  grfwhmb1->Draw("ALP");
  grfwhmb2->Draw("LP");
  grfwhmb3->Draw("LP");
  grfwhmb1ef->Draw("LP");
  grfwhmb2ef->Draw("LP");
  grfwhmb3ef->Draw("LP");

  leg->Draw();
  
  return 0;
}




void comparePerformanceFinalGeo()
{
  //  gStyle->SetOptTitle(0);

  char *lab0 = "2 rings, 4 + 6 clusters, big cyl. pipe";
  char *lab1 = "2 rings, 4 + 6 clusters, small sq. pipe";
  char *lab2 = "symmetric ring 10 cluster";

  const int ndata = 7;
  const int ndata1 = 6;
  float d[ndata1] = {-15,-10,-5,0,5,10};
  for(int i=0;i<ndata1;i++)
    d[i] += 15.0;

  float d2[ndata] = {0,5,8,10,12,15,20};

  for(int i=0;i<ndata;i++)
    d2[i] = 23.5 - d2[i];

  // PHOTOPEAK EFFICIENCY 
  TCanvas *ce = new TCanvas("ce","",1);
  ce->SetFillColor(10);
  // 5mm
  const float pheffd1[ndata1] = {15.5,18.8,19.5,14.8,10.6,7.4};//agantav6_ds 
  const float pheffd12[ndata1] = {12.1,13.2,13.9,13.8,12.5,10.0};//agantav7_ds 
  const float pheffd2[ndata] = {6.59,7.60,7.98,8.21,8.20,7.84,6.71}; // agata v2


  TGraph *gr = new TGraph(ndata1,d,pheffd1);
  TGraph *gr12 = new TGraph(ndata1,d,pheffd12);
  TGraph *grd2 = new TGraph(ndata,d2,pheffd2);
  gr->SetMarkerStyle(21);
  gr12->SetMarkerStyle(22);
  grd2->SetMarkerStyle(25);

  gr->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs distance");
  gr->Draw("ALP");
  gr12->Draw("LP");
  grd2->Draw("LP");


  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12,lab0,"lp");
  leg->AddEntry(gr,lab1,"lp");
  leg->AddEntry(grd2,lab2,"lp");

  leg->Draw();


  TCanvas *cegg = new TCanvas("cegg","",1);
  cegg->SetFillColor(10);
  // 5mm
  float pheffd1gg[ndata1];//agantav6_ds 
  float pheffd12gg[ndata1];//agantav7_ds 
  float pheffd2gg[ndata]; // agata v2

  for(int i=0;i<ndata1;i++)
    {
      pheffd1gg[i] = TMath::Power(pheffd1[i]/100.0,2)*100;
      pheffd12gg[i] = TMath::Power(pheffd12[i]/100.0,2)*100;
    }

 for(int i=0;i<ndata;i++)
   pheffd2gg[i] = TMath::Power(pheffd2[i]/100.0,2)*100;
 
 
 TGraph *grgg = new TGraph(ndata1,d,pheffd1gg);
 TGraph *gr12gg = new TGraph(ndata1,d,pheffd12gg);
 TGraph *grd2gg = new TGraph(ndata,d2,pheffd2gg);
 grgg->SetMarkerStyle(21);
 gr12gg->SetMarkerStyle(22);
 grd2gg->SetMarkerStyle(25);

 grgg->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
 grgg->GetYaxis()->SetTitle("#gamma-#gamma Efficiency (%)");
 grgg->SetTitle("#gamma-#gamma Efficiency");
 grgg->Draw("ALP");
 gr12gg->Draw("LP");
 grd2gg->Draw("LP");

  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12gg,lab0,"lp");
  leg->AddEntry(grgg,lab1,"lp");
  leg->AddEntry(grd2gg,lab2,"lp");

  leg->Draw();


  // RESOLUTION

  TCanvas *cr = new TCanvas("cr","",1);
  cr->SetFillColor(10);

  // 5mm
   const float resol1[ndata1] = {10.4,11.8,13.7,12.3,9.28,6.48};//agantav6
   const float resol12[ndata1] = {10.66,9.56,9.39,9.17,8.75,7.0};//agantav7
   const float resol2[ndata] = {6.27,7.28,7.79,8.23,8.69,8.79,8.86};//agatav2_ds (corrected)

  TGraph *gr1 = new TGraph(ndata1,d,resol1);
  TGraph *gr12 = new TGraph(ndata1,d,resol12);
  TGraph *gr2 = new TGraph(ndata,d2,resol2);
  gr1->SetMarkerStyle(21);
  gr12->SetMarkerStyle(22);
  gr2->SetMarkerStyle(25);
  gr2->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr2->GetYaxis()->SetTitle("FWHM (keV)");
  gr2->SetTitle("Intrinsic Spatial Resolution 5 mm");
  gr2->Draw("ALP");
  gr1->Draw("LP");
  gr12->Draw("LP");
  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12,lab0,"lp");
  leg->AddEntry(gr1,lab1,"lp");
  leg->AddEntry(gr2,lab2,"lp");
  leg->Draw();




  // RESOLUTION

  TCanvas *cr2 = new TCanvas("cr2","",1);
  cr2->SetFillColor(10);

  //2mm
  float resol1v2[ndata1] = {4.9,5.5,6.3,5.5,4.3,3.3};//agantav6
  float resol12v2[ndata1] = {4.9,4.6,4.4,4.3,4.2,3.6};//agantav7
  float resol2v2[ndata] = {3.0,3.4,3.7,3.8,4.0,4.1,4.0};//agatav2_ds (corrected)


  TGraph *gr1v2 = new TGraph(ndata1,d,resol1v2);
  TGraph *gr12v2 = new TGraph(ndata1,d,resol12v2);
  TGraph *gr2v2 = new TGraph(ndata,d2,resol2v2);
  gr1v2->SetMarkerStyle(21);
  gr12v2->SetMarkerStyle(22);
  gr2v2->SetMarkerStyle(25);
  gr2v2->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  gr2v2->GetYaxis()->SetTitle("FWHM (keV)");
  gr2v2->SetTitle("Intrinsic Spatial Resolution 2 mm");
  gr2v2->Draw("ALP");
  gr1v2->Draw("LP");
  gr12v2->Draw("LP");
  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr12v2,lab0,"lp");
  leg->AddEntry(gr1v2,lab1,"lp");
  leg->AddEntry(gr2v2,lab2,"lp");
  leg->Draw();

  /*


  TCanvas *cs = new TCanvas("cs","",1);
  cs->SetFillColor(10);

  float gs1[ndata1];
  float gs12[ndata1];
  float gs2[ndata];

  for(int i=0;i<ndata1;i++)
    {
      gs1[i] = pheffd1[i]/resol1[i];
      gs12[i] = pheffd12[i]/resol12[i];
    }

  for(int i=0;i<ndata;i++)
    {
      gs2[i] = pheffd2[i]/resol2[i];
    }

  TGraph *grgs1 = new TGraph(ndata1,d,gs1);
  TGraph *grgs12 = new TGraph(ndata1,d,gs12);
  TGraph *grgs2 = new TGraph(ndata,d2,gs2);
  grgs1->SetMarkerStyle(21);
  grgs12->SetMarkerStyle(22);
  grgs2->SetMarkerStyle(25);
  grgs1->GetXaxis()->SetTitle("distance from Sec. Target (cm)");
  grgs1->GetYaxis()->SetTitle("#gamma-ray sensitivity (a.u.)");
  grgs1->SetTitle("#gamma-ray sensitivity vs distance from Sec. Target");
  grgs1->Draw("ALP");
  grgs2->Draw("LP");
  grgs12->Draw("LP");

  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(grgs12,lab0,"lp");
  leg->AddEntry(grgs1,lab1,"lp");
  leg->AddEntry(grgs2,lab2,"lp");
  leg->Draw();
  */
}



void compareResolution1Cr()
{
//   char *lab0 = "1 Crystal at 0 deg";
//   char *lab1 = "1 Crystal at 20 deg";
//   char *lab2 = "1 Crystal at 39 deg";
  char *lab0 = "1 Crystal at 10 deg";
  char *lab1 = "1 Crystal at 20 deg";
  char *lab2 = "1 Crystal at 40 deg";

  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);
  const int ndata = 5;
  float d[ndata] = {5,10,15,20,70};
  
  TCanvas *cr = new TCanvas("cr","",1);
  cr->SetFillColor(10);

//   const float resol1[ndata] = {8.72,3.70,2.46,2.04,1.81};
//   const float resol2[ndata] = {10.62,6.22,4.56,3.54,1.91};
//   const float resol3[ndata] = {15.58,9.37,6.82,5.22,2.35};

  const float resol1[ndata] = {5.03,3.1,2.24,1.75,0.55};
  const float resol2[ndata] = {9.58,5.9,4.26,3.34,1.05};
  const float resol3[ndata] = {16,9.86,7.12,5.57,1.76};


  TGraph *gr1 = new TGraph(ndata,d,resol1);
  TGraph *gr2 = new TGraph(ndata,d,resol2);
  TGraph *gr3 = new TGraph(ndata,d,resol3);

  gr1->SetMarkerStyle(21);
  gr2->SetMarkerStyle(22);
  gr3->SetMarkerStyle(23);

  gr2->GetXaxis()->SetTitle("distance (cm)");
  gr2->GetYaxis()->SetTitle("FWHM (keV)");
  gr2->SetTitle("FWHM vs distance");
  gr2->Draw("ALP");
  gr1->Draw("LP");
  gr3->Draw("LP");

  leg = new TLegend(0.6,0.7,1.0,1.0);
  char legtitle[100];
  leg->AddEntry(gr1,lab0,"lp");
  leg->AddEntry(gr2,lab1,"lp");
  leg->AddEntry(gr3,lab2,"lp");
  leg->Draw();
}


void drawPhotoPeakEffvsThetaFinalGeo()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);

  //  const int ndata = 11;
  const int ndata = 10;
  //  const float d[ndata] = {30,40,50,60,70,80,90,100,110,120,130};
  const float d[ndata] = {30,40,50,60,70,80,90,100,110,120};
  // Agantav7
  float pheff[ndata] = {0.6,2.2,2.85,2.14,0.85,0.60,1.28,1.45,1.22,0.69};
//   float resol[ndata] = {5.93,7.52,9.15,9.51,8.98,12.41,12.59,11.12,11.23,8.84};
//   float resol2[ndata] = {3.2,3.7,4.5,4.5,4.3,5.8,5.9,5.1,5.0,4.5};

  // Agantav6
  //float pheff[ndata] = {1.5,3.0,3.2,3.0,2.6,2.0,2.1,1.8,1.1,0.25};
  float resol[ndata] = {6.1,8.6,13,16.96,17.97,18.46,18.13,15.5,15.6,13.18};
  float resol2[ndata] = {3.2,4.3,6.1,7.6,7.9,8.1,7.5,7.0,7.4,6.5};


  TGraph *gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("#theta (deg)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs #theta d = 10 cm");
  gr->Draw("ALP");


  TCanvas *cg2 = new TCanvas("cg2","",1);
  cg2->SetFillColor(10);

  for(int i=1;i<ndata;i++)
    pheff[i] += pheff[i-1];
  gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("#theta (deg)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs #theta d = 10 cm");
  gr->Draw("ALP");

  TCanvas *cg2r = new TCanvas("cg2r","",1);
  cg2r->SetFillColor(10);


  gr = new TGraph(ndata,d,resol);
  gr2 = new TGraph(ndata,d,resol2);
  gr->SetMarkerStyle(21);
  gr2->SetMarkerStyle(25);
  gr->GetXaxis()->SetTitle("#theta (deg)");
  gr->GetYaxis()->SetTitle("FWHM (keV)");
  gr->SetTitle("Resolution vs #theta d = 10 cm");
  gr->Draw("ALP");
  gr2->SetLineStyle(2);
  gr2->Draw("LP");

 leg = new TLegend(0.6,0.7,1.0,1.0);
 char legtitle[100];
 leg->AddEntry(gr,"#Delta r = 5 mm","lp");
 leg->AddEntry(gr2,"#Delta r = 2 mm","lp");
 
 leg->Draw();
 
}




void drawPhotoPeakEffvsThetaFinalGeo_HTarget()
{
  TCanvas *cg = new TCanvas("cg","",1);
  cg->SetFillColor(10);

  const int ndata = 5;
  float d[ndata] = {0,5.0,10.0,15.0,20.0}; // agatav2
  for(int i=0;i<ndata;i++)
    d[i] = 23.5 - d[i];

  float d2[ndata] = {-10,-5,0,5,10}; // agantav7
  for(int i=0;i<ndata;i++)
    d2[i] = 10 + d2[i];
  // Agatav2
  float pheff[ndata] = {8.3,9.2,9.1,8.5,6.7};
  float pheff2[ndata] = {13.5,14.9,15.0,14.7,12.7};
  float resol[ndata] = {40.6,41.6,37.4,34.1,33.5};
  float resol2[ndata] = {51.9,52.4,50.5,53.5,54.1};
  //  float resol2[ndata] = {3.2,4.3,6.1,7.6,7.9,8.1,7.5,7.0,7.4,6.5};


  TGraph *gr = new TGraph(ndata,d,pheff);
  gr->SetMarkerStyle(25);
  gr->GetXaxis()->SetTitle("distance (cm)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs distance");
  gr->Draw("ALP");

  gr = new TGraph(ndata,d2,pheff2);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("distance (cm)");
  gr->GetYaxis()->SetTitle("Efficiency (%)");
  gr->SetTitle("Photopeak Efficiency vs distance");
  gr->Draw("LP");

  TCanvas *cg2 = new TCanvas("cg2","",1);
  cg2->SetFillColor(10);

  gr = new TGraph(ndata,d,resol);
  gr->SetMarkerStyle(25);
  gr->GetXaxis()->SetTitle("distance (cm)");
  gr->GetYaxis()->SetTitle("FWHM (keV)");
  gr->SetTitle("Resolution vs distance");
  gr->Draw("ALP");

  gr = new TGraph(ndata,d2,resol2);
  gr->SetMarkerStyle(21);
  gr->GetXaxis()->SetTitle("distance (cm)");
  gr->GetYaxis()->SetTitle("FWHM (keV)");
  gr->SetTitle("Resolution vs distance");
  gr->Draw("LP");

}

void savePHRootFile(TH1F *h,string rootfilename)
{
  TFile f(rootfilename.c_str(),"new");
  h->Write();
  cout << " ROOT File " << rootfilename << " with doppler-corrected spectrum saved."  << endl;
}




int repairfile(void)
{
  int firstvalue = 0;
  int my_ev_cr = 1;

  FILE *fin = fopen("../SimulationResults/Builder/50fe/g50kEvts50fe150meV_S2ADCA_bkgMg500.txt","r");
  FILE *fout = fopen("../SimulationResults/Builder/50fe/g50kEvts50fe150meV_S2ADCA_bkgMg500.txt.rep","w");

  char temp[500];

  fgets(temp , 500 , fin);
  fprintf(fout,"%s",temp);

  while((strstr(temp,"$")==0))  
    {
      fgets (temp , 500 , fin);
      fprintf(fout,"%s",temp);
      //      cout << " DEBUG " << temp << endl;
    }

  while(!feof(fin))
    {
      fgets(temp , 500 , fin);
      
      sscanf(temp,"%d",&firstvalue);
      
      if(firstvalue == -100)
	{
	  //do nothing
	}
      else if(firstvalue == -101)
	{
	  fprintf(fout,"-100   %d\n",my_ev_cr);
	  fprintf(fout,temp);
	  my_ev_cr++;
	}
      else
	fprintf(fout,temp);
    }

  return my_ev_cr;
}


int PlotRootSpectra()
{
  gStyle->SetOptStat(0000);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  int reb_factor = 1;
  TCanvas *c = new TCanvas("c","",1);
  c->SetFillColor(10);
  TFile f1("../SimulationResults/Reconstructor/74ni/DopplerCorrectedSpectrum0.1ps.root");
  TH1F *h1 = (TH1F*)f1.Get("hEdepdc[3]");
  h1->Rebin(reb_factor);
  h1->DrawCopy();

  TFile f2("../SimulationResults/Reconstructor/74ni/DopplerCorrectedSpectrum1ps.root");
  TH1F *h2 = (TH1F*)f2.Get("hEdepdc[3]");
  h2->SetLineColor(2);
  h2->Rebin(reb_factor);
  h2->SetLineColor(2);
  h2->DrawCopy("same");

  TFile f3("../SimulationResults/Reconstructor/74ni/DopplerCorrectedSpectrum5ps.root");
  TH1F *h3 = (TH1F*)f3.Get("hEdepdc[3]");
  h3->SetLineColor(2);
  h3->Rebin(reb_factor);
  h3->SetLineColor(4);
  h3->DrawCopy("same");

 //  TH1F *hd1 = new TH1F("hd1","",1000,0,1);
//   hd1->SetLineColor(1);
//   TH1F *hd2 = new TH1F("hd2","",1000,0,1);
//   hd2->SetLineColor(2);
//  TH1F *hd3 = new TH1F("hd3","",1000,0,1);
//   hd3->SetLineColor(4);
//   leg = new TLegend(0.6,0.7,1.0,1.0);
//   leg->AddEntry(hd1,"#tau = 0.1 ps","l");
//   leg->AddEntry(hd2,"#tau = 1 ps","l");
//   leg->AddEntry(hd3,"#tau = 5 ps","l");
  //  leg->Draw();

  return 0;
}
