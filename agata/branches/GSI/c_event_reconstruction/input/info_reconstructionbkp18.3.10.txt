#AGATA Output Ascii File
#../SimulationResults/Builder/50fe/g50kEvts50fe150meV_S2ADCA_d0cmNomDist.txt
#../SimulationResults/Builder/50fe/g500kEvts50fe150meV_S2ADCA_d10cmfar.txt
#../SimulationResults/Builder/50fe/g500kEvts50fe150meV_S2ADCA.txt
#../SimulationResults/Builder/50fe/g500kEvts50fe150meV_S2_d0cmNomDist.txt
#../SimulationResults/Builder/50fe/g500kEvts50fe150meV_S2_d-10cm.txt
#../SimulationResults/Builder/50fe/g500kEvts50fe150meV_S2_d10cm.txt
#../SimulationResults/Builder/50fe/g50kEvts50fe150meV_S2ADCA.txt
#../SimulationResults/Builder/50fe/g50kEvts50fe150meV_S2ADCA_bkgMg500.txt.rep
#../SimulationResults/Builder/50fe/g50kEvts50fe150meV.txt
#../SimulationResults/Builder/74ni/g50kEvts5ps500mgFe.txt
#../SimulationResults/Builder/74ni/g50kEvts74ni165meV_S2_d0cm_5ps.txt
../SimulationResults/Builder/S2pPerformance/Eg1MeVBeta0.43dta0mmS2p.txt
# Number of Simulated Events or Events to process
100000
#FWHM Spatial Resolution of the AGATA detectors (mm)
5
#FWHM Spatial Resolution of DSSSD (mm)
0.7
#Time Resolution for fragment detection (LYCCA) in (ps)
80
#visualization lower Energy Range for HPGe Pulse Height Spectrum (eV)
0
#visualization high Energy Range for HPGe Pulse Height Spectrum (eV)
2000
# flag reconstruction (always 3)
3
# flag active target (0 no / 1 yes) flag_activetarget
1
# flag plot (0 no plot output, 1 several analysis plots out) flag_plot
1
# flag plot Energy Resolution of HPGe used (flag_plot_Eresol)
1
# rebin factor for HPGe Raw and Doppler Pulse Height Spectra (1 for no rebinning)
10
