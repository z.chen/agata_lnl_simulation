#include <iostream>

class Event
{
public:
  //  Event(){this->clean();}
  Event(){n_int=0; int_index = 0;flag_badevent = 0;ndetectorshitted=0;}
  ~Event();

  // set methods
  void seteventnumber(int eventnumber){ev_number = eventnumber;}
  void setfragmentbeta(float betafragment){frag_beta = betafragment;}
  void setegamma(float energy){g_energy = energy;}
  void setgraydirection(float x, float y, float z){g_dx = x; g_dy=y; g_dz=z;}
  void setgrayorigin(float x, float y, float z){g_x = x; g_y=y; g_z=z;}
  void setintflag(bool flag){interaction_flag = flag;}
  void increasendethitted(void){ndetectorshitted++;}
  void increase_nint(void){n_int++;}
  void increase_int_index(void){int_index++;}
  void set_edepint(float edep){edep_int[int_index] = edep;}
  void set_edepintc(float edep){edep_intc[int_index] = edep;}
  void setintcoor(float x, float y, float z){int_x[int_index] = x; int_y[int_index]=y; int_z[int_index]=z;}
  void setintcoorc(float x, float y, float z){int_xc[int_index] = x; int_yc[int_index]=y; int_zc[int_index]=z;}
  void increase_edep(float edep){total_edep += edep;}
  //  void increase_edep(float edep, float edepc){total_edep += edep; total_edepc += edepc;}
  void increase_edep(float edep, float totedepc){total_edep += edep; total_edepc += totedepc;}
  void set_edepc(float totedepc){total_edepc = totedepc;}
  void increase_accedepint(void){accedep_int[int_index] = total_edep;}
  void clean(void);
  void increase_ev_nr(void){ev_number++;}
  void setxint(float x){int_x[int_index] = x;}
  void setyint(float y){int_y[int_index] = y;}
  void setzint(float z){int_z[int_index] = z;}
  void setclnhit(int cln){cln_hit[int_index] = cln;}
  void setdetnhit(int detn){detn_hit[int_index] = detn;}
  void setnseghit(int nseg){nseg_hit[int_index] = nseg;}
  void sethitlargestedep(int index){hit_largestEdep_i = index;}
  void setfirsthit(int index){hit_first = index;}
  void settimeint(float time){time_int[int_index] = time;}
  void resetnint(void){n_int = 1;int_index=0;}
  void flagbadevent(void){flag_badevent = 1;}

  virtual void setposxyz_det(int det_i,float posx,float posy,float posz)
  {
    detpos_x[det_i] = posx;
    detpos_y[det_i] = posy;
    detpos_z[det_i] = posz;
  }

  virtual void setposxyz_cl(int cl_i,float posx,float posy,float posz)
  {
    clpos_x[cl_i] = posx;
    clpos_y[cl_i] = posy;
    clpos_z[cl_i] = posz;
  }

  virtual void setposxyz_crincl(int cr_i,float posx,float posy,float posz)
  {
    crinclpos_x[cr_i] = posx;
    crinclpos_y[cr_i] = posy;
    crinclpos_z[cr_i] = posz;
  }

  virtual void setcrystalclindex(int crystal_nr,int cl_index)
  {
    crystalclindex[crystal_nr] = cl_index;
  }

  //access methods
  float getedep(int interaction){return edep_int[interaction];}
  float getedepc(int interaction){return edep_intc[interaction];}
  float getfragmentbeta(void){return frag_beta;}
  float getaccedep(int interaction){return accedep_int[interaction];}
  float gettotedep(void){return total_edep;}
  float gettotedepc(void){return total_edepc;}
  float getegamma(void){return g_energy;}
  bool getintflag(void){return interaction_flag;}
  float getgxdir(void){return g_dx;}
  float getgydir(void){return g_dy;}
  float getgzdir(void){return g_dz;}
  float getgsourcex(void){return g_x;}
  float getgsourcey(void){return g_y;}
  float getgsourcez(void){return g_z;}
  float getxint(int intn){return int_x[intn];}
  float getyint(int intn){return int_y[intn];}
  float getzint(int intn){return int_z[intn];}
  float getxintc(int intn){return int_xc[intn];}
  float getyintc(int intn){return int_yc[intn];}
  float getzintc(int intn){return int_zc[intn];}
  int getndetectorshitted(void){return ndetectorshitted;}
  int getninteractions(void){return n_int;}

  int getdetnhit(int int_index){return detn_hit[int_index];}
  int getclnhit(int int_index){return cln_hit[int_index];}
  int getnseghit(int int_index){return nseg_hit[int_index];}

  virtual float getposx_det(int det_i){return detpos_x[det_i];}
  virtual float getposy_det(int det_i){return detpos_y[det_i];}
  virtual float getposz_det(int det_i){return detpos_z[det_i];}
  virtual float getposx_cl(int det_i){return clpos_x[det_i];}
  virtual float getposy_cl(int det_i){return clpos_y[det_i];}
  virtual float getposz_cl(int det_i){return clpos_z[det_i];}
  virtual float getposx_crincl(int det_i){return crinclpos_x[det_i];}
  virtual float getposy_crincl(int det_i){return crinclpos_y[det_i];}
  virtual float getposz_crincl(int det_i){return crinclpos_z[det_i];}

  virtual float getthresholdEdep(){return threshold_Edep;}
  int gethitlargestedep(void){return hit_largestEdep_i;}
  int getfirsthit(void){return hit_first;}
  float gettimeint(int intn){return time_int[intn];}
  int getintindex(void){return int_index;}
  int getcrystalclindex(int crystal_nr){return crystalclindex[crystal_nr];}
  bool getflagbadevent(void){return flag_badevent;}

  static TH1F *hfwhmx;
  static TH1F *hfwhmy;
  static TH1F *hfwhmz;
  static TH1F *hfwhme;

  static TH2F *hgxy;
  static TH1F *hgx;
  static TH1F *hgy;
  static TH1F *hgz;

private:
  int ev_number;
  float g_energy;

  float frag_beta;
  
  float g_dx;
  float g_dy;
  float g_dz;

  float g_x;
  float g_y;
  float g_z;

  bool interaction_flag;
  int n_int;
  int int_index;
  int det_int[100];

  float edep_int[100];
  float edep_intc[100];

  float accedep_int[100];
  float time_int[100];
  float int_x[100];
  float int_y[100];
  float int_z[100];
  float int_xc[100];
  float int_yc[100];
  float int_zc[100];

  int detn_hit[100];
  int cln_hit[100];
  int nseg_hit[100];

  static float detpos_x[200];
  static float detpos_y[200];
  static float detpos_z[200];
  static float clpos_x[60];
  static float clpos_y[60];
  static float clpos_z[60];
  static float crinclpos_x[3];
  static float crinclpos_y[3];
  static float crinclpos_z[3];
  static float threshold_Edep;
  static int crystalclindex[200];

  float total_edep;
  float total_edepc;

  int hit_largestEdep_i;
  int hit_first;
  bool flag_badevent;
  int ndetectorshitted;
};


Event::~Event()
{
}

void Event::clean()
{
  flag_badevent = 0;

  n_int = 0;

  total_edep = 0;

  ev_number = 0;
  g_energy = 0;
  g_x = 0;
  g_y = 0;
  g_z = 0;
  interaction_flag = 0;
  
  det_int[0] = {0};
  /*
  edep_int[0] = {0};
  int_x[0] = {0};
  int_y[0] = {0};
  int_z[0] = {0};
  */
  total_edep = 0;
  hit_largestEdep_i = 0;
  hit_first = 0;
}
