#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include "/usr/include/sys/types.h"
#include "/usr/include/sys/stat.h"
#include <unistd.h>




#define PI 3.1415927
#define rho_ge 5.32 /* g/cm3 */
#define Z_ge 32 
#define A_ge 74
#define N_av 6.022e23
#define r_0 2.8179e-15
#define mec2 0.511 /* electron mass MeV */
#define SQ(x) ((x)*(x))
#define CB(x) ((x)*(x)*(x))
#define alpha 0.0072993 /* fine structure constant 1/137 */
#define voverc 0. /* velocity of source */
#define betafactor sqrt(1-SQ(voverc))
main()
{
  
  char fn[50],buffer[200];
  FILE *fp,*fp1,*fp2,*fp3;
  int counter;
  int i,ir,iff;
  int j,jr;
  int n;
  int k,kmax,nbinter;
  int l,m,o,p,q,qq;
  int itot,nbrtotgam,pairprod,npair,readpair;
  int nintu,flagerr,nsingint,numbsingrecon,nbgoodpair;
  double u2,ddu,ee1u,facu,erfcu[1000];
  double etot[200000],probtot[200000],sigma_thet,effphot[30],peak_total[30],power;
  double angn[800],alfamax,minprobtrack,minprobsing,yfl;
  int sn[800][800],numn[800],order[800],ordern[800];
  double angth,angph,etotalen,etotale,escatter,escattern,deltaesc,deltaescn;
  int flagu[800],flagpair[800],flagpack;
  int itotcurr,totnumbofgammas,nseg[800],ndet[800];
  long int spec[10000];
  double rmin,r[800][800],r_ge[800][800],costheta;
  double costest,angtheta[800],angphi[800],alfa;
  double Ax,Ay,Az,Bx,By,Bz,vectproduct,r_center,r_vacuum,ri_orig,r_vacuumi,packenergy;
  double mincosdif,costest1,probtest,probtest1;
  double probtest3,probtest2,probtest4,photodistmax;
  double e[800],x[800],y[800],z[800],et[800],probcomp;
  double cross1,cross2,en[800],xn[800],yn[800],zn[800];
  double d_res,ri,rj,lambda2;
  double cross, lambda;
  int nb_int,numinter[800],mult,readflag,nshoot,numintern[800],nrj,phot[30];
  int nintprev,nprev,icomp[500],icompseg[36],iseg[800];
  int number_of_unflagged;
  double sigma_pos,energygam[30],totenergy,energythresh,lambda1;
  int nphoto4pi[30],nodirect,temp1[800],interaction[800][800];
  double coef1,coef2,ercos,eres;
  double deltaalfa,thetafirst[800],radius,radius_out;
  /* inner and outer radius now read from file */
  int source_type, source_movement,dummy_i,flagseg,imult;
  double  xsource, ysource, zsource;
  char dummy_str[20];
 char first_char='$';
int zero = 0L;
 int ntot=0;
  double sig_compt(double E);
  /* total compton cross section, cm2/atom , E en MeV */
   double sig_abs(double E); 
  /* total photoelectric cross section , cm2/atom, E en MeV */
  double range_process(double sig); 
  /* interaction length in cm = 1/SIGMA_macro */
  double proba(double range, double distance);
  /* probability of interaction at a given distance given a certain range */
  void swap(double v[],int m, int l);
  /* pair production cross section */
 void swapi(int v[],int m, int l);
  /* pair production cross section */
  double sig_pair(double E); 
 /* error in cosine */
  double err_cos(double x, double xp, double xpp, double y, double yp, double ypp, double z, double zp, double zpp, double r, double rp, double err);
  int packpoints(int number, double posx[], double posy[], double posz[], double energy[], int intnb[], int intorder[],int nseg[], int ndet[], double resolution);
  void smearpoints(int number, double energy[], double posx[], double posy[], double posz[],double errorfc[],double step, int nbsteps, double uncertainty); 
   
  
  /*********************************************************************/
  /**** energies in MeV and positions in cm ****************************/
  /*********************************************************************/ 
 
 
  /********************************************************************/
  /**************** user parameter initialisation *********************/
  /********************************************************************/

  


  mult= 1; /* required event multiplicity */
  totnumbofgammas = 10000; /* number of emitted gammas to treat */



 
  /********************************************************************/
  /**************** parameter initialisation **************************/
  /********************************************************************/




  yfl=-0.5;
  nprev = 0;
  nintprev = 0;
  readflag = 0;
  pairprod=0;
  npair=0;
  nbgoodpair=0;
  readpair=0;
 
    for (j=0;j<500;j++){
    if(j<36)icompseg[j]=0;
    if(j<30){
      phot[j]=0;
      nphoto4pi[j]=0;
      
    }
    icomp[j]=0;

  }
  
  
  k=0;
  n=0;
  nodirect = 0; /* if set to 1 do not check for direct photopics */
  itot = 0; /* number of reconstructed gamma-rays */

  nbrtotgam = 0; /* number of incident gamma-rays */
  nshoot = 0; /* number of events with given multiplicity */
  
  nintu=1000; /* initialisation for random number vs uncertainty procedure */
  ddu=0.005; /* step in random number vs uncertainty procedure */
  ee1u=0; /* initial value for random number vs uncertainty procedure */
  
  photodistmax = 4. ; /*min distance in cm between 2 points to process single int. point */
 
  flagseg = 0; /* if on pack all interactions in same segment */
  flagerr =1 ; /* if set to one, x,y and z smeared by gaussian of sigma_pos */
  flagpack =1; /* if set to one, pack points within d_res of each other */
    
  sigma_pos=0.1 ; /* width in mm of gaussian position smearing FWHM= 2.35mm*/
  sigma_thet = 0.25; /* position uncertainty in mm for costheta */
 
  d_res=0.5; /* min distance between points below which they are smeared*/

 
  energythresh = 0.005; /* energy threshold */
  eres = 3e-3; /* energy resolution */
  
  numbsingrecon=0; /* number of single hit photopeak cluster reconstucted */
  nsingint=0; /* total number of single hit clusters */
  nbinter = 0; /* number of interactions per event */
  minprobtrack = 0.05; /* threshold for track acceptance */
  minprobsing = 0.15; /* threshold for single int acceptance */
  alfa = 0.15; // min angular separation in phi and theta in rad
  deltaalfa = 0.1; // step in alpha  
 
  kmax = 10; /* max number of interactions for a given incident gamma-ray */


  /**********************************************************/
  /* mapping random number vs uncertainty in units of sigma */
  /**********************************************************/
  

  u2=ddu;
  
  for(j=1;j<=nintu;j++) {   
    ee1u=ee1u+exp(-(u2*u2/2.));   
    erfcu[j]=ee1u;
    u2=u2+ddu;
  }
  
  facu=ee1u;
  for(j=1;j<=nintu;j++) {
    erfcu[j]=erfcu[j]/facu; /* random => ddu * j  in units of sigma_pos */  
  }
  
  /*********************************************************************/
  /* reading in source position and interaction positions and energies */
  /* and opening files for spectra- performances - detailed output *****/
  /*********************************************************************/
  

  sprintf(fn,"spectrum");
  fp1=fopen(fn,"w");
  
  sprintf(fn,"perf");
  fp2=fopen(fn,"w");
  
  sprintf(fn,"fullout");
  fp3=fopen(fn,"w");

  sprintf(fn,"../../../GammaEvents.0000");
  fp = fopen(fn,"r");
  if(!fp)
    {
      printf("could not open file %s\n",fn);
    }
  else
    {
      printf("opened file %s\n",fn);
    }
  
  
  counter=0;
  
  
  /******************************************************************/
  /*********** read buffer at the beginning of file *****************/
  /******************************************************************/
  
  while(1) {
    memset(buffer,zero,sizeof(buffer));
    fgets(buffer,150,fp);
    if (strncmp(buffer,"GAMMA",5)==0) {
      sscanf(buffer,"%s %d \n",&fn,&imult);
      printf("mult = %d \n",imult);
      memset(buffer,zero,sizeof(buffer));
            for(j=0;j<imult;j++){
           fgets(buffer,150,fp);
           sscanf(buffer,"%lf", &energygam[j]);
         energygam[j]=energygam[j]*1e-3;
    
        printf("energygam = %f \n",energygam[j]);
       }
    }
    if (strncmp(buffer,"SHELL",5)==0) {
      memset(buffer,zero,sizeof(buffer));
      fgets(buffer,150,fp);
      sscanf(buffer,"%lf %lf", &radius,&radius_out);
      radius=radius/10; // mm to cm
      radius_out=radius_out/10;
      
      printf("radius inner = %f outer = %f \n",radius,radius_out);
    }
    if (strncmp(buffer,"SUMMARY",7)==0) {
      
      sscanf(buffer,"%s %lf %lf %d %d %d %d %d %d %d %d ",&fn,&radius,&radius_out,&dummy_i,&dummy_i,&dummy_i,&dummy_i,&dummy_i,&dummy_i,&dummy_i,&dummy_i);
       radius=radius/10; // mm to cm
      radius_out=radius_out/10;
      printf("radius inner = %f outer = %f \n",radius,radius_out);
    }

     if (strncmp(buffer,"SOURCE",6)==0) {
      sscanf(buffer,"%s %d %d %lf %lf %lf", 
	     &dummy_str, 
	     &source_type, &source_movement,
	     &xsource, &ysource, &zsource);
	xsource=xsource*0.1;
	ysource=ysource*0.1;
	zsource=zsource*0.1;

	//	printf("%s %d and %d\n"1dummy_str,source_type,source_movement );

      if (!(source_type==0 && source_movement ==0)) {
	printf("Error: bad source data\n");
        return(-1);
      }
      printf(" Source position x, y, z (cm): %f %f %f \n ",
	     xsource, ysource, zsource);
    }

    if (strncmp(buffer,&first_char,1)==0) break;
  }
  
  

 readfile: ; // beginning of an event


  

  
  /************************************************************************/
  /**************** read interaction points and energies  **************/
  /***********************************************************************/
  
 
  
 
  
  if(itot>0){
    icomp[itot-itotcurr]++;  // reconstructed multiplicity counter    
  }
  
  ir=0;
  nb_int=0;
  alfa =0.15;
  i=0;
  
  
  while(1) { 
    
    
  readread: // next interaction point 
    

    if(nintprev > totnumbofgammas) { // maximum nb of gamma-rays treated
      readflag =1;
 
      goto end;
    }
    
    
    if(counter > mult)break;
    
    jr=fscanf(fp,"%d %lf %lf %lf %lf %d",&iff,&e[ir],&x[ir],&y[ir],&z[ir],&nprev);
  
    if(jr == -1) {
      readflag =1;
     
    }
    
    if(readflag ==1) goto end;
    
    if(iff == -1) {    
      nintprev++;
      counter++;
      i=0;
      goto readread;      
    }
   
    numinter[ir]=nintprev;
    order[ir] = i;
    i++;
    nseg[ir]=nprev;
    ndet[ir]=iff;
    e[ir]=1e-3*e[ir];
    x[ir]=x[ir]/10; // mm to cm
    y[ir]=y[ir]/10;
    z[ir]=z[ir]/10;
 
    nb_int++;
    ir++;
    
  }
  
  
  if(nb_int != 0)nshoot++;
 
  nbrtotgam = nbrtotgam+mult;
  itotcurr=itot;
  
  if((nintprev)%1000 ==0){
    printf("%d events \n",nintprev);
    system("date");
  }
  
  /****************************************************/
  /************ count photopics  **********************/
  /*********** works for mult =1 **********************/
  /****************************************************/
  
  totenergy = 0;
  
  
  for (i=0;i<nb_int;i++){
  
    totenergy = totenergy + e[i];
  
  }
  for(i=0;i<imult;i++) {
  if(fabs(totenergy - energygam[i]) <= 3e-3) {
    nphoto4pi[i]++;  
  }
  }  
 
  
  
  /****************************************************/
  /******* pack points in same segment  ****************/
  /****************************************************/


  if(flagseg==1){

  for(i=0;i<nb_int;i++) {

    for(j=i+1;j<nb_int;j++){
      if(e[i]!=0 && ndet[i]==ndet[j] && nseg[i]==nseg[j]){

	en[i]=e[i]+e[j];
	x[i]=((e[i]*x[i])+(e[j]*x[j]))/en[i];
	y[i]=((e[i]*y[i])+(e[j]*y[j]))/en[i];
	z[i]=((e[i]*z[i])+(e[j]*z[j]))/en[i];

	e[j]=0;

	e[i]=en[i];
	
      }

    }

  }


  j=0;
  for(i=0;i<nb_int;i++){
    if(e[i]!=0){
      e[j]=e[i];
      x[j]=x[i];
      y[j]=y[i];
      z[j]=z[i];
      numinter[j]=numinter[i];
      nseg[j]=nseg[i];
      ndet[j]=ndet[i];
      order[j]=order[i];
      j++;
    }

  }
  nb_int = j;
  }

 /****************************************************/
  /******* pack points with r < d_res  ****************/
  /****************************************************/

  if(flagpack == 1 && flagseg!=1) {
    
    nb_int=packpoints(nb_int, x, y, z, e,numinter,order,nseg,ndet,d_res);
    
  }
  
  
  packenergy = 0;
  
  for(j=0;j<nb_int;j++){
   
    packenergy=packenergy+e[j];
 
  }
  
  
  if(fabs(packenergy -totenergy) > 1e-4) printf("pb with energy after packing %f %f \n",packenergy,totenergy);
  
  
 
 for(i=0;i<nb_int;i++){
  
    flagu[i]=0;
  }
  for(i=0;i<nb_int;i++){
    iseg[i]=1;
    for(j=i+1;j<nb_int;j++){
      if(flagu[i]==0 && ndet[i]==ndet[j]){
	if(nseg[i]==nseg[j]){
	 
	  iseg[i]++;

	  flagu[j]=1;
	}
      }

    }
    if(flagu[i]==0){
      icompseg[iseg[i]]++;
     
    }
  }
  
  /*******************************************************/
  /********* smear positions *****************************/
  /*******************************************************/
  
  
  if(flagerr == 1) { /* if flagerr ==0 , no smearing */    
    smearpoints(nb_int, e, x, y, z, erfcu, ddu, nintu,sigma_pos);
  }
 
  //  printf("number of hits for event =%d \n",nb_int);
  /*****************************************************/
  /*********** Apply Energy threshold ******************/
  /*****************************************************/
  
    j=0;
  
    for (i=0; i< nb_int; i++) {
    
    if(e[i] > energythresh) {
      
      en[j] = e[i];
      xn[j] = x[i];
      yn[j] = y[i];
      zn[j] = z[i];
      numintern[j]=numinter[i];
      ordern[j]=order[i];
      j++;
      
    }
  }
  
  nb_int = j;
  totenergy=0;
  
  for (i=0;i<nb_int;i++) {
    
    e[i] = en[i];
    x[i] = xn[i];
    y[i] = yn[i];
    z[i] = zn[i];
    totenergy=totenergy+e[i];
    numinter[i]=numintern[i];
    order[i]=ordern[i];
    
  }
  
 

  for(i=0;i<imult;i++) {
    if(nb_int == 1 && fabs(totenergy- energygam[i])<=3e-3)nsingint++;  
  }
  
  /*****************************************************/
  /******** compute distance source-int and ************/
  /*************theta and phi for each int *************/
  /*****************************************************/
  

  
  for (i=0; i< nb_int; i++) {
  
 
    r[i][i] = sqrt(SQ(x[i]-xsource)+SQ(y[i]-ysource)+SQ(z[i]-zsource));
    angtheta[i] = acos((z[i]-zsource)/r[i][i]);
    angphi[i] = atan2((y[i]-ysource),(x[i]-xsource));
  
    if(angphi[i] <0)angphi[i]=2*PI+angphi[i];

  }
  
  /*****************************************************/
  /********* sort according to increasing theta*********/
  /*****************************************************/
  
  
  
  for (i=0; i< nb_int;i++)
    {
      for (j=i+1;j<nb_int; j++)
	{	  	  	  
	  if(angtheta[j] < angtheta[i])
	    {
	      swap(e, i, j);
	      swap(x, i, j);
	      swap(y, i, j);
	      swap(z, i, j);
	      swapi(numinter,i,j);
	      swapi(order,i,j);
	      swap(angtheta, i, j);
	      swap(angphi, i, j);
	    }	  
	}      
    }
  
  
  for(i=0;i<nb_int;i++){

  
  }
  
  /*****************************************************************/
  /******** computing all distances between interactions   *********/
  /*****************************************************************/
  
  
  
  for (i=0;i<nb_int;i++) {
    r[i][i] = sqrt(SQ(x[i]-xsource)+SQ(y[i]-ysource)+SQ(z[i]-zsource));
    
    ri_orig=sqrt(SQ(x[i])+SQ(y[i])+SQ(z[i])); //distance from geometrical point to i
    
    Ax = -x[i]; 
    Ay = -y[i];
    Az = -z[i];
    
    Bx=xsource-x[i];
    By=ysource-y[i];
    Bz=zsource-z[i];
    
    vectproduct = sqrt(SQ((Ay*Bz)-(Az*By))+SQ((Az*Bx)-(Ax*Bz))+SQ((Ax*By)-(Ay*Bx)));
    
    /* distance from center to line joining points : rcenter = sintheta * distance center-i */
    
    r_center = vectproduct/r[i][i]; 
    
    r_ge[i][i]=r[i][i];
    
    if(r_center < radius) {
      r_vacuum=sqrt(SQ(radius)-SQ(r_center)); // distance from perpendicular intersection to i-source
      // from origin to inner surface of shell
      
      r_vacuumi=sqrt(SQ(ri_orig)-SQ(r_center)); // distance from perpendicular intersction to i-source
      //from origin to i
      
      r_ge[i][i]=r_vacuumi-r_vacuum;
      
    }	  
    
    if(r_ge[i][i] < 0)r_ge[i][i]=d_res;
    
    
    for (j=i+1;j<nb_int;j++) {
      
      r[i][j]= sqrt(SQ(x[i]-x[j])+SQ(y[i]-y[j])+SQ(z[i]-z[j]));
      r[j][i]=r[i][j];
      
     
      ri = ri_orig;
      rj = sqrt(SQ(x[j]) + SQ(y[j]) + SQ(z[j]));;
      if (ri > rj) {
		k = j;
		l = i;
      } else {
		k = i;
		l = j;
      }
      
      /********************************************************/
      /***** determine effective length in Ge *****************/
      /********************************************************/	  
      
      
      
      /* vector (interaction point i - geometrical center) */
	Ax = -x[k]; 
	Ay = -y[k];
	Az = -z[k];
	
	
	
	/* vector (interaction point i - interaction point j  */ 	  
	Bx= x[l]-x[k];
	By= y[l]-y[k];
	Bz= z[l]-z[k];
	
	costest = Ax * Bx + Ay * By + Az * Bz;
	costest1 = sqrt(SQ(Ax) + SQ(Ay) + SQ(Az));
	costest = costest / (costest1 * r[i][j]);
	
	
	/* norm of vector product = sintheta * distance center-i * distance i-j */
	vectproduct = sqrt(SQ((Ay*Bz)-(Az*By))+SQ((Az*Bx)-(Ax*Bz))+SQ((Ax*By)-(Ay*Bx)));
	
	/* distance from center to line joining points : rcenter = sintheta * distance center-i */	 
	r_center = vectproduct/r[i][j]; 
	
	r_ge[i][j] = r[i][j];
	
	
	if((r_center < radius)  && (costest > 0)) {
	  /* if < radius, the photon does not go through */
	  /* Ge all the way from j to i */
	  /* right triangle:  r_vacuum^2 + rcenter^2 = radius^2 */
	  /* 2*r_vacuum = distance to take away from rmin */
	  r_vacuum = sqrt(SQ(radius)-SQ(r_center));
	  r_ge[i][j]= r[i][j] - 2*r_vacuum;
	  
	  
	}
	
	if(r_ge[i][j] < 0)r_ge[i][j]=d_res;
	r_ge[j][i]=r_ge[i][j];
    }
  }        
  
  
  
  
  
  /****************************************************/
  /*************** find cluster ***********************/
  /****************************************************/
  
  n=0;

/* calculating maximum opening angle from the number of interaction points */ 
 
  power = pow(((nb_int+2)/3),0.9);
  alfamax=acos(1-2/power);
 findcluster: ;
 
 
  
  for(i=0; i<nb_int;i++) flagu[i]=0;
  
  for(i=0;i<nb_int;i++) {
    rmin=0;
    et[n]=0;
    k=0;
    if(flagu[i] ==0) {
      sn[n][k] = i;
      et[n] = et[n]+e[i];
      angth=angtheta[sn[n][k]];
      
      angph = angphi[sn[n][k]];
      k++;
      flagu[i]=1;
      
      for(j=0;j<nb_int;j++) {
	if(j!=i && flagu[j]==0) {
	  
	  costest1 = (cos(angth)*cos(angtheta[j]));
	  costest1=costest1+((sin(angth)*sin(angtheta[j]))*cos(angphi[j]-angph));
	  costest1=acos(costest1);
	  
	  if((fabs(costest1) <= alfa)) {
	   
	    et[n] = et[n]+e[j];
	    sn[n][k] = j;
	    k++;
	    flagu[j]=1;
	    
	    angth=angtheta[j];  
	    angph=angphi[j];
	   
	  }
	  
	  
	}
	if(k == kmax) break;
	
      }
      numn[n]=k;
      angn[n]=alfa;
      n++;
     
    }
    
    
  }
  ntot=ntot+n;

  /**************************************************/ 
  /*********** loop on alpha values *****************/
  /**************************************************/ 
  
  if(alfa < alfamax) {
    alfa =alfa + deltaalfa;
    goto findcluster;
  }
  
  
  //  printf("%d clusters \n",n);
  /*******************************************************/
  /******** compute figure of merit of clusters **********/
  /*******************************************************/
  
  for(i=0;i<n;i++) {
    etotale = et[i];
    flagu[i]=0;
    flagpair[i]=0;
    
    if(numn[i]==1)goto suite; /* keep single int points for later */
    mincosdif=0;
    
    for(j=0;j<numn[i];j++) {
           
   
      for(l=0;l<numn[i];l++) {

	if(l!=j) {
	 
	  
	  costheta = acos(z[sn[i][j]]/r[sn[i][j]][sn[i][j]]);
	  costheta=(x[sn[i][j]]-xsource)*(x[sn[i][l]]-x[sn[i][j]])+(y[sn[i][j]]-ysource)*(y[sn[i][l]]-y[sn[i][j]])+(z[sn[i][j]]-zsource)*(z[sn[i][l]]-z[sn[i][j]]);
	  costheta= costheta/(r[sn[i][j]][sn[i][j]]*r[sn[i][j]][sn[i][l]]);
	  ercos= err_cos(xsource,x[sn[i][j]],x[sn[i][l]],ysource,y[sn[i][j]],y[sn[i][l]],zsource,z[sn[i][j]],z[sn[i][l]],r[sn[i][j]][sn[i][j]],r[sn[i][j]][sn[i][l]],sigma_thet);
	  
	  escattern = etotale/(1+(etotale/mec2)*(1-costheta));
	  escatter = etotale - e[sn[i][j]];
	  
	  deltaescn = SQ(escattern)*ercos/mec2;
	  deltaesc = sqrt((numn[i]+1)*SQ(eres));
	  
	 
	  probtest=exp(-2.*SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc))); 
	
 
	    if(probtest < 0.00005) goto nextsecond;
	  cross1 = sig_compt(etotale);
	  cross= sig_pair(etotale);
	  coef1 = cross1/(cross1+sig_abs(etotale)+cross);
	  cross2 = sig_compt(escatter);
	  cross=sig_pair(escatter);
	  coef2 = cross2/(cross2+cross+sig_abs(escatter));
	  if(numn[i] == 2) {
	    cross2=sig_abs(escatter);	   
	    coef2= cross2/(cross2+cross+sig_compt(escatter));
	  }
	  lambda1=range_process(cross1);
	  lambda2=range_process(cross2);
	  probtest=probtest*SQ(coef1*proba(lambda1,r_ge[sn[i][j]][sn[i][j]]))*coef2*proba(lambda2,r_ge[sn[i][j]][sn[i][l]]);
	  
	
	  
	  for(m=0;m<numn[i];m++) {
	    if(m!=j && m!=l) {
	      
	      costheta=(x[sn[i][l]]-x[sn[i][j]])*(x[sn[i][m]]-x[sn[i][l]])+(y[sn[i][l]]-y[sn[i][j]])*(y[sn[i][m]]-y[sn[i][l]])+(z[sn[i][l]]-z[sn[i][j]])*(z[sn[i][m]]-z[sn[i][l]]);
	      costheta= costheta/(r[sn[i][j]][sn[i][l]]*r[sn[i][l]][sn[i][m]]);
	      ercos= err_cos(x[sn[i][j]],x[sn[i][l]],x[sn[i][m]],y[sn[i][j]],y[sn[i][l]],y[sn[i][m]],z[sn[i][j]],z[sn[i][l]],z[sn[i][m]],r[sn[i][j]][sn[i][l]],r[sn[i][l]][sn[i][m]],sigma_thet);
	      etotalen=etotale - e[sn[i][j]];
	      escattern = etotalen/(1+(etotalen/mec2)*(1-costheta));
	      escatter = etotalen - e[sn[i][l]];
	      
	      deltaescn = SQ(escattern)*ercos/mec2;
	      deltaesc = sqrt((numn[i]+2)*SQ(eres));
	      
	      probcomp=exp(-SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc)));
 
	        if(probcomp < 0.00005) goto nextthird;
	      cross1 = sig_compt(etotalen);
	      cross=sig_pair(etotalen);
	      coef1 = cross1/(cross1+cross+sig_abs(etotalen));
	      cross2 = sig_compt(escatter);
	      cross=sig_pair(escatter);
	      coef2 = cross2/(cross2+cross+sig_abs(escatter));
	      if(numn[i] == 3) {
		cross2=sig_abs(escatter);
		coef2= cross2/(cross2+cross+sig_compt(escatter));
		
	      }
	      lambda1=range_process(cross1);
	      lambda2=range_process(cross2);
	      
	      probcomp = probtest*probcomp*coef2*proba(lambda2,r_ge[sn[i][l]][sn[i][m]]);
	      
	      
	      for(o=0;o<numn[i];o++) {
		if(o!=j && o!=l && o!=m) {
		  costheta=(x[sn[i][m]]-x[sn[i][l]])*(x[sn[i][o]]-x[sn[i][m]])+(y[sn[i][m]]-y[sn[i][l]])*(y[sn[i][o]]-y[sn[i][m]])+(z[sn[i][m]]-z[sn[i][l]])*(z[sn[i][o]]-z[sn[i][m]]);
		  costheta= costheta/(r[sn[i][l]][sn[i][m]]*r[sn[i][m]][sn[i][o]]);
		  ercos= err_cos(x[sn[i][l]],x[sn[i][m]],x[sn[i][o]],y[sn[i][l]],y[sn[i][m]],y[sn[i][o]],z[sn[i][l]],z[sn[i][m]],z[sn[i][o]],r[sn[i][l]][sn[i][m]],r[sn[i][m]][sn[i][o]],sigma_thet);
		  etotalen=etotale - e[sn[i][j]] - e[sn[i][l]];
		  escattern = etotalen/(1+(etotalen/mec2)*(1-costheta));
		  escatter = etotalen - e[sn[i][m]];
		  
		  deltaescn = SQ(escattern)*ercos/mec2;
		  deltaesc = sqrt((numn[i]+3)*SQ(eres));
		  
		  probtest1=exp(-SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc)));
		
		   if(probtest1 < 0.00005) goto nextfourth;
		  cross1 = sig_compt(etotalen);
		  cross=sig_pair(etotalen);
		  coef1 = cross1/(cross1+cross+sig_abs(etotalen));
		  cross2 = sig_compt(escatter);
		  cross=sig_pair(escatter);
		  coef2 = cross2/(cross2+cross+sig_abs(escatter));
		  if(numn[i] == 4) {
		    cross2=sig_abs(escatter);
		    coef2= cross2/(cross2+cross+sig_compt(escatter));
		    
		  }
		  lambda1=range_process(cross1);
		  lambda2=range_process(cross2);
		  probtest1=probcomp*probtest1*coef2*proba(lambda2,r_ge[sn[i][m]][sn[i][o]]);
		  
		  for(p=0;p<numn[i];p++) {
		    
		    if(p!=o && p!=m && p!=l && p!=j) {
		      
		      costheta=(x[sn[i][o]]-x[sn[i][m]])*(x[sn[i][p]]-x[sn[i][o]])+(y[sn[i][o]]-y[sn[i][m]])*(y[sn[i][p]]-y[sn[i][o]])+(z[sn[i][o]]-z[sn[i][m]])*(z[sn[i][p]]-z[sn[i][o]]);
		      costheta= costheta/(r[sn[i][m]][sn[i][o]]*r[sn[i][o]][sn[i][p]]);
		      ercos= err_cos(x[sn[i][m]],x[sn[i][o]],x[sn[i][p]],y[sn[i][m]],y[sn[i][o]],y[sn[i][p]],z[sn[i][m]],z[sn[i][o]],z[sn[i][p]],r[sn[i][m]][sn[i][o]],r[sn[i][o]][sn[i][p]],sigma_thet);
		      etotalen=etotale - e[sn[i][j]] - e[sn[i][l]] - e[sn[i][m]];
		      escattern = etotalen/(1+(etotalen/mec2)*(1-costheta));
		      escatter = etotalen - e[sn[i][o]];
		      
		      deltaescn = SQ(escattern)*ercos/mec2;
		      
		      deltaesc = sqrt((numn[i]+4)*SQ(eres));
		      
		      probtest2 = exp(-SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc)));
		       if(probtest2 < 0.00005) goto nextfifth;
		      cross1 = sig_compt(etotalen);
		      cross=sig_pair(etotalen);
		      coef1 = cross1/(cross1+cross+sig_abs(etotalen));
		      cross2 = sig_compt(escatter);
		      cross=sig_pair(escatter);
		      coef2 = cross2/(cross2+cross+sig_abs(escatter));
		      if(numn[i] == 5) {
			cross2=sig_abs(escatter);
			coef2= cross2/(cross2+cross+sig_compt(escatter));
			
		      }
		      lambda1=range_process(cross1);
		      lambda2=range_process(cross2);
		      probtest2=probtest1*probtest2*coef2*proba(lambda2,r_ge[sn[i][o]][sn[i][p]]);
		      
		      
		      for(q=0;q<numn[i];q++) {
			if(q!=j && q!=l && q!=m && q!=o && q!=p ) {
			  
			  costheta=(x[sn[i][p]]-x[sn[i][o]])*(x[sn[i][q]]-x[sn[i][p]])+(y[sn[i][p]]-y[sn[i][o]])*(y[sn[i][q]]-y[sn[i][p]])+(z[sn[i][p]]-z[sn[i][o]])*(z[sn[i][q]]-z[sn[i][p]]);
			  costheta= costheta/(r[sn[i][o]][sn[i][p]]*r[sn[i][p]][sn[i][q]]);
			  ercos= err_cos(x[sn[i][o]],x[sn[i][p]],x[sn[i][q]],y[sn[i][o]],y[sn[i][p]],y[sn[i][q]],z[sn[i][o]],z[sn[i][p]],z[sn[i][q]],r[sn[i][o]][sn[i][p]],r[sn[i][p]][sn[i][q]],sigma_thet);
			  etotalen=etotale - e[sn[i][j]] - e[sn[i][l]] - e[sn[i][m]] - e[sn[i][o]];
			  escattern = etotalen/(1+(etotalen/mec2)*(1-costheta));
			  escatter = etotalen - e[sn[i][p]];
			  
			  deltaescn = SQ(escattern)*ercos/mec2;
			  
			  deltaesc = sqrt((numn[i]+5)*SQ(eres));
			  
			  probtest3=exp(-SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc)));
			  
			  cross1 = sig_compt(etotalen);
			  cross=sig_pair(etotalen);
			  coef1 = cross1/(cross1+cross+sig_abs(etotalen));
			  cross2 = sig_compt(escatter);
			  cross=sig_pair(escatter);
			  coef2 = cross2/(cross2+cross+sig_abs(escatter));
			  if(numn[i] == 6) {
			    cross2=sig_abs(escatter);
			    coef2= cross2/(cross2+cross+sig_compt(escatter));
			    
			  }
			  lambda1=range_process(cross1);
			  lambda2=range_process(cross2);
			  probtest3=probtest2*probtest3*coef2*proba(lambda2,r_ge[sn[i][p]][sn[i][q]]);
			 
			  
			  for(qq=0;qq<numn[i];qq++) {
			    if(qq!=j && qq!=l && qq!=m && qq!=o && qq!=p && qq != q ) {
			      
			      costheta=(x[sn[i][q]]-x[sn[i][p]])*(x[sn[i][qq]]-x[sn[i][q]])+(y[sn[i][q]]-y[sn[i][p]])*(y[sn[i][qq]]-y[sn[i][q]])+(z[sn[i][q]]-z[sn[i][p]])*(z[sn[i][qq]]-z[sn[i][q]]);
			      costheta= costheta/(r[sn[i][p]][sn[i][q]]*r[sn[i][q]][sn[i][qq]]);
			      ercos= err_cos(x[sn[i][p]],x[sn[i][q]],x[sn[i][qq]],y[sn[i][p]],y[sn[i][q]],y[sn[i][qq]],z[sn[i][p]],z[sn[i][q]],z[sn[i][qq]],r[sn[i][p]][sn[i][q]],r[sn[i][q]][sn[i][qq]],sigma_thet);
			      etotalen=etotale - e[sn[i][j]] - e[sn[i][l]] - e[sn[i][m]] - e[sn[i][o]] -e[sn[i][p]];
			      escattern = etotalen/(1+(etotalen/mec2)*(1-costheta));
			      escatter = etotalen - e[sn[i][q]];
			      
			      deltaescn = SQ(escattern)*ercos/mec2;
			      
			      deltaesc = sqrt((numn[i]+6)*SQ(eres));
			      
			      probtest4=exp(-SQ(escattern-escatter)/(SQ(deltaescn)+SQ(deltaesc)));
			      
			      cross1 = sig_compt(etotalen);
			      cross=sig_pair(etotalen);
			      coef1 = cross1/(cross1+cross+sig_abs(etotalen));
			      cross2 = sig_compt(escatter);
			      cross=sig_pair(escatter);
			      coef2 = cross2/(cross2+cross+sig_abs(escatter));
			      if(numn[i] == 6) {
				cross2=sig_abs(escatter);
				coef2= cross2/(cross2+cross+sig_compt(escatter));
				
			      }
			      lambda1=range_process(cross1);
			      lambda2=range_process(cross2);
			      probtest4=probtest3*probtest4*coef2*proba(lambda2,r_ge[sn[i][q]][sn[i][qq]]);
			      probtest4=pow(probtest4,1/13.);
			      
			      if(probtest4 > mincosdif) {
				mincosdif=probtest4;
				interaction[i][0]=sn[i][j];
				interaction[i][1]=sn[i][l];
				interaction[i][2]=sn[i][m];
				interaction[i][3]=sn[i][o];
				interaction[i][4]=sn[i][p];
				interaction[i][5]=sn[i][q];	
				interaction[i][6]=sn[i][qq];
				
			      }
			      
			    }
			  }
			  
			  
			  if(numn[i]==6) {
			    probtest3=pow(probtest3,1/11.);
			    if(probtest3 > mincosdif) {
			      mincosdif=probtest3;
			      interaction[i][0]=sn[i][j];
			      interaction[i][1]=sn[i][l];
			      interaction[i][2]=sn[i][m];
			      interaction[i][3]=sn[i][o];
			      interaction[i][4]=sn[i][p];
			      interaction[i][5]=sn[i][q];		    
			      
			    }
			    
			  }
			}
			
		      }
		      
		      if(numn[i]==5) {
			
			probtest2=pow(probtest2,1/9.);
			
			if(probtest2 > mincosdif) {
			  mincosdif=probtest2;
			  
			  interaction[i][0]=sn[i][j];
			  interaction[i][1]=sn[i][l];
			  interaction[i][2]=sn[i][m];
			  interaction[i][3]=sn[i][o];
			  interaction[i][4]=sn[i][p];
			  
			  
			}
		      }
		      
		    }
		  nextfifth: ;
		  }
		  
		  if(numn[i]==4){
		    probtest1=pow(probtest1,1/7.);
		    if(probtest1 > mincosdif) {
		      mincosdif = probtest1;
		      
		      interaction[i][0]=sn[i][j];
		      interaction[i][1]=sn[i][l];
		      interaction[i][2]=sn[i][m];
		      interaction[i][3]=sn[i][o];
   		      
		    }
		  }
		}
	      nextfourth:;		
		
	      }
	      if(numn[i]==3) {
		probcomp=pow(probcomp,1/5.);
		if(probcomp > mincosdif) {
		  mincosdif = probcomp;
		  interaction[i][0]=sn[i][j];
		  interaction[i][1]=sn[i][l];
		  interaction[i][2]=sn[i][m];
 		}
		
	      }
	    }
	  nextthird:;
	  }
	  if(numn[i]==2) {
	    probtest=pow(probtest,1/3.);
	    if(probtest > mincosdif) {
	      mincosdif=probtest;
	      interaction[i][0]=sn[i][j];
	      interaction[i][1]=sn[i][l];
	    }
	  }
	}
   nextsecond:;  
      }
   
 
    }
    
  suite:if(numn[i]==1){
    mincosdif=minprobtrack; 
    interaction[i][0]=sn[i][0];
  }
    probtot[i]=mincosdif;
  
  }
  
  
  /***********************************************************************/
  /************ sort clusters according to figure of merit ****************/ 
  /***********************************************************************/
 
 
  // single interactions are awarded for the time being the minimum figure of merit 
  
  for(i=0;i<n;i++) {
    for(j=i+1;j<n;j++) {
      if(probtot[i] < probtot[j]) {
	swap(probtot, i, j);
	swap(angn,i,j);
	swap(et, i, j);
	swapi(flagu, i, j);
	swapi(flagpair, i, j);
	for (l=0;l<numn[i];l++)
	  temp1[l] = interaction[i][l];
	for(l=0;l<numn[j];l++)
	  interaction[i][l]=interaction[j][l];
	for(l=0;l<numn[i];l++)
	  interaction[j][l]=temp1[l];
	for(l=0;l<numn[i];l++)
	  temp1[l] = sn[i][l];
	for(l=0;l<numn[j];l++)
	  sn[i][l]=sn[j][l];
	for(l=0;l<numn[i];l++)
	  sn[j][l]=temp1[l];
        swapi(numn, i, j);      
	
      }
    }
  }
  
  
  
  for(i=0;i<n;i++) {
    
    if(flagu[i]==0) {
      for(k=0;k<numn[i];k++) {
	for(l=i+1;l<n;l++) {
	  
	  for(m=0;m<numn[l];m++) {
	    if(sn[i][k] == sn[l][m]){
	      
	      flagu[l]=1;
	      break;
	    }
	  }
	  
	}
	
      }   
      
    }
    
  }

  number_of_unflagged=0;
   for(i=0;i<n;i++){
    if(flagu[i]==0)number_of_unflagged++;

  }
  

  for(j=0;j<n;j++){
  
   
    if(flagu[j]==0 && probtot[j] >= minprobtrack && numn[j]>1) {
     
   
      thetafirst[j]=(z[interaction[j][0]]-zsource)/r[interaction[j][0]][interaction[j][0]];
    
     
      etot[itot]=et[j]*(1-voverc*thetafirst[j])/betafactor;
  
      nrj=(int) (1000*(etot[itot]));
      if(nrj < 10000)spec[nrj]++;
      
    
           fprintf(fp3,"event # %d reconstructed energy %f int points %d \n",nshoot-1,etot[itot],numn[j]);
      for(i=0;i<numn[j];i++)
		fprintf(fp3,"incident number %d order %d energy %f and position %f %f %f \n",numinter[interaction[j][i]],order[interaction[j][i]],e[interaction[j][i]],x[interaction[j][i]],y[interaction[j][i]],z[interaction[j][i]]);

      itot++;
  
      
    }
  }
  
  
  i=0;
  
  for(j=0;j<n;j++) {
    if(numn[j]==1 && flagu[j]==0)i++;
    
  }
  
 
  
  /**********************************/
  /* test of direct photopic events */
  /**********************************/
  
  
  if(nodirect==1)goto end; // do not treat single interactions
  j=i;
  
  for(l=0;l<n;l++) {
    
    if(numn[l]==1 && flagu[l]==0) {
      rmin=100;
      if(nb_int==1)rmin=photodistmax+1;
      for(m=0;m<nb_int;m++) {
	if(m!=sn[l][0]){
	  if(r[sn[l][0]][m] < rmin)
	    rmin = r[sn[l][0]][m];
	}
	
      }
      if(rmin > photodistmax) {

	cross1=sig_abs(et[l]);
	
	cross = sig_pair(et[l]);
	cross2=cross1+cross+sig_compt(et[l]);
	lambda=range_process(cross2);
	probcomp = proba(lambda,r_ge[sn[l][0]][sn[l][0]])*cross1/cross2;

	if(sqrt(probcomp) > minprobsing) {
	  thetafirst[l]=(z[interaction[l][0]]-zsource)/r[interaction[l][0]][interaction[l][0]];
	  
	  etot[itot]=et[l]*(1-voverc*thetafirst[l])/betafactor;

	  nrj=(int) (1000*(etot[itot]));
	  if(nrj < 10000)spec[nrj]++;
	  
	  fprintf(fp3,"event # %d reconstructed energy %f int points %d \n",nshoot-1,etot[itot],1);
	  fprintf(fp3,"incident number %d order %d energy %f and position %f %f %f \n",numinter[sn[l][0]],order[sn[l][0]],e[sn[l][0]],x[sn[l][0]],y[sn[l][0]],z[sn[l][0]]);
	  
	  for(i=0;i<imult;i++) {
	    if(fabs(et[l]-energygam[i])<=3e-3) {
	    numbsingrecon++;
	  }
	  }
	  itot++;

	  j--;
	  
	}
      }
    }
    
    
  }
  
  
  
  
 end: ;
  
  if(readflag == 0) {
    counter =1;
     pairprod=0;
     readpair=0;
    goto readfile ; /* go to read next event */
  }
  
  /**********************************************************************/
  /************* end of tracking procedure *****************************/
  /*********************************************************************/
  
  

  for (i=0;i<itot;i++) {
    for(l=0;l<imult;l++) {
    if(fabs(etot[i] - energygam[l]) <= 3e-3 ) {
      phot[l]++;                 
    }
    }
  }
  
  for(i=0;i<10000;i++)fprintf(fp1,"%d %d \n",i,spec[i]);
 

  for(i=0;i<imult;i++) {
  effphot[i] = phot[i]*1.0/(nbrtotgam/imult);
  peak_total[i] = phot[i]*1.0/(itot/imult);
  
  }
   printf("total numb of gamma-rays shot = %d \n",nbrtotgam);
  printf("total numb of gamma-rays with interaction = %d \n",nshoot);
  printf("total numb of gamma-rays detected = %d \n",itot);
    for(i=0;i<imult;i++){
      printf("energy = %f \n",energygam[i]);
    printf("number of photopics reconstructed = %d \n",phot[i]);
    printf("number of real photopic events %d \n",nphoto4pi[i]);
    }
   printf("number of sing photo clusters =%d \n",nsingint);
  printf("number of sing photo clusters properly reconstructed=%d \n",numbsingrecon);
 printf("total number of clusters =%d \n",ntot);
    fprintf(fp2,"multiplicity =%d \n",mult);
     fprintf(fp2,"total number of incident gammas =%d \n",nbrtotgam);
  for(i=0;i<imult;i++){
    fprintf(fp2,"energy =%f \n",energygam[i]);
 fprintf(fp2,"photopic eff =%f \n",effphot[i]);
 fprintf(fp2,"peak to total =%f \n",peak_total[i]);
 


  }
 
  for(i=0;i<10;i++)printf("icompseg of i = %d = %d \n",i,icompseg[i]);
 

  fclose(fp);
  fclose(fp1);
  fclose(fp2);
  fclose(fp3);
 
  
} /* end main */



double sig_compt(double E)
     
{
  /* sigma = 2 pi r_0^2 *[((1+gamma)/gamma^2 *(2(1+gamma)/1+2gamma - ln(1+2gamma)/gamma)) + ln(1+2gamma)/2gamma - 1+3gamma/(1+2gamma)^2] */
  // fits geant data very well   
  
  double temp;
  double temp0;
  double temp1;
  double temp2;
  double temp3;
  double gamma;
  
  temp0 = 1e4*2*PI*SQ(r_0)*Z_ge; /* in cm2/atom */
  gamma = E/mec2;
  
  temp1 =1+gamma;
  temp2= 1 +(2*gamma);
  temp3= 1+(3*gamma);
  temp = (temp1/SQ(gamma)) * (((2*temp1)/temp2) - (log(temp2)/gamma));
  temp=temp +(log(temp2)/(2*gamma) - (temp3/SQ(temp2))); 
  temp=temp*temp0;
  
  return temp ;
  
}

double sig_abs(double E)
     
{
  
  /* sigma = 4 alpha^4*sqrt(2)*Z^5 phi_0*(E/mec2)^(-7/2),phi_0=8/3 pi r_0^2 */
  /* sigma abs K+L = 9/8* sigma_Kshell */
  
  double temp;
  double gamma;
  double hnu_k;
  
  
  hnu_k = SQ(Z_ge - 0.03)*mec2*SQ(alpha)/2;
  gamma = CB(E/mec2)*CB(E/mec2)*E/mec2;
  gamma = sqrt(gamma);
  
  temp = 4*CB(alpha)*alpha*1.4142*6.651e-25*CB(Z_ge)*SQ(Z_ge);
  
  temp = sqrt(E/mec2) * temp/gamma; /* en cm2/atom */
  
  // not well suited for energies below 20 keV 
  // removed the 1.125 factor and added sqrt(E/mec2) to fit data 
  
  if(E < 0.025) {
    
    temp = 2.2*pow((hnu_k/E),2.6666)*6.3e-18/SQ(Z_ge);
    if(E<0.0111)
      temp = temp/8.5;
    
  }
  return temp;
  
}


double range_process(double sig)
{
  
  /* SIGMA MACRO = 1/lambda = 6.022e23 * rho_ge/A_ge * sigma (cm2/atom) */
  
  
  double temp;
  
  temp = (sig * N_av * rho_ge) / A_ge;
  temp = 1/(temp); /* in cm */
  
  return temp ;
  
  
  
}

double proba(double range, double distance)
     
{
  double temp;
  double nlambda;
  
  nlambda = distance/range;
  temp = exp(-nlambda);
  
  return temp;
  
  
  
}

void swap(double v[],int m, int l)
{
  double temp;
  
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
  
}

void swapi(int v[],int m, int l)
{
  int temp;
  
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
  
}

double sig_pair(double E)
{/* fitted in 1e-24cm2 units for Ge from atomic data tables 1970 7 page 590 */
  
  double temp;
  
  temp = 0.792189*log(E+0.948261-1.1332*E+0.15567*SQ(E));
  
  if(E<1.022)
    temp = 0;
  
  if((E < 1.15) & (E >= 1.022))
    temp=(1-((1.15-E)/0.129))*7.55e-28;
  
  
  return temp*1e-24;
}
double err_cos(double xa, double xb, double xc, double ya, double yb, double yc, double za, double zb, double zc, double rab, double rbc, double err)
     /* error on AB.BC/AB x BC */
{
  double prod;
  double dcosdxa,dcosdxb,dcosdxc;
  double dcosdya,dcosdyb,dcosdyc;
  double dcosdza,dcosdzb,dcosdzc;
  
  double temp;
  
  prod = (xa-xb)*(xb-xc) + (ya-yb)*(yb-yc)+ (za-zb)*(zb-zc);
  
  
  dcosdxa = -(xc-xb)/(rab*rbc) -0.5*(2*xa-2*xb)*prod/(CB(rab)*rbc);  
  dcosdxb = (xa-2*xb+xc)/(rab*rbc) -0.5*(2*xb-2*xa)*prod/(CB(rab)*rbc) +0.5*(2*xc-2*xb)*prod/(CB(rbc)*rab);
  dcosdxc = (xb-xa)/(rab*rbc) - 0.5*(2*xc-2*xb)*prod/(CB(rbc)*rab);
  
  dcosdya = -(yc-yb)/(rab*rbc) -0.5*(2*ya-2*yb)*prod/(CB(rab)*rbc);
  dcosdyb = (ya-2*yb+yc)/(rab*rbc) -0.5*(2*yb-2*ya)*prod/(CB(rab)*rbc) +0.5*(2*yc-2*yb)*prod/(CB(rbc)*rab);
  dcosdyc = (yb-ya)/(rab*rbc) - 0.5*(2*yc-2*yb)*prod/(CB(rbc)*rab);

  dcosdza = -(zc-zb)/(rab*rbc) -0.5*(2*za-2*zb)*prod/(CB(rab)*rbc);
  dcosdzb = (za-2*zb+zc)/(rab*rbc) -0.5*(2*zb-2*za)*prod/(CB(rab)*rbc) +0.5*(2*zc-2*zb)*prod/(CB(rbc)*rab);  
  dcosdzc = (zb-za)/(rab*rbc) - 0.5*(2*zc-2*zb)*prod/(CB(rbc)*rab);

  temp = SQ(dcosdxa)+SQ(dcosdxb)+SQ(dcosdxc);
  
  temp=temp+SQ(dcosdya)+SQ(dcosdyb)+SQ(dcosdyc);
  
  temp=temp+SQ(dcosdza)+SQ(dcosdzb)+SQ(dcosdzc);
  
  temp=sqrt(temp);
  
  temp=temp*err;
  


  return temp;
  } 


int packpoints(int number, double posx[], double posy[], double posz[], double energy[],int intnb[], int intorder[],int nseg[], int ndet[], double resolution)
{
  int i,j,n,l,jp[5000],jjp[5000],ip[5000],iip[5000];
  double rpack,esum;

 l=0;
 for (i=0;i<number;i++) {
      for (j=i+1;j<number;j++) {
	
	rpack= sqrt(SQ(posx[i]-posx[j])+SQ(posy[i]-posy[j])+SQ(posz[i]-posz[j]));
	
	if(rpack < resolution) {
	  l++;
	  jp[l]=j;
	  jjp[l]=j;
	  ip[l]=i;
	  iip[l]=i;
	}
      }
    }
    
    /* check if couples have already been packed by previous couples */
    
    for (i=1;i<=l;i++) {
      for(j=i+1;j<=l;j++) {
	if(ip[i] == ip[j]){ //&& ip[i] != -1) {
	  for(n=j+1;n<=l;n++) {
	    if(ip[n] == jp[i] && jp[n]==jp[j]) {
	      
	      
	      iip[n] = -1;
	      jjp[n]= -1;
	      
	      
	    }
	    if(ip[n] == jp[j] && jp[n] == jp[i]) {
	      
	      iip[n] = -1;
	      jjp[n] = -1;
	    }
	    
	  }
	  
	}
	
      }
      
    }
    
    for(n=1;n<=l;n++) {
      if(iip[n]==-1) ip[n] = -1;
      if(jjp[n]==-1) jp[n]=-1;
      
    }
    
    
    
    for (j=1;j<=l;j++) {
      
      
      
      
      for (i=0;i<number;i++) {
	
	if(ip[j] == i && jp[j] != i) {
	  
	  esum=energy[i]+energy[jp[j]];
	  
	  /* new position pondered by energie */ 
	  
	  
	  posx[i]=((posx[i]*energy[i])+(posx[jp[j]]*energy[jp[j]]))/esum; 
	  posy[i]=((posy[i]*energy[i])+(posy[jp[j]]*energy[jp[j]]))/esum;
	  posz[i]=((posz[i]*energy[i])+(posz[jp[j]]*energy[jp[j]]))/esum;
	  energy[i]=esum; /* put 2 energies into 1 */
	  
	  
	  swap(energy,number-1,jp[j]); /* put unused energy at the end of the list */
	  swap(posx,number-1,jp[j]);
	  swap(posy,number-1,jp[j]);
	  swap(posz,number-1,jp[j]);
	  swapi(intnb,number-1,jp[j]);	 
	  swapi(intorder,number-1,jp[j]);
	  swapi(nseg,number-1,jp[j]);
	  swapi(ndet,number-1,jp[j]);
	  
	  
	  
	  for (n=j+1;n<=l;n++) { 	    
	    if(ip[n] == jp[j]) { /* if the one just packed needs to be packed */
	      
	      ip[n]= i;
	      
	    }
	    
	    if(jp[n] == jp[j]) { /* if the one just packed needs to be packed */
	      jp[n]= i;
	    }
	    if(ip[n] == number-1) {/* if end of list needs to be packed */
	      
	      
	      ip[n] = jp[j];
	      
	    }
	    
	    if(jp[n]== number-1) {/* if end of list needs to be packed */
	      
	      jp[n]=jp[j];
	    }
	    
	  }
	  number-=1;  /* decrement the number of interactions */	
	}	
	
      }      
    }
    
  
    return number;



}

void smearpoints(int number, double energy[], double posx[], double posy[], double posz[],double errorfc[],double step, int nbsteps, double uncertainty)
{
  int i,j;


  double tru,err,ener_u,ener_p;

 for (i=0;i<number;i++) {

   ener_u = sqrt(1 + energy[i]*3.7)/2.35; // fwhm/2.35 in keV
   ener_p = 0.5*sqrt(0.1/energy[i])/2.35; // fwhm/2.35 in cm

   tru=drand48();
    if (tru < errorfc[0])
	err = step;
      for(j=0;j<=nbsteps;j++){
	if(errorfc[j] > tru ){
	  err = j*step;
	  break;
	}
      }      

      tru=drand48();
      if(tru > 0.5)err=err;
      if(tru <= 0.5)err=-err;
  
      
      energy[i] = energy[i]+(err*ener_u)/1000; // add error in MeV
      if(energy[i]<0)energy[i] = fabs((err*ener_u)/1000);
   
      tru = drand48();
      // printf("random = %f \n",tru);
      
      if (tru < errorfc[0])
	err = step;
      for(j=0;j<=nbsteps;j++){
	if(errorfc[j] > tru ){
	  err = j*step;
	  break;
	}
      }      
      tru=drand48();
      if(tru>0.5) /* if random > 0.5 dx=>dx */
	err=err;
      if(tru<=0.5) /* if random <=0.5 dx=>-dx */
	err=-err;      
      posx[i]=posx[i]+ener_p*err;  
      
      tru = drand48();
      if (tru < errorfc[0])
	err = step;
      for(j=0;j<=nbsteps;j++){
	
	if(errorfc[j] > tru){
	  err = j*step;
	  break;
	}
      }      
      tru=drand48();
      if(tru>0.5)
	err=err;
      if(tru<=0.5)
	err=-err;      
      posy[i]=posy[i]+ener_p*err;
      
      tru=drand48();
      if (tru < errorfc[0])
	err = step;
      for(j=0;j<nbsteps;j++){
	
	if(errorfc[j] > tru){
	  err = j*step;
	  break;
	}
      }      
      tru=drand48();
      if(tru>0.5)
	err=err;
      if(tru<=0.5)
	err=-err;      
      posz[i]=posz[i]+ener_p*err;
      
    }








}
