#ifdef GEANT2GASP
#include "geant2Gasp.h"
#else
#include "mgt.h"
#endif
#include "vectorlib.h"

#define ANCMAXDETS  256
#define ANCBUFSIZE 8192
#define ANCTIMEGAIN 1000.  /* 1ps/ch */

int     doAncTiming;
int     compactInfo;

double *ancTime;
double *ancTheta;
double *ancPhi;
double *ancPsi;

double *intriPosX;
double *intriPosY;
double *intriPosZ;

double *pixX;
double *pixY;

double *SizeX;
double *SizeY;

double *segTheta;
double *segPhi;

int    *ancLut;


unsigned short int *ancEvBuffer;

/*********************************************
*** Function prototypes   ********************
*********************************************/
void initAncillary();
void fillAncLut();
void initAncBuffer();
void initAncTime();
void initIntriPos();
void resetIntriPos();
void resetAncBuffer();
void resetAncTime();
void initAncAngles();
void initAncPixels();
void readAncAngles();
void readSegmAngles();
void readEulerAngles();
void addToAncBuffer( int ndet, char * line);
void addToAncTime( int ndet, double time );
void addIntrinsicPosition ( int ndet, int ndet1, double ener, double x1, double y1, double z1 );
void addPixelPosition( int ndet, int nseg, double xx, double yy, double zz, double x1, double y1, double z1 );
int getNumberOfAncillary( int nAnc );
int lengthOfAncillary( int type, int number );
unsigned short int* getAncillaryEvent( int nAnc );
void writeAncillaryEvent();
void finitAncBuffer();

/************************************************
*** Initializes several variables.            ***
*** numAnc types of ancillaries are assumed   ***
*** ancNDetec detectors are assumed, each of  ***
*** them with ancNSegme segments              ***
*** If ancSize exceeds the number             ***
*** which gsort can handle, the segment number  *
*** is written out as a parameter, otherwise    *
*** the index is calculated as for a cdetector  *
************************************************/
void initAncillary()
{
  if( ancSize < ANCMAXDETS )
    compactInfo = 1;
  else
    compactInfo = 0;

  if( outputmask[6]=='1' )
    doAncTiming = 1;
  else
    doAncTiming = 0;  
  
  if( doAncPixels ) {
    if( outputmask[3]=='0' ) { /* intrinsic position */ 
      printf(" Data format is not compatible with selected option! Ignoring PIXELS option.\n");
      doAncPixels = 0;
    }
  }
    
  fillAncLut();
  initAncBuffer();
  resetAncBuffer();    
}

/********************************************************
*** Fills a Lookup Table offset-->type of ancillary   ***
********************************************************/
void fillAncLut()
{
  int ii, jj;
  ancLut = (int *) calloc( maxOffset/1000, sizeof(int) );
  for( ii=0; ii<maxOffset/1000; ii++ )
    ancLut[ii] = -1;
  for( ii=0; ii<numAncil; ii++ ) {
    for( jj=0; jj<ancNParam[ii]; jj++ ) {
      ancLut[(ancOffset[ii]+1000*jj)/1000] = ii;
    }
  }  
}

/************************************************
*** Initializes the energy buffer where the   ***
*** individual energy depositions are summed  ***
************************************************/
void initAncBuffer()
{
  ancBuffer   = (double *)             calloc( ancBufLen,  sizeof(double) );
  ancEvBuffer = (unsigned short int *) calloc( ANCBUFSIZE, sizeof(int) );
  
  initAncTime();
  initIntriPos();
  initAncAngles();
  initAncPixels();
}

/**************************************************
*** Buffer for the timing information for the   ***
*** ancillary detectors. One timing information ***
*** for each parameter!                         ***
**************************************************/
void initAncTime()
{
  if( !doAncTiming ) return;

  ancTime = (double *) calloc( ancBufLen,  sizeof(double) );
}

void initIntriPos()
{
  if( outputmask[3]=='0' ) return;
  
  intriPosX = (double *) calloc( ancSize * numAncil,  sizeof(double) );
  intriPosY = (double *) calloc( ancSize * numAncil,  sizeof(double) );
  intriPosZ = (double *) calloc( ancSize * numAncil,  sizeof(double) );
}

void resetIntriPos()
{
  if( outputmask[3]=='0' ) return;

  memset( intriPosX, 0, ancSize * numAncil *sizeof(double) );
  memset( intriPosY, 0, ancSize * numAncil *sizeof(double) );
  memset( intriPosZ, 0, ancSize * numAncil *sizeof(double) );
}



/************************************************
*** Empties the energy buffer where the       ***
*** individual energy depositions are summed  ***
************************************************/
void resetAncBuffer()
{
  memset( ancBuffer, 0, ancBufLen*sizeof(double) );
  
  resetAncTime();
  resetIntriPos();
}

/************************************************
*** Empties the timing buffer                 ***
************************************************/
void resetAncTime()
{
  if( !doAncTiming ) return;

  memset( ancTime, 0, ancBufLen*sizeof(double) );
}

/************************************************
*** The detector angles can be read from file ***
*** and written out to the gsort file as      ***
*** parameters.                               ***
************************************************/
void initAncAngles()
{
  if( !(doAncAngles+doAncPixels) ) return;

  /* It is a "safe" allocation! */
  ancTheta = (double *) calloc( ancSize * numAncil, sizeof(double) );
  ancPhi   = (double *) calloc( ancSize * numAncil, sizeof(double) );

  /* sets everything to zero! */
  memset( ancTheta, 0, ancSize * numAncil*sizeof(double) );
  memset( ancPhi,   0, ancSize * numAncil*sizeof(double) );
}

/***************************************************
*** Additional data needed when PIXEL is active  ***
***************************************************/
void initAncPixels()
{
  if( !doAncPixels ) return;

  /* It is a "safe" allocation! */
  ancPsi   = (double *) calloc( ancSize * numAncil, sizeof(double) );
  segTheta = (double *) calloc( ancSize * numAncil, sizeof(double) );
  segPhi   = (double *) calloc( ancSize * numAncil, sizeof(double) );
  
  SizeX    = (double *) calloc( ancSize * numAncil, sizeof(double) );
  SizeY    = (double *) calloc( ancSize * numAncil, sizeof(double) );
  
  pixX     = (double *) calloc( ancSize * numAncil, sizeof(double) );
  pixY     = (double *) calloc( ancSize * numAncil, sizeof(double) );

  /* sets everything to zero! */
  memset( segTheta, 0, ancSize * numAncil*sizeof(double) );
  memset( segPhi,   0, ancSize * numAncil*sizeof(double) );
  memset( ancPsi,   0, ancSize * numAncil*sizeof(double) );
  memset( SizeX,    0, ancSize * numAncil*sizeof(double) );
  memset( SizeY,    0, ancSize * numAncil*sizeof(double) );
  memset( pixX,     0, ancSize * numAncil*sizeof(double) );
  memset( pixY,     0, ancSize * numAncil*sizeof(double) );
}

/*************************************
***  Read information from file(s) ***
*************************************/
void readAncAngles()
{
  if( doAncAngles )
    readSegmAngles();
  else if( doAncPixels )
    readEulerAngles();  
}

/**********************************
*** Reads the detector angles   ***
*** for each defined ancillary  ***
**********************************/
void readSegmAngles()
{
  char   line[256];
  int    ndet, nseg, ancType;
  double theta, phi;

  if((afp=fopen(afname,"r"))==NULL) {
    printf( " Could not read angles from file %s\n", afname );
    return;
  }
  if( compactInfo ) {
    while( fgets(line,256,afp) ) {
      if( line[0] == '#' ) continue;
      sscanf( line, "%d %d %lf %lf", &ancType, &ndet, &theta, &phi );
      ancTheta[ancType*ancSize+ndet] = theta;
      ancPhi  [ancType*ancSize+ndet] = phi;
    }
  }
  else {
    while( fgets(line,256,afp) ) {
      if( line[0] == '#' ) continue;
      sscanf( line, "%d %d %d %lf %lf", &ancType, &ndet, &nseg, &theta, &phi );
      ancTheta[ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = theta;
      ancPhi  [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = phi;
    }
  }
  fclose(afp);
}

/**********************************
*** Reads the detector pixels   ***
*** for each defined ancillary  ***
**********************************/
void readEulerAngles()
{
  char   line[256];
  int    ndet, nseg, nx, ny, ancType;
  double theta, phi, psi, sX, sY;
  double posX, posY, posZ;

  if((afp=fopen(afname,"r"))==NULL) {
    printf( " Could not read pixels from file %s\n", afname );
    return;
  }
  if( compactInfo ) {
    while( fgets(line,256,afp) ) {
      if( line[0] == '#' ) continue;
      sscanf( line, "%d %d %lf %lf %lf %lf %d %lf %d", &ancType, &ndet, &psi, &theta, &phi, &sX, &nx, &sY, &ny );
      ancPsi  [ancType*ancSize+ndet] = psi;
      ancTheta[ancType*ancSize+ndet] = theta;
      ancPhi  [ancType*ancSize+ndet] = phi;
      SizeX   [ancType*ancSize+ndet] = sX;
      pixX    [ancType*ancSize+ndet] = sX/nx;
      SizeY   [ancType*ancSize+ndet] = sY;
      pixY    [ancType*ancSize+ndet] = sY/ny;
    }
  }
  else {
    while( fgets(line,256,afp) ) {
      if( line[0] == '#' ) continue;
      sscanf( line, "%d %d %d %lf %lf %lf %lf %d %lf %d", &ancType, &ndet, &nseg, &psi, &theta, &phi, &sX, &nx, &sY, &ny );
      ancPsi  [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = psi;
      ancTheta[ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = theta;
      ancPhi  [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = phi;
      SizeX   [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = sX;
      pixX    [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = sX/nx;
      SizeY   [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = sY;
      pixY    [ancType*ancSize+ndet*ancNSegme[ancType]+nseg] = sY/ny;
    }
  }
  fclose(afp);
}

/************************************************
*** Adds the energy deposition to the buffer  ***
*** and dispatches additional info to the     ***
*** relevant subroutines                      ***
************************************************/
void addToAncBuffer( int ndet, char * line)
{
  double en, xx, yy, zz, tt, x1, y1, z1;
  int    nr, nseg, detNum, detPar, index, index1;
  
  if( !doAncillary ) return;
  
  if(outputmask[6]=='1') {  // time is present
    if(outputmask[3]=='1') {
      nr = sscanf(line, "%lf %lf %lf %lf %d %lf %lf %lf %lf", &en, &xx, &yy, &zz, &nseg, &tt, &x1, &y1, &z1);
      if(nr != 9) errexit("decoding line addToAncBuffer");
    }
    else {
      nr = sscanf(line, "%lf %lf %lf %lf %d %lf", &en, &xx, &yy, &zz, &nseg, &tt);
      if(nr != 6) errexit("decoding line addToAncBuffer");
    }
  }
  else {
    if(outputmask[3]=='1') {
      nr = sscanf(line, "%lf %lf %lf %lf %d %lf %lf %lf", &en, &xx, &yy, &zz, &nseg, &x1, &y1, &z1);
      if(nr != 8) errexit("decoding line addToAncBuffer");
    }
    else {
      nr = sscanf(line, "%lf %lf %lf %lf %d", &en, &xx, &yy, &zz, &nseg);
      if(nr != 5) errexit("decoding line in addToAncBuffer");
    }
  }

  detNum = ndet%1000;
  detPar = ancLut[ndet/1000];
  index  = ancSize * (ndet/1000) + detNum * ancNSegme[detPar] + nseg;
  index1 = detPar*ancSize+detNum*ancNSegme[detPar]+nseg;
  //printf( " addToAncBuffer %d %s %d\n", ndet, line, index );
  //printf( " addToAncBuffer %d %d %d %d %d %d %lf %d\n", ancSize, ndet, ancNSegme[detPar], detNum, detPar, nseg, en, index );
  
  ancBuffer[index] += en*enerfactor;
  
  addToAncTime         ( index, tt );
  addIntrinsicPosition ( index, index1, en*enerfactor, x1, y1, z1 );
  addPixelPosition     ( ndet, nseg, xx, yy, zz, x1, y1, z1 );
}

/***************************************************
*** Stores the timing information in the buffer  ***
***************************************************/
void addToAncTime( int ndet, double time )
{
  if( !doAncTiming ) return;
  
  if( ancTime[ndet] )
    ancTime[ndet] = 0.5 * ( ancTime[ndet] + time );
  else
    ancTime[ndet] = time;  
}

void addIntrinsicPosition ( int ndet, int ndet1, double ener, double x1, double y1, double z1 )
{
  if(outputmask[3]=='0') return;
  
  V3D oldPosition;
  oldPosition.xx = intriPosX[ndet1] * ( ancBuffer[ndet] - ener ) + ener * x1 / ancBuffer[ndet];
  oldPosition.yy = intriPosY[ndet1] * ( ancBuffer[ndet] - ener ) + ener * y1 / ancBuffer[ndet];
  oldPosition.zz = intriPosZ[ndet1] * ( ancBuffer[ndet] - ener ) + ener * z1 / ancBuffer[ndet];
  
  intriPosX[ndet1] = oldPosition.xx;
  intriPosY[ndet1] = oldPosition.yy;
  intriPosZ[ndet1] = oldPosition.zz;

}


/*******************************************************************************/
/*********** takes intrinsic position (mm) *************************************/
/*********** provides theta, phi of segment ************************************/
/*******************************************************************************/
void addPixelPosition( int ndet, int nseg, double xx, double yy, double zz, double x1, double y1, double z1 )
{
  int n1, n2, index, detNum, detPar;
  
  double resX, resY, shiftX, shiftY;
  double theta, phi;
  
  V3D centreModule;
  V3D absolPosition;
  V3D intriPosition;
  V3D intriPosition1;
  P3D absolPolar;
  Euler eulAng;
  
  if( !doAncPixels ) return;

  detNum = ndet%1000;
  detPar = ancLut[ndet/1000];
  index = detPar*ancSize+detNum*ancNSegme[detPar]+nseg;
  
  eulAng.ps = ancPsi  [index];
  eulAng.th = ancTheta[index];
  eulAng.ph = ancPhi  [index];
    
  absolPosition.xx = xx;
  absolPosition.yy = yy;
  absolPosition.zz = zz;
    
  /* components of intriPosition are in CRYSTAL reference frame */
  intriPosition.xx = x1;
  intriPosition.yy = y1;
  intriPosition.zz = z1;
    
  intriPosition1.xx = x1;
  intriPosition1.yy = y1;
  intriPosition1.zz = z1;
  
  /* must rotate to LAB reference frame */
  rot3D1EE( &eulAng, &intriPosition1 );
  
  centreModule.xx = absolPosition.xx - intriPosition1.xx;
  centreModule.yy = absolPosition.yy - intriPosition1.yy;
  centreModule.zz = absolPosition.zz - intriPosition1.zz;
  
  shiftX = SizeX[index]/2. + intriPosition.xx;
  intriPosition.xx = (0.5+((int)(shiftX/pixX[index])))*pixX[index] - SizeX[index]/2.;
  shiftY = SizeY[index]/2. + intriPosition.yy;
  intriPosition.yy = (0.5+((int)(shiftY/pixY[index])))*pixY[index] - SizeY[index]/2.;
  
  /* must rotate to LAB reference frame */
  rot3D1EE( &eulAng, &intriPosition );
  
  absolPosition.xx = centreModule.xx + intriPosition.xx;
  absolPosition.yy = centreModule.yy + intriPosition.yy;
  absolPosition.zz = centreModule.zz + intriPosition.zz;

  cart2pol( &absolPosition, &absolPolar );
  
  theta = absolPolar.th * 180./M_PI;
  phi   = absolPolar.ph * 180./M_PI;
  if( phi < 0. ) phi += 360.;
    
  segTheta[index] = theta;
  segPhi  [index] = phi;
  
}


/*****************************************************
*** Calculates the number of elements with nonzero   *
*** energy deposition for a given ancillary          * 
*****************************************************/
int getNumberOfAncillary( int nAnc )
{
  int i, j, k;
  int index;
  int number = 0;

  for( j=0; j<ancNDetec[nAnc]; j++ ) {
    for( k=0; k<ancNSegme[nAnc]; k++ ) {
      for( i=0; i<ancNParam[nAnc]; i++ ) {
        index = ancSize * ((ancOffset[nAnc]+1000*i)/1000) + j * ancNSegme[nAnc] + k;
        /* at least one nonzero parameter! */
        if( ancBuffer[index] ) {
          number++;
          break;
        }
      }
    }
  }
  return number;
}



/*****************************************************
*** Calculates the number of parameters written out  *
*** to the binary file for a given ancillary         * 
*****************************************************/
int lengthOfAncillary( int type, int number )
{
  int length = number * ( (1+doAncTiming)*ancNParam[type] + 1 + 2*doAncAngles + 2*doAncPixels );
  
  if( !compactInfo )
    length += number;

  return length;
    
}

/*********************************************************
*** Prepares the contribution of an ancillary detector   *
*** to the event in gsort format                         * 
*********************************************************/
unsigned short int* getAncillaryEvent( int nAnc )
{
  
  int i, j, k, index;
  
  int ancPosition = 0;
  int emptyDetector = 1;

  for( j=0; j<ancNDetec[nAnc]; j++ ) {
    for( k=0; k<ancNSegme[nAnc]; k++ ) {
      emptyDetector = 1;
      for( i=0; i<ancNParam[nAnc]; i++ ) {
        index = ancSize * ((ancOffset[nAnc]+1000*i)/1000) + j * ancNSegme[nAnc] + k;
        /* at least one nonzero parameter! */
        if( ancBuffer[index] ) {
	  //printf(" Full buffer at index %d\n", index );
          emptyDetector = 0;
	}  
      }
      if( !emptyDetector ) {
        if( compactInfo ) {
          ancEvBuffer[ancPosition++] = (unsigned short int)(ancNSegme[nAnc]*j+k);
        }  
        else {
          ancEvBuffer[ancPosition++] = (unsigned short int)j;
          ancEvBuffer[ancPosition++] = (unsigned short int)k;
        }
        for( i=0; i<ancNParam[nAnc]; i++ ) {
          index = ancSize * ((ancOffset[nAnc]+1000*i)/1000) + j * ancNSegme[nAnc] + k;
          ancEvBuffer[ancPosition++] = (unsigned short int)(ancBuffer[index]/ancBufGain);
        }
        if( doAncTiming ) {
          for( i=0; i<ancNParam[nAnc]; i++ ) {
            index = ancSize * ((ancOffset[nAnc]+1000*i)/1000) + j * ancNSegme[nAnc] + k;
            ancEvBuffer[ancPosition++] = (unsigned short int)(ANCTIMEGAIN*ancTime[index]);
          }
        }
        if( doAncAngles ) {
	  index = ancSize * nAnc + j * ancNSegme[nAnc] + k;
          ancEvBuffer[ancPosition++] = (unsigned short int)(10.*ancTheta[index]);
          ancEvBuffer[ancPosition++] = (unsigned short int)(10.*ancPhi  [index]);
        }
        if( doAncPixels ) {
	  index = ancSize * nAnc + j * ancNSegme[nAnc] + k;
          ancEvBuffer[ancPosition++] = (unsigned short int)(10.*segTheta[index]);
          ancEvBuffer[ancPosition++] = (unsigned short int)(10.*segPhi  [index]);
        }
      }
    }
  }
  return ancEvBuffer;
}

/***************************************************************
*** In case of preprocessing, writes out the contribution of ***
*** the ancillary detectors to the event in gsort format     ***
***************************************************************/
void writeAncillaryEvent()
{
  int i, j, k, l, index, index1;
  int emptyDetector = 1;
  
  if( !doAncillary ) return;
  
  for( l=0; l<numAncil; l++ ) {
    for( j=0; j<ancNDetec[l]; j++ ) {
      for( k=0; k<ancNSegme[l]; k++ ) {
	emptyDetector = 1;
	for( i=0; i<ancNParam[l]; i++ ) {
          index = ancSize * ((ancOffset[l]+1000*i)/1000) + j*ancNSegme[l] + k;
          /* at least one nonzero parameter! */
          if( ancBuffer[index] )
            emptyDetector = 0;
	}
	if( !emptyDetector ) {
          for( i=0; i<ancNParam[l]; i++ ) {
          index  = ancSize * ((ancOffset[l]+1000*i)/1000) + j*ancNSegme[l] + k;
	  index1 = ancSize * l + j*ancNSegme[l] + k;
            if( ancBuffer[index] )
              fprintf(ofp, "%4d %9.3f %7.3f %7.3f %7.3f %2d\n",
                    ancOffset[l]+1000*i+j, ancBuffer[index], 
		    intriPosX[index1], intriPosY[index1], intriPosZ[index1], k );
          }
	}
      }
    }    
  }
  
}

void finitAncBuffer()
{}
