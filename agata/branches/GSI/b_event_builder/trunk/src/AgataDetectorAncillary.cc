//////////////////////////////////////////////////////////////////
/// This class handles the placement of ancillary detectors
/// in AGATA. Each ancillary detector should be described in
/// a different class inheriting from AgataAncillaryScheme
/// and providing concrete implementation of the pure virtual
/// methods (of AgataAncillaryScheme and AgataDetectorConstructed)
/// User should describe his own ancillary!!!!
/////////////////////////////////////////////////////////////////

#include "AgataDetectorAncillary.hh"

/// User ancillary class should be included here!
#ifdef GASP
#include "AgataAncillaryEuclides.hh"
#else
#ifdef CLARA
#include "AgataAncillaryMcp.hh"
#else
#ifdef POLAR
#include "AgataAncillaryScatterer.hh"
#else
#ifdef EUCLIDES
#include "AgataAncillaryEuclides.hh"
#else
#include "AgataAncillaryShell.hh"
#include "AgataAncillaryKoeln.hh"
#include "AgataAncillaryMcp.hh"
#include "AgataAncillaryADCA.hh"
#ifdef ANCIL
#include "AgataAncillaryEuclides.hh"
#include "AgataAncillaryNeutronWall.hh"
#include "AgataAncillaryRFD.hh"
#include "AgataAncillaryBrick.hh"
#include "AgataAncillaryHelena.hh"
#include "AgataAncillaryDiamant.hh"
#include "AgataAncillaryAida.hh"
#include "AgataAncillaryCassandra.hh"
#endif
#endif
#endif
#endif
#endif

using namespace std;

#ifdef GASP
AgataDetectorAncillary::AgataDetectorAncillary( G4int type, G4String path, G4String name )
{
  numAnc = 1;
  minOffset = 3000;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theEuclides = new AgataAncillaryEuclides(path,name);
  theAncillary  [0] = (AgataAncillaryScheme*)    theEuclides;
  theConstructed[0] = (AgataDetectorConstructed*)theEuclides;
}

AgataDetectorAncillary::AgataDetectorAncillary( G4String type, G4String path, G4String name )
{
  numAnc = 1;
  minOffset = 3000;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theEuclides = new AgataAncillaryEuclides(path,name);
  theAncillary  [0] = (AgataAncillaryScheme*)    theEuclides;
  theConstructed[0] = (AgataDetectorConstructed*)theEuclides;
}
#else
#ifdef CLARA
AgataDetectorAncillary::AgataDetectorAncillary( G4int type, G4String path, G4String name )
{
  numAnc = 1;
  minOffset = 1000;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theMcp = new AgataAncillaryMcp(path,name);
  theAncillary  [0] = (AgataAncillaryScheme*)    theMcp;
  theConstructed[0] = (AgataDetectorConstructed*)theMcp;
}

AgataDetectorAncillary::AgataDetectorAncillary( G4String type, G4String path, G4String name )
{
  numAnc = 1;
  minOffset = 1000;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theMcp = new AgataAncillaryMcp(path,name);
  theAncillary  [0] = (AgataAncillaryScheme*)    theMcp;
  theConstructed[0] = (AgataDetectorConstructed*)theMcp;
}
#else
#ifdef POLAR
AgataDetectorAncillary::AgataDetectorAncillary( G4int type, G4String path )
{
  numAnc = 1;
  minOffset = 0;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  AgataAncillaryScatterer* theScatterer = new AgataAncillaryScatterer(path);
  theAncillary  [0] = (AgataAncillaryScheme*)    theScatterer;
  theConstructed[0] = (AgataDetectorConstructed*)theScatterer;
}

AgataDetectorAncillary::AgataDetectorAncillary( G4String type, G4String path )
{
  numAnc = 1;
  minOffset = 0;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  AgataAncillaryScatterer* theScatterer = new AgataAncillaryScatterer(path);
  theAncillary  [0] = (AgataAncillaryScheme*)    theScatterer;
  theConstructed[0] = (AgataDetectorConstructed*)theScatterer;
}
#else
#ifdef EUCLIDES
AgataDetectorAncillary::AgataDetectorAncillary( G4int type, G4String path )
{
  numAnc = 1;
  minOffset = 0;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theEuclides = new AgataAncillaryEuclides(path);
  theAncillary  [0] = (AgataAncillaryScheme*)    theEuclides;
  theConstructed[0] = (AgataDetectorConstructed*)theEuclides;
}

AgataDetectorAncillary::AgataDetectorAncillary( G4String type, G4String path )
{
  numAnc = 1;
  minOffset = 0;
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  theEuclides = new AgataAncillaryEuclides(path);
  theAncillary  [0] = (AgataAncillaryScheme*)    theEuclides;
  theConstructed[0] = (AgataDetectorConstructed*)theEuclides;
}
#else
AgataDetectorAncillary::AgataDetectorAncillary( G4int type, G4String path, G4String name )
{
  numAnc = 1;
#ifdef ANTIC
  minOffset = 1000;
#else
  minOffset = 0;
#endif
  theAncillary   = new AgataAncillaryScheme*    [numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];

  // pointers to the ancillary detector classes
  AgataAncillaryKoeln*    theKoeln    = NULL;
  AgataAncillaryADCA*    theADCA    = NULL;
  AgataAncillaryMcp*      theMcp      = NULL;
  AgataAncillaryShell*    theShell    = NULL;
#ifdef ANCIL
  AgataAncillaryEuclides*    theEuclides  = NULL;
  AgataAncillaryNeutronWall* theNWall     = NULL;
  AgataAncillaryRFD*         theRFD       = NULL;
  AgataAncillaryBrick*       theBrick     = NULL;
  AgataAncillaryHelena*      theHelena    = NULL;
  AgataAncillaryDiamant*     theDiamant   = NULL;
  AgataAncillaryAida*        theAida      = NULL;
  AgataAncillaryCassandra*   theCassandra = NULL;
#endif 
  
  switch(type)
  {
    case 0: // empty
      break;
    case 1:
      theKoeln       = new AgataAncillaryKoeln(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theKoeln;
      theConstructed[0] = (AgataDetectorConstructed*)theKoeln;
      break;
    case 2:
      theShell       = new AgataAncillaryShell(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theShell;
      theConstructed[0] = (AgataDetectorConstructed*)theShell;
      break;
    case 3:
      theMcp         = new AgataAncillaryMcp(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theMcp;
      theConstructed[0] = (AgataDetectorConstructed*)theMcp;
      break;
#ifdef ANCIL
    case 4:
      theEuclides    = new AgataAncillaryEuclides(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theEuclides;
      theConstructed[0] = (AgataDetectorConstructed*)theEuclides;
      break;
  case 5: // empty->ADCA
    theADCA = new AgataAncillaryADCA(path,name);
    theAncillary  [0] = (AgataAncillaryScheme*)    theADCA;
    theConstructed[0] = (AgataDetectorConstructed*)theADCA;
    break;
  case 6: 
      theBrick          = new AgataAncillaryBrick(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theBrick;
      theConstructed[0] = (AgataDetectorConstructed*)theBrick;
      break;
    case 7:
      theNWall       = new AgataAncillaryNeutronWall(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theNWall;
      theConstructed[0] = (AgataDetectorConstructed*)theNWall;
      break;
    case 8:  // Diamant
      theDiamant        = new AgataAncillaryDiamant(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theDiamant;
      theConstructed[0] = (AgataDetectorConstructed*)theDiamant;
      break;
//    case 9:  // Exogam
//      break;
    case 10:
      theHelena         = new AgataAncillaryHelena(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theHelena;
      theConstructed[0] = (AgataDetectorConstructed*)theHelena;
      break;
    case 11:
      theRFD             = new AgataAncillaryRFD(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theRFD;
      theConstructed[0] = (AgataDetectorConstructed*)theRFD;
      break;
//    case 12:  // Trace (2 SD instances)
//      break;
//    case 14:  // cup
//      break;
//    case 15:  // GASPARD
//      break;
    case 16:  // Cassandra
      theCassandra      = new AgataAncillaryCassandra(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theCassandra;
      theConstructed[0] = (AgataDetectorConstructed*)theCassandra;
      break;
    case 17:  // AIDA
      theAida           = new AgataAncillaryAida(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theAida;
      theConstructed[0] = (AgataDetectorConstructed*)theAida;
      break;
#endif
    default:
      theShell       = new AgataAncillaryShell(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theShell;
      theConstructed[0] = (AgataDetectorConstructed*)theShell;
      break;
  }
}

AgataDetectorAncillary::AgataDetectorAncillary( G4String type, G4String path, G4String name )
{
  /////////////////////////////////////////
  /// Decode the "type" string
  ///////////////////////////////////////
  G4int    position = 0;
  G4int ii;
  G4String aType = type;
  G4int length = aType.length();
  
#ifdef ANTIC
  minOffset = 1000;
#else
  minOffset = 0;
#endif
  //> how many ancillaries
  sscanf( aType.c_str(), "%d", &numAnc );
  //> allocate vectors
  whichAnc       = new G4int[numAnc];
  theAncillary   = new AgataAncillaryScheme*[numAnc];
  theConstructed = new AgataDetectorConstructed*[numAnc];
  
  //> strip first number
  if( aType.find(" ", position) != string::npos ) {
    position = aType.find(" ", position);
    aType = aType.substr(position, length);
  }  

  // pointers to the ancillary detector classes
  AgataAncillaryKoeln*    theKoeln    = NULL;
  AgataAncillaryADCA*    theADCA    = NULL;
  AgataAncillaryMcp*      theMcp      = NULL;
  AgataAncillaryShell*    theShell    = NULL;
#ifdef ANCIL
  AgataAncillaryEuclides*    theEuclides  = NULL;
  AgataAncillaryNeutronWall* theNWall     = NULL;
  AgataAncillaryRFD*         theRFD       = NULL;
  AgataAncillaryBrick*       theBrick     = NULL;
  AgataAncillaryHelena*      theHelena    = NULL;
  AgataAncillaryDiamant*     theDiamant   = NULL;
  AgataAncillaryAida*        theAida      = NULL;
  AgataAncillaryCassandra*   theCassandra = NULL;
#endif 

  for( ii=0; ii<numAnc; ii++ ) {
    //> which ancillary
    sscanf( aType.c_str(), "%d", &whichAnc[ii] );
    switch(whichAnc[ii])
    {
      case 0: // empty
	break;
      case 1:
	theKoeln       = new AgataAncillaryKoeln(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theKoeln;
	theConstructed[ii] = (AgataDetectorConstructed*)theKoeln;
	break;
      case 2:
	theShell       = new AgataAncillaryShell(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theShell;
	theConstructed[ii] = (AgataDetectorConstructed*)theShell;
	break;
      case 3:
	theMcp         = new AgataAncillaryMcp(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theMcp;
	theConstructed[ii] = (AgataDetectorConstructed*)theMcp;
	break;
#ifdef ANCIL
      case 4:
	theEuclides    = new AgataAncillaryEuclides(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theEuclides;
	theConstructed[ii] = (AgataDetectorConstructed*)theEuclides;
	break;
    case 5: // empty->ADCA
      theADCA = new AgataAncillaryADCA(path,name);
      theAncillary  [0] = (AgataAncillaryScheme*)    theADCA;
      theConstructed[0] = (AgataDetectorConstructed*)theADCA;
      break;
      case 6: 
	theBrick          = new AgataAncillaryBrick(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theBrick;
	theConstructed[ii] = (AgataDetectorConstructed*)theBrick;
	break;
      case 7:
	theNWall       = new AgataAncillaryNeutronWall(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theNWall;
	theConstructed[ii] = (AgataDetectorConstructed*)theNWall;
	break;
      case 8:  // Diamant
	theDiamant         = new AgataAncillaryDiamant(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theDiamant;
	theConstructed[ii] = (AgataDetectorConstructed*)theDiamant;
	break;
//      case 9:  // Exogam
//	break;
      case 10:
	theHelena         = new AgataAncillaryHelena(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theHelena;
	theConstructed[ii] = (AgataDetectorConstructed*)theHelena;
	break;
      case 11:
	theRFD             = new AgataAncillaryRFD(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theRFD;
	theConstructed[ii] = (AgataDetectorConstructed*)theRFD;
	break;
//      case 12:  // Trace (2 SD instances)
//	break;
//      case 14:  // cup
//	break;
//      case 15:  // Gaspard
//	break;
      case 16:  // Cassandra
	theCassandra       = new AgataAncillaryCassandra(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theCassandra;
	theConstructed[ii] = (AgataDetectorConstructed*)theCassandra;
	break;
      case 17:  // AIDA
	theAida            = new AgataAncillaryAida(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theAida;
	theConstructed[ii] = (AgataDetectorConstructed*)theAida;
	break;
#endif
      default:
	theShell       = new AgataAncillaryShell(path,name);
	theAncillary  [ii] = (AgataAncillaryScheme*)    theShell;
	theConstructed[ii] = (AgataDetectorConstructed*)theShell;
	break;
    }
    if( aType.find(" ", position) != string::npos ) {
      position = aType.find(" ", position);
      aType = aType.substr(position, length);
    }
  }
}
#endif
#endif
#endif
#endif

AgataDetectorAncillary::~AgataDetectorAncillary()
{}

////////////////////////////////////////////////////////////
///// The Placement() method calls (in the proper sequence)
///// the methods of the ancillary detector class which has
///// been instantiated in the constructor
/////////////////////////////////////////////////////////////
void AgataDetectorAncillary::Placement()
{  
  for( G4int ii=0; ii<numAnc; ii++ ) {
    if( theAncillary[ii]->FindMaterials() ) return;
    theAncillary  [ii]->GetDetectorConstruction();
    theAncillary  [ii]->InitSensitiveDetector();
    theConstructed[ii]->Placement();
  }
  this->FillAncLut();
  ///////////////////////////////////////////////////////////////
  /// Copy the offset LUT (not strictly needed for AGATA, but
  /// useful for other applications
  //////////////////////////////////////////////////////////////
  G4RunManager* runManager = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theDetector->CopyOffset( ancLut );
}

///////////////////////////////////////////////////////////
/// ancLut tells which of the ancillaries uses a specific
/// offset for AgataSensitiveDetector
///////////////////////////////////////////////////////////
#ifdef FIXED_OFFSET
void AgataDetectorAncillary::FillAncLut()
{
  G4int maxIndex = -1;
  G4int ii, jj, offs;
  
  for( ii=0; ii<numAnc; ii++ ) {
    if( maxIndex < ( theAncillary[ii]->GetAncOffset() + ( theAncillary[ii]->GetNumAncSd() - 1 ) * 1000 ) ) {
      maxIndex = theAncillary[ii]->GetAncOffset() + ( theAncillary[ii]->GetNumAncSd() - 1 ) * 1000;
    }
  }
  maxIndex /= 1000;
  
  ancLut.clear();
  ancLut.resize(maxIndex+1);
  for( ii=0; ii<maxIndex+1; ii++ )
    ancLut[ii] = -1; //> this saves some operations later!!!
  for( G4int ii=0; ii<numAnc; ii++ ) {
    offs = theAncillary[ii]->GetAncOffset();
    for( G4int jj=0; jj<theAncillary[ii]->GetNumAncSd(); jj++ ) {
      ancLut[offs/1000] = ii;
      offs += 1000;
    }
  }
  G4cout << " Filled AncLut! "<< G4endl;
}
#else
void AgataDetectorAncillary::FillAncLut()
{
  ancLut.clear();
  //> this saves some operations later!!! 
  for( G4int jj=0; jj<(minOffset/1000+1); jj++ )
    ancLut.push_back(-1);  
  
  for( G4int ii=0; ii<numAnc; ii++ ) {
    for( G4int jj=0; jj<theAncillary[ii]->GetNumAncSd(); jj++ ) {
      ancLut.push_back( ii );
    }
  }
  for( G4int kk=0;kk<((G4int)ancLut.size()); kk++ )
    G4cout << " ancLut[" << kk << "] = " << ancLut[kk] << G4endl;
}
#endif

G4int AgataDetectorAncillary::GetSegmentNumber( G4int offset, G4int nGe, G4ThreeVector position )
{
  return theConstructed[ancLut[offset/1000]]->GetSegmentNumber( offset, nGe, position );
}

void AgataDetectorAncillary::ShowStatus()
{
  for( G4int ii=0; ii<numAnc; ii++ ) {
    G4cout << " ---> Ancillary " << theAncillary[ii]->GetAncName() <<
              " has registered " << theAncillary[ii]->GetNumAncSd() << " SensitiveDetector instances"; 
#ifdef FIXED_OFFSET
    G4cout << " starting from offset " << theAncillary[ii]->GetAncOffset();
#endif	      
    G4cout << "." << G4endl;
    theConstructed[ii]->ShowStatus();
  }  
}

void AgataDetectorAncillary::WriteHeader(std::ofstream &outFileLMD, G4double unitLength)
{
  G4int offset = 0, ii, jj;
  
  offset = minOffset;
  for( ii=0; ii<numAnc; ii++ ) {
#ifdef FIXED_OFFSET
    offset = theAncillary[ii]->GetAncOffset() - 1000;
#endif  
    outFileLMD << "ANCIL " << theAncillary[ii]->GetAncName() << " " << theAncillary[ii]->GetNumAncSd();
    for( jj=0; jj<theAncillary[ii]->GetNumAncSd(); jj++ ) {
      offset += 1000;
      outFileLMD << " " << offset;
    }
    outFileLMD << G4endl;
    theConstructed[ii]->WriteHeader(outFileLMD,unitLength);
  }  
}

//////////////////////////////////////////////////////////////
/// This method returns the sum of the number of detectors
/// of each ancillary
//////////////////////////////////////////////////////////////
G4int AgataDetectorAncillary::GetNumberOfDetectors()
{
  G4int totNum = 0;
  for( G4int ii=0; ii<numAnc; ii++ ) {
    totNum += theConstructed[ii]->GetNumberOfDetectors();
  }  
  return totNum;
}

//////////////////////////////////////////////////////////////
/// This method returns the sum of max detector index
/// of each ancillary
//////////////////////////////////////////////////////////////
G4int AgataDetectorAncillary::GetMaxDetectorIndex()
{
  G4int totNum = 0;
  for( G4int ii=0; ii<numAnc; ii++ ) {
    totNum += theConstructed[ii]->GetMaxDetectorIndex();
  }  
  return totNum;
}

//////////////////////////////////////////////////////////////
/// This method returns true if at least one of the
/// ancillaries returns true
//////////////////////////////////////////////////////////////
G4bool AgataDetectorAncillary::GetReadOut()
{
  for( G4int ii=0; ii<numAnc; ii++ ) {
    if(theConstructed[ii]->GetReadOut()) {
      return true;
    }
  }  
  return false;
}

#ifdef GASP
void AgataDetectorAncillary::SetGeometry( G4int geom )
{
  theEuclides->SetGeometry(geom);
}

void AgataDetectorAncillary::ResetNumberOfSi()
{
  theEuclides->ResetNumberOfSi();
}

void AgataDetectorAncillary::ResetMaxSiIndex()
{
  theEuclides->ResetMaxSiIndex();
}

#endif
