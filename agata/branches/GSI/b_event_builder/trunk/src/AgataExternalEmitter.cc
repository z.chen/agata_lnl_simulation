#include "AgataExternalEmitter.hh"

#include "G4RunManager.hh"
#ifdef G4V10
#include "G4IonTable.hh"
#endif
#include <string>
#include <cmath>
#include "Randomize.hh"

////////////////////////////////////////////////////////////////////////////////////////
/// The default constructor assumes a proton beam on a 12C target (at 0 beam energy!)
////////////////////////////////////////////////////////////////////////////////////////
AgataExternalEmitter::AgataExternalEmitter(G4String name)
{
  aBeam = 1;
  zBeam = 1;
  aTarg = 12;
  zTarg = 6;
  eBeam = 0.;
  
  sigmaEBeamFrac = 0.;
  
  InitData();

  myMessenger = new AgataExternalEmitterMessenger(this,name);
  
}

///////////////////////////////////////////////////////////////////
/// With the second constructor beam and target can be specified
///////////////////////////////////////////////////////////////////
AgataExternalEmitter::AgataExternalEmitter( G4int z1, G4int a1, G4int z2, G4int a2, G4String name )
{
  aBeam = a1;
  zBeam = z1;
  aTarg = a2;
  zTarg = z2;
  eBeam = 0.;
  
  sigmaEBeamFrac = 0.;
  
  InitData();

  myMessenger = new AgataExternalEmitterMessenger(this,name);
  
}

/////////////////////////////////////////////////////////////////////////////
/// With the last constructor beam, target and beam energy can be specified
/////////////////////////////////////////////////////////////////////////////
AgataExternalEmitter::AgataExternalEmitter( G4int z1, G4int a1, G4int z2, G4int a2, G4double en, G4String name )
{
  aBeam = a1;
  zBeam = z1;
  aTarg = a2;
  zTarg = z2;
  eBeam = en;
  
  sigmaEBeamFrac = 0.;
  
  InitData();

  myMessenger = new AgataExternalEmitterMessenger(this,name);
  
}

AgataExternalEmitter::~AgataExternalEmitter()
{
  delete myMessenger;
}

void AgataExternalEmitter::InitBeta()
{
  emitterEnergy = G4RandGauss::shoot( 1., sigmaEBeamFrac ) * eBeam * mBeam / ( mBeam + mTarg );
  
  if( emitterEnergy < 0. )
    emitterEnergy = 0.;
  
  emitterBeta = MomentumFromEnergy ( emitterMass, emitterEnergy );
  emitterBeta = BetaFromMomentum   ( emitterMass, emitterBeta   );
  
}

void AgataExternalEmitter::UpdateEmitter( G4int zEmi, G4int aEmi )
{
  if( (zEmi > emitterAtomNum) || (aEmi > emitterMassNum) ) {
    G4cout << " Warning! Emitted particle is not compatible with the residual nucleus." << G4endl;
    return;
  }
  
  emitterAtomNum -= zEmi;
  emitterMassNum -= aEmi;
  if( (emitterAtomNum == 0) && (emitterMassNum == 0) ) /// no residual nucleus left!
    return;
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  if( (emitterAtomNum == 0) && (emitterMassNum == 1) ) /// neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#else
    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#endif

  if( !emitted )
    G4cout << " Warning! Could not find ion with Z = " << emitterAtomNum << ", A = " << emitterMassNum << G4endl;
  else
    emitterMass = emitted->GetPDGMass();
  
}

void AgataExternalEmitter::UpdateEmitter( G4int zEmi, G4int aEmi, G4ThreeVector momentumEmi )
{
  UpdateEmitter( zEmi, aEmi );
  
  emitterMomentum -= momentumEmi;
  if( emitterMomentum.mag() )
    emitterDirection = emitterMomentum/emitterMomentum.mag();
  else
    emitterDirection = G4ThreeVector( 0., 0., 1. );  
  emitterEnergy    = EnergyFromMomentum( emitterMass, emitterMomentum );
  emitterVelocity  = VelocityFromMomentum( emitterMass, emitterMomentum );
  emitterBeta      = emitterVelocity.mag() / c_light;
}

void AgataExternalEmitter::UpdateTotalNucleus()
{
  emitterMassNum = aBeam + aTarg;
  emitterAtomNum = zBeam + zTarg;

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  if( (emitterAtomNum == 0) && (emitterMassNum == 1) ) /// neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#else
    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#endif
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << emitterAtomNum << ", A = " << emitterMassNum << G4endl;
    emitterMass = mBeam + mTarg;
  }
  else
    emitterMass = emitted->GetPDGMass();
}

void AgataExternalEmitter::InitReaction()
{
  aTota = aBeam + aTarg;
  zTota = zBeam + zTarg;
  
  /// masses
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif

  G4ParticleDefinition* emitted;
  
  if( (zBeam == 0) && (aBeam == 1) ) /// neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( zBeam, aBeam, 0. );
#else
    emitted = particleTable->GetIon( zBeam, aBeam, 0. );
#endif
 
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << zBeam << ", A = " << aBeam << G4endl;
    G4cout << " Using a proton beam instead." << G4endl;
    zBeam = 1;
    aBeam = 1;
#ifdef G4V10
    emitted = ionTable->GetIon( zBeam, aBeam, 0. );
#else
    emitted = particleTable->GetIon( zBeam, aBeam, 0. );
#endif 
  }
  mBeam = emitted->GetPDGMass();
  
  if( (zTarg == 0) && (aTarg == 1) ) // neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( zTarg, aTarg, 0. );
#else
    emitted = particleTable->GetIon( zTarg, aTarg, 0. );
#endif
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << zTarg << ", A = " << aTarg << G4endl;
    G4cout << " Using a 12C target instead." << G4endl;
    zTarg =  6;
    aTarg = 12;
#ifdef G4V10
    emitted = ionTable->GetIon( zTarg, aTarg, 0. );
#else
    emitted = particleTable->GetIon( zTarg, aTarg, 0. );
#endif  
  }
  mTarg = emitted->GetPDGMass();
  
  if( (zTota == 0) && (aTota == 1) ) /// neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( zTota, aTota, 0. );
#else
    emitted = particleTable->GetIon( zTota, aTota, 0. );
#endif 
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << zTota << ", A = " << aTota << G4endl;
    mTota = mBeam + mTarg;
  }
  else
    mTota = emitted->GetPDGMass();
  
  emitterMassNum  = aTota;
  emitterAtomNum  = zTota;
  emitterMass     = mTota;
  
  emitterEnergy   = eBeam * mBeam / ( mBeam + mTarg );
  emitterMomentum = MomentumFromEnergy ( emitterMass, emitterEnergy ) * emitterDirection;
  emitterBeta     = BetaFromMomentum   ( emitterMass, emitterMomentum.mag() );
}

void AgataExternalEmitter::BeginOfRun()
{
  ((AgataEmitter*)this)->BeginOfRun();
  
  InitReaction();
  
  ((AgataEmitter*)this)->SetRecoilDiffuse (true);
  ((AgataEmitter*)this)->SetSourceDiffuse (true);
  ((AgataEmitter*)this)->SetSourceLived   (true);
  ((AgataEmitter*)this)->SetMovingSource  (true);
  ((AgataEmitter*)this)->SetBetaDiffuse   (true);
}

void AgataExternalEmitter::BeginOfEvent()
{}

void AgataExternalEmitter::EndOfEvent()
{}

void AgataExternalEmitter::EndOfRun()
{}


//////////////////////
/// Service methods
/////////////////////
G4double AgataExternalEmitter::MomentumFromEnergy( G4double mass, G4double energy )
{
  return sqrt( energy * ( energy + 2.*mass ) );
}

G4double AgataExternalEmitter::EnergyFromMomentum( G4double mass, G4ThreeVector momentum )
{
  return sqrt( momentum.mag2() + mass * mass ) - mass;
}

G4double AgataExternalEmitter::EnergyFromMomentum( G4double mass, G4double momentum )
{
  return sqrt( momentum*momentum + mass * mass ) - mass;
}

G4double AgataExternalEmitter::BetaFromMomentum( G4double mass, G4double momentum )
{
  G4double energy = EnergyFromMomentum( mass, momentum );
  
  return momentum / ( energy + mass );
}


G4ThreeVector AgataExternalEmitter::VelocityFromMomentum( G4double mass, G4ThreeVector momentum )
{
  G4double energy = EnergyFromMomentum( mass, momentum );
  
  return momentum * c_light / ( energy + mass );
}

G4ThreeVector AgataExternalEmitter::MomentumFromVelocity( G4double mass, G4ThreeVector velocity )
{
  G4double beta = velocity.mag() / c_light;
  G4double gamma = sqrt( 1. - beta * beta ); // inverse of gamma as we know it
  
  return mass * velocity / ( c_light * gamma );
}

//////////////////
/// Set methods
/////////////////
void AgataExternalEmitter::SetTargetNucleus( G4int z1, G4int a1 )
{
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  if( (z1 == 0) && (a1 == 1) ) // neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( z1, a1, 0. );
#else
    emitted = particleTable->GetIon( z1, a1, 0. );
#endif  
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << z1 << ", A = " << a1 << G4endl;
    G4cout << " ----> Keeping previous values, Z = " << zTarg << ", A = " << aTarg << G4endl;
  }
  else {
    zTarg = z1;
    aTarg = a1;
    mTarg = emitted->GetPDGMass();
    G4cout << " ----> Target nucleus has now Z = " << zTarg << ", A = " << aTarg << G4endl;
  }
}

void AgataExternalEmitter::SetBeamNucleus( G4int z1, G4int a1 )
{
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  if( (z1 == 0) && (a1 == 1) ) // neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( z1, a1, 0. );
#else
    emitted = particleTable->GetIon( z1, a1, 0. );
#endif 
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << z1 << ", A = " << a1 << G4endl;
    G4cout << " ----> Keeping previous values, Z = " << zBeam << ", A = " << aBeam << G4endl;
  }
  else {
    zBeam = z1;
    aBeam = a1;
    mBeam = emitted->GetPDGMass();
    G4cout << " ----> Beam nucleus has now Z = " << zBeam << ", A = " << aBeam << G4endl;
  }
}

void AgataExternalEmitter::SetBeamEnergy( G4double energy )
{
  if( energy < 0. ) {
    G4cout << " ----> Illegal value, keeping previous value (" << eBeam/keV << " keV)" << G4endl;
  }
  else {
    eBeam = energy * keV;
    G4cout << " ----> Beam energy is now " << eBeam/keV << " keV" << G4endl;
  }  
}

void AgataExternalEmitter::SetEnerSigma( G4double sigma )
{
  if( sigma < 0. || sigma > 100. ) {
    G4cout << " ----> Illegal value, keeping previous value (" << 100.*sigmaEBeamFrac << " %)" << G4endl;
  }
  else {
    sigmaEBeamFrac = sigma/100.;
    G4cout << " ----> Dispersion on beam energy is now " << 100.*sigmaEBeamFrac << " %" << G4endl;
  }  
}

void AgataExternalEmitter::setTotalNucleus( G4int dumZ, G4int dumA )
{
  emitterMassNum = dumA;
  emitterAtomNum = dumZ;

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  if( (emitterAtomNum == 0) && (emitterMassNum == 1) ) // neutron
    emitted = particleTable->FindParticle("neutron");
  else
#ifdef G4V10
    emitted = ionTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#else
    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
#endif 
  if( !emitted ) {
    G4cout << " Warning! Could not find ion with Z = " << emitterAtomNum << ", A = " << emitterMassNum << G4endl;
    emitterMass = mBeam + mTarg;
  }
  else
    emitterMass = emitted->GetPDGMass();
}

void AgataExternalEmitter::RestartEmitter( G4int dumZ, G4int dumA, G4double energy, 
                                               G4ThreeVector direction, G4ThreeVector position )
{
  setTotalNucleus( dumZ, dumA );

  emitterPosition  = G4ThreeVector();   /// reset emitter position
  emitterVelocity  = G4ThreeVector();   /// reset emitter velocity

  emitterEnergy    = energy;
  emitterDirection = direction;
  emitterMomentum  = MomentumFromEnergy( emitterMass, emitterEnergy ) * direction;
  emitterBeta      = BetaFromMomentum( emitterMass, emitterMomentum.mag() );
  emitterVelocity  = emitterBeta * c_light * direction;
  emitterPosition  = position;
}

void AgataExternalEmitter::RestartEmitter( G4int dumZ, G4int dumA, G4double energy, 
                                               G4ThreeVector direction )
{
  setTotalNucleus( dumZ, dumA );

  emitterPosition  = G4ThreeVector();   /// reset emitter position
  emitterVelocity  = G4ThreeVector();   /// reset emitter velocity

  emitterEnergy    = energy;
  emitterDirection = direction;
  emitterMomentum  = MomentumFromEnergy( emitterMass, emitterEnergy ) * direction;
  emitterBeta      = BetaFromMomentum( emitterMass, emitterMomentum.mag() );
  emitterVelocity  = emitterBeta * c_light * direction;
  emitterPosition  = SourceDisplacement();
  
}

void AgataExternalEmitter::RestartEmitter( G4int dumZ, G4int dumA, G4double energy )
{
  G4double momentum;

  setTotalNucleus( dumZ, dumA );

  emitterPosition  = G4ThreeVector();   /// reset emitter position
  emitterVelocity  = G4ThreeVector();   /// reset emitter velocity

  emitterEnergy    = energy;
  momentum         = MomentumFromEnergy( emitterMass, emitterEnergy );
  emitterBeta      = BetaFromMomentum( emitterMass, momentum );
  InitDirection();   
  emitterVelocity  = emitterBeta * c_light * emitterDirection;
  emitterMomentum  = momentum * emitterDirection;
  emitterPosition  = SourceDisplacement();
  
}

void AgataExternalEmitter::RestartEmitter( G4int dumZ, G4int dumA )
{
  setTotalNucleus( dumZ, dumA );

  emitterPosition  = G4ThreeVector();   /// reset emitter position
  emitterVelocity  = G4ThreeVector();   /// reset emitter velocity
  
  InitBeta();
  InitDirection();
  VelocityVector();
  emitterMomentum = MomentumFromVelocity( emitterMass, emitterVelocity );
  emitterPosition = SourceDisplacement();
}

void AgataExternalEmitter::RestartEmitter()
{  
  
  emitterMassNum = aTota;
  emitterAtomNum = zTota;
  emitterMass    = mTota;

  emitterPosition  = G4ThreeVector();   /// reset emitter position
  emitterVelocity  = G4ThreeVector();   /// reset emitter velocity
  
  InitBeta();
  InitDirection();
  VelocityVector();
  emitterMomentum = MomentumFromVelocity( emitterMass, emitterVelocity );
  emitterPosition = SourceDisplacement();
}

void AgataExternalEmitter::SetEmitterPosition( G4ThreeVector position )
{
  emitterPosition = position;
}

void AgataExternalEmitter::GetStatus( G4int emitterType )
{  
  G4cout << G4endl;
  G4cout << " Target nucleus has Z = " << zTarg << ", A = " << aTarg << G4endl;
  G4cout << " Beam   nucleus has Z = " << zBeam << ", A = " << aBeam << G4endl;
  G4cout << " Beam energy is " << eBeam/MeV << "MeV" << G4endl;
  if( (emitterType == 3) && (sigmaEBeamFrac * eBeam) )
    G4cout << " Dispersion of emitter energy " << 100. * sigmaEBeamFrac << G4endl;

  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  
  if( emitterType > 0 ) {
    switch( sourceType )
    {
      case 0:
        G4cout << " Point source in position " << std::setw(8) << sourcePosition.x()/cm
                                               << std::setw(8) << sourcePosition.y()/cm
                                               << std::setw(8) << sourcePosition.z()/cm << " cm" << G4endl;
        break;
      case 1:
        G4cout << " Point source in position " << std::setw(8) << targetPosition.x()/cm
                                               << std::setw(8) << targetPosition.y()/cm
                                               << std::setw(8) << targetPosition.z()/cm << " cm" << G4endl;
        break;
      case 2:
        G4cout << " Diffused source in position " << std::setw(8) << targetPosition.x()/cm
                                                  << std::setw(8) << targetPosition.y()/cm
                                                  << std::setw(8) << targetPosition.z()/cm << " cm" << G4endl;
        break;
      case 3:
        G4cout << " Gaussian source in position " << std::setw(8) << targetPosition.x()/cm
                                                  << std::setw(8) << targetPosition.y()/cm
                                                  << std::setw(8) << targetPosition.z()/cm << " cm" << G4endl;


        break;
    case 4:
      G4cout << " Gaussian (fwhm_x = 6 cm, fwhm_y = 4 cm) in position " << std::setw(8) << targetPosition.x()/cm
	     << std::setw(8) << targetPosition.y()/cm
	     << std::setw(8) << targetPosition.z()/cm << " cm" << G4endl;
      

        break;
    }
  }
    
  if( emitterType > 1 ) {
    G4cout << " Recoil direction " << std::setw(8) << emitterDirection.x()
                                   << std::setw(8) << emitterDirection.y()
                                   << std::setw(8) << emitterDirection.z() << G4endl;
    if( emitterOpAngle )
      G4cout << " Recoil opening angle    " << emitterOpAngle/deg << " deg." << G4endl;
  }
    
  
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataExternalEmitter::PrintToFile( G4int emitterType, std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  G4int prec = outFileLMD.precision(4);
  outFileLMD.setf(ios::fixed);
  
  outFileLMD  << "REACTION " << std::setw(4)  << zTarg
                             << std::setw(4)  << aTarg
                             << std::setw(4)  << zBeam
                             << std::setw(4)  << aBeam
                             << std::setw(16) << eBeam/unitE;
  if( emitterType >= 3 )
    outFileLMD << std::setw(8) << sigmaEBeamFrac;
    
  if( emitterType > 1 ) {  
    outFileLMD << std::setw(8) << emitterDirection.x()
               << std::setw(8) << emitterDirection.y()
               << std::setw(8) << emitterDirection.z() 
               << std::setw(8) << emitterOpAngle/deg  << G4endl;
  }
  else
    outFileLMD << G4endl;                           
  
  //////////////////////////////////////////////////////////////////////
  /// Always write out the information on the source, although it is ///
  /// not used when emitterType == 0                                 ///
  //////////////////////////////////////////////////////////////////////
  outFileLMD << "SOURCE " << std::setw(8) << sourceType
                          << std::setw(8) << movingSource;
  if( sourceType == 0 ) {
    outFileLMD  << std::setw(8) << sourcePosition.x()/unitL
                << std::setw(8) << sourcePosition.y()/unitL
                << std::setw(8) << sourcePosition.z()/unitL << G4endl;
  }
  else {
    outFileLMD  << std::setw(8) << targetPosition.x()/unitL
                << std::setw(8) << targetPosition.y()/unitL
                << std::setw(8) << targetPosition.z()/unitL << G4endl;
    if( sourceType > 1 ) {
      outFileLMD  << "TARGET " 
                  << std::setw(8) << targetSize.x()/unitL
                  << std::setw(8) << targetSize.y()/unitL
                  << std::setw(8) << targetSize.z()/unitL << G4endl;
    }            
  }             
  
  outFileLMD.unsetf(ios::fixed);
  outFileLMD.precision(prec);
}



//////////////////
/// The Messenger
//////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataExternalEmitterMessenger::AgataExternalEmitterMessenger(AgataExternalEmitter* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  

  directoryName = name + "/generator/emitter/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of emitting recoil.");
  
  commandName = directoryName + "beta";
  aLine = commandName.c_str();
  SetBetaCmd = new G4UIcmdWithADouble(aLine, this);  
  SetBetaCmd->SetGuidance("Define recoil v/c.");
  SetBetaCmd->SetGuidance("Required parameters: 1 double (v/c in %).");
  SetBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "energy";
  aLine = commandName.c_str();
  SetEnergyCmd = new G4UIcmdWithADouble(aLine, this);  
  SetEnergyCmd->SetGuidance("Define beam energy.");
  SetEnergyCmd->SetGuidance("Required parameters: 1 double (beam energy in MeV).");
  SetEnergyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "sigma";
  aLine = commandName.c_str();
  SetEnerSigmaCmd = new G4UIcmdWithADouble(aLine, this);  
  SetEnerSigmaCmd->SetGuidance("Define sigma of the beam energy distribution.");
  SetEnerSigmaCmd->SetGuidance("Required parameters: 1 double (sigma in % of the beam energy).");
  SetEnerSigmaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "openingAngle";
  aLine = commandName.c_str();
  SetAngleCmd = new G4UIcmdWithADouble(aLine, this);  
  SetAngleCmd->SetGuidance("Define opening angle for the recoil cone.");
  SetAngleCmd->SetGuidance("Required parameters: 1 double (half-opening of the cone in degrees).");
  SetAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "gaussAngle";
  aLine = commandName.c_str();
  GaussAngleCmd = new G4UIcmdWithABool(aLine, this);  
  GaussAngleCmd->SetGuidance("Set opening angle for the recoil cone to a gaussian distribution.");
  GaussAngleCmd->SetGuidance("Required parameters: none.");
  GaussAngleCmd->SetParameterName("gaussAngle",true);
  GaussAngleCmd->SetDefaultValue(true);
  GaussAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "uniformAngle";
  aLine = commandName.c_str();
  UnifoAngleCmd = new G4UIcmdWithABool(aLine, this);  
  UnifoAngleCmd->SetGuidance("Set opening angle for the recoil cone to a uniform distribution.");
  UnifoAngleCmd->SetGuidance("Required parameters: none.");
  UnifoAngleCmd->SetParameterName("gaussAngle",true);
  UnifoAngleCmd->SetDefaultValue(false);
  UnifoAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "direction";
  aLine = commandName.c_str();
  SetRecoilCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetRecoilCmd->SetGuidance("Define a fixed direction of the recoil.");
  SetRecoilCmd->SetGuidance("Required parameters: 3 double (defining the versor).");
  SetRecoilCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "thetaPhi";
  aLine = commandName.c_str();
  SetThPhiCmd = new G4UIcmdWithAString(aLine, this);  
  SetThPhiCmd->SetGuidance("Define a fixed direction of the recoil.");
  SetThPhiCmd->SetGuidance("Required parameters: 2 double (polar and azimutal angles of the recoil direction in degrees).");
  SetThPhiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "sourceType";
  aLine = commandName.c_str();
  SetSourceCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetSourceCmd->SetGuidance("Define behaviour of the source.");
  SetSourceCmd->SetGuidance(" 0 --> point source, chosen position");
  SetSourceCmd->SetGuidance(" 1 --> point source, target position");
  SetSourceCmd->SetGuidance(" 2 --> uniformly diffused source (over target)");
  SetSourceCmd->SetGuidance(" 3 --> diffused source, gaussian distribution in target");
  SetSourceCmd->SetGuidance(" 4 --> diffused source, gaussian (gsi) fwhm x/y 6/4 cm in target");
  SetSourceCmd->SetGuidance("Required parameters: 1 integer (0--3).");
  SetSourceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "positionOfSource";
  aLine = commandName.c_str();
  SetPositionCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetPositionCmd->SetGuidance("Define initial position of the source.");
  SetPositionCmd->SetGuidance("Required parameters: 3 double (source position coordinates in cm).");
  SetPositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Beam";
  aLine = commandName.c_str();
  SetBeamNucleusCmd = new G4UIcmdWithAString(aLine, this);  
  SetBeamNucleusCmd->SetGuidance("Define beam nucleus Z, A");
  SetBeamNucleusCmd->SetGuidance("Required parameters: 2 integers (Z, A of the beam nuclei).");
  SetBeamNucleusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Target";
  aLine = commandName.c_str();
  SetTargetNucleusCmd = new G4UIcmdWithAString(aLine, this);  
  SetTargetNucleusCmd->SetGuidance("Define target nucleus Z, A");
  SetTargetNucleusCmd->SetGuidance("Required parameters: 2 integers (Z, A of the target nuclei).");
  SetTargetNucleusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataExternalEmitterMessenger::~AgataExternalEmitterMessenger()
{
  delete myDirectory;
  delete SetBetaCmd;
  delete SetEnergyCmd;
  delete SetEnerSigmaCmd;
  delete SetAngleCmd;
  delete SetPositionCmd;
  delete SetRecoilCmd;
  delete SetSourceCmd;
  delete SetBeamNucleusCmd;
  delete SetThPhiCmd;
  delete SetTargetNucleusCmd;
  delete GaussAngleCmd;
  delete UnifoAngleCmd;
}

void AgataExternalEmitterMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetBetaCmd ) {
    G4cout << " This command has been disabled, use /emitter/energy instead! " << G4endl;
  }
  if( command == SetEnergyCmd ) {
    myTarget->SetBeamEnergy(SetEnergyCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetEnerSigmaCmd ) {
    myTarget->SetEnerSigma(SetEnerSigmaCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetEmitterOpAngle(SetAngleCmd->GetNewDoubleValue(newValue));
  }
  if( command == GaussAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetGaussAngle(GaussAngleCmd->GetNewBoolValue(newValue));
  }
  if( command == UnifoAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetGaussAngle(UnifoAngleCmd->GetNewBoolValue(newValue));
  }
  if( command == SetRecoilCmd ) {
    ((AgataEmitter*)(myTarget))->SetEmitterDirection(SetRecoilCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetThPhiCmd ) {
    G4double th, ph;
    sscanf( newValue, "%lf %lf", &th, &ph);
    th *= deg;
    ph *= deg;
    G4ThreeVector direzione( sin(th)*cos(ph), sin(th)*sin(ph), cos(th) );
    ((AgataEmitter*)(myTarget))->SetEmitterDirection(direzione);
  }
  if( command == SetSourceCmd ) {
    ((AgataEmitter*)(myTarget))->SetSourceType(SetSourceCmd->GetNewIntValue(newValue));
  }
  if( command == SetPositionCmd ) {
    ((AgataEmitter*)(myTarget))->SetSourcePosition(SetPositionCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetBeamNucleusCmd ) {
    G4int n1, n2;
    sscanf( newValue, "%d %d", &n1, &n2 );
    myTarget->SetBeamNucleus( n1, n2 );
  }
  if( command == SetTargetNucleusCmd ) {
    G4int n1, n2;
    sscanf( newValue, "%d %d", &n1, &n2 );
    myTarget->SetTargetNucleus( n1, n2 );
  }
}
