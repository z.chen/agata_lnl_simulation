#include "AgataPhysicsList.hh"
//
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4LossTableManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4ios.hh"

// CDP muons
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#ifndef G4V10
#include "G4MuonMinusCaptureAtRest.hh"
#else
#include "G4MuonMinusCapture.hh"
#endif

#ifndef G4V10
#include "G4SystemOfUnits.hh"
#endif

#if defined G4V495 || G4V496 || G4V10

#include "G4ParticleDefinition.hh"
#include "G4LossTableManager.hh"
#include "G4EmProcessOptions.hh"

#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4RayleighScattering.hh"
#include "G4PEEffectFluoModel.hh"
#include "G4KleinNishinaModel.hh"

#ifndef G4V495
#include "G4LowEPComptonModel.hh"
#endif

#include "G4PenelopeGammaConversionModel.hh"   

#include "G4LivermorePhotoElectricModel.hh"
#include "G4LivermoreComptonModel.hh"
#include "G4LivermoreGammaConversionModel.hh"
#include "G4LivermoreRayleighModel.hh"


#include "G4eMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#include "G4hMultipleScattering.hh"
#include "G4MscStepLimitType.hh"

#ifndef G4V10
#include "G4UrbanMscModel93.hh"
#include "G4UrbanMscModel95.hh"
#ifndef G4V495
#include "G4UrbanMscModel96.hh"
#endif
#endif

#include "G4DummyModel.hh"
#include "G4WentzelVIModel.hh"
#include "G4CoulombScattering.hh"
#include "G4eCoulombScatteringModel.hh"

#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4Generator2BS.hh"
#include "G4Generator2BN.hh"
#include "G4SeltzerBergerModel.hh"
#include "G4PenelopeIonisationModel.hh"
#include "G4LivermoreIonisationModel.hh"
#include "G4UniversalFluctuation.hh"

#include "G4eplusAnnihilation.hh"
#include "G4UAtomicDeexcitation.hh"    

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#include "G4hBremsstrahlung.hh"
#include "G4hPairProduction.hh"

#include "G4MuBremsstrahlungModel.hh"
#include "G4MuPairProductionModel.hh"
#include "G4hBremsstrahlungModel.hh"
#include "G4hPairProductionModel.hh"

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"
#include "G4IonParametrisedLossModel.hh"
#include "G4NuclearStopping.hh"


#include "G4PhysicsListHelper.hh"
#include "G4BuilderType.hh"

#endif

#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4MuonPlus.hh"
#include "G4MuonMinus.hh"
#include "G4PionPlus.hh"
#include "G4PionMinus.hh"
#include "G4KaonPlus.hh"
#include "G4KaonMinus.hh"   
#include "G4Proton.hh"
#include "G4AntiProton.hh"
#include "G4Deuteron.hh"
#include "G4Triton.hh"
#include "G4He3.hh"
#include "G4Alpha.hh"
#include "G4GenericIon.hh"
#include "G4Geantino.hh"

#ifdef G4V10
using namespace CLHEP;
#endif



AgataPhysicsList::AgataPhysicsList(G4String name, G4bool hadr, G4bool lowE, G4bool lowEH, G4bool polar, G4bool lecs ):  G4VUserPhysicsList()
{
  hadrons = hadr;
  lowEner = lowE;
  lowEnHa = lowEH;
  usePola = polar;
  useLECS = lecs;

  G4LossTableManager::Instance();
  
  defaultCutValue = 0.1*mm;
  cutForGamma     = defaultCutValue;
  cutForElectron  = defaultCutValue;
  cutForPositron  = defaultCutValue;
  cutForProton    = defaultCutValue;
  cutForNeutron   = defaultCutValue;
  cutForMuon      = defaultCutValue;

  SetVerboseLevel(1);

  myMessenger = new AgataPhysicsListMessenger(this, name, hadrons);
}

AgataPhysicsList::~AgataPhysicsList(){
  delete myMessenger;
}

////////////////////////////////////////////////////////////////
// In this method, static member functions are called
// for all particles which are used.
// This ensures that objects of these particle types will be
// created in the program.
////////////////////////////////////////////////////////////////
#if defined G4V48 || G4V494 || G4V495 || G4V496 || G4V10
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"

void AgataPhysicsList::ConstructParticle()
{
  G4BosonConstructor  pBosonConstructor;
  pBosonConstructor.ConstructParticle();

  G4LeptonConstructor pLeptonConstructor;
  pLeptonConstructor.ConstructParticle();

  G4MesonConstructor pMesonConstructor;
  pMesonConstructor.ConstructParticle();

  G4BaryonConstructor pBaryonConstructor;
  pBaryonConstructor.ConstructParticle();

  G4IonConstructor pIonConstructor;
  pIonConstructor.ConstructParticle();

  G4ShortLivedConstructor pShortLivedConstructor;
  pShortLivedConstructor.ConstructParticle();  
  
  G4Geantino::GeantinoDefinition();
}
#else
void AgataPhysicsList::ConstructParticle()
{
  G4Gamma   ::GammaDefinition();
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  G4Muon::MuonDefinition();

  if(hadrons) {
    G4Neutron ::NeutronDefinition();
    G4AntiNeutron::AntiNeutronDefinition();
    G4Proton  ::ProtonDefinition();
    G4AntiProton::AntiProtonDefinition();
    //  Ions
    G4Deuteron::DeuteronDefinition();
    G4Triton::TritonDefinition();
    G4He3::He3Definition();
    G4Alpha::AlphaDefinition();
    G4GenericIon::GenericIonDefinition();
  }
  G4Geantino::GeantinoDefinition();
}
#endif

void AgataPhysicsList::ConstructProcess()
{
  AddTransportation();
  ConstructEMStandard();
  ConstructGeneral();

#if defined G4V494 || G4V48 || G4V47
   if(hadrons) {
    ConstructHadronsEMStandard();
    ConstructHadronsEMStopp();
    ConstructHadronsElastic();
    ConstructHadronsInelastic();
  }
#endif

  ConstructAdditionalProcesses();  
}

#if defined G4V495 || G4V496 || G4V10
void AgataPhysicsList::ConstructEMStandard()
{

 G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  // muon & hadron bremsstrahlung and pair production
  G4MuBremsstrahlung* mub = new G4MuBremsstrahlung();
  G4MuPairProduction* mup = new G4MuPairProduction();
  G4hBremsstrahlung* pib = new G4hBremsstrahlung();
  G4hPairProduction* pip = new G4hPairProduction();
  G4hBremsstrahlung* kb = new G4hBremsstrahlung();
  G4hPairProduction* kp = new G4hPairProduction();
  G4hBremsstrahlung* pb = new G4hBremsstrahlung();
  G4hPairProduction* pp = new G4hPairProduction();

  // muon & hadron multiple scattering
  G4MuMultipleScattering* mumsc = new G4MuMultipleScattering();
  mumsc->AddEmModel(0, new G4WentzelVIModel());
  G4hMultipleScattering* pimsc = new G4hMultipleScattering();
  pimsc->AddEmModel(0, new G4WentzelVIModel());
  G4hMultipleScattering* kmsc = new G4hMultipleScattering();
  kmsc->AddEmModel(0, new G4WentzelVIModel());
  G4hMultipleScattering* pmsc = new G4hMultipleScattering();
  pmsc->AddEmModel(0, new G4WentzelVIModel());
  G4hMultipleScattering* hmsc = new G4hMultipleScattering("ionmsc");

  // high energy limit for e+- scattering models
  G4double highEnergyLimit = 100*MeV;

  // nuclear stopping
  G4NuclearStopping* ionnuc = new G4NuclearStopping();
  G4NuclearStopping* pnuc = new G4NuclearStopping();

  // Add standard EM Processes
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4String particleName = particle->GetParticleName();
    //if(verbose > 1)
    //  G4cout << "### " << GetPhysicsName() << " instantiates for " 
    //	     << particleName << G4endl;

    if (particleName == "gamma") {

      // Compton scattering
      G4ComptonScattering* cs = new G4ComptonScattering;
#ifdef G4V10
      cs->SetEmModel(new G4KleinNishinaModel(),1);
      G4VEmModel* theLowEPComptonModel = new G4LivermoreComptonModel();
      //G4VEmModel* theLowEPComptonModel = new G4LowEPComptonModel();
      theLowEPComptonModel->SetHighEnergyLimit(2*MeV);
      cs->AddEmModel(0, theLowEPComptonModel);
#else
      cs->SetModel(new G4LivermoreComptonModel());
#endif
      ph->RegisterProcess(cs, particle);

      // Photoelectric
      G4PhotoElectricEffect* pe = new G4PhotoElectricEffect();
#ifdef G4V10
      G4VEmModel* theLivermorePEModel = new G4LivermorePhotoElectricModel();
      theLivermorePEModel->SetHighEnergyLimit(10*GeV);
      pe->SetEmModel(theLivermorePEModel,1);
#else
      pe->SetModel(new G4LivermorePhotoElectricModel());
#endif
      ph->RegisterProcess(pe, particle);

      // Gamma conversion
      G4GammaConversion* gc = new G4GammaConversion();
#ifdef G4V10
      G4VEmModel* thePenelopeGCModel = new G4PenelopeGammaConversionModel();
      thePenelopeGCModel->SetHighEnergyLimit(1*GeV);
      gc->SetEmModel(thePenelopeGCModel,1);
#else
      gc->SetModel(new G4LivermoreGammaConversionModel());
#endif
      ph->RegisterProcess(gc, particle);

      // Rayleigh scattering
#ifdef G4V10
      ph->RegisterProcess(new G4RayleighScattering(), particle);
#else
      G4RayleighScattering* theRayleighScatt= new G4RayleighScattering();
      theRayleighScatt->SetModel(new G4LivermoreRayleighModel());
      ph->RegisterProcess(theRayleighScatt,particle);
#endif
 
    } else if (particleName == "e-") {

      // multiple scattering
      G4eMultipleScattering* msc = new G4eMultipleScattering;
      msc->SetStepLimitType(fUseDistanceToBoundary);
#ifndef G4V10
      G4UrbanMscModel95* msc1 = new G4UrbanMscModel95();
#endif
      G4WentzelVIModel* msc2 = new G4WentzelVIModel();
#ifndef G4V10
      msc1->SetHighEnergyLimit(highEnergyLimit);
#endif
      msc2->SetLowEnergyLimit(highEnergyLimit);
#ifndef G4V10
      msc->AddEmModel(0, msc1);
#endif
      msc->AddEmModel(0, msc2);

      G4eCoulombScatteringModel* ssm = new G4eCoulombScatteringModel(); 
      G4CoulombScattering* ss = new G4CoulombScattering();
#ifdef G4V10
      ss->SetEmModel(ssm, 1); 
#else
      ss->SetModel(ssm);
#endif
      ss->SetMinKinEnergy(highEnergyLimit);
      ssm->SetLowEnergyLimit(highEnergyLimit);
      ssm->SetActivationLowEnergyLimit(highEnergyLimit);

      // ionisation
      G4eIonisation* eIoni = new G4eIonisation();
      eIoni->SetStepFunction(0.2, 100*um);
#ifdef G4V10
      G4VEmModel* theIoniPenelope = new G4PenelopeIonisationModel();
      theIoniPenelope->SetHighEnergyLimit(0.1*MeV);
      eIoni->AddEmModel(0, theIoniPenelope, new G4UniversalFluctuation());
#else
      G4VEmModel* theIoniLivermore = new G4LivermoreIonisationModel();
      theIoniLivermore->SetHighEnergyLimit(0.1*MeV);
      eIoni->AddEmModel(0, theIoniLivermore, new G4UniversalFluctuation());
#endif

      // bremsstrahlung
      G4eBremsstrahlung* eBrem = new G4eBremsstrahlung();

      ph->RegisterProcess(msc, particle);
      ph->RegisterProcess(eIoni, particle);
      ph->RegisterProcess(eBrem, particle);
      ph->RegisterProcess(ss, particle);

    } else if (particleName == "e+") {

      // multiple scattering
      G4eMultipleScattering* msc = new G4eMultipleScattering;
      msc->SetStepLimitType(fUseDistanceToBoundary);
#ifndef G4V10
      G4UrbanMscModel95* msc1 = new G4UrbanMscModel95();
#endif
      G4WentzelVIModel* msc2 = new G4WentzelVIModel();
#ifndef G4V10
      msc1->SetHighEnergyLimit(highEnergyLimit);
#endif
      msc2->SetLowEnergyLimit(highEnergyLimit);
#ifndef G4V10
      msc->AddEmModel(0, msc1);
#endif
      msc->AddEmModel(0, msc2);

      G4eCoulombScatteringModel* ssm = new G4eCoulombScatteringModel(); 
      G4CoulombScattering* ss = new G4CoulombScattering();
#ifdef G4V10
      ss->SetEmModel(ssm, 1); 
#else
      ss->SetModel(ssm); 
#endif
      ss->SetMinKinEnergy(highEnergyLimit);
      ssm->SetLowEnergyLimit(highEnergyLimit);
      ssm->SetActivationLowEnergyLimit(highEnergyLimit);

      // ionisation
      G4eIonisation* eIoni = new G4eIonisation();
      eIoni->SetStepFunction(0.2, 100*um);
#ifdef G4V10
      G4VEmModel* theIoniPenelope = new G4PenelopeIonisationModel();
      theIoniPenelope->SetHighEnergyLimit(0.1*MeV);
      eIoni->AddEmModel(0, theIoniPenelope, new G4UniversalFluctuation());
#else
      G4VEmModel* theIoniLivermore = new G4LivermoreIonisationModel();
      theIoniLivermore->SetHighEnergyLimit(0.1*MeV);
      eIoni->AddEmModel(0, theIoniLivermore, new G4UniversalFluctuation());
#endif

      // bremsstrahlung
      G4eBremsstrahlung* eBrem = new G4eBremsstrahlung();

      ph->RegisterProcess(msc, particle);
      //ph->RegisterProcess(eIoni, particle);
      ph->RegisterProcess(eBrem, particle);
      ph->RegisterProcess(ss, particle);

      // annihilation at rest and in flight
      ph->RegisterProcess(new G4eplusAnnihilation(), particle);

    } else if (particleName == "mu+" ||
               particleName == "mu-"    ) {

      G4MuIonisation* muIoni = new G4MuIonisation();
      muIoni->SetStepFunction(0.2, 50*um);          

      ph->RegisterProcess(mumsc, particle);
      ph->RegisterProcess(muIoni, particle);
      ph->RegisterProcess(mub, particle);
      ph->RegisterProcess(mup, particle);
      ph->RegisterProcess(new G4CoulombScattering(), particle);

    } else if (particleName == "alpha" ||
               particleName == "He3") {

      G4hMultipleScattering* msc = new G4hMultipleScattering();
      G4ionIonisation* ionIoni = new G4ionIonisation();
      ionIoni->SetStepFunction(0.1, 10*um);

      ph->RegisterProcess(msc, particle);
      ph->RegisterProcess(ionIoni, particle);
      ph->RegisterProcess(ionnuc, particle);

    } else if (particleName == "GenericIon") {

      G4ionIonisation* ionIoni = new G4ionIonisation();
      ionIoni->SetEmModel(new G4IonParametrisedLossModel());
      ionIoni->SetStepFunction(0.1, 1*um);

      ph->RegisterProcess(hmsc, particle);
      ph->RegisterProcess(ionIoni, particle);
      ph->RegisterProcess(ionnuc, particle);





    } else if (particleName == "pi+" ||
               particleName == "pi-" ) {

      //G4hMultipleScattering* pimsc = new G4hMultipleScattering();
      G4hIonisation* hIoni = new G4hIonisation();
      hIoni->SetStepFunction(0.2, 50*um);

      ph->RegisterProcess(pimsc, particle);
      ph->RegisterProcess(hIoni, particle);
      ph->RegisterProcess(pib, particle);
      ph->RegisterProcess(pip, particle);

    } else if (particleName == "kaon+" ||
               particleName == "kaon-" ) {

      //G4hMultipleScattering* kmsc = new G4hMultipleScattering();
      G4hIonisation* hIoni = new G4hIonisation();
      hIoni->SetStepFunction(0.2, 50*um);

      ph->RegisterProcess(kmsc, particle);
      ph->RegisterProcess(hIoni, particle);
      ph->RegisterProcess(kb, particle);
      ph->RegisterProcess(kp, particle);

    } else if (particleName == "proton" ||
	       particleName == "anti_proton") {

      //G4hMultipleScattering* pmsc = new G4hMultipleScattering();
      G4hIonisation* hIoni = new G4hIonisation();
      hIoni->SetStepFunction(0.2, 50*um);

      ph->RegisterProcess(pmsc, particle);
      ph->RegisterProcess(hIoni, particle);
      ph->RegisterProcess(pb, particle);
      ph->RegisterProcess(pp, particle);
      ph->RegisterProcess(pnuc, particle);

    } else if (particleName == "B+" ||
	       particleName == "B-" ||
	       particleName == "D+" ||
	       particleName == "D-" ||
	       particleName == "Ds+" ||
	       particleName == "Ds-" ||
               particleName == "anti_He3" ||
               particleName == "anti_alpha" ||
               particleName == "anti_deuteron" ||
               particleName == "anti_lambda_c+" ||
               particleName == "anti_omega-" ||
               particleName == "anti_sigma_c+" ||
               particleName == "anti_sigma_c++" ||
               particleName == "anti_sigma+" ||
               particleName == "anti_sigma-" ||
               particleName == "anti_triton" ||
               particleName == "anti_xi_c+" ||
               particleName == "anti_xi-" ||
               particleName == "deuteron" ||
	       particleName == "lambda_c+" ||
               particleName == "omega-" ||
               particleName == "sigma_c+" ||
               particleName == "sigma_c++" ||
               particleName == "sigma+" ||
               particleName == "sigma-" ||
               particleName == "tau+" ||
               particleName == "tau-" ||
               particleName == "triton" ||
               particleName == "xi_c+" ||
               particleName == "xi-" ) {

      ph->RegisterProcess(hmsc, particle);
      ph->RegisterProcess(new G4hIonisation(), particle);
      ph->RegisterProcess(pnuc, particle);
    }
  }
    
  // Em options
  //      
  G4EmProcessOptions opt;
  //opt.SetVerbose(verbose);
  
  // Multiple Coulomb scattering
  //
  opt.SetPolarAngleLimit(CLHEP::pi);
    
  // Physics tables
  //
  opt.SetMinEnergy(100*eV);
  opt.SetMaxEnergy(10*TeV);
  opt.SetDEDXBinning(220);
  opt.SetLambdaBinning(220);

  // Nuclear stopping
  pnuc->SetMaxKinEnergy(MeV);
    
  // Ionization
  //
  //opt.SetSubCutoff(true);    

  // Deexcitation
  G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
  de->SetFluo(true);

}
#endif
//
// end of EM processes for G4 version above G495
//



#if defined G4V494 || G4V47 || G4V48

#include "G4LossTableManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
// for Nuclei:
#include "G4IonConstructor.hh"
#include "G4ionIonisation.hh"

//////////////////////////////////////////////////////////
/// Inclusion of header files for the classes describing
/// the required processes.
///////////////////////////////////////////////////////
///
/// gamma
///////////////////////////////////////////////////////

/// standard
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"

#include "AgataPolarizedComptonScattering.hh"

/// low energy -> Livermore

#include "G4LowEnergyCompton.hh"
#include "G4LowEnergyGammaConversion.hh"
#include "G4LowEnergyPhotoElectric.hh"
#include "AgataLowEnergyPolarizedCompton.hh"

#ifdef G4V47
#include "G4LowEnergyPolarizedRayleigh.hh"
#endif

/// lecs (Compton profile)
#ifdef G4LECS
#include "G4LECSCompton.hh"
#include "G4LECSRayleigh.hh"
#endif

//////////////////////////////////////////
/// all charged particles
/////////////////////////////////////////
#if defined G4V494
#include "G4hMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
#include "G4eMultipleScattering.hh"
#else
#include "G4MultipleScattering.hh"
#endif

#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"
#include "G4hIonisation.hh"    

#include "G4UserSpecialCuts.hh"

///////////////////////////////////////
/// e- e+
//////////////////////////////////////
/// standard
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

/// low energy
#include "G4LowEnergyBremsstrahlung.hh"
#include "G4LowEnergyIonisation.hh"


///////////////////////////////////////////////////////////////////////////
/// This method registers the electromagnetic processes for gamma, e-, e+
///////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructEMStandard()
{
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
    G4String particleType = particle->GetParticleType();
    if (particleName == "gamma") { //> gamma
      G4VProcess* theGammaConversion;
      G4VProcess* thePhotoElectricEffect;
      G4VProcess* theComptonScattering;
      //      G4VProcess* theRayleighScattering = NULL;

      if( !lowEner ) {
        G4cout << " Using standard EM interactions for photons" << G4endl;
        theGammaConversion     = new G4GammaConversion;
        thePhotoElectricEffect = new G4PhotoElectricEffect;
	if( usePola )
          theComptonScattering   = new AgataPolarizedComptonScattering;
	else
          theComptonScattering   = new G4ComptonScattering;
      }
      else {
	/*
 #if defined G4V495 || G4V496
         G4cout << " Using low-energy Livermore EM interactions for photons" << G4endl;
	 G4GammaConversion* theGammaConv= new G4GammaConversion();
         G4PhotoElectricEffect* thePhotoElectricEff= new G4PhotoElectricEffect();
         G4ComptonScattering* theComptonScatt= new G4ComptonScattering();
         G4RayleighScattering* theRayleighScatt= new G4RayleighScattering();
 
	 //	  theGammaConv->SetModel(new G4LivermoreComptonModel());
#ifndef G4V496
	 theGammaConv->SetModel(new G4LivermoreGammaConversionModel());
#else
	 theGammaConv->SetEmModel(new G4LivermoreGammaConversionModel());
#endif
	  ph->RegisterProcess(theGammaConv,particle);
#ifndef G4V496     
	  thePhotoElectricEff->SetModel(new G4LivermorePhotoElectricModel());
#else
	  thePhotoElectricEff->SetEmModel(new G4LivermorePhotoElectricModel());
#endif
	  ph->RegisterProcess(thePhotoElectricEff,particle);

#else
        G4cout << " Using low-energy EM interactions for photons" << G4endl;
        theGammaConversion     = new G4LowEnergyGammaConversion;
        thePhotoElectricEffect = new G4LowEnergyPhotoElectric;
#endif
	*/

       // NB polarization overrides LECS (polarization -->no LECS )
        if( !usePola ) {
          if( useLECS ) {
#ifdef G4LECS
            G4cout << " Using G4LECS package" << G4endl;
            theComptonScattering   = new G4LECSCompton;
            theRayleighScattering  = new G4LECSRayleigh;
#else

	    /*
#if defined G4V495 || G4V496
#ifndef G4V496
	    theComptonScatt->SetModel(new G4LivermoreComptonModel());
#else
	    theComptonScatt->SetEmModel(new G4LivermoreComptonModel());
#endif
	    ph->RegisterProcess(theComptonScatt,particle);

#ifndef G4V496
	    theRayleighScatt->SetModel(new G4LivermoreRayleighModel());
#else
	    theRayleighScatt->SetEmModel(new G4LivermoreRayleighModel());
#endif
	    ph->RegisterProcess(theRayleighScatt,particle);

	    #else */
            theComptonScattering   = new G4LowEnergyCompton;
            theRayleighScattering  = new G4LowEnergyRayleigh;
	    //#endif 
#endif
	  }
	  else {
	    /*
#if defined G4V495 || G4V496
#ifndef G4V496
	    theComptonScatt->SetModel(new G4LivermoreComptonModel());
#else
	    theComptonScatt->SetEmModel(new G4LivermoreComptonModel());
#endif
	    ph->RegisterProcess(theComptonScatt,particle);
#ifndef G4V496
	    theRayleighScatt->SetModel(new G4LivermoreRayleighModel());
#else
	    theRayleighScatt->SetEmModel(new G4LivermoreRayleighModel());
#endif
	    ph->RegisterProcess(theRayleighScatt,particle);

	    #else */
            theComptonScattering   = new G4LowEnergyCompton;
            theRayleighScattering  = new G4LowEnergyRayleigh;
	    //#endif
	  }	    
	}  
	else { 
          G4cout << " Using polarized photons" << G4endl;
          theComptonScattering   = new AgataLowEnergyPolarizedCompton;
#ifdef G4V47
          theRayleighScattering  = new G4LowEnergyPolarizedRayleigh;
#else

	  /*
#if defined G4V495 || G4V496
#ifndef G4V496
	  theRayleighScatt->SetModel(new G4LivermoreRayleighModel());
#else
	  theRayleighScatt->SetEmModel(new G4LivermoreRayleighModel());
#endif
	  ph->RegisterProcess(theRayleighScatt,particle); 
	  #else */
          theRayleighScattering  = new G4LowEnergyRayleigh;  // obsolete since 9.5
	  //#endif
 #endif
        }
      }    
      /*
#if defined G4V495 || G4V496
	// do nothing
	#else */
      // add processes
      pmanager->AddDiscreteProcess(theGammaConversion);
      pmanager->AddDiscreteProcess(theComptonScattering);      
      pmanager->AddDiscreteProcess(thePhotoElectricEffect);    
      if( lowEner )
        pmanager->AddDiscreteProcess(theRayleighScattering);
      //#endif
    } 


    else if (particleName == "e-") { //> electron
      //#if defined G4V494 || G4V495 || G4V496
#if defined G4V494
      G4VProcess* theeminusMultipleScattering = new G4eMultipleScattering;
#else
      G4VProcess* theeminusMultipleScattering = new G4MultipleScattering;
#endif
      G4VProcess* theeminusIonisation;        
      G4VProcess* theeminusBremsstrahlung;    
      
      if( !lowEner ) {
        theeminusIonisation         = new G4eIonisation;
        theeminusBremsstrahlung     = new G4eBremsstrahlung;
      }
      else {
	/*
#ifdef G4V495 || G4V496
        G4eIonisation* theeminusIonisat         = new G4eIonisation();
 	 theeminusIonisat->SetEmModel(new G4LivermoreIonisationModel());
	 ph->RegisterProcess(theeminusIonisat,particle); 
	G4eBremsstrahlung* theeminusBremsstrah = new G4eBremsstrahlung();
	 theeminusBremsstrah->SetEmModel(new G4LivermoreBremsstrahlungModel());
 	 ph->RegisterProcess(theeminusBremsstrah,particle); 
	 #else */
        theeminusIonisation         = new G4LowEnergyIonisation;  // obsolete since 9.5
        theeminusBremsstrahlung     = new G4LowEnergyBremsstrahlung;  // obsolete since 9.5
	pmanager->AddProcess(theeminusIonisation);
	pmanager->AddProcess(theeminusBremsstrahlung);

	//#endif
      }

       // add processes
      pmanager->AddProcess(theeminusMultipleScattering);
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxPostStep,1);

      //#if defined G4V495 || G4V496
      //#else

      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeminusIonisation,         idxAlongStep,2);
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeminusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeminusBremsstrahlung,     idxPostStep,3);
      //#endif

    } 
    else if (particleName == "e+") {  //> positron
      //#if defined G4V494 || G4V495 || G4V496
#if defined G4V494 
      G4VProcess* theeplusMultipleScattering = new G4eMultipleScattering;
#else
      G4VProcess* theeplusMultipleScattering = new G4MultipleScattering;
#endif
      G4VProcess* theeplusAnnihilation       = new G4eplusAnnihilation;
      G4VProcess* theeplusIonisation         = new G4eIonisation;    
      G4VProcess* theeplusBremsstrahlung     = new G4eBremsstrahlung;
      // add processes
      pmanager->AddProcess(theeplusMultipleScattering);
      pmanager->AddProcess(theeplusIonisation);
      pmanager->AddProcess(theeplusBremsstrahlung);
      pmanager->AddProcess(theeplusAnnihilation);
      // set ordering for AtRestDoIt
      pmanager->SetProcessOrderingToFirst(theeplusAnnihilation, idxAtRest);
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxAlongStep,2);
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeplusBremsstrahlung,     idxPostStep,3);
      pmanager->SetProcessOrdering(theeplusAnnihilation,       idxPostStep,4);
    }
  }
}



///////////////////////////////////////////////////////////////////////////
// ElectroNuclear Physics /////////////////////////////////////////////////
// void AgataPhysicsList::ConstructElectroNuclearPhysics() {

//   G4cout << "ElectroNuclear Physics" << G4endl;
//     G4ProcessManager* pmanager = particle->GetProcessManager();
//   // Mu-nuclear reaction
//   // mu-
//   pmanager = G4MuonMinus::MuonMinus()->GetProcessManager();
//   pmanager->AddDiscreteProcess(&theMuMinusNuclearInteraction);
//   // mu+
//   pmanager = G4MuonPlus::MuonPlus()->GetProcessManager();
//   pmanager->AddDiscreteProcess(&theMuPlusNuclearInteraction);
// }


////////////////////////////////////////////////////////////////////////////
/// This method registers multiple scattering processes for charged hadrons
////////////////////////////////////////////////////////////////////////////
#include "G4CoulombScattering.hh"
#include "G4IonCoulombScatteringModel.hh"
#include "G4ScreenedNuclearRecoil.hh"

void AgataPhysicsList::ConstructHadronsEMStandard()
{
  /*
#if defined G4V495 || G4V496 
  G4ScreenedNuclearRecoil* nucr = new G4ScreenedNuclearRecoil();
  G4double energyLimit = 100.*GeV;
  nucr->SetMaxEnergyForScattering(energyLimit);
#endif
  */
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    if( particle->GetPDGCharge() == 0. ) continue;

    if( particle->GetParticleName() == "e-" ) continue; //> multiple scattering for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" ) continue;

    G4ProcessManager* pmanager = particle->GetProcessManager();

    /*
#if defined G4V495 || G4V496 
    if(particle->GetParticleName()!="GenericIon" || !useScreenedNuclear){
      G4hMultipleScattering* aMultipleScattering = new G4hMultipleScattering;
      pmanager->AddProcess(aMultipleScattering, -1, 1, 1);
    } else  pmanager->AddDiscreteProcess(nucr);      
    #else */
    G4hMultipleScattering* aMultipleScattering = new G4hMultipleScattering;
    pmanager->AddProcess(aMultipleScattering, -1, 1, 1);
    //#endif
  }
}

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

// alpha and GenericIon and deuterons, triton, He3:
#if defined G4V494 || G4V49 || G4V48 || G4V47
#include "G4hLowEnergyIonisation.hh"  // obsolete since 9.5
#endif
#include "G4EnergyLossTables.hh"
// hLowEnergyIonisation uses Ziegler 1988 as the default
//#if defined G4V495 || G4V496
//#include "G4IonParametrisedLossModel.hh"
//#endif

//////////////////////////////////////////////////////////////////////////
/// This method registers stopping power EM processes for hadrons
//////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructHadronsEMStopp()
{

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    
    if( particle->GetPDGCharge() == 0. ) continue;

    if( particle->GetParticleName() == "e-" ) continue; // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" ) continue;
    
    G4ProcessManager* pmanager = particle->GetProcessManager();

    // low energy: all hadrons use G4hLowEnergyIonisation
    // but the call to SetElectronicStoppingPowerModel depends
    // on the particle type
    if( lowEnHa ) {
      G4cout << " Using low-energy EM interactions for hadrons" << G4endl;
      //#if defined G4V494 || G4V49 || G4V48 || G4V47
      G4hLowEnergyIonisation* ahadronLowEIon = new G4hLowEnergyIonisation; // obsolete since 9.5
      pmanager->AddProcess(ahadronLowEIon, -1, 2, 2 );
      //#else
      /*#if defined G4V495 || G4V496 
      if(particle->GetParticleName()!="GenericIon"){
	G4hIonisation* ahadronLowEIon = new G4hIonisation;// since 9.5
	pmanager->AddProcess(ahadronLowEIon, -1, 2, 2 );
      } else {
	G4ionIonisation* ionIoniProcess = new G4ionIonisation();
	G4IonParametrisedLossModel* ionModel = 
	  new G4IonParametrisedLossModel();
	ionIoniProcess -> SetEmModel(ionModel);
	pmanager -> AddProcess(ionIoniProcess, -1, 2, 2) ;
	if(!useScreenedNuclear){//We have to add nuclearstopping
	  G4PhysicsListHelper* ph = 
	    G4PhysicsListHelper::GetPhysicsListHelper();
	  ph->RegisterProcess(new G4NuclearStopping(), particle);
     	}
      }
#endif 
#endif */


     
   //#if defined G4V494 || G4V49 || G4V48 || G4V47
     ahadronLowEIon->SetNuclearStoppingOn();

      if (particle->GetParticleName() == "proton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Proton::ProtonDefinition(),         "ICRU_R49p");    
      else if (particle->GetParticleName() == "antiproton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4AntiProton::AntiProtonDefinition(), "ICRU_R49p");    
      else if (particle->GetParticleName() == "deuteron")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Deuteron::DeuteronDefinition(),     "ICRU_R49p");    
      else if (particle->GetParticleName() == "triton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Triton::TritonDefinition(),         "ICRU_R49p");    
      else if (particle->GetParticleName() == "He3")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4He3::He3Definition(),               "ICRU_R49p");    
      else if (particle->GetParticleName() == "alpha")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Alpha::AlphaDefinition(),           "ICRU_R49p");    
      else if (particle->GetParticleName() == "GenericIon")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");    
      else if (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");
      else if ((!particle->IsShortLived()) && (particle->GetPDGCharge() != 0.0))  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");
        
      ahadronLowEIon->SetNuclearStoppingPowerModel("ICRU_R49");      
      //#else
      //#endif      
    }
    else {
      G4cout << " Using standard EM interactions for hadrons" << G4endl;
      // standard EM: proton, deuteron and triton use G4hIonisation
      // the other ions use G4ionIonisation
      if (particle->GetParticleName() == "alpha"      ||
          particle->GetParticleName() == "He3"        ||
          particle->GetParticleName() == "GenericIon" || 
         (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  ) {
        G4ionIonisation* aIonIonisation = new G4ionIonisation; 
        pmanager->AddProcess(aIonIonisation, -1, 2, 2);
      }
 // 31/03/09: apparently there is some problem with the stopping powers for
 //           other particles (the  !particle->IsShortLived()  condition)
 //           Is that a real issue for us? Unless we go to energies opening pion production or similar ...   
/*      else if ( particle->GetParticleName() == "proton"   ||
                particle->GetParticleName() == "deuteron" ||
                particle->GetParticleName() == "triton"   ||
	       !particle->IsShortLived()  ) {
        G4hIonisation* ahadronIonisation = new G4hIonisation;
        pmanager->AddProcess(ahadronIonisation, -1, 2, 2);
      }*/
      else if ( particle->GetParticleName() == "proton"   ||
                particle->GetParticleName() == "deuteron" ||
                particle->GetParticleName() == "triton" ) {
        G4hIonisation* ahadronIonisation = new G4hIonisation;
        pmanager->AddProcess(ahadronIonisation, -1, 2, 2);
      }
    }
  }
}

/////////////////////////////////////////////////////////////
/// Hadronic processes
////////////////////////////////////////////////////////////// 
/// Elastic processes:
#include "G4HadronElasticProcess.hh"

// Inelastic processes:
#include "G4ProtonInelasticProcess.hh"
#include "G4AntiProtonInelasticProcess.hh"
#include "G4NeutronInelasticProcess.hh"
#include "G4AntiNeutronInelasticProcess.hh"
#include "G4DeuteronInelasticProcess.hh"
#include "G4TritonInelasticProcess.hh"
#include "G4AlphaInelasticProcess.hh"

// Low-energy Models: < 20GeV
#include "G4LElastic.hh"
#include "G4LEProtonInelastic.hh"
#include "G4LEAntiProtonInelastic.hh"
#include "G4LENeutronInelastic.hh"
#include "G4LEAntiNeutronInelastic.hh"
#include "G4LEDeuteronInelastic.hh"
#include "G4LETritonInelastic.hh"
#include "G4LEAlphaInelastic.hh"

// High-energy Models: >20 GeV
#include "G4HEProtonInelastic.hh"
#include "G4HEAntiProtonInelastic.hh"
#include "G4HENeutronInelastic.hh"
#include "G4HEAntiNeutronInelastic.hh"

// Neutron high-precision models: <20 MeV
#include "G4NeutronHPElastic.hh"
#include "G4NeutronHPElasticData.hh"
#include "G4NeutronHPCapture.hh"
#include "G4NeutronHPCaptureData.hh"
#include "G4NeutronHPInelastic.hh"
#include "G4NeutronHPInelasticData.hh"
#include "G4LCapture.hh"

// Stopping processes
#include "G4AntiProtonAnnihilationAtRest.hh"
#include "G4AntiNeutronAnnihilationAtRest.hh"

// Decays 
#include "G4Decay.hh"
#include "G4RadioactiveDecay.hh"
#include "G4IonTable.hh"
#include "G4Ions.hh"
/*
#if defined G4V495 || G4V496
#include "G4UAtomicDeexcitation.hh"
#include "AgataNuclearDecay.hh"
#endif
*/

//////////////////////////////////////////////////////////////
/// This method registers the elastic scattering processes
/// for hadrons
//////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructHadronsElastic()
{
  G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
  G4LElastic* theElasticModel = new G4LElastic;
  theElasticProcess->RegisterMe(theElasticModel);

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();

    if( particle->GetParticleName() == "gamma" ) continue;
    if( particle->GetParticleName() == "e-" )    continue; // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" )    continue;

    G4ProcessManager* pManager = particle->GetProcessManager();

    if( particle->GetParticleName() == "neutron" ) {
      // elastic scattering						         
      G4HadronElasticProcess* theNeutronElasticProcess = new G4HadronElasticProcess;					         
      G4LElastic* theElasticModel1 = new G4LElastic;			         
      G4NeutronHPElastic * theElasticNeutron = new G4NeutronHPElastic;           
      theNeutronElasticProcess->RegisterMe(theElasticModel1);		         
      theElasticModel1->SetMinEnergy(19*MeV);				         
      theNeutronElasticProcess->RegisterMe(theElasticNeutron);  	         
      G4NeutronHPElasticData * theNeutronData = new G4NeutronHPElasticData;	 
      theNeutronElasticProcess->AddDataSet(theNeutronData);		         
      pManager->AddDiscreteProcess(theNeutronElasticProcess);		         
    }
//    else if ( !particle->IsShortLived() ) { 
    else if ( particle->GetParticleName() == "anti_neutron" ||
              particle->GetParticleName() == "proton"       ||
              particle->GetParticleName() == "anti_proton"  ||
              particle->GetParticleName() == "deuteron"     ||
              particle->GetParticleName() == "triton"       ||
              particle->GetParticleName() == "alpha"        || 
              particle->GetParticleName() == "He3"          	      
	      //#ifndef G4V496
	      ||
              particle->GetParticleName() == "GenericIon"   || 
             (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)
	      //#endif
	      ) {
      pManager->AddDiscreteProcess(theElasticProcess);
    }
  }
}

//////////////////////////////////////////////////////////////
/// This method registers the inelastic scattering processes
/// for hadrons
//////////////////////////////////////////////////////////////
#include "G4HadronCaptureProcess.hh"
void AgataPhysicsList::ConstructHadronsInelastic()
{

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();

    if( particle->GetParticleName() == "gamma" ) continue;
    // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e-" )    continue; 
    if( particle->GetParticleName() == "e+" )    continue;

    G4ProcessManager* pmanager = particle->GetProcessManager();

    if(particle->GetParticleName() == "proton") {
      G4ProtonInelasticProcess* theInelasticProcess = new G4ProtonInelasticProcess("inelastic");
      G4LEProtonInelastic* theLEInelasticModel = new G4LEProtonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEProtonInelastic* theHEInelasticModel = new G4HEProtonInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    } 
    else if(particle->GetParticleName() == "anti_proton") {
      G4AntiProtonInelasticProcess* theInelasticProcess = new G4AntiProtonInelasticProcess("inelastic");
      G4LEAntiProtonInelastic* theLEInelasticModel = new G4LEAntiProtonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEAntiProtonInelastic* theHEInelasticModel = new G4HEAntiProtonInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    } 
    else if(particle->GetParticleName() == "neutron") {
      // inelastic scattering
      G4NeutronInelasticProcess* theInelasticProcess = new G4NeutronInelasticProcess("inelastic");
      G4LENeutronInelastic* theInelasticModel = new G4LENeutronInelastic;
      theInelasticModel->SetMinEnergy(19*MeV);
      theInelasticProcess->RegisterMe(theInelasticModel);
      G4NeutronHPInelastic * theLENeutronInelasticModel = new G4NeutronHPInelastic;
      theInelasticProcess->RegisterMe(theLENeutronInelasticModel);
      G4NeutronHPInelasticData * theNeutronData1 = new G4NeutronHPInelasticData;
      theInelasticProcess->AddDataSet(theNeutronData1);
      pmanager->AddDiscreteProcess(theInelasticProcess);

      // capture
      G4HadronCaptureProcess* theCaptureProcess = new G4HadronCaptureProcess;
      G4LCapture* theCaptureModel = new G4LCapture;
      theCaptureModel->SetMinEnergy(19*MeV);
      theCaptureProcess->RegisterMe(theCaptureModel);
      G4NeutronHPCapture * theLENeutronCaptureModel = new G4NeutronHPCapture;
      theCaptureProcess->RegisterMe(theLENeutronCaptureModel);
      G4NeutronHPCaptureData * theNeutronData3 = new G4NeutronHPCaptureData;
      theCaptureProcess->AddDataSet(theNeutronData3);
      pmanager->AddDiscreteProcess(theCaptureProcess);
    } 
    else if(particle->GetParticleName() == "anti_neutron") {
      G4AntiNeutronInelasticProcess* theInelasticProcess = new G4AntiNeutronInelasticProcess("inelastic");
      G4LEAntiNeutronInelastic* theLEInelasticModel = new G4LEAntiNeutronInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEAntiNeutronInelastic* theHEInelasticModel = new G4HEAntiNeutronInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "deuteron") {
      G4DeuteronInelasticProcess* theInelasticProcess = new G4DeuteronInelasticProcess("inelastic");
      G4LEDeuteronInelastic* theLEInelasticModel = new G4LEDeuteronInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "triton") {
      G4TritonInelasticProcess* theInelasticProcess = new G4TritonInelasticProcess("inelastic");
      G4LETritonInelastic* theLEInelasticModel = new G4LETritonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "alpha") {
      G4AlphaInelasticProcess* theInelasticProcess = new G4AlphaInelasticProcess("inelastic");
      G4LEAlphaInelastic* theLEInelasticModel = new G4LEAlphaInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    }
  }
}

#endif   // end of G4V494 || G4V48 | G4V47



// Decays 
#include "G4Decay.hh"
#include "G4RadioactiveDecay.hh"
#include "G4RadioactiveDecayPhysics.hh"
#include "G4IonTable.hh"
#include "G4Ions.hh"


//////////////////////////////////////////////////////////////
/// This method registers the decay processes
/// for hadrons
//////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructGeneral()
{
#if defined G4V495 || G4V496 || G4V10
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();  
#endif


  // Add Decay Process
  G4Decay* theDecayProcess = new G4Decay();
  theParticleIterator->reset();
  while( (*theParticleIterator)() ) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    if (theDecayProcess->IsApplicable(*particle) 
	&& !particle->IsShortLived()) { 
#if defined G4V495 || G4V496 || G4V10
      ph->RegisterProcess(theDecayProcess,particle);
#else
    G4ProcessManager* pmanager = particle->GetProcessManager();

      pmanager ->AddProcess(theDecayProcess);
      pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
      pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
#endif
    }
  }
#if defined G4V495 || G4V496 || G4V10
// development in work Joa Ljungvall 28/03/2013
//  AgataNuclearDecay *agatadecay = new AgataNuclearDecay;
//  ph->RegisterProcess(agatadecay,G4GenericIon::GenericIon());
  /// !!! the command lines below works with G4V495 but not with 
  //G4V496 -> Segmentation fault (Why ????) !!! 
  /// To be investigated !!!
//Comment JL 28/03/2013, For me it works also for g496
  G4RadioactiveDecay* radioactiveDecay = new G4RadioactiveDecay();
  radioactiveDecay->SetHLThreshold(-1.*s);
  radioactiveDecay->SetICM(true);
  radioactiveDecay->SetARM(false);
  ph->RegisterProcess(radioactiveDecay, G4GenericIon::GenericIon());
  // Deexcitation (in case of Atomic Rearangement)
  //
  G4UAtomicDeexcitation* de = new G4UAtomicDeexcitation();
  de->SetFluo(true);
  de->SetAuger(true);   
  de->SetPIXE(false);  
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
  //  de->InitialiseAtomicDeexcitation();
  
#endif

}

//////////////////////////////////////////////////////////////////////////////
/// In case other processes are needed to plug-in another event generator,
/// they should be registered with this method
//////////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructAdditionalProcesses()
{


  // **************************************************//
  // *** preparing inelastic reactions for hadrons *** // 
  // **************************************************//
  //

  /*
  // high energy model for proton, neutron, pions and kaons
  theHEModel = new G4TheoFSGenerator;
  // all models for treatment of thermal nucleus 
  theEvaporation = new G4Evaporation;
  theFermiBreakUp = new G4FermiBreakUp;
  theMF = new G4StatMF;
  // evaporation logic
  theHandler = new G4ExcitationHandler;
  theHandler->SetEvaporation(theEvaporation);
  theHandler->SetFermiModel(theFermiBreakUp);
  theHandler->SetMultiFragmentation(theMF);
  theHandler->SetMaxAandZForFermiBreakUp(12, 6);
  theHandler->SetMinEForMultiFrag(3.*MeV);

  // pre-equilibrium stage 
  thePreEquilib = new G4PreCompoundModel(theHandler);
  thePreEquilib->SetMaxEnergy(70*MeV);

  // a no-cascade generator-precompound interaface
  theCascade = new G4GeneratorPrecompoundInterface;
  theCascade->SetDeExcitation(thePreEquilib);

  // QGSP model
  theStringModel = new G4QGSModel<G4QGSParticipants>;
  theHEModel->SetTransport(theCascade);
  theHEModel->SetHighEnergyGenerator(theStringModel);
  theHEModel->SetMinEnergy(6*GeV);
  theHEModel->SetMaxEnergy(100*TeV);
  // Binary cascade for p, n
  theCasc = new G4BinaryCascade;
  theCasc->SetMinEnergy(65*MeV);
  theCasc->SetMaxEnergy(6.1*GeV);
  // fragmentation
  theFragmentation = new G4QGSMFragmentation;
  theStringDecay = new G4ExcitedStringDecay(theFragmentation);
  theStringModel->SetFragmentationModel(theStringDecay);
  //
  // Binary Cascade for Pi
  theCascForPi = new G4BinaryCascade;
  theCascForPi->SetMinEnergy(0*MeV);
  theCascForPi->SetMaxEnergy(1.5*GeV);
  // LEP to fill the gap
  theLEPionPlusInelasticModel = new G4LEPionPlusInelastic();
  theLEPionPlusInelasticModel->SetMinEnergy(1.4*GeV);
  theLEPionPlusInelasticModel->SetMaxEnergy(6.1*GeV);
  theLEPionMinusInelasticModel = new G4LEPionMinusInelastic();
  theLEPionMinusInelasticModel->SetMinEnergy(1.4*GeV);
  theLEPionMinusInelasticModel->SetMaxEnergy(6.1*GeV);


  // ******************************************************* //
  // *** preparing inelastic reactions for light nuclei ***  // 
  // ******************************************************* //
  //
  // binary cascade for light nuclei
  // NOTE: Shen XS only up to 10 GeV/n;
  theIonCascade= new G4BinaryLightIonReaction;
  theIonCascade->SetMinEnergy(80*MeV);
  theIonCascade->SetMaxEnergy(40*GeV);
  theTripathiCrossSection = new G4TripathiCrossSection;
  theShenCrossSection = new G4IonsShenCrossSection;
  //
  // deuteron
  theLEDeuteronInelasticModel = new G4LEDeuteronInelastic();
  theLEDeuteronInelasticModel->SetMaxEnergy(100*MeV);
  //
  // triton
  theLETritonInelasticModel = new G4LETritonInelastic();
  theLETritonInelasticModel->SetMaxEnergy(100*MeV);
  //
  // alpha
  theLEAlphaInelasticModel = new G4LEAlphaInelastic();
  theLEAlphaInelasticModel->SetMaxEnergy(100*MeV);
  //
  // Generic Ion and He3
  // NOTE: Shen XS only up to 10 GeV/n;
  theGenIonCascade = new G4BinaryLightIonReaction;
  theGenIonCascade->SetMinEnergy(0*MeV);
  theGenIonCascade->SetMaxEnergy(30*GeV);

  
  // *************************** //
  // *** elastic scattering ***  //
  // *************************** //
  //
  theElasticModel = new G4LElastic();
  theElasticProcess.RegisterMe(theElasticModel);

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
    G4String particleType = particle->GetParticleType();
    
    if ( particleName == "mu+" || 
	 particleName == "mu-"    ) {
      //muon
      G4MuMultipleScattering* aMultipleScattering = new G4MuMultipleScattering();
      pmanager->AddProcess(aMultipleScattering,      -1, 1, 1);
      pmanager->AddProcess(new G4MuIonisation(),     -1, 2, 2);
      pmanager->AddProcess(new G4MuBremsstrahlung(), -1,-1, 3);
      pmanager->AddProcess(new G4MuPairProduction(), -1,-1, 4);
      if( particleName == "mu-" )
	pmanager->AddProcess(new G4MuonMinusCaptureAtRest(),0,-1,-1);

      // Mu-nuclear reaction
      // mu-
      pmanager = G4MuonMinus::MuonMinus()->GetProcessManager();
      pmanager->AddDiscreteProcess(&theMuMinusNuclearInteraction);
      // mu+
      pmanager = G4MuonPlus::MuonPlus()->GetProcessManager();
      pmanager->AddDiscreteProcess(&theMuPlusNuclearInteraction);
    }
    else if (particleName == "pi+") {
      pmanager->AddDiscreteProcess(&theElasticProcess);
      // NOTE: PreCo crahes for Pi+
      // thePionPlusInelasticProcess.RegisterMe(thePreEquilib);
      thePionPlusInelasticProcess.RegisterMe(theCascForPi);
      thePionPlusInelasticProcess.RegisterMe(theLEPionPlusInelasticModel);
      thePionPlusInelasticProcess.RegisterMe(theHEModel);
      pmanager->AddDiscreteProcess(&thePionPlusInelasticProcess);

    } else if (particleName == "pi-") {
      pmanager->AddDiscreteProcess(&theElasticProcess);
      // thePionMinusInelasticProcess.RegisterMe(thePreEquilib);
      thePionMinusInelasticProcess.RegisterMe(theCascForPi);
      thePionMinusInelasticProcess.RegisterMe(theLEPionMinusInelasticModel);
      thePionMinusInelasticProcess.RegisterMe(theHEModel);
      pmanager->AddDiscreteProcess(&thePionMinusInelasticProcess);
      pmanager->AddRestProcess(&thePiMinusAbsorptionAtRest, ordDefault);

    }
  }
  */
}

#include "G4StepLimiter.hh"

/*
void::AgataPhysicsList::ConstructSpecialCuts()

{
  
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    if(!useScreenedNuclear) {
      pmanager->AddDiscreteProcess(new AgataSpecialCuts());
    } else {
      pmanager->AddDiscreteProcess(new AgataSNCuts());
    }
    pmanager->AddProcess(new G4StepLimiter, -1,-1,1);
  }
  
}
*/

void AgataPhysicsList::SetCuts()
{
  SetCutsWithDefault();
  // set cut values for gamma at first and for e- second and next for e+,
  // because some processes for e+/e- need cut values for gamma 
  SetCutValue(cutForGamma,    "gamma");
  G4cout << " ----> Cut for gamma is " << cutForGamma/mm << " mm" << G4endl;
  SetCutValue(cutForElectron, "e-");
  G4cout << " ----> Cut for electron is " << cutForElectron/mm << " mm" << G4endl;
  SetCutValue(cutForPositron, "e+");
  G4cout << " ----> Cut for positron is " << cutForPositron/mm << " mm" << G4endl;
  SetCutValue(cutForMuon,    "mu-");
  G4cout << " ----> Cut for muon is " << cutForMuon/mm << " mm" << G4endl;
  SetCutValue(cutForNeutron,  "neutron");
  G4cout << " ----> Cut for neutron is " << cutForNeutron/mm << " mm" << G4endl;
  SetCutValue(cutForNeutron,  "anti_neutron");
  G4cout << " ----> Cut for antineutron is " << cutForNeutron/mm << " mm" << G4endl;
  
  SetCutValue(cutForProton,   "proton");      
  G4cout << " ----> Cut for proton is " << cutForProton/mm << " mm" << G4endl;
  SetCutValue(cutForProton,   "anti_proton"); 
  G4cout << " ----> Cut for antiproton is " << cutForProton/mm << " mm" << G4endl;
  
  if (verboseLevel>0)
    DumpCutValuesTable();
}

void AgataPhysicsList::SetGammaCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForGamma/mm << " mm" << G4endl;
  }
  else {
    cutForGamma = cut * mm;
    SetCutValue(cutForGamma,    "gamma");
    G4cout << " ----> Cut for gamma has been set to " << cutForGamma/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetElectronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForElectron/mm << " mm" << G4endl;
  }
  else {
    cutForElectron = cut * mm;
    SetCutValue(cutForElectron, "e-");
    G4cout << " ----> Cut for electron has been set to " << cutForElectron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetPositronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForPositron/mm << " mm" << G4endl;
  }
  else {
    cutForPositron = cut * mm;
    SetCutValue(cutForPositron, "e+");
    G4cout << " ----> Cut for positron has been set to " << cutForPositron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetNeutronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForNeutron/mm << " mm" << G4endl;
  }
  else {
    cutForNeutron = cut * mm;
    SetCutValue(cutForNeutron,  "neutron");
    SetCutValue(cutForNeutron,  "anti_neutron");
    G4cout << " ----> Cut for neutron has been set to " << cutForNeutron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetProtonCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForProton/mm << " mm" << G4endl;
  }
  else {
    cutForProton = cut * mm;
    SetCutValue(cutForProton,   "proton");      
    SetCutValue(cutForProton,   "anti_proton"); 
    G4cout << " ----> Cut for proton has been set to " << cutForProton/mm << " mm" << G4endl;
  }
}


/////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"

AgataPhysicsListMessenger::AgataPhysicsListMessenger(AgataPhysicsList* pTarget, G4String name, G4bool had)
:myTarget(pTarget)
{ 
  hadrons = had;

  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/physics/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of physics list parameters.");

  commandName = directoryName + "gammaCut";
  aLine = commandName.c_str();
  SetGammaCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetGammaCutCmd->SetGuidance("Sets cut for gammas.");
  SetGammaCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetGammaCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "electronCut";
  aLine = commandName.c_str();
  SetElectronCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetElectronCutCmd->SetGuidance("Sets cut for electrons.");
  SetElectronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetElectronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "positronCut";
  aLine = commandName.c_str();
  SetPositronCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetPositronCutCmd->SetGuidance("Sets cut for positrons.");
  SetPositronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetPositronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  if( hadrons ) {
    commandName = directoryName + "neutronCut";
    aLine = commandName.c_str();
    SetNeutronCutCmd = new G4UIcmdWithADouble(aLine, this);
    SetNeutronCutCmd->SetGuidance("Sets cut for neutrons.");
    SetNeutronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
    SetNeutronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName + "protonCut";
    aLine = commandName.c_str();
    SetProtonCutCmd = new G4UIcmdWithADouble(aLine, this);
    SetProtonCutCmd->SetGuidance("Sets cut for protons.");
    SetProtonCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
    SetProtonCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
}

AgataPhysicsListMessenger::~AgataPhysicsListMessenger()
{

  delete myDirectory;
  delete SetGammaCutCmd;
  delete SetElectronCutCmd;
  delete SetPositronCutCmd;
  if(hadrons) {
    delete SetNeutronCutCmd;
    delete SetProtonCutCmd;
  }  
}

void AgataPhysicsListMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetGammaCutCmd ) {
    myTarget->SetGammaCut(SetGammaCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetElectronCutCmd ) {
    myTarget->SetElectronCut(SetElectronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositronCutCmd ) {
    myTarget->SetPositronCut(SetPositronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetNeutronCutCmd ) {
    myTarget->SetNeutronCut(SetNeutronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetProtonCutCmd ) {
    myTarget->SetProtonCut(SetProtonCutCmd->GetNewDoubleValue(newValue));
  }
}    
