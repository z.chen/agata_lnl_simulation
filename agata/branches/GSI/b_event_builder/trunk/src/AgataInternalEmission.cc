#include "AgataInternalEmission.hh"
#include "Randomize.hh"
#include "AgataEmitted.hh"
#include "AgataEmitter.hh"
#include "AgataInternalEmitter.hh"

#include "AgataGeneratorAction.hh"
#include "G4RunManager.hh"

AgataInternalEmission::AgataInternalEmission( G4String path, G4bool hadr, G4bool pol, G4String name )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  hadrons    = hadr;
  usePolar   = pol;
  
  abortRun = false;
  
  theEmitted = NULL;
  if( hadrons ) {
    theEmitted    = new AgataEmitted*[6];
    theEmitted[5] = new AgataEmitted( name, "neutron", iniPath, false, true );	// admit only polarized gammas!
    theEmitted[4] = new AgataEmitted( name, "proton", iniPath, false, true );	// admit only polarized gammas!
    theEmitted[3] = new AgataEmitted( name, "alpha", iniPath, false, true );	// admit only polarized gammas!
    types = 6;
  }
  else {
    theEmitted = new AgataEmitted*[3];
    types = 3;
  }  
  
  theEmitted[2] =  new AgataEmitted( name, "e+", iniPath, false, true );  	// admit only polarized gammas! 
  theEmitted[1] =  new AgataEmitted( name, "e-", iniPath, false, true );  	// admit only polarized gammas! 
  theEmitted[0] =  new AgataEmitted( name, "gamma", iniPath, usePolar, true );  // admit only polarized gammas! 
  
  mult.resize   ( types );
  multSum.resize( types+1 );
  
  theInternalEmitter = new AgataInternalEmitter(name);
  theEmitter = (AgataEmitter*)theInternalEmitter;
  
  myMessenger = new AgataInternalEmissionMessenger(this, name);
  
}

AgataInternalEmission::~AgataInternalEmission()
{
  delete theInternalEmitter;
  for( G4int ii=0; ii<types; ii++ )
    delete theEmitted[ii];
  delete myMessenger;
}

void AgataInternalEmission::BeginOfRun()
{
  G4int i, j;
  
  theEmitter->BeginOfRun();
  
  cascadeOrder = 0;
  startOfEvent = true;
  
  mult.clear();
  multSum.clear();
  
  cascadeMult = 0;
  
  while( cascadeMult < 1 ) {
    for( i=0; i<types; i++ )
      mult[i] = theEmitted[i]->GetMultiplicity();

    multSum[0] = 0;
    for( i=0; i<types; i++ )
      multSum[i+1] = mult[types-(i+1)] + multSum[i];
      
    cascadeMult = multSum[types];

    /// There should be at least a particle!
    if( cascadeMult < 1 ) {
      G4cout << " Warning! At least one particle should be emitted!" << G4endl;
      G4cout << " ----> Setting gamma multiplicity to 1 ... " << G4endl;
      theEmitted[0]->SetMultiplicity( 1 );
    }
  }
  
  index.clear();
  order.clear();
  index.resize( cascadeMult );
  order.resize( cascadeMult );
  
  for( i=0; i<types; i++ ) {
    for( j=multSum[i]; j<multSum[i+1]; j++ ) {
      index[j] = types - (i+1);
      order[j] = j - multSum[i];
    }   
  } 
  
  G4double totalTau = 0.;
  for( i=0; i<cascadeMult; i++ ) {
    totalTau += theEmitted[index[i]]->GetEmittedTau(order[i]);
  }
  
  if( totalTau )
    theEmitter->SetSourceLived( true );  
  else
    theEmitter->SetSourceLived( false );
      
  if( totalTau && theEmitter->GetEmitterBeta() )
    theEmitter->SetMovingSource( true );  
  else
    theEmitter->SetMovingSource( false );
}

void AgataInternalEmission::BeginOfEvent()
{
  
  startOfEvent = (  cascadeOrder == 0 )              ? (true) : (false);
  endOfEvent   = (  cascadeOrder == cascadeMult -1 ) ? (true) : (false);
  
  cascadeDeltaTime = 0.;
  if( startOfEvent ) {
    cascadeTime = 0.;
    theEmitter->RestartEmitter();
  }

}

void AgataInternalEmission::EndOfEvent()
{
  cascadeOrder++;
  if( cascadeOrder >= cascadeMult )
    cascadeOrder = 0;
  
}

void AgataInternalEmission::EndOfRun()
{}

void AgataInternalEmission::NextParticle()
{
  G4int indice = index[cascadeOrder];
  G4int ordine = order[cascadeOrder];
  
  theEmitted[indice]->Emit( ordine, theEmitter->GetEmitterVelocity() );
  
  emittedName      = theEmitted[indice]->GetEmittedName();
  emittedEnergy    = theEmitted[indice]->GetEnergyLab();
  emittedDirection = theEmitted[indice]->GetDirLab();
  
  if( emittedName == "gamma" && usePolar ) {
    emittedPolarization = theEmitted[indice]->GetPolarizationLab();
    polarizedEmitted = true;
  }  
  else
    polarizedEmitted = false;  
  
  
  G4double emittedTau = theEmitted[indice]->GetEmittedTau( ordine );
  if( emittedTau ) {
    cascadeDeltaTime = -log(G4UniformRand())*emittedTau;
    cascadeTime += cascadeDeltaTime;
  }
  if( theEmitter->GetEmitterVelocity().mag2() && emittedTau ) {
    theEmitter->TraslateEmitter( cascadeDeltaTime );
  }
}

void AgataInternalEmission::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  outFileLMD << "GENERATOR 0" << G4endl;
  outFileLMD << "ENDGENERATOR" << G4endl;
  for( G4int i=0; i<types; i++ )
    if( theEmitted[i]->GetMultiplicity() )
      theEmitted[i]->PrintToFile( outFileLMD, unitE );
  theInternalEmitter->PrintToFile( outFileLMD, unitL, unitE );  
}

void AgataInternalEmission::GetStatus()
{
  for( G4int i=0; i<types; i++ )
    theEmitted[i]->GetStatus();
  theInternalEmitter->GetStatus();  
}

G4String AgataInternalEmission::GetEventHeader( G4double unitLength )
{
  char aLine[128];
  G4String eventHeader = G4String("");
  
  if( this->IsStartOfEvent() ) {
    // First writes out what is common to the cascade (recoil velocity and source position)
    // writes out beta and direction only when they may change event-by-event
    if( ( this->IsRecoilDiffuse() && this->GetEmitterBetaAverage() ) || this->IsBetaDiffuse() ) {
      if( this->GetEmitterVelocity().mag2() > 0. ) {
        G4ThreeVector recoildir  = this->GetEmitterVelocity().unit();
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, this->GetEmitterBeta(), recoildir.x(), recoildir.y(), recoildir.z() );
      }
      else {
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, 0., 0., 0., 1. );
      }
      eventHeader += G4String(aLine);
    }

    // writes out source position only when it may change event-by-event
    if( this->IsSourceDiffuse() || this->IsSourceMoving() ) {
      G4ThreeVector position = this->GetEmitterPosition();
      sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position.x()/unitLength, position.y()/unitLength, position.z()/unitLength );
      eventHeader += G4String(aLine);
    }
  }
  else {  // following events
    // For the following particles, writes out source position only when it has changed
    if( this->GetCascadeOrder() > 0 && this->IsSourceMoving() && this->GetCascadeDeltaTime() > 0.) {
      G4ThreeVector position1 = this->GetEmitterPosition();
      sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position1.x()/unitLength, position1.y()/unitLength, position1.z()/unitLength );
      eventHeader += G4String(aLine);
    }

    // writes out the time difference between the emission of two particles
    // in the cascade (or the time for the first particle) only when it is
    // non-zero
    if( (this->IsSourceLongLived()) && (this->GetCascadeDeltaTime() > 0.) ) {
      G4double cascadeTime = this->GetCascadeTime();
      sprintf(aLine, "%5d  %9.3f\n", -103, cascadeTime/ns );
      eventHeader += G4String(aLine);
    }
  }
  return eventHeader;  
}

G4String AgataInternalEmission::GetParticleHeader( const G4Event* evt, G4double /*unitLength*/, G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", -partType, epart/unitEnergy, direction.x(), direction.y(), direction.z(), evt->GetEventID());
  return G4String(aLine);

}


#include "AgataRunAction.hh"
#include "AgataAnalysis.hh"
void AgataInternalEmission::SetTargetEvents( G4int value )
{
  G4int targetEvents = value;
  
  if( targetEvents < 0 ) {
    G4cout << " Warning! Setting target event number to zero." << G4endl;
    return;
  }
  
  G4RunManager * runManager = G4RunManager::GetRunManager();
  AgataRunAction* theRun    = (AgataRunAction*)runManager->GetUserRunAction();
  
  G4bool doWrite    = theRun->DoWrite();
  G4bool doAnalysis = theRun->GetTheAnalysis()->IsEnabled();
  
  if( doWrite )
    theRun->EnableWrite( false );
  if( doAnalysis )
    theRun->GetTheAnalysis()->EnableAnalysis( false );  
    
  // need a fake run to initialize properly the cascade multiplicity
#ifndef G4V10
  runManager->BeamOn(0);
#else
  runManager->BeamOn(1); // BeamOn(0) is not enough with G4.10 to initialize properly the cascade multiplicity 
#endif

  runManager->SetRunIDCounter( theRun->GetRunNumber() );
  // restore status
  if( doWrite )
    theRun->EnableWrite( true );
  if( doAnalysis )
    theRun->GetTheAnalysis()->EnableAnalysis( true );  

  G4cout << " Processing " << targetEvents << " events..." << G4endl;
  
  runManager->BeamOn(targetEvents*cascadeMult);
}

/////////////////////////////////////////////////////////////////////////
// The Messenger
// generates main command directory, plus an additional beamOn command
/////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataInternalEmissionMessenger::AgataInternalEmissionMessenger(AgataInternalEmission* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of the event generation.");
  
  directoryName = name + "/run/";
  
  commandName = directoryName + "beamOn";
  aLine = commandName.c_str();
  SetTargetEventsCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetTargetEventsCmd->SetGuidance("Process multiple events from the input file.");
  SetTargetEventsCmd->SetGuidance("Required parameters: 1 integer.");
  SetTargetEventsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataInternalEmissionMessenger::~AgataInternalEmissionMessenger()
{
  delete   myDirectory;
  delete   SetTargetEventsCmd;
}

void AgataInternalEmissionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == SetTargetEventsCmd ) {
    myTarget->SetTargetEvents(SetTargetEventsCmd->GetNewIntValue(newValue));
  }
}

