#ifdef ANCIL
#include "AgataAncillaryAida.hh"
#include "AgataAncillaryCluster.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

//#include "G4Material.hh"
//#include "G4Box.hh"
//#include "G4LogicalVolume.hh"
//#include "G4ThreeVector.hh"
//#include "G4Transform3D.hh"
//#include "G4RotationMatrix.hh"
//#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
//#include "G4VisAttributes.hh"
//#include "G4Colour.hh"
//#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryAida::AgataAncillaryAida(G4String path, G4String name )
{
  G4String iniPath = path;
  
  dirName     = name;

  // files needed by AgataAncillaryCluster
  G4String solidName = G4String("aisolid");
  G4String clustName = G4String("aiclust");
  G4String angleName = G4String("aieuler");
  G4String wallsName = G4String("aiwalls");
  G4String matName   = G4String("Silicon");
  
  // members inherited from AgataAncillaryScheme
  ancSD       = NULL;
  ancName     = G4String("AIDA");
  ancOffset   = 17000;
  
  numAncSd = 0;
  
  // finally, instance a "Cluster" object
  theCluster = new AgataAncillaryCluster( iniPath, name, solidName, clustName, angleName, wallsName, ancName, matName );
  
}

AgataAncillaryAida::~AgataAncillaryAida()
{}

G4int AgataAncillaryAida::FindMaterials()
{
  return theCluster->FindMaterials(ancName);
  return 0;
}

void AgataAncillaryAida::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif 

  G4String name   = G4String("/anc/Aida");
  G4String HCname = G4String("AidaCollection");

  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, name, HCname, offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }
  theCluster->SetSensitiveDetector(ancSD);
}

void AgataAncillaryAida::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theCluster->GetDetectorConstruction();
}


void AgataAncillaryAida::Placement()
{
  theCluster->Placement();
  return;
}


void AgataAncillaryAida::ShowStatus()
{
  theCluster->ShowStatus(ancName);
}

void AgataAncillaryAida::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  theCluster->WriteHeader(outFileLMD, unit);
}

#endif
