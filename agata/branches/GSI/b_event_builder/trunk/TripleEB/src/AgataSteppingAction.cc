#include "AgataSteppingAction.hh"
#include "G4Step.hh"

void AgataSteppingAction::UserSteppingAction(const G4Step* theStep)
{
  G4int stepNumber = theStep->GetTrack()->GetCurrentStepNumber();
  if(stepNumber > 98) {
      G4cout << "UserSteppingAction: Too many steps (" << stepNumber << ")  ==> track killed" << G4endl;
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);
  }
}
