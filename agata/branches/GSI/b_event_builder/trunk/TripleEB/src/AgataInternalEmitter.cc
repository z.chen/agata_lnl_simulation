#include "AgataInternalEmitter.hh"

#include "G4RunManager.hh"
#include <string>
#include <cmath>
#include "Randomize.hh"

AgataInternalEmitter::AgataInternalEmitter(G4String name)
{
  InitData();
  myMessenger = new AgataInternalEmitterMessenger(this,name);
}

AgataInternalEmitter::~AgataInternalEmitter()
{
  delete myMessenger;
}

void AgataInternalEmitter::GetStatus()
{  
  G4cout << G4endl;      
  G4cout << " Recoil velocity v/c     " << emitterBetaAverage * 100 << " %" << G4endl;
  if( emitterBetaSigma * emitterBetaAverage )  
    G4cout << " Dispersion of emitter velocity " << 100. * emitterBetaSigmaFrac << G4endl;  
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout << " Recoil direction " << std::setw(8) << emitterDirectionAverage.x()
                                 << std::setw(8) << emitterDirectionAverage.y()
                                 << std::setw(8) << emitterDirectionAverage.z() << G4endl;
  if( emitterOpAngle )
    G4cout << " Recoil opening angle    " << emitterOpAngle/deg << " deg." << G4endl;
  switch( sourceType )
  {
    case 0:
      G4cout << " Point source in position " << std::setw(10) << sourcePosition.x()/cm
                                             << std::setw(10) << sourcePosition.y()/cm
                                             << std::setw(10) << sourcePosition.z()/cm << " cm" << G4endl;
      break;
    case 1:
      G4cout << " Point source in position " << std::setw(10) << targetPosition.x()/cm
                                             << std::setw(10) << targetPosition.y()/cm
                                             << std::setw(10) << targetPosition.z()/cm << " cm" << G4endl;
      break;
    case 2:
      G4cout << " Diffused source in position " << std::setw(10) << targetPosition.x()/cm
                                                << std::setw(10) << targetPosition.y()/cm
                                                << std::setw(10) << targetPosition.z()/cm << " cm" << G4endl;
      break;
    case 3:
      G4cout << " Gaussian source in position " << std::setw(10) << targetPosition.x()/cm
                                                << std::setw(10) << targetPosition.y()/cm
                                                << std::setw(10) << targetPosition.z()/cm << " cm" << G4endl;
      break;
  }
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataInternalEmitter::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double /*unitE*/ )
{
  G4int prec = outFileLMD.precision(4);
  outFileLMD.setf(ios::fixed);

  outFileLMD  << "RECOIL " << std::setw(8) << emitterBetaAverage
                           << std::setw(8) << emitterBetaSigmaFrac
                           << std::setw(8) << emitterDirectionAverage.x()
                           << std::setw(8) << emitterDirectionAverage.y()
                           << std::setw(8) << emitterDirectionAverage.z() 
                           << std::setw(8) << emitterOpAngle/deg  << G4endl;

  outFileLMD << "SOURCE " << std::setw(8) << sourceType
                          << std::setw(8) << movingSource;
  if( sourceType == 0 ) {
    outFileLMD  << std::setw(10) << sourcePosition.x()/unitL
                << std::setw(10) << sourcePosition.y()/unitL
                << std::setw(10) << sourcePosition.z()/unitL << G4endl;
  }
  else {
    outFileLMD  << std::setw(10) << targetPosition.x()/unitL
                << std::setw(10) << targetPosition.y()/unitL
                << std::setw(10) << targetPosition.z()/unitL << G4endl;
    if( sourceType > 1 ) {
      outFileLMD  << "TARGET " 
                  << std::setw(10) << targetSize.x()/unitL
                  << std::setw(10) << targetSize.y()/unitL
                  << std::setw(10) << targetSize.z()/unitL << G4endl;
    }            
  }             
  outFileLMD.unsetf(ios::fixed);
  outFileLMD.precision(prec);
}

void AgataInternalEmitter::SetEmitterBetaAverage( G4double velo )
{
  if( velo > 0. &&  velo < 100. )
    emitterBetaAverage = velo/100.;  // velo in %, betaCM fractions of C
  else
    emitterBetaAverage = 0.;  
  G4cout << " ----> Beta = " << emitterBetaAverage * 100. << "%" << G4endl;  
}

void AgataInternalEmitter::SetEmitterBetaSigma( G4double sigma )
{
  if( emitterBetaAverage > 0. ) {
    if( sigma < 0. ){
      G4cout << " Invalid value, keeping previous value " << 
			          100. * emitterBetaSigmaFrac << "%" << G4endl;
    }
    else if( sigma < 33. )   //> chosen so that 3*sigma = 100%
      emitterBetaSigmaFrac = sigma/100.;  // sigma fractions of betaCM
    else {
      emitterBetaSigmaFrac = 0.33;
      G4cout << " -----> Value out of range, setting to maximum value " << 
			          100. * emitterBetaSigmaFrac  << "%" << G4endl;
    }													    
    G4cout << " -----> Sigma (fractions of beta) = " << 100. * emitterBetaSigmaFrac << "%" << G4endl;	    
    emitterBetaSigma = emitterBetaSigmaFrac * emitterBetaAverage;					    
    G4cout << " -----> Sigma of beta distribution (sigma * beta) = " << emitterBetaSigma << G4endl;	    
  }
  else {
    emitterBetaSigmaFrac = 0.;
    G4cout << " Beta is zero, change beta before!" << G4endl;
  }  
}

////////////////////
// The Messenger
////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataInternalEmitterMessenger::AgataInternalEmitterMessenger(AgataInternalEmitter* pTarget,G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/recoil/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of emitting recoil.");
  
  commandName = directoryName + "beta";
  aLine = commandName.c_str();
  SetBetaCmd = new G4UIcmdWithADouble(aLine, this);  
  SetBetaCmd->SetGuidance("Define recoil v/c.");
  SetBetaCmd->SetGuidance("Required parameters: 1 double (v/c in %).");
  SetBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "sigma";
  aLine = commandName.c_str();
  SetBetaSigmaCmd = new G4UIcmdWithADouble(aLine, this);  
  SetBetaSigmaCmd->SetGuidance("Define sigma of the gaussian distribution of recoil v/c.");
  SetBetaSigmaCmd->SetGuidance("Required parameters: 1 double (sigma in % of v/c).");
  SetBetaSigmaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "openingAngle";
  aLine = commandName.c_str();
  SetAngleCmd = new G4UIcmdWithADouble(aLine, this);  
  SetAngleCmd->SetGuidance("Define opening angle for the recoil cone.");
  SetAngleCmd->SetGuidance("Required parameters: 1 double (half-opening of the cone in degrees).");
  SetAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "gaussAngle";
  aLine = commandName.c_str();
  GaussAngleCmd = new G4UIcmdWithABool(aLine, this);  
  GaussAngleCmd->SetGuidance("Set opening angle for the recoil cone to a gaussian distribution.");
  GaussAngleCmd->SetGuidance("Required parameters: none.");
  GaussAngleCmd->SetParameterName("gaussAngle",true);
  GaussAngleCmd->SetDefaultValue(true);
  GaussAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "uniformAngle";
  aLine = commandName.c_str();
  UnifoAngleCmd = new G4UIcmdWithABool(aLine, this);  
  UnifoAngleCmd->SetGuidance("Set opening angle for the recoil cone to a uniform distribution.");
  UnifoAngleCmd->SetGuidance("Required parameters: none.");
  UnifoAngleCmd->SetParameterName("gaussAngle",true);
  UnifoAngleCmd->SetDefaultValue(false);
  UnifoAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "direction";
  aLine = commandName.c_str();
  SetRecoilCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetRecoilCmd->SetGuidance("Define a fixed direction of the recoil.");
  SetRecoilCmd->SetGuidance("Required parameters: 3 double (defining the versor).");
  SetRecoilCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "thetaPhi";
  aLine = commandName.c_str();
  SetThPhiCmd = new G4UIcmdWithAString(aLine, this);  
  SetThPhiCmd->SetGuidance("Define a fixed direction of the recoil.");
  SetThPhiCmd->SetGuidance("Required parameters: 2 double (polar and azimutal angles of the recoil direction in degrees).");
  SetThPhiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "sourceType";
  aLine = commandName.c_str();
  SetSourceCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetSourceCmd->SetGuidance("Define behaviour of the source.");
  SetSourceCmd->SetGuidance(" 0 --> point source, chosen position");
  SetSourceCmd->SetGuidance(" 1 --> point source, target position");
  SetSourceCmd->SetGuidance(" 2 --> uniformly diffused source (over target)");
  SetSourceCmd->SetGuidance(" 3 --> diffused source, gaussian distribution in target");
  SetSourceCmd->SetGuidance("Required parameters: 1 integer (0--3).");
  SetSourceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "positionOfSource";
  aLine = commandName.c_str();
  SetPositionCmd = new G4UIcmdWith3Vector(aLine, this);  
  SetPositionCmd->SetGuidance("Define initial position of the source.");
  SetPositionCmd->SetGuidance("Required parameters: 3 double (source position coordinates in mm).");
  SetPositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataInternalEmitterMessenger::~AgataInternalEmitterMessenger()
{
  delete myDirectory;
  delete SetBetaCmd;
  delete SetBetaSigmaCmd;
  delete SetAngleCmd;
  delete SetPositionCmd;
  delete SetRecoilCmd;
  delete SetThPhiCmd;
  delete SetSourceCmd;
  delete GaussAngleCmd;
  delete UnifoAngleCmd;
}

void AgataInternalEmitterMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetBetaCmd ) {
    myTarget->SetEmitterBetaAverage(SetBetaCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetBetaSigmaCmd ) {
    myTarget->SetEmitterBetaSigma(SetBetaSigmaCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetEmitterOpAngle(SetAngleCmd->GetNewDoubleValue(newValue));
  }
  if( command == GaussAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetGaussAngle(GaussAngleCmd->GetNewBoolValue(newValue));
  }
  if( command == UnifoAngleCmd ) {
    ((AgataEmitter*)(myTarget))->SetGaussAngle(UnifoAngleCmd->GetNewBoolValue(newValue));
  }
  if( command == SetRecoilCmd ) {
    ((AgataEmitter*)(myTarget))->SetEmitterDirection(SetRecoilCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetThPhiCmd ) {
    G4double th, ph;
    sscanf( newValue, "%lf %lf", &th, &ph);
    th *= deg;
    ph *= deg;
    G4ThreeVector direzione( sin(th)*cos(ph), sin(th)*sin(ph), cos(th) );
    ((AgataEmitter*)(myTarget))->SetEmitterDirection(direzione);
  }
  if( command == SetSourceCmd ) {
    ((AgataEmitter*)(myTarget))->SetSourceType(SetSourceCmd->GetNewIntValue(newValue));
  }
  if( command == SetPositionCmd ) {
    ((AgataEmitter*)(myTarget))->SetSourcePosition(SetPositionCmd->GetNew3VectorValue(newValue));
  }
}
