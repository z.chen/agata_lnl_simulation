#include "AgataDetectorConstruction.hh"
#include "AgataPhysicsList.hh"
//#include "myTestPhysicsList.hh"
#include "AgataAnalysis.hh"
#include "AgataGeneratorAction.hh"
#include "AgataRunAction.hh"
#include "AgataEventAction.hh"
#include "AgataSteppingAction.hh"
#include "AgataGeneratorOmega.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIdirectory.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "Randomize.hh"
#include <time.h>

#ifdef G4VIS_USE
#ifdef G4V47
#include "G4VisExecutive.hh"
#else
#include "AgataVisManager.hh"
#endif
#endif

using namespace CLHEP;

void  decodeLine(int, char **);   // decoding of command line
#ifdef GASP
G4int   gtype   = 1;                // which geometry
#else
G4int   gtype   = 0;                // which geometry
#endif
G4String ancType = "1 0";           // which ancillary
G4int   genType = 0;                // which event generator ( 0, 1 --> AgataStd; 2 --> user defined)
G4int   batch   = 0;                // points to macro name for batch mode
#ifdef NARRAY
G4bool  hadrons = true;            // physics list generates hadrons
#else
G4bool  hadrons = false;            // physics list generates hadrons
#endif
G4bool  lowEner = true;             // physics list uses low-energy interaction for gammas/electrons
G4bool  lowEnHa = true;             // physics list uses low-energy interaction for hadrons
G4bool  usePola = false;            // photon polarization is considered
                                    // NB polarization overrides LECS!
G4bool  useLECS = false;            // considers Compton profile (non-std Geant distribution!)
G4bool  volume  = false;            // enables geantinos for special purposes (volume and solid angle calculations)
G4String iniPath = "./";             // path of the detector configuration files

G4long seed = 0;
G4int runNo = 0;

G4bool  interno = true;
G4bool  userDef = false;

int main(int argc,char** argv) {

  if(argc >1)
    decodeLine(argc, argv);

  if( seed )
    HepRandom::setTheSeed(seed);//changes the seed of the random engine
   
  if( runNo <= 0 )
    runNo = 0;  
    
  // try to avoid some known conflicts
  if( !lowEner ) {
    useLECS = false;
    G4cout << " ---> Standard interaction set chosen, disabling Compton profile." << G4endl;
  }
  if( usePola ) {
    useLECS = false;
    G4cout << " ---> Polarization of the gammas considered, disabling Compton profile." << G4endl;
  }
  if( userDef ) {
    genType = 2; 
  }
  else if( interno ) {
    genType = 0;
  }   
  else {
    genType = 1;
  }
    

#ifdef G4VIS_USE
  // Visualization, if you choose to have it!
#ifdef G4V47
  G4VisManager* visManager = new G4VisExecutive;
#else
  G4VisManager* visManager = new AgataVisManager;
#endif
  visManager->Initialize();
#endif

  // The main directory for Agata UI commands
  const char *aLine;
  G4String commandName;
  G4String directoryName;
#ifdef GASP
  directoryName  = "Gasp";
#else  
#ifdef CLARA
  directoryName  = "Clara";
#else  
#ifdef POLAR
  directoryName  = "Polar";
#else  
#ifdef DEIMOS
  directoryName  = "Deimos";
#else  
#ifdef NARRAY
  directoryName  = "NArray";
#else  
  directoryName  = "Agata";
#endif  
#endif  
#endif  
#endif  
#endif  
  G4UIdirectory* pAgataUIDirectory;
  commandName = "/" + directoryName + "/";
  pAgataUIDirectory = new G4UIdirectory(commandName);
  commandName = directoryName + " simulation control.";
  aLine = commandName.c_str();
  pAgataUIDirectory->SetGuidance(aLine);
  
  directoryName = "/" + directoryName;

  // Run manager
  G4RunManager * runManager = new G4RunManager;

 // UserInitialization classes 
  AgataPhysicsList*            pAgataPhysics  = new AgataPhysicsList(directoryName, hadrons, lowEner, lowEnHa, usePola, useLECS);
//  myTestPhysicsList* pAgataPhysics  = new myTestPhysicsList();
  runManager->SetUserInitialization( pAgataPhysics );
  
  // The on-line analysis class 
  AgataAnalysis*               pAgataAnalysis = new AgataAnalysis(directoryName);

 // UserInitialization classes 
  AgataDetectorConstruction*   pAgataDetector = new AgataDetectorConstruction(gtype, ancType, iniPath, volume, directoryName);
  runManager->SetUserInitialization( pAgataDetector );

  // UserAction classes 
  AgataGeneratorAction*        pAgataGenerator   = new AgataGeneratorAction(iniPath, genType, hadrons, usePola, directoryName);
  AgataRunAction*              pAgataRunAction   = new AgataRunAction( pAgataAnalysis, volume, directoryName );
  AgataEventAction*            pAgataEventAction = new AgataEventAction( pAgataAnalysis, directoryName );

  
  if( runNo ) {
    AgataGeneratorOmega *pDummyGen = new AgataGeneratorOmega();
    runManager->SetUserAction( pDummyGen );
    runManager->BeamOn(0);  // this is needed to initialize the run number
    runManager->SetRunIDCounter( runNo );
  }

  runManager->SetUserAction( pAgataGenerator );
  runManager->SetUserAction( pAgataRunAction );  
  runManager->SetUserAction( pAgataEventAction );
  
// Uncomment if infinite loops appear
  //AgataSteppingAction*  pAgataStepping = new AgataSteppingAction();
  //runManager->SetUserAction( pAgataStepping );
  
  //Initialize G4 kernel
  runManager->Initialize();
      
  pAgataDetector->ShowStatus();    
  pAgataGenerator->GetStatus();    
  pAgataEventAction->ShowStatus();    
  pAgataRunAction->ShowStatus();    
  pAgataAnalysis->ShowStatus(); 
  
  //get the pointer to the User Interface manager 
  G4UImanager * UI = G4UImanager::GetUIpointer();  

  if(!batch) {                    // Define (G)UI terminal for interactive mode 
    G4UIsession * session = 0;
    session = new G4UIterminal(new G4UItcsh);      

    UI->ApplyCommand("/control/verbose 2");    
    UI->ApplyCommand("/run/verbose 2");    
    UI->ApplyCommand("/hits/verbose 0");

    session->SessionStart();
    delete session;
  }
  else {                          // Batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[batch];
    UI->ApplyCommand(command+fileName);
  }

  delete runManager;

#ifdef G4VIS_USE
  delete visManager;
#endif

  return 0;
}

void
decodeLine(int argc, char *argv[])
{
  int nn = 0, ok = 0;
  if (argc > 1) {
    nn = 1;
    ok = 1;
  }
  else {
    ok = 0;
  }
  while(nn < argc && ok == 1) {
    ok = 0;
#ifndef CLARA
    if(!strcmp(argv[nn], "-g")) {
      if(++nn < argc) gtype = atoi(argv[nn]);
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }
#endif
#ifndef GASP
#ifndef CLARA
#ifndef POLAR
#ifndef DEIMOS
#ifndef NARRAY
    if(!strcmp(argv[nn], "-a")) {
      G4int ij, ik, nAnc, nAnc1, *aaType, shift = 0;
      char aLine[256];
      if(++nn < argc) {
        nAnc = atoi(argv[nn]);
	nAnc1 = nAnc;
	if( nAnc <= 0 ) {
          printf("Invalid value!\n");
	  exit(-1);
	}
	aaType = new G4int[nAnc];
	for( ij=0; ij<nAnc1; ij++ ) {
	  if(++nn < argc) {
	    aaType[ij+shift] = atoi(argv[nn]);
	    for( ik=0; ik<ij+shift; ik++ ) {
	      if( aaType[ik] == aaType[ij+shift] ) {
	        printf("Warning! Skipped repeated ancillary #%d ...\n", aaType[ij+shift]);
		nAnc--;
		shift--;
		continue;
	      } 
	    }
	  }
	  else {
            printf("Not enough data!!!\n");
	    exit(-1);
	  }
	}
	sprintf(aLine, "%d", nAnc);
	ancType = G4String(aLine);
	for( ij=0; ij<nAnc; ij++ ) {
	  sprintf(aLine, " %d", aaType[ij]);
	  ancType = ancType + G4String(aLine);
	}
        //G4cout << " AncilString is " << ancType << G4endl;
	//exit(-1);
      }
      else {
        printf("Not enough data\n");
	exit(-1);
      }
      ok = 1;
    }
#endif
#endif
#endif
#endif
#endif
    if(!strcmp(argv[nn], "-b")) {
      if(++nn < argc) batch = nn;
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }
    if(!strcmp(argv[nn], "-n")) {
      hadrons = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-noLE")) {
      lowEner = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-noLowH")) {
      lowEnHa = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-p")) {
      usePola = true;
      ok = 1;
    }
#ifdef G4LECS
    if(!strcmp(argv[nn], "-C")) {
      useLECS = true;
      ok = 1;
    }
#endif    
    if(!strcmp(argv[nn], "-Vol")) {
      volume = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Ext")) {
      interno = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Gen")) {
      userDef = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Path")) {
      if(++nn < argc)  {
        char *path;
        path = (char*)calloc(strlen(argv[nn]) + 1, 1);
        strcpy(path,  argv[nn]);
        G4int length = strlen(path);
        if( path[length-1] != '/' )
          iniPath = G4String(path) + "/";
        else    
          iniPath = G4String(path);
      }
      ok = 1;
    }
    if(!strcmp(argv[nn], "-seed")) {
      seed=time(0); //returns time in seconds as an integer
      ok = 1;
    }
    if(!strcmp(argv[nn], "-run")) {
      if(++nn < argc) runNo = atoi(argv[nn]);
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }
    if(!strcmp(argv[nn], "-h")) {
      ok = -1;
    }
    nn++;
  }

  if(ok < 1) {
    if(ok == 0 && argc > 1) printf("\nInvalid switch %s\n", argv[nn-1]);
    printf("\nUsage: %s  [-g -n -noLE -C -p -Vol -Ext -b -Path -h]\n\n", argv[0]);
#ifdef GASP
    printf("    -g   index    select GASP configuration:\n");
    printf("         1 --> Configuration I (BGO Inner Ball)\n");
    printf("         2 --> Configuration I (BGO Inner Ball) + n-Ring\n");
    printf("         3 --> Configuration II with lead collimator\n");
    printf("         4 --> Configuration II without lead collimator\n");
    printf("         5 --> Configuration II with n-Detectors and without lead collimator\n");
    printf("         6 --> Configuration II with n-Ring and without lead collimator\n");
    printf("         7 --> Configuration II with big lead collimator\n");
    printf("         8 --> Configuration I (BGO Inner Ball) + ISIS\n");
    printf("         9 --> Configuration I (BGO Inner Ball) + ISIS + n-Ring\n");
    printf("        10 --> Configuration II with big lead collimator + EUCLIDES\n");
#else        
#ifdef POLAR
    printf("    -g   index    select geometry (0=Polarimeter, 1=Clover)\n");
#else
    printf("    -g   index    select geometry (0=AGATA, 1=SHELL, 2=Simple)\n");
#endif
#endif
#ifndef GASP
#ifndef CLARA
#ifndef POLAR
#ifndef DEIMOS
#ifndef NARRAY
    printf("    -a  N_anc n_1 ... n_N    select ancillary devices\n");
#endif
#endif
#endif
#endif
#endif
    printf("    -n            use also neutrons, protons, ... \n");
    printf("    -noLE         use the standard treatment of the interactions for gamma/e-/e+\n");
    printf("    -noLowH       use the standard treatment of the interactions for hadrons\n");
#ifdef G4LECS
    printf("    -C            consider the Compton profile\n");
#endif
    printf("    -p            consider the linear polarization of the photons\n");
    printf("    -Vol          enables the use of geantinos for volume and solid angle calculation\n");
    printf("    -Ext          reads the event sequence from file\n");
    printf("    -Gen          uses user-supplied event generation\n");
    printf("    -b file.mac   runs in batch mode executing file.mac\n");
    printf("    -Path <dir>   sets base directory to <dir>\n");
    printf("    -seed         uses current time as seed for random number engine\n");
    printf("    -run r        sets first run number to r\n");
    printf("    -h            help, print this list\n");
    if(argc > 1 || ok < 0) exit(-1);
  }

}
