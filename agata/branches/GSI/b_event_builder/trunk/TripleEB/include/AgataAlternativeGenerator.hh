//////////////////////////////////////////////////////////////
/// This class is used only as an example of "non-standard"
/// event generator (opposite to the "standard" one for
/// the Agata code). User should provide his own code!!!
//////////////////////////////////////////////////////////////

#ifndef AgataAlternativeGenerator_h
#define AgataAlternativeGenerator_h 1

#include "AgataGeneratorAction.hh"
#include "globals.hh"

using namespace std;

class G4ParticleGun;
class G4Event;

class AgataAlternativeGenerator : public AgataGeneration
{
  public:
    AgataAlternativeGenerator();    
   ~AgataAlternativeGenerator();

  private:
    G4ParticleGun*              particleGun;

  public:
    void BeginOfRun   () {};
    void EndOfRun     () {};
    
  public:
    void BeginOfEvent () {};
    void EndOfEvent   () {};
    
  public:
    void GetStatus    () {};
    
  public:
    void PrintToFile  ( std::ofstream &/*outFileLMD*/, G4double=1.*mm, G4double=1.*keV ) {};
    
  public:
    inline G4String GetBeginOfEventTag ()                   { return G4String(""); };
    inline G4String GetEventHeader     ( G4double = 1.*mm ) { return G4String(""); };
    
  public:
           G4String GetParticleHeader  ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
    
  public:
    inline G4bool   IsStartOfEvent () { return true;  };
    inline G4bool   IsEndOfEvent   () { return true;  };
    inline G4bool   IsAbortRun     () { return false; }; 
    
  public:
    inline G4String GetAbortMessage() { return G4String(""); };
    
  public:
    inline G4int    GetCascadeMult () { return 1; };
    
  public:
    void     GeneratePrimaries ( G4Event* );
    
};

#endif
