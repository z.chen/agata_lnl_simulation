//////////////////////////////////////////////////////////////////////////////////////
/// This class registers the processes needed to treat the interaction of several 
/// particles with matter. Several possibilities are offered. The main processes
/// are:
/// photon --> photoelectric absorption, Compton scattering, pair production,
///             Rayleigh scattering
/// electron, positron --> multiple scattering, ionization, bremmstrahlung
/// neutron, antineutron --> elastic and inelastic scattering, capture
/// charged ions --> multiple scattering, ionization
//////////////////////////////////////////////////////////////////////////////////////
#ifndef AgataPhysicsList_h
#define AgataPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

using namespace std;

class AgataPhysicsListMessenger;

class AgataPhysicsList: public G4VUserPhysicsList
{
  public:
    AgataPhysicsList(G4String, G4bool, G4bool, G4bool, G4bool, G4bool);
   ~AgataPhysicsList();


  private:
    G4bool hadrons;        //> true: hadrons can be used (and processes are registered)
    G4bool lowEner;        //> true: low-energy processes are used for gammas (default choice)
    G4bool lowEnHa;        //> true: low-energy processes are used for hadrons
    G4bool usePola;        //> true: use processes for polarized photons
    G4bool useLECS;        //> true: considers the finite momentum distribution of the
                           //> electron in Compton and Rayleigh scatterings

  //////////////////////////////////////////////////
  /// Energy cuts for several particles
  /////////////////////////////////////////////////
  private:
    G4double cutForGamma;
    G4double cutForElectron;
    G4double cutForPositron;
    G4double cutForNeutron;
    G4double cutForProton;

  private:
    AgataPhysicsListMessenger* myMessenger;

  /////////////////////////////////////////////////
  /// This method is needed by G4VUserPhysicsList
  /////////////////////////////////////////////////
  public:
    void ConstructProcess();
    
  public:
    inline G4bool IsHadron()   { return hadrons; };
    inline G4bool IsLowEner()  { return lowEner; };
    inline G4bool IsPolar()    { return usePola; };
    inline G4bool IsLecs()     { return useLECS; };
    
  public:
    void SetGammaCut     ( G4double );
    void SetElectronCut  ( G4double );
    void SetPositronCut  ( G4double );
    void SetNeutronCut   ( G4double );
    void SetProtonCut    ( G4double );
    
  protected:
    void ConstructParticle();     //> Construct particles and physics
    void SetCuts();               //> Sets cuts for particles

  ////////////////////////////////////////////////////////////////////
  /// These methods Construct physics processes and register them
  ////////////////////////////////////////////////////////////////////
  protected:
    void ConstructEMStandard();
    void ConstructHadronsEMStandard();
    void ConstructHadronsEMStopp();
    void ConstructHadronsElastic();
    void ConstructHadronsInelastic();
    void ConstructGeneral();

  ///////////////////////////////////////////////////////////////////////
  /// In case users implement their own event generator (Geant4-based),
  /// additional physics processes should be registered via this method
  /// (meaning that the user should implement it!!!)
  ///////////////////////////////////////////////////////////////////////
  protected:
    void ConstructAdditionalProcesses();
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;

class AgataPhysicsListMessenger: public G4UImessenger
{
  public:
    AgataPhysicsListMessenger(AgataPhysicsList*, G4String, G4bool);
   ~AgataPhysicsListMessenger();
    
  private:
    AgataPhysicsList*          myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithADouble*        SetGammaCutCmd;
    G4UIcmdWithADouble*        SetElectronCutCmd;
    G4UIcmdWithADouble*        SetPositronCutCmd;
    G4UIcmdWithADouble*        SetNeutronCutCmd;
    G4UIcmdWithADouble*        SetProtonCutCmd;
            
  private:
    G4bool hadrons;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};



#endif

