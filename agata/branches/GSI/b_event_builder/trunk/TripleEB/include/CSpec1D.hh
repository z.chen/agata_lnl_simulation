#ifndef CSpec1D_h
#define CSpec1D_h 1

class CSpec1D
{
  public:
      CSpec1D(int nch = 1024, int nsp = 1);
     ~CSpec1D();

  private:
      int  nSpec;
      int  nChan;
      int *pData;
     
  public:
      void         reNew(int nch = 1024, int nsp = 1);
      void         erase();
      inline void  set  (const int val, const int ch, const int sp = 0);
      inline void  Set  (const int val,       int ch, const int sp = 0);
      inline int   get  (               const int ch, const int sp = 0);
      inline void  incr (               const int ch, const int sp = 0);
      inline void  Incr (                     int ch, const int sp = 0);
             void  list ();
             void  write (const char *, int=true);
             void  writeA(const char *, int=true);
      inline int   GetNumberOfSpectra ();
      inline int   GetNumberOfChannels();
};

inline void CSpec1D::set(const int val, const int ch, const int sp)
{
  if(sp < 0 || sp >= nSpec) return;
  if(ch < 0 || ch >= nChan) return;
  *(pData + sp*nChan + ch) = val;
}

inline void CSpec1D::Set(const int val, int ch, const int sp)
{
  if(sp < 0 || sp >= nSpec) return;
  if(ch <  0) ch = 0;
  if(ch >= nChan) ch = nChan-1;
  *(pData + sp*nChan + ch) = val;
}

inline int CSpec1D::get(int ch, const int sp)
{
  if(sp < 0 || sp >= nSpec) return 0;
  if(ch < 0 || ch >= nChan) return 0;
  return *(pData + sp*nChan + ch);
}

inline void CSpec1D::incr(const int ch, const int sp)
{
  if(sp < 0  || sp >= nSpec) return;
  if(ch <= 0 || ch >= nChan) return;
  pData[nChan*sp + ch]++;
}

inline void CSpec1D::Incr(int ch, const int sp)
{
  if(sp < 0 || sp >= nSpec) return;
  if(ch <  0) ch = 0;
  if(ch >= nChan) ch = nChan-1;
  pData[nChan*sp + ch]++;
}

inline int CSpec1D::GetNumberOfSpectra()
{
  return nSpec;
}

inline int CSpec1D::GetNumberOfChannels()
{
  return nChan;
}

#endif
