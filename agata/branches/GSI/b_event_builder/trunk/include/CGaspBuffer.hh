#ifndef CGaspBuffer_h
#define CGaspBuffer_h 1

#include <vector>
#include <fstream>

using namespace std;

class CGaspBuffer
{
  public:
    CGaspBuffer();
   ~CGaspBuffer();

  private:
    unsigned short int *pData;

  private:
    int  currentRecord;
    int  bufferPosition;

  private:
    int  runNumber;
    bool firstHeader;

  private:
    fstream fout;

  private:
    int     maxFileSize;
    int     fileNumber;
    
  public:
    void Reset        ();
    void AddToBuffer  ( unsigned short int *, int );  
    
  public:
    void BeginOfRun   ( int );
    void EndOfRun     ();    

  public:
    void SetRunNumber    ( int );
    void SetMaxFileSize  ( int );  
    
  private:
    void FlushBuffer  ();
    void ResetBuffer  ();
    void RecordHeader ();
    
  private:
    void OpenFile();
    void CloseFile();
    void SplitFile();
    
  public:
    int  GetFileSize  ();
    
};

inline void CGaspBuffer::SetRunNumber( int number )
{
  runNumber = number;
}

inline int CGaspBuffer::GetFileSize()
{
  return fout.tellp();
}

#endif
