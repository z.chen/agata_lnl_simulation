#ifndef AgataDetectorArray_h
#define AgataDetectorArray_h 1

#include "globals.hh"
#include "G4Point3D.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include <vector>

using namespace std;

class CConvexPolyhedron;
class G4Tubs;
class G4Polycone;
class G4IntersectionSolid;
class G4LogicalVolume;
class G4VisAttributes;
class G4VPhysicalVolume;

////////////////////////////////////////////////////////////////////////////////////
/// This class handles the information about the encapsulated detectors of AGATA
/// (size, ...) together with the objects actually generating the detectors
/// within the simulation. It is basically a struct, but was written as a class
/// where all members are public since the compiler does not accept to allocate
/// a std::vector with a structure. It is also used for the cryostats.
////////////////////////////////////////////////////////////////////////////////////
class CpolyhPoints
{
  // default empty creator/destructor
  public:
     CpolyhPoints() {};
    ~CpolyhPoints() {};
  
  ///////////////////////////////
  /// All members are public!!!  
  ///////////////////////////////
  public:  
    G4int  whichGe;        //> kind of crystal (for the cryostats: cluster they belong to)
    G4int  whichCrystal;   //> for the cryostats: crystal they belong to
    G4int  whichWall;      //> for the cryostats: kind of part composing the cryostat
    
  public:  
    G4int  npoints;        //> number of points composing the solid
    G4int  nfaces;         //> number of faces composing the solid
    
  public:  
    std::vector<G4Point3D> vertex;  //> 3D-vertexes of the solid
    std::vector<G4int>     ifaces;  //> points (of vertex) forming each face
    
  ////////////////////
  /// Cylinder data
  ///////////////////
  public:  
    G4double  tubr, tubR, tubL;      //> inner and outer radius, length
    G4double  tubX, tubY, tubZ;      //> position of the cylinder with respect to the polyhedron (for the intersection)
    G4double  thick;                 //> distance from the front face to the coaxial hole
    
  public:
    G4bool    cylinderMakesSense;    //> false: dimensions are not compatible with a real coaxial crystal
                                     //>       (thus the cylinder will not be built)  

  ///////////////////
  /// Capsule data
  //////////////////
  public:
    G4double  capThick;              //> thickness
    G4double  capSpace;              //> crystal-capsule spacing

  public:
    G4bool    makeCapsule;           //> true: encapsulation will be built

  ///////////////////////////
  /// passivated areas size
  ///////////////////////////
  public:
    G4double  passThick1;            //> at the back of the crystal
    G4double  passThick2;            //> around the coaxial hole
    
  public:
    G4Point3D centerFace1;           //> center of front face
    G4Point3D centerFace2;           //> center of back face  
    
  public:  
    G4double  zFace1;                //> z-coordinate of the center of the front face
    G4double  zFace2;                //> z-coordinate of the center of the back face
    G4double  zCenter;               //> z-coordinate of the center of the crystal
    
  ///////////////////
  /// segmentation
  ///////////////////
  public:  
    G4int                  nslice;   //> number of slices in which the crystal is divided
    std::vector<G4double>  zSliceI;  //> z-coordinate of the slices at the crystal axis
    std::vector<G4double>  zSliceO;  //> z-coordinate of the slices at the outer surface
    
  public:  
    G4double  minR;                  //> minimum radius of a cylinder surrounding the polyhedron
    
  public:  
    G4double  colx, coly, colz;      //> RGB components defining the colour
    
  /////////////////////////////////////////////////////
  /// Objects defining the detector in the simulation
  /////////////////////////////////////////////////////
  public:  
    CConvexPolyhedron   *pPoly;      //> original polyhedron
    G4Polycone          *pCoax;      //> cylinder 
    G4IntersectionSolid *pCaps;      //> their intersection
    G4LogicalVolume     *pDetL;      //> its logical
    
  public:  
    G4Polycone          *pTubs1;     //> cylinder (passive area behind detector)     
    G4IntersectionSolid *pCaps1;     //> intersection with polyhedron		    
    G4LogicalVolume     *pDetL1;     //> its logical				    
    G4VPhysicalVolume   *pDetP1;     //> passivated area (back)			    
   
  public:  
    G4Polycone          *pCoax2;     //> cylinder (passive area at the coaxial hole) 
    G4LogicalVolume     *pDetL2;     //> its logical				    
    G4VPhysicalVolume   *pDetP2;     //> passivated area (coax)			    
    
  public:  
    G4VisAttributes     *pDetVA;     //> visualization attributes
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/// This "structure" (written as a class as explained above) handles the transformations needed to
/// place the AGATA clusters into space.
////////////////////////////////////////////////////////////////////////////////////////////////////
class CeulerAngles
{
  // default empty creator/destructor
  public:
     CeulerAngles() {};
    ~CeulerAngles() {};

  // all members are public  
  public:  
    G4int     whichGe;       //> type of cluster which should be placed   
    G4int     numPhys;       //> number of cluster which should be placed

  public:  
    G4double  ps, th, ph;    //> Rotation angles; they are needed since one cannot easily extract
                             //> them from the rotation matrix

  public:
    G4ThreeVector trasl;     //> traslation

  public:
    G4RotationMatrix rotMat; //> rotation matrix

  public:
    G4Transform3D *pTransf;  //> full transformation
};

class G4AssemblyVolume;
////////////////////////////////////////////////////////////////////////////////////////////////////
/// This "structure" (written as a class as explained above) handles the transformations needed to
/// place the crystals within an AGATA cluster
////////////////////////////////////////////////////////////////////////////////////////////////////
class CclusterAngles
{
  // default empty creator/destructor
  public:
     CclusterAngles() {};
    ~CclusterAngles() {};

  // all members are public  
  public:
    G4int                      whichClus;   //> type of cluster  
    
  public:
    G4int                      nsolids;     //> number of crystals arranged to form a cluster 
    std::vector<CeulerAngles>  solids;      //> each component of this array contains the information
                                            //> needed to place a single crystal within the cluster
    
  public:
    G4int                      nwalls;      //> number of parts composing the cryostat  
#ifdef ANTIC
    G4int                      nantic;      //> number of parts composing the antiCompton 
#endif
    
  public:
    G4AssemblyVolume*          pAssV;       //> pointer to the G4AssemblyVolume forming the cluster
};


#include "AgataDetectorConstruction.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Plane3D.hh"
#include "G4Normal3D.hh"

class G4Material;
class AgataDetectorArrayMessenger;
class AgataDetectorAncillary;
class AgataDetectorConstruction;

/////////////////////////////////////////////////////////////////////
/// This class handles the construction of the actual AGATA array
////////////////////////////////////////////////////////////////////
class AgataDetectorArray : protected AgataDetectorConstructed
{
  public:
    AgataDetectorArray(G4int, G4String);
    AgataDetectorArray(G4String, G4String);
    ~AgataDetectorArray();
    
  private:
    AgataDetectorAncillary      *theAncillary;
    AgataDetectorArrayMessenger *myMessenger;
    
  /////////////////////////////////////////////////
  /// Files from which actual geometry is read 
  ///////////////////////////////////////////////// 
  private:
    G4String                    iniPath;     //> directory where the files are located
    G4String                    eulerFile;   //> angles and positions to place the clusters into space
    G4String                    solidFile;   //> shape of the crystals
    G4String                    sliceFile;   //> segmentation
    G4String                    wallsFile;   //> cryostats
#ifdef ANTIC
    G4String                    anticFile;   //> antiCompton
#endif
    G4String                    clustFile;   //> arrangement of the crystals and cryostats within a cluster
    
  /////////////////////////////////////
  /// materials (pointers and names)
  ///////////////////////////////////// 
  private:
    G4Material                  *matCryst;   //> crystals
    G4Material                  *matWalls;   //> cryostats and encapsulation
    G4Material                  *matHole;    //> vacuum within the cryostat
#ifdef ANTIC
    G4Material                  *matAnti;    //> antiCompton
#endif
    
  private:
    G4String                    matCrystName; //> crystals
    G4String                    matWallsName; //> cryostats and encapsulation
    G4String                    matHoleName;  //> vacuum within the cryostat
#ifdef ANTIC
    G4String                    matAntiName;  //> antiCompton
#endif
     
  ///////////////////////////////////////////////////////////////////////
  /// structures needed to store geometry data during the construction  
  ///////////////////////////////////////////////////////////////////////
  private:
    std::vector<CeulerAngles>   euler;        //> angles and positions to place the clusters into space
    std::vector<CpolyhPoints>   pgons;        //> shape of the crystals
    std::vector<CpolyhPoints>   walls;        //> cryostats
#ifdef ANTIC
    std::vector<CpolyhPoints>   antic;        //> anticompton
#endif
    std::vector<CclusterAngles> clust;        //> arrangement of the crystals and cryostats within a cluster
    std::vector<CpolyhPoints>   capsO;        //> encapsulation (outer size)
    std::vector<CpolyhPoints>   capsI;        //> encapsulation (inner size)

  private:
    G4int                       nEuler;       //> number of clusters composing the array
    G4int                       nPgons;       //> number of different crystal shapes within the array
    G4int                       nClAng;       //> number of crystals composing a cluster
    G4int                       nWalls;       //> number of cryostat parts within a cluster
#ifdef ANTIC
    G4int                       nAntic;       //> number of anticompton parts within a cluster
#endif
    G4int                       maxPgons;     //> maximum index of crystal shapes

  private:
    std::vector<G4int>          crystType;    //> lookup table detector number --> crystal shape

  private:
    G4int                       nWlTot;       //> total number of cryostat parts within the array
    G4int                       maxSolids;    //> maximum number of solids within a cluster
#ifdef ANTIC
    G4int                       nAcTot;       //> total number of anciCompton parts within the array
    G4int                       maxAntic;    //> maximum number of antiCompton parts within a cluster
#endif
    
  /////////////////////////////////////////////////
  /// structures needed to build the segmentation
  ////////////////////////////////////////////////
  private:
    std::vector<CpolyhPoints>   pgSegLl;      //> segments on lower Left  side of edges
    std::vector<CpolyhPoints>   pgSegLu;      //> segments on upper Left  side of edges
    std::vector<CpolyhPoints>   pgSegRl;      //> segments on lower Right side of edges
    std::vector<CpolyhPoints>   pgSegRu;      //> segments on upper Right side of edges

  private:
    std::vector<G4int>          nSegments;
    std::vector<G4int>          tSegments;
    G4int                       totSegments;
    G4int                       nSeg;
    
  private:
    std::vector<G4double>       segVolume;    //> the volume of the (composite) segment
    std::vector<G4Point3D>      segCenter;    //> the center of mass of the (composite) segment

  private:
    G4int                       stepFactor;    //> integration step for the calculation of segment volume
    G4bool                      stepHasChanged;//> true: integration step was changed and segment volumes
                                               //>       should be recomputed
   
  ////////////////////////////////////////////
  /// size of the equivalent germanium shell
  ////////////////////////////////////////////
  private:
    G4double                    arrayRmin;     //> inner radius
    G4double                    arrayRmax;     //> outer radius
    
  ///////////////////////////////////////////
  /// rotation applied to the whole array
  //////////////////////////////////////////
  private:
    G4double                    thetaShift;   //> theta
    G4double                    phiShift;     //> phi

  ///////////////////////////////////////////
  /// traslation applied to the whole array
  //////////////////////////////////////////
  private:
    G4ThreeVector               posShift;   
    
  /////////////////
  /// some flags 
  //////////////// 
  private:
    G4bool                      usePassive;   //> true: passive areas of the crystals will be generated
    G4bool                      drawReadOut;  //> true: segments will be visualized
    G4bool                      useAncillary; //> true: ancillary detectors will be constructed
    G4bool                      makeCapsule;  //> true: encapsulation will be generated
    G4bool                      useCylinder;  //> true: the intersection with the cylinder will be considered

#ifdef ANTIC
  private:
    AgataSensitiveDetector*     acSD;
#endif


  //////////////////////////////
  ///////// Methods  ///////////
  //////////////////////////////
  private:
    void     InitData( G4String, G4String );

  //////////////////////////
  /// read the input files
  /////////////////////////
  private:
    void      ReadEulerFile();
    void      ReadSolidFile();
    void      ReadSliceFile();
    void      ReadWallsFile();
#ifdef ANTIC
    void      ReadAnticFile();
#endif
    void      ReadClustFile();
  
  //////////////////////////////////////////////////////////////
  /// look for the materials starting from the material names
  /////////////////////////////////////////////////////////////
  private:
    G4int     FindMaterials();
    
  /////////////////////////////////////////////////////////
  /// Construct the various elements composing the array
  /////////////////////////////////////////////////////////  
  private:
    void      ConstructGeCrystals  ();    
    void      ConstructTheCapsules ();    
    void      ConstructTheClusters ();    
    void      ConstructTheWalls    ();
#ifdef ANTIC
    void      ConstructTheAntic    ();
#endif
    
  ////////////////////////////////
  /// placement of the elements  
  ////////////////////////////////
  private:
    void      PlaceTheClusters     ();

  //////////////////////////////////
  /// Construction of the segments
  //////////////////////////////////
  
  private:
    //////////////////////////////
    /// Construct the segments
    //////////////////////////////
    void      ConstructSegments        ();
    /////////////////////////////////////////////////////////////////////////////////
    /// Calculate the vertexes of the segments starting from the original polyhedra
    /////////////////////////////////////////////////////////////////////////////////
    G4int     CalculateSegments        ( G4int );
    ///////////////////////////////////
    /// Checks for possible overlaps
    //////////////////////////////////
    G4int     CheckOverlap             ( G4int, G4int, G4int );
    /////////////////////////////////////////////////////////////////////////////////////////
    /// Calculates volume and center of the segments (each of them composed of more parts!)
    //////////////////////////////////////////////////////////////////////////////////////////
    void      CalculateVolumeAndCenter ( G4int, G4int, G4int, G4double );
    ////////////////////////////////////////////////////////////////////////////////////////
    /// Calculates the intersection between a plane and a line passing through two points
    ///////////////////////////////////////////////////////////////////////////////////////
    G4Point3D XPlaneLine               ( const G4Plane3D &vv, const G4Point3D &pA,  const G4Point3D &pB );
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// Calculates segment number (corresponding to a given detector and position relative to the crystal)
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    G4int     GetSegmentNumber         ( G4int, G4ThreeVector );    

  //////////////////////////////////
  /// writes out the information
  /////////////////////////////////
  private:
    void WritePositions               ( std::ofstream &outFileLMD, G4double=1.*mm );
    void WriteSegmentPositions        ( std::ofstream &outFileLMD, G4double=1.*mm );
    void WriteCrystalPositions        ( std::ofstream &outFileLMD, G4double=1.*mm );
    void WriteCrystalTransformations  ( std::ofstream &outFileLMD, G4double=1.*mm );
  
  ////////////////
  /// placement
  ///////////////
  public:
    void Placement ();

  //////////////////////////////////////////////
  /// public interface to the private method!
  ////////////////////////////////////////////////
  public:
    G4int     GetSegmentNumber         ( G4int, G4int, G4ThreeVector );    
  
  //////////////////////////////////
  /// writes out the information
  /////////////////////////////////
  public:
    void WriteHeader                  ( std::ofstream &outFileLMD, G4double=1.*mm );
    void WriteSegmentAngles           ( G4String, G4int = 0 );
    void WriteCrystalAngles           ( G4String );
    
  public:
    G4int GetCrystalType ( G4int );
     
  //////////////////////////////////////////////////
  ///////////// public methods for the messenger
  ////////////////////////////////////////////////// 
  public:       
    void SetSolidFile           ( G4String );
    void SetWallsFile           ( G4String );
#ifdef ANTIC
    void SetAnticFile           ( G4String );
#endif
    void SetAngleFile           ( G4String );
    void SetSliceFile           ( G4String );
    void SetClustFile           ( G4String );

  public:       
    void SetDetMate             ( G4String );
    void SetWallsMate           ( G4String );

  public:      
    void SetThetaShift          ( G4double );
    void SetPhiShift            ( G4double );
    void SetPosShift            ( G4ThreeVector );

  public:      
    void SetMakeCapsules        ( G4bool );
    void SetUseCylinder         ( G4bool );
    void SetUsePassive          ( G4bool );
    void SetDrawReadOut         ( G4bool );
    void SetUseAncillary        ( G4bool );
    void SetWriteSegments       ( G4bool );
    
  public:       
    void SetStep                ( G4int  ); 
     
  public:
    void ShowStatus ();
    
  ///////////////////////////////////////////
  //////////////// inline "get" methods
  ///////////////////////////////////////////
  public:
    inline G4double              GetThetaShift      () { return thetaShift;  };
    inline G4double              GetPhiShift        () { return phiShift;    };
    
  public:
    inline G4String              GetEulerFile       () { return eulerFile;   };
    inline G4String              GetSolidFile       () { return solidFile;   };
    inline G4String              GetSliceFile       () { return sliceFile;   };
    inline G4String              GetWallsFile       () { return wallsFile;   };
#ifdef ANTIC
    inline G4String              GetAnticFile       () { return anticFile;   };
#endif
    
  public:
    inline G4bool                GetDrawReadOut     () { return drawReadOut; };

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class AgataDetectorArrayMessenger: public G4UImessenger
{
  public:
    AgataDetectorArrayMessenger(AgataDetectorArray*);
   ~AgataDetectorArrayMessenger();
    
  private:
    AgataDetectorArray*        myTarget;
    
  private:
    G4UIcmdWithAString*        SetSolidCmd;
    G4UIcmdWithAString*        SetAngleCmd;
#ifdef ANTIC
    G4UIcmdWithAString*        SetAnticCmd;
#endif
    G4UIcmdWithAString*        SetWallsCmd;
    G4UIcmdWithAString*        SetClustCmd;
    G4UIcmdWithAString*        SetSliceCmd;
    G4UIcmdWithAString*        DetMatCmd;
    G4UIcmdWithAString*        WalMatCmd;
    G4UIcmdWithAString*        RotateArrayCmd;
    G4UIcmdWith3Vector*        TraslateArrayCmd;
    G4UIcmdWithAString*        WriteAnglesCmd;
    G4UIcmdWithAString*        WriteCryAnglesCmd;
    G4UIcmdWithABool*          EnableCylCmd;
    G4UIcmdWithABool*          DisableCylCmd;
    G4UIcmdWithABool*          EnablePassiveCmd;
    G4UIcmdWithABool*          DisablePassiveCmd;
    G4UIcmdWithABool*          DontDrawReadOutCmd;
    G4UIcmdWithABool*          DrawReadOutCmd;
    G4UIcmdWithABool*          EnableCapsulesCmd;
    G4UIcmdWithABool*          DisableCapsulesCmd;            
    G4UIcmdWithABool*          EnableAncillaryCmd;
    G4UIcmdWithABool*          DisableAncillaryCmd;            
    G4UIcmdWithAnInteger*      SetStepCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};

#endif
