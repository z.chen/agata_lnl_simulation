#include <stdio.h>

using namespace std;

int main(int argc, char *argv[])
{  
  if(argc < 2)
    {
      printf(" main needs argument (the setup) \n");
      return 0;
    }

  const int line_with_distance = 4;// counting from 0
  const int line_euler = 7;// counting from 0
  const int n_distances = 7;
  const float target_offset[n_distances] = {0,50,80,100,120,150,200};
  const float target_offsety[n_distances] = {10,20,20,20,30,40,50};
  const int nlines = 23;
  char setup[100];
  sprintf(setup,argv[1]);

  char line[50][200] = {"/Agata/file/workingPath /u/cdomingo/simulations/agata_gsi_v3/SimulationResults/Builder/S2pPerformance/auto_eff",
			"/Agata/file/enableLM",
			"/Agata/file/verbose 1",
			"/Agata/file/info/outputMask 11100110",
			"/Agata/detector/traslateArray 0.",
			"/Agata/detector/rotateArrayYZX 175. 30. -17.0",
			"/Agata/detector/solidFile ./A180/A180solid.list",
			"/Agata/detector/angleFile ./A180/A180euler",
			/*"/Agata/detector/wallsFile ./A180/A180wallsS2p.list",*/
			"/Agata/detector/wallsFile ./A180/A180walls.list",
			/*"/Agata/detector/clustFile ./A180/A180clustS2p.list",*/
			"/Agata/detector/clustFile ./A180/A180clust.list",
			"/Agata/detector/sliceFile ./A180/A180slice.list",
			"/Agata/detector/enableCapsules",
			"/Agata/detector/wallThickness 4.0",
			/*"/Agata/detector/chamberRadius 230.0",*/
			"/Agata/detector/chamberMaterial Aluminium",
			"/Agata/detector/pipeRadius 45.0",
			"/Agata/detector/targetMaterial Iron",
			"/Agata/detector/targetSize 62.25 62.25 500.0",
			"/Agata/detector/targetPosition 0 0 0",
			"/Agata/detector/update",
			"/Agata/generator/gamma/energy 1000",
			"/Agata/generator/recoil/beta 43",
			"/Agata/generator/recoil/sourceType 4",
			"/Agata/run/beamOn 500000"};
  
  
  FILE * pFile;
  char filename[200];
  for(int i= 0;i<n_distances;i++)
    {
      sprintf(filename,"/u/cdomingo/simulations/agata_gsi_v3/b_event_builder/macros_prespec/mc/auto_mc/mc_d%d.mac",i);
      pFile = fopen (filename,"w");

      for(int j=0;j<nlines;j++)
	{
	  if(j==line_euler)
	    fprintf(pFile, "%s%s.list\n",line[line_euler],setup);
	  else if(j==line_with_distance)
	    fprintf (pFile, "%s %.0f %.0f\n",line[j],target_offsety[i],target_offset[i]);
	  else 
	    fprintf (pFile, "%s\n",line[j]);
	}

      fclose (pFile);
    }

  return 0;

}
