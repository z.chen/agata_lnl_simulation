#ifndef AgataAncillaryTrace1_h
#define AgataAncillaryTrace1_h 1

class G4Material;
class G4Tubs;
class G4Box;
class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VisAttributes;
class AgataAncillaryTrace1Messenger;
class AgataSensitiveDetector;

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
//#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

class AgataAncillaryTrace1 : public AgataAncillaryScheme
{
  //friend class AgataDetectorConstruction;
  
  public:
    AgataAncillaryTrace1(G4String);
    ~AgataAncillaryTrace1();

  public:
    virtual void Placement();

  private:
    G4String iniPath;
    
  // annular detector size
  private:
    G4double innRad;
    G4double outRad;
    G4double passTh;
    G4double thickn;
    
    
    
  // new ancillary
  private:
    G4double innZ;
    G4double outZ;
    G4double barLenZ;
    G4double barLenY;
    G4double barLenX;
    G4double barLenXdE;
    G4double endcapSide;
    G4double deThick;
    G4double eThick;
    G4double AlThick;
    G4double kapThick;
    G4double CuThick;
    G4double PCBThick;
    
    
    G4double dZ;
    G4int NPadZ;
    G4int NSector;
    G4int nPadZ;
    G4int NCrown;
    
    G4ThreeVector positionBar[40],positionBardE[40],positionBarAl[32],positionBarAsics[32],positionBarPinCon[32],positionBarChip[32],
    positionECE[4],positionECE_Box[4], positionECPad[36],positionECStrip[38],positionECStripA[38], positionECAsics[4], positionBarPinConEC[4],positionBarChipEC[4];
     

  // absorber size
  private:
    G4double innRaA;
    G4double outRaA;
    G4double thickA;

  // target size
  private:
    G4ThreeVector sizeT;
  
  // segmentation
  private:
    G4int nSect;
    G4int nRing;

  private:
    G4double dPhi;
    G4double dRad;
    
  //positions
  private:
    G4ThreeVector positionD;  
    G4ThreeVector positionA;  
    G4ThreeVector positionT;
    
    
    
  // target rotation
  private:
    G4double psi;
    G4double theta;
    G4double phi;    
  
  // materials
  private:
    G4String                 matDNam;
    G4Material              *matAnnul;
    G4String                 matANam;
    G4Material              *matAbso;
    G4String                 matTNam;
    G4Material              *matTarg;
    G4String                 matAsNam;
    G4Material              *matAsic;
    G4String                 matPlNam;
    G4Material              *matPlas;

  private:
    //AgataSensitiveDetector* ancSD;  //DSSD
    AgataSensitiveDetector* ancSDdE;//SiPad dE
    AgataSensitiveDetector* ancSDE; //SiPad E
    
    
    
/*
  private:
    G4Tubs*            pAnnS;
    G4LogicalVolume*   pAnnL;
    G4Tubs*            pSegS;
    G4LogicalVolume*   pSegL;
    G4VPhysicalVolume* pSegP;

  private:
    G4Tubs*            pAbsS;
    G4LogicalVolume*   pAbsL;

  private:
    G4Box*             pTarS;
    G4LogicalVolume*   pTarL;
*/
  private:
    AgataAncillaryTrace1Messenger* myMessenger;

  //private:
  //  AgataDetectorConstruction* theDetector;

  private:
    void PlaceDetector ();
    void PlaceAbsorber ();
    void PlaceTarget   ();

  public:
    G4int FindMaterials ();
    void GetDetectorConstruction ();
    void   InitSensitiveDetector   ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm) {};
    void   ShowStatus              ();
    
  public:
    void SetNSect     ( G4int );
    void SetNRing     ( G4int );
    void SetAnnulRmin ( G4double ); 
    void SetAnnulRmax ( G4double ); 
    void SetPassTh    ( G4double );
    void SetThickn    ( G4double );
    void SetPositionD ( G4ThreeVector ); 
    
  public:
    void SetAbsoMate  ( G4String ); 
    void SetAbsoRmin  ( G4double ); 
    void SetAbsoRmax  ( G4double ); 
    void SetThickA    ( G4double );
    void SetPositionA ( G4ThreeVector ); 
    
  public:
    void SetTargMate  ( G4String ); 
    void SetTargXSize ( G4double ); 
    void SetTargYSize ( G4double ); 
    void SetThickT    ( G4double );
    void SetRotationT ( G4ThreeVector ); 
    void SetPositionT ( G4ThreeVector ); 

  public:
    G4int GetSegmentNumber( G4int, G4int, G4ThreeVector );

  public:
    inline G4int GetCrystalType( G4int ) { return -1; };

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;

class AgataAncillaryTrace1Messenger: public G4UImessenger
{
  public:
    AgataAncillaryTrace1Messenger(AgataAncillaryTrace1*);
   ~AgataAncillaryTrace1Messenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataAncillaryTrace1*    myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAString*        SetAnnulSizeCmd;
    G4UIcmdWithAString*        SetSegmentsCmd;
    G4UIcmdWithADouble*        SetThicknCmd;
    G4UIcmdWithADouble*        SetPassThCmd;
    G4UIcmdWith3Vector*        SetPositionDCmd;
    G4UIcmdWithAString*        SetAbsoMateCmd;
    G4UIcmdWithAString*        SetAbsoSizeCmd;
    G4UIcmdWithADouble*        SetThickACmd;
    G4UIcmdWith3Vector*        SetPositionACmd;
    G4UIcmdWithAString*        SetTargMateCmd;
    G4UIcmdWithAString*        SetTargSizeCmd;
    G4UIcmdWithADouble*        SetThickTCmd;
    G4UIcmdWith3Vector*        SetRotationTCmd;
    G4UIcmdWith3Vector*        SetPositionTCmd;
            
};

#endif
