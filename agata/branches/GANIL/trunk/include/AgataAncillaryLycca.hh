////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryLycca_h
#define AgataAncillaryLycca_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "DiamondLycca.h"
#include "DiamondTgt.h"
#include "SiDetector.h"
#include "SiTgtDetector.h"
#include "CsIDetector.h"
#include "FPDetector.h"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillarylyccaMessenger;

class AgataAncillaryLycca : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryLycca(G4String, G4String);
    ~AgataAncillaryLycca();


  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName1, matName2, matName3, matName4 ;
    G4Material  *matLycca1, *matLycca2, *matLycca3, *matLycca4 ;
  
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();
  public:
//    void DiamondLycca(G4double) ;
      DiamondLycca  *DiamondLyccaDet ;
      DiamondTgt  *DiamondTgtDet ;
      SiDetector  *SiDet ;
      SiTgtDetector *SiTgtDet ;
      CsIDetector  *CsIDet ;
      FPDetector  *FPDet ;
      G4double defTgtDetDist ;

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

//  public:
//    inline G4String GetName () { return G4String("SHELL"); };
    
  ////////////////////////////////
  //// Methods for the messenger
  ////////////////////////////////

};

#endif
