#ifndef AgataDefaultGenerator_h
#define AgataDefaultGenerator_h 1

#include "AgataEmitter.hh"

#include "AgataGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

// For AGATA-PRISMA
#include "G4ParticleGun.hh"

using namespace std;

class G4ParticleGun;
class G4Event;
class AgataEmitted;
class AgataEmitter;

///////////////////////////////////////////////////////////////////////////////////
/// This abstract class handles the emission of particles. Its main task is to
/// tell the AgataDefaultGenerator class what will be the next emitted particle
/// and what will be the characteristics of the source (position, velocity).
///////////////////////////////////////////////////////////////////////////////////
class AgataEmission
{
  public:
     AgataEmission(){};
    ~AgataEmission(){};

  protected:
    AgataEmitted** theEmitted;   //> Each AgataEmitted object is needed to provide
                                 //> the transformations CM->Lab

  protected:
    AgataEmitter*  theEmitter;   //> this member is needed only to inline the get/set methods 
                                 //> concerning the actual implementation of the emitter class (internal or external)
        
  protected:
    /////////////////////////////////////////////////
    /// Characteristics of the next emitted particle
    /////////////////////////////////////////////////
    G4String      emittedName;
    G4double      emittedEnergy;
    G4ThreeVector emittedDirection;
    G4ThreeVector emittedPolarization;
    G4bool        polarizedEmitted;

  protected:
    G4bool        hadrons;       //> true: also hadrons are emitted
    G4bool        usePolar;      //> true: also polarized photons are possible

  protected:
    G4bool        startOfEvent;  //> true: begin of event (including the "true" multiplicity)
    G4bool        endOfEvent;    //> true: end of event (including the "true" multiplicity)

  protected:
    G4bool        abortRun;      //> true: current run will be aborted immediately after the current particle
    G4String      abortMessage;  //> message displayed when current run is aborted
    
  protected:
    G4double      cascadeTime;       //> current time (calculated from the moment startOfEvent==true) 
    G4double      cascadeDeltaTime;  //> time difference between the current and the previous particle
    G4int         cascadeMult;       //> total multiplicity of the current cascade
    G4int         cascadeOrder;      //> order of the current particle in the current cascade

  /////////////////////////////////////////////////////////////////////
  /// Virtual methods which should be implemented
  ////////////////////////////////////////////////////////////////////
  public:
    virtual void BeginOfRun   ()			                                     = 0;
    virtual void EndOfRun     ()			                                     = 0;
    
  public:
    virtual void BeginOfEvent ()			                                     = 0;
    virtual void EndOfEvent   ()			                                     = 0;
    
  public:
    virtual void NextParticle ()			                                     = 0;
    
  public:
    virtual void GetStatus    ( )			                                     = 0;
    virtual void PrintToFile  ( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV ) = 0;
    
  public:
    virtual G4String GetBeginOfEventTag()                                                    = 0;
    virtual G4String GetEventHeader    ( G4double = 1.*mm )                                  = 0;
    virtual G4String GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV )   = 0;

  ///////////////////////////////////////////////////////////////////////////////
  //// Inline methods
  //////////////////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////
  /// Characteristics of the emitted particle
  ////////////////////////////////////////////////////////
  public:
    inline G4String      GetEmittedName         () { return emittedName;	 };
    inline G4double      GetEmittedEnergy       () { return emittedEnergy;	 };
    inline G4ThreeVector GetEmittedDirection    () { return emittedDirection;	 };
    inline G4ThreeVector GetEmittedPolarization () { return emittedPolarization; };

  ///////////////////////////////////////////////////////////////////////////////
  /// Characteristics of the emitter
  ////////////////////////////////////////////////////////////////////////////////
  public:
    inline G4double      GetEmitterBeta        ()  { return theEmitter->GetEmitterBeta();	 };
    inline G4double      GetEmitterBetaAverage ()  { return theEmitter->GetEmitterBetaAverage(); };
    inline G4ThreeVector GetEmitterVelocity    ()  { return theEmitter->GetEmitterVelocity();	 };
    inline G4ThreeVector GetEmitterPosition    ()  { return theEmitter->GetEmitterPosition();	 };

  public:
    inline G4bool        IsBetaDiffuse        ()  { return theEmitter->IsBetaDiffuse();      };   
    inline G4bool        IsSourceDiffuse      ()  { return theEmitter->IsSourceDiffuse();    };   
    inline G4bool        IsRecoilDiffuse      ()  { return theEmitter->IsRecoilDiffuse();    };     
    inline G4bool        IsSourceMoving       ()  { return theEmitter->IsSourceMoving();     };
    inline G4bool        IsSourceLongLived    ()  { return theEmitter->IsSourceLongLived();  };

  ///////////////////////////////////////////////////////////////////
  /// Information on the current cascade
  /////////////////////////////////////////////////////////////////////
  public:
    inline G4double      GetCascadeTime       ()  { return cascadeTime; 	};
    inline G4double      GetCascadeDeltaTime  ()  { return cascadeDeltaTime;	};
    inline G4int         GetCascadeOrder      ()  { return cascadeOrder;	};
    inline G4int         GetCascadeMult       ()  { return cascadeMult; 	};

  public:
    inline G4bool        IsStartOfEvent       ()  { return startOfEvent;	};				      
    inline G4bool        IsEndOfEvent         ()  { return endOfEvent;  	};				      
    inline G4bool        IsPolarized          ()  { return polarizedEmitted;	};

  public:
    inline G4bool        IsAbortRun()             { return abortRun;            }; 
    inline G4String      GetAbortMessage()        { return abortMessage;        }; 

};

class AgataInternalEmission;
class AgataExternalEmission;

/////////////////////////////////////////////////////////////////////////////
/// This class provides the implementation of AgataGeneration
/// handling the actual particle emission within the simulation through
/// an instance of G4ParticleGun.
////////////////////////////////////////////////////////////////////////////
class AgataDefaultGenerator : public AgataGeneration
{

  public:
    AgataDefaultGenerator(G4String, G4bool, G4bool, G4bool, G4String);    
   ~AgataDefaultGenerator();

// For AGATA-PRISMA:
    G4ParticleGun* GetParticleGun() {return particleGun;}

  private:
    G4ParticleGun*                        particleGun;
    
  private:
    G4bool                                internal;       //> true: built-in generator; false: events decoded from file
    G4bool                                hadrons;        //> true: hadrons can be emitted
    G4bool                                usePola;        //> true: polarized gammas can be considered

  private:
    G4String                              iniPath;        //> directory where the data files are stored
    
  private:
    AgataEmission*                        theEmission;
    AgataInternalEmission*                theInternalEmission;
    AgataExternalEmission*                theExternalEmission;
    
  /////////////////////////////////////////////////////////////
  /// This method is needed by G4VUserPrimaryGeneratorAction 
  ////////////////////////////////////////////////////////////  
  public:
    void GeneratePrimaries(G4Event*);

  ///////////////////////////////////////////////
  /// Begin/end of run methods
  /////////////////////////////////////////////
  public:
    void BeginOfRun();
    void EndOfRun();
    
  //////////////////////////////////////////////////
  /// Begin/end of event (single particle) methods
  /////////////////////////////////////////////////
  public:
    void BeginOfEvent();
    void EndOfEvent();

  public:
  void GetEventInfo(std::ostream &eventinfo){eventinfo.clear();}
    void GetStatus   ();
    void PrintToFile ( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV  );

  //////////////////////////////////////////////////////////////
  //// Inline methods
  ///////////////////////////////////////////////////////////////
  
  public:
    inline G4bool        GetHadrons()         { return hadrons;                            };
    inline G4bool        GetPolariz()         { return usePola;                            };
    inline G4bool        IsInternal()         { return internal;                           };

  public:
    inline G4bool        IsAbortRun()         { return theEmission->IsAbortRun();          };
    inline G4String      GetAbortMessage()    { return theEmission->GetAbortMessage();     };

  public:
    inline G4bool        IsStartOfEvent()     { return theEmission->IsStartOfEvent();      };
    inline G4bool        IsEndOfEvent()       { return theEmission->IsEndOfEvent();        };      

  public:
    inline G4bool        IsRecoilDiffuse()    { return theEmission->IsRecoilDiffuse();     };   
    inline G4bool        IsSourceDiffuse()    { return theEmission->IsSourceDiffuse();     };   
    inline G4bool        IsBetaDiffuse()      { return theEmission->IsBetaDiffuse();       };     
    inline G4bool        IsSourceMoving()     { return theEmission->IsSourceMoving();      };    
    inline G4bool        IsSourceLongLived()  { return theEmission->IsSourceLongLived();   }; 

  public:
    inline G4double      GetEmitterBeta()       { return theEmission->GetEmitterBeta();        };      
    inline G4double      GetEmitterBetaAverage(){ return theEmission->GetEmitterBetaAverage(); };      
    inline G4ThreeVector GetEmitterVelocity()   { return theEmission->GetEmitterVelocity();    };
    inline G4ThreeVector GetEmitterPosition()   { return theEmission->GetEmitterPosition();    };

  public:
    inline G4double      GetCascadeTime()     { return theEmission->GetCascadeTime();      };     
    inline G4double      GetCascadeDeltaTime(){ return theEmission->GetCascadeDeltaTime(); };
    inline G4int         GetCascadeOrder()    { return theEmission->GetCascadeOrder();     };   
    inline G4int         GetCascadeMult()     { return theEmission->GetCascadeMult();      };    
    
  public:
    inline G4String      GetBeginOfEventTag() { return theEmission->GetBeginOfEventTag();  };
    
  public:
    inline G4String      GetEventHeader    ( G4double=1.*mm );
    inline G4String      GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV );

};

inline G4String AgataDefaultGenerator::GetEventHeader ( G4double unitLength )
{
  return theEmission->GetEventHeader( unitLength );  
}

inline G4String AgataDefaultGenerator::GetParticleHeader ( const G4Event* evt, G4double unitLength, G4double unitEnergy )
{
  return theEmission->GetParticleHeader( evt, unitLength, unitEnergy );  
}

#endif
