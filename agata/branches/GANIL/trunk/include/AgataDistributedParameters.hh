#ifndef _AgataDistributedParameters_
#define _AgataDistributedParameters_
#include "G4String.hh"
class  AgataDistributedParameters {
public:
  AgataDistributedParameters(){;}
  ~AgataDistributedParameters(){;}
  static G4bool resume;
  static G4String resume_dir;
  static G4String eventinfo_file;
  static G4int resume_runnb;
  static G4int resume_eventnb;
};
#endif
