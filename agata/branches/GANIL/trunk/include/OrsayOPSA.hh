////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef OrsayOPSA_h
#define OrsayOPSA_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class OrsayOPSAMessenger;

class OrsayOPSA : public AgataAncillaryScheme
{
  
public:
  OrsayOPSA(G4String, G4String);
  ~OrsayOPSA();

private:
  OrsayOPSAMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
private:
  G4String     matName;
  G4Material  *matOrsayOPSA;
  
  //////////////////////////
  /// Size of the shell
  /////////////////////////
private:
  G4double OrsayOPSAZ;
  const G4double R1=12.3*mm;
  const G4double R2=19.8*mm;
  const G4double LYSOThickness=0.5*mm;
  const G4double LYSOSide=6.0*mm;
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
public:
  G4int  FindMaterials           ();
  void   GetDetectorConstruction ();
  void   InitSensitiveDetector   ();
  void   Placement               ();

public:
  void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
  void   ShowStatus              ();

public:
  inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector );// { return 0;  };
  inline G4int GetCrystalType    ( G4int )			   { return -1; };

  //  public:
  //    inline G4String GetName () { return G4String("SHELL"); };
    
  ////////////////////////////////
  //// Methods for the messenger
  ////////////////////////////////
public:
  void SetOrsayOPSAZ( G4double value) {OrsayOPSAZ=value;}

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class OrsayOPSAMessenger: public G4UImessenger
{
public:
  OrsayOPSAMessenger(OrsayOPSA*, G4String);
  ~OrsayOPSAMessenger();
    
private:
  OrsayOPSA*    myTarget;
  G4UIdirectory*          myDirectory;
  G4UIcmdWithADouble*     SetOrsayOPSAZ;
    
public:
  void SetNewValue(G4UIcommand*, G4String);
            
};

#endif
