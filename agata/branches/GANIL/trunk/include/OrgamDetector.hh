////////////////////////////////////////////////////////
/// This class handles the Orgam array 
////////////////////////////////////////////////////////

#ifndef OrgamDetector_h
#define OrgamDetector_h 1

#include "AgataDetectorConstruction.hh"

#include "AgataSensitiveDetector.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <set>

using namespace std;

class G4Material;
class OrgamDetectorMessenger;
class AgataDetectorAncillary;

#ifndef _PlacementOfAPhaseI_
#define _PlacementOfAPhaseI_
struct PlacementOfAPhaseI {
  G4double x,y,z,phi,theta,psi;
  G4int id;
};
#endif

class OrgamDetector : protected AgataDetectorConstructed
{
public:
  OrgamDetector(G4String,G4String, G4String);
  ~OrgamDetector();

  void Placement       ();
  void  WriteHeader      ( std::ofstream &outFileLMD, G4double = 1.*mm );
  void  ShowStatus       ()                                              {;}
  G4int GetSegmentNumber ( G4int offset, G4int nGe, G4ThreeVector position );
  inline G4int GetCrystalType   ( G4int detNum )
  {return -1*(detNum+1)/(detNum+1); };
  void AddOrgamDetectors(G4String detectors);
  void AddPhaseIDetector(G4String detector);
  void AddFrameNotToBuild(G4String frames);
  void AddFrameToBuild(G4String frames);
  void ClearPhaseIDetectors(){fExtraPhaseIs.clear();}
  void ClearOrgamPhaseIDetectors(){fUsedOrgamDetectors.clear();}
  void SetMakeFrame(G4bool val) {fMakeFrame=val;}
  void SetMakeChamber(G4bool val){fMakeChamber=val;
    std::cout << "-----> Set Make Chamber to : " << fMakeChamber << std::endl;}
  G4String GetAnc(){return ancType;}
  std::map<int,G4ThreeVector> GetAllDetAngles(){return fAllDetAngles;}
private:
  AgataDetectorAncillary      *theAncillary;
  G4String                    directoryName;  //> for the command line  
  G4String                    iniPath;     //> directory where the files are located
  G4String ancType;
  G4LogicalVolume *pCrystal;
  G4LogicalVolume *pCapsule;
  G4LogicalVolume *pBGOCrystal;
  G4LogicalVolume *pBGOCapsule;
   
  G4LogicalVolume *pTronCrystal;
  G4LogicalVolume *pTronCapsule;
  G4LogicalVolume *pTronCapsuleOut;
  G4LogicalVolume *pTronBGO;
  G4LogicalVolume *pTronOuterCapsule;
  
  G4LogicalVolume *pTronArre;
  G4LogicalVolume *pTronColimator;


  G4String    matCrystalName;
  G4String    matCapsuleName;
  G4String    matAntiCompton1Name;
  G4String    matAntiCompton2Name;

  G4Material  *matCrystal;
  G4Material  *matCapsule;
  G4Material  *matAntiCompton1;
  G4Material  *matAntiCompton2;
  G4Material  *matCollimator;    

  AgataSensitiveDetector    *bgoSD;
  OrgamDetectorMessenger*  myMessenger;
  G4int FindMaterials  ();
        

  // PhaseI related
  G4LogicalVolume *GetTronCrystal();
  G4LogicalVolume *GetTronBGO();    
  G4LogicalVolume *GetTronColimator();
  G4LogicalVolume *GetTronCapsule();
  
  /* TO BE CHECKED   

     G4LogicalVolume *GetTronCapsuleOut();
     G4LogicalVolume *GetTronOuterCapsule();
     G4LogicalVolume *GetTronArre(); */
  

  std::set<int> fUsedOrgamDetectors;
  std::vector<PlacementOfAPhaseI> fExtraPhaseIs;
  //To keep track of detectorangles for header
  std::map<int,G4ThreeVector> fDetAngles;
  std::map<int,G4ThreeVector> fAllDetAngles;
  //Make or not Orgam frame
  G4bool fMakeFrame;
  std::set<int> fDontMakeThisFrame;
  G4bool fMakeChamber;
};



#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class OrgamDetectorMessenger: public G4UImessenger
{
public:
  OrgamDetectorMessenger(OrgamDetector*);
  ~OrgamDetectorMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
    
private:
  OrgamDetector*       myTarget;
    
  G4UIdirectory*             myDirectory;
  G4UIcmdWithAString*        SetUsedGePositionCmd;
  G4UIcmdWithAString*        SetFreeGePositionCmd;
  G4UIcmdWithAString*        AddMiniballCluster;
  G4UIcmdWithoutParameter*        ClearFreeGePositionCmd;
  G4UIcmdWithoutParameter*        ClearOrgamGePositionCmd;
  G4UIcmdWithABool*          SetMakeFrameCmd;
  G4UIcmdWithAString*        AddFrameNotToAdd;
  G4UIcmdWithAString*        AddFrameToAdd;
  G4UIcmdWithABool*          SetMakeChamberCmd;
}; 

#endif
