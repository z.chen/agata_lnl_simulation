//////////////////////////////////////////////////////////////////////////////////////
/// This class registers the processes needed to treat the interaction of several 
/// particles with matter. Several possibilities are offered. The main processes
/// are:
/// photon --> photoelectric absorption, Compton scattering, pair production,
///             Rayleigh scattering
/// electron, positron --> multiple scattering, ionization, bremmstrahlung
/// neutron, antineutron --> elastic and inelastic scattering, capture
/// charged ions --> multiple scattering (or G4ScreenedNuclearRecoil) , ionization
//////////////////////////////////////////////////////////////////////////////////////
#ifndef AgataPhysicsList_h
#define AgataPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"


// Hadronics
#include "G4HadronElasticProcess.hh"
#include "G4HadronInelasticProcess.hh"
#include "G4HadronCaptureProcess.hh"
#include "G4HadronFissionProcess.hh"

// Inelastic Processes
#include "G4ProtonInelasticProcess.hh"
#include "G4AntiProtonInelasticProcess.hh"
#include "G4NeutronInelasticProcess.hh"
#include "G4AntiNeutronInelasticProcess.hh"
#include "G4PionPlusInelasticProcess.hh"
#include "G4PionMinusInelasticProcess.hh"
#include "G4KaonPlusInelasticProcess.hh"
#include "G4KaonZeroSInelasticProcess.hh"
#include "G4KaonZeroLInelasticProcess.hh"
#include "G4KaonMinusInelasticProcess.hh"
#include "G4DeuteronInelasticProcess.hh"
#include "G4TritonInelasticProcess.hh"
#include "G4AlphaInelasticProcess.hh"
#include "G4LambdaInelasticProcess.hh"
#include "G4AntiLambdaInelasticProcess.hh"
#include "G4OmegaMinusInelasticProcess.hh"
#include "G4AntiOmegaMinusInelasticProcess.hh"
#include "G4SigmaMinusInelasticProcess.hh"
#include "G4AntiSigmaMinusInelasticProcess.hh"
#include "G4SigmaPlusInelasticProcess.hh"
#include "G4AntiSigmaPlusInelasticProcess.hh"
#include "G4XiZeroInelasticProcess.hh"
#include "G4AntiXiZeroInelasticProcess.hh"
#include "G4XiMinusInelasticProcess.hh"
#include "G4AntiXiMinusInelasticProcess.hh"

// Low-energy Parameterised Models: 1 to 25 GeV
#ifndef G4V10
#include "G4LElastic.hh"
#include "G4LEPionPlusInelastic.hh"
#include "G4LEPionMinusInelastic.hh"
#include "G4LEKaonPlusInelastic.hh"
#include "G4LEKaonZeroSInelastic.hh"
#include "G4LEKaonZeroLInelastic.hh"
#include "G4LEKaonMinusInelastic.hh"
// #include "G4LEProtonInelastic.hh"
#include "G4LEAntiProtonInelastic.hh"
// #include "G4LENeutronInelastic.hh"
#include "G4LEAntiNeutronInelastic.hh"
#include "G4LEDeuteronInelastic.hh"
#include "G4LETritonInelastic.hh"
#include "G4LEAlphaInelastic.hh"
#include "G4LELambdaInelastic.hh"
#include "G4LEAntiLambdaInelastic.hh"
#include "G4LEOmegaMinusInelastic.hh"
#include "G4LEAntiOmegaMinusInelastic.hh"
#include "G4LESigmaMinusInelastic.hh"
#include "G4LEAntiSigmaMinusInelastic.hh"
#include "G4LESigmaPlusInelastic.hh"
#include "G4LEAntiSigmaPlusInelastic.hh"
#include "G4LEXiZeroInelastic.hh"
#include "G4LEAntiXiZeroInelastic.hh"
#include "G4LEXiMinusInelastic.hh"
#include "G4LEAntiXiMinusInelastic.hh"
// neutrons
#include "G4LCapture.hh"
#endif

// neutrons
#include "G4LFission.hh"

// High-energy Parameterised Models: 25 GeV to 10 TeV
//  #include "G4HEPionPlusInelastic.hh"
//  #include "G4HEPionMinusInelastic.hh"
//  #include "G4HEKaonPlusInelastic.hh"
//  #include "G4HEKaonZeroInelastic.hh"
//  #include "G4HEKaonZeroInelastic.hh"
//  #include "G4HEKaonMinusInelastic.hh"
//  #include "G4HEProtonInelastic.hh"
//  #include "G4HENeutronInelastic.hh"

#ifndef G4V10
#include "G4HEAntiProtonInelastic.hh"
#include "G4HEAntiNeutronInelastic.hh"
#include "G4HELambdaInelastic.hh"
#include "G4HEAntiLambdaInelastic.hh"
#include "G4HEOmegaMinusInelastic.hh"
#include "G4HEAntiOmegaMinusInelastic.hh"
#include "G4HESigmaMinusInelastic.hh"
#include "G4HEAntiSigmaMinusInelastic.hh"
#include "G4HESigmaPlusInelastic.hh"
#include "G4HEAntiSigmaPlusInelastic.hh"
#include "G4HEXiZeroInelastic.hh"
#include "G4HEAntiXiZeroInelastic.hh"
#include "G4HEXiMinusInelastic.hh"
#include "G4HEAntiXiMinusInelastic.hh"
#endif

// // Neutron HP Models: Thermal to 19 MeV
// #include "G4NeutronHPElastic.hh"
// #include "G4NeutronHPElasticData.hh"
// #include "G4NeutronHPCapture.hh"
// #include "G4NeutronHPCaptureData.hh"
// #include "G4NeutronHPInelastic.hh"
// #include "G4NeutronHPInelasticData.hh"

// Stopping processes
#ifndef G4V10
#include "G4PiMinusAbsorptionAtRest.hh"
#include "G4KaonMinusAbsorptionAtRest.hh"
#include "G4AntiProtonAnnihilationAtRest.hh"
#endif
#include "G4AntiNeutronAnnihilationAtRest.hh"


// Generator models: HE
#include "G4TheoFSGenerator.hh"
#include "G4Evaporation.hh"
#include "G4CompetitiveFission.hh"
#ifndef G4V10_5
#include "G4FermiBreakUp.hh"
#else
#include "G4FermiBreakUpVI.hh"
#endif
#include "G4StatMF.hh"
#include "G4ExcitationHandler.hh"
#include "G4PreCompoundModel.hh"
#include "G4GeneratorPrecompoundInterface.hh"
// #include "G4QGSModel.hh"
// #include "G4QGSParticipants.hh"
//#include "G4QGSMFragmentation.hh"
#include "G4ExcitedStringDecay.hh"

// Kinetic Model
#include "G4BinaryCascade.hh"
#include "G4BinaryLightIonReaction.hh"
#include "G4TripathiCrossSection.hh"
#include "G4IonsShenCrossSection.hh"


///////////////////////////
// ElectroNuclear Physics

// photonuclear and electronuclear reaction
#include "G4PhotoNuclearProcess.hh"
#include "G4ElectronNuclearProcess.hh"
#include "G4PositronNuclearProcess.hh"

#ifndef G4V10
#include "G4GammaNuclearReaction.hh"
#include "G4ElectroNuclearReaction.hh"
#endif

// CHIPS fragmentation model
#include "G4TheoFSGenerator.hh"
#ifndef G4V10
#include "G4StringChipsParticleLevelInterface.hh"
#endif

// #include "G4QGSModel.hh"
// #include "G4GammaParticipants.hh"
// #include "G4QGSMFragmentation.hh"
#include "G4ExcitedStringDecay.hh"

// muon photonuclear reaction
#if defined G4V495 || G4V496 || G4V10
#include "G4MuonNuclearProcess.hh"
#else
#include "G4MuNuclearProcess.hh"
#endif



// To stop particles arriving at TestSphere
#include "AgataSpecialCuts.hh"

// There are cuts used to speed up screen nuclear...
#include "AgataSNCuts.hh"
// Agata reaction physics
#include "AgataReactionPhysics.hh"

class Outgoing_Beam;
class Incoming_Beam;

using namespace std;

class AgataPhysicsListMessenger;

class AgataPhysicsList: public G4VUserPhysicsList
{
  public:
  AgataPhysicsList(G4String, G4bool, G4bool, G4bool, G4bool, G4bool, 
		   G4bool usescreennuclear,G4bool intern, G4bool usrDef, G4bool scuts=false);
   ~AgataPhysicsList();


  private:
    G4bool hadrons;        //> true: hadrons can be used (and processes are registered)
    G4bool lowEner;        //> true: low-energy processes are used for gammas (default choice)
    G4bool lowEnHa;        //> true: low-energy processes are used for hadrons
    G4bool usePola;        //> true: use processes for polarized photons
    G4bool useLECS;        //> true: considers the finite momentum distribution of the
                           //> electron in Compton and Rayleigh scatterings
    G4bool interno;        //> true: built-in generator
    G4bool userDef;        //> true: built-in -Gen user defined generator
  G4bool useScreenedNuclear; //> Uses screened Coulomb scattering instead of multiplescattering 
  //for Generic Ions, slow but "real" physics
  //////////////////////////////////////////////////
  /// Energy cuts for several particles
  /////////////////////////////////////////////////
  private:
    G4double cutForGamma;
    G4double cutForElectron;
    G4double cutForPositron;
    G4double cutForMuon;
    G4double cutForNeutron;
    G4double cutForProton;

  private:
    AgataPhysicsListMessenger* myMessenger;

  /////////////////////////////////////////////////
  /// This method is needed by G4VUserPhysicsList
  /////////////////////////////////////////////////
  public:
    void ConstructProcess();
    
  public:
    inline G4bool IsHadron()   { return hadrons; };
    inline G4bool IsLowEner()  { return lowEner; };
    inline G4bool IsPolar()    { return usePola; };
    inline G4bool IsLecs()     { return useLECS; };
    
  public:
    void SetGammaCut     ( G4double );
    void SetElectronCut  ( G4double );
    void SetPositronCut  ( G4double );
    void SetNeutronCut   ( G4double );
    void SetProtonCut    ( G4double );

  void SetOutgoingBeam(Outgoing_Beam*);
  void SetIncomingBeam(Incoming_Beam*);
  AgataReactionPhysics *GetReactionPhysics() {return reacList;}
  protected:
    void ConstructParticle();     //> Construct particles and physics
    void SetCuts();               //> Sets cuts for particles

  ////////////////////////////////////////////////////////////////////
  /// These methods Construct physics processes and register them
  ////////////////////////////////////////////////////////////////////
  protected:
    void ConstructEMStandard();
    void ConstructHadronsEMStandard();
    void ConstructHadronsEMStopp();
    void ConstructHadronsElastic();
    void ConstructHadronsInelastic();
    void ConstructSpecialCuts();
  
    void ConstructGeneral();

  ///////////////////////////////////////////////////////////////////////
  /// In case users implement their own event generator (Geant4-based),
  /// additional physics processes should be registered via this method
  /// (meaning that the user should implement it!!!)
  ///////////////////////////////////////////////////////////////////////
  protected:
    void ConstructAdditionalProcesses();

  // PhotoNuclear *************************************************

#ifndef G4V10
  G4PhotoNuclearProcess thePhotoNuclearProcess;
  G4GammaNuclearReaction* theGammaReaction;      
  G4StringChipsParticleLevelInterface* theCascade_PN;
#endif
  G4TheoFSGenerator* theHEModel_PN;


  // G4QGSModel<G4GammaParticipants>* theStringModel_PN;
  // G4QGSMFragmentation theFragmentation_PN;
  G4ExcitedStringDecay* theStringDecay_PN;
  // ElectronNuclear
  G4ElectronNuclearProcess theElectronNuclearProcess;

#ifndef G4V10
  G4ElectroNuclearReaction* theElectroReaction;
#endif

  // PositronNuclear
  G4PositronNuclearProcess thePositronNuclearProcess;
  // MuNucleus
#if defined G4V495 || G4V496 || G4V10 
  G4MuonNuclearProcess theMuMinusNuclearInteraction;
  G4MuonNuclearProcess theMuPlusNuclearInteraction;
#else
  G4MuNuclearInteraction theMuMinusNuclearInteraction;
  G4MuNuclearInteraction theMuPlusNuclearInteraction;
#endif

   // Hadronics  *************************************************
  
  // Binary Cascade
  G4TheoFSGenerator* theHEModel;
  G4Evaporation* theEvaporation;
#ifndef G4V10_5
  G4FermiBreakUp* theFermiBreakUp;
#else
  G4FermiBreakUpVI* theFermiBreakUp;
#endif
  G4StatMF* theMF;
  G4ExcitationHandler* theHandler;
  G4PreCompoundModel* thePreEquilib;
  G4GeneratorPrecompoundInterface* theCascade;
  //  G4VPartonStringModel* theStringModel;
  G4BinaryCascade* theCasc;
  G4VLongitudinalStringDecay* theFragmentation;
  G4ExcitedStringDecay* theStringDecay;
  G4BinaryCascade* theCascForPi;
  // Cascade for light ions
  G4BinaryLightIonReaction* theIonCascade;
  G4TripathiCrossSection* theTripathiCrossSection;
  G4IonsShenCrossSection* theShenCrossSection;
  G4BinaryLightIonReaction* theGenIonCascade;
  
  
   // Elastic Process
   
   //G4HadronElasticProcess theElasticProcess;
   //G4HadronElasticProcess* theElasticProcess;
#ifndef G4V10
  G4HadronElasticProcess *theElasticProcess = new G4HadronElasticProcess;
  G4LElastic* theElasticModel;
#else
  G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;  
#endif

  // pi+
  G4PionPlusInelasticProcess thePionPlusInelasticProcess;
#ifndef G4V10
  G4LEPionPlusInelastic* theLEPionPlusInelasticModel;
#endif

  // pi-
  G4PionMinusInelasticProcess thePionMinusInelasticProcess;
#ifndef G4V10
  G4LEPionMinusInelastic* theLEPionMinusInelasticModel;
  G4PiMinusAbsorptionAtRest thePiMinusAbsorptionAtRest;
#endif
  
  // kaon+
  G4KaonPlusInelasticProcess theKaonPlusInelasticProcess;
#ifndef G4V10
  G4LEKaonPlusInelastic* theLEKaonPlusInelasticModel;
#endif

  // kaon0S
  G4KaonZeroSInelasticProcess theKaonZeroSInelasticProcess;
#ifndef G4V10
  G4LEKaonZeroSInelastic* theLEKaonZeroSInelasticModel;
#endif

  // kaon0L
  G4KaonZeroLInelasticProcess theKaonZeroLInelasticProcess;
#ifndef G4V10
  G4LEKaonZeroLInelastic* theLEKaonZeroLInelasticModel;
#endif

  // kaon-
  G4KaonMinusInelasticProcess theKaonMinusInelasticProcess;
#ifndef G4V10
  G4LEKaonMinusInelastic* theLEKaonMinusInelasticModel;
  G4KaonMinusAbsorptionAtRest theKaonMinusAbsorptionAtRest;
#endif
  
  // proton
  G4ProtonInelasticProcess theProtonInelasticProcess;

  // anti-proton
  G4AntiProtonInelasticProcess theAntiProtonInelasticProcess;
#ifndef G4V10
  G4LEAntiProtonInelastic* theLEAntiProtonInelasticModel;
  G4HEAntiProtonInelastic* theHEAntiProtonInelasticModel;
  G4AntiProtonAnnihilationAtRest theAntiProtonAnnihilationAtRest;
#endif

  // neutron
  //G4HadronElasticProcess theNeutronElasticProcess;
#ifndef G4V10
  G4LElastic* theNeutronElasticModel1;
#endif
  //   G4NeutronHPElastic* theNeutronElasticModel2;
  //   G4NeutronHPElasticData* theNeutronElasticData;
  G4NeutronInelasticProcess theNeutronInelasticProcess;
  //   G4NeutronHPInelastic* theNeutronInelasticModel1;
  //   G4NeutronHPInelasticData* theNeutronInelasticData1;
  G4HadronCaptureProcess theNeutronCaptureProcess;
#ifndef G4V10
  G4LCapture* theNeutronCaptureModel1;
#endif
  //   G4NeutronHPCapture* theNeutronCaptureModel2;
  //   G4NeutronHPCaptureData* theNeutronCaptureData;
  G4HadronFissionProcess theNeutronFissionProcess;
  G4LFission* theNeutronFissionModel;

  // anti-neutron
  G4AntiNeutronInelasticProcess theAntiNeutronInelasticProcess;
#ifndef G4V10
  G4LEAntiNeutronInelastic* theLEAntiNeutronInelasticModel;
  G4HEAntiNeutronInelastic* theHEAntiNeutronInelasticModel;
  G4AntiNeutronAnnihilationAtRest theAntiNeutronAnnihilationAtRest;
#endif

  // deuteron
  G4DeuteronInelasticProcess* theDeuteronInelasticProcess;
#ifndef G4V10
  G4LEDeuteronInelastic* theLEDeuteronInelasticModel;
#endif

  // triton
  G4TritonInelasticProcess* theTritonInelasticProcess;
#ifndef G4V10
  G4LETritonInelastic* theLETritonInelasticModel;
#endif
  // alpha
  G4AlphaInelasticProcess* theAlphaInelasticProcess;
#ifndef G4V10
  G4LEAlphaInelastic* theLEAlphaInelasticModel;
#endif

  // He-3
  G4HadronInelasticProcess* theHe3InelasticProcess;

  // Generic Ion
  G4HadronInelasticProcess* theGenericIonInelasticProcess;

  // lambda
#ifndef G4V10
  G4LambdaInelasticProcess theLambdaInelasticProcess;
  G4LELambdaInelastic* theLELambdaInelasticModel;
  G4HELambdaInelastic* theHELambdaInelasticModel;
#endif

  // anti-lambda
  G4AntiLambdaInelasticProcess theAntiLambdaInelasticProcess;
#ifndef G4V10
  G4LEAntiLambdaInelastic* theLEAntiLambdaInelasticModel;
  G4HEAntiLambdaInelastic* theHEAntiLambdaInelasticModel;
#endif

  // omega-
  G4OmegaMinusInelasticProcess theOmegaMinusInelasticProcess;
#ifndef G4V10
  G4LEOmegaMinusInelastic* theLEOmegaMinusInelasticModel;
  G4HEOmegaMinusInelastic* theHEOmegaMinusInelasticModel;
#endif

  // anti-omega-
  G4AntiOmegaMinusInelasticProcess theAntiOmegaMinusInelasticProcess;
#ifndef G4V10
  G4LEAntiOmegaMinusInelastic* theLEAntiOmegaMinusInelasticModel;
  G4HEAntiOmegaMinusInelastic* theHEAntiOmegaMinusInelasticModel;
#endif

  // sigma-
  G4SigmaMinusInelasticProcess theSigmaMinusInelasticProcess;
#ifndef G4V10
  G4LESigmaMinusInelastic* theLESigmaMinusInelasticModel;
  G4HESigmaMinusInelastic* theHESigmaMinusInelasticModel;
#endif

  // anti-sigma-
  G4AntiSigmaMinusInelasticProcess theAntiSigmaMinusInelasticProcess;
#ifndef G4V10
  G4LEAntiSigmaMinusInelastic* theLEAntiSigmaMinusInelasticModel;
  G4HEAntiSigmaMinusInelastic* theHEAntiSigmaMinusInelasticModel;
#endif

  // sigma+
  G4SigmaPlusInelasticProcess theSigmaPlusInelasticProcess;
#ifndef G4V10
  G4LESigmaPlusInelastic* theLESigmaPlusInelasticModel;
  G4HESigmaPlusInelastic* theHESigmaPlusInelasticModel;
#endif

  // anti-sigma+
  G4AntiSigmaPlusInelasticProcess theAntiSigmaPlusInelasticProcess;
#ifndef G4V10
  G4LEAntiSigmaPlusInelastic* theLEAntiSigmaPlusInelasticModel;
  G4HEAntiSigmaPlusInelastic* theHEAntiSigmaPlusInelasticModel;
#endif

  // xi0
  G4XiZeroInelasticProcess theXiZeroInelasticProcess;
#ifndef G4V10
  G4LEXiZeroInelastic* theLEXiZeroInelasticModel;
  G4HEXiZeroInelastic* theHEXiZeroInelasticModel;
#endif

  // anti-xi0
  G4AntiXiZeroInelasticProcess theAntiXiZeroInelasticProcess;
#ifndef G4V10
  G4LEAntiXiZeroInelastic* theLEAntiXiZeroInelasticModel;
  G4HEAntiXiZeroInelastic* theHEAntiXiZeroInelasticModel;
#endif

  // xi-
  G4XiMinusInelasticProcess theXiMinusInelasticProcess;
#ifndef G4V10
  G4LEXiMinusInelastic* theLEXiMinusInelasticModel;
  G4HEXiMinusInelastic* theHEXiMinusInelasticModel;
#endif

  // anti-xi-
  G4AntiXiMinusInelasticProcess theAntiXiMinusInelasticProcess;
#ifndef G4V10
  G4LEAntiXiMinusInelastic* theLEAntiXiMinusInelasticModel;
  G4HEAntiXiMinusInelastic* theHEAntiXiMinusInelasticModel;
#endif
  //Special cuts
  bool constructspecialcuts;


  //Agata reaction physics
  AgataReactionPhysics *reacList;
  Outgoing_Beam *BeamOut;
  Incoming_Beam *BeamIn;
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;

class AgataPhysicsListMessenger: public G4UImessenger
{
  public:
    AgataPhysicsListMessenger(AgataPhysicsList*, G4String, G4bool);
   ~AgataPhysicsListMessenger();
    
  private:
    AgataPhysicsList*          myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithADouble*        SetGammaCutCmd;
    G4UIcmdWithADouble*        SetElectronCutCmd;
    G4UIcmdWithADouble*        SetPositronCutCmd;
    G4UIcmdWithADouble*        SetNeutronCutCmd;
    G4UIcmdWithADouble*        SetProtonCutCmd;
            
  private:
    G4bool hadrons;

  public:
    void SetNewValue(G4UIcommand*, G4String);
};



#endif

