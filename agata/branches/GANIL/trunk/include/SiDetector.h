/***************************************************************************
                          SiDetector.h  -  description
                             -------------------
    begin                : Thu Sep 29 2005
    copyright            : (C) 2005 by Mike Taylor
    email                : mjt502@york.ac.uk
    
Modified by Pavel Golubev (pavel.golubev@nuclear.lu.se) 
LYCCA0 configuration implemented
 ***************************************************************************/

#ifndef SiDetector_h
#define SiDetector_h 1

#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#if defined G4V10
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#endif


class G4LogicalVolume;
class G4VPhysicalVolume;
class G4PVPlacement;
class G4Material;

class SiDetector
{
  public:

    SiDetector(G4double);
    virtual ~SiDetector();

    void createDetector(G4VPhysicalVolume *,  G4Material *);

    G4double detDist;

    G4PVPlacement *Si_phys_cpy[13];
    G4ThreeVector placement[13];
    G4LogicalVolume *Si_log;

  private:

//    void defineMaterial();
    void setAsSensitiveDetector();

    G4Material *detectorMaterial;
    G4VPhysicalVolume *Si_phys;

}; 

#endif
