#ifndef AgataDetectorConstruction_h
#define AgataDetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "G4UserLimits.hh"
#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

#include <vector>
#include <map>

using namespace std;

class G4Material;
class CConvexPolyhedron;
class G4Box;
class G4Tubs;
class G4Polycone;
class G4Sphere;
class G4IntersectionSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4VisAttributes;
class AgataSensitiveDetector;
class AgataDetectorConstructionMessenger;

///////////////////////////////////////////////////////////////////////
/// This abstract class provides members and methods to count
/// the number of detectors and clusters which have been placed.
///////////////////////////////////////////////////////////////////////
class AgataDetectorConstructed
{
  public:
     AgataDetectorConstructed() {};
    ~AgataDetectorConstructed() {};
  
  protected:
    G4int     nDets;    //> number of detectors
    G4int     nClus;    //> number of clusters
    G4int     iGMin;    //> minimum index for detectors
    G4int     iCMin;    //> minimum index for clusters
    G4int     iGMax;    //> maximum index for detectors
    G4int     iCMax;    //> maximum index for clusters
  
  protected:
    G4int     maxSec;    //> maximum number of sectors
    G4int     maxSli;    //> maximum number of slices
      
  protected:
    G4bool    readOut;  //> true: a segmentation have been defined  
  
  public:
    inline G4int  GetNumberOfDetectors()    { return nDets;             };
    inline G4int  GetMinDetectorIndex()     { return iGMin;             };
    inline G4int  GetMaxDetectorIndex()     { return iGMax;             };
  
  public:
    inline G4int  GetNumberOfClusters()     { return nClus;             };
    inline G4int  GetMinClusterIndex()      { return iCMin;             };
    inline G4int  GetMaxClusterIndex()      { return iCMax;             };
  
  public:
    inline G4int  GetNumberOfSectors ()     { return maxSec;            };
    inline G4int  GetNumberOfSlices  ()     { return maxSli;            };

  public:
    inline G4bool GetReadOut()              { return readOut;           };

  ////////////////////////////////////////////////////////////////////////////
  /// Pure virtual methods which must be implemented
  ////////////////////////////////////////////////////////////////////////////
  public:
    virtual void  Placement        ()                                               = 0;
    virtual void  WriteHeader      ( std::ofstream &outFileLMD, G4double = 1.*mm )  = 0;
    virtual void  ShowStatus       ()                                               = 0;
    virtual G4int GetSegmentNumber ( G4int, G4int, G4ThreeVector )                  = 0;
    virtual G4int GetCrystalType   ( G4int )                                        = 0;
};


#ifdef ADCA
class AgataDetectorADCA;
#endif
#ifdef GASP
class AgataDetectorGasp;
#else
#ifdef CLARA
class AgataDetectorClara;
#else
#ifdef POLAR
class AgataDetectorPolar;
class AgataDetectorClover;
#else
#ifdef EUCLIDES
class AgataDetectorEuclides;
#else
class AgataDetectorArray;
#ifndef DEIMOS
class AgataDetectorShell;
class AgataDetectorSimple;
class AgataDetectorTest;
#endif
#endif
#endif  
#endif
#endif
class OrgamDetector;
class GalileoDetector;
class Nuball2Detector;
///////////////////////////////////////////////////////////////
/// This class handles the construction of the geometry.
/// It provides the material definition, plus the construction
/// of the experimental hall ("world"), of a target and of a
/// reaction chamber. The actual detector placement is done
/// by instantiating other classes.
///////////////////////////////////////////////////////////////
class AgataDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    AgataDetectorConstruction(G4int, G4int,    G4String, G4bool, G4String,
			      G4bool toscore=false);
    AgataDetectorConstruction(G4int, G4String, G4String, G4bool, G4String,
			      G4bool toscore=false);
   ~AgataDetectorConstruction();
   
  public:
    /////////////////////////////////////////////////////////////////////
    /// This method is required (defined in G4VUserDetectorConstruction)
    /////////////////////////////////////////////////////////////////////
    G4VPhysicalVolume* Construct();

  protected:
    G4int ancillaryOffset;    //> offset to handle the sensitive detector objects for ancillaries
    
  private:
    AgataDetectorConstructionMessenger* myMessenger;  // pointer to the Messenger

  private:
    /////////////////////////////////
    /// Experimental hall
    /////////////////////////////////
    G4Box                  *hallBox;
    G4LogicalVolume        *hallLog;
    G4VPhysicalVolume      *hallPhys;

  private:
    ///////////////////////////////////////
    /// Pointers to the classes handling
    /// the germanium detector placement
    ///////////////////////////////////////
#ifdef ADCA
    AgataDetectorADCA*        newADCA;
#endif
#ifdef GASP
    AgataDetectorGasp*        newGasp;
#else
#ifdef CLARA
    AgataDetectorClara*       newClara;
#else
#ifdef POLAR
    AgataDetectorPolar*       newPolar;
    AgataDetectorClover*      newClover;
#else
#ifdef EUCLIDES
    AgataDetectorEuclides*    newEuclides;
#else
    AgataDetectorArray*       newArray;
#ifndef DEIMOS
    AgataDetectorShell*       newShell;
    AgataDetectorSimple*      newSimple;
  AgataDetectorTest* newTestHall;
#endif
#endif  
#endif    
#endif    
#endif    
  OrgamDetector *newOrgam;
  GalileoDetector *newGalileo;
  Nuball2Detector *newNuball2;
  private:
    ////////////////////////////////////////
    /// Pointer where the class which has 
    /// been instantiated is cast
    ////////////////////////////////////////
    AgataDetectorConstructed* theConstructed;

  private:
    //////////////////////////////////////
    /// Sensitive detector for germanium
    /// detectors (and other detectors)
    /////////////////////////////////////
    AgataSensitiveDetector    *geSD;
    
  private:
    /////////////////////////////////////
    /// Pointers to the materials
    /////////////////////////////////////
    G4Material             *matWorld;
    G4Material             *matTarget;
  G4Material *matProductionTarget;
    G4Material             *matFronting;
    G4Material             *matBacking;
    G4Material             *matChamber;
    G4Material             *matDegrader;
    G4Material             *matSupport;


  private:
    /////////////////////////////////////
    /// Names of the materials
    /////////////////////////////////////
    G4String                matWorldName;
    G4String                matTargetName;
  G4String matProductionTargetName;
    G4String                matFrontingName;
    G4String                matBackingName;
    G4String                matChamberName;
    G4String                matDegraderName;
    G4String                matSupportName;

  private:
    G4int                   geomType;   //> which geometry (0=AGATA, 1=shell, 2=simple)
    G4String                ancType;    //> which ancillary (only for AGATA)

  private:
    G4String                iniPath;    //> directory where the data files are stored

  private:
    G4bool                  volume;     //> true: enables the parts about volume calculation
                                        //> of the germanium detectors
    G4bool                  calcOmega;  //> true: solid angle covered by the germanium detectors
                                        //> is calculated
    
  private:
    G4int*                  numHits;    //> buffer used during volume calculation
  G4int*                    histoTheta; //
  G4int*                    histoPhi; //
  G4int*                    histoThetaCoinc;
  G4int**                   histoThetaCoincDetperDet;
  G4int oldtrackid,oldevtnb,oldid;
  G4ThreeVector oldpos;
 private:
    //////////////////////////////////////////////
    /// Dimensions of the scattering chamber
    /////////////////////////////////////////////
    G4double                outRadius;     
    G4double                wallThickness; 
    G4double                pipeRadius;    

  private:
    //////////////////////////////////////////////
    /// Rotation applied to the scattering chamber
    /////////////////////////////////////////////
    G4double                thetaChamb;     
    G4double                phiChamb; 
    
  private:
    //////////////////////////////////////////////
    /// Dimensions and position of the target
    /////////////////////////////////////////////
    G4ThreeVector           targetPosition;
    G4ThreeVector           targetSize;
    G4double                targetThickness; //> in mg/cm2
    G4ThreeVector           productiontargetPosition;
    G4ThreeVector           productiontargetSize;
    G4double                productiontargetThickness; //> in mg/cm2

  G4double                frontingThickness; 
  G4double                backingThickness; 
    G4ThreeVector           degraderPosition;
    G4ThreeVector           degraderSize;
    G4double                degraderThickness; //> in mg/cm2
    G4ThreeVector           supportPosition;
    G4ThreeVector           supportSize;
    G4double                supportThickness; //> in mg/cm2
    G4double                separation;
    G4double                tiltAngle;
    G4ThreeVector           normal2Target;
    G4int                   nStepTarget;
    G4int                   nStepDegrader;
    G4UserLimits*           degraderLimits;
    G4UserLimits*           targetLimits;
    G4bool                  builtTarget;
    G4bool                  builtDegrader;
    G4bool                  builtSupport;
  G4bool builtProductionTarget;
  G4bool                    buildSpectrometer;
  //These are "VAMOS angles". 
  // 	 			 |  		      ^ Y
  // 	 			 |\  V-part	      |
  // 				 | \           	      |
  // 				 |  \          	      |
  // 				 |   \                |
  // 				 |    \               |
  // 				 |     \              |
  //  Z<\ 			 |	\             |
  //  	 --\ 			 |	 \            |
  //  	    --\ 		 |	  \           |
  //  	       --\     	       	 |     	   |          |
  // 	          ---\		 |	   \          |
  // 	              --\	 |	    \         |
  // 	                 --\	 |	     \        |
  // 		            --\	 |	      \       |
  // 		               --| 	       \      |
  // 		                 |--\ 	   PHI /-     |
  // 		                 |   --\     /-	 \    |
  // 		                 |  /   ---+-	  \   |
  // 		     THETA       |  \      |--\	   \  |
  // 		                 |  |      |   --\  \ |
  // 		                 +---\-----+      --\\|
  // 				            \----------
  // 				             /-----   
  // 				       /-----	      
  // 			         /-----		      
  // 		           /-----
  // 		     /-----
  // 	       /-----
  // 	 /-----
  //X <--
  G4double SpecOpeningTheta;
  G4double SpecOpeningPhi;
  G4bool AddVAMOSDetection;
  ///////////////////////////////////////////////
  /// Lookup table of offsets used by ancillary
  /// detectors
  //////////////////////////////////////////////  
  private:
    std::vector<G4int>      offsetLut;
    
    
  //////////////////////////////////////////////
  /// Command directory name
  /////////////////////////////////////////////
    G4String directoryName;    

  ///////////////////////////////////////////
  // List of detector and or other structures to add
  ///////////////////////////////////////////

  std::vector<void*> listofadditionalstructures;
////////////////////////////////////////////////////////////////////////////  
/// Private methods							      
////////////////////////////////////////////////////////////////////////////  
  private:
    void InitData (G4int, G4String, G4String, G4bool, G4String);

  private:
    ////////////////////////////////////////////////
    /// Definition of the materials
    ///////////////////////////////////////////////
    void DefineMaterials();
    void DefineAdditionalMaterials();

  private:
    ////////////////////////////////////////////////
    /// Construction of the experimental hall,
    /// target and scattering chamber
    ///////////////////////////////////////////////
    void ConstructHall();     
    void ConstructTarget();  
    void ConstructProductionTarget();  
    void ConstructChamber();  


  /////////////////////////////////////////////
  // To create "score" sphere
  ///////////////////////////////////////////
  G4bool MakeScoreSphere;
//////////////////////////////////////////////////////////////////////////// 
/// Public methods							     
//////////////////////////////////////////////////////////////////////////// 
  public:
    void SetTargetReactionDepth   ( G4double );
    void SetDegraderReactionDepth ( G4double );
    
  public:
    ////////////////////////////////////////////////////////////
    /// Writes out information to the list-mode file
    ///////////////////////////////////////////////////////////
    void   WriteHeader  ( std::ofstream &outFileLMD, G4double=1.*mm );
    
  public:
    ///////////////////////////////////////////////////////////
    /// Methods performing volume and solid angle calculation
    /// (with Monte Carlo methods)
    //////////////////////////////////////////////////////////
    void   IncrementHits        ( G4int index );
  void   IncrementThetaPhi    (const G4ThreeVector &hitpos);
  void IncrementThetaCoin(G4int parid, G4int eventid, G4int detid, const G4ThreeVector &hitpos);
    void   CalculateGeVolume    ( G4int numberOfEvent, G4ThreeVector size );
    void   CalculateGeSolidAngle( G4int numberOfEvent );
    
  public:
    /////////////////////////////////////////////////////////////////////////
    /// This method calculates the effective distance in germanium between
    /// two points. Materials different than germanium are normalized through
    /// their radiation length.
    /////////////////////////////////////////////////////////////////////////
    G4double DistanceInGe ( G4ThreeVector, G4ThreeVector, G4int );
    
  public:
    /////////////////////////////////////////////////////////////////////////
    /// These methods return the number of segment and the crystal type
    /// (presently meaningful only for the AGATA geometry).
    /////////////////////////////////////////////////////////////////////////
    G4int  GetSegmentNumber ( G4int, G4int, G4ThreeVector ); 
    G4int  GetCrystalType   ( G4int ); 

  ////////////////////////////////////////////////////////////////////////////
  /// Public methods to perform actions interactively through the messenger
  /// class.
  ///////////////////////////////////////////////////////////////////////////
  public:
    ///////////////////////////////////////////////////////////
    /// Characteristics of the target
    ///////////////////////////////////////////////////////////
    void SetTargetSize     ( G4ThreeVector );
    void SetProductionTargetSize     ( G4ThreeVector );
  void SetFrontingThickness (G4double);
  void SetBackingThickness (G4double);
    void SetDegraderSize   ( G4ThreeVector );
    void SetSupportSize    ( G4ThreeVector );
    void SetTargetPosition ( G4ThreeVector );
    void SetProductionTargetPosition ( G4ThreeVector );
    void SetTargMate       ( G4String );
    void SetProductionTargMate       ( G4String );
    void SetFrontingMate    ( G4String );
    void SetBackingMate    ( G4String );
    void SetDegraderMate   ( G4String );
    void SetSupportMate    ( G4String );
    void SetSeparation     ( G4double );
    void SetTiltAngle      ( G4double );
    void SetNStepTarget    ( G4int );
    void SetNStepDegrader  ( G4int );

  public:
    ///////////////////////////////////////////////////////////
    /// Characteristics of the scattering chamber
    ///////////////////////////////////////////////////////////
    void SetChamMate       ( G4String );
    void SetWallThickness  ( G4double );
    void SetChamberRadius  ( G4double );
    void SetPipeRadius     ( G4double );
    void SetThetaChamb     ( G4double );
    void SetPhiChamb       ( G4double );

  /////////////////////////////////////////////////////////////
  /// Other methods for the messenger
  //////////////////////////////////////////////////////////////
  public:
  void SetBuildSpectrometer(G4bool val){
    buildSpectrometer=val;
    std::cout << "------> Setting build spectrometer to " << buildSpectrometer
	      << std::endl;
  }
  void SetAddVAMOSDetection(G4bool val){
    AddVAMOSDetection=val;
    std::cout << "------> Setting AddVAMOSDetection to " << AddVAMOSDetection
	      << std::endl;
  } 
  void SetSpecOpeningTheta(G4double val){
    SpecOpeningTheta = val;
    std::cout << "------> Set spectrometer opening theta to " 
	      << SpecOpeningTheta/deg << " degrees\n";
  }
  void SetSpecOpeningPhi(G4double val){
    SpecOpeningPhi = val;
    std::cout << "------> Set spectrometer opening phi to " 
	      << SpecOpeningPhi/deg << " degrees\n";
  }

  ////////////////////////////////////////////////////////
    /// This method should be execute to commit each change
    /// of geometry.
    ///////////////////////////////////////////////////////
    void UpdateGeometry    ();

 
  public:
    void ShowStatus        ();
    void ShowCommonStatus  ();
    void GetDistInGe       ( G4ThreeVector, G4ThreeVector, G4int );

  ////////////////////////////////////////////////////////////////////////
  /// This method returns the next available offset to be used by the
  /// ancillary detectors
  ///////////////////////////////////////////////////////////////////////
  public:
    G4int GetAncillaryOffset();
    
  //////////////////////////////////////////////////////////////////////
  /// Methods to set/get elements of the lookup table
  /////////////////////////////////////////////////////////////////////
  public:
    G4int GetOffset     ( G4int );
    void  SetOffset     ( G4int, G4int );
    void  CopyOffset    ( std::vector<G4int>& );
    void  AddOffset     ( G4int ); 
    void  ResetOffset   ();
    void  PrintOffset   ();
    G4int HowManyOffset ();
    
  /////////////////////////////////////////////////////////////////
  /// Inline methods
  ////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////////////////////////////
  /// These methods return the information handled by the AgataDetectorConstructed
  /// object.
  ///////////////////////////////////////////////////////////////////////////////////
  public:
    inline G4int  GetNumberOfDetectors ()    { return  theConstructed->GetNumberOfDetectors(); };
    inline G4int  GetMinDetectorIndex  ()    { return  theConstructed->GetMinDetectorIndex();  };
    inline G4int  GetMaxDetectorIndex  ()    { return  theConstructed->GetMaxDetectorIndex();  };
    inline G4int  GetNumberOfClusters  ()    { return  theConstructed->GetNumberOfClusters();  };
    inline G4int  GetMinClusterIndex   ()    { return  theConstructed->GetMinClusterIndex();   };
    inline G4int  GetMaxClusterIndex   ()    { return  theConstructed->GetMaxClusterIndex();   };
    inline G4int  GetNumberOfSectors   ()    { return  theConstructed->GetNumberOfSectors();   };
    inline G4int  GetNumberOfSlices    ()    { return  theConstructed->GetNumberOfSlices ();   };
    inline G4bool GetReadOut           ()    { return  theConstructed->GetReadOut();	       };

  public:
    inline G4ThreeVector GetNormal2Target () { return  normal2Target;                          };

#ifdef GASP
  public:
           G4int  GetNumberOfNeutron   ();
           G4int  GetMaxNeutronIndex   ();

  public:
           G4int  GetNumberOfBgo       ();
           G4int  GetMaxBgoIndex       ();

  public:
           G4int  GetNumberOfSi        ();
           G4int  GetMaxSiIndex        ();
           G4bool IsSiSegmented        ();
#endif
//#ifdef CLARA
#if defined(CLARA) || defined(MCP)
  public:
           G4int  GetNumberOfMcp       ();
           G4int  GetMaxMcpIndex       ();
#endif
    
  //////////////////////////////////////////////////////////////////////////////////
  /// These methods return useful pointers
  //////////////////////////////////////////////////////////////////////////////////
  public:
    inline G4VPhysicalVolume*       HallPhys          () { return hallPhys;  };
    inline G4LogicalVolume*         HallLog           () { return hallLog;   };
    inline AgataSensitiveDetector*  GeSD              () { return geSD;      };
#ifdef ADCA
   inline AgataDetectorADCA*       GetAdcaDetector   () { return newADCA;   };
#endif
#ifdef GASP    
   inline AgataDetectorGasp*       GetGaspDetector   () { return newGasp;   };
#else
#ifdef CLARA
    inline AgataDetectorClara*      GetClaraDetector  () { return newClara;  };
#else
#ifdef POLAR
    inline AgataDetectorPolar*      GetPolarDetector  () { return newPolar;  };
    inline AgataDetectorClover*     GetCloverDetector () { return newClover; };
#else
#ifdef EUCLIDES
    inline void SetCalcOmega( G4bool value ) { calcOmega = value; };
#else
    inline AgataDetectorArray*      GetDetectorArray  () { return newArray;  };
#ifndef DEIMOS
    inline AgataDetectorShell*      GetDetectorShell  () { return newShell;  };
    inline AgataDetectorSimple*     GetDetectorSimple () { return newSimple; };
#endif
#endif
#endif
#endif
#endif

  ////////////////////////////////////////////////////////////////////////
  /// These methods return the characteristics of the target
  ///////////////////////////////////////////////////////////////////////
  public:
    inline G4ThreeVector GetTargetPosition () { return targetPosition; };
    inline G4ThreeVector GetTargetSize     () { return targetSize;     };
    inline G4ThreeVector GetProductionTargetPosition () 
  { return productiontargetPosition; };
    inline G4ThreeVector GetProductionTargetSize     () 
  { return productiontargetSize;     };
    inline G4ThreeVector GetDegraderPosition () { return degraderPosition; };
    inline G4ThreeVector GetDegraderSize     () { return degraderSize;     };
    inline G4ThreeVector GetSupportPosition  () { return supportPosition;  };
    inline G4ThreeVector GetSupportSize      () { return supportSize;	   };
    inline G4Material*   GetTargetMaterial   () { return matTarget;        };
    inline G4Material*   GetProductionTargetMaterial   () 
  { return matProductionTarget;        };
    inline G4Material*   GetDegraderMaterial () { return matDegrader;      };
    inline G4Material*   GetSupportMaterial  () { return matSupport;       };
    inline G4double      GetTargetThickness  () { return targetThickness;  };
    inline G4double      GetProductionTargetThickness  () 
  { return productiontargetThickness;  };
    inline G4double      GetDegraderThickness() { return degraderThickness;};
    inline G4double      GetSupportThickness () { return supportThickness; };
    inline G4bool        IsTarget            () { return builtTarget;      };
    inline G4bool        IsDegrader          () { return builtDegrader;    };
    inline G4bool        IsSupport           () { return builtSupport;     };
  inline G4bool IsProductionTarget() {return builtProductionTarget;}    
  inline G4bool GetBuildSpectrometer() {return buildSpectrometer;}
  inline G4bool GetAddVAMOSDetection() {return AddVAMOSDetection;}
  inline G4double GetSpecOpeningTheta() {return SpecOpeningTheta;}
  inline G4double GetSpecOpeningPhi() {return SpecOpeningPhi;}
    
  ////////////////////////////////////////////////////////////////////////
  /// This method is needed to notify AgataEventAction whether a solid
  /// angle calculation is being performed or not
  ///////////////////////////////////////////////////////////////////////
  public:
    inline G4bool        CalcOmega         () { return calcOmega;      };  
    

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADouble;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWith3Vector;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class AgataDetectorConstructionMessenger: public G4UImessenger
{
  public:
    AgataDetectorConstructionMessenger(AgataDetectorConstruction*, G4bool, G4String);
   ~AgataDetectorConstructionMessenger();
        
  private:
    AgataDetectorConstruction* myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             myADirectory;
    G4UIcmdWithoutParameter*   UpdateCmd;   
    G4UIcmdWithoutParameter*   StatusCmd;   
    G4UIcmdWithAString*        TarMatCmd;
    G4UIcmdWithAString*        ProdTarMatCmd;
  G4UIcmdWithAString*        FroMatCmd;
    G4UIcmdWithAString*        BacMatCmd;
    G4UIcmdWithAString*        DegMatCmd;
    G4UIcmdWithAString*        SupMatCmd;
    G4UIcmdWithAString*        ChamMatCmd;
    G4UIcmdWithADouble*        ChamRadiusCmd;
    G4UIcmdWithADouble*        PipeRadiusCmd;
    G4UIcmdWithADouble*        SetSeparationCmd;
    G4UIcmdWithADouble*        SetTiltAngleCmd;
    G4UIcmdWithAString*        RotateChamberCmd;
    G4UIcmdWithADouble*        WalThickCmd;
    G4UIcmdWith3Vector*        SetTarSizeCmd;
  G4UIcmdWith3Vector*        ProdSetTarSizeCmd;
    G4UIcmdWithADouble*        SetFrontThickCmd;
    G4UIcmdWithADouble*        SetBackThickCmd;
    G4UIcmdWith3Vector*        SetDegSizeCmd;
    G4UIcmdWith3Vector*        SetSupSizeCmd;
    G4UIcmdWith3Vector*        SetSizeCmd;
    G4UIcmdWith3Vector*        SetPositionCmd;
  G4UIcmdWith3Vector*        ProdSetPositionCmd;
    G4UIcmdWithAString*        CalcDistGeCmd;
    G4UIcmdWithAString*        CalcVolCmd;
    G4UIcmdWithAnInteger*      CalcOmegaCmd;
    G4UIcmdWithAnInteger*      SetNStepTargetCmd;
    G4UIcmdWithAnInteger*      SetNStepDegraderCmd;
  G4UIcmdWithABool*         SetBuildSpectrometer;
  G4UIcmdWithABool*         SetAddVAMOSDetection;    
  G4UIcmdWithADoubleAndUnit*       SetSpecOpeningPhi;
  G4UIcmdWithADoubleAndUnit*       SetSpecOpeningTheta;
  private:
    G4bool volume;

  public:
    void SetNewValue(G4UIcommand*, G4String);
            
};


#endif
