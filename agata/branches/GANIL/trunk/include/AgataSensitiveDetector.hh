/////////////////////////////////////////////////////////////////////////
/// This class takes care of extracting the information about the
/// energy deposition within the active parts of the geometry.
/// It is possible to choose whether to follow the actual electron
/// tracks within the crystals or to assign their energy deposition
/// to the point where the primary interaction took place. This
/// possibility is disabled in the case of the ancillary detectors.
////////////////////////////////////////////////////////////////////////
#ifndef AgataSensitiveDetector_h
#define AgataSensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"
#include "AgataHitDetector.hh"
#include "globals.hh"

using namespace std;

class G4Step;
class G4HCofThisEvent;
class AgataDetectorConstruction;
class AgataSensitiveDetectorMessenger;

class AgataSensitiveDetector : public G4VSensitiveDetector
{
  public:
      AgataSensitiveDetector(G4String, G4String, G4String, G4int, G4int, G4bool = false);
      AgataSensitiveDetector(G4String, G4String, G4String, G4bool = true);
     ~AgataSensitiveDetector();

  private:
      AgataDetectorConstruction*       theDetector;
      AgataSensitiveDetectorMessenger* myMessenger;
      
  private:
      G4int                       HCID;
      AgataHitDetectorCollection* theHits;

  private:
      G4int     trackMethod;       //> 0--> follow electron tracks; 1-->just the primary interaction points

  private:
      G4bool        annihilationHappened;
      G4ThreeVector positionOfAnnihilation;
      G4double      residualEnergy;
  
  private:
      G4int     depth;             //> depth (with respect to the mother volume) at which the sensitive part is placed
      G4int     offset;            //> the detector numbers are stored into the hit collection increased by this offset
     
  private:
     void InitData ( G4String, G4int, G4int, G4String );   //> Initialization of class members  

  ////////////////////////////////////////////
  /// Methods needed by the class prototype
  ////////////////////////////////////////////
  public:
     G4bool ProcessHits (G4Step*, G4TouchableHistory*);
     void Initialize    (G4HCofThisEvent*);
     void EndOfEvent    (G4HCofThisEvent*);
     void DeleteHits    ();

  public:
    void  SetMethod  ( G4int ); 
    void  ShowStatus ();        
    void  SetDepth   ( G4int ); 

  private:
    G4int GetProcessNum ( G4String );  //> returns a number corresponding to the name of the interaction
                                       //>  (passed as an argument)

  /////////////////////////////////
  /// Methods for the light output for NWall and NEDA
  ////////////////////////////////
  
private:
  G4double EnergyToLight    ( G4double );
  G4double LightProduced    ( AgataHitDetector* );
  G4int    ParticleID       ( AgataHitDetector* );
  public:
    inline G4int GetMethod ()	{ return trackMethod; };
    inline G4int GetDepth  ()	{ return depth;       };
      
};
#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;

class AgataSensitiveDetectorMessenger: public G4UImessenger
{
  public:
    AgataSensitiveDetectorMessenger(AgataSensitiveDetector*,G4String);
   ~AgataSensitiveDetectorMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAnInteger*      SetMethodCmd;
    G4UIcmdWithoutParameter*   StatusCmd;
    AgataSensitiveDetector*    myTarget;

};

#endif

