////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryShell_h
#define AgataAncillaryShell_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryShellMessenger;

class AgataAncillaryShell : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryShell(G4String, G4String);
    ~AgataAncillaryShell();

  private:
    AgataAncillaryShellMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName;
    G4Material  *matShell;
  
  //////////////////////////
  /// Size of the shell
  /////////////////////////
  private:
    G4double     shellRmin;
    G4double     shellRmax;

  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

//  public:
//    inline G4String GetName () { return G4String("SHELL"); };
    
  ////////////////////////////////
  //// Methods for the messenger
  ////////////////////////////////
  public:
    void SetShellRmin              ( G4double );
    void SetShellRmax              ( G4double );
    void SetShellMate              ( G4String ); 

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class AgataAncillaryShellMessenger: public G4UImessenger
{
  public:
    AgataAncillaryShellMessenger(AgataAncillaryShell*, G4String);
   ~AgataAncillaryShellMessenger();
    
  private:
    AgataAncillaryShell*    myTarget;
    G4UIdirectory*          myDirectory;
    G4UIcmdWithAString*     ShellMatCmd;
    G4UIcmdWithAString*     SetShellSizeCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
            
};

#endif
