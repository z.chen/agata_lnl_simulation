#ifndef Outgoing_Beam_TargetEx_h
#define Outgoing_Beam_TargetEx_h 1

#include <cmath>
#include <vector>

#include "globals.hh"
#include "G4UnitsTable.hh"
#include "G4ThreeVector.hh"
#include "G4Track.hh"
#include "G4DynamicParticle.hh"
#include "G4ParticleTable.hh"
#include "G4RandomDirection.hh"
#include "G4Decay.hh"
#include "Randomize.hh"
#include "Incoming_Beam.hh"
#include "Charge_State.hh"
#include "G4LorentzRotation.hh"
#include "G4LorentzVector.hh"
#include "Outgoing_Beam.hh"

#ifdef G4V10
#include "G4IonTable.hh"
#endif

#include "CAngDistHandler.hh"
#include "CEvapDistHandler.hh"

#include "CSpec1D.hh"
#include "CSpec2D.hh"

// this is? it may be to separate the old and new track...
//#define  eps 0.00001
#define  OBeps (0.000001*mm)

using namespace std;

class Outgoing_Beam_TargetEx_Messenger;
class Outgoing_Beam_TargetEx : public Outgoing_Beam
{
  public:
     Outgoing_Beam_TargetEx(G4String);
    ~Outgoing_Beam_TargetEx();

  public:
    void Report();

  public:
    void defaultIncomingIon(Incoming_Beam *);
    void setDecayProperties(void);

  public:
    void setA(G4int);
    void setZ(G4int);

  public:
    void setDA(G4int);
    void setDZ(G4int);
    void setEx(G4String);
    void setProjEx(G4double);
    void settau(G4double);
    void setPLEx(G4String);
    void setTLEx(G4String);

  public:
    void setTarEx1(G4double);
    void setTarEx2(G4double);
    void setCFrac(G4double);
    void setTFrac(G4double);

  public:
    void setPhiRange(G4double, G4double);
    void setThRange (G4double, G4double);

  public:
    void ScanInitialConditions(const G4Track &);
    
  public:
    void EndOfRun ();  

  public:
    inline void     SetReactionOn () { reacted = true;  };
    inline void     SetReactionOff() { reacted = false; };
    inline G4double getTime()        { return tau;      };
    inline G4bool   ReactionOn()     { return reacted;  };
    inline G4double getTFrac()       { return TFrac;    };

  public:
    inline G4int    GetReactionFlag()       { return ReactionFlag; };
    inline void     SetReactionFlag(G4int ff){ ReactionFlag=ff;      };
    inline G4int    GetIndex()              { return Index;        };
    
  private:
    G4double Q_value (G4double,G4double);  

  public:
  std::vector<G4DynamicParticle*> ReactionProduct();
    G4DynamicParticle* ProjectileGS();
    G4DynamicParticle* TargetExcitation();

  public:
    G4ThreeVector ReactionPosition();
    G4double GetTargetExcitation();

  public:
    void     SetNQ(G4int n)  { NQ=n;SetUpChargeStates(); };
    void     SelectQ(G4int q){ SQ=q;G4cout<<" Charge state "<<SQ<<" selected for setup"<<G4endl; };

  public:
    void     SetUpChargeStates();
    void     SetQCharge(G4int);
    void     SetQUnReactedFraction(G4double);
    void     SetQReactedFraction(G4double);

  public:
    void     SetQKEu(G4double);
    void     SetQKE(G4double);

  public:
    void     CalcQR();
    void     CalcQUR();

  public:
    G4double GetURsetKE();
    G4double GetRsetKE();

  public:
    void     SetAngDistFile(G4String);
  
  private:
    G4ThreeVector GetOutgoingMomentum();  //tmp
    G4ThreeVector GetOutgoingMomentumFE();
    G4ThreeVector GetOutgoingMomentumTR();
    G4ThreeVector GetOutgoingMomentumCLX();
    G4ThreeVector GetOutgoingMomentumFF();
    G4ThreeVector GetOutgoingMomentumIF();
    G4ThreeVector TargetAngularDistribution();
    void SelectExcitionEnergy();
    void SelectTLExcitionEnergy();
    void SelectPLExcitionEnergy();
    
  private:
    Outgoing_Beam_TargetEx_Messenger* myMessenger;

  private:
    G4int Ain;
    G4int Zin;

  private:
    G4ThreeVector dirIn;
    G4ThreeVector posIn;
    G4ThreeVector posOut;
    G4ThreeVector pIn;

  private:
    G4int  ReactionFlag;

  private:
    G4ParticleTable* particleTable; 

  private:
    G4double      tauIn;
    G4double      KEIn;

  private:
    G4int         n_part[3];
    G4double      e_part[3];
    G4double      s_part[3];

  private:
    G4int DZ;
    G4int DA;

  private:
    G4int Ztar;
    G4int Atar;

  private:
  G4double Ex;
  std::vector<std::pair<G4double,G4double> > listEx;
  std::vector<std::pair<G4double,G4double> > listPLEx;
  std::vector<std::pair<G4double,G4double> > listTLEx;
  G4double totFeeding;
  G4double totPLFeeding;
  G4double totTLFeeding;
  
  //Fusion-fission
  G4double nybar; //mean number of neutrons
  G4double sigmanybar; //
  G4double fractionKinE;
  G4double phiMin,phiMax;//In CM (same as LAB)
  G4bool   TargetLikeFrag_detection;
  G4double thetaMin,thetaMax;//In LAB
    G4double projEx;
    G4double PLEx;
    G4double TLEx;
    G4double TarEx1;
    G4double TarEx2;
    G4double CFrac;
    G4double TFrac;
    G4double tau;
  
  private:
    G4int    NQ;
    G4int    SQ;
    vector<Charge_State*> Q;
    G4double  QR[1000];
    G4double  QUR[1000];
    G4int     QRI[1000];
    G4int     QURI[1000];
    G4int     Index;


  private:
    G4ParticleDefinition* ion;
    G4ParticleDefinition* targetrecoil;
    G4ThreeVector         ptargetrecoil;
    G4ParticleDefinition* iongs;
    G4ParticleDefinition* iontar;
    G4ParticleDefinition* ionpro;
    G4ParticleDefinition* iontl;
    
    G4ParticleDefinition* BeamLikeFragment;

    G4ParticleDefinition* TargetLikeFragment;
    G4ThreeVector         TLF_momentum;

  private:
    G4bool evapFile[3];
 
  private:
    G4bool TLFdetection;
  private:
//    static G4Decay decay;
    G4bool  reacted;

  
  private:
   Incoming_Beam *beamIn;
   
  private:
    G4ThreeVector partMomentum;  

  
  private:
    AngDistHandler* theAngDist;
    G4String angDistFileName;
    EvapDistHandler* theEvapDist[3];
    G4String evapDistFileName[3];

  private:
    CSpec1D* theSpectrum;
    CSpec2D* theMatrix;
    
  private:
    G4double p_fe;
    G4double p_tr; 
    G4double p_clx;
    G4double p_ff; 
    G4double p_if;
 
 public:
    void SetTargetLikeDetection (G4bool);
    void SetNumEvapN( G4int );
    void SetNumEvapP( G4int );
    void SetNumEvapA( G4int );
    void SetEnEvapN( G4double );
    void SetSigEvapN( G4double );
    void SetEnEvapP( G4double );
    void SetSigEvapP( G4double );
    void SetEnEvapA( G4double );
    void SetSigEvapA( G4double );
    void SetEvapFromFileN( G4bool );
    void SetEvapFromFileP( G4bool );
    void SetEvapFromFileA( G4bool );
    void SetPfe( G4double );
    void SetPtr( G4double );
    void SetPclx( G4double );
  void SetPff( G4double );
  void SetPif( G4double );
  /******************************************************************************
   *                      Code generated with sympy 0.7.1                       *
   *                                                                            *
   *              See http://www.sympy.org/ for more information.               *
   *                                                                            *
   *                       This file is part of 'project'                       *
   *****************************************************************************/

  double f1(const double &beta, const double &gamma, const double &thetaCM, 
	   const double &theta_LAB, const double &u) {
    return tan(theta_LAB) - sin(thetaCM)/(gamma*(beta/u + cos(thetaCM)));
  }

  /******************************************************************************
   *                      Code generated with sympy 0.7.1                       *
   *                                                                            *
   *              See http://www.sympy.org/ for more information.               *
   *                                                                            *
   *                       This file is part of 'project'                       *
   *****************************************************************************/

  double fp(const double &beta, const double &gamma, const double &thetaCM, 
	    const double &u) {
    return -cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) - 
      pow(sin(thetaCM), 2)/(gamma*pow(beta/u + cos(thetaCM), 2));
  }


  /******************************************************************************
   *                      Code generated with sympy 0.7.1                       *
   *                                                                            *
   *              See http://www.sympy.org/ for more information.               *
   *                                                                            *
   *                       This file is part of 'project'                       *
   *****************************************************************************/

  double thetaLAB(double beta, double gamma, double thetaCM, double u) {
    return atan(sin(thetaCM)/(gamma*(beta/u + cos(thetaCM))));
  }

  /******************************************************************************
   *                      Code generated with sympy 0.7.1                       *
   *                                                                            *
   *              See http://www.sympy.org/ for more information.               *
   *                                                                            *
   *                       This file is part of 'project'                       *
   *****************************************************************************/

  double dthetaLAB(double beta, double gamma, double thetaCM, double u) {
    return (cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) + 
	    pow(sin(thetaCM), 2)/(gamma*pow(beta/u + cos(thetaCM), 2)))/
      (1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2)));
  }

  /******************************************************************************
   *                      Code generated with sympy 0.7.1                       *
   *                                                                            *
   *              See http://www.sympy.org/ for more information.               *
   *                                                                            *
   *                       This file is part of 'project'                       *
   *****************************************************************************/

  double ddthetaLAB(double beta, double gamma, double thetaCM, double u) {

    return -(sin(thetaCM)/(gamma*(beta/u + cos(thetaCM))) - 
	     3*sin(thetaCM)*cos(thetaCM)/(gamma*pow(beta/u + 
						    cos(thetaCM), 2)) - 
	     2*pow(sin(thetaCM), 3)/(gamma*pow(beta/u + cos(thetaCM), 3)))/
      (1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2))) 
      - 2*(cos(thetaCM)/(gamma*(beta/u + cos(thetaCM))) + pow(sin(thetaCM), 2)/
	   (gamma*pow(beta/u + cos(thetaCM), 2)))*
      (sin(thetaCM)*cos(thetaCM)/
       (pow(gamma, 2)*pow(beta/u + cos(thetaCM), 2)) + 
       pow(sin(thetaCM), 3)/(pow(gamma, 2)*pow(beta/u + cos(thetaCM), 3)))/
      pow(1 + pow(sin(thetaCM), 2)/(pow(gamma, 2)*
				    pow(beta/u + cos(thetaCM), 2)), 2);
  }
 
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class Outgoing_Beam_TargetEx_Messenger: public G4UImessenger
{
  public:
    Outgoing_Beam_TargetEx_Messenger(Outgoing_Beam_TargetEx*,G4String);
   ~Outgoing_Beam_TargetEx_Messenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    Outgoing_Beam_TargetEx*             myTarget;    
    G4UIdirectory*             BeamOutDir;
    G4UIdirectory*             QDir;
    G4UIcmdWithAnInteger*      ACmd;
    G4UIcmdWithAnInteger*      ZCmd;
    G4UIcmdWithAnInteger*      DACmd;
    G4UIcmdWithAnInteger*      DZCmd;
    G4UIcmdWithAString*        ExCmd;
    G4UIcmdWithADoubleAndUnit* projExCmd;
    G4UIcmdWithAString*        PLExCmd;
    G4UIcmdWithAString*        TLExCmd;
    G4UIcmdWithADoubleAndUnit* TEx1Cmd;
    G4UIcmdWithADoubleAndUnit* TEx2Cmd;
    G4UIcmdWithADouble*        CExFCmd;
    G4UIcmdWithADouble*        TExFCmd;
    G4UIcmdWithADoubleAndUnit* tauCmd;
    G4UIcmdWithoutParameter*   RepCmd;
    G4UIcmdWithABool*          TLFDetCmd;
    G4UIcmdWithAnInteger*      NQCmd;    
    G4UIcmdWithAnInteger*      SQCmd;  
    G4UIcmdWithAnInteger*      SCCmd;  
    G4UIcmdWithADouble*        QUFCmd;  
    G4UIcmdWithADouble*        QRFCmd;  
    G4UIcmdWithADoubleAndUnit* QKECmd;
    G4UIcmdWithADoubleAndUnit* QKEuCmd;
    G4UIcmdWithAString*        aFileCmd;
    G4UIcmdWithAString*        phiCmd;
    G4UIcmdWithAString*        thCmd;
    G4UIcmdWithAnInteger*      npartNCmd;
    G4UIcmdWithAnInteger*      npartPCmd;
    G4UIcmdWithAnInteger*      npartACmd;
    G4UIcmdWithADoubleAndUnit* epartNCmd;
    G4UIcmdWithADoubleAndUnit* spartNCmd;
    G4UIcmdWithABool*          enableEvapFromFileNCmd;
    G4UIcmdWithABool*          disableEvapFromFileNCmd;
    G4UIcmdWithADoubleAndUnit* epartPCmd;
    G4UIcmdWithADoubleAndUnit* spartPCmd;
    G4UIcmdWithABool*          enableEvapFromFilePCmd;
    G4UIcmdWithABool*          disableEvapFromFilePCmd;
    G4UIcmdWithADoubleAndUnit* epartACmd;
    G4UIcmdWithADoubleAndUnit* spartACmd;
    G4UIcmdWithABool*          enableEvapFromFileACmd;
    G4UIcmdWithABool*          disableEvapFromFileACmd;
    G4UIcmdWithADouble*        PfeCmd;  
    G4UIcmdWithADouble*        PtrCmd;  
    G4UIcmdWithADouble*        PclxCmd;  
    G4UIcmdWithADouble*        PffCmd;  
    G4UIcmdWithADouble*        PifCmd;  

};

#endif


           
