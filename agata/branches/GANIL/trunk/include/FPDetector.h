#ifndef FPDetector_h
#define FPDetector_h 1

#include "G4ThreeVector.hh"
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4PVPlacement;
class G4Material;

// class FPThicknessMessenger;

class FPDetector
{
  public:

    FPDetector(G4double);
    virtual ~FPDetector();

    void createDetector(G4VPhysicalVolume *, G4Material *);

//     void SetDetectorThickness(G4double);

    G4double detDist;
//     G4double thickness;

    G4PVPlacement *FP_phys_cpy[1];
    G4ThreeVector placement[1];
    G4LogicalVolume *FP_log;

//     void UpdateGeometry();

  private:

//    void defineMaterial();
//    void setAsSensitiveDetector();

//     FPThicknessMessenger *thicknessMessenger;

    G4Material *detectorMaterial;
    G4VPhysicalVolume *FP_phys;
    
}; 

#endif
