////////////////////////////////////////////////////////
/// This class handles the Nuball2 array 
////////////////////////////////////////////////////////

#ifndef Nuball2Detector_h
#define Nuball2Detector_h 1

#include "AgataDetectorConstruction.hh"

#include "AgataSensitiveDetector.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include <set>

using namespace std;

class G4Material;
class Nuball2DetectorMessenger;
class AgataDetectorAncillary;

#ifndef _PlacementOfAPhaseI_
#define _PlacementOfAPhaseI_
struct PlacementOfAPhaseI {
  G4double x,y,z,phi,theta,psi;
  G4int id;
};
#endif

class Nuball2Detector : protected AgataDetectorConstructed
{
public:
  Nuball2Detector(G4String,G4String, G4String);
  ~Nuball2Detector();

  void Placement       ();
  void  WriteHeader      ( std::ofstream &outFileLMD, G4double = 1.*mm );
  void  ShowStatus       ()                                              {;}
  G4int GetSegmentNumber ( G4int offset, G4int nGe, G4ThreeVector position );
  inline G4int GetCrystalType   ( G4int detNum )
  {return -1*(detNum+1)/(detNum+1); };
  G4String GetAnc(){return ancType;}
  void SetMode(int mode_/*=1*Clover+2*Phase1+4*LaBr3*/) { mode=mode_;}
  void SetCloverBGO(bool value){UseCloverBGO=value;}
  void SetCloverColl(bool value){UseCloverColl=value;}
  void SetPhaseIBGO(bool value){UsePhaseIBGO=value;}
  void SetPhaseIColl(bool value){UsePhaseIColl=value;}
private:
  AgataDetectorAncillary      *theAncillary;
  G4String                    directoryName;  //> for the command line  
  G4String                    iniPath;     //> directory where the files are located
  G4String ancType;

  G4String    matCrystalName;
  G4String    matCapsuleName;
  G4String    matAntiCompton1Name;
  G4String    matAntiCompton2Name;

  G4Material  *matCrystal;
  G4Material  *matCapsule;
  G4Material  *matAntiCompton1;
  G4Material  *matAntiCompton2;
  G4Material  *matCollimator;    


  AgataSensitiveDetector    *bgoSD;
  Nuball2DetectorMessenger*  myMessenger;

  //To keep track of detectorangles for header
  std::map<int,G4ThreeVector> fDetAngles;

  
  G4int FindMaterials  ();

  G4int mode;

  bool UseCloverBGO;
  bool UseCloverColl;
  bool UsePhaseIBGO;
  bool UsePhaseIColl;
  
  G4double cell_length;
  G4double chamber_length;
  G4double translationdistance;

  // PhaseI related

  G4LogicalVolume *pCrystal;
  G4LogicalVolume *pCapsule;
  G4LogicalVolume *pBGOCrystal;
  G4LogicalVolume *pBGOCapsule;
   
  G4LogicalVolume *pTronCrystal;
  G4LogicalVolume *pTronCapsule;
  G4LogicalVolume *pTronCapsuleOut;
  G4LogicalVolume *pTronBGO;
  G4LogicalVolume *pTronOuterCapsule;
  
  G4LogicalVolume *pTronArre;
  G4LogicalVolume *pTronColimator;



  G4LogicalVolume *GetTronCrystal();
  G4LogicalVolume *GetTronBGO();    
  G4LogicalVolume *GetTronColimator();
  G4LogicalVolume *GetTronCapsule();

  
  // StringToRotationMatrix() converts a string "X90,Y45" into a
  // G4RotationMatrix.
  // This is an active rotation, in that the object is first rotated
  // around the parent's X axis by 90 degrees, then the object is
  // further rotated around the parent's Y axis by 45 degrees.
  // The return value points to a G4RotationMatrix on the heap, so
  // it is persistent. Angles are in degrees, can have decimals,
  // and can be negative. Axes are X, Y, Z.

  static G4RotationMatrix StringToRotationMatrix(G4String rotation);

};



#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class Nuball2DetectorMessenger: public G4UImessenger
{
public:
  Nuball2DetectorMessenger(Nuball2Detector*);
  ~Nuball2DetectorMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
    
private:
  Nuball2Detector*       myTarget;
    
  G4UIdirectory*             myDirectory;
  G4UIcmdWithAnInteger* setMode;
  G4UIcmdWithABool* setCloverBGO;
  G4UIcmdWithABool* setCloverColl;
  G4UIcmdWithABool* setPhaseIBGO;
  G4UIcmdWithABool* setPhaseIColl;
}; 

#endif
