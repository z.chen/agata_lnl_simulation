/***************************************************************************
                          DiamondLycca.h  -  description
                             -------------------
    begin                : Thu Jul 13 2006
    copyright            : (C) 2006 by Mike Taylor
    email                : mjt502@york.ac.uk
 ***************************************************************************/

#ifndef DiamondLycca_h
#define DiamondLycca_h 1

#include "G4ThreeVector.hh"

#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4PVPlacement;
class G4Material;

class DiamondLycca
{
  public:

    DiamondLycca(G4double);
    virtual ~DiamondLycca();

    void createDetector(G4VPhysicalVolume *, G4Material *);

    G4double detDist;

    G4PVPlacement *Diamond_phys_cpy[109];
    G4ThreeVector placement[109];
    G4LogicalVolume *DiamondLycca_log;

  private:

//    void defineMaterial();
//    void setAsSensitiveDetector();
    
    G4Material *detectorMaterial;
    G4VPhysicalVolume *DiamondLycca_phys;
 };                                          

#endif
