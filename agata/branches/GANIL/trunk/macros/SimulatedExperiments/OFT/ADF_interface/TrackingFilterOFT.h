#ifndef ADF_TRACKINGFILTER_OFT_H
#define ADF_TRACKINGFILTER_OFT_H

#include "TrackingFilter.hh"
#include "includeOFT/tracking_define.h"

class TrackingFilterOFT : public TrackingFilter
{   
private:
  CntlStruct *local_ptr;
  void InitOFTStructures();
  void FillOFTStructures();
  void MoveOFTStructures();

public:
  TrackingFilterOFT();
  //virtual ~TrackingFilterOFT(); 

  //!In here you open your data file etc;
  virtual Int_t InitDataContainer();

  //! override Process method to call OFT
  virtual Int_t Process();

 };

#endif // ADF_TRACKINGFILTER_OFT_H


