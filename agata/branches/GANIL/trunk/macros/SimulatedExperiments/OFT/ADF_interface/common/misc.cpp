#include "misc.h"

using namespace std;

void stringTrim(std::string &str)
{
  size_t ll = str.length();

  // get read of empty  strings
  if(ll <= 0)
    return;

  // get read of the CR character
  // (e.g. files written on Windows (\r\n) and read on Unix(\n) )
  if(str[ll-1] == '\r') {
    str = str.substr(0, ll-1);
    ll--;
  }

  // discard if now empty
  if(ll <= 0)
    return;

  size_t ii = str.find_first_not_of(" \t");

  // contains only white space
  if(ii == string::npos) {
    str.clear();
    return;
  }

  // remove white space at the beginning
  if(ii > 0) {
    str = str.substr(ii);
    ll = str.length();
  }

  ii = str.find_last_not_of(" \t");

  // remove white space at the end
  if(ii >= 0 && ii < ll-1)
    str = str.substr(0, ii+1);
}

bool stringSplit(std::string &line, const std::string sepa, std::string &keyw)
{
  stringTrim(line);
  if(line.size() == 0)
    return false;
  if(line[0] == '#' )
    return false;
  size_t npos1 = line.find_first_not_of(sepa, 0);
  size_t npos2 = line.find_first_of(sepa, npos1);
  keyw = line.substr(npos1, npos2);
  line = line.substr(npos2+1);
  stringTrim(keyw);
  stringTrim(line);
  return true;
}

bool stringSplit(std::string &line, const std::string sepa, std::string &keyw, std::string &data)
{
  stringTrim(line);
  if(line.size() == 0)
    return false;
  if(line[0] == '#' )
    return false;
  size_t npos1 = line.find_first_not_of(sepa, 0);
  size_t npos2 = line.find_first_of(sepa, npos1);
  keyw = line.substr(npos1, npos2);
  data = line.substr(npos2+1);
  stringTrim(keyw);
  stringTrim(data);
  return true;
}

bool stringIncrement(std::string &fn, int count)
{
  if(count == 0)
    return false;

  size_t lf = fn.size();
  if(lf == string::npos || lf < 1)
    return false;

  if(count > 0) {
    // increment
    for(int nn = 0; nn < count; nn++) {
      for(size_t ll = lf-1; ll > 0; ll--) {
        if(fn[ll] >= '0' && fn[ll] < '9') {
          fn[ll]++;
          break;
        }
        else if(fn[ll] == '9') {
          fn[ll] = '0';
        }
        else if(fn[ll] >= 'a' && fn[ll] < 'z') {
          fn[ll]++;
          break;
        }
        else if(fn[ll] == 'z') {
          fn[ll] = 'a';
        }
        else if(fn[ll] >= 'A' && fn[ll] < 'Z') {
          fn[ll]++;
          break;
        }
        else if(fn[ll] == 'Z') {
          fn[ll] = 'A';
        }
        else {
          return false;
        }
      }
    }
  }
  else {
    // decrement
    for(int nn = 0; nn < -count; nn++) {
      for(size_t ll = lf-1; ll > 0; ll--) {
        if(fn[ll] >= '0' && fn[ll] < '9') {
          fn[ll]--;
          break;
        }
        else if(fn[ll] == '0') {
          fn[ll] = '9';
        }
        else if(fn[ll] >= 'a' && fn[ll] < 'z') {
          fn[ll]++;
          break;
        }
        else if(fn[ll] == 'a') {
          fn[ll] = 'z';
        }
        else if(fn[ll] >= 'A' && fn[ll] < 'Z') {
          fn[ll]++;
          break;
        }
        else if(fn[ll] == 'A') {
          fn[ll] = 'Z';
        }
        else 
          return false;
      }
    }
  }
  return true;
}

bool stringIncrement(std::string &fn, const std::string sp, int count)
{
  if(count == 0)
    return false;

  size_t lf = fn.size();
  if(lf == string::npos || lf < 1)
    return false;

  size_t lff = (int)fn.find_last_of(sp);
  if(lff == string::npos)
    return false;

  string fnn = fn.substr(0, lff);
  if(stringIncrement(fnn, count)) {
    fn = fnn + fn.substr(lff);
    return true;
  }

  return false;

}
