/* PSA-actor base class, J. Ljungvall 2008,
based on implementation by Olivier Stezowski*/

#include "TrackingFilter.hh"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cmath>

#include "misc.h"

using namespace ADF;
using namespace std;

string TrackingFilter::fConfPath="./";
string TrackingFilter::fWhichTrackingFilter="OFT";
string TrackingFilter::fWhichRotoTranslations="CrystalPositionLookUpTable";
string TrackingFilter::fOdirPrefix = "./";
Int_t  TrackingFilter::fMajorKey = 1;
Int_t  TrackingFilter::fMinorKey = 0;
Int_t  TrackingFilter::fMajorFrame = 0;
Int_t  TrackingFilter::fMinorFrame = 0;
Int_t  TrackingFilter::fMajorPSAKey = 1;
Int_t  TrackingFilter::fMinorPSAKey = 0;
Int_t  TrackingFilter::fMajorPSAFrame = 0;
Int_t  TrackingFilter::fMinorPSAFrame = 0;
double TrackingFilter::fMGS000[3] = {0, 0, 0};
double TrackingFilter::fSource[3] = {0, 0, 0};
double TrackingFilter::fRecoil[4] = {0, 0, 1., 0};
bool   TrackingFilter::fVerbose = false;

const float eScale = 1.f;
const int specLenE = 32*1024;
const int intmax = 800;
const UInt_t afterHowMany = 10000;

TrackingFilter::TrackingFilter() :
fFramePSA(NULL),
fFrameTRACK(NULL),
fTrigger("Event Data PSA"), 
fTriggerEvent("Event Data"),
fTriggerPSA("Data PSA"),
fDataContainer(NULL)
{
  pEXYZ = NULL;
  rotTr = NULL;
  pGams = NULL;
  number_of_hits   = 0;
  number_of_gammas = 0;
  number_of_event_frames   = 0;
  number_of_psa_frames     = 0;
  number_of_tracked_gammas = 0;

  crystal_id     = 0;
  crystal_status = 0;
  CoreE[0] = CoreE[1] = 0;
  evnumber  = 0; 
  timestamp = 0;

  fp_conf  = NULL;
  fp_event = NULL;
  fp_hits  = NULL;

  OftSpec_EE = NULL;
  trackCount = 0;

  RecoilVc = 0;
  RecoilGm = 1;
  RecoilCx = 0;
  RecoilCy = 0;
  RecoilCz = 1.;

#ifdef _FromGRU_
  Int_t port=9095;
  SpectraDB = new GSpectra();
  NetworkRoot = new GNetServerRoot (SpectraDB);
  NetworkRoot->SetPort(port);
  SpectraDB->SetfDefaultHostPort(port);
  SpectraDB->SetfDefaultHostName((char*)gSystem->HostName());
  NetworkRoot->SetVerbose(0);

  HitDistrib          = new TH1I("HitDistrib","Number of hits", 100, 0, 99);
  TrackedEnergies     = new TH1F("Trackedenergies",    "energy",    32768,0,32767);
  TrackedEnergiesCorr = new TH1F("TrackedenergiesCorr","energyCorr",32768,0,32767);
  CE_Tracking         = new TH1F("SumHitEnergy","SumHitEnergy",32768,0,32767);
  CC3D_BefAftRT       = new TH3F("Hist_position_befaft_transform","Hist_position_befaft_transform",256,-200,200,256,-200,200,256,-300,50);

  SpectraDB->AddSpectrum (HitDistrib);
  SpectraDB->AddSpectrum (TrackedEnergies);
  SpectraDB->AddSpectrum (TrackedEnergiesCorr);
  SpectraDB->AddSpectrum (CE_Tracking);
  SpectraDB->AddSpectrum (CC3D_BefAftRT);  
  
  NetworkRoot->StartGNetServer(false);
#endif
  
}

TrackingFilter::~TrackingFilter() 
{  
  WriteSpectra();
#ifdef _FromGRU_  
  delete HitDistrib;
  delete TrackedEnergies;
  delete TrackedEnergiesCorr;
  delete CE_Tracking;
  delete CC3D_BefAftRT;
#endif
} 

void TrackingFilter::process_config(const Char_t *directory_path, UInt_t *error_code)
{
  // first init narval stuff
  NarvalInterface::process_config(directory_path, error_code);
  if( *error_code )
    return;

  // now init your stuff
  fConfPath = string(directory_path);
  if( fConfPath.size() > 0 && fConfPath.at(fConfPath.size()-1) != '/' ) fConfPath += '/';
  string configfileName = fConfPath+"TrackingFilter.conf";
  ifstream configfile( configfileName.data() );
  if(!configfile.good()) {
    cout << "Error opening " << configfileName << endl << flush;
    *error_code = 102;
    return;
  }
  cout << "\nTrackingFilter::process_config() reading from --> " << configfileName << endl;

  string line, keyw, data;
  bool ok = true;
  while(getline(configfile, line)) {
    if(!stringSplit(line, " \t", keyw, data))
      continue;       // empty or comment lines

    cout << line;
    ok = false;
    if (keyw == "TrackingType") {
      ok = data.size() > 0;
      fWhichTrackingFilter  = data;
    }
    else if(keyw == "MajorKey") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMajorKey);
    }
    else if(keyw == "MinorKey") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMinorKey);
    }
    else if(keyw == "MajorFrame") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMajorFrame);
    }
    else if(keyw == "MinorFrame") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMinorFrame);
    }
    else if(keyw == "MajorPSAKey") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMajorPSAKey);
    }
    else if(keyw == "MinorPSAKey") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMinorPSAKey);
    }
    else if(keyw == "MajorPSAFrame") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMajorPSAFrame);
    }
    else if(keyw == "MinorPSAFrame") {
      ok = 1 == sscanf(data.c_str(), "%d", &fMinorPSAFrame);
    }
    else if(keyw == "RotoTranslations") {
      ok = data.size() > 0;
      fWhichRotoTranslations  = data;
    }
    else if(keyw == "RecoilBeta") {
      ok = 1 == sscanf(data.c_str(), "%lf", &fRecoil[3]);
    }
    else if(keyw == "RecoilDirection") {
      ok = 3 == sscanf(data.c_str(), "%lf %lf %lf", &fRecoil[0], &fRecoil[1], &fRecoil[2]);
    }
    else if(keyw == "SourcePosition") {
      ok = 3 == sscanf(data.c_str(), "%lf %lf %lf", &fSource[0], &fSource[0], &fSource[0]);
    }
    else if(keyw == "WriteDataPrefix") {
      ok = data.size() > 0;
      fOdirPrefix = data;
    }
    else if(keyw == "Verbose") {
      fVerbose = true;
      ok = true;
    }
    else {
      cout << "   --> ignored";
      ok = true;
    }

    if(!ok) {
      cout << "   --> missing argument(s)" << endl << flush;
      *error_code = 103;
      return;
    }
    else {
      cout << endl;
    }
  }
}

Int_t TrackingFilter::SetInput()
{

  number_of_hits = 0;
 
  /************* fTriggerPSA **********************/
  // i.e. output of the PSA without the global event builder
  // In this case there is only one PSA frame to be decoded
  if( fTriggerPSA.IsFired() ) {
    //printf("trigger data:psa is fired \n");
    fFramePSA = dynamic_cast< PSAFrame* > ( fTriggerPSA.GetInputFrame(0) );
    if(fFramePSA->Read() == 0)
      return 17;          // ??
    if(crystal_status)    // flagged as bad in previous filters
      return 0;           // nothing was done but this is not an error
    GetDataPSA();         // extract the needed data from the PSA frame
    return 0;             // normal return (but if it is possible to have individual PSA frames
                          // and triggered frames at the same time one should proceed to the rest)
  }

  // the rest is for the composite frames after the global event-builder

  AgataCompositeFrame *psaframe = NULL;

  /************* fTrigger **********************/
  // this is the actual trigger found after the global event builder
  if( fTrigger.IsFired() ) {
    //printf("************** trigger event:data:psa is fired ****************** \n");
    psaframe = dynamic_cast< AgataCompositeFrame* > ( fTrigger.GetInputFrame(0) );
    number_of_event_frames++;
    //Char_t buffer[20];
    //int loc_error_code;
    //sprintf(buffer, "%d", number_of_event_frames);
    //ada_set_parameter("number_of_event_frames", buffer, &loc_error_code);
  }

  /************* fTriggerEvent **********************/
  // is this really necessary ??
  if( fTriggerEvent.IsFired() ) {
    //printf("trigger event:data is fired \n");
    psaframe = dynamic_cast< AgataCompositeFrame* > ( fTriggerEvent.GetInputFrame(1) );
  }

  if(psaframe == NULL) {
    return 1;     // no frames ==> is this an error ?? If not, should return 0
  }

  psaframe->Scan();

  int nbSubFrames = psaframe->GetNbSubFrame();
  number_of_psa_frames += nbSubFrames;
  //Char_t buffer[20];
  //int loc_error_code;
  //sprintf(buffer, "%d", number_of_psa_frames);
  //ada_set_parameter("number_of_psa_frames", buffer, &loc_error_code);

  // loop on subframes
  for(Int_t nsf = 0; nsf < nbSubFrames; nsf++) {
    // Attach subframe to fFramePSA and load the ADFObjects in memory
    psaframe->LinkSubFrame(nsf, fFramePSA);
    if( fFramePSA->Read() == 0 )
      break;              // probably this should be returned as an error (see the 17 above) 
    if(crystal_status)    // flagged as bad in previous filters
      continue;           // go to the next
    GetDataPSA();         // extract the needed data from this PSA frame
  }

  return 0;
}

void TrackingFilter::GetDataPSA()
{
    evnumber  = ((AgataKey *)fFramePSA->GetKey())->GetEventNumber();
    timestamp = ((AgataKey *)fFramePSA->GetKey())->GetTimeStamp();  

    PSAInterface *data = fFramePSA->Data();

    int nbhits = data->GetNbHits();

#ifdef _FromGRU_
    HitDistrib->Fill (data->GetNbHits());
#endif

    // loop over number of interaction points in fFramePSA
    double esumhits = 0.;
    exyzHit *pt = pEXYZ + number_of_hits;
    for(Int_t nh = 0; nh < nbhits; nh++, pt++) {
      const Hit *ahit = data->GetHit(nh);
      pt->E  = ahit->GetE();  // passed as keV (even if officially still in 0.1 keV units) 
      esumhits += pt->E;      // build the sum-of-segments energy
      ahit->GetXYZ(pt->X, pt->Y, pt->Z);
      pt->Id = crystal_id;
      // until the definition of the PSA frame is modified to pass this as an integer,
      // this information (useful to e.g. imaging) is passed into the DE field. 
      pt->Sg = int(ahit->GetDE() + 0.001);
      
#ifdef _FromGRU_      
      CC3D_BefAftRT ->Fill( pt->X, pt->Y, pt->Z);
#endif
      
      Transform(pt, crystal_id);

#ifdef _FromGRU_      
      CC3D_BefAftRT ->Fill(pt->X, pt->Y, pt->Z);
#endif

    }
    number_of_hits += nbhits;

#ifdef _FromGRU_
    CE_Tracking->Fill(esumhits*10);
#endif

    //Char_t buffer[20];
    //int loc_error_code;
    //sprintf(buffer, "%d", number_of_hits);
    //ada_set_parameter("input_number_of_hits", buffer, &loc_error_code);

    OftSpec_EE->Incr(int(eScale*CoreE[0]), 0);    // energy of the core
    OftSpec_EE->Incr(int(eScale*esumhits), 1);    // sum energy of the segments
}

Int_t TrackingFilter::SetOutput()
{

  TrackedFrame *trackframe=NULL;

  if( fTriggerPSA.IsFired() ) {
    trackframe = dynamic_cast< TrackedFrame * > (fTriggerPSA.GetOutputFrame());
  }
  if( fTrigger.IsFired() ) {
    trackframe = dynamic_cast< TrackedFrame * > (fTrigger.GetOutputFrame());
  }
  if( fTriggerEvent.IsFired() ) {
    trackframe = dynamic_cast< TrackedFrame * > (fTriggerEvent.GetOutputFrame());
  }
  if (trackframe == NULL)
    {
      return 3;
    }

  GammaTrackedInterface *data = trackframe->Data();

  trackframe->Reset(); // MANDATORY to start filling a new Frame to set # of gammas to 0

  ///////////////////////////////////////////////////////////////
  // evnum should be the same for all items (if valid)
  // But, which tstamp should be written out ??
  // probably all of them ==> should keep the PSA frame(s)
  // At the moment save the last obtained from the PSA frame(s)
  ///////////////////////////////////////////////////////////////

  ((AgataKey *)trackframe->GetKey())->SetEventNumber(evnumber);
  ((AgataKey *)trackframe->GetKey())->SetTimeStamp(timestamp);  

  number_of_tracked_gammas = number_of_tracked_gammas + number_of_gammas;

  //Char_t buffer[20];
  //Int_t loc_error_code;
  //sprintf(buffer, "%d", local_ptr->mult);
  //ada_set_parameter("output_number_of_gammas", buffer, &loc_error_code);
  //sprintf(buffer, "%d", number_of_tracked_gammas);
  //ada_set_parameter("number_of_tracked_frames", buffer, &loc_error_code);

  // loop on tracked gammas in the event
  trGamma *pg = pGams;
  for(Int_t i = 0; i < number_of_gammas; i++, pg++ ) {
    TrackedHit *ahit = data->NewGamma();
    ahit->SetE(pg->E);  // passed over as keV (even if officially still in 0.1 keV units)
    ahit->GetHit(0)->SetXYZ(pg->X1, pg->Y1, pg->Z1); 
    ahit->GetHit(1)->SetXYZ(pg->X2, pg->Y2, pg->Z2);
    ahit->SetStatus(0);

    if(fp_event)    // write tracked gammas to file (in keV mm)
      fprintf(fp_event, "%.1f %.1f %.1f %.1f %.1f %.1f %.1f\n",
        pg->E, pg->X1, pg->Y1, pg->Z1, pg->X2, pg->Y2, pg->Z2);


    OftSpec_EE->Incr(int(eScale*pg->E), 2);
    OftSpec_EE->Incr(int(eScale*pg->E), pg->nH == 1 ? 3 : 4);

#ifdef _FromGRU_
    if(pg->nH > 1)
      TrackedEnergies->Fill(eScale*pg->E);
#endif

    if(RecoilVc > 0) {
      double Ecorr = DopplerCorrect(pg);
      OftSpec_EE->Incr(int(eScale*Ecorr), 5);
#ifdef _FromGRU_
    if(pg->nH > 1)
      TrackedEnergiesCorr->Fill(eScale*Ecorr);
#endif
    }
  }
  
  Int_t nwritten = trackframe->Write();
  if(nwritten == 0)
    return 1;   // error
  else
    return 0;   // normal
}

Int_t TrackingFilter::Process()
{
  cout << "WARNING!! Empty TrackingFilter::Process()" << endl << flush;
  trackCount++;
  return 0;
}

void TrackingFilter::process_block(
                      void   *input_buffer, UInt_t  size_of_input_buffer,
                      void   *output_buffer, UInt_t  size_of_output_buffer,
                      UInt_t *used_size_of_output_buffer,
                      UInt_t *error_code)
{
  OneBlock inblock(1, ConfAgent::kRead);
  OneBlock oublock(1, ConfAgent::kWrite);

  inblock.SetBlock((Char_t *)input_buffer,  size_of_input_buffer);
  oublock.SetBlock((Char_t *)output_buffer, size_of_output_buffer);
  
  *error_code = TrackingFilter::ProcessBlock(inblock, oublock);
  *used_size_of_output_buffer = oublock.GetSize();
}

UInt_t TrackingFilter::ProcessBlock(ADF::FrameBlock &in, ADF::FrameBlock &out)
{
  // attach the input/output buffer to the FrameIO system
  fFrameIO.Attach(&in, &out);

  // start the processing
  UInt_t error_code = 0;

  cout << "TrackingFilter::ProcessBlock at trackCount = " << trackCount << endl;

  while( fFrameIO.Notify() ) { // process the input 

    if( SetInput() ) {
      error_code = 1;
      Log.GetProcessMethod() = "TrackingFilter::ProcessBlock";
      Log << error  << " During : SetInput()" << dolog;
      break;
    } 

    if(number_of_hits == 0)   // not processed
      continue;

    // process the input buffer
    if( Process() ) {
      error_code = 2;
      Log.GetProcessMethod() = "TrackingFilter::ProcessBlock";
      Log << error  << " During : Process()" << dolog;
      break;
    } 

    // fill the output buffer
    if( SetOutput() ) {
      error_code = 3;
      Log.GetProcessMethod() = "TrackingFilter::ProcessBlock";
      Log << error  << " During : SetOutput()" << dolog;
      break;
    } 

    // send the produced frame to the ouput
    if( !fFrameIO.Record() ) {
      error_code = 4;
      Log.GetProcessMethod() = "TrackingFilter::ProcessBlock";
      Log << error  << " During : Record()" << dolog;
      break;
    } 

    if(trackCount && !(trackCount%afterHowMany)) {
      WriteSpectra();
    }

  }

  fFrameIO.Detach(&in, &out);

  return error_code;
}	      

void TrackingFilter::WriteSpectra()
{
  if(OftSpec_EE) {
    OftSpec_EE->write();
    cout << " " << OftSpec_EE->getFileName()
         << " " << OftSpec_EE->getNumSpec1() << "*" << OftSpec_EE->getLenSpec() << endl;
  }
}

// still to be adjusted like the other filter base-classes
void TrackingFilter::process_initialise (UInt_t *error_code)
{
    // log related
  Log.ClearMessage();
  Log.GetProcessMethod() = "process_initialise";
  *error_code = 0;

  // this should be moved to a configuration file
  Version vkey(4,0), vframe(65000,0), vkey1(1,0), vkey2(0,0);

  // definition of the frames needed by the tracking algorithm
  Frame *frame;

  // Trigger 1, trigger on evolutive event data PSA frames - if true frames are consumed 
  fTrigger.Add("event:data:psa", vkey, vkey2, true);
  fTrigger.SetOutputFrame("data:tracked", vkey, vframe);
  if( !fFrameIO.Register(&fTrigger) ) {
    cout << " ********  trigger 1 register problem " << endl;
    *error_code = 100;
    return;
  }

  // Trigger 2, trigger on data frames WITH event data psa frames inside
  fTriggerEvent.Add("event:data", vkey, vkey1, false);
  fTriggerEvent.Add("event:data:psa", vkey, vkey2, true);
  fTriggerEvent.SetOutputFrame("data:tracked", vkey, vframe);
  if( !fFrameIO.Register(&fTriggerEvent) ) {
    cout << "  ******    trigger 2 register problem " << endl;
    *error_code = 100;
    return;
  }

  // Trigger 3, trigger on evolutive psa frames. 
  // Just here to have the current definition of the psa frame on the data flow 
  frame = fTriggerPSA.Add("data:psa", vkey, vframe, true, false);
  fTriggerPSA.SetOutputFrame("data:tracked", vkey, vframe);
  if( !fFrameIO.Register(&fTriggerPSA) ) {
    cout << " *********  trigger 3 register problem " << endl;
    *error_code = 100;
    return;
  }

  // the frame is allocated, linking globals.
  if( frame ) {
    fFramePSA = dynamic_cast< PSAFrame * > (frame);
    GObject *glob = fFramePSA->Data()->Global();
    glob->LinkItem("CrystalID",     "US", &crystal_id); 
    glob->LinkItem("CrystalStatus", "US", &crystal_status); 
    glob->LinkItem("Core1",         "F",  &CoreE[0]); 
    glob->LinkItem("Core2",         "F",  &CoreE[1]);
  }

  // ready to run ..
  fFrameIO.SetStatus(BaseFrameIO::kIdle);

  cout << "\nTrackingFilter::process_initialise() --> " << endl;

  // algo-related initializations 
  InitGenStructures();  // parts managed by the base class

  // opening file where tracked energies will be written out
  event_file = fOdirPrefix + "Oft_TrackedEnergies";
  fp_event = fopen(event_file.c_str(), "w");
  printf("tracked energies will be written to %s\n", event_file.c_str());

#ifdef WRITE_INPUT_HITS
  // opening file to write the hits at the input of OFT in mgt format
  hits_file = fOdirPrefix + "Oft_Hits";
  fp_hits = fopen(hits_file.c_str(), "w");
  printf("input hits will be written to %s\n", hits_file.c_str());
  fprintf(fp_hits, "AGATA 6.6.6\n");
  fprintf(fp_hits, "DISTFACTOR 1\n");
  fprintf(fp_hits, "ENERFACTOR 1\n");
  fprintf(fp_hits, "GEOMETRY\n");
  fprintf(fp_hits, "AGATA\n");
  fprintf(fp_hits, "SUMMARY 235.008 329.202 180 3 6 6 6 6 6 6\n"); 
  fprintf(fp_hits, "ENDGEOMETRY\n");
  fprintf(fp_hits, "$\n");
#endif

  // configuration file to get transformation from relative to world coordinates
  conf_file = fConfPath + fWhichRotoTranslations;
  printf("Opening configuration file %s\n", conf_file.c_str());
  fp_conf = fopen(conf_file.c_str(), "r");
  if(!fp_conf) {
    printf("Could not open configuration file %s\n", conf_file.c_str());
    *error_code = 100;
    return;
  }
  
  int nr = GetRotoTranslations(fp_conf);
  printf("Found %d coefficients\n", nr);
  if(nr < 3) {
    printf("Error reading configuration file %s\n", conf_file.c_str());
    *error_code = 100;
    return;
  }
  fclose(fp_conf);

  number_of_psa_frames     = 0;
  number_of_event_frames   = 0;
  number_of_tracked_gammas = 0;

  // version-specific initializations
  *error_code = InitDataContainer();
  
  //printf("-------------------------------------------------------EXIT TRACKING INITIALISE\n");
}

void TrackingFilter::InitGenStructures()
{
  rotTr = (rotTr3D *)calloc(   180, sizeof(rotTr3D));
  pEXYZ = (exyzHit *)calloc(intmax, sizeof(exyzHit));
  pGams = (trGamma *)calloc(intmax, sizeof(trGamma));
  number_of_hits   = 0;
  number_of_gammas = 0;

  OftSpec_EE = new spec1D<int>("Oft_Spec_EE.ldat", specLenE, 6);

  // for Doppler correction
  if(fRecoil[3] > 0 && fRecoil[3] < 1.) {
    RecoilVc = fRecoil[3];
    RecoilGm = 1. / sqrt(1. - RecoilVc*RecoilVc);
    double dd = (fRecoil[0]*fRecoil[0] + fRecoil[1]*fRecoil[1] + fRecoil[1]*fRecoil[1]);
    if(dd < 1.e-6) {
      cout << "Size of direction cosines vector is too small ==> recoil direction set along z" << endl;
      RecoilCx = RecoilCy = 0;
      RecoilCz = 1.;
    }
    else {
      dd = sqrt(dd);
      RecoilCx = fRecoil[0]/dd;
      RecoilCy = fRecoil[1]/dd;
      RecoilCz = fRecoil[2]/dd;
    }
  }
  else {
    RecoilVc = 0;
    RecoilGm = 1.;
    RecoilCx = 0;
    RecoilCy = 0;
    RecoilCz = 1.;
  }
}

int TrackingFilter::GetRotoTranslations(FILE *fp)
{
  int nd, k;
  int nt = 0;
  Double_t x, y, z;

  while(true) {
    // detector number and translation
    if(fscanf(fp, "%d %d %lf %lf %lf", &nd, &k, &x, &y, &z) != 5)
      break;

    if(nd < 0 || nd >= 180 || k != 0)
      return -1;

    rotTr3D *prt = rotTr + nd;

    prt->TrX = x; // in mm
    prt->TrY = y;
    prt->TrZ = z;

    // matrix elements of rotation
    if(fscanf(fp, "%d  %lf %lf %lf", &k, &x, &y, &z) != 4)
      return -1;
    if(k != 1)
      return -1;

    prt->rXX = x;
    prt->rXY = y;
    prt->rXZ = z;

    if(fscanf(fp, "%d  %lf %lf %lf", &k, &x, &y, &z) != 4)
      return -1;
    if(k != 2)
      return -1;
    prt->rYX = x;
    prt->rYY = y;
    prt->rYZ = z;

    if(fscanf(fp, "%d  %lf %lf %lf", &k, &x, &y, &z) != 4)
      return -1;
    if(k != 3)
      return -1;
    prt->rZX = x;
    prt->rZY = y;
    prt->rZZ = z;

    nt++;
  }
  return nt;
}

double TrackingFilter::DopplerCorrect(trGamma *pg)
{
  double dx = pg->X1 - fSource[0];
  double dy = pg->Y1 - fSource[1];
  double dz = pg->Z1 - fSource[2];
  double dd = dx*dx + dy*dy + dz*dz;
  if(dd == 0)
    return pg->E;
  double cosTheta = (dx*RecoilCx + dy*RecoilCy + dz*RecoilCz)/sqrt(dd);
  double cDoppler = RecoilGm*(1. - RecoilVc * cosTheta);
  return cDoppler*pg->E;
}

void TrackingFilter::process_reset (UInt_t *error_code)
{
  //// OComment
  //// dolog is a manipulator (like cout << endl) defined for a LogMessage
  //// one it is added to the LogMessage, it means : send the message to the LogCollector
  //// if you redefined it as a string, the Logmessage is never flushed to the LogCollector
  //// that is why we had nothing in the TEST.log file

  ////  string dolog;

  //Log.ClearMessage(); Log.GetProcessMethod() = "process_reset"; 
  //fFrameIO.Print(Log()); 
  //// connect to the frameIO
  ////   if ( fNarvalIO ) 
  ////     { delete fNarvalIO; fNarvalIO = NULL; }
  ////   // to get the input/output frameIO
  ////   if ( fFrameCrystal ) 
  ////     { delete fFrameCrystal; fFrameCrystal = NULL; }
  ////   if ( fFramePSA ) 
  ////     { delete fFramePSA; fFramePSA = NULL; }
  ////   if ( fTrigger ) 
  ////     { delete fTrigger; fTrigger = NULL; }	
  //Log << dolog;

  Log.ClearMessage();
  Log.GetProcessMethod() = "process_reset"; 

  NarvalFilter::process_reset(error_code);

  Log << dolog;
}

void TrackingFilter::process_start (UInt_t *error_code)
{
  //string info, dolog;
  //Log << info << " Start the inner loop " << dolog; 
  //Log.GetProcessMethod() = "process_block"; 
  //*error_code = 0;
  Log.GetProcessMethod() = "process_block"; 
  *error_code = 0;

}

void TrackingFilter::process_stop (UInt_t *error_code)
{
  //cout << "process_stop called with GetPID()" << GetPID() << endl;
  //*error_code = 0;
  cout << "process_stop called with GetPID()" << GetPID() << endl;
  *error_code = 0;
}
void TrackingFilter::process_pause (UInt_t *error_code)
{
  //cout << "process_pause called with GetPID()" << GetPID() << endl;
  //*error_code = 0;
  cout << "process_pause called with GetPID()" << GetPID() << endl;
  *error_code = 0;
}
void TrackingFilter::process_resume (UInt_t *error_code)
{
  //cout << "process_resume called with GetPID()" << GetPID() << endl;
  //*error_code = 0;
  cout << "process_resume called with GetPID()" << GetPID() << endl;
  *error_code = 0;
}
