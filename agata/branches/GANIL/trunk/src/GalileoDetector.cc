#include "GalileoDetector.hh"
#include "AgataSensitiveDetector.hh"
#include "G4VSensitiveDetector.hh"
#include "G4SDManager.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4ThreeVector.hh"
#include "G4ExtrudedSolid.hh"
#include "G4TwoVector.hh"
#include "G4Trd.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include <iomanip>
#include <sstream>
#include <iostream>
#include <string>
#include "G4AssemblyVolume.hh"
#include "GalileoAngles.hh"

namespace GalileoGeom  {
    
    // To be easier to understand all position are defined with respect
    // to the Front shielding
    
    // Front shielding
    // Constructed in GalileoDetector::GetGalileoFronting()
    const G4double GalileoFrontingPosition                =   0.000*mm;
    const G4double GalileoFrontingLength                  =   5.200*mm;
    const G4double GalileoFrontingInnerRadius             =  23.301*mm;
    const G4double GalileoFrontingRadiusMin               =  46.41*mm;
    const G4double GalileoFrontingRadiusMax               =  48.20*mm;
    
    // AntiCompton Capsule
    // Constructed in GalileoDetector::GetGalileoACCapsule()
    const G4double GalileoACCapsulePosition               =   5.20*mm;  // AC position with respect to entrance face of Fronting
    const G4double GalileoACCapsuleLength                 = 178.55*mm;  // AC capsule length
    const G4double GalileoACCapsuleEdge1                  =   5.20*mm;  // depth of Edge1
    const G4double GalileoACCapsuleEdge2                  =  17.20*mm;  // depth of Edge2
    const G4double GalileoACCapsuleEdge3                  =  31.29*mm;  // depth of Edge3
    const G4double GalileoACCapsuleEdge4                  =  65.20*mm;  // depth of Edge4
    const G4double GalileoACCapsuleEdge5                  =  97.55*mm;  // depth of Edge5
    const G4double GalileoACCapsuleInnerRadiusMin         =  25.00*mm;  // Inner radius of the AC Capsule at the entrance face
    const G4double GalileoACCapsuleInnerRadius2           =  26.26*mm;  // inner radius at Edge2
    const G4double GalileoACCapsuleInnerRadius3           =  37.62*mm;  // Inner radius at Edge3
    const G4double GalileoACCapsuleInnerRadiusMax         =  43.60*mm;  // Radius at the back face
    const G4double GalileoACCapsuleOuterRadiusMin         =  46.41*mm;  // Outer radius of the AC Capsule at the entrance face
    const G4double GalileoACCapsuleOuterRadius1           =  48.20*mm;  // Outer radius at Edge1
    const G4double GalileoACCapsuleOuterRadius2           =  52.33*mm;  // Outer radius at Edge2
    const G4double GalileoACCapsuleOuterRadius3           =  57.18*mm;  // Outer radius at Edge3
    const G4double GalileoACCapsuleOuterRadius4           =  68.86*mm;  // Outer radius at Edge4
    const G4double GalileoACCapsuleOuterRadius5           =  80.00*mm;  // Outer radius at Edge5
    const G4double GalileoACCapsuleOuterRadiusMax         =  86.00*mm;  // Outer radius at Back face
    const G4double GalileoACCapsuleThickness              =   0.40*mm;
    
    //AntiCompton Crystal
    // Constructed in GalileoDetector::GetGalileoACCrystal()
    const G4double GalileoACCrystalPosition               =  10.75*mm;  // position of the AC with respect to the fronting
    const G4double GalileoACCrystalLength                 = 172.85*mm;  // BGO Length
    const G4double GalileoACCrystalInnerRadiusMin         =  25.61*mm;  // Inner radius of the AC at the entrance face
    const G4double GalileoACCrystalOuterRadiusMin         =  47.67*mm;  // Outer radius of the AC at the entrance face
    const G4double GalileoACCrystalEdge1                  =  11.50*mm;  // depth of Edge1
    const G4double GalileoACCrystalInnerRadius1           =  26.76*mm;  // Inner radius of the AC at the Edge1
    const G4double GalileoACCrystalOuterRadius1           =  51.83*mm;  // Outer radius of the AC at the Edge1
    const G4double GalileoACCrystalEdge2                  =  25.59*mm;  // depth of Edge2
    const G4double GalileoACCrystalInnerRadius2           =  38.12*mm;  // Inner radius of the AC at the Edge2
    const G4double GalileoACCrystalOuterRadius2           =  56.58*mm;  // Outer radius of the AC at the Edge2
    const G4double GalileoACCrystalEdge3                  =  59.50*mm;  // depth of Edge3
    const G4double GalileoACCrystalOuterRadius3           =  68.33*mm;  // Outer radius of the AC at the Edge3
    const G4double GalileoACCrystalEdge4                  =  91.85*mm;  // depth of Edge4
    const G4double GalileoACCrystalOuterRadius4           =  79.50*mm;  // Outer radius of the AC at the Edge4
    const G4double GalileoACCrystalInnerRadiusMax         =  44.10*mm;  // Inner radius of the AC at the back face
    const G4double GalileoACCrystalOuterRadiusMax         =  85.50*mm;  // Outer radius of the AC at the back face
    
    
    // Crystal capsule inner part
    // Constructed in GalileoDetector::GetGalileoGeCapsule()
    const G4double GalileoCrystalCapsulePosition          =  39.20*mm;  // position of the capsule with respect to the fronting
    const G4double GalileoCrystalCapsuleLength            = 240.00*mm;  // capsule length
    const G4double GalileoCrystalCapsuleEdge              =  30.62*mm;  // depth at which starts RadiusMax
    const G4double GalileoCrystalCapsuleThickness         =   2.0*mm;  // capsule thickness
    const G4double GalileoCrystalCapsuleRadiusMin         =  38.00*mm;  // radius of the capsule at the entrance face
    const G4double GalileoCrystalCapsuleRadiusMax         =  43.40*mm;  // radius of the capsule at the back face
    
    // Crystal
    // Constructed in GalileoDetector::GetGalileoGeCrystal()
    const G4double GalileoGeCrystalPosition              =  42.60*mm; // crystal position with respect to fronting
    const G4double GalileoGeCrystalLength	               =  82.00*mm; // cystal length
    const G4double GalileoGeCrystalEdgeDepth             =  25.22*mm; // depth at which starts RadiusMax
    const G4double GalileoGeCrystalRadiusMin             =  33.55*mm; // radius of the crystal at the entrance face
    const G4double GalileoGeCrystalRadiusMax             =  38.00*mm; // radius of the crystal at the back face
    const G4double GalileoGeCrystalHoleDepth             =  15.00*mm; // depth at which starts the hole
    const G4double GalileoGeCrystalHoleRadius            =   5.00*mm; // radius of the hole
    
    
    //Lead Collimator
    const G4double GalileoLeadCollimatorRadiusMin          = 126.00*mm; // Inner radius of the lead shielding
    const G4double GalileoLeadCollimatorRadiusMax          = 186.00*mm; // Inner radius of the lead shielding
    const G4double GalileoLeadCollimatorCentralRadiusMin   = 126.00*mm; // Inner radius of the lead shielding
    const G4double GalileoLeadCollimatorCentralRadiusMax   = 186.00*mm; // Inner radius of the lead shielding
    
    
    const G4double GeDeadLayer =1.88*mm; //3.20*mm;
    const G4double RGalileo = 195*mm;
}

using namespace GalileoGeom;

GalileoDetector::GalileoDetector(G4String anctype, G4String path, G4String name)
{
    //G4cout<<"AG: GalileoDetector, beginning"<<G4endl;
    // base to build one clover (shaped crystal)
    
    AgataDetectorConstructed::readOut   = false;
    fMakeLeadCollimator = false;
    fMakeChamber = false;
    fUserDefinedRadius = 0.*mm;
    iniPath       = path;
    directoryName = name;
    
    
    // Detector Logical volumes
    galileoTC=NULL;
    pGalileoFronting = NULL;
    pGalileoACCrystal = pGalileoACCapsule = NULL;
    pGalileoGeCrystal = pGalileoGeCapsule = NULL;
    //  pGalileoTripleCluster = NULL;
    fTripleCluster=false;
    pGalileoTCCollimator=NULL;
    pGalileoTripleClusterGe.clear();
    pGalileoTripleClusterAC.clear();
    pGalileoTripleClusterGeHousing.clear();
    pGalileoTripleClusterACHousing.clear();
    
    // Detector material definition
    matFronting = matCapsule = matGeCrystal = matACCrystal = NULL;
    matVacuum = NULL;
    matFrontingName  = "Tungsten";
    matGeCrystalName = "Germanium";
    matCapsuleName   = "Aluminium";
    matACCrystalName = "BGO";
    if(!getenv("G4GDMLPATH")){
        G4cerr << "Error! GDML Path is missing and I need it to construct GALILEO triple clusters: please define G4GDMLPATH" << G4endl;
        std::exit(10);
    }
    gdml_path        = std::string(getenv("G4GDMLPATH"))+G4String("/GALILEO/TripleCluster/"); //(modified by Marc Sept2017)
    
    //AgataDetectorConstructed::nDets = 0;
    //AgataDetectorConstructed::nClus = 0;
    //AgataDetectorConstructed::iCMin = 0;
    //AgataDetectorConstructed::iGMin = 0;
    //Commented by AO
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    SDman->
    AddNewDetector(bgoSD=new AgataSensitiveDetector("Galileo",
                                                    "/BGO/all",
                                                    "BGOCollection",false ) );
    
    
    // Lead collimator attached to the structure
    matLeadCollimatorName = "Lead";
    matLeadCollimator = NULL;
    
    ancType = anctype;
    theAncillary = NULL;
    if(ancType!="1 0") {
        theAncillary = new AgataDetectorAncillary(ancType,"",
                                                  "/Galileo");
        
    }
    
    myMessenger = new GalileoDetectorMessenger(this);
    fExtraPhaseIs.clear();
    //We also make a table over Galileo angles
    std::ofstream oa("GalileoAngles.txt");
    for(int detnb=0; detnb<40; detnb++){
        G4RotationMatrix rm;
        if(galileoangles[detnb][0]>90) rm.rotateZ(180.*deg);
        rm.rotateY(galileoangles[detnb][0]*deg);
        rm.rotateZ(galileoangles[detnb][1]*deg);
        rm.rotateY(galileoangles[detnb][2]*deg);
        rm.rotateZ(galileoangles[detnb][3]*deg);
        fAllDetAngles[detnb] = rm(G4ThreeVector(0,0,1));
        G4double theta  = fAllDetAngles[detnb].theta()/deg;
        G4double phi = (fAllDetAngles[detnb].phi()/deg>0 ?
                        fAllDetAngles[detnb].phi()/deg :
                        fAllDetAngles[detnb].phi()/deg+360.);
        oa << detnb << " " << theta << " " << phi << std::endl;
    }
    oa.close();
}

GalileoDetector::~GalileoDetector()
{
    G4cout << "### ~GalileoDetector "<< G4endl;
    delete myMessenger;
}

G4int GalileoDetector::FindMaterials()
{
    // search the material by their names
    // lead collimator
    G4cout << "Searching the materials ... ";
    matLeadCollimator = G4Material::GetMaterial(matLeadCollimatorName);
    if(matLeadCollimator==NULL){
        G4cout << " ***** NOT KNOWN **** " <<  matLeadCollimatorName  << " *****" << G4endl;
        exit(-1);
    }
    
    // Fronting
    matFronting = G4Material::GetMaterial(matFrontingName);
    if ( matFronting == NULL ) {
        G4cout << " ***** NOT KNOWN **** " <<  matFrontingName  << " *****" << G4endl;
        exit(-1);
    }
    
    // Capsules
    matCapsule      = G4Material::GetMaterial(matCapsuleName); // should be ok .. standard in Galileo
    if(matCapsule==NULL) {
        G4cout << " ***** NOT KNOWN **** " <<  matCapsuleName  << " *****" << G4endl;
        exit(-1);
    }
    
    matVacuum       = G4Material::GetMaterial("Vacuum");
    if(matVacuum==NULL) {
        G4cout << " ***** NOT KNOWN **** " <<  "Vacuum"  << " *****" << G4endl;
        exit(-1);
    }
    
    matACCrystal = G4Material::GetMaterial(matACCrystalName); // in case BGO is not known
    if ( matACCrystal == NULL ) {
        // Germanium isotopes
        G4Isotope* Ge70 = new G4Isotope("Ge70", 32, 70, 69.9242*g/mole);
        G4Isotope* Ge72 = new G4Isotope("Ge72", 32, 72, 71.9221*g/mole);
        G4Isotope* Ge73 = new G4Isotope("Ge73", 32, 73, 72.9235*g/mole);
        G4Isotope* Ge74 = new G4Isotope("Ge74", 32, 74, 73.9212*g/mole);
        G4Isotope* Ge76 = new G4Isotope("Ge76", 32, 76, 75.9214*g/mole);
        // germanium defined via its isotopes
        G4Element* elGe = new G4Element("Germanium","Ge", 5);
        elGe->AddIsotope(Ge70, 0.2123);
        elGe->AddIsotope(Ge72, 0.2766);
        elGe->AddIsotope(Ge73, 0.0773);
        elGe->AddIsotope(Ge74, 0.3594);
        elGe->AddIsotope(Ge76, 0.0744);
        
        matACCrystal = new G4Material("BGO", 7.13*g/cm3, 3);
        matACCrystal->AddElement( new G4Element("Bismuth",  "Bi", 83., 208.98038*g/mole), 4);
        matACCrystal->AddElement(elGe, 3);
        matACCrystal->AddElement( new G4Element("Oxigen",	"O",  8.,  15.9994 *g/mole) , 12);
    }
    
    matGeCrystal      = G4Material::GetMaterial(matGeCrystalName); // should be ok .. standard in Galileo
    if(matGeCrystal==0) {
        G4cout << " ***** NOT KNOWN **** " <<  matGeCrystalName  << " *****" << G4endl;
        exit(-1);
    }
    
    if (matGeCrystal && matACCrystalName) {
        G4cout << "\n ---->  The detector material are "
        << matGeCrystal << " " << matACCrystalName << G4endl;
    }
    else {
        G4cout << " Could not find the correct materials " <<  G4endl;
        G4cout << " Could not build the detector! " << G4endl;
        return 1;
    }
    G4cout << "done !" << G4endl;
    return 0;
}


G4LogicalVolume *GalileoDetector::GetGalileoFronting()
{
    
    char sName[40];
    G4int nbSlice = 2;
    
    G4double zSlice[2]  = {GalileoFrontingPosition,
        GalileoFrontingPosition+GalileoFrontingLength};
    G4double InnRad[2]  = {GalileoFrontingInnerRadius,
        GalileoFrontingInnerRadius};
    G4double OutRad[2]  = {GalileoFrontingRadiusMin,
        GalileoFrontingRadiusMax};
    
    if(pGalileoFronting==NULL){
        sprintf(sName,"aGalileoFronting");
        G4Polycone *GalileoCollimator=new G4Polycone(G4String(sName),0.0*deg,360.0*deg, nbSlice,zSlice,InnRad,OutRad);
        pGalileoFronting  = new G4LogicalVolume( GalileoCollimator, matFronting, G4String(sName), 0, 0, 0 );
        G4VisAttributes *pGalileoCollimatorVA = new G4VisAttributes(G4Colour(0.3, 0.3, .3));
        pGalileoFronting->SetVisAttributes(pGalileoCollimatorVA);
    }
    return pGalileoFronting;
    
}

// Anticompton Capsules
G4LogicalVolume *GalileoDetector::GetGalileoACCapsule()
{	
    char sName[40];
    
    G4int nbSlice = 7;
    G4double zSlice[7] = {GalileoACCapsulePosition,
        GalileoACCapsulePosition+GalileoACCapsuleEdge1,
        GalileoACCapsulePosition+GalileoACCapsuleEdge2,
        GalileoACCapsulePosition+GalileoACCapsuleEdge3,
        GalileoACCapsulePosition+GalileoACCapsuleEdge4,
        GalileoACCapsulePosition+GalileoACCapsuleEdge5,
        GalileoACCapsulePosition+GalileoACCapsuleLength};
    
    G4double InnRad[7] = {GalileoACCapsuleInnerRadiusMin,
        GalileoACCapsuleInnerRadiusMin,
        GalileoACCapsuleInnerRadius2,
        GalileoACCapsuleInnerRadius3,
        GalileoACCapsuleInnerRadiusMax,
        GalileoACCapsuleInnerRadiusMax,
        GalileoACCapsuleInnerRadiusMax};
    
    G4double OutRad[7] = {GalileoACCapsuleOuterRadiusMin,
        GalileoACCapsuleOuterRadius1,
        GalileoACCapsuleOuterRadius2,
        GalileoACCapsuleOuterRadius3,
        GalileoACCapsuleOuterRadius4,
        GalileoACCapsuleOuterRadius5,
        GalileoACCapsuleOuterRadiusMax};
    
    
    G4double zSliceIn[7] = {GalileoACCrystalPosition-GalileoACCapsuleThickness,
        GalileoACCapsulePosition+GalileoACCapsuleEdge1,
        GalileoACCapsulePosition+GalileoACCapsuleEdge2,
        GalileoACCapsulePosition+GalileoACCapsuleEdge3,
        GalileoACCapsulePosition+GalileoACCapsuleEdge4,
        GalileoACCapsulePosition+GalileoACCapsuleEdge5,
        GalileoACCapsulePosition+GalileoACCapsuleLength-GalileoACCapsuleThickness};
    
    G4double InnRadIn[7] = {GalileoACCapsuleInnerRadiusMin+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadiusMin+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadius2+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadius3+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadiusMax+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadiusMax+GalileoACCapsuleThickness,
        GalileoACCapsuleInnerRadiusMax+GalileoACCapsuleThickness};
    
    G4double OutRadIn[7] = {GalileoACCapsuleOuterRadiusMin-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadius1-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadius2-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadius3-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadius4-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadius5-GalileoACCapsuleThickness,
        GalileoACCapsuleOuterRadiusMax-GalileoACCapsuleThickness};
    
    if ( pGalileoACCapsule == NULL ){
        sprintf(sName, "aGalileoACCapsOut");
        G4Polycone *GalileoACCapsOut =
        new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
        sprintf(sName, "aGalileoACCapsIn");
        G4Polycone *GalileoACCapsInt =
        new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSliceIn,InnRadIn,OutRadIn);
        sprintf(sName, "subtractedCaps");
        G4SubtractionSolid *subtractedCaps =  new G4SubtractionSolid(G4String(sName),GalileoACCapsOut,GalileoACCapsInt,0,G4ThreeVector(0.,0.,0.));
        sprintf(sName, "aGalileoACCaps");
        pGalileoACCapsule  = new G4LogicalVolume( subtractedCaps, matCapsule,G4String(sName), 0, 0, 0 );
        G4VisAttributes *pBGOCapsuleVA = new G4VisAttributes(G4Colour(0.7, 0.7, 0.7));
        pGalileoACCapsule->SetVisAttributes(pBGOCapsuleVA);
    }
    return pGalileoACCapsule;
    
}

//Anticompton crystals
G4LogicalVolume *GalileoDetector::GetGalileoACCrystal()
{	
    char sName[40];
    
    G4int nbSlice = 6;
    G4double zSlice[6] = {GalileoACCrystalPosition,
        GalileoACCrystalPosition+GalileoACCrystalEdge1,
        GalileoACCrystalPosition+GalileoACCrystalEdge2,
        GalileoACCrystalPosition+GalileoACCrystalEdge3,
        GalileoACCrystalPosition+GalileoACCrystalEdge4,
        GalileoACCrystalPosition+GalileoACCrystalLength};
    
    G4double InnRad[6] = {GalileoACCrystalInnerRadiusMin,
        GalileoACCrystalInnerRadius1,
        GalileoACCrystalInnerRadius2,
        GalileoACCrystalInnerRadiusMax,
        GalileoACCrystalInnerRadiusMax,
        GalileoACCrystalInnerRadiusMax};
    
    G4double OutRad[6] = {GalileoACCrystalOuterRadiusMin,
        GalileoACCrystalOuterRadius1,
        GalileoACCrystalOuterRadius2,
        GalileoACCrystalOuterRadius3,
        GalileoACCrystalOuterRadius4,
        GalileoACCrystalOuterRadiusMax};
    
    if ( pGalileoACCrystal == NULL ){
        sprintf(sName, "aGalileoACCryst");
        G4Polycone *GalileoACCryst =
        new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
        pGalileoACCrystal  = new G4LogicalVolume( GalileoACCryst, matACCrystal, G4String(sName), 0, 0, 0 );
        G4VisAttributes *pBGOCrystVA = new G4VisAttributes(G4Colour(0.7, 0.7, 0.7));
        pGalileoACCrystal->SetVisAttributes(pBGOCrystVA);
    }
    return pGalileoACCrystal;
    
}


G4LogicalVolume *GalileoDetector::GetGalileoGeCapsule()
{
    char sName[40];
    G4int nbSlice=7;
    G4double zSlice[7]={	GalileoCrystalCapsulePosition,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleEdge,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleLength-GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleLength-GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsulePosition+GalileoCrystalCapsuleLength };
    G4double InnRad[7]={	0.0*mm,
        0.0*mm,
        GalileoCrystalCapsuleRadiusMin-GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsuleRadiusMax-GalileoCrystalCapsuleThickness,
        GalileoCrystalCapsuleRadiusMax-GalileoCrystalCapsuleThickness,
        0.0*mm,
        0.0*mm};
    
    G4double OutRad[7]={	GalileoCrystalCapsuleRadiusMin,
        GalileoCrystalCapsuleRadiusMin,
        GalileoCrystalCapsuleRadiusMin,
        GalileoCrystalCapsuleRadiusMax,
        GalileoCrystalCapsuleRadiusMax,
        GalileoCrystalCapsuleRadiusMax,
        GalileoCrystalCapsuleRadiusMax};
    if(pGalileoGeCapsule==NULL){
        sprintf(sName,"aGalileoCapsule");
        G4Polycone *aCapsule=new G4Polycone(G4String(sName),0.0*deg,360.0*deg, nbSlice,zSlice,InnRad,OutRad);
        pGalileoGeCapsule  = new G4LogicalVolume( aCapsule, matCapsule, G4String(sName), 0, 0, 0 );
        G4VisAttributes *pCapsuleVA = new G4VisAttributes(G4Colour(0.95, 0.95, 0.05));
        pGalileoGeCapsule->SetVisAttributes(pCapsuleVA);
    }
    return pGalileoGeCapsule;
}

G4LogicalVolume *GalileoDetector::GetGalileoGeCrystal()
{
    char sName[40];
    
    G4double intermediateOutRad1 = GalileoGeCrystalRadiusMin+(GalileoGeCrystalRadiusMax-GalileoGeCrystalRadiusMin)
    *GalileoGeCrystalHoleDepth/GalileoGeCrystalEdgeDepth;
    G4double intermediateOutRad2 = GalileoGeCrystalRadiusMin+(GalileoGeCrystalRadiusMax-GalileoGeCrystalRadiusMin)
    *(GalileoGeCrystalHoleDepth)/GalileoGeCrystalEdgeDepth;
    
    G4int nbSlice = 5;
    G4double zSlice[5] = { GalileoGeCrystalPosition+GeDeadLayer,
        GalileoGeCrystalPosition+GalileoGeCrystalHoleDepth-GeDeadLayer,
        GalileoGeCrystalPosition+GalileoGeCrystalHoleDepth-GeDeadLayer,
        GalileoGeCrystalPosition+GalileoGeCrystalEdgeDepth,
        GalileoGeCrystalPosition+GalileoGeCrystalLength-GeDeadLayer };
    
    G4double InnRad[5] = { 0.0*mm,
        0.0*mm,
        GalileoGeCrystalHoleRadius+GeDeadLayer,
        GalileoGeCrystalHoleRadius+GeDeadLayer,
        GalileoGeCrystalHoleRadius+GeDeadLayer };
    
    G4double OutRad[5] = { GalileoGeCrystalRadiusMin-GeDeadLayer,
        intermediateOutRad1-GeDeadLayer,
        intermediateOutRad2-GeDeadLayer,
        GalileoGeCrystalRadiusMax-GeDeadLayer,
        GalileoGeCrystalRadiusMax-GeDeadLayer };
    
    if ( pGalileoGeCrystal == NULL ){
        sprintf(sName,"aGalileoGe");
        G4Polycone *Galileo =
        new G4Polycone(G4String(sName),0.0*deg,360.0*deg,nbSlice,zSlice,InnRad,OutRad);
        pGalileoGeCrystal  = new G4LogicalVolume( Galileo, matGeCrystal, G4String(sName), 0, 0, 0 );
        G4VisAttributes *pDetVACaps = new G4VisAttributes( G4Colour(0.9, 0.9, 0.9) );
        pGalileoGeCrystal->SetVisAttributes(pDetVACaps);
    }
    return pGalileoGeCrystal;
}

G4bool GalileoDetector::GetGalileoTripleClusterGe()
{
    G4bool status = false;
    string gdml_file = "galileoTC_Ge.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Triple Cluster GDML file, please check path: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file));
    G4VPhysicalVolume* W = gdmlparser.GetWorldVolume();
    W->GetLogicalVolume()->SetVisAttributes(G4VisAttributes::Invisible);
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    float Rcol[3]={1.0,0.0,0.0};
    float Gcol[3]={0.0,1.0,0.0};
    float Bcol[3]={0.0,0.0,1.0};
    for(int j=0;j<3;++j){
        el_name.str("");name.str("");
        el_name << "galileoTC_Ge3_" << j << "_vol";
        name << "Gal_TC_" << j ;
        if(pGalileoTripleClusterGe.find(j)==pGalileoTripleClusterGe.end()){
            pGalileoTripleClusterGe[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),matGeCrystal,"galileoTCGe",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(Rcol[j], Gcol[j], Bcol[j]) );
            pGalileoTripleClusterGe[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}


G4bool GalileoDetector::GetGalileoTripleClusterGeHousing()
{
    G4bool status = false;
    string gdml_file = "galileoTC_Housing.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Triple Cluster Ge Housing GDML file, please check path: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    status=true;
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file));
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    float Rcol[14]={0.55,0.95,0.95,0.91,0.0,0.1,1.0,0.0,0.2,0.2,0.2,0.2,0.2,0.2};
    float Gcol[14]={0.1,0.95,0.95,0.91,0.0,0.5,0.0,1.0,0.2,0.2,0.2,0.2,0.2,0.2};
    float Bcol[14]={0.1,0.95,0.95,0.91,1.0,0.6,0.0,0.0,0.2,0.2,0.2,0.2,0.2,0.2};
    for(int j=0;j<13;j++){
        el_name.str("");name.str("");
        el_name << "galileoTC_Housing_" << j << "_vol";
        name << "Gal_TC_" << j ;
        pDetVTC = new G4VisAttributes( G4Colour(Rcol[j], Gcol[j], Bcol[j]) );
        pGalileoTripleClusterGeHousing[j] = new G4LogicalVolume( (gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
                                                                matCapsule,"galileoTCCapsule",0,0,0);
        pDetVTC->SetForceWireframe (true);
        pGalileoTripleClusterGeHousing[j]->SetVisAttributes(pDetVTC);
    }
    return status;
}

G4bool GalileoDetector::GetGalileoTripleClusterAC()
{
    G4bool status = false;
    string gdml_file = "galileoBGO_crystals.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Anti-Compton GDML file, please check path: "
        << (gdml_path+gdml_file) << G4endl;
        return status;
        std::exit(10);
    }
    status=true;
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file));
    std::ostringstream el_name;
    std::ostringstream name;
    float Rcol[3]={1.0,0.0,0.0};
    float Gcol[3]={0.0,1.0,0.0};
    float Bcol[3]={0.0,0.0,1.0};
    G4VisAttributes *pDetVTC;
    for(int j=0;j<3;++j){
        el_name.str("");name.str("");
        el_name << "galileoBGO_crystals_" << j << "_vol";
        name << "BGO_TC_" << j ;
        pDetVTC = new G4VisAttributes( G4Colour(Rcol[j], Gcol[j], Bcol[j]) );
        pGalileoTripleClusterAC[j] = new G4LogicalVolume( (gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
                                                         matACCrystal,"galileoTCANTIC",0,0,0);
        pGalileoTripleClusterAC[j]->SetVisAttributes(pDetVTC);
    }
    return status;
}


G4bool GalileoDetector::GetGalileoTripleClusterACHousing()
{
    G4bool status = false;
    string gdml_file = "galileoBGO_carter.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open Galileo Anti-Compton GDML file, please check path: "
        << (gdml_path+gdml_file) << G4endl;
        return status;
        std::exit(10);
    }
    status=true;
    check_file.close();
    G4VisAttributes *pDetVTC;
    gdmlparser.Read((gdml_path+gdml_file));
    G4RotationMatrix* Rot = new G4RotationMatrix;
    std::ostringstream el_name;
    std::ostringstream name;
    for(int j=0;j<7;++j){
        el_name.str("");name.str("");
        el_name << "galileoBGO_carter_" << j << "_vol";
        name << "BGO_TC_Caps" << j ;
        pDetVTC = new G4VisAttributes( G4Colour(0.2,0.2,0.2) );
        pGalileoTripleClusterACHousing[j] = new G4LogicalVolume( (gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
                                                                matCapsule,"galileoTCANTIC",0,0,0);
        pGalileoTripleClusterACHousing[j]->SetVisAttributes(pDetVTC);
    }
    
    return status;
}



G4LogicalVolume *GalileoDetector::GetGalileoCollimator()
{
    G4int nbSliceCol = 2;
    G4double zSlideCol[2] = {0*mm,54.0*mm};
    G4double InnRadCol[2] = {32.0/2.*mm,47.6/2.*mm};
    G4double OutRadCol[2] = {50.0/2.*mm,50.0/2.*mm};
    G4Polycone *detCone = new G4Polycone("det_cone",0*deg, 360.0*deg,nbSliceCol,
                                         zSlideCol,InnRadCol,OutRadCol);
    
    pGalileoCollimator	= new G4LogicalVolume(detCone,matLeadCollimator,"collimator",0,0,0);
    G4VisAttributes *vislead = new G4VisAttributes(G4Colour(0.50, 0.50, 0.80));
    pGalileoCollimator->SetVisAttributes(vislead);
    return pGalileoCollimator;
}

G4LogicalVolume *GalileoDetector::GetGalileoTCCollimator()
{
    G4int nbSliceCol = 2;
    G4double zSlideCol[2] = {0*mm,54.0*mm};
    G4double InnRadCol[2] = {32.0/2.*mm,47.6/2.*mm};
    G4double OutRadCol[2] = {50.0/2.*mm,50.0/2.*mm};
    G4Polycone *detCone = new G4Polycone("det_cone",0*deg, 360.0*deg,nbSliceCol,
                                         zSlideCol,InnRadCol,OutRadCol);
    
    pGalileoCollimator	= new G4LogicalVolume(detCone,matLeadCollimator,"collimator",0,0,0);
    G4VisAttributes *vislead = new G4VisAttributes(G4Colour(0.50, 0.50, 0.80));
    pGalileoCollimator->SetVisAttributes(vislead);
    return pGalileoTCCollimator;
}

G4LogicalVolume *GalileoDetector::GetGalileoCollimatorHL(G4int j,bool tc)
{
    
    G4double theta_min(0.),theta_range(0.),theta_det_min(0.),theta_det_max(0.);
    G4double tl_beam_hole(0.);
    G4String shield_name;
    G4SubtractionSolid *temp ;
    G4double RadMin(0.),RadMax(0.);
    G4int det_min(0), det_max(0);
    
    
    switch(j){
        case 1: { // Backward part
            det_min=0;det_max=15;
            theta_min=107.*deg; theta_range=74.*deg;
            theta_det_min = 95.*deg; theta_det_max = 160.;
            shield_name="backward_lead";
            RadMin=GalileoLeadCollimatorRadiusMin;
            RadMax=GalileoLeadCollimatorRadiusMax;
            tl_beam_hole=-RadMax;
            break;
        }
        case 2: { // Central part
            det_min=15;det_max=25;
            theta_min=74.00*deg;
            theta_range=33.00*deg;
            theta_det_min = 85.*deg; theta_det_max = 95.;
            RadMin=GalileoLeadCollimatorCentralRadiusMin;
            RadMax=GalileoLeadCollimatorCentralRadiusMax;
            shield_name="central_lead";
            break;
        }
        case 3: {// Froward part (det 61-51)
            det_min=25;det_max=35;
            theta_min=39.5*deg; theta_range=34.5*deg;
            theta_det_min = 50.*deg; theta_det_max = 70.;
            RadMin=GalileoLeadCollimatorRadiusMin;
            RadMax=GalileoLeadCollimatorRadiusMax;
            shield_name="forward_lead1";
            break;
        }
        case 4: {// Froward part (det 61-51)
            det_min=35;det_max=40;
            theta_min=0.*deg; theta_range=39.5*deg;
            theta_det_min = 10.; theta_det_max = 45.;
            RadMin=GalileoLeadCollimatorRadiusMin;
            RadMax=GalileoLeadCollimatorRadiusMax;
            shield_name="forward_lead2";
            tl_beam_hole=RadMax-80;
            break;
        }
        default:
            exit(-1);
    }
    
    std::vector<G4SubtractionSolid*> step;
    G4VSolid *sphericalCol = new G4Sphere("leadcol",
                                          RadMin, RadMax,
                                          0.0*deg,360.0*deg,
                                          theta_min,theta_range);
    
    G4int nbSliceBeam = 2;
    G4double zSliceBeam[2]  = {0*mm,90.*mm};
    G4double InnRadBeam[2]  = {0.0*mm,0.0*mm};
    G4double OutRadBeam[2]  = {70.0/2.*mm,70.0/2.*mm};
    G4Polycone *beamlinehole = new G4Polycone("beamhole",0*deg, 360.0*deg,nbSliceBeam,
                                              zSliceBeam,InnRadBeam,OutRadBeam);
    
    G4int nbSliceDet = 2;
    G4double zSliceDet[2]  = {0*mm,88.0*mm};
    G4double InnRadDet[2]  = {0.0*mm,0.0*mm};
    G4double OutRadDet[2]  = {48.24/2.*mm,48.24/2.*mm};
    
    G4Polycone *dethole = new G4Polycone("det_hole",0*deg, 360.0*deg,nbSliceDet,
                                         zSliceDet,InnRadDet,OutRadDet);
    
    
    G4int nbSliceDet2 = 2;
    G4double zSliceDet2[2]  = {0*mm,85.0*mm};
    G4double InnRadDet2[2]  = {0.0*mm,0.0*mm};
    G4double OutRadDet2[2]  = {40.0/2.*mm,50.6/2.*mm};
    
    G4Polycone *dethole_tc = new G4Polycone("det_hole",0*deg, 360.0*deg,nbSliceDet2,
                                            zSliceDet2,InnRadDet2,OutRadDet2);
    
    temp = new G4SubtractionSolid("step_0",sphericalCol,beamlinehole,0,G4ThreeVector(0,0,tl_beam_hole));
    step.push_back(temp);
    
    
    G4int iteration=0;
    ostringstream os;
    for(G4int detnb = det_min;detnb<det_max; ++detnb){
        if (galileoangles[detnb][0]>theta_det_max || galileoangles[detnb][0]<theta_det_min) continue;
        if(!tc){
            G4RotationMatrix rm2;
            if(j==1)rm2.rotateZ(180.*deg);
            rm2.rotateY(galileoangles[detnb][0]*deg);
            rm2.rotateZ(galileoangles[detnb][1]*deg);
            os.str("");
            os << "step_" << iteration;
            temp = new G4SubtractionSolid(os.str().c_str(),step.back(),dethole,G4Transform3D(rm2, rm2( G4ThreeVector(0,0,105.))));
            step.push_back(temp);
            iteration++;
        }else if(tc){
            //      if(detnb>15)break;
            G4RotationMatrix rm_tc;
            for(int tc_crys=0;tc_crys<3;tc_crys++){
                rm_tc=G4RotationMatrix::IDENTITY;
                rm_tc.rotateY(6*deg);
                rm_tc.rotateZ((tc_crys+1)*120*deg+180*deg);
                //      rm2=G4RotationMatrix::IDENTITY;
                rm_tc.rotateZ(galileoangles[detnb][2]*deg);
                rm_tc.rotateX(galileoangles[detnb][0]*deg);
                rm_tc.rotateZ(galileoangles[detnb][1]*deg);
                
                os.str("");
                os << "step_" << iteration;
                temp = new G4SubtractionSolid(os.str().c_str(),step.back(),dethole_tc,
                                              G4Transform3D(rm_tc, rm_tc( G4ThreeVector(0,0,110.))));
                
                step.push_back(temp);
                iteration++;
            }
        }
    }
    
    G4LogicalVolume *lead_shield = new G4LogicalVolume(step.back(),matLeadCollimator,shield_name,0,0,0);
    G4VisAttributes *vislead = new G4VisAttributes(G4Colour(0.65, 0.65, 0.65));
    lead_shield->SetVisAttributes(vislead);
    
    return lead_shield;
}




//#define _DEBUGGEOM_

void GalileoDetector::Placement()
{
    if( FindMaterials() ) return;	// check if materials are known
    G4RunManager* runManager                = G4RunManager::GetRunManager();
    AgataDetectorConstruction* theTarget  =
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    GetGalileoFronting();         // Fronting
    GetGalileoACCapsule();        // Anti Compton Capsule
    GetGalileoACCrystal();        // Anti Compton Crystal
    GetGalileoGeCapsule();        // Single capsule
    GetGalileoGeCrystal();        // Single crystal
    ofstream eulerlist;
    eulerlist.open("Galileoaeuler",ios::out);
    G4RotationMatrix rm2;
    
    
    if(fTripleCluster){
        GetGalileoTripleClusterGe();
        GetGalileoTripleClusterGeHousing();
        GetGalileoTripleClusterAC();
        GetGalileoTripleClusterACHousing();
        
    }
    std::map<int,G4LogicalVolume*>::iterator it;
    
    G4double RGal = RGalileo+fUserDefinedRadius;
    pGalileoGeCrystal->SetSensitiveDetector(theTarget->GeSD());
    pGalileoACCrystal->SetSensitiveDetector(theTarget->GeSD());
    
    if(fTripleCluster){
        it=pGalileoTripleClusterGe.begin();
        for(;it!=pGalileoTripleClusterGe.end();++it){
            it->second->SetSensitiveDetector(theTarget->GeSD());
        }
        it=pGalileoTripleClusterAC.begin();
        for(;it!=pGalileoTripleClusterAC.end();++it){
            it->second->SetSensitiveDetector(theTarget->GeSD());
        }
    }
    std::ostringstream os;
#ifndef _DEBUGGEOM_
    fDetAngles.clear();
    fDetType.clear();
    std::set<int>::iterator itgalileodet = fUsedGalileoDetectors.begin();
    for(;itgalileodet!=fUsedGalileoDetectors.end(); ++itgalileodet){
        //G4cout << "Construction and placement of Detector# " << *itgalileodet << G4endl;
        if(*itgalileodet<400){
            int detnb = (*itgalileodet<300? *itgalileodet : *itgalileodet-300);
            os.str("");
            os << "Galileo_crystal_" << detnb;
            G4RotationMatrix rm;
            if(galileoangles[detnb][0]>90) rm.rotateZ(180.*deg);
            rm.rotateY(galileoangles[detnb][0]*deg);
            rm.rotateZ(galileoangles[detnb][1]*deg);
            
            fDetAngles[detnb] = rm(G4ThreeVector(0,0,RGalileo));
            fDetType[detnb] = 0;
            new G4PVPlacement(G4Transform3D(rm, rm(G4ThreeVector(0,0,RGalileo))),
                              G4String(os.str()),pGalileoGeCrystal,(theTarget)->HallPhys(), false, detnb);
            os.str("");
            os << "Galileo_capsule_" << detnb;
            new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGalileo))),
                              G4String(os.str()),pGalileoGeCapsule,(theTarget)->HallPhys(), false, detnb);
            if(*itgalileodet>=300){
                os.str("");
                os << "Galileo_BGO_Capsule_" << detnb;
                new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGalileo))),
                                  G4String(os.str()),pGalileoACCapsule,(theTarget)->HallPhys(), false, detnb+300);
                os.str("");
                os << "Galileo_BGO_Crystal_" << detnb;
                new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGalileo))),
                                  G4String(os.str()),pGalileoACCrystal,(theTarget)->HallPhys(), false, detnb+300);
                os.str("");
                os << "Galileo_Collimator_" << detnb;
                new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGalileo))),
                                  G4String(os.str()),pGalileoFronting,(theTarget)->HallPhys(), false, detnb);
            }
        }
        
        if(*itgalileodet>=400){
            int detnb=(*itgalileodet<500? *itgalileodet-400:*itgalileodet-500);
            
            os << "Galileo_TC_" << detnb;
            G4RotationMatrix rm;
            rm = G4RotationMatrix::IDENTITY;
            rm.rotateZ(180*deg);
            rm.rotateZ(galileoangles[detnb][2]*deg);
            rm.rotateX(galileoangles[detnb][0]*deg);
            rm.rotateZ(galileoangles[detnb][1]*deg);
            it=pGalileoTripleClusterGe.begin();
            for(;it!=pGalileoTripleClusterGe.end();it++){
                os.str("");
                os << "Galileo_TC_" << detnb << "_crys_" <<it->first ;
                fDetAngles[400+detnb*3+it->first] = rm(G4ThreeVector(0,0,67.5*cm+fUserDefinedRadius));
                fDetType[400+detnb*3+it->first] = 1;
                new G4PVPlacement(G4Transform3D(rm, rm(G4ThreeVector(0,0,67.5*cm+fUserDefinedRadius))),
                                  G4String(os.str()),
                                  it->second,
                                  (theTarget)->HallPhys(),
                                  false,400+detnb*3+it->first);
            }
            rm2=G4RotationMatrix::IDENTITY;
            rm2.rotateX(270.*deg);
            rm2.rotateZ(150*deg);
            rm2.rotateZ(galileoangles[detnb][3]*deg);
            rm2.rotateX(galileoangles[detnb][0]*deg);
            rm2.rotateZ(galileoangles[detnb][1]*deg);
            it=pGalileoTripleClusterGeHousing.begin();
            for(;it!=pGalileoTripleClusterGeHousing.end();++it){
                os << "Galileo_TC_capsule_" << detnb;
                new G4PVPlacement(G4Transform3D(rm2, rm2( G4ThreeVector(0,-67.5*cm,0))),
                                  G4String(os.str()),it->second,(theTarget)->HallPhys(), false,0);
                os.str("");
            }
            if(*itgalileodet>=500){
                rm2=G4RotationMatrix::IDENTITY;
                rm2.rotateX(270.*deg);
                rm2.rotateZ(150*deg);
                rm2.rotateZ(galileoangles[detnb][3]*deg);
                rm2.rotateX(galileoangles[detnb][0]*deg);
                rm2.rotateZ(galileoangles[detnb][1]*deg);
                //
                it=pGalileoTripleClusterAC.begin();
                for(;it!=pGalileoTripleClusterAC.end();++it){
                    os.str("");
                    os << "Galileo_TC_BGO_" << 300+9*detnb+it->first;
                    new G4PVPlacement(G4Transform3D(rm2, rm2( G4ThreeVector(0,-48*cm,0))),
                                      G4String(os.str()),it->second,(theTarget)->HallPhys(), false,600+detnb);
                    
                    os.str("");
                }
                it=pGalileoTripleClusterACHousing.begin();
                for(;it!=pGalileoTripleClusterACHousing.end();++it){
                    os.str("");
                    os << "Galileo_AC_capsule_" << detnb;
                    new G4PVPlacement(G4Transform3D(rm2, rm2( G4ThreeVector(0,-48*cm,0))),
                                      G4String(os.str()),it->second,(theTarget)->HallPhys(), false,600+detnb);
                    
                    os.str("");
                }
                os << "Galileo_TC_BGO_capsule_" << detnb;
                
            }
        }
    }
    
    std::vector<PlacementOfAGASP>::iterator itextraphaseI = fExtraPhaseIs.begin();
    for(; itextraphaseI!=fExtraPhaseIs.end(); ++itextraphaseI){
        int detnb = (itextraphaseI->id<300 ? itextraphaseI->id : itextraphaseI->id-300);
        G4ThreeVector Trans(itextraphaseI->x,itextraphaseI->y,itextraphaseI->z);
        fDetAngles[detnb] = Trans;
        G4RotationMatrix rm;
        rm.rotateZ(itextraphaseI->phi);
        rm.rotateY(itextraphaseI->theta);
        rm.rotateZ(itextraphaseI->psi);
        os.str("");
        os << "Extra_Galileo_crystal_" << detnb;
        new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RGal))-rm(G4ThreeVector(0,0,RGal))+Trans),
                          G4String(os.str()),pGalileoGeCrystal,(theTarget)->HallPhys(), false, detnb );
        os.str("");
        os << "Extra_Galileo_capsule_" << detnb;
        new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGal))+Trans),
                          G4String(os.str()),pGalileoGeCapsule,(theTarget)->HallPhys(), false, detnb );
        os.str("");
        if(itextraphaseI->id>=300){
            os << "Extra_Galileo_BGO_" << detnb;
            new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RGal))-rm(G4ThreeVector(0,0,RGal))+Trans),
                              G4String(os.str()),pGalileoACCrystal,(theTarget)->HallPhys(), false, detnb+300);
            os.str("");
            os << "Extra_Galileo_BGO_Capsule_" << detnb;
            new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGal))),
                              G4String(os.str()),pGalileoACCapsule,(theTarget)->HallPhys(), false, detnb+300);
            
            os.str("");
            os << "Extra_Galileo_Collimator_" << detnb;
            new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RGal))-rm(G4ThreeVector(0,0,RGal))+Trans),
                              G4String(os.str()),pGalileoFronting,(theTarget)->HallPhys(), false, detnb);
        }
    }
    
#else
    os.str("");
    os << "Galileo_crystal_0";
    G4RotationMatrix rm;
    rm.rotateX(0*deg);
    rm.rotateZ(0*deg);
    new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,RGal))),
                      G4String(os.str()),pGalileoGeCrystal,
                      (theTarget)->HallPhys(), false, 0 );
    os.str("");
    os << "Galileo_BGO_0";
    new G4PVPlacement(G4Transform3D(rm,
                                    rm( G4ThreeVector(0,0,RGal))),
                      G4String(os.str()),pGalileoACCrytal,
                      (theTarget)->HallPhys(), false, 300);
    os.str("");
    os << "Galileo_Collimator_0";
    new G4PVPlacement(G4Transform3D(rm,rm(G4ThreeVector(0,0,RGal))),
                      G4String(os.str()),pGalileoFronting,
                      (theTarget)->HallPhys(), false, 0);
    
    
#endif
    //We also put in place our chambre
    //First the beam in part
    if(fMakeChamber){
        G4cout << " -----> Building Galileo chamber" << G4endl;
        G4int cp1 = 11;
        G4double z[11] = {-468*mm,
            -453*mm,
            -453*mm,
            -116*mm,
            -105*mm,
            -96*mm,
            -81.8*mm,
            -72*mm,
            -54*mm,
            -41.6*mm,
            -36*mm,};
        G4double router[11]={50*mm,
            50*mm,
            31.75*mm,
            31.75*mm,
            60*mm,
            72*mm,
            88*mm,
            96*mm,
            102*mm,
            103*mm,
            110*mm,};
        G4double rinner[11] ={29.75*mm,
            29.75*mm,
            29.75*mm,
            29.75*mm,
            24*mm,
            // 18.21*mm,//31.85486009350359*mm ,//<-
            69*mm,
            84*mm,
            92*mm,
            100*mm,
            102*mm,
            102*mm,};
        G4Polycone *solchamber1 = new G4Polycone("chamber1sol",0*deg, 360.0*deg,
                                                 cp1,z,rinner,router);
        
        G4LogicalVolume *logchamber1 = new G4LogicalVolume(solchamber1,matCapsule,"chambre1log",0,0,0);
        G4VisAttributes *vischal = new G4VisAttributes(G4Colour(1., 1, 1));
        logchamber1->SetVisAttributes(vischal);
        vischal->SetForceWireframe (true);
        new G4PVPlacement(G4Transform3D(),
                          "chamber1phys",logchamber1,
                          (theTarget)->HallPhys(), false, 0);
        
        //Then the central part of the chamber
        G4int cp2 = 4;
        G4double z2[4] = {-32*mm,-28*mm,28*mm,32*mm};
        G4double router2[4]={110*mm,106*mm,106*mm,110*mm};
        G4double rinner2[4]={102*mm,102*mm,102*mm,102*mm};
        G4Polycone *solchamber2 = new G4Polycone("chamber2sol",0*deg, 360.0*deg,
                                                 cp2,z2,rinner2,router2);
        
        G4LogicalVolume *logchamber2 = new G4LogicalVolume(solchamber2,matCapsule,"chambre2log",0,0,0);
        G4VisAttributes *vischal2 = new G4VisAttributes(G4Colour(.6, .6, .6));
        logchamber2->SetVisAttributes(vischal2);
        vischal2->SetForceWireframe (true);
        new G4PVPlacement(G4Transform3D(),
                          "chamber2phys",logchamber2,
                          (theTarget)->HallPhys(), false, 0);
        
        //Now the beam out part...
        
        G4int cp3 = 11;
        G4double z3[11]= { 36*mm,
            41.6*mm,
            54*mm,
            72*mm,
            81.8*mm,
            96*mm,
            105*mm,
            115.82*mm,
            1115.82*mm,
            1115.82*mm,
            1127.82*mm};
        G4double router3[11]={110*mm,
            103*mm,
            102*mm,
            96*mm,
            88*mm,
            72*mm,
            60*mm,
            31.75*mm,
            31.75*mm,
            50*mm,
            50*mm};
        G4double rinner3[11] ={102*mm,
            102*mm,
            100*mm,
            92*mm,
            84*mm,
            69*mm,
            24*mm,
            29.75*mm,
            29.75*mm,
            29.75*mm,
            29.75*mm};
        G4Polycone *solchamber3 = new G4Polycone("chamber3sol",0*deg, 360.0*deg,cp3,
                                                 z3,rinner3,router3);
        G4LogicalVolume *logchamber3 = new G4LogicalVolume(solchamber3,matCapsule,"chamber3log",0,0,0);
        G4VisAttributes *vischal3 = new G4VisAttributes(G4Colour(1., 1., 1.));
        vischal3->SetForceWireframe (true);
        logchamber3->SetVisAttributes(vischal3);
        new G4PVPlacement(G4Transform3D(),
                          "chamber3phys",logchamber3,
                          (theTarget)->HallPhys(), false, 0);
    }
    //If asked for the lead Collimator
    if(fMakeLeadCollimator){
        G4cout << " -----> Building Galileo Lead Collimator" << G4endl;
        
        bool already_placed_back = false;
        bool already_placed_cent = false;
        bool already_placed_for1 = false;
        bool already_placed_for2 = false;
        
        
        
        
        itgalileodet = fUsedGalileoDetectors.begin();
        for(;itgalileodet!=fUsedGalileoDetectors.end(); ++itgalileodet){
            if(*itgalileodet<400){
                GetGalileoCollimator();
                
                
                int detnb = (*itgalileodet<300? *itgalileodet : *itgalileodet-300);
                if(detnb>=100 && detnb < 200) detnb-=100;
                if(detnb>=200)detnb-=200;
                
                os.str("");
                os << "Collimator_det_"<< detnb;
                G4RotationMatrix rm;
                if (galileoangles[detnb][0]>90){
                    rm.rotateZ(180.*deg);
                    if(!already_placed_back){
                        pGalileoCollimatorBackward = GetGalileoCollimatorHL(1,false);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorBack",pGalileoCollimatorBackward,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_back=true;
                        
                    }
                }
                else if (galileoangles[detnb][0]==90){
                    if(!already_placed_cent){
                        pGalileoCollimatorCentral  = GetGalileoCollimatorHL(2,false);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorCentral",pGalileoCollimatorCentral,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_cent=true;
                    }
                }
                else if (galileoangles[detnb][0]<90 && galileoangles[detnb][0]>40){
                    if(!already_placed_for1){
                        pGalileoCollimatorForward1 = GetGalileoCollimatorHL(3,false);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorForward1",pGalileoCollimatorForward1,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_for1=true;
                    }
                }
                else if (galileoangles[detnb][0]<40){
                    if(!already_placed_for2){
                        pGalileoCollimatorForward2 = GetGalileoCollimatorHL(4,false);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorForward2",pGalileoCollimatorForward2,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_for2=true;
                    }
                }
                
                rm.rotateY(galileoangles[detnb][0]*deg);
                rm.rotateZ(galileoangles[detnb][1]*deg);
                new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,GalileoLeadCollimatorRadiusMin))),
                                  G4String(os.str()),pGalileoCollimator,(theTarget)->HallPhys(), false, detnb );
            }else if(*itgalileodet>=400){
                int detnb = (*itgalileodet>=500? *itgalileodet-500 : *itgalileodet-400);
                GetGalileoTCCollimator();
                os.str("");
                os << "CollimatorTC_det_"<< detnb;
                G4RotationMatrix rm;
                if (galileoangles[detnb][0]>90){
                    rm.rotateZ(180.*deg);
                    if(!already_placed_back){
                        pGalileoCollimatorBackward = GetGalileoCollimatorHL(1,true);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorBack",pGalileoCollimatorBackward,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_back=true;
                    }
                }
                else if (galileoangles[detnb][0]==90){
                    if(!already_placed_cent){
                        pGalileoCollimatorCentral  = GetGalileoCollimatorHL(2,true);
                        //	    new G4PVPlacement(G4Transform3D(),
                        //			      "CollimatorCentral",pGalileoCollimatorCentral,
                        //			      (theTarget)->HallPhys(), false, 0);
                        already_placed_cent=true;
                    }
                }
                else if (galileoangles[detnb][0]<90 && galileoangles[detnb][0]>40){
                    if(!already_placed_for1){
                        pGalileoCollimatorForward1 = GetGalileoCollimatorHL(3,true);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorForward1",pGalileoCollimatorForward1,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_for1=true;
                    }
                }
                else if (galileoangles[detnb][0]<40){
                    if(!already_placed_for2){
                        pGalileoCollimatorForward2 = GetGalileoCollimatorHL(4,true);
                        new G4PVPlacement(G4Transform3D(),
                                          "CollimatorForward2",pGalileoCollimatorForward2,
                                          (theTarget)->HallPhys(), false, 0);
                        already_placed_for2=true;
                    }
                }
                for(int tc_crys=0;tc_crys<1;tc_crys++){
                    rm=G4RotationMatrix::IDENTITY;
                    //rm.rotateY(6*deg);
                    rm.rotateZ(180*deg+(tc_crys)*120*deg);
                    rm.rotateZ(galileoangles[detnb][2]*deg);
                    rm.rotateY(galileoangles[detnb][0]*deg);
                    rm.rotateZ(galileoangles[detnb][1]*deg);
                    //	  new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0,0,GalileoLeadCollimatorRadiusMin))),
                    //			    G4String(os.str()),pGalileoCollimator,
                    //			    (theTarget)->HallPhys(), false, detnb );
                }
            }
        }
    }
    if(theAncillary) theAncillary->Placement();
}


void GalileoDetector::WriteHeader( std::ofstream &outFileLMD, G4double unit )
{
    outFileLMD << "GALILEO WITH " << fDetAngles.size()
    << " HPGe Crystals Detector Type  nb theta[deg] phi[deg] \n";
    std::map<int,G4ThreeVector>::iterator itgalileodet = fDetAngles.begin();
    for(;itgalileodet!=fDetAngles.end(); ++itgalileodet){
        if(fDetType[itgalileodet->first]==0){
            int detnb = itgalileodet->first;
            outFileLMD << std::setiosflags(ios::fixed) << std::setprecision(2)
            << std::setw(10) << "HpGe"
            << std::setw(10) << detnb
            << std::setw(10) << 3
            //	       << std::setw(10) << itgalileodet->first
            << std::setw(10) << itgalileodet->second.theta()/deg << " "
            << std::setw(10)
            << (itgalileodet->second.phi()/deg>0 ?
                itgalileodet->second.phi()/deg :
                itgalileodet->second.phi()/deg+360.)
            <<  "\n";
        }
        else if(fDetType[itgalileodet->first]==1) {
            int detnb = itgalileodet->first - 400;
            outFileLMD << std::setiosflags(ios::fixed) << std::setprecision(2)
            << std::setw(10) << "TC"
            << std::setw(10) << detnb/3
            << std::setw(10) << detnb - (detnb/3)*3
            //	       << std::setw(10) << itgalileodet->first
            << std::setw(10) << itgalileodet->second.theta()/deg << " "
            << std::setw(10)
            << (itgalileodet->second.phi()/deg>0 ?
                itgalileodet->second.phi()/deg :
                itgalileodet->second.phi()/deg+360.)
            <<  "\n";
            
        }
    }
    outFileLMD << " Galileo det coded as det number\n";
    outFileLMD << " BGO shield coded as 300+det number\n";
    outFileLMD << " Triple Cluster coded as 400+det number*3+crystal \n";
    outFileLMD << " Triple Cluster AC coded as 600+det number \n";
    
    outFileLMD << "END GALILEO\n";
    //for(G4int j=0 ;j<theAncillary.size();++j) theAncillary[j]->WriteHeader(outFileLMD,unit);
    if(theAncillary) theAncillary->WriteHeader(outFileLMD,unit);
}

void GalileoDetector::AddGalileoDetectors(G4String detectorstring)
{
    
    std::istringstream decode(detectorstring);
    int galileonb;
    fUsedGalileoDetectors.clear();
    while(decode>>galileonb){
        if(galileonb>=400){
            fTripleCluster=true;
            fUsedGalileoDetectors.insert(galileonb);
        }
        else if(((galileonb>=0 && galileonb<40)
                 || (galileonb>=300 && galileonb<340))){
            //	    && std::find(fDontMakeThisFrame.begin(),fDontMakeThisFrame.end(),galileonb>=300 ? galileonb-300:galileonb)==fDontMakeThisFrame.end())
            fUsedGalileoDetectors.insert(galileonb);
        }
    }
}

G4int GalileoDetector::GetSegmentNumber( G4int offset, G4int nGe, G4ThreeVector position )
{  
    //if(nGe>50)nGe/=1000;
    //  std::cout << offset << " " << nGe << " "  << std::endl;
    
    if(offset>500 && theAncillary) return theAncillary->GetSegmentNumber( offset, nGe, position );
    return nGe;
}
////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UImanager.hh"

GalileoDetectorMessenger::GalileoDetectorMessenger(GalileoDetector* pTarget)
:myTarget(pTarget)
{ 
    
    G4String Name = "/Galileo/";
    G4UIdirectory* topDirectory = new G4UIdirectory(Name);
    topDirectory->SetGuidance("Galileo Array from LNL (Only with single cristal)");
    
    G4String directoryName= Name+ "detector/";
    G4UIdirectory* detDirectory = new G4UIdirectory(directoryName);
    detDirectory->SetGuidance("Detector settings");
    
    G4String commandName;
    const char *aLine;
    
    commandName=directoryName+"setUsedDetectors";
    aLine=commandName.c_str();
    SetUsedGePositionCmd = new G4UIcmdWithAString(aLine,this);
    SetUsedGePositionCmd->SetGuidance("Give positions used in frame (add 300 to include BGO shield):");
    SetUsedGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
    commandName=directoryName+"SetArrayRadius";
    aLine=commandName.c_str();
    SetArrayRadiusCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
    SetArrayRadiusCmd->SetGuidance("Set array radius change with respect to the default one (192.5 mm)");
    SetArrayRadiusCmd->SetGuidance("Lead collimator radius used in the simulation is 190.0 mm");
    SetArrayRadiusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
    commandName=directoryName+"SetMakeChamber";
    aLine=commandName.c_str();
    SetMakeChamberCmd = new G4UIcmdWithABool(aLine,this);
    SetMakeChamberCmd->SetGuidance("Make Galileo Plunger chamber or not (true/false)");
    SetMakeChamberCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
    commandName=directoryName+"SetMakeLeadCollimator";
    aLine=commandName.c_str();
    SetMakeLeadCollimatorCmd = new G4UIcmdWithABool(aLine,this);
    SetMakeLeadCollimatorCmd->SetGuidance("Make Galileo Lead Collimator or not (true/false)");
    SetMakeLeadCollimatorCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
}

GalileoDetectorMessenger::~GalileoDetectorMessenger()
{
    G4cout << "### ~GalileoDetectorMessenger "<< G4endl;
    delete SetUsedGePositionCmd;
    delete SetArrayRadiusCmd;
    delete SetMakeChamberCmd;
    delete SetMakeLeadCollimatorCmd;
    
}

void GalileoDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
    if( command == SetUsedGePositionCmd ) {
        myTarget->AddGalileoDetectors(newValue);
    }
    if( command == SetArrayRadiusCmd ) {
        myTarget->SetArrayRadius(SetArrayRadiusCmd->GetNewDoubleValue(newValue));
    }
    if(command==SetMakeChamberCmd){
        myTarget->SetMakeChamber(SetMakeChamberCmd->GetNewBoolValue(newValue));
    }
    if(command==SetMakeLeadCollimatorCmd){
        myTarget->SetMakeLeadCollimator(SetMakeLeadCollimatorCmd->GetNewBoolValue(newValue));
    }
}

