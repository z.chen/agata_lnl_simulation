#include "AgataAnalysis.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "AgataHitDetector.hh"
#include "AgataGeneratorAction.hh"
#include "CSpec1D.hh"
// uncomment if matrices are needed!
//#include "CSpec2D.hh"

#include "G4RunManager.hh"
#include <fstream>
#include "G4ios.hh"
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


AgataAnalysis::AgataAnalysis()
{
  det2clust         = NULL;
  detAccu           = NULL;
  cluAccu           = NULL;
  
  aLine             = new char[100];
  runNumber         = 0;
  nDetectors        = 0;
  nClusters         = 0;
  
  enabledAnalysis   = false;       //> generates spectra ?
  specLength        = 4096;        //> length of spectra
  offset            = 0.;          //> offset for spectra
  gain              = 1.;          //> gain   for spectra
  
  ///////////////////////////////////////////////////////////
  /// Energy resolution modelled as FWHM = sqrt( A + B E )
  //////////////////////////////////////////////////////////
  resolutionEnabled = false;       //> energy resolution on/off 
  //////////////////////////////////////////////////////////////
  /// Default values are calculated from the FWHM values at
  /// 122 and 1333 keV
  //////////////////////////////////////////////////////////////
  CalculateResolution( 122., 1.1, 1333., 2.3, false);
  FWHMk             = 1./(sqrt(8.*log(2.)));  //> sigma = FWHM/2.355
  
  packEvent         = false;       //> don't pack multievent-gammas
  
  asciiSpectra      = false;       //> default: binary spectra
  
  maxHits           = 11;         //> covers the 0-10 interval; 11-->anything larger than 10
  
  myMessenger = new AgataAnalysisMessenger(this);
}

AgataAnalysis::~AgataAnalysis()
{  
  spec1.clear();
  // uncomment if matrices are needed!
  //spec2.clear();
  
  if( det2clust ) delete [] det2clust;
  if( detAccu   ) delete [] detAccu;
  if( cluAccu   ) delete [] cluAccu;
  delete [] aLine;
}

//////////////////////////////////////////////
/// This method is executed at begin of run
//////////////////////////////////////////////
void AgataAnalysis::AnalysisStart(G4int run)
{
  if(!enabledAnalysis)
    return;

  G4RunManager * runManager = G4RunManager::GetRunManager();

  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();

  ////////////////////////////////////////////////////////////////////////
  /// In the case of events decoded from input files, high multiplicity
  /// should always be considered
  ////////////////////////////////////////////////////////////////////////
  if( (theGenerator->GetBeginOfEventTag() != "") && (!packEvent) )
    SetPackEvent( true );
    
  runNumber  = run;
  nDetectors = theDetector->GetMaxDetectorIndex() + 1;
  nClusters  = theDetector->GetMaxClusterIndex()  + 1;

  if( !theDetector->GetReadOut() )
    doSegPoints = false;
  else
    doSegPoints = true;

  if(packEvent && theGenerator->GetCascadeMult() > 1)
    scaleEtot = 0.1;
  else if(packEvent && (theGenerator->GetBeginOfEventTag() != "") ) 
    scaleEtot = 0.1;
  else
    scaleEtot = 1.;

  /////////////////////
  /// Create spectra
  ////////////////////
  spec1.clear();
  // uncomment if matrices are needed!
  //spec2.clear();

  spec1.push_back(new CSpec1D(specLength, 1));           //> total spectrum
  spec1.push_back(new CSpec1D(specLength, nDetectors));  //> spectrum of detectors
  spec1.push_back(new CSpec1D(specLength, nClusters));   //> spectrum of clusters
  spec1.push_back(new CSpec1D(specLength, 1));           //> sum of spectrum of detectors

  // uncomment if matrices are needed!
  // spec2.push_back(new CSpec2D(256, 256, 1));
  
  /////////////////////////////////
  /// Allocate memory buffers
  /////////////////////////////////
  if( det2clust ) delete [] det2clust;
  if( detAccu   ) delete [] detAccu;
  if( cluAccu   ) delete [] cluAccu;

  det2clust = new G4int    [nDetectors];
  memset(det2clust, 0, nDetectors*sizeof(G4int));

  detAccu   = new G4double [nDetectors];  
  memset(detAccu,   0, nDetectors*sizeof(G4double));
  minAccu = nDetectors;
  maxAccu = 0;

  cluAccu   = new G4double [nClusters];
  
  if( doSegPoints ) {
    isFirst = true;    
    nSectors = theDetector->GetNumberOfSectors();
    nSlices  = theDetector->GetNumberOfSlices();
    
    if( nSectors*nSlices < 2 ) {
      doSegPoints = false;
      return;
    }
    
    //G4cout << " Segments " << theDetector->GetNumberOfSectors() << " " << theDetector->GetNumberOfSlices  () << G4endl;
    G4int ik, segSize = nDetectors*nSlices*nSectors;
    
    
    if( fullPos )  delete [] fullPos;
    if( firstPos ) delete [] fullPos;
    if( nFull )    delete [] nFull;
    if( nFirst )   delete [] nFirst;
    if( enFull )   delete [] enFull;
    if( enFirst )  delete [] enFirst;
    
    fullPos = new G4ThreeVector[segSize];
    for( ik=0; ik<segSize; ik++ )
      fullPos[ik] = G4ThreeVector();
    
    firstPos = new G4ThreeVector[segSize];
    for( ik=0; ik<segSize; ik++ )
      firstPos[ik] = G4ThreeVector();
    
    nFull = new G4int[segSize];
    memset(nFull, 0, segSize*sizeof(G4int));
    
    nFirst = new G4int[segSize];
    memset(nFirst, 0, segSize*sizeof(G4int));
    
    enFull = new G4double[segSize];
    memset(enFull, 0, segSize*sizeof(G4double));
    
    enFirst = new G4double[segSize];
    memset(enFirst, 0, segSize*sizeof(G4double));
    
    numHitsEvt = new G4int[segSize];
    memset(numHitsEvt, 0, segSize*sizeof(G4int));
    
    numHitsTot = new G4int[segSize*maxHits];
    memset(numHitsTot, 0, segSize*maxHits*sizeof(G4int));
  }
}

///////////////////////////////////////////////
/// this method is executed at the end of run
//////////////////////////////////////////////
void AgataAnalysis::AnalysisEnd( G4String workPath )
{
  if(!enabledAnalysis)
    return;

  ////////////////////////////////////////////
  /// Write out spectra (packed GASP format)
  ////////////////////////////////////////////
  G4int size = spec1.size();
  G4int nn;
  for(nn = 0; nn <size; nn++) {
    if(spec1[nn]) {
      sprintf(aLine, "%sG%2.2d.%4.4d", workPath.c_str(), nn, runNumber);
      if( asciiSpectra )
        spec1[nn]->writeA(aLine);
      else  
        spec1[nn]->write(aLine);
    }
  }

  // uncomment if matrices are needed!
  /*
  size = spec2.size();
  for(nn = 0; nn < size; nn++) {
    if(spec2[nn]) {
      sprintf(aLine, "M%2.2d.%4.4d", nn, runNumber);
      spec2[nn]->write(aLine);
    }
  }
  */
  if( doSegPoints ) {
    FlushSegAngles(workPath);
    FlushSegHits(workPath);
  }  
}

void AgataAnalysis::FlushSegAngles( G4String workPath )
{
  char segFileName[256];
  sprintf(segFileName, "%sGammaSegments.%4.4d", workPath.c_str(), runNumber);
  
  std::ofstream segFile;
  
  segFile.open(G4String(segFileName));
  if( !segFile.is_open() ) {
    G4cout << " ---> Could not open segment position file! Segment positions will not be written out." << G4endl;
    return;
  }

  G4RunManager * runManager = G4RunManager::GetRunManager();

  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();

  time_t vTime;
  time( &vTime );
  
  segFile << "SEGMENTS " << G4endl;

  segFile << "G4TRACKING " 
             << theDetector->GeSD()->GetMethod() << G4endl;

  segFile << "DATE " << ctime(&vTime);  // date has already end-of-line character!

  theDetector->WriteHeader(segFile);
  theGenerator->PrintToFile(segFile);
  segFile << "$" << G4endl;    
  
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "#### First interaction point (cartesian) #########################\n" );
  segFile << aLine;
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "# ndet nseg   Edep (keV)  npoints   x(mm)    y(mm)    z(mm)\n" );
  segFile << aLine;

  G4int ii, jj, kk, index;
  
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        index = ii*nSlices*nSectors + jj*nSectors + kk;
	if( enFirst[index] )
	  firstPos[index] /= enFirst[index];
//	sprintf( aLine, " %4d %3d %12.3lf %10d %8.3lf %8.3lf %8.3lf\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	sprintf( aLine, " %4d %3d %12.3f %10d %8.3f %8.3f %8.3f\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	                firstPos[index].x()/mm, firstPos[index].y()/mm, firstPos[index].z()/mm );  
        segFile << aLine;
      }
    }
  }

  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "#### First interaction point (polar) #############################\n" );
  segFile << aLine;
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "# ndet nseg   Edep (keV)  npoints   R(mm) theta(deg)  phi(deg)\n" );
  segFile << aLine;

  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        index = ii*nSlices*nSectors + jj*nSectors + kk;
//	sprintf( aLine, " %4d %3d %12.3lf %10d %8.3lf %8.3lf %8.3lf\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	sprintf( aLine, " %4d %3d %12.3f %10d %8.3f %8.3f %8.3f\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	                firstPos[index].mag()/mm, firstPos[index].theta()/deg,
                       ((firstPos[index].phi()>0.) ? (firstPos[index].phi()/deg) : (firstPos[index].phi()/deg + 360.)) );
        segFile << aLine;
      }
    }
  }
  
  
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "#### Full interaction points (cartesian) )########################\n" );
  segFile << aLine;
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "# ndet nseg   Edep (keV)  npoints   x(mm)    y(mm)    z(mm)\n" );
  segFile << aLine;
  
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        index = ii*nSlices*nSectors + jj*nSectors + kk;
	if( enFull[index] )
	  fullPos[index] /= enFull[index];
//	sprintf( aLine, " %4d %3d %12.3lf %10d %8.3lf %8.3lf %8.3lf\n", ii, 10*jj+kk, enFull[index]/keV, nFull[index],
	sprintf( aLine, " %4d %3d %12.3f %10d %8.3f %8.3f %8.3f\n", ii, 10*jj+kk, enFull[index]/keV, nFull[index],
	                fullPos[index].x()/mm, fullPos[index].y()/mm, fullPos[index].z()/mm );  
        segFile << aLine;
      }
    }
  }

  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "#### Full interaction points (polar) #############################\n" );
  segFile << aLine;
  sprintf( aLine, "##################################################################\n" );
  segFile << aLine;
  sprintf( aLine, "# ndet nseg   Edep (keV)  npoints   R(mm) theta(deg)  phi(deg)\n" );
  segFile << aLine;

  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        index = ii*nSlices*nSectors + jj*nSectors + kk;
//	sprintf( aLine, " %4d %3d %12.3lf %10d %8.3lf %8.3lf %8.3lf\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	sprintf( aLine, " %4d %3d %12.3f %10d %8.3f %8.3f %8.3f\n", ii, 10*jj+kk, enFirst[index]/keV, nFirst[index],
	                fullPos[index].mag()/mm, fullPos[index].theta()/deg,
                       ((fullPos[index].phi()>0.) ? (fullPos[index].phi()/deg) : (fullPos[index].phi()/deg + 360.)) );
        segFile << aLine;
      }
    }
  }
  
  segFile.close();
  G4cout << " ---> Segment positions have been written out to file " << G4String(segFileName) << G4endl;
}

void AgataAnalysis::FlushSegHits( G4String workPath )
{
  char segFileName[256];
  sprintf(segFileName, "%sGammaHits.%4.4d", workPath.c_str(), runNumber);
  
  std::ofstream segFile;
  
  segFile.open(G4String(segFileName));
  if( !segFile.is_open() ) {
    G4cout << " ---> Could not open segment hits file! Segment hits will not be written out." << G4endl;
    return;
  }

  G4RunManager * runManager = G4RunManager::GetRunManager();

  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();

  time_t vTime;
  time( &vTime );
  
  segFile << "HITS " << G4endl;

  segFile << "G4TRACKING " 
             << theDetector->GeSD()->GetMethod() << G4endl;

  segFile << "DATE " << ctime(&vTime);  // date has already end-of-line character!

  theDetector->WriteHeader(segFile);
  theGenerator->PrintToFile(segFile);
  segFile << "$" << G4endl;    

  G4int ii, jj, kk, ll, index, indice = 0;
  
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        sprintf( aLine, " %4d %3d ", ii, 10*jj+kk );
	segFile << aLine;
      
        for( ll=0; ll<maxHits; ll++ ) {
          index = ii*maxHits*nSlices*nSectors + jj*maxHits*nSectors + kk*maxHits + ll;
	  sprintf( aLine, "%10d ", numHitsTot[index] );
	  segFile << aLine;
	}
        segFile << G4endl;
      }	
    }
  }
  segFile << "@" << G4endl;    
  
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        sprintf( aLine, " %5d ", indice );
	segFile << aLine;
      
        for( ll=0; ll<maxHits; ll++ ) {
          index = ii*maxHits*nSlices*nSectors + jj*maxHits*nSectors + kk*maxHits + ll;
	  sprintf( aLine, "%10d ", numHitsTot[index] );
	  segFile << aLine;
	}
        segFile << G4endl;
	indice++;
      }	
    }
  }
  
  segFile << "%" << G4endl;    
  indice = 0;
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        for( ll=0; ll<maxHits; ll++ ) {
          index = ii*maxHits*nSlices*nSectors + jj*maxHits*nSectors + kk*maxHits + ll;
	  sprintf( aLine, " %6d %10d\n", indice, numHitsTot[index] );
	  segFile << aLine;
	  indice++;
	}
      }	
    }
  }
  
  segFile.close();
  G4cout << " ---> Segment hits have been written out to file " << G4String(segFileName) << G4endl;
}

/////////////////////////////////////////////////////////////////
/// Extracts relevant information from the hit structure
///////////////////////////////////////////////////////////////// 
void AgataAnalysis::AddHit( AgataHitDetector* theHit )
{
  if(!enabledAnalysis)
    return;
  AddEnergy( theHit->GetEdep(), theHit->GetDetNb(), theHit->GetCluNb() );
  if(!doSegPoints)
    return;
  AddPoint( theHit->GetEdep(), theHit->GetPos(), theHit->GetDetNb(), theHit->GetSegNb() );
}

/////////////////////////////////////////////////////////////////
/// Adds energy deposition to buffer. Information from the
/// ancillary detectors is discarded!!!
///////////////////////////////////////////////////////////////// 
void AgataAnalysis::AddEnergy(G4double energy, G4int detnum, G4int clunum)
{
  //if(!enabledAnalysis)
  //  return;
  
  if(detnum >= 0 && detnum < nDetectors) {
    detAccu[detnum] += energy;
    if(detnum < minAccu) minAccu = detnum;
    if(detnum > maxAccu) maxAccu = detnum;
    det2clust[detnum] = clunum;
  }
}

/////////////////////////////////////////////////////////////////
/// Adds point deposition to buffer. Information from the
/// ancillary detectors is discarded!!!
///////////////////////////////////////////////////////////////// 
void AgataAnalysis::AddPoint(G4double energy, G4ThreeVector position, G4int detnum, G4int segnum)
{
  //if(!enabledAnalysis)
  //  return;
  //if(!doSegPoints)
  //  return;
  
  G4int index = detnum*nSectors*nSlices+segnum/10*nSectors+segnum%10;

  //G4cout << " Indici: " << energy/keV << " " << detnum << " " << segnum << " " << index << G4endl;
  
  fullPos[index] += energy*position;
  enFull [index] += energy;
  nFull  [index] ++;
  if( isFirst ) {
    firstPos[index] += energy*position;
    enFirst [index] += energy;
    nFirst  [index] ++;
    isFirst = false;
  }
  /// number of interaction points per segment
  numHitsEvt[index]++; 
  
}

void AgataAnalysis::IncrHitsEvt()
{
  G4int ii, jj, kk;
  G4int num, index;
  
  for( ii=0; ii<nDetectors; ii++ ) {
    for( jj=0; jj<nSlices; jj++ ) {
      for( kk=0; kk<nSectors; kk++ ) {
        index = ii*nSlices*nSectors + jj*nSectors + kk;
	num = numHitsEvt[index] - 1;
	if( num > 10 ) num = 10;
	if( num >= 0 )
	  numHitsTot[maxHits*index + num]++;
      }
    }
  }
  memset(numHitsEvt, 0, nDetectors*nSlices*nSectors*sizeof(G4int));
}


//////////////////////////////////////////////////////////
/// At the end of each "true" event (high multiplicity)
/// spectra are incremented. In this process, buffers are
/// erased and prepared for the next event.
/////////////////////////////////////////////////////////
void AgataAnalysis::EndOfEvent()
{
  if(!enabledAnalysis)
    return;
    
  if( !isFirst )
    isFirst = true;  

  if( packEvent && !theGenerator->IsEndOfEvent() )
    return;

  if(minAccu > maxAccu)
    return;
    
  IncrHitsEvt();  

  G4double edepTot      = 0.;
  memset(cluAccu, 0, nClusters*sizeof(G4double));
  G4int minClu = nClusters;
  G4int maxClu = 0;
  G4int clunum;
  G4int ii;
  G4int nd = 0;
  G4double edummy;
  for(ii = minAccu; ii <= maxAccu; ii++) {
    edummy   = detAccu[ii]/keV;
    if( edummy > 0. ) {
      clunum= det2clust[ii];
      detAccu[ii] = 0;
      if( resolutionEnabled )
        edummy = EnergyResolution( edummy );
      edummy   = offset + gain * edummy;
      if(edummy) {
        spec1[1]->incr((G4int)edummy, ii);          // the energy of the detectors
        spec1[3]->incr((G4int)edummy, 0);           // the energy of the detectors
        edepTot += edummy;
        nd++;
      }
      if(clunum >= 0 && clunum < nClusters) {
        cluAccu[clunum] += edummy;
        if(clunum < minClu) minClu = clunum;
        if(clunum > maxClu) maxClu = clunum;
      }
    }
  }
  minAccu = nDetectors;                           //> we have erased detAccu in the loop
  maxAccu = 0;                                    //> so we prepare it for the next event

  if (edepTot) {
    spec1[0]->incr((G4int)(edepTot*scaleEtot), 0);   //> the total energy of the array
  }

  if(minClu > maxClu)
    return;

  G4int nc = 0;
  for(ii = minClu; ii <= maxClu; ii++) {
    edummy   = cluAccu[ii];
    if(edummy) {
      spec1[2]->incr((G4int)edummy, ii);             //> the energy of the clusters
      nc++;
    }  
  }

//spec2[0]->incr((int)(edepTot*0.1), nd);
}

G4double AgataAnalysis::EnergyResolution(G4double value)
{
  //> resolution modelled as FWHM = sqrt( A + B E )
  //> FWHMk = 1/2.355
  return G4RandGauss::shoot( value, FWHMk*sqrt( FWHMa + FWHMb*value ) );
}

////////////////////////////////
/// Methods for the Messenger
////////////////////////////////
void AgataAnalysis::EnableAnalysis( G4bool enable )
{
  enabledAnalysis = enable;

  if(enabledAnalysis)
    G4cout << " ----> On-line spectra enabled!" << G4endl; 
  else
    G4cout << " ----> On-line spectra disabled!" << G4endl; 
}

void AgataAnalysis::EnableResolution(G4bool enable )
{
  resolutionEnabled = enable;

  if(resolutionEnabled)
    G4cout << " ----> Energy resolution will be considered!" << G4endl; 
  else
    G4cout << " ----> Energy resolution will not be considered!" << G4endl; 
}

void AgataAnalysis::CalculateResolution( G4double e1, G4double f1, G4double e2, G4double f2, G4bool print)
{
  if( e1 != e2 )
  {
    FWHMb = ( pow(f1, 2.)-pow(f2, 2.) )/( e1-e2 );
    FWHMa = pow(f1, 2.) - FWHMb*e1;
    if(!print) return;
    sprintf(aLine, " FWHM = %8.3f keV at  %8.3f keV \n FWHM = %8.3f keV at  %8.3f keV", f1, e1, f2, e2);
    G4cout << aLine << G4endl;
    sprintf(aLine, " ----> a = %f\n b = %f", FWHMa, FWHMb);
    G4cout << aLine << G4endl;
  }  
}

void AgataAnalysis::SetOffset( G4double value )
{
  offset = value;
  G4cout << " ----> Offset = " << offset << G4endl;
}

void AgataAnalysis::SetGain( G4double value )
{
  if( value > 0. )
    gain = value;
  else
    gain = 1.0;
  G4cout << " ----> Gain = " << gain << G4endl;    
}

void AgataAnalysis::SetPackEvent( G4bool value )
{
  packEvent = value;
  if(packEvent)
    G4cout << " ----> Packing gammas of cascade is enabled" << G4endl;
  else
    G4cout << " ----> Packing gammas of cascade is disabled" << G4endl;
}

void AgataAnalysis::SetAsciiSpectra( G4bool value )
{
  asciiSpectra = value;
  if(asciiSpectra)
    G4cout << " ----> Spectra will be written in ASCII format." << G4endl;
  else
    G4cout << " ----> Spectra will be written in BINARY format." << G4endl;
}

void AgataAnalysis::SetSpecLength( G4int value )
{
  if( value > 0 )
    specLength = value * 1024;
  else
    specLength = 1024;
  G4cout << " ----> Spectra with " << specLength << " channels"  << G4endl;    
}

void AgataAnalysis::ShowStatus()
{
  G4cout << G4endl;
  if(enabledAnalysis) {
    G4cout << " On-line spectra enabled" << G4endl;
    if(asciiSpectra)
      G4cout << " Spectra written in ASCII format." << G4endl;
    else   
      G4cout << " Spectra written in BINARY format." << G4endl;
  }
  else
    G4cout << " On-line spectra disabled" << G4endl; 

  G4cout << " Spectra with " << specLength << " channels"  << G4endl;    

  sprintf(aLine, " Offset = %f\n Gain   = %f",offset, gain);
  G4cout << aLine << G4endl;

  if(resolutionEnabled) { 
    G4cout << " Energy resolution will be applied" << G4endl;
    sprintf(aLine, " Energy resolution coefficients  a = %f   b = %f", FWHMa, FWHMb);
    G4cout << aLine << G4endl;
  }  
  else
    G4cout << " Energy resolution will not be considered" << G4endl; 

  if(packEvent)
    G4cout << " Packing gammas of cascade is enabled" << G4endl;
  else
    G4cout << " Packing gammas of cascade is disabled" << G4endl;

 }

////////////////////
/// The Messenger
////////////////////
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataAnalysisMessenger::AgataAnalysisMessenger(AgataAnalysis* pTarget)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
#ifdef GASP
  directoryName  = "/Gasp";
#else  
#ifdef CLARA
  directoryName  = "/Clara";
#else  
#ifdef POLAR
  directoryName  = "/Polar";
#else  
#ifdef DEIMOS
  directoryName  = "/Deimos";
#else  
#ifdef NARRAY
  directoryName  = "/NArray";
#else  
  directoryName  = "/Agata";
#endif  
#endif  
#endif  
#endif  
#endif  
  directoryName += "/analysis/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of on-line generated spectra.");

  commandName = directoryName + "enable";
  aLine = commandName.c_str();
  EnableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  EnableAnalysisCmd->SetGuidance("Activate the on-line spectra.");
  EnableAnalysisCmd->SetGuidance("Required parameters: none.");
  EnableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  EnableAnalysisCmd->SetDefaultValue(true);
  EnableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disable";
  aLine = commandName.c_str();
  DisableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  DisableAnalysisCmd->SetGuidance("Deactivate the on-line spectra.");
  DisableAnalysisCmd->SetGuidance("Required parameters: none.");
  DisableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  DisableAnalysisCmd->SetDefaultValue(false);
  DisableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionDisable";
  aLine = commandName.c_str();
  DisableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  DisableResolutionCmd->SetGuidance("Disable application of energy resolution.");
  DisableResolutionCmd->SetGuidance("Required parameters: none.");
  DisableResolutionCmd->SetParameterName("enabledResolution",true);
  DisableResolutionCmd->SetDefaultValue(false);
  DisableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionEnable";
  aLine = commandName.c_str();
  EnableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  EnableResolutionCmd->SetGuidance("Enable application of energy resolution.");
  EnableResolutionCmd->SetGuidance("Required parameters: none.");
  EnableResolutionCmd->SetParameterName("enabledResolution",true);
  EnableResolutionCmd->SetDefaultValue(true);
  EnableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "calculateResolution";
  aLine = commandName.c_str();
  CalculateResolutionCmd = new G4UIcmdWithAString(aLine, this);
  CalculateResolutionCmd->SetGuidance("Calculate parameters of the energy resolution starting from FWHM values at two energies.");
  CalculateResolutionCmd->SetGuidance("Required parameters: 4 double (E1, FWHM1, E2, FWHM2 in keV).");
  CalculateResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "offset";
  aLine = commandName.c_str();
  SetOffsetCmd = new G4UIcmdWithADouble(aLine, this);  
  SetOffsetCmd->SetGuidance("Define offset of the energy calibration.");
  SetOffsetCmd->SetGuidance("Required parameters: 1 double.");
  SetOffsetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "gain";
  aLine = commandName.c_str();
  SetGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetGainCmd->SetGuidance("Define gain of the energy calibration.");
  SetGainCmd->SetGuidance("Required parameters: 1 double.");
  SetGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "packGammas";
  aLine = commandName.c_str();
  PackEventCmd = new G4UIcmdWithABool(aLine, this);
  PackEventCmd->SetGuidance("Activate packing the gammas of a cascade in the on-line analysis.");
  PackEventCmd->SetGuidance("Does not affect the LM output file!");
  PackEventCmd->SetGuidance("Required parameters: none.");
  PackEventCmd->SetParameterName("packEvent",true);
  PackEventCmd->SetDefaultValue(true);
  PackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "unpackGammas";
  aLine = commandName.c_str();
  UnpackEventCmd = new G4UIcmdWithABool(aLine, this);
  UnpackEventCmd->SetGuidance("Deactivate packing the gammas of a cascade in the on-line analysis.");
  UnpackEventCmd->SetGuidance("Does not affect the LM output file!");
  UnpackEventCmd->SetGuidance("Required parameters: none.");
  UnpackEventCmd->SetParameterName("UnpackEvent",true);
  UnpackEventCmd->SetDefaultValue(false);
  UnpackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "asciiSpectra";
  aLine = commandName.c_str();
  SetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  SetAsciiSpectraCmd->SetGuidance("Enables ASCII writing in the on-line analysis.");
  SetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  SetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  SetAsciiSpectraCmd->SetDefaultValue(true);
  SetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "binarySpectra";
  aLine = commandName.c_str();
  UnsetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  UnsetAsciiSpectraCmd->SetGuidance("Enables BINARY writing in the on-line analysis.");
  UnsetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  UnsetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  UnsetAsciiSpectraCmd->SetDefaultValue(false);
  UnsetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "specLength";
  aLine = commandName.c_str();
  SetSpecLengthCmd = new G4UIcmdWithAnInteger(aLine, this);  
  SetSpecLengthCmd->SetGuidance("Define number of channels of spectra");
  SetSpecLengthCmd->SetGuidance("Required parameters: 1 integer (spectra length as a multiple of 1024 channels)");
  SetSpecLengthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataAnalysisMessenger::~AgataAnalysisMessenger()
{
  delete myDirectory;
  delete EnableAnalysisCmd;
  delete DisableAnalysisCmd;
  delete EnableResolutionCmd;
  delete DisableResolutionCmd;
  delete CalculateResolutionCmd;
  delete SetOffsetCmd;
  delete SetGainCmd;
  delete PackEventCmd;
  delete UnpackEventCmd;
  delete SetSpecLengthCmd;
  delete StatusCmd;
}

void AgataAnalysisMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 

  if( command == EnableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableResolutionCmd ) {
    myTarget->EnableResolution( EnableResolutionCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableResolutionCmd ) {
    myTarget->EnableResolution( DisableResolutionCmd->GetNewBoolValue(newValue) );
  }
  if( command == CalculateResolutionCmd ) { 
    float e1, e2, f1, f2;
//    sscanf( newValue, " %lf %lf %lf %lf ", &e1, &f1, &e2, &f2 );
    sscanf( newValue, " %f %f %f %f ", &e1, &f1, &e2, &f2 );
    myTarget->CalculateResolution((G4double)e1, (G4double)f1, (G4double)e2, (G4double)f2 );
  }
  if( command == SetOffsetCmd ) {
    myTarget->SetOffset(SetOffsetCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetGainCmd ) {
    myTarget->SetGain(SetGainCmd->GetNewDoubleValue(newValue));
  }
  if( command == PackEventCmd ) {
    myTarget->SetPackEvent( PackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnpackEventCmd ) {
    myTarget->SetPackEvent( UnpackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( SetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnsetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( UnsetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetSpecLengthCmd ) {
    myTarget->SetSpecLength(SetSpecLengthCmd->GetNewIntValue(newValue));
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus( );
  }

}


