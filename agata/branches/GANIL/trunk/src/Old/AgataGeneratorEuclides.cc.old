#include "AgataGeneratorEuclides.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"


AgataGeneratorEuclides::AgataGeneratorEuclides( G4double step, G4int numEner, G4int numEvents )
{
  targetEvents  = numEvents;
  energyStep    = step;
  currentEnergy = step;
  
  particleNames = new G4String[3];
  particleNames[0] = G4String("proton");
  particleNames[1] = G4String("alpha");
  particleNames[2] = G4String("deuteron");
  
  particleIndex = 0;
  
  currentParticle = particleNames[particleIndex];
  
  currentEvent = 0;
  
  currentEner = 0;
  maxEner     = numEner;
  maxEnergy   = (numEner+1) * step;
  
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  particleGun->SetParticleDefinition(particleTable->FindParticle(currentParticle));
  particleGun->SetParticlePosition(G4ThreeVector());
  particleGun->SetParticleEnergy(currentEnergy);
  
}

AgataGeneratorEuclides::~AgataGeneratorEuclides()
{
  delete particleGun;
}

void AgataGeneratorEuclides::GeneratePrimaries(G4Event* anEvent)
{ 
  G4double costheta = 2.0 * G4UniformRand() - 1.0;
  G4double sintheta = sqrt( 1 - costheta * costheta );
  G4double phi      = 360. * G4UniformRand() * deg;
  
  G4ThreeVector direction;
  direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );

  particleGun->SetParticleMomentumDirection( direction );
  particleGun->GeneratePrimaryVertex(anEvent);

  currentEvent++;
  if( currentEvent == targetEvents ) {
    currentEvent = 0;
    currentEner++;
    if( currentEner == maxEner ) {
      currentEner = 0;
      particleIndex++;
      if( particleIndex > 2 ) particleIndex = 2;
      currentParticle = particleNames[particleIndex];
      G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
      particleGun->SetParticleDefinition(particleTable->FindParticle(currentParticle));
    }
    currentEnergy = energyStep * (currentEner+1);
    particleGun->SetParticleEnergy(currentEnergy);
  }
}
