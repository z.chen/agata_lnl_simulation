#include "AgataSteppingAction.hh"
#include "G4Step.hh"
#include "G4VProcess.hh"
#include <fstream>

#ifdef _CHECKOSDECAY_
std::ofstream bla2("decay2.txt");
std::ofstream bla4("decay4.txt");
std::ofstream bla6("decay6.txt");
std::ofstream bla8("decay8.txt");
#endif

void AgataSteppingAction::UserSteppingAction(const G4Step* theStep)
{
#ifdef _CHECKOSDECAY_
  if(theStep->GetTrack()->GetParticleDefinition()->GetParticleName()=="Os170[286.7]"){
    if(theStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="RadioactiveDecay"){
      bla2 << theStep->GetPostStepPoint()->GetGlobalTime()  << "\n";
    }
  }
  if(theStep->GetTrack()->GetParticleDefinition()->GetParticleName()=="Os170[749.9]"){
    if(theStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="RadioactiveDecay"){
      bla4 << theStep->GetPostStepPoint()->GetGlobalTime()  << "\n";
    }
  }
  if(theStep->GetTrack()->GetParticleDefinition()->GetParticleName()=="Os170[1325.4]"){
    if(theStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="RadioactiveDecay"){
      bla6 << theStep->GetPostStepPoint()->GetGlobalTime()  << "\n";
    }
  }
  if(theStep->GetTrack()->GetParticleDefinition()->GetParticleName()=="Os170[1945.8]"){
    if(theStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName()=="RadioactiveDecay"){
      bla8 << theStep->GetPostStepPoint()->GetGlobalTime()  << "\n";
    }
  }
#endif
  G4int stepNumber = theStep->GetTrack()->GetCurrentStepNumber();
  if(stepNumber > 100000) {
    G4cout << "UserSteppingAction: Too many steps (" << stepNumber << ")  ==> track killed" << G4endl;
    theStep->GetTrack()->SetTrackStatus(fStopAndKill);
  }
}
