#include "OrsayPlastic.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

OrsayPlastic::OrsayPlastic(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  dirName = name;
  
  OrsayPlasticInnerR=0*mm;
  OrsayPlasticOuterR=38.1*mm;
  OrsayPlasticLength=76.2*mm;
  OrsayPlasticZ=250*mm;
  matOrsayPlastic    = NULL;
  matName     = "NaI";
  
  ancSD       = NULL;
  ancName     = G4String("OrsayPlastic");
  ancOffset   = 18000;
  
  numAncSd = 0;

  myMessenger = new OrsayPlasticMessenger(this,name);
}

OrsayPlastic::~OrsayPlastic()
{
  delete myMessenger;
}

G4int OrsayPlastic::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matOrsayPlastic = ptMaterial;
    G4String nome = matOrsayPlastic->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary shell! " << G4endl;
    return 1;
  }  
  return 0;
}

void OrsayPlastic::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/orsayplastic", 
					"OrsayPlasticAncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
}


void OrsayPlastic::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void OrsayPlastic::Placement()
{
  ///////////////////////
  /// construct a tube section
  ///////////////////////
  G4Tubs *solidOrsayPlastic = new G4Tubs("tubsector", 
					 OrsayPlasticInnerR, 
					 OrsayPlasticOuterR, 
					 OrsayPlasticLength/2,
					 0.*deg, 360*deg);
  G4LogicalVolume *logicOrsayPlastic = 
    new G4LogicalVolume(solidOrsayPlastic, 
			matOrsayPlastic, 
			"OrsayPlastic", 0, 0, 0 );

  G4RotationMatrix *rmDetA = new G4RotationMatrix;
  rmDetA->rotateZ(0);
  new G4PVPlacement(rmDetA,G4ThreeVector(0,0,
					 OrsayPlasticZ+OrsayPlasticLength/2), 
		    "OrsayPlastic", logicOrsayPlastic, 
		    theDetector->HallPhys(), false, 0);
  // Sensitive Detector
  logicOrsayPlastic->SetSensitiveDetector( ancSD );

  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, .5) );
  logicOrsayPlastic->SetVisAttributes( pVA );

  G4cout << "OrsayPlastic radius " << OrsayPlasticInnerR/cm << " -- " << OrsayPlasticOuterR/cm << " cm" << G4endl;
  G4cout << "OrsayPlastic material is " << matOrsayPlastic->GetName() << G4endl;

  return;
}

////////////////////////////////
/// methods for the messenger
////////////////////////////////

void OrsayPlastic::SetOrsayPlasticRmin( G4double radius )
{
  if( radius < 0. ) {
  }
  else {
  }
}

void OrsayPlastic::SetOrsayPlasticRmax( G4double radius )
{
  if( radius < 0. ) {
  }
  else {
  }
}


void OrsayPlastic::ShowStatus()
{
  G4cout << " Orsay Plastic has been constructed\n" << G4endl;
  G4cout << " Orsay Plastic  material is " << matOrsayPlastic->GetName() << G4endl;
}

void OrsayPlastic::WriteHeader(std::ofstream &outFileLMD, G4double)
{
  outFileLMD << "ORSAY-PLASTIC DET. 8 SEG OFFSET\n";
  outFileLMD << " coded as " << ancOffset << "+SegId\n";
  outFileLMD << "END ORSAY-PLASTIC\n";
}

///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

OrsayPlasticMessenger::OrsayPlasticMessenger(OrsayPlastic* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 
  
  myDirectory = new G4UIdirectory("/Orgam/detector/ancillary/OrsayPlastic/");
  myDirectory->SetGuidance("Control of Orsay Plastic detector construction.");

  /*  SetOrsayPlasticSizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/OrsayPlastic/ancillarySize", this);  
  SetOrsayPlasticSizeCmd->SetGuidance("Define size of the ancillary shell.");
  SetOrsayPlasticSizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetOrsayPlasticSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  OrsayPlasticMatCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/OrsayPlastic/ancillaryMaterial", this);
  OrsayPlasticMatCmd->SetGuidance("Select material of the ancillary.");
  OrsayPlasticMatCmd->SetGuidance("Required parameters: 1 string.");
  OrsayPlasticMatCmd->SetParameterName("choice",false);
  OrsayPlasticMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);*/
}

OrsayPlasticMessenger::~OrsayPlasticMessenger()
{
  delete myDirectory;
  delete OrsayPlasticMatCmd;
  delete SetOrsayPlasticSizeCmd;
}

void OrsayPlasticMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetOrsayPlasticSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetOrsayPlasticRmin( e1*mm );
    myTarget->SetOrsayPlasticRmax( e2*mm );
  }
  if( command == OrsayPlasticMatCmd ) {
    
  }
}    

G4int OrsayPlastic::GetSegmentNumber( G4int, G4int , G4ThreeVector )
{
  //G4cout<<i2<<"  "<<v1<<G4endl;
  return 1;
}
