#include "AgataEventAction.hh"
#include "AgataRunAction.hh"
#include "AgataGeneratorAction.hh"
#include "AgataAnalysis.hh"
#include "AgataHitDetector.hh"
#include "AgataDetectorConstruction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4Point3D.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include <cstdio>

#define MGS_TRASL (51.0 * mm)


AgataEventAction::AgataEventAction( AgataAnalysis* pAna, G4String name )
: theAnalysis(pAna)
{ 
  writeAll     = true;
  drawRays     = false;
  drawHits     = false;
  
  // default data output is for TRACKING
  writeData[0] = true;
  writeData[1] = true;
  writeData[2] = true;
  writeData[3] = false;
  writeData[4] = false;
  writeData[5] = true;
  writeData[6] = false;
  writeData[7] = false;
  
  pulseShape   = false;
#ifdef SWAP_PAR
  mgsFormat    = false;   // if PULSE SHAPE format is selected, reference frame will not be of mgs type
#else
  mgsFormat    = true;   // if PULSE SHAPE format is selected, reference frame will be of mgs type
#endif
  
  unitLength   = 1.*mm;
  unitEnergy   = 1.*keV;
  
  interval     = 500;
  
  outMask      = G4String("11100100");
    
  packDist     = 0;
  packTimeEThreshold = 0;  //mp
  aLine        = new char[256];
  
  myMessenger  = new AgataEventActionMessenger(this, name);
}

AgataEventAction::~AgataEventAction()
{
  delete myMessenger;
  delete [] aLine;
}

void AgataEventAction::BeginOfRun()
{
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theRun = (AgataRunAction *)runManager->GetUserRunAction();
  
  theDetector = (AgataDetectorConstruction*)runManager->GetUserDetectorConstruction();
  
  if( !theDetector->CalcOmega() )
    theGenerator = (AgataGeneratorAction*)runManager->GetUserPrimaryGeneratorAction();
    
  if( mgsFormat ) {
    if( writeData[3] ) {
      writeData[3] = false;
      writeData[4] = true;
    }
  }
  else {
    if( writeData[4] ) {
      writeData[4] = false;
      writeData[3] = true;
    }
  }  
    
  G4int nEnabled = -1;
  for( G4int ii=0; ii<8; ii++ ) {
    if( writeData[ii] ) {
      outMask(ii) = '1';
      nEnabled++;
    }
    else
      outMask(ii) = '0';
  }
  if( nEnabled < 0 ) {
    G4cout << " ----> Warning! No interaction point will be written to file!" << G4endl;
  } 
}

void AgataEventAction::WriteHeader( std::ofstream &outFileLMD )
{
  outFileLMD << "OUTPUT_MASK " << outMask << G4endl;
  outFileLMD << "# nDet Energy AbsolutePosition(x y z) RelativePosition(x' y' z') " << G4endl;
  outFileLMD << "# RelativePosition for mgs (x'' y'' z'') nSeg time interaction" << G4endl;
  outFileLMD << "# 0--> disabled; 1--> enabled" << G4endl;
  outFileLMD << "DISTFACTOR " << unitLength/mm << G4endl;
  outFileLMD << "ENERFACTOR " << unitEnergy/keV << G4endl;
}

void AgataEventAction::BeginOfEventAction(const G4Event* evt)
{
  if( !theDetector->CalcOmega() ) {
    if( theGenerator->IsAbortRun() ) {
      G4RunManager * runManager = G4RunManager::GetRunManager();
      G4cout << theGenerator->GetAbortMessage() << G4endl;
      runManager->AbortEvent();
      runManager->AbortRun(false);
    }  
  }
  
  eventID = evt->GetEventID();
  // periodic printing
  if (eventID < 100 || eventID%interval == 0)  {
    printf( " >>> Ev.# %5d\r", eventID);
    G4cout.flush(); 
  }
  
  if( theRun->DoWrite() ) {
    if( theRun->GetLMFileSize() > theRun->GetMaxFileSize() )
      theRun->SplitLMFile();
  }
}

void AgataEventAction::EndOfEventAction(const G4Event* evt)
{
#ifndef EUCLIDES
  if( theDetector->CalcOmega() )
    return;
  
  if( theGenerator->IsAbortRun() )
    return;
#endif
  ////////////////////////////
  /// Write to List Mode File
  ////////////////////////////
  if( theRun->DoWrite() ) {
    
    ////////////////////////////////////
    // Begin Of Event Tag
    ///////////////////////////////////
    if( theGenerator->IsStartOfEvent() ) {
     theRun->LMwrite(theGenerator->GetBeginOfEventTag());
      ////////////////////////////////////
    } 
    /// prepare and write event header
    ////////////////////////////////////
    if( !pulseShape )
      theRun->LMwrite( theGenerator->GetEventHeader(unitLength) );
  }
    
  G4HCofThisEvent* HCEV = evt->GetHCofThisEvent();
  if( HCEV==NULL ) {
#ifndef EUCLIDES
    // keep this call as the last one!
    theGenerator->EndOfEvent();
#endif  
    return;
  } 
  
  G4int nHCEV = HCEV->GetNumberOfCollections();
    
  if(!writeAll) {
    G4int nhits = 0;
    for(G4int nn = 0; nn < nHCEV; nn++) {
      AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
      nhits += theHits->entries();
    }
    if(!nhits) {
      if( theAnalysis->IsEnabled() )
        theAnalysis->EndOfEvent();
#ifndef EUCLIDES
      // keep this call as the last one!
      theGenerator->EndOfEvent();
#endif  
      return;
    }
  }

  ////////////////////////////
  /// Write to List Mode File
  ////////////////////////////
  if( theRun->DoWrite() ) {
    ////////////////////////////////////
    /// emitted particle
    ////////////////////////////////////
    theRun->LMwrite( theGenerator->GetParticleHeader( evt, unitLength, unitEnergy ) );
    
    ///////////////////////////
    /// write the event hits
    //////////////////////////
    G4int nn, nhits;
    AgataHitDetector *aHit;
    G4double      edep;
    G4double      time;
    G4Point3D     posA;
    G4Point3D     posI;
    G4int         detnum;
    G4int         segnum;
    G4int         inter;

    // MP  May 2009 sort hits in time order
    if (timeSorting) {
      G4int nhits_total = 0;
      for(nn = 0; nn < nHCEV; nn++) {
	AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
	nhits = theHits->entries();
	nhits_total = nhits_total + nhits;
      }
      if(nhits_total) {
	G4double *edep_table = new G4double [nhits_total];
	G4double *time_table = new G4double [nhits_total];
	G4Point3D *posA_table = new G4Point3D [nhits_total];
	G4Point3D *posI_table  = new G4Point3D [nhits_total];
	G4int     *detnum_table = new G4int [nhits_total];
	G4int     *segnum_table = new G4int [nhits_total];
	G4int     *inter_table = new G4int [nhits_total];
	G4int     *hit_index = new G4int [nhits_total]; 
	G4int i_hit;
	G4int i_hit_all = 0;
	for(nn = 0; nn < nHCEV; nn++) {
	  AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
	  nhits = theHits->entries();
	  for (i_hit = 0; i_hit<nhits; i_hit++) {
	    aHit =   (*theHits)[i_hit];
	    edep_table[i_hit_all]   = aHit->GetEdep();
	    posA_table[i_hit_all]   = aHit->GetPos();
	    posI_table[i_hit_all]   = aHit->GetRelPos();
	    time_table[i_hit_all]   = aHit->GetTime();
	    detnum_table[i_hit_all] = aHit->GetDetNb();
	    segnum_table[i_hit_all] = aHit->GetSegNb();
	    inter_table[i_hit_all]  = aHit->GetInterNb();
	    time_table[i_hit_all] = aHit->GetTime();
	    i_hit_all++;
	  }
	}
	tagSort(hit_index,time_table,nhits_total);
	G4int i_sorted;
	if (packDist <= 0) {
	  for (i_hit = 0; i_hit<nhits_total; i_hit++) {
	    i_sorted = hit_index[i_hit];
	    detnum = detnum_table[i_sorted];
	    edep  =  edep_table[i_sorted];
	    posA  = posA_table[i_sorted];
	    posI = posI_table[i_sorted];
	    segnum = segnum_table[i_sorted];
	    time = time_table[i_sorted];
	    inter = inter_table[i_sorted];
	    theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	  }
	} 
 	else {
 	  // case with pseudo-packing: 
 	  // if hit is close enough to the previous one:
 	  //        replace the two points with the energy-weighted average position
 	  // if not, print previous point
 	  G4double      edepT;
 	  G4Point3D     posAT;
 	  G4Point3D     posIT;
 	  G4double      timeT;
 	  G4int         detnumT;
 	  G4int         segnumT;
 	  G4int         interT;
	  G4double      time_overEThreshold = 0;
 	  i_sorted =  hit_index[0];
 	  edep   = edep_table[i_sorted];
 	  posA  = posA_table[i_sorted];
 	  posI = posI_table[i_sorted];
 	  segnum = segnum_table[i_sorted];
	  detnum = detnum_table[i_sorted];
 	  time = time_table[i_sorted];
 	  inter = inter_table[i_sorted];
	  timeT = time_table[i_sorted];   // mp: timeT is needed even for single interaction
	  if (edep >  packTimeEThreshold && !time_overEThreshold)  
	    time_overEThreshold = timeT;
	  for(G4int i  = 1; i < nhits_total; i++) {
 	    i_sorted =  hit_index[i];
 	    edepT   = edep_table[i_sorted];
 	    posAT  = posA_table[i_sorted];
 	    posIT = posI_table[i_sorted];
 	    segnumT = segnum_table[i_sorted];
	    detnumT = detnum_table[i_sorted];
 	    interT = inter_table[i_sorted];
 	    timeT = time_table[i_sorted];
 	    if( detnumT != detnum || segnumT != segnum || posAT.distance(posA) > packDist ) {
 	      // chain broken ==> print previous
	      if (packTimeEThreshold>=0) {    // if packTimeEThreshold < 0 do nothing
		                              // i.e. write out weighted avarage time
		if (time_overEThreshold) 
		  time = time_overEThreshold;
		else 
		  time = -time;
	      }
	      theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	      // and update point
	      time_overEThreshold = 0.0;
	      edep   = edepT;
	      posA   = posAT;
	      posI   = posIT;
	      time   = timeT;
	      detnum = detnumT;
	      segnum = segnumT;
	      inter  = interT;
	    }
	    else {
	      // energy-weighted average position of point and pointT
	      edep += edepT;
	      posA  = ((edep-edepT)*posA + edepT*posAT)/edep;
	      posI  = ((edep-edepT)*posI + edepT*posIT)/edep;
	      time  = ((edep-edepT)*time + edepT*timeT)/edep;
	      if (edep >  packTimeEThreshold && !time_overEThreshold)  
		time_overEThreshold = timeT;
	    }
	  }
	  // print the last point of this collection
	  if (time_overEThreshold) 
	    time = time_overEThreshold;
	  else 
	    time = -time;
	      theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	}  // if  packDist > 0 and sorted
	delete [] edep_table;
	delete [] time_table;
	delete [] posA_table;
	delete [] posI_table;
	delete [] detnum_table;
	delete [] segnum_table;
	delete [] inter_table;
      }   // if nhits_total
    }   // if sorted_output
    else {
      if( packDist <= 0.) {                     
	// standard case with no pseudo-packing
	for(nn = 0; nn < nHCEV; nn++) {
	  AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
	  nhits = theHits->entries();
	  for(G4int i = 0; i < nhits; i++) {
	    aHit   = (*theHits)[i];
	    edep   = aHit->GetEdep();
	    posA   = aHit->GetPos();
	    posI   = aHit->GetRelPos();
	    time   = aHit->GetTime();
	    detnum = aHit->GetDetNb();
	    segnum = aHit->GetSegNb();
	    inter  = aHit->GetInterNb();
	    theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	  }
	}
      }
      else {
	// case with pseudo-packing: 
	// if hit is close enough to the previous one:
	//        replace the two points with the energy-weighted average position
	// if not, print previous point
	G4double      edepT;
	G4Point3D     posAT;
	G4Point3D     posIT;
	G4double      timeT;
	G4int         detnumT;
	G4int         segnumT;
	G4int         interT;
	for(nn = 0; nn < nHCEV; nn++) {
	  AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
	  nhits = theHits->entries();
	  if(!nhits) continue;
	  aHit    = (*theHits)[0];
	  edep   = aHit->GetEdep();
	  posA   = aHit->GetPos();
	  posI   = aHit->GetRelPos();
	  detnum = aHit->GetDetNb();
	  segnum = aHit->GetSegNb();
	  inter  = aHit->GetInterNb();
	  time   = aHit->GetTime();
	  for(G4int i = 1; i < nhits; i++) {
	    aHit    = (*theHits)[i];
	    edepT   = aHit->GetEdep();
	    posAT   = aHit->GetPos();
	   posIT   = aHit->GetRelPos();
	   detnumT = aHit->GetDetNb();
	   segnumT = aHit->GetSegNb();
	   interT  = aHit->GetInterNb();
	   timeT   = aHit->GetTime();
	   if( detnumT != detnum || segnumT != segnum || posAT.distance(posA) > packDist ) {
	     // chain broken ==> print previous
	     theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	     // and update point
	     edep   = edepT;
	     posA   = posAT;
	     posI   = posIT;
	     time   = timeT;
	     detnum = detnumT;
	     segnum = segnumT;
	     inter  = interT;
	   }
	   else {
	     // energy-weighted average position of point and pointT
	     edep += edepT;
	     posA  = ((edep-edepT)*posA + edepT*posAT)/edep;
	     posI  = ((edep-edepT)*posI + edepT*posIT)/edep;
	     time  = ((edep-edepT)*time + edepT*timeT)/edep;
	   }
	 }
	  // print the last point of this collection
	  theRun->LMwrite( PrepareLine(detnum, edep, posA, posI, segnum, time, inter) );
	}
      } //  if else sorted_output
    }
  }  
  ////////////////////////////////
  /// dispatch data to analysis
  ////////////////////////////////
  if( theAnalysis->IsEnabled() ) {
    G4int nn, nhits;
    AgataHitDetector *aHit;
    for(nn = 0; nn < nHCEV; nn++) {
      AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
      nhits = theHits->entries();
      for(G4int i = 0; i < nhits; i++) {
        aHit   = (*theHits)[i];
        theAnalysis->AddHit( aHit );
      }
    }
    this->FlushAnalysis();
  }
  
  ////////////////
  /// draw hits
  ////////////////
  if( drawHits ) {
    G4int nn, nhits;
    AgataHitDetector *aHit;
    for(nn = 0; nn < nHCEV; nn++) {
      AgataHitDetectorCollection* theHits = (AgataHitDetectorCollection*)HCEV->GetHC(nn);
      nhits = theHits->entries();
      for(G4int i = 0; i < nhits; i++) {
        aHit = (*theHits)[i];
        aHit->Draw();
      }
    }
  }
  
  ///////////////////////
  /// draw trajectories
  //////////////////////
  if(drawRays) {
    G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
    G4int n_trajectories = 0;
    if (trajectoryContainer) n_trajectories = trajectoryContainer->entries();
    if(n_trajectories) {
      if (G4VVisManager::GetConcreteInstance()) {
        for (G4int i=0; i<n_trajectories; i++) { 
          G4Trajectory* trj = (G4Trajectory*)((*(evt->GetTrajectoryContainer()))[i]);
          trj->DrawTrajectory(50);
        }
      }
    }
  }

#ifndef EUCLIDES
  // keep this call as the last one!
  theGenerator->EndOfEvent();
#endif  
}

G4String AgataEventAction::PrepareLine( G4int ndet, G4double ener, G4ThreeVector pos1, 
                               G4ThreeVector pos2, G4int nseg, G4double time, G4int inter )
{
  G4String dummy1 = "";
  char     dummy2[128];
  
  if( writeData[0] ) {
    sprintf( dummy2, "%5d ", ndet );
    dummy1 += G4String(dummy2);
  }
  if( writeData[1] ) {
    sprintf( dummy2, "%9.3f ", ener/unitEnergy );
    dummy1 += G4String(dummy2);
  }
  if( writeData[2] ) {
    sprintf( dummy2, "%8.3f %8.3f %8.3f ", pos1.x()/unitLength, pos1.y()/unitLength, pos1.z()/unitLength );
    dummy1 += G4String(dummy2);
  }
#ifndef SWAP_PAR
  if( writeData[3] ) {
    sprintf( dummy2, "%8.4f %8.4f %8.4f ", pos2.x()/unitLength, pos2.y()/unitLength, pos2.z()/unitLength );
    dummy1 += G4String(dummy2);
  }
#endif
  if( writeData[4] ) {
    sprintf( dummy2, "%8.4f %8.4f %8.4f ", (pos2.x()+MGS_TRASL)/unitLength, (pos2.y()+MGS_TRASL)/unitLength, pos2.z()/unitLength );
    dummy1 += G4String(dummy2);
  }
  if( writeData[5] ) {
    sprintf( dummy2, "%2.2d ", nseg );
    dummy1 += G4String(dummy2);
  }
  if( writeData[6] ) {
    sprintf( dummy2, "%9.3f ", time/ns );
    dummy1 += G4String(dummy2);
  }
#ifdef SWAP_PAR
  if( writeData[3] ) {
    sprintf( dummy2, "%8.4f %8.4f %8.4f ", pos2.x()/unitLength, pos2.y()/unitLength, pos2.z()/unitLength );
    dummy1 += G4String(dummy2);
  }
#endif
  if( writeData[7] ) {
    sprintf( dummy2, "%2.2d ", inter );
    dummy1 += G4String(dummy2);
  }
  dummy1.replace( dummy1.size()-1, 1, "\n" );
  return dummy1;
}                    

void AgataEventAction::FlushAnalysis()
{
  theAnalysis->EndOfEvent();
}


////////////////////////////////
// Methods for the Messenger
///////////////////////////////

void AgataEventAction::SetWriteAll( G4bool value )
{
  writeAll = value;
  if(writeAll)
    G4cout << " ----> All gammas will be written in LM" << G4endl;
  else
    G4cout << " ----> Only gammas with hits will be written in LM" << G4endl;
}

void AgataEventAction::SetDrawHits( G4bool value )
{
  drawHits = value;
  if(drawHits)
    G4cout << " ----> Drawing of hits enabled" << G4endl;
  else
    G4cout << " ----> Drawing of hits disabled" << G4endl;
}

void AgataEventAction::SetDrawRays( G4bool value )
{
  drawRays = value;
  if(drawRays)
    G4cout << " ----> Drawing of g-rays enabled" << G4endl;
  else
    G4cout << " ----> Drawing of g-rays disabled" << G4endl;
}

void AgataEventAction::SetPackDist( G4double value )
{
  if( value > 0. ) {
    packDist = value * mm;
    G4cout << " ----> Data in the list-mode file will be packed with " 
           << packDist/mm << " mm packing distance." << G4endl;
  }
  else {
    packDist = 0.;
    G4cout << " ----> Data packing in the list-mode output has been disabled." << G4endl;
  }
}

void AgataEventAction::SetPackTimeEThreshold( G4double value) 
{
  if( value > 0. ) {
    packTimeEThreshold = value * keV;
  }
  else {
    packTimeEThreshold = 0.;
  }
  G4cout << " Energy threshold for times of sorted and packed interactions is set at " 
	 << packTimeEThreshold/keV << " keV." << G4endl;
}


void AgataEventAction::SetUnitLength( G4double value )
{
  if( value > 0. )
    unitLength = value * mm;
  G4cout << " ----> Unit length for distances in the list-mode file will be " 
           << unitLength/mm << " mm." << G4endl;
}

void AgataEventAction::SetUnitEnergy( G4double value )
{
  if( value > 0. )
    unitEnergy = value * keV;
  G4cout << " ----> Unit energy in the list-mode file will be " 
           << unitEnergy/keV << " keV." << G4endl;
}

void AgataEventAction::SetInterval( G4int value )
{
  if( value > 0. )
    interval = value;
  G4cout << " ----> Event number will be printed out each " 
           << interval << " events." << G4endl;
}

void AgataEventAction::SetEnableMgsFormat( G4bool value )
{
  mgsFormat = value;
  if( mgsFormat )
    G4cout << " ----> The intrinsic coordinates will be written according to the MGS reference frame." << G4endl;
  else
    G4cout << " ----> The intrinsic coordinates will be written according to the GEANT4 reference frame." << G4endl;
}

void AgataEventAction::SetEnableNDet( G4bool value )
{
  writeData[0] = value;
  if( writeData[0] )
    G4cout << " ----> Detector number will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Detector number will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnableEner( G4bool value )
{
  writeData[1] = value;
  if( writeData[1] )
    G4cout << " ----> Interaction energy will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction energy will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnablePosA( G4bool value )
{
  writeData[2] = value;
  if( writeData[2] )
    G4cout << " ----> Interaction absolute position will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction absolute position will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnablePosI( G4bool value )
{
  writeData[3] = value;
  if( writeData[3] )
    G4cout << " ----> Interaction relative position will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction relative position will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnablePosM( G4bool value )
{
  writeData[4] = value;
  if( writeData[4] )
    G4cout << " ----> Interaction relative position (in mgs coordinates) will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction relative position (in mgs coordinates) will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnableNSeg( G4bool value )
{
  writeData[5] = value;
  if( writeData[5] )
    G4cout << " ----> Segment number will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Segment number will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnableTime( G4bool value )
{
  writeData[6] = value;
  if( writeData[6] )
    G4cout << " ----> Interaction time will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction time will not be written in the LM file." << G4endl;
}

void AgataEventAction::SetEnableInte( G4bool value )
{
  writeData[7] = value;
  if( writeData[7] )
    G4cout << " ----> Interaction type will be written in the LM file." << G4endl;
  else
    G4cout << " ----> Interaction type will not be written in the LM file." << G4endl;
}
//mp:

void AgataEventAction::SetEnableTimeSorting( G4bool value){
  timeSorting=value;
  if(timeSorting)
    G4cout << " Interactions will be time sorted in the LM file." << G4endl;
  else
    G4cout << " Interactions won't be time sorted in the LM file." << G4endl;
}


void AgataEventAction::SetOutMask( G4String mask )
{
  G4int ii = 0;
  G4int size = mask.size();
  
  if( size >= 8 ) {
    for( ii=0; ii<8; ii++ ) {
      if( (mask(ii) == '0') || (mask(ii) == 'n') || (mask(ii) == 'N') ) {
        outMask(ii) = '0';
        writeData[ii] = false;
      }
      else {
        outMask(ii) = '1';
        writeData[ii] = true;
      }
    }
  }
  else {
    G4cout << " Warning! Parameters " << size-1 << "--7 will not be modified!!" << G4endl;
    for( ii=0; ii<size; ii++ ) {
      if( (mask(ii) == '0') || (mask(ii) == 'n') || (mask(ii) == 'N') ) {
        outMask(ii) = '0';
        writeData[ii] = false;
      }
      else {
        outMask(ii) = '1';
        writeData[ii] = true;
      }
    }
  }
  
  G4int nEnabled = -1;
  for( ii=0; ii<8; ii++ )
    if( outMask(ii) == '1' )
      nEnabled++;
      
  if( nEnabled < 0 )
    G4cout << " Warning! No information selected for output in the LM file." << G4endl;
    
  pulseShape = false;       
  
  G4cout << " ----> Output mask has been set to " << outMask << G4endl;
}

void AgataEventAction::DataForTracking()
{
  G4cout << " Setting output mask for TRACKING ..." << G4endl;
  SetOutMask("11100100");
}

void AgataEventAction::DataForPulseShape()
{
  if( mgsFormat ) {  
    G4cout << " Setting output mask for PULSE SHAPE (mgs coordinates) ..." << G4endl;
    SetOutMask("11001100");
  }
  else {
    G4cout << " Setting output mask for PULSE SHAPE ..." << G4endl;
    SetOutMask("11010100");
  }
  pulseShape  = true;
}

void AgataEventAction::ShowStatus()
{
  G4cout << G4endl;
  G4cout << " Unit length for distances in the LM file will be " << unitLength/mm << " mm" << G4endl;
  if(writeAll)
    G4cout << " All gammas will be written in LM" << G4endl;
  else
    G4cout << " Only gammas with hits will be written in LM" << G4endl;
  
  if(drawHits)
    G4cout << " Drawing of hits enabled" << G4endl;
  else
    G4cout << " Drawing of hits disabled" << G4endl;
  
  if(drawRays)
    G4cout << " Drawing of g-rays enabled" << G4endl;
  else
    G4cout << " Drawing of g-rays disabled" << G4endl;
    
  G4cout << " Output mask for the LM file is " << outMask << G4endl;  

  if(packDist > 0)
    G4cout << " Packing distance for the LM file "<< packDist/mm << " mm" << G4endl;
  else 
    G4cout << " Packing of points for LM file is disabled" << G4endl;
  if (timeSorting) {
    G4cout << " Interactions will be time sorted in the LM file." << G4endl;
    if (packTimeEThreshold) {
      if (packDist) 
	G4cout << " ----> Energy threshold for times of sorted and packed interactions is set at " 
	       << packTimeEThreshold/keV << " keV." << G4endl;
      else {
	G4cout << " ----> Energy threshold for times of sorted and packed interactions is set at " 
	       << packTimeEThreshold/keV << " keV." << G4endl;
	G4cout << " ----> but won't be used because packing is disabled. " << G4endl;
      }
    }
  } 
}

// mp May 2009   The following two functions are needed for hits time sorting
void AgataEventAction::tagSort(G4int tagArr[], G4double dataArr[], int size)
{
   G4int k, indexOfMin, pass;

   for (k = 0; k < size; k++)
       tagArr[k] = k;

   for (pass = 0; pass < size - 1; pass++)
   {
       indexOfMin = pass;

       for (k = pass + 1; k < size; k++)
           if (dataArr[ tagArr[k] ] < dataArr[ tagArr[indexOfMin] ])
               indexOfMin = k;

       if (indexOfMin != pass)
           swap (tagArr[pass], tagArr[indexOfMin]);
   }
}// end tagSort( )

void AgataEventAction::swap(G4int& x, G4int& y)
{
   G4double temp;
   temp = x;
   x = y;
   y = temp;
}



/////////////////////
/// The Messenger
////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataEventActionMessenger::AgataEventActionMessenger(AgataEventAction* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/file/";
  
  commandName = directoryName + "packingDistance";
  aLine = commandName.c_str();
  SetPackDistCmd = new G4UIcmdWithADouble(aLine, this);
  SetPackDistCmd->SetGuidance("Set packing distance for the LM output file.");
  SetPackDistCmd->SetGuidance("Required parameters: 1 double (packing distance in mm).");
  SetPackDistCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  //mp
  commandName = directoryName + "packTimeEthreshold";
  aLine = commandName.c_str();
  SetPackTimeEThresholdCmd = new G4UIcmdWithADouble(aLine, this);
  SetPackTimeEThresholdCmd->SetGuidance("Set energy threshold for packed time for the LM output file.");
  SetPackTimeEThresholdCmd->SetGuidance("Required parameters: 1 double (threshold in keV).");
  SetPackTimeEThresholdCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  
  commandName = directoryName + "unitLength";
  aLine = commandName.c_str();
  SetUnitLengthCmd = new G4UIcmdWithADouble(aLine, this);
  SetUnitLengthCmd->SetGuidance("Set unit length for the LM output file.");
  SetUnitLengthCmd->SetGuidance("Required parameters: 1 double (unit length in mm).");
  SetUnitLengthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "unitEnergy";
  aLine = commandName.c_str();
  SetUnitEnergyCmd = new G4UIcmdWithADouble(aLine, this);
  SetUnitEnergyCmd->SetGuidance("Set unit energy for the LM output file.");
  SetUnitEnergyCmd->SetGuidance("Required parameters: 1 double (unit energy in keV).");
  SetUnitEnergyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "interval";
  aLine = commandName.c_str();
  SetIntervalCmd = new G4UIcmdWithAnInteger(aLine, this);  
  SetIntervalCmd->SetGuidance("Set interval for event number printout.");
  SetIntervalCmd->SetGuidance("Required parameters: 1 integer (required interval)");
  SetIntervalCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  

  directoryName = name + "/event/";
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of event action.");
  
  commandName = directoryName + "enableHits";
  aLine = commandName.c_str();
  EnableHitsCmd = new G4UIcmdWithABool(aLine, this);
  EnableHitsCmd->SetGuidance("Activate drawing of hits.");
  EnableHitsCmd->SetGuidance("Required parameters: none.");
  EnableHitsCmd->SetParameterName("drawHits",true);
  EnableHitsCmd->SetDefaultValue(true);
  EnableHitsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "disableHits";
  aLine = commandName.c_str();
  DisableHitsCmd = new G4UIcmdWithABool(aLine, this);
  DisableHitsCmd->SetGuidance("Deactivate drawing of hits.");
  DisableHitsCmd->SetGuidance("Required parameters: none.");
  DisableHitsCmd->SetParameterName("drawHits",true);
  DisableHitsCmd->SetDefaultValue(false);
  DisableHitsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enableRays";
  aLine = commandName.c_str();
  EnableRaysCmd = new G4UIcmdWithABool(aLine, this);
  EnableRaysCmd->SetGuidance("Activate drawing of gamma-rays.");
  EnableRaysCmd->SetGuidance("Required parameters: none.");
  EnableRaysCmd->SetParameterName("drawRays",true);
  EnableRaysCmd->SetDefaultValue(true);
  EnableRaysCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "disableRays";
  aLine = commandName.c_str();
  DisableRaysCmd = new G4UIcmdWithABool(aLine, this);
  DisableRaysCmd->SetGuidance("Deactivate drawing of gamma-rays.");
  DisableRaysCmd->SetGuidance("Required parameters: none.");
  DisableRaysCmd->SetParameterName("drawRays",true);
  DisableRaysCmd->SetDefaultValue(false);
  DisableRaysCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters.");
  StatusCmd->SetGuidance("Required parameters: none.");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  

  directoryName = name + "/file/info/";
  myFileDirectory = new G4UIdirectory(directoryName);
  myFileDirectory->SetGuidance("Detailed control of the list-mode verbosity level.");
  
  commandName = directoryName + "outputMask";
  aLine = commandName.c_str();
  SetOutMaskCmd = new G4UIcmdWithAString(aLine, this);
  SetOutMaskCmd->SetGuidance("Configure the information written in the LM output file.");
  SetOutMaskCmd->SetGuidance("Required parameters: a 7-digit string.");
  SetOutMaskCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "trackingData";
  aLine = commandName.c_str();
  TrackDataCmd = new G4UIcmdWithoutParameter(aLine, this);
  TrackDataCmd->SetGuidance("Sets the output mask to the value needed by the TRACKING programs.");
  TrackDataCmd->SetGuidance("Required parameters: none.");
  TrackDataCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "pulseShapeData";
  aLine = commandName.c_str();
  PSADataCmd = new G4UIcmdWithoutParameter(aLine, this);
  PSADataCmd->SetGuidance("Sets the output mask to the value needed by the PULSE SHAPE CALCULATION programs.");
  PSADataCmd->SetGuidance("Required parameters: none.");
  PSADataCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableMgsFormat";
  aLine = commandName.c_str();
  EnableMgsFormatCmd = new G4UIcmdWithABool(aLine, this);
  EnableMgsFormatCmd->SetGuidance("Chooses the mgs coordinate type for the PULSE SHAPE CALCULATION format of the LM file.");
  EnableMgsFormatCmd->SetGuidance("Required parameters: none.");
  EnableMgsFormatCmd->SetParameterName("mgsFormat",true);
  EnableMgsFormatCmd->SetDefaultValue(true);
  EnableMgsFormatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableMgsFormat";
  aLine = commandName.c_str();
  DisableMgsFormatCmd = new G4UIcmdWithABool(aLine, this);
  DisableMgsFormatCmd->SetGuidance("Chooses the GEANT4 coordinate type for the PULSE SHAPE CALCULATION format of the LM file.");
  DisableMgsFormatCmd->SetGuidance("Required parameters: none.");
  DisableMgsFormatCmd->SetParameterName("mgsFormat",true);
  DisableMgsFormatCmd->SetDefaultValue(false);
  DisableMgsFormatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "enableNDet";
  aLine = commandName.c_str();
  EnableNDetCmd = new G4UIcmdWithABool(aLine, this);
  EnableNDetCmd->SetGuidance("Enables the output of the detector number in the LM file.");
  EnableNDetCmd->SetGuidance("Required parameters: none.");
  EnableNDetCmd->SetParameterName("writeData[0]",true);
  EnableNDetCmd->SetDefaultValue(true);
  EnableNDetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableNDet";
  aLine = commandName.c_str();
  DisableNDetCmd = new G4UIcmdWithABool(aLine, this);
  DisableNDetCmd->SetGuidance("Disables the output of the detector number in the LM file.");
  DisableNDetCmd->SetGuidance("Required parameters: none.");
  DisableNDetCmd->SetParameterName("writeData[0]",true);
  DisableNDetCmd->SetDefaultValue(false);
  DisableNDetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enableEner";
  aLine = commandName.c_str();
  EnableEnerCmd = new G4UIcmdWithABool(aLine, this);
  EnableEnerCmd->SetGuidance("Enables the output of the interaction energy in the LM file.");
  EnableEnerCmd->SetGuidance("Required parameters: none.");
  EnableEnerCmd->SetParameterName("writeData[1]",true);
  EnableEnerCmd->SetDefaultValue(true);
  EnableEnerCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableEner";
  aLine = commandName.c_str();
  DisableEnerCmd = new G4UIcmdWithABool(aLine, this);
  DisableEnerCmd->SetGuidance("Disables the output of the interaction energy in the LM file.");
  DisableEnerCmd->SetGuidance("Required parameters: none.");
  DisableEnerCmd->SetParameterName("writeData[1]",true);
  DisableEnerCmd->SetDefaultValue(false);
  DisableEnerCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enablePosA";
  aLine = commandName.c_str();
  EnablePosACmd = new G4UIcmdWithABool(aLine, this);
  EnablePosACmd->SetGuidance("Enables the output of the position of the interaction (absolute) in the LM file.");
  EnablePosACmd->SetGuidance("Required parameters: none.");
  EnablePosACmd->SetParameterName("writeData[2]",true);
  EnablePosACmd->SetDefaultValue(true);
  EnablePosACmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disablePosA";
  aLine = commandName.c_str();
  DisablePosACmd = new G4UIcmdWithABool(aLine, this);
  DisablePosACmd->SetGuidance("Disables the output of the position of the interaction (absolute) in the LM file.");
  DisablePosACmd->SetGuidance("Required parameters: none.");
  DisablePosACmd->SetParameterName("writeData[2]",true);
  DisablePosACmd->SetDefaultValue(false);
  DisablePosACmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enablePosI";
  aLine = commandName.c_str();
  EnablePosICmd = new G4UIcmdWithABool(aLine, this);
  EnablePosICmd->SetGuidance("Enables the output of the position of the interaction (relative to the crystal) in the LM file.");
  EnablePosICmd->SetGuidance("Required parameters: none.");
  EnablePosICmd->SetParameterName("writeData[3]",true);
  EnablePosICmd->SetDefaultValue(true);
  EnablePosICmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disablePosI";
  aLine = commandName.c_str();
  DisablePosICmd = new G4UIcmdWithABool(aLine, this);
  DisablePosICmd->SetGuidance("Disables the output of the position of the interaction (relative to the crystal) in the LM file.");
  DisablePosICmd->SetGuidance("Required parameters: none.");
  DisablePosICmd->SetParameterName("writeData[3]",true);
  DisablePosICmd->SetDefaultValue(false);
  DisablePosICmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enablePosM";
  aLine = commandName.c_str();
  EnablePosMCmd = new G4UIcmdWithABool(aLine, this);
  EnablePosMCmd->SetGuidance("Enables the output of the position of the interaction (relative to the crystal, mgs coordinates) in the LM file.");
  EnablePosMCmd->SetGuidance("Required parameters: none.");
  EnablePosMCmd->SetParameterName("writeData[4]",true);
  EnablePosMCmd->SetDefaultValue(true);
  EnablePosMCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disablePosM";
  aLine = commandName.c_str();
  DisablePosMCmd = new G4UIcmdWithABool(aLine, this);
  DisablePosMCmd->SetGuidance("Disables the output of the position of the interaction (relative to the crystal, mgs coordinates) in the LM file.");
  DisablePosMCmd->SetGuidance("Required parameters: none.");
  DisablePosMCmd->SetParameterName("writeData[4]",true);
  DisablePosMCmd->SetDefaultValue(false);
  DisablePosMCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enableNSeg";
  aLine = commandName.c_str();
  EnableNSegCmd = new G4UIcmdWithABool(aLine, this);
  EnableNSegCmd->SetGuidance("Enables the output of the segment number in the LM file.");
  EnableNSegCmd->SetGuidance("Required parameters: none.");
  EnableNSegCmd->SetParameterName("writeData[5]",true);
  EnableNSegCmd->SetDefaultValue(true);
  EnableNSegCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableNSeg";
  aLine = commandName.c_str();
  DisableNSegCmd = new G4UIcmdWithABool(aLine, this);
  DisableNSegCmd->SetGuidance("Disables the output of the segment number in the LM file.");
  DisableNSegCmd->SetGuidance("Required parameters: none.");
  DisableNSegCmd->SetParameterName("writeData[5]",true);
  DisableNSegCmd->SetDefaultValue(false);
  DisableNSegCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enableTime";
  aLine = commandName.c_str();
  EnableTimeCmd = new G4UIcmdWithABool(aLine, this);
  EnableTimeCmd->SetGuidance("Enables the output of the time of the interaction in the LM file.");
  EnableTimeCmd->SetGuidance("Required parameters: none.");
  EnableTimeCmd->SetParameterName("writeData[6]",true);
  EnableTimeCmd->SetDefaultValue(true);
  EnableTimeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableTime";
  aLine = commandName.c_str();
  DisableTimeCmd = new G4UIcmdWithABool(aLine, this);
  DisableTimeCmd->SetGuidance("Disables the output of the time of the interaction in the LM file.");
  DisableTimeCmd->SetGuidance("Required parameters: none.");
  DisableTimeCmd->SetParameterName("writeData[6]",true);
  DisableTimeCmd->SetDefaultValue(false);
  DisableTimeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "enableInte";
  aLine = commandName.c_str();
  EnableInteCmd = new G4UIcmdWithABool(aLine, this);
  EnableInteCmd->SetGuidance("Enables the output of the interaction type in the LM file.");
  EnableInteCmd->SetGuidance("Required parameters: none.");
  EnableInteCmd->SetParameterName("writeData[7]",true);
  EnableInteCmd->SetDefaultValue(true);
  EnableInteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "disableInte";
  aLine = commandName.c_str();
  DisableInteCmd = new G4UIcmdWithABool(aLine, this);
  DisableInteCmd->SetGuidance("Disables the output of the interaction type in the LM file.");
  DisableInteCmd->SetGuidance("Required parameters: none.");
  DisableInteCmd->SetParameterName("writeData[7]",true);
  DisableInteCmd->SetDefaultValue(false);
  DisableInteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  //mp:
  commandName = directoryName + "disableTimeSorting";
  aLine = commandName.c_str();
  DisableTimeSortingCmd = new G4UIcmdWithABool(aLine, this);
  DisableTimeSortingCmd->SetGuidance("Disables time sorting of the interactions in the LM file.");
  DisableTimeSortingCmd->SetGuidance("Required parameters: none.");
  DisableTimeSortingCmd->SetParameterName("timeSorting",true);
  DisableTimeSortingCmd->SetDefaultValue(false);
  DisableTimeSortingCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "enableTimeSorting";
  aLine = commandName.c_str();
  EnableTimeSortingCmd = new G4UIcmdWithABool(aLine, this);
  EnableTimeSortingCmd->SetGuidance("Enables time sorting of the interactions in the LM file.");
  EnableTimeSortingCmd->SetGuidance("Required parameters: none.");
  EnableTimeSortingCmd->SetParameterName("timeSorting",true);
  EnableTimeSortingCmd->SetDefaultValue(true);
  EnableTimeSortingCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "enableAll";
  aLine = commandName.c_str();
  EnableAllCmd = new G4UIcmdWithABool(aLine, this);
  EnableAllCmd->SetGuidance("Activate LM writing of empty gammas.");
  EnableAllCmd->SetGuidance("Required parameters: none.");
  EnableAllCmd->SetParameterName("writeAll",true);
  EnableAllCmd->SetDefaultValue(true);
  EnableAllCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  commandName = directoryName + "disableAll";
  aLine = commandName.c_str();
  DisableAllCmd = new G4UIcmdWithABool(aLine, this);
  DisableAllCmd->SetGuidance("Deactivate LM writing of empty gammas.");
  DisableAllCmd->SetGuidance("Required parameters: none.");
  DisableAllCmd->SetParameterName("writeAll",true);
  DisableAllCmd->SetDefaultValue(false);
  DisableAllCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  
  
  
}

AgataEventActionMessenger::~AgataEventActionMessenger()
{
  delete myDirectory;
  delete EnableAllCmd;
  delete EnableHitsCmd;
  delete EnableRaysCmd;
  delete DisableAllCmd;
  delete DisableHitsCmd;
  delete DisableRaysCmd;
  delete SetPackDistCmd;
  //mp
  delete SetPackTimeEThresholdCmd;
  delete SetUnitEnergyCmd;
  delete SetUnitLengthCmd;
  delete SetIntervalCmd;
  delete StatusCmd;
  delete myFileDirectory;
  delete SetOutMaskCmd;
  delete EnableMgsFormatCmd;
  delete EnableNDetCmd;
  delete EnableEnerCmd;
  delete EnablePosACmd;
  delete EnableNSegCmd;
  delete EnablePosICmd;
  delete EnablePosMCmd;
  delete EnableTimeCmd;
  delete EnableInteCmd;
  delete DisableMgsFormatCmd;
  delete DisableNDetCmd;
  delete DisableEnerCmd;
  delete DisablePosACmd;
  delete DisableNSegCmd;
  delete DisablePosICmd;
  delete DisablePosMCmd;
  delete DisableTimeCmd;
  delete DisableInteCmd;
  delete TrackDataCmd;
  delete PSADataCmd;
  //mp:
  delete EnableTimeSortingCmd;
  delete DisableTimeSortingCmd;
}

void AgataEventActionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == EnableAllCmd ) {
    myTarget->SetWriteAll( EnableAllCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableAllCmd ) {
    myTarget->SetWriteAll( DisableAllCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetPackDistCmd ) {
    myTarget->SetPackDist( SetPackDistCmd->GetNewDoubleValue(newValue) );
  }
  // mp
  if( command == SetPackTimeEThresholdCmd ) {
    myTarget->SetPackTimeEThreshold( SetPackTimeEThresholdCmd->GetNewDoubleValue(newValue) );
  }
  if( command == SetUnitEnergyCmd ) {
    myTarget->SetUnitEnergy( SetUnitEnergyCmd->GetNewDoubleValue(newValue) );
  }
  if( command == SetUnitLengthCmd ) {
    myTarget->SetUnitLength( SetUnitLengthCmd->GetNewDoubleValue(newValue) );
  }
  if( command == SetIntervalCmd ) {
    myTarget->SetInterval( SetIntervalCmd->GetNewIntValue(newValue) );
  }
  if( command == EnableHitsCmd ) {
    myTarget->SetDrawHits( EnableHitsCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableHitsCmd ) {
    myTarget->SetDrawHits( DisableHitsCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableRaysCmd ) {
    myTarget->SetDrawRays( EnableRaysCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableRaysCmd ) {
    myTarget->SetDrawRays( DisableRaysCmd->GetNewBoolValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus();
  }
  if( command == SetOutMaskCmd ) {
    myTarget->SetOutMask( newValue );
  }
  if( command == TrackDataCmd ) {
    myTarget->DataForTracking();
  }
  if( command == PSADataCmd ) {
    myTarget->DataForPulseShape();
  }
  if( command == EnableMgsFormatCmd ) {
    myTarget->SetEnableMgsFormat( EnableMgsFormatCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableMgsFormatCmd ) {
    myTarget->SetEnableMgsFormat( DisableMgsFormatCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableNDetCmd ) {
    myTarget->SetEnableNDet( EnableNDetCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableNDetCmd ) {
    myTarget->SetEnableNDet( DisableNDetCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableEnerCmd ) {
    myTarget->SetEnableEner( EnableEnerCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableEnerCmd ) {
    myTarget->SetEnableEner( DisableEnerCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnablePosACmd ) {
    myTarget->SetEnablePosA( EnablePosACmd->GetNewBoolValue(newValue) );
  }
  if( command == DisablePosACmd ) {
    myTarget->SetEnablePosA( DisablePosACmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableNSegCmd ) {
    myTarget->SetEnableNSeg( EnableNSegCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableNSegCmd ) {
    myTarget->SetEnableNSeg( DisableNSegCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnablePosICmd ) {
    myTarget->SetEnablePosI( EnablePosICmd->GetNewBoolValue(newValue) );
  }
  if( command == DisablePosICmd ) {
    myTarget->SetEnablePosI( DisablePosICmd->GetNewBoolValue(newValue) );
  }
  if( command == EnablePosMCmd ) {
    myTarget->SetEnablePosM( EnablePosMCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisablePosMCmd ) {
    myTarget->SetEnablePosM( DisablePosMCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableTimeCmd ) {
    myTarget->SetEnableTime( EnableTimeCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableTimeCmd ) {
    myTarget->SetEnableTime( DisableTimeCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableInteCmd ) {
    myTarget->SetEnableInte( EnableInteCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableInteCmd ) {
    myTarget->SetEnableInte( DisableInteCmd->GetNewBoolValue(newValue) );
  }
  //mp:
   if( command == DisableTimeSortingCmd ) {
     myTarget->SetEnableTimeSorting( DisableTimeSortingCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableTimeSortingCmd ) {
    myTarget->SetEnableTimeSorting( EnableTimeSortingCmd->GetNewBoolValue(newValue) );
  }

 
}

