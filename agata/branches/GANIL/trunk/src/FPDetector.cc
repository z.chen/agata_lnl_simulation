// Adapted for Agata code, October 2009 by P. Joshi, The University of York.
#include "FPDetector.h"

#include "G4Box.hh"
#include "G4Polyhedra.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "globals.hh"
#include "G4ios.hh"


 FPDetector::FPDetector(G4double defTgtDetDist) :
 FP_log(0), detectorMaterial(0),  FP_phys(0) 
 {
   //ELOG292 (S377)
   detDist = defTgtDetDist-0.034*m;

   }

 FPDetector::~FPDetector()
 {
}

void FPDetector::createDetector(G4VPhysicalVolume *lab_phy,  G4Material* detMaterial)
{


  G4double fp_z_values[]={0.0*mm, 2.0*mm};

  const G4double fp_rOuter[]={150.0*mm,150.0*mm};
  const G4double fp_rInner[]={0*cm,0*cm};

  G4Polyhedra* FP_box = new G4Polyhedra("FP_box",0*deg, 360*deg,8,2,fp_z_values,fp_rInner,fp_rOuter);

  FP_log = new G4LogicalVolume(FP_box, detMaterial, "FP_log", 0,0,0);

   G4VisAttributes* FPVisATT
                 = new G4VisAttributes(G4Colour(0.0,1.0,0.0));      // Set detector colour for visualisation

  FPVisATT->SetForceSolid(true);     // Solid appearance NOT wireframe

  FP_log->SetVisAttributes(FPVisATT);

  G4RotationMatrix *Ra;
  Ra = new G4RotationMatrix;
//  Ra->rotateX(0.0*deg);
//  Ra->rotateY(90.0*deg);
  Ra->rotateZ(45.0*deg);

  placement[0] = G4ThreeVector(0,   0,  detDist);
  char phys_name[20];
  sprintf(phys_name, "FP_Segment%d", 1);

    FP_phys_cpy[0] = new G4PVPlacement(Ra,
                       placement[0],
                       phys_name, FP_log, lab_phy, false, 201);
G4cout << "Lycca Placement for FP (Fast Plastic) Segment# " << "201" <<"=" << placement[0] << G4endl;

}
