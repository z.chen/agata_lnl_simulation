#ifdef ANCIL
#include "AgataAncillaryHC.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"




AgataAncillaryHC::AgataAncillaryHC(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  dirName     = name;

  
  matShell    = NULL;
  matName     = "Aluminum";


  ancSD       = NULL;
  ancName     = G4String("HC");
  ancOffset   = 26000;
  
  numAncSd = 0;
}

AgataAncillaryHC::~AgataAncillaryHC()
{}

G4int AgataAncillaryHC::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryHC::InitSensitiveDetector()
{}


void AgataAncillaryHC::GetDetectorConstruction()
{  
	
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
  
}


void AgataAncillaryHC::Placement()
{	
  //    m_gdmlparser.Read("gdml/HoneyComb.gdml");
    m_gdmlparser.Read("/data2/joa/agataganil/gdml-files/AGATA/HoneyComb/HoneyComb.gdml");
    m_LogicalDetector= m_gdmlparser.GetVolume("Structure_200195488");

    G4RotationMatrix* rm= new G4RotationMatrix();
    G4RotationMatrix rmY, rmZ;
	rmZ.rotateZ(0.*deg);
	rmY.rotateY(0.*deg);
   
	*rm=rmY*rmZ;

	//G4Transform3D TF(rm, rm*G4ThreeVector(0., 0., 0.));

    // gdml World box
    //m_LogicalDetector->SetVisAttributes(G4VisAttributes::Invisible); 
    G4VisAttributes *AttVol2 = new G4VisAttributes(G4Colour(1.0, 1.0, 0.0)); 
    AttVol2->SetForceSolid(true);
    m_LogicalDetector->SetVisAttributes(AttVol2);
    
    new G4PVPlacement(rm, G4ThreeVector(0., 0., 0.), "HoneyComb", m_LogicalDetector, theDetector->HallPhys(), false, 0 );

	return ;
  
}


void AgataAncillaryHC::ShowStatus()
{
  G4cout << " HoneyComb support has been constructed." << G4endl;
}

void AgataAncillaryHC::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
