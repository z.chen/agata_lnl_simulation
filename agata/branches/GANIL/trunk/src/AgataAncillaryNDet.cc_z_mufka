 #ifdef ANCIL
 // this code was created by grzegorz jaworski -> tatrofil@slcj.uw.edu.pl
 #include "AgataAncillaryNDet.hh"
 #include "AgataDetectorAncillary.hh"
 #include "AgataDetectorConstruction.hh"
 #include "AgataSensitiveDetector.hh"

 #include "G4Material.hh"
 #include "G4Tubs.hh"
 #include "G4SubtractionSolid.hh"
 #include "G4LogicalVolume.hh"
 #include "G4ThreeVector.hh"
 #include "G4PVPlacement.hh"
 #include "globals.hh"
 #include "G4Transform3D.hh"
 #include "G4RotationMatrix.hh"
 #include "G4SDManager.hh"
 #include "G4VisAttributes.hh"
 #include "G4Colour.hh"
 #include "G4RunManager.hh"
 #include "G4ios.hh"

 using namespace std;

AgataAncillaryNDet::AgataAncillaryNDet(G4String path, G4String name)
{
  G4String iniPath     = path;
  dirName = name;
  
  useAlCans = true;
  
  distanceToTheTarget =  51.   * cm;
  cylinderDiameter    =  16.5  * cm; 
  cylinderHight       =  15.   * cm;
  aluminiumThickness  =   2.   * mm;
  
  theSmallDistance      =   1.   * nm;
  
  rmZero.set(0,0,0);
  
  nameAl     = "Aluminium";
  matAl      =  NULL;
  //nameScint  = "matCarbon";
  //nameScint  = "matDeuter";
  //  nameScint  = "matHydrogen";
  nameScint  = "BC501A";
  //nameScint  = "BC537";
  Scint      =  NULL;
  
  ancSD = NULL;
  
  ancName   = G4String("NDet");
  ancOffset = 66000;
  
  numAncSd = 0;
  
  myMessenger = new AgataAncillaryNDetMessenger(this,name);
}

AgataAncillaryNDet::~AgataAncillaryNDet()
{
  delete myMessenger;
}


G4int AgataAncillaryNDet::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(nameAl);
  if (ptMaterial){
    matAl = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
	   << matAl->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameAl << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  ptMaterial = G4Material::GetMaterial(nameScint);
  if (ptMaterial){
    Scint = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
	   << Scint->GetName() << G4endl;
  }
  else{
    G4cout << " Could not find the material " << nameScint << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }   
  return 0;
}


void AgataAncillaryNDet::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

void AgataAncillaryNDet::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryNDet::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName,"/anc/NDet", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

void AgataAncillaryNDet::PlaceCylinders(){
  G4Tubs *cylinder = new G4Tubs("cylinder",0.,.5*cylinderDiameter,.5*cylinderHight,0.,360.*deg);
  G4LogicalVolume *cylinder_d_l = new G4LogicalVolume(cylinder, Scint, "cylindrical scintillator", 0,0,0);
  cylinder_d_l->SetSensitiveDetector(ancSD);
  if(!useAlCans)aluminiumThickness=0.;
  G4double zet=distanceToTheTarget+.5*cylinderHight+aluminiumThickness;
  
  G4VPhysicalVolume *cylinder_d_ph; 
  cylinder_d_ph 
    = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,zet)),"cylindrical scintillatorA",
			cylinder_d_l,theDetector->HallPhys(), false, 2);
  
  //outer cylinder:
  G4double outer_cilnder_thickness = 1. *m;
  G4Tubs *outer_cylinder_t = new G4Tubs("outer cylinder",
					.5*cylinderDiameter+aluminiumThickness+2.*theSmallDistance,
					.5*cylinderDiameter+aluminiumThickness+2.*theSmallDistance+outer_cilnder_thickness,
					.5*cylinderHight,0.,360.*deg);
  G4LogicalVolume *outer_cylinder_l = new G4LogicalVolume(outer_cylinder_t, Scint, "outer cylinder", 0,0,0);  
  outer_cylinder_l->SetSensitiveDetector(ancSD);
//  G4VPhysicalVolume *outer_cylinder;
//  outer_cylinder =  new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,zet)),"outer cylinder",
//				      outer_cylinder_l,theDetector->HallPhys(), false, 66);
//  

  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));//zielony
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  zielony->SetVisibility(true);czerwony->SetVisibility(true);
  cylinder_d_l->SetVisAttributes(czerwony);
  outer_cylinder_l->SetVisAttributes(zielony);
}

void AgataAncillaryNDet::PlaceAlCans(){
  G4Tubs *cylinder = new G4Tubs("",0.,.5*cylinderDiameter+theSmallDistance,.5*cylinderHight+theSmallDistance,0.,360.*deg);
  G4Tubs *AlCylinder = new G4Tubs("",0,.5*cylinderDiameter+theSmallDistance+aluminiumThickness,
				  .5*cylinderHight+theSmallDistance+aluminiumThickness,0.,360.*deg);
  G4SubtractionSolid* AlCan_sub = new G4SubtractionSolid("al can",AlCylinder,cylinder,0,G4ThreeVector(0,0,0));
  G4LogicalVolume *AlCan_log = new G4LogicalVolume(AlCan_sub, matAl,"al can");
  G4double zet = distanceToTheTarget+.5*cylinderHight+aluminiumThickness;
  G4VPhysicalVolume *AlCan_ph; 
  AlCan_ph = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,zet)),
			       "al can",AlCan_log,theDetector->HallPhys(), false, 0);
  //puszka dla deuterowanego
//  G4VPhysicalVolume *AlCan_d_ph; 
//  AlCan_d_ph = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0.,0.,-zet)),
//  				 "al can",AlCan_log,theDetector->HallPhys(), false, 0);
}



G4int AgataAncillaryNDet::GetSegmentNumber(G4int offset, G4int detCode, G4ThreeVector position)
{
  //just to avoid warnings:
  offset=detCode;position=G4ThreeVector(0,0,0);return 0;
}

void AgataAncillaryNDet::Placement()
{
  PlaceCylinders();
  if(useAlCans)
    PlaceAlCans();
}


void AgataAncillaryNDet::ShowStatus(){}

void AgataAncillaryNDet::SetUseAlCans( G4bool value )
{
  useAlCans = value;
  if( useAlCans )
    G4cout << " ----> Aluminium cans of scintillator will be used." << G4endl;
  else  
    G4cout << " ----> Aluminium cans of scintillator wont be generated." << G4endl;
}

void AgataAncillaryNDet::SetDistanceToTheTarget(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << distanceToTheTarget/cm << " cm)" << G4endl;
  else
    distanceToTheTarget = length;
}

void AgataAncillaryNDet::SetCylinderDiameter(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << cylinderDiameter/cm << " cm)" << G4endl;
  else
    cylinderDiameter = length;
}

void AgataAncillaryNDet::SetCylinderHight(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << cylinderHight/cm << " cm)" << G4endl;
  else
    cylinderHight = length;
}

void AgataAncillaryNDet::SetAluminiumThickness(G4double length)
{
  if(length < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << aluminiumThickness/cm << " cm)" << G4endl;
  else
    aluminiumThickness = length;
}


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillaryNDetMessenger::AgataAncillaryNDetMessenger(AgataAncillaryNDet* pTarget, G4String name)
  :myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  

  directoryName = name + "/detector/ancillary/NDet/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of NDet construction.");  

  //al cans
  commandName = directoryName + "enableAlCans";
  aLine = commandName.c_str(); 
  enableAlCans = new G4UIcmdWithABool(commandName, this);
  enableAlCans->SetGuidance("Creating aluminium cans for scintillators.");
  enableAlCans->SetGuidance("Required parameters: none.");
  enableAlCans->SetParameterName("useAlCans",true);
  enableAlCans->SetDefaultValue(true);
  enableAlCans->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "disableAlCans";
  aLine = commandName.c_str(); 
  disableAlCans = new G4UIcmdWithABool(commandName, this);
  disableAlCans->SetGuidance("Aluminium cans for scintillators wont be generated.");
  disableAlCans->SetGuidance("Required parameters: none.");
  disableAlCans->SetParameterName("useAbsorbers",true);
  disableAlCans->SetDefaultValue(false);
  disableAlCans->AvailableForStates(G4State_PreInit,G4State_Idle);

  //changeable parameters
  commandName = directoryName + "DistanceToTheTarget";
  aLine = commandName.c_str(); 
  DistanceToTheTarget =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  DistanceToTheTarget -> SetGuidance("Sets distance between target and detector's face.");
  DistanceToTheTarget -> SetParameterName("Size",false);
  DistanceToTheTarget -> SetRange("Size>0.");
  DistanceToTheTarget -> SetUnitCategory("Length");
  DistanceToTheTarget -> AvailableForStates(G4State_Idle);
  
  commandName = directoryName + "CylinderDiameter";
  aLine = commandName.c_str(); 
  CylinderDiameter =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  CylinderDiameter -> SetGuidance("Sets diameter of cylindrical scintillation detector.");
  CylinderDiameter -> SetParameterName("Size",false);
  CylinderDiameter -> SetRange("Size>0.");
  CylinderDiameter -> SetUnitCategory("Length");
  CylinderDiameter -> AvailableForStates(G4State_Idle);
  
  commandName = directoryName + "CylinderHight";
  aLine = commandName.c_str(); 
  CylinderHight =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  CylinderHight -> SetGuidance("Sets thickness of cylindrical scintillation detector.");
  CylinderHight -> SetParameterName("Size",false);
  CylinderHight -> SetRange("Size>0.");
  CylinderHight -> SetUnitCategory("Length");
  CylinderHight -> AvailableForStates(G4State_Idle);
  
  commandName = directoryName + "AluminiumThickness";
  aLine = commandName.c_str(); 
  AluminiumThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  AluminiumThickness -> SetGuidance("Sets thickness of aluminium cans.");
  AluminiumThickness -> SetParameterName("Size",false);
  AluminiumThickness -> SetRange("Size>0.");
  AluminiumThickness -> SetUnitCategory("Length");
  AluminiumThickness -> AvailableForStates(G4State_Idle);
}

AgataAncillaryNDetMessenger::~AgataAncillaryNDetMessenger()
{
  delete  enableAlCans;
  delete disableAlCans;
  delete DistanceToTheTarget;
  delete CylinderDiameter; 
  delete CylinderHight;
  delete AluminiumThickness;
}

void AgataAncillaryNDetMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == enableAlCans ) 
    myTarget->SetUseAlCans( enableAlCans->GetNewBoolValue(newValue) );
  if( command == disableAlCans ) 
    myTarget->SetUseAlCans( disableAlCans->GetNewBoolValue(newValue) );
  if( command == DistanceToTheTarget )
    myTarget->SetDistanceToTheTarget(DistanceToTheTarget->GetNewDoubleValue(newValue));
  if( command == CylinderDiameter )
    myTarget->SetCylinderDiameter(CylinderDiameter->GetNewDoubleValue(newValue));
  if( command == CylinderHight )
    myTarget->SetCylinderHight(CylinderHight->GetNewDoubleValue(newValue));
  if( command == AluminiumThickness )
    myTarget->SetAluminiumThickness(AluminiumThickness->GetNewDoubleValue(newValue));
}

#endif
