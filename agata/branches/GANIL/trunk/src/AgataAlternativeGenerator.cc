#include "AgataAlternativeGenerator.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include "G4RunManager.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataEventAction.hh"
#include "AgataSteppingPrisma.hh"
#include "AgataPhysicsList.hh"
#include "G4NuclideTable.hh"

AgataAlternativeGenerator::AgataAlternativeGenerator( G4String name,G4int ob ) 
{
  whichoutgoingbeam = ob;
  thesources.clear();
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  tarFraction = 1.0;
  BeamIn = new Incoming_Beam(name);
  switch(whichoutgoingbeam){
  case 0:    
    BeamOut = new Outgoing_Beam(name);
    break;
  case 1:
    BeamOut = dynamic_cast<Outgoing_Beam*>(new Outgoing_Beam_TargetEx(name));
    break;
  default:
    BeamOut = new Outgoing_Beam(name);
    break;
  }
  BeamOut->defaultIncomingIon(BeamIn);
  G4RunManager * runManager = G4RunManager::GetRunManager();
  ((AgataPhysicsList*)runManager->GetUserPhysicsList())->
    SetOutgoingBeam(BeamOut);
  ((AgataPhysicsList*)runManager->GetUserPhysicsList())->
    SetIncomingBeam(BeamIn);
  myMessenger = new AgataAlternativeGeneratorMessenger(this,name);
  profileDistFileName = G4String("profile.dat");
  theProfileDist = new ProfileDistHandler( profileDistFileName );
  fixDepth=false;
  isUnifDistrib=true; 
  //as default it takes an uniform 
  //distribution of target nuclei in the target volume
  depth=0.;
  nbextragammasbkg=0;
  SlopeExpBkg=1;
  MaxEBkg = 1e24;
  eventTime=0*s;
  min=0;
  hours=0;
  days=0;
  pps=0./s;
  HF=0./s;
  WidthBeamBunch=0*s;
  particlesperbunch=0;
  HFtime=0;
  Bunchtime=0;
  TrackIonEnergy=false;
  RecoilFile="";
  G4NuclideTable::GetInstance()->SetThresholdOfHalfLife(0.001*picosecond);
}

AgataAlternativeGenerator::~AgataAlternativeGenerator()
{
  delete particleGun;
  if( theProfileDist )
    delete theProfileDist;
}

void AgataAlternativeGenerator::SetIonRecoilFile(G4String file){
  std::ifstream inputfile(file.data());
  std::istringstream is;
  std::string oneline;
  onelinefromfile afileline;
  RecoilData.clear();
  if(inputfile.good()){
    RecoilFile=file;
    while(inputfile.good()){
      std::getline(inputfile,oneline);
      is.clear();
      is.str(oneline);
      if(is >> afileline.v >> afileline.vx >> afileline.vy >> afileline.vz){
	RecoilData.push_back(afileline);
      }
    }
  } else {
    G4cout << "Could not open file " << file << ". Ignoring command\n";
  }
}

void AgataAlternativeGenerator::BeginOfRun()
{
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) 
    runManager->GetUserDetectorConstruction();
  theEvent     = (AgataEventAction*)          
    runManager->GetUserEventAction();
  theStepping  = (AgataSteppingPrisma*)       
    runManager->GetUserSteppingAction();
  /////////////////////////////////////////
  /// Only if target is actually built  ///
  /// "reaction" makes sense            ///
  /////////////////////////////////////////     
  builtTarget   = theDetector -> IsTarget();
  builtDegrader = theDetector -> IsDegrader();
  builtProductionTarget = theDetector->IsProductionTarget();
  if( builtTarget ) {
    BeamOut->SetReactionOn();
    BeamOut->setDecayProperties();
    if(RecoilFile!=""){
      BeamOut->SetRecoilData(&RecoilData);
    }
  }
  targetZThick   = theDetector->GetTargetSize().z();
  degraderZThick = theDetector->GetDegraderSize().z();
  normal2Target  = theDetector->GetNormal2Target();
  theProfileDist->ReNew( profileDistFileName );
  eventTime=0*s;
  HFtime=0*s;
  Bunchtime=0*s;
  // Are we resuming a simulation we have to re-set these parameters to 
  // their correct values
  if(AgataDistributedParameters::resume){
    G4String infofilename=AgataDistributedParameters::resume_dir+"/"+
      AgataDistributedParameters::eventinfo_file;
    std::ifstream infofile(infofilename.c_str());
    std::string aline;
    std::getline(infofile,aline);
    std::getline(infofile,aline);
    std::istringstream ais(aline);
    ais >> days >> hours >> min >> oldeventTime >> HFtime >> Bunchtime 
	>> pps >> HF >> WidthBeamBunch >> particlesperbunch;
  }
  // Here we should make first decay time estimates...
  std::vector<asource>::iterator itsources=thesources.begin();
  for(; itsources!=thesources.end(); ++itsources){
    itsources->decay_at_time=CLHEP::RandExponential::
      shoot(1./(1e3*itsources->activity)*s);
  }
}

bool SortSources(const asource &s1,const asource &s2){
  return s1.activity>s2.activity;
}

double asource::Legendre(int l,double x)

//This is slow since I have to calculate all orders up to l,
//but only used when setting up source, so that is ok
{
  if(l==0) return 1.;
  if(l==1) return x;
  double pn=x;
  double pnm=1;
  double pnp=0;
  for(int n=1; n<l; n++){
    pnp=((2*n+1)*x*pn-n*pnm)/(n+1);
    pnm=pn;
    pn=pnp;
  }
  return pnp;
}


//Helper functions to make sure we use the units we claim in
//source input files
//......
void makecm(G4double &val)
{
  val*=cm;
}

void makekeV(G4double &val)
{
  val*=keV;
}
//...........

bool asource::ReadDataFile()

{
  std::string filename=source+".g4srcdata";
  std::ifstream input(filename.c_str());
  if(input.good()){
    bool GotGeometry=false;
    std::istringstream inputline;
    std::string aline;
    DecayParticle oneparticle;
    DecayBranch onebranch;
    G4double prob;
    G4String particle,tmp;
    std::vector<G4double> as;
    while(input.good() && !input.eof()){
      //First find geom of source
      if(!GotGeometry){
	do{
	  std::getline(input,aline);
	  if(aline[0]=='#' || aline.size()==0) continue;
	  inputline.clear();
	  inputline.str(aline);
	  if(inputline >> tmp){;} 
	  else {
	    std::cout << "Format error in " << filename << std::endl;
	    return false;
	  }
	}while(!(tmp=="Point" || tmp=="Line" 
		 || tmp=="Plane" || tmp=="Sphere")
	       && input.good()); 
	if(!input.good()) {
	  std::cout << "Format error in " << filename 
		    << " did not find geometry discription\n";
	  return false;
	}
	if(tmp=="Point"){
	  sourcegeometry=Point;
	  GeometryParameters.resize(3);
	  if(inputline >> GeometryParameters[0] >>  GeometryParameters[1] >>
	     GeometryParameters[2]){}
	  else {
	    std::cout << "Format error in " << filename << std::endl;
	    return false;
	  }
	}
	if(tmp=="Line"){
	  sourcegeometry=Line;
	  GeometryParameters.resize(6);
	  if(inputline >> GeometryParameters[0] >>  GeometryParameters[1]
	     >> GeometryParameters[2] >> GeometryParameters[3] 
	     >> GeometryParameters[4] >> GeometryParameters[5] ){}
	  else {
	    std::cout << "Format error in " << filename << std::endl;
	    return false;
	  }
	}
	if(tmp=="Plane"){
	  sourcegeometry=Plane;
	  GeometryParameters.resize(9);
	  if(inputline >> GeometryParameters[0] >>  GeometryParameters[1]
	     >> GeometryParameters[2] >> GeometryParameters[3] 
	     >> GeometryParameters[4] >> GeometryParameters[5]
	     >> GeometryParameters[6] >> GeometryParameters[7] 
	     >> GeometryParameters[8] ){}
	  else {
	    std::cout << "Format error in " << filename << std::endl;
	    return false;
	  }
	}
	if(tmp=="Sphere"){
	  sourcegeometry=Sphere;
	  GeometryParameters.resize(5);
	  if(inputline >> GeometryParameters[0] >>  GeometryParameters[1]
	     >> GeometryParameters[2] >> GeometryParameters[3] 
	     >> GeometryParameters[4]){}
	  else {
	    std::cout << "Format error in " << filename << std::endl;
	    return false;
	  }
	}
	//Make sure the units are correct
	std::for_each(GeometryParameters.begin(),GeometryParameters.end(),
		      makecm);
	GotGeometry=true;
      }
      //Here we should look for a branch
      prob=-1;
      do{
	std::getline(input,aline);
	if(aline[0]=='#' || aline.size()==0) continue;
	inputline.clear();
	inputline.str(aline);
	if(inputline >> prob && !inputline.good()){;}//Only the prob to read 
	else {
	  std::cout << "Format error in " << filename << std::endl;
	  return false;
	}
      }while(prob<0 && input.good());
      if(prob>0){//We got a valid branch, read it in
	DecayBranch adb;
	adb.particles.clear();
	DecayParticle aparticle;
	//G4String tmp;  // tmp already define line 193
	adb.prob=prob;
	std::getline(input,aline);
	inputline.clear();
	inputline.str(aline);
	while(aline.size()>0 && input.good()){//Here we decode the particles
	  aparticle.Integral.clear();
	  aparticle.As.clear();
	  if(inputline >> aparticle.particle
	     >> aparticle.tau >> tmp){
	    if(tmp=="Discrete") aparticle.EnergyDist=Discrete;
	    if(tmp=="Exp") aparticle.EnergyDist=Exp;
	  } else {
	  std::cout << "Format error in " << filename << std::endl;
	  std::cout << "while reading branch " << DecayBranches.size()+1
		    << std::endl;
	  return false;
	  }
	  //Set the lifetime to correct unit
	  aparticle.tau*=s;
	  //Here we read the energy dist par
	  switch(aparticle.EnergyDist){
	  case Discrete:{
	    aparticle.EnergyPar.resize(1);
	    if(inputline>>aparticle.EnergyPar[0]){} else {
	      std::cout << "Format error in " << filename << std::endl;
	      std::cout << "while reading branch " << DecayBranches.size()+1
			<< "No energy dist parameters"
 			<< std::endl;
	      return false;
	    }
	    break;
	  }
	  case Exp: {
	    aparticle.EnergyPar.resize(1);
	    if(inputline>>aparticle.EnergyPar[0]){} else {
	      std::cout << "Format error in " << filename << std::endl;
	      std::cout << "while reading branch " << DecayBranches.size()+1
			<< "No energy dist parameters"
 			<< std::endl;
	      return false;
	    }
	    break;
	  }
	  }
	  //Make sure the units are correct
	  std::for_each(aparticle.EnergyPar.begin(),aparticle.EnergyPar.end(),
			makekeV);
	  //Here we read ang. corr. coeff
	  G4double tmpas;
	  while(inputline>>tmpas) aparticle.As.push_back(tmpas);
	  // Here we will integrate the ang. dist. times sin(theta)
	  // for later use when creating particles
	  aparticle.Integral.push_back(0);
 	  for(int ang=1; ang<=180; ang++){
	    G4double kernel1=0,kernel2=0;
	    for(unsigned int l=0; l<aparticle.As.size(); l++){
	      kernel1+=aparticle.As[l]*Legendre(l,cos((ang-1)*deg));
	    }
	    for(unsigned int l=0; l<aparticle.As.size(); l++){
	      kernel2+=aparticle.As[l]*Legendre(l,cos(ang*deg));
	    }
	    if(kernel1==0) kernel1=1.;
	    if(kernel2==0) kernel2=1.;
	    kernel1*=sin((ang-1)*deg);
	    kernel2*=sin(ang*deg);
	    double val=deg*(kernel2+kernel1)/2.+aparticle.Integral.back(); 
	    aparticle.Integral.push_back(val);
	  }
	  double I=aparticle.Integral.back();
	  for(int ang=0; ang<=180; ang++){
	    aparticle.Integral[ang]/=I;
	  }
	  adb.particles.push_back(aparticle);
	  //New particle in branch
	  std::getline(input,aline);
	  inputline.clear();
	  inputline.str(aline);
	}
	DecayBranches.push_back(adb);
      }
      if(prob<0 && DecayBranches.size()==0){
	  std::cout << "Format error in " << filename << std::endl;
	  std::cout << "Did not find valid decay branch\n";
	  return false;
      }
    }
    return true;
  } else return false;
}

void asource::CreateParticles(G4Event* anEvent,G4ParticleGun *particleGun)
//			      ,   const G4double &eventTime)

{
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
//#ifdef G4V10
  //G4IonTable* ionTable;
  //ionTable = G4IonTable::GetIonTable();
//#endif
  //select position
  G4ThreeVector Pos;
  switch(sourcegeometry){
  case Point:{
    Pos[0]=GeometryParameters[0];
    Pos[1]=GeometryParameters[1];
    Pos[2]=GeometryParameters[2];
    break;
  }
  case Line:{
    G4double u=G4UniformRand();
    Pos[0]=GeometryParameters[0]+
      (GeometryParameters[3]-GeometryParameters[0])*u;
    Pos[1]=GeometryParameters[1]+
      (GeometryParameters[4]-GeometryParameters[1])*u;
    Pos[2]=GeometryParameters[2]+
      (GeometryParameters[5]-GeometryParameters[2])*u;
    break;
  }
  case Plane:{
    G4double u=G4UniformRand();
    G4double v=G4UniformRand();
    Pos[0]=GeometryParameters[0]+
      (GeometryParameters[3]-GeometryParameters[0])*u+
      (GeometryParameters[6]-GeometryParameters[0])*v;
    Pos[1]=GeometryParameters[1]+
      (GeometryParameters[4]-GeometryParameters[1])*u+
      (GeometryParameters[7]-GeometryParameters[0])*v;
    Pos[2]=GeometryParameters[2]+
      (GeometryParameters[5]-GeometryParameters[2])*u+
      (GeometryParameters[8]-GeometryParameters[0])*v;
    break;
  }
  case Sphere:{
    //Angles
    G4double costheta = 2.0 * G4UniformRand() - 1.0;
    G4double sintheta = sqrt( 1 - costheta * costheta );
    G4double phi      = 360. * G4UniformRand() * deg; 
    //R
    G4double r2c=pow(GeometryParameters[4],3);
    G4double r1c=pow(GeometryParameters[3],3);
    G4double r=pow(G4UniformRand()*(r2c-r1c)+r1c,1./3);
    Pos.set(GeometryParameters[0]+r*sintheta*cos(phi), 
	    GeometryParameters[1]+r*sintheta*sin(phi), 
	    GeometryParameters[2]+r*costheta); 
   break;
  }
  }
  //select decay branch
  G4double whichb=G4UniformRand();
  G4double accumulated=0;
  unsigned int branch=0;
  for(; branch<DecayBranches.size(); branch++){
    accumulated+=DecayBranches[branch].prob;
    if(accumulated>whichb) break;
  }
  //Position same for all
  particleGun->SetParticlePosition(Pos);
  //First particle have direction relative to x-axis
  //The source definition file decides if this is isotropic or not
  G4ThreeVector direction(0,0,1);
  //Here we run through particles to emit
  std::vector<DecayParticle>::iterator itpar=
    DecayBranches[branch].particles.begin();
  for(; itpar!=DecayBranches[branch].particles.end(); ++itpar){
    //First set the particle
    particleGun->SetParticleDefinition(particleTable->
				       FindParticle(itpar->particle));
    //Then the energy
    switch(itpar->EnergyDist){
    case Discrete:{
      particleGun->SetParticleEnergy(itpar->EnergyPar[0]);
      break;
    }
    case Exp: {
      particleGun->
	SetParticleEnergy(CLHEP::RandExponential::shoot(itpar->EnergyPar[0]));
      break;
    }
    }
    //Here we should pick the direction of emitted particle
    G4double phi=360.*G4UniformRand()*deg; 
    G4double theta=G4UniformRand();
    //This works because vector is sorted (integration of positive function)
    //what we do is that we get the angle bin whose integral is larger
    //than the theta [0,1] from above
    std::vector<double>::iterator ittheta=
      std::lower_bound(itpar->Integral.begin(),itpar->Integral.end(),theta);
    //Then we smear the actual value within the angle bin... and get the
    //unit right
    //    theta=((ittheta-itpar->Integral.begin())-theta)*deg;
    theta=((ittheta-itpar->Integral.begin())-G4UniformRand())*deg;
    //Now we rotate the direction vector so that z-hat points into the picked 
    //direction, our first vector is z-hat, hence the new vector is the new
    //direction. Since we reuse it this way we get the correlations in there.
    G4ThreeVector ort = direction.cross(G4ThreeVector(1,0,0));
    if(ort.mag2()==0){
      ort = direction.cross(G4ThreeVector(0,1,0));
    }
    G4ThreeVector tmp = direction;tmp.rotate(theta,ort);
    tmp.rotate(phi,direction);
    direction=tmp;
    particleGun->SetParticleMomentumDirection(direction);
    //Here we make sure the time is correct
    //This works if we have >> 60bq in the source, if not
    //we get issues with the fact that eventTime only covers
    //60 s. Maybe it should be replaced with a proper
    //eventTime class???
    //But first, if the state has a lifetime, add it
    G4double statelifetime = itpar->tau>0 ? 
      CLHEP::RandExponential::shoot(itpar->tau) : 0;
    particleGun->SetParticleTime(decay_at_time+statelifetime);
    //And generates the particle
    particleGun->GeneratePrimaryVertex(anEvent);
  }
}

void AgataAlternativeGenerator::GeneratePrimaries(G4Event* anEvent)
{ 
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ThreeVector position = G4ThreeVector(0,0,0);
  G4ThreeVector direction;
  G4double      energy=0;
  //Here we make bkg gammas, first of all so we can reuse stuff
  if(nbextragammasbkg>0 || fDiscreteExtraGammas.size()>0){
    //G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable(); 
    G4String particleName = "gamma";
    particleGun->SetParticleDefinition(particleTable->
				       FindParticle(particleName));
  }
  double A = exp(-SlopeExpBkg*MaxEBkg);
  for(int i=0; i<nbextragammasbkg; i++){
    double r = A+(1-A)*G4UniformRand();
    while(r<=0) r = A+(1-A)*G4UniformRand();
    energy   = -1/SlopeExpBkg*log(r);
    position = G4ThreeVector();
    if(energy>MaxEBkg) std::cout << energy << " " << MaxEBkg << std::endl;
    G4double costheta = 2.0 * G4UniformRand() - 1.0;
    G4double sintheta = sqrt( 1 - costheta * costheta );
    G4double phi      = 360. * G4UniformRand() * deg;
    direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );
    particleGun->SetParticlePosition(position);
    particleGun->SetParticleMomentumDirection(direction);
    particleGun->SetParticleEnergy(energy);
    particleGun->GeneratePrimaryVertex(anEvent);
  }
  std::vector<std::pair<G4double,G4double> >::iterator 
    iteg = fDiscreteExtraGammas.begin();
  for(;iteg!=fDiscreteExtraGammas.end(); ++iteg){
    int nbgammas = 
      ((int)floor(iteg->second)+
       (G4UniformRand()<(iteg->second-floor(iteg->second)) ? 1:0));
    for(int i=0; i<nbgammas; i++){
      energy   = iteg->first;
      position = G4ThreeVector();
      G4double costheta = 2.0 * G4UniformRand() - 1.0;
      G4double sintheta = sqrt( 1 - costheta * costheta );
      G4double phi      = 360. * G4UniformRand() * deg;
      direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );
      particleGun->SetParticlePosition(position);
      particleGun->SetParticleMomentumDirection(direction);
      particleGun->SetParticleEnergy(energy);
      particleGun->GeneratePrimaryVertex(anEvent);
    }
  }
  if(builtTarget) {
#ifdef G4V10
    G4ParticleDefinition* ion = 
      ionTable->GetIon(BeamIn->getZ(),BeamIn->getA(),BeamIn->getEx());
#else
    G4ParticleDefinition* ion = 
      particleTable->GetIon(BeamIn->getZ(),BeamIn->getA(),BeamIn->getEx());
#endif
    if(BeamIn->getEx()){//Excited ions are not stable
      particleGun->GetParticleDefinition()->SetPDGStable(false);
    }
    particleGun->SetParticleDefinition(ion);
    position  = BeamIn->getPosition();
    direction = BeamIn->getDirection();
    energy    = BeamIn->getKE(ion);
    BeamOut->SetReactionFlag(-1);
    BeamOut->SetReactionOn();//It sets reaction off after it does it once ...
    if( isUnifDistrib )  
      depth = (1.0*G4UniformRand()-0.5)*targetZThick;
    else if( !(fixDepth) ) 
      depth = theProfileDist->GetDepth()-0.5*targetZThick; 
    theDetector->SetTargetReactionDepth(depth);
  } else if(thesources.size()==0){
    //G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4String particleName = "gamma";
    particleGun->SetParticleDefinition(particleTable->
				       FindParticle(particleName));
    energy   = 1.0*MeV;
    position = G4ThreeVector();
    G4double costheta = 2.0 * G4UniformRand() - 1.0;
    G4double sintheta = sqrt( 1 - costheta * costheta );
    G4double phi      = 360. * G4UniformRand() * deg;
    direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );
  }
  ///////////////////////////////////////////////
  // finally, call particle gun 
  ///////////////////////////////////////////////////
  if(builtTarget || thesources.size()==0){
    particleGun->SetParticlePosition(position);
    particleGun->SetParticleMomentumDirection(direction);
    particleGun->SetParticleEnergy(energy);
    particleGun->GeneratePrimaryVertex(anEvent);
  }
  while(eventTime>=60.*s){
    eventTime-=60.*s;
    min++;
  }
  while(min>=60){
    min-=60;
    hours++;
  }
  while(hours>=24){
    hours-=24;
    days++;
  }
  oldeventTime = eventTime;
  if(pps>0){
    // Here update event time if beam I  not 0
    if(HF<=0){
      //if no structure just add 1/pps seconds
      eventTime+=1./pps*s;
    } else {
      //Otherwise we have to be smart. 
      //Number of particles per bunch can be <1 or >1
      //so the amount of time that has to be added is not always the same
      //First find how many HF we should jump forward
      G4double meanHF=1./particlesperbunch;
      G4int nHFtoJump=CLHEP::RandPoisson::shoot(meanHF);
      if(nHFtoJump>0){
	//Now we should move forward eventtime to 
	//to an hf...
	unsigned long int athf = floor(eventTime*HF);
	eventTime = athf/HF;
      }
      G4double inbunch=particlesperbunch<1 ? G4UniformRand()*WidthBeamBunch
	: CLHEP::RandExponential::shoot(WidthBeamBunch/particlesperbunch);
      eventTime+=(nHFtoJump/(HF*s))*s+inbunch;
      while(eventTime<oldeventTime) eventTime+=1/HF;
    }
  }
  //Here we will see if we should add gammas from sources
  if(pps<=0){
    //Here I have to use the decay of gammas as the thing that moves 
    //the event time forward. What I do is that I set the event time to 
    //the "latest" gamma decay, decay the other ones the number of required 
    //times to get it right. If there is a large number of sources
    //maybe this sort should be replaced by something else...
    if(thesources.size()>0){
      std::sort(thesources.begin(),thesources.end(),SortSources);
      thesources.back().decay_at_time=CLHEP::RandExponential::
	shoot(1./(1e3*thesources.back().activity)*s);
      eventTime+=thesources.back().decay_at_time;
      thesources.back().CreateParticles(anEvent,particleGun);//,eventTime);
    }
  }
  //Here I just send gammas until there decay time have passed eventTime 
  std::vector<asource>::iterator itsources = thesources.begin();
  for(; itsources!=thesources.end() && 
	!(pps<=0 && itsources==--(thesources.end()));
      ++itsources){ /*If time driven by source dont do source with
		      lowest activity*/
    itsources->decay_at_time=0;
    while(itsources->decay_at_time<=eventTime-oldeventTime){
      itsources->decay_at_time+=CLHEP::RandExponential::
	shoot(1./(1e3*itsources->activity)*s);
      if(itsources->decay_at_time<=eventTime-oldeventTime)
	itsources->CreateParticles(anEvent,particleGun);//,eventTime);
    }
  }
  particleGun->SetParticleTime(0*s);

}

G4String AgataAlternativeGenerator::GetParticleHeader(const G4Event* evt, 
						      G4double /* unitLength */
						      , G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); 
  // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           
    // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  if(pps<=0 && thesources.size()==0){
    sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", 
	    -partType, epart/unitEnergy, 
	    direction.x(), direction.y(), direction.z(), evt->GetEventID());
  } else {
    sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %5d %5d %5d %5d %8.5f\n", 
	    -partType, epart/unitEnergy, 
	    direction.x(), direction.y(), direction.z(), 
	    evt->GetEventID(),days,hours,min,oldeventTime);
  }
  return G4String(aLine);

}

G4String AgataAlternativeGenerator::GetEventHeader( G4double unitLength )
{ 
  char aLine[128];
  G4String eventHeader = G4String("");
  
  G4ThreeVector zerov(0,0,0);
  G4int         atoNum   = theStepping->GetAtoNum();
  G4int         masNum   = theStepping->GetMasNum();
#ifdef G4V10
  G4ParticleDefinition* ion = atoNum>0 && masNum>0 ? 
    G4IonTable::GetIonTable()->GetIon(atoNum,masNum,0.) : 0;
#else
  G4ParticleDefinition* ion = atoNum>0 && masNum>0 ? 
    G4ParticleTable::GetParticleTable()->GetIon(atoNum,masNum,0.) : 0;
#endif

  G4double massP            = ion ? ion ->GetPDGMass() : 0;
  G4double      energy   = ion ? theStepping->GetEnergyI() : 0;
  G4ThreeVector position = ion ? theStepping->GetPosition() : zerov;
  G4ThreeVector momentum = ion ? theStepping->GetMomentumI() : zerov;
  
  //G4cout << "atoN, masN, massP, energy  " << atoNum << " " << masNum << " " << massP/keV << " " << energy/keV << G4endl;
  
  G4double beta = massP>0 ? sqrt( 2. * energy / massP ) : 0;
  
  G4ThreeVector recoildir  = ion ? momentum.unit() : zerov;
  sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, beta, 
	  recoildir.x(), recoildir.y(), recoildir.z() );
  eventHeader += G4String(aLine);
  
  sprintf(aLine, "%5d   %10.8f %10.8f %10.8f\n", -102, position.x()/unitLength, 
	  position.y()/unitLength, position.z()/unitLength );
  eventHeader += G4String(aLine);
  if(theDetector->GetBuildSpectrometer() || TrackIonEnergy){
    sprintf(aLine,"%5d %9.5f %9.5f\n",-104,energy/MeV,massP/MeV);
    eventHeader+=G4String(aLine);
  }
  return eventHeader;  
}


G4String AgataAlternativeGenerator::GetBeginOfEventTag()
{
  char dummy[64];
#ifdef WRITE_EVNUM
  sprintf( dummy, "-100\t%10d\n", theEvent->GetEventID() );
#else  
  sprintf( dummy, "-100\n" );
#endif
  return G4String(dummy);
}

void AgataAlternativeGenerator::PrintToFile( std::ofstream &outFileLMD, G4double /*unitL*/, G4double /*unitE*/ )
{
  outFileLMD << "GENERATOR 1" << G4endl;
  outFileLMD << "ENDGENERATOR" << G4endl;
}


void AgataAlternativeGenerator::SetTarFraction( G4double frac )
{
  if( (frac<0.)||(frac>1.0) ) {
    G4cout << " ---> Invalid value, keeping previous one (" << tarFraction << ")" << G4endl;
    return;
  }
  tarFraction = frac;
  G4cout << " ---> The fraction of reactions on target has been set to " << tarFraction << G4endl;
}

void AgataAlternativeGenerator::EndOfRun()
{
  BeamOut->EndOfRun();
}

void AgataAlternativeGenerator::SetDepth( G4double value )
{
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  targetZThick   = theDetector->GetTargetSize().z();
  depth = targetZThick*(value-0.5);
  G4cout << "Reaction will occur at fixed depth depth= " << depth/(0.001*mm) << G4endl;
}

void AgataAlternativeGenerator::SetFixDepth( G4bool flag )
{
  fixDepth = flag;
  if( fixDepth ) {
       G4cout << "Reaction will occur at fixed depth" << G4endl;
//       isUnifDistrib = false;
  }
  else {
       G4cout << "Reaction will occur at different depths according to profile.dat distribution if specified or uniformely" << G4endl;       
//       isUnifDistrib = true;
  }
}
void AgataAlternativeGenerator::SetUnifDistrib( G4bool flag )
{
  isUnifDistrib = flag;
  if( isUnifDistrib ) {
       G4cout << "Reaction will occur at depths uniformely distributed in the target thichness" << G4endl;
//       fixDepth = false;
  }
//  else {
//       G4cout << "Reaction will occur at fixed depth" << G4endl;       
//       fixDepth = true;
//  }
}

void AgataAlternativeGenerator::SetSourceType(G4String src,G4double act){
  asource tmp;
  tmp.source=src;
  tmp.activity=act;
  if(tmp.ReadDataFile()) thesources.push_back(tmp);
  else {
    std::cout << "WARNING! No data file (" << src << ".g4srcdata) was fond!!!!"
	      << std::endl;
  }
}


void AgataAlternativeGenerator::GetEventInfo(std::ostream &eventinfo)

{
  eventinfo << " days hours min oldeventTime HFtime Bunchtime pps HF WidthBeamBunch particlesperbunch \n";
  eventinfo << days << " " << hours << " " << min << " " 
	    << oldeventTime << " "
	    << " " << HFtime
	    << " " << Bunchtime
	    << " " << pps //Particles per second
	    << " " << HF //Acc High freq
	    << " " << WidthBeamBunch
	    << " " << particlesperbunch << "\n";
}

////////////////////
// The Messenger
////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"


AgataAlternativeGeneratorMessenger::AgataAlternativeGeneratorMessenger(AgataAlternativeGenerator* pTarget,G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/";
  
  topDirectory = new G4UIdirectory(directoryName);
  topDirectory->SetGuidance("Control of event generation.");
  
  directoryName = name + "/generator/emitter/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of reaction parameters.");
  
  commandName = directoryName + "tarFraction";
  aLine = commandName.c_str();
  SetTarFractionCmd = new G4UIcmdWithADouble(aLine, this);  
  SetTarFractionCmd->SetGuidance("Define fraction of reactions in target and degrader.");
  SetTarFractionCmd->SetGuidance("Required parameters: 1 double (fraction).");
  SetTarFractionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableFixDepth";
  aLine = commandName.c_str();
  EnableFixDepthCmd = new G4UIcmdWithABool(aLine, this);
  EnableFixDepthCmd->SetGuidance("Activate fix reaction depth mode");
  EnableFixDepthCmd->SetGuidance("Required parameters: none.");
  EnableFixDepthCmd->SetParameterName("fixDepth",true);
  EnableFixDepthCmd->SetDefaultValue(true);
  EnableFixDepthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableFixDepth";
  aLine = commandName.c_str();
  DisableFixDepthCmd = new G4UIcmdWithABool(aLine, this);
  DisableFixDepthCmd->SetGuidance("De-Activate fix reaction depth mode");
  DisableFixDepthCmd->SetGuidance("Required parameters: none.");
  DisableFixDepthCmd->SetParameterName("fixDepth",true);
  DisableFixDepthCmd->SetDefaultValue(false);
  DisableFixDepthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "enableUniformDistr";
  aLine = commandName.c_str();
  EnableUnifDistribCmd = new G4UIcmdWithABool(aLine, this);
  EnableUnifDistribCmd->SetGuidance("Activate uniform distribution reaction points mode");
  EnableUnifDistribCmd->SetGuidance("Required parameters: none.");
  EnableUnifDistribCmd->SetParameterName("fixDepth",true);
  EnableUnifDistribCmd->SetDefaultValue(true);
  EnableUnifDistribCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableUniformDistr";
  aLine = commandName.c_str();
  DisableUnifDistribCmd = new G4UIcmdWithABool(aLine, this);
  DisableUnifDistribCmd->SetGuidance("De-Activate uniform distribution reaction points mode");
  DisableUnifDistribCmd->SetGuidance("Required parameters: none.");
  DisableUnifDistribCmd->SetParameterName("fixDepth",true);
  DisableUnifDistribCmd->SetDefaultValue(false);
  DisableUnifDistribCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "setDepth";
  aLine = commandName.c_str();
  SetDepthCmd = new G4UIcmdWithADouble(aLine, this);  
  SetDepthCmd->SetGuidance("Define depth of reaction in terms of percentage of target thickness");
  SetDepthCmd->SetGuidance("Required parameters: 1 double (fraction) between 0 and 1.");
  SetDepthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


  commandName = directoryName + "setSlopeGammaBackground";
  aLine = commandName.c_str();
  SetSlopeExpBkgCmd = new G4UIcmdWithADouble(aLine, this);  
  SetSlopeExpBkgCmd->SetGuidance("Set slope parameter in Exp gamma bkg");
  SetSlopeExpBkgCmd->SetGuidance("Required parameters: 1 double");
  SetSlopeExpBkgCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setMaxEGammaBackground";
  aLine = commandName.c_str();
  SetMaxEBgkGamma = new G4UIcmdWithADouble(aLine, this);  
  SetMaxEBgkGamma->SetGuidance("Set Max E gamma bkg");
  SetMaxEBgkGamma->SetGuidance("Required parameters: 1 double");
  SetMaxEBgkGamma->AvailableForStates(G4State_PreInit,G4State_Idle);


  commandName = directoryName + "setNumberOfGammaBackground";
  aLine = commandName.c_str();
  SetNbExtraGammasCmd = new G4UIcmdWithAnInteger(aLine, this);  
  SetNbExtraGammasCmd->SetGuidance("Set number of background gammas");
  SetNbExtraGammasCmd->SetGuidance("Required parameters: 1 int");
  SetNbExtraGammasCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ClearListOfDiscreteGammas";
  aLine = commandName.c_str();
  ClearDiscreteGammas = new G4UIcmdWithoutParameter(aLine,this);
  ClearDiscreteGammas->SetGuidance("Clears list of discrete gamma");
  ClearDiscreteGammas->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "AddDiscreteGamma";
  aLine = commandName.c_str();
  AddDiscreteGamma = new  G4UIcmdWithAString(aLine,this);
  AddDiscreteGamma->SetGuidance("Adds a discrete gamma");
  AddDiscreteGamma->SetGuidance("Requires energy [MeV] Gammas/event [double]");
  AddDiscreteGamma->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetGammaRaySource";
  aLine = commandName.c_str();
  SetSource = new G4UIcmdWithAString(aLine,this);
  SetSource->SetGuidance("Adds a gamma-ray source");
  SetSource->SetGuidance("Give source and activity (in kBq)");
  SetSource->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetParticlePerSeconds";
  aLine=commandName.c_str();
  SetPPS = new G4UIcmdWithADouble(aLine,this);
  SetPPS->SetGuidance("Beam-particle/s [Hz]");
  SetPPS->SetGuidance("0 means disregard");
  SetPPS->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetAccelaratorHF";
  aLine=commandName.c_str();
  SetHF = new G4UIcmdWithADoubleAndUnit(aLine,this);
  SetHF->SetGuidance("Repetion rate");
  SetHF->SetGuidance("0 means disregard");
  SetHF->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "SetWidthOfBeamPulse";
  aLine=commandName.c_str();
  SetWidthBeamBunch = new G4UIcmdWithADoubleAndUnit(aLine,this);
  SetWidthBeamBunch->SetGuidance("Width of beampulse");
  SetWidthBeamBunch->SetGuidance("0 means disregard");
  SetWidthBeamBunch->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "SetNumberOfParticlesPerReaction";
  aLine = commandName.c_str();
  SetEventsPerReaction = new  G4UIcmdWithAnInteger(aLine,this);
  SetEventsPerReaction->
    SetGuidance("Set number of events before each reaction");
  SetEventsPerReaction->SetGuidance("integer");
  SetEventsPerReaction->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "SetTrackIonEnergy";
  aLine=commandName.c_str();
  TrackIonEnergyCmd = new G4UIcmdWithABool(aLine,this);
  TrackIonEnergyCmd->SetGuidance("Store energy of ion when leaves target");
  TrackIonEnergyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  
  commandName = directoryName + "SetRecoilFile";
  aLine = commandName.c_str();
  SetRecoilFile = new  G4UIcmdWithAString(aLine,this);
  SetRecoilFile->SetGuidance("Set file to read recoils from");
  SetRecoilFile->SetGuidance("Filename");
  SetRecoilFile->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAlternativeGeneratorMessenger::~AgataAlternativeGeneratorMessenger()
{
  delete myDirectory;
  delete SetTarFractionCmd;
  delete EnableFixDepthCmd;
  delete DisableFixDepthCmd;
  delete EnableUnifDistribCmd;
  delete DisableUnifDistribCmd;
  delete SetDepthCmd;
  delete SetNbExtraGammasCmd;
  delete SetSlopeExpBkgCmd;
  delete SetEventsPerReaction;
  delete SetPPS;
  delete SetSource;
  delete TrackIonEnergyCmd;
  delete SetRecoilFile;
}

void AgataAlternativeGeneratorMessenger::SetNewValue(G4UIcommand* command,
						     G4String newValue)
{ 
  if( command == SetTarFractionCmd ) {
    myTarget->SetTarFraction(SetTarFractionCmd->GetNewDoubleValue(newValue));
  }
  if( command == EnableFixDepthCmd ) {
    myTarget->SetFixDepth(EnableFixDepthCmd->GetNewBoolValue(newValue));
  }
  if( command == DisableFixDepthCmd ) {
    myTarget->SetFixDepth(DisableFixDepthCmd->GetNewBoolValue(newValue));
  }
  if( command == EnableUnifDistribCmd ) {
    myTarget->SetUnifDistrib(EnableUnifDistribCmd->GetNewBoolValue(newValue));
  }
  if( command == DisableUnifDistribCmd ) {
    myTarget->SetUnifDistrib(EnableUnifDistribCmd->GetNewBoolValue(newValue));
  }
  if( command == SetDepthCmd ) {
    myTarget->SetDepth(SetDepthCmd->GetNewDoubleValue(newValue));
  }
  if(command==SetSlopeExpBkgCmd){
    myTarget->SetSlopeExpBkg(SetSlopeExpBkgCmd->GetNewDoubleValue(newValue));
  }
  if(command==SetMaxEBgkGamma){
    myTarget->SetMaxEBkg(SetMaxEBgkGamma->GetNewDoubleValue(newValue));
  }
  if(command==SetNbExtraGammasCmd){
    myTarget->SetNbExtraBkgGammas(SetNbExtraGammasCmd->
				  GetNewIntValue(newValue));
  }
  if(command==ClearDiscreteGammas){
    myTarget->ClearExtraGammas();
  }
  if(command==AddDiscreteGamma){
    std::istringstream input(newValue);
    G4double e,I;
    if(input >> e >> I){
      myTarget->AddExtraDiscreteGamma(e*MeV,I);
      G4cout<<"---->Added discrete gamma energy  "
	    <<G4BestUnit(e,"Energy")<<G4endl;
    }
  }
  if(command==SetEventsPerReaction){
    G4RunManager * runManager = G4RunManager::GetRunManager();    
    ((AgataPhysicsList*)runManager->GetUserPhysicsList())->
      GetReactionPhysics()->GetReactionProcess()->
      SetEventsPerReaction(SetEventsPerReaction->GetNewIntValue(newValue));
  }
  if(command==SetPPS){
    myTarget->SetPPS(SetPPS->GetNewDoubleValue(newValue));
  }
  if(command==SetHF){
    myTarget->SetHF(SetHF->GetNewDoubleValue(newValue));
  }
  if(command==SetWidthBeamBunch){
    myTarget->SetWidthBeamBunch(SetWidthBeamBunch->
				GetNewDoubleValue(newValue));
  }
  if(command==SetSource){
    G4String src; G4double a;
    std::istringstream input(newValue);
    if(input >> src >> a){
      myTarget->SetSourceType(src,a);
      G4cout<<"---->Added source " << src << " with activity " << a << " kBq"
	    << G4endl;
    }
  }
  if(command==TrackIonEnergyCmd){
    myTarget->SetTrackIonEnergy(TrackIonEnergyCmd->GetNewBoolValue(newValue));
  }
  if(command==SetRecoilFile){
    myTarget->SetIonRecoilFile(newValue);
  }
}
