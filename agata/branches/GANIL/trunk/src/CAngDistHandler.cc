#include "CAngDistHandler.hh"
#include "Randomize.hh"
#ifdef G4V10
#include "G4SystemOfUnits.hh"
#endif

AngDistHandler::AngDistHandler( G4String inFileName )
{
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open angular distribution file "
           << inFileName << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;

  //G4double th_min, th_max, dth, e_min, e_max, de;
  //inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
  //this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  this->ReadDistribution(inputFile);
  inputFile.close();
  
  phi_min =   0.*deg;
  phi_dif = 360.*deg;
  
  fixedTheta  = false;
}

void AngDistHandler::ReNew( G4String inFileName )
{
  for( G4int ii=0; ii<(G4int)difAngDist.size(); ii++ ) {
    difAngDist[ii].clear();
    intAngDist[ii].clear();  
  }
  difAngDist.clear();
  intAngDist.clear();
  eneDifDist.clear();  
  eneIntDist.clear(); 
  
  rawEner.clear();
  rawThet.clear();
  rawSigm.clear();
  mulEner.clear();
  mulThet.clear();
  mulSigm.clear();
  intSigm.clear();
  
  goodDistribution = false;	   
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open angular distribution file "
           << inFileName << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;
  //G4double th_min, th_max, dth, e_min, e_max, de;
  //inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
  //this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  this->ReadDistribution(inputFile);
  inputFile.close();
}

void AngDistHandler::InitData( G4double emin, G4double emax, G4double de, G4double thmin, G4double thmax, G4double dth)
{
  if( emax > emin ) {
    E_min = emin;
    E_max = emax;
  }
  else {
    E_min = emax;
    E_max = emin;
  }
  
  E_dif = de;
//   if( E_dif == 0. )
//     E_dif = 1.0*MeV;
  
  if( thmax > thmin ) {
    th_min = thmin;
    th_max = thmax;
  }
  else {
    th_min = thmax;
    th_max = thmin;
  }
  th_dif = dth;
//   if( th_dif == 0. )
//     th_dif = 1.0*deg;
  
  G4int n_en=0;
  G4int n_th=0; 
  
  if( E_dif==0. ) 
     n_en=1;
  else
     n_en = (G4int)((E_max-E_min)/E_dif+1.0000001);
     
  if( th_dif==0. ) 
     n_th=1;
  else 
     n_th = (G4int)((th_max-th_min)/th_dif+1.000000001);
     
  //G4int n_en = (G4int)ceil((E_max-E_min)/E_dif )+1;
  //G4int n_th = (G4int)ceil((th_max-th_min)/th_dif)+1;

//   G4cout << "th_max, th_min, th_dif " << th_max << " " << th_min << " " << th_dif << G4endl;
// 
//   G4cout << "n_en " << n_en << " " << "n_th " << n_th << G4endl;
//   G4cout <<  (th_max-th_min)/th_dif+1. << G4endl;
  
  difAngDist.resize(n_en);
  intAngDist.resize(n_en);
  eneDifDist.resize(n_en);
  eneIntDist.resize(n_en);
  
  for( G4int ii=0; ii<n_en; ii++ ) {
    difAngDist[ii].resize(n_th);
    intAngDist[ii].resize(n_th);
    for( G4int jj=0; jj<n_th; jj++ ) {
      difAngDist[ii][jj] = 0.;
      intAngDist[ii][jj] = 0.;    
    }
    eneDifDist[ii] = 0.;  
    eneIntDist[ii] = 0.;  
  }
}

AngDistHandler::~AngDistHandler()
{
  for( G4int ii=0; ii<(G4int)difAngDist.size(); ii++ ) {
    difAngDist[ii].clear();
    intAngDist[ii].clear();  
  }
  difAngDist.clear();
  intAngDist.clear();
  eneDifDist.clear();  
  eneIntDist.clear();
                      
  rawEner.clear();  
  rawThet.clear();  
  rawSigm.clear();  
  mulEner.clear();  
  mulThet.clear();  
  mulSigm.clear();  
  intSigm.clear();  
}

void AngDistHandler::ReadDistribution(std::ifstream& inFile)
{
  G4int ii, jj;

  char linea[256];
  G4double dum1, dum2, dum3;
  
  G4int maxSize = 18000;    // will convert distributions into 1/100 deg spacing
  maxSigma = -1.;  // already decalred in header file
  G4int  oriSize;

  // Read differential distribution from file
  while( inFile.getline(linea,256) ) {
    sscanf( linea, "%lf %lf %lf", &dum1, &dum2, &dum3 );
    //G4cout << " letto " << dum1 << " " << dum2 << " " << dum3 << G4endl;
    rawEner.push_back( dum2*keV );
    rawThet.push_back( dum1*deg );
    rawSigm.push_back( dum3 );
    if( maxSigma < dum3 ) 
      maxSigma = dum3;
  }
  
  oriSize = rawThet.size();

  // are angles increasing? If not, reverse!
  //  G4bool isReversed = false;
  if( rawThet[1] < rawThet[0] ) {
    //    isReversed = true;
    std::vector<G4double> dummy_rev;
    dummy_rev.clear();
    dummy_rev.resize(rawThet.size());

    for( ii=0; ii<(G4int)(rawEner.size()); ii++ ) {
      dummy_rev[ii] = rawEner[rawEner.size()-1-ii];
    }
    for( ii=0; ii<(G4int)(rawEner.size()); ii++ ) {
      rawEner[ii] = dummy_rev[ii];
    }

    for( ii=0; ii<(G4int)(rawSigm.size()); ii++ ) {
      dummy_rev[ii] = rawSigm[rawSigm.size()-1-ii];
    }
    for( ii=0; ii<(G4int)(rawSigm.size()); ii++ ) {
      rawSigm[ii] = dummy_rev[ii];
    }

    for( ii=0; ii<(G4int)(rawThet.size()); ii++ ) {
      dummy_rev[ii] = rawThet[rawThet.size()-1-ii];
    }
    for( ii=0; ii<(G4int)(rawThet.size()); ii++ ) {
      rawThet[ii] = dummy_rev[ii];
    }

  }

  // interpolate (evenly spaced values)...
  mulThet.resize(maxSize);
  mulEner.resize(maxSize);
  mulSigm.resize(maxSize);
  
  for( ii=0; ii<maxSize; ii++ ) {
    // angle is easy, simpy the vector index ...
    mulThet[ii] = (G4double)(ii/100.)*deg;
    
    // where is theta in the original data???
    if( rawThet[0] > mulThet[ii] ) 
      continue;
    // should not exceed boundaries!!!
    if( rawThet[oriSize-1] < mulThet[ii] ) 
      continue;
      
    for( jj=0; jj<oriSize; jj++ ) {
      if( rawThet[jj] > mulThet[ii] ) break;   
    }   
    mulEner[ii] = rawEner[jj-1] + (rawEner[jj]-rawEner[jj-1])*(mulThet[ii]-rawThet[jj-1])/(rawThet[jj]-rawThet[jj-1]);
    mulSigm[ii] = rawSigm[jj-1] + (rawSigm[jj]-rawSigm[jj-1])*(mulThet[ii]-rawThet[jj-1])/(rawThet[jj]-rawThet[jj-1]);
  }   
  for( ii=0; ii<maxSize; ii++ ) {
    mulSigm[ii] = mulSigm[ii] * sin(mulThet[ii]);
//    
//     G4cout << "mulThet " << mulThet[ii] << ", mulSigm " << mulSigm[ii] << G4endl;
//  
  }   
  
   refSigma.clear();
   refSigma.resize(mulSigm.size());
   for( ii=0; ii<(G4int)mulSigm.size(); ii++ )
     refSigma[ii] = mulSigm[ii];

  intSigm.clear();
  intSigm.resize( mulSigm.size() );
  //G4cout << " size matters " << rawSigm.size() << G4endl;
  //G4cout << " size matters1 " << rawThet.size() << G4endl;
  //G4cout << " size matters2 " << rawEner.size() << G4endl;

//     G4cout << "Distrib originale " << G4endl;
//     for( ii=0; ii<(G4int)mulSigm.size(); ii++ ) {
//       G4cout << mulThet[ii]/deg << " " << mulSigm[ii] << G4endl;
//     }


  // generates integral distribution
  for( ii=1; ii<(G4int)mulSigm.size(); ii++ ) {
    intSigm[ii] = mulSigm[ii] + intSigm[ii-1];
  }
  // normalizes last value to 1
  for( ii=0; ii<(G4int)mulSigm.size(); ii++ ) {
    intSigm[ii] = intSigm[ii] / intSigm[(G4int)mulSigm.size()-1];
   
//    G4cout << "ii " << ii << ", intSigm[ii] " <<  intSigm[ii] << G4endl;

  }
  
  refSigIn.clear();
  refSigIn.resize(intSigm.size());
   for( ii=0; ii<(G4int)intSigm.size(); ii++ )
     refSigIn[ii] = intSigm[ii];
  
  
//     G4cout << "Distrib integrale " << G4endl;
//     for( ii=0; ii<(G4int)mulSigm.size(); ii++ ) {
//       G4cout << mulThet[ii]/deg << " " << intSigm[ii] << G4endl;
//     }
//     G4cout << "Fine" << G4endl;
  
}

G4double AngDistHandler::GetEnergy()
{
  this->SelectTheta();
  return chosenEnergy;  
}

G4ThreeVector AngDistHandler::GetDirection( G4double )
{
  G4double theta = chosenTheta;  
  G4double phi = phi_min + G4UniformRand() * phi_dif;
  
  return G4ThreeVector( sin(theta)*cos(phi), sin(theta)*sin(phi), cos(theta) );
}

void AngDistHandler::SelectTheta()
{
  G4int jj = 0, sss;
  G4double z = G4UniformRand() * 1.0;
  
  //G4cout << "size " << rawThet.size() << G4endl;
  
#if 0
  for( ii=0; ii<(G4int)(mulSigm.size()); ii++ ) { 
    if( mulSigm[ii] != refSigma[ii] ) 
      G4cout << "Losco!!!" << G4endl;

    if( intSigm[ii] != refSigIn[ii] ) 
      G4cout << "Piu' losco!!!" << G4endl;
  }
#endif

#if 0
  while(1) { 
    // angle
    size = mulSigm.size();
    ii = (G4int)(G4UniformRand() * size);
    w  = G4UniformRand() * maxSigma;
    if( w < mulSigm[ii] ) break;
  }
  chosenTheta = mulThet[ii];
#endif
    
    
//#if 0
  if( rawThet.size() == 1 ) {
    chosenTheta = rawThet[jj];
    chosenEnergy = rawEner[jj];
  //G4cout << " chosenTheta " << chosenTheta/deg << G4endl;
  //G4cout << " chosenEnergy " << chosenEnergy/keV << G4endl;
    return;
  }
  //while( (intSigm[jj] < z ) && (jj<(G4int)(intSigm.size()) ) )    
  //  jj++;
  sss = (G4int)(intSigm.size()) - 1;
  while( intSigm[jj] < z ) { 
    if( jj==sss ) {
      chosenTheta  = mulThet[sss];
      chosenEnergy = mulEner[sss];
      G4cout << "Reached boundaries " << G4endl;
      return;
    }
    jj++;
  }
  jj--;
  //chosenTheta  = mulThet[jj] + (mulThet[jj+1]-mulThet[jj]) * (z-intSigm[jj])/(intSigm[jj+1]-intSigm[jj]);
  //chosenEnergy = mulEner[jj] + (mulEner[jj+1]-mulEner[jj]) * (z-intSigm[jj])/(intSigm[jj+1]-intSigm[jj]);
  chosenTheta = mulThet[jj];
  
//   G4cout << "                        " << jj << " " << mulThet[jj]/deg << " " << chosenTheta/deg << G4endl; 
//   G4cout << "Ultra " << jj << " " << mulThet[jj] << G4endl; 
//   G4cout << "Iper " << 100.*chosenTheta << G4endl; 
  
  //chosenTheta  = rawThet[jj] + (rawThet[jj+1]-rawThet[jj]) * (z-intSigm[jj])/(intSigm[jj+1]-intSigm[jj]);
  //chosenEnergy = rawEner[jj] + (rawEner[jj+1]-rawEner[jj]) * (z-intSigm[jj])/(intSigm[jj+1]-intSigm[jj]);
  //G4cout << " chosenTheta " << chosenTheta/deg << G4endl;
  //G4cout << " chosenEnergy " << chosenEnergy/keV << G4endl;
//#endif
}

void AngDistHandler::SetPhiRange( G4double phimin, G4double phimax )
{
  phi_min = phimin * deg;
  phi_dif = phimax * deg;
  
  if( phi_dif < phi_min ) {
    phi_dif = phimin * deg;
    phi_min = phimax * deg;
  }
  
  if( phi_min < 0. ) 
    phi_min = 0.;
  if( phi_dif > 360.*deg )
    phi_dif = 360.*deg;
    
  G4cout << " --> Projectile-like emission will be limited to the " << phi_min/deg
         << "--" << phi_dif/deg << " deg range." << G4endl;

  phi_dif -= phi_min;	  
}

void AngDistHandler::SetThRange( G4double thmin, G4double thmax )
{
  
  if( thmax < thmin ) {
    fixedTheta = false;
    return;
  }
  
  fixedTheta = true;
  
  the_min = thmin * deg;
  the_dif = thmax * deg;
  
  G4cout << " --> Projectile-like emission will be limited to the theta " << the_min/deg
         << "--" << the_dif/deg << " deg range." << G4endl;

  G4double cosmin, cosmax;
  
  cosmin = cos(the_dif);
  cosmax = cos(the_min);
  
  the_min = cosmin;
  the_dif = cosmax - cosmin;
}
