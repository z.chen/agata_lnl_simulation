#include "Reaction.hh"
#include "Outgoing_Beam.hh"
#include "Incoming_Beam.hh"

using namespace std;

Reaction::Reaction(Outgoing_Beam* BO, Incoming_Beam *BI, const G4String& aName)
  : G4VProcess(aName), BeamOut(BO), BeamIn(BI) 
{
  threshold=0.00001;  //1e-05mm
//  G4cout << " Reaction costruttore threshold " << threshold/mm << G4endl; 
  if (verboseLevel>1) {
  G4cout <<GetProcessName() << " is created "<< G4endl;
  }
  ReactionEachNEvents=1;
}

Reaction::~Reaction() 
{}                                     

G4VParticleChange* Reaction::PostStepDoIt(
			     const G4Track& aTrack,
			     const G4Step& 
			    )
//
// Stop the current particle, if requested by G4UserLimits 
// 			    			    			    
{
  aParticleChange.Initialize(aTrack);
  if(reaction_here) {
    reaction_here=false;
    BeamOut->ScanInitialConditions(aTrack);
    aParticleChange.ProposeTrackStatus(fStopAndKill);
    if(G4UniformRand()<BeamOut->getTFrac()) {
      aParticleChange.SetNumberOfSecondaries(2);	  
      aParticleChange.AddSecondary(BeamOut->ProjectileGS(),
				   BeamOut->ReactionPosition(),true);
      aParticleChange.AddSecondary(BeamOut->TargetExcitation(),
				   BeamOut->ReactionPosition(),true);
    } else {
      std::vector<G4DynamicParticle*> secvec = BeamOut->ReactionProduct();
      aParticleChange.SetNumberOfSecondaries(secvec.size());
      for(unsigned int i=0; i<secvec.size(); i++){
	aParticleChange.AddSecondary(secvec[i],
				     BeamOut->ReactionPosition(),true);
      }
    }
  }
  return &aParticleChange;
}


#include "G4RunManager.hh"

G4double Reaction::PostStepGetPhysicalInteractionLength(
                             const G4Track& aTrack,
                             G4double,
                             G4ForceCondition* condition
							)
{
  reaction_here=false;
  *condition=NotForced;
  G4RunManager * runManager = G4RunManager::GetRunManager();
  if(ReactionEachNEvents>0){
    if(runManager->GetCurrentEvent()->GetEventID()%ReactionEachNEvents!=0){
      return DBL_MAX;
    } else {
      ;
    }
  }
  if(BeamOut->ReactionOn()) {
    if(BeamOut!=NULL && BeamIn!=NULL){
      G4String name=aTrack.GetVolume()->GetLogicalVolume()->GetName();
      G4UserLimits* pUserLimits = aTrack.GetVolume()->GetLogicalVolume()->
	GetUserLimits();
      int Ain   = aTrack.GetDynamicParticle()->GetDefinition()->
	GetAtomicMass();
      int Zin   = aTrack.GetDynamicParticle()->GetDefinition()->
	GetAtomicNumber();
      if((BeamIn->getA()==Ain && BeamIn->getZ()==Zin)){
	if(name=="Target"||name=="Degrader") {
	  G4ThreeVector frameTrans = 
	    aTrack.GetVolume()->GetFrameTranslation();
	  const G4RotationMatrix* rot = 
	    aTrack.GetVolume()->GetFrameRotation();
	  G4RotationMatrix frameRot;
	  if(rot) frameRot = *rot;
	  G4ThreeVector position = aTrack.GetPosition();  
	  G4ThreeVector posSol = frameRot( position );
	  posSol += frameRot( frameTrans );
	  G4double Z = pUserLimits->GetUserMinRange(aTrack) - posSol.z();
	  if(Z<0) {
	    return DBL_MAX;
	  }
	  if(Z>threshold) {
	    G4ThreeVector dir=aTrack.GetDynamicParticle()->
	      GetMomentumDirection();
	    return Z*dir.mag();
	  }
	  if(Z<threshold) {
	    reaction_here=true;
	    //in such a way, once it reacts it cannot do once more ...
	    BeamOut->SetReactionOff(); 
	    //in case we would like to change the final nucleus event 
	    //by event ...
	    //	BeamOut->setDecayProperties();
	    return 0.;
	  }
	}
      }
    }
  }
  return DBL_MAX;
}

G4double Reaction::AtRestGetPhysicalInteractionLength( const G4Track&, 
						       G4ForceCondition* )
{ 
  return -1.0; 
}

G4VParticleChange* Reaction::AtRestDoIt( const G4Track&, const G4Step& )
{
  return 0;
}

G4double Reaction::AlongStepGetPhysicalInteractionLength( const G4Track&, 
							  G4double, G4double, 
							  G4double&, 
							  G4GPILSelection* )
{ 
  return -1.0; 
}

G4VParticleChange* Reaction::AlongStepDoIt( const G4Track&, const G4Step& )
{
  return 0;
}

  

  
