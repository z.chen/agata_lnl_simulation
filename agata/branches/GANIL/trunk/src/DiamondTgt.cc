/***************************************************************************
                          DiamondTgt.cc  -  description
                             -------------------
    begin                : July 2006
    copyright            : (C) 2006 by Mike Taylor
    email                : mjt502@york.ac.uk
 ***************************************************************************/
// October 2009 Adapted for Agata code by P. Joshi, The University of York.
#include "DiamondTgt.h"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "globals.hh"
#include "G4ios.hh"

 DiamondTgt::DiamondTgt() :
 DiamondTgt_log(0), DiamondTgt_phys(0)
 {
   }

 DiamondTgt::~DiamondTgt()
 {
   }

void DiamondTgt::createDetector(G4VPhysicalVolume *lab_phy, G4Material* detMaterial)
{

  G4double DiamondTgt_z = 0.1*mm;  //   1/2 z length (thickness i.e. 200 um)
  G4double DiamondTgt_y = 9.5*mm;     //   1/2 y length (3cm x 3cm) was 1.5
  G4double DiamondTgt_x = 9.5*mm;     //   1/2 x length was 1.5

  G4Box* DiamondTgt_box = new G4Box("DiamondTgt_box", DiamondTgt_x, DiamondTgt_y, DiamondTgt_z);

  DiamondTgt_log = new G4LogicalVolume(DiamondTgt_box, detMaterial, "DiamondTgt_log", 0,0,0);

  G4VisAttributes* DiamondTgtVisATT
                 = new G4VisAttributes(G4Colour(0.72,0.72,0.99));      // Set detector colour for visualisation

  DiamondTgtVisATT->SetForceSolid(true);     // Solid appearance NOT wireframe

  DiamondTgt_log->SetVisAttributes(DiamondTgtVisATT);

  // set placement positions of physical segment volumes
 
 placement[1] = G4ThreeVector(20*mm, 20*mm, 1*cm);
 placement[2] = G4ThreeVector(20*mm, 0, 1*cm);
 placement[3] = G4ThreeVector(20*mm, -20*mm, 1*cm);
 placement[4] = G4ThreeVector(0, 20*mm, 1*cm);
 placement[5] = G4ThreeVector(0, 0, 1*cm);
 placement[6] = G4ThreeVector(0, -20*mm, 1*cm);
 placement[7] = G4ThreeVector(-20*mm, 20*mm, 1*cm);
 placement[8] = G4ThreeVector(-20*mm, 0, 1*cm);
 placement[9] = G4ThreeVector(-20*mm, -20*mm, 1*cm);
  
 char phys_name[20];

  for(G4int i=1; i<10; i++)
    {
      sprintf(phys_name, "DiamondTgt%d", i);
        Diamond_phys_cpy[i] = new  G4PVPlacement(0,
                 placement[i],
                 phys_name, DiamondTgt_log, lab_phy, false, i );
G4cout << "Lycca Placement for DiamondTgt Segment#" << i+000 <<"=" << placement[i] << G4endl;
	}


 }
