#ifdef ANCIL
// this code was created by grzegorz jaworski -> tatrofil@slcj.uw.edu.pl

#include "AgataAncillaryRFD.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4Cons.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

using namespace std;

/****************************
 * RFD geometry description *
 ****************************/
/*
  RFD = Recoil Filter Detector

  to start AGATA + RFD simulation you should start AGATA with "-a 5".
 
  The RFD consists of three concentric rings of 6 individual detector elements mounted 1250 mm from the thin target.

  In the desription I will use the following notation:
  - Z axis is the axis of the beam (direction of the Z axis is downstream),
  - Y axis is directed verticaly and up,
  - X axis is directed to the rigth (while looking from the target at the RFD foils),
  - theta angle is an angle from Z axis,
  - phi angle is an angle directed from +Y to -X axis.
  
  The theta angle of each ring is:
  2.90 * deg - for  inner ring (IR)
  5.15 * deg - for middle ring (MR)
  5.69 * deg - for  outer ring (OR)

  Each detector element has it's own number, which is set in this code. (last column is my own notation)
   1 - IR, phi = 270. * deg;  IR5
   2 - IR, phi = 330. * deg;  IR6
   3 - IR, phi =  30. * deg;  IR1
   4 - IR, phi =  90. * deg;  IR2
   5 - IR, phi = 150. * deg;  IR3
   6 - IR, phi = 210. * deg;  IR4
   7 - OR, phi = 270. * deg;  OR5
   8 - MR, phi = 300. * deg;  MR6
   9 - OR, phi = 330. * deg;  OR6
  10 - MR, phi =   0. * deg;  MR1
  11 - OR, phi =  30. * deg;  OR1
  12 - MR, phi =  60. * deg;  MR2
  13 - OR, phi =  90. * deg;  OR2
  14 - MR, phi = 120. * deg;  MR3
  15 - OR, phi = 150. * deg;  OR3
  16 - MR, phi = 180. * deg;  MR4
  17 - OR, phi = 210. * deg;  OR4
  18 - MR, phi = 240. * deg;  MR5
  

  in this version of the RFD GEANT4 for AGATA code, the following parts are included:
  - mylar foils of detector elements,
  - cone - largest part of RFD construction,
  - target chamber,
  - ion guide.

  The following commands can be used to set some RFD parameters interactively  (at the "Idle>" prompt):
   enableIonGuide * Creating ion guide.
   disableIonGuide * Ion guide will not be generated.
   enableRFDCones * Creating RFD cones.
   disableRFDCones * No RFD cones will be generated.
   enableTargetChamber * Creating target chamber.
   disableTargetChamber * No target chamber will be generated.
   TargetChamberDiameter * Sets inner diameter of target chamber.
   TargetChamberThickness * Sets target chamber thickness.
   IonGuideLength * Sets ion guide length.
   IonGuideThickness * Sets ion guide thickness.
   IonGuideInnerDiameter * Sets ion guide inner diameter.
   RFDMylarFoilThickness * Sets RFD mylar foils thickness.
   RFDMylarFoilsDistanceToTarget * Sets RFD mylar foils distance to target.
   LengthOfBellow * Sets length of the bellow' (place for bellow). - bellow will be set between target chamber and RFD cone. bellow is not included in this version.

  for more information about RFD -> http://chall.ifj.edu.pl/~dept2/rfd/rfd.htm
*/



AgataAncillaryRFD::AgataAncillaryRFD(G4String path,G4String name)
{
  // dummy assignment needed for compatibility with "real" implementations of this class
  G4String iniPath     = path;
  dirName = name;

  useIonGuide      = true;
  useRFDCones      = true;
  useTargetChamber = true;
  useTarget        = false;

  RFDTargetChamber_Phi          =  200.  * mm; //inner diameter
  RFDTargetChamber_Thickness    =    2.  * mm;  

  RFDIonGuide_Length            =    0.5 *  m;
  RFDIonGuide_Thickness         =    2.  * mm;
  RFDIonGuide_Phi               =   40.  * mm; //inner diameter

  RFDMylarFoilsDistanceToTarget = 1350. * mm;
  SensitivePartsMylarThickness  =  800. * nm; 
  //RFD mylar foils thickness - most common 800 nm, other: 500nm, 1000nm

  LengthOfBellow                =   30. * mm;
  LengthOfBaniak                =  425. * mm;
  
  RFDOneCone_InnerPhiSt         =   31.74 * mm;
  RFDOneCone_Thickness          =    2.   * mm;
  RFDOneCone_InnerPhiEnd        =  242.5  * mm;

  RFDTarget_Thick               =    2.   * mm;
  RFDTarget_Diameter            =   10.   * mm;
  RFDTarget_Offset              =    0.   * mm;
  
  //you can easily move all parts of RFD geometry by changing ZeroPosition values
  ZeroPositionX = 0.0 * mm;
  ZeroPositionY = 0.0 * mm;
  ZeroPositionZ = 0.0 * mm;

  //MATERIALS
  matMylarRFDName = "Mylar";
  matMylarRFD = NULL;
  matRFDTargetChamberName = "Aluminium";
  matRFDTargetChamber     = NULL;
  matRFDIonsTubeName = "Aluminium";
  matRFDIonsTube     = NULL;
  matRFDOneConeName = "Aluminium";
  matRFDOneCone     = NULL;
  matRFDTargetName         = "Aluminium";
  matRFDTarget             = NULL;

  ancSD = NULL;
  
  numAncSd = 0;
  
  ancName   = G4String("RFD");
  ancOffset = 11000;

  myMessenger = new AgataAncillaryRFDMessenger(this,name);
}

AgataAncillaryRFD::~AgataAncillaryRFD()
{
  delete myMessenger;
}

G4int AgataAncillaryRFD::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(matMylarRFDName);
  if (ptMaterial)
    {
      matMylarRFD = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matMylarRFD->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matMylarRFDName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   

  ptMaterial = G4Material::GetMaterial(matRFDTargetChamberName);
  if (ptMaterial)
    {
      matRFDTargetChamber = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matRFDTargetChamber->GetName() << " (for RFD foils)." << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matRFDTargetChamberName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   

  ptMaterial = G4Material::GetMaterial(matRFDIonsTubeName);
  if (ptMaterial)
    {
      matRFDIonsTube = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matRFDIonsTube->GetName() << " (for ion guide)." << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matRFDIonsTubeName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   

  ptMaterial = G4Material::GetMaterial(matRFDOneConeName);
  if (ptMaterial)
    {
      matRFDOneCone = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matRFDOneCone->GetName() << " (for RFD cone)." << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matRFDOneConeName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   
  
  ptMaterial = G4Material::GetMaterial(matRFDTargetName);
  if (ptMaterial)
    {
      matRFDTarget = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matRFDTarget->GetName() << " (for RFD target)." << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matRFDTargetName << G4endl;
      G4cout << " Could not build the target! " << G4endl;
      return 1;
    }   

  return 0;
}

void AgataAncillaryRFD::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

void AgataAncillaryRFD::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryRFD::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  G4SDManager* SDman = G4SDManager::GetSDMpointer();

  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/RFD", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

void AgataAncillaryRFD::PlaceMylar()
{
  //SENSITIVE PARTS
  //  SensitivePartsAlThickness    = 200 * nm;
  SensitivePartsMylarHalfThickness = SensitivePartsMylarThickness * 0.5;
  //  SensitivePartsAlHalfThickness    = SensitivePartsAlThickness * 0.5; 

  SensitivePartsRmin         =   0.0 * mm;
  SensitivePartsPhi          =  50.0 * mm; //diameter
  SensitivePartsRmax         =   0.5 * SensitivePartsPhi;
  SensitivePartsStartAngle   =   0.0 * deg;
  SensitivePartsSegmentAngle = 360.0 * deg;

  G4double defaultRFDMylarFoilsDistanceToTarget = 1350. * mm;
  G4double ZOffset = RFDMylarFoilsDistanceToTarget - defaultRFDMylarFoilsDistanceToTarget;

  //distance to target means distance from the (0,0,0) point to the center of the sensitive part
  SensitivePartsInnerRingMylarDistanceToTarget  = 1342.5 * mm;
  SensitivePartsMiddleRingMylarDistanceToTarget = 1326.5 * mm;
  SensitivePartsOuterRingMylarDistanceToTarget  = 1321.1 * mm;

  SensitivePartsInnerRingTheta  = 2.90 * deg;
  SensitivePartsMiddleRingTheta = 5.15 * deg;
  SensitivePartsOuterRingTheta  = 5.69 * deg;
  
  ThetaIR = SensitivePartsInnerRingTheta;
  ThetaMR = SensitivePartsMiddleRingTheta;
  ThetaOR = SensitivePartsOuterRingTheta;

  SensitivePartInnerRingPhi1  =  30.0 * deg;
  SensitivePartInnerRingPhi2  =  90.0 * deg;
  SensitivePartInnerRingPhi3  = 150.0 * deg;
  SensitivePartInnerRingPhi4  = 210.0 * deg;
  SensitivePartInnerRingPhi5  = 270.0 * deg;
  SensitivePartInnerRingPhi6  = 330.0 * deg;
  SensitivePartMiddleRingPhi1 =   0.0 * deg;
  SensitivePartMiddleRingPhi2 =  60.0 * deg;
  SensitivePartMiddleRingPhi3 = 120.0 * deg;
  SensitivePartMiddleRingPhi4 = 180.0 * deg;
  SensitivePartMiddleRingPhi5 = 240.0 * deg;
  SensitivePartMiddleRingPhi6 = 300.0 * deg;
  SensitivePartOuterRingPhi1  =  30.0 * deg;
  SensitivePartOuterRingPhi2  =  90.0 * deg;
  SensitivePartOuterRingPhi3  = 150.0 * deg;
  SensitivePartOuterRingPhi4  = 210.0 * deg;
  SensitivePartOuterRingPhi5  = 270.0 * deg;
  SensitivePartOuterRingPhi6  = 330.0 * deg;

  //x = x0 -r * sin(theta)*sin(phi)
  //INNER RING X:
  SensitivePartInnerRingDet1MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi1);
  SensitivePartInnerRingDet2MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi2);
  SensitivePartInnerRingDet3MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi3);
  SensitivePartInnerRingDet4MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi4);
  SensitivePartInnerRingDet5MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi5);
  SensitivePartInnerRingDet6MylarPositionX = 
    ZeroPositionX - SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * sin(SensitivePartInnerRingPhi6);
  //MIDDLE RING X:
  SensitivePartMiddleRingDet1MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi1);
  SensitivePartMiddleRingDet2MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi2);
  SensitivePartMiddleRingDet3MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi3);
  SensitivePartMiddleRingDet4MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi4);
  SensitivePartMiddleRingDet5MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi5);
  SensitivePartMiddleRingDet6MylarPositionX = 
    ZeroPositionX - SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * sin(SensitivePartMiddleRingPhi6);
  //OUTER RING X:
  SensitivePartOuterRingDet1MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi1);
  SensitivePartOuterRingDet2MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi2);
  SensitivePartOuterRingDet3MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi3);
  SensitivePartOuterRingDet4MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi4);
  SensitivePartOuterRingDet5MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi5);
  SensitivePartOuterRingDet6MylarPositionX = 
    ZeroPositionX - SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * sin(SensitivePartOuterRingPhi6);

  //y = y0 + r * sin(theta) * cos(phi)
  //INNER RING Y:
  SensitivePartInnerRingDet1MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi1);
  SensitivePartInnerRingDet2MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi2);
  SensitivePartInnerRingDet3MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi3);
  SensitivePartInnerRingDet4MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi4);
  SensitivePartInnerRingDet5MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi5);
  SensitivePartInnerRingDet6MylarPositionY =
    ZeroPositionY + SensitivePartsInnerRingMylarDistanceToTarget * sin(ThetaIR) * cos(SensitivePartInnerRingPhi6);
  //MIDDLE RING Y:
  SensitivePartMiddleRingDet1MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi1);
  SensitivePartMiddleRingDet2MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi2);
  SensitivePartMiddleRingDet3MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi3);
  SensitivePartMiddleRingDet4MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi4);
  SensitivePartMiddleRingDet5MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi5);
  SensitivePartMiddleRingDet6MylarPositionY =
    ZeroPositionY + SensitivePartsMiddleRingMylarDistanceToTarget * sin(ThetaMR) * cos(SensitivePartMiddleRingPhi6);
  //OUTER RING Y:
  SensitivePartOuterRingDet1MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi1);
  SensitivePartOuterRingDet2MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi2);
  SensitivePartOuterRingDet3MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi3);
  SensitivePartOuterRingDet4MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi4);
  SensitivePartOuterRingDet5MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi5);
  SensitivePartOuterRingDet6MylarPositionY= 
    ZeroPositionY + SensitivePartsOuterRingMylarDistanceToTarget * sin(ThetaOR) * cos(SensitivePartOuterRingPhi6);

  //z = z0 + offset + r * cos(theta)
  //INNER RING Z:
  SensitivePartInnerRingDet1MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  SensitivePartInnerRingDet2MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  SensitivePartInnerRingDet3MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  SensitivePartInnerRingDet4MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  SensitivePartInnerRingDet5MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  SensitivePartInnerRingDet6MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsInnerRingMylarDistanceToTarget * cos(ThetaIR);
  //MIDDLE RING Z:
  SensitivePartMiddleRingDet1MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  SensitivePartMiddleRingDet2MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  SensitivePartMiddleRingDet3MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  SensitivePartMiddleRingDet4MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  SensitivePartMiddleRingDet5MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  SensitivePartMiddleRingDet6MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsMiddleRingMylarDistanceToTarget * cos(ThetaMR);
  //OUTER RING Z:
  SensitivePartOuterRingDet1MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);
  SensitivePartOuterRingDet2MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);
  SensitivePartOuterRingDet3MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);
  SensitivePartOuterRingDet4MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);
  SensitivePartOuterRingDet5MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);
  SensitivePartOuterRingDet6MylarPositionZ =
    ZeroPositionZ + ZOffset + SensitivePartsOuterRingMylarDistanceToTarget * cos(ThetaOR);

  //SOLIDS OF ALL 18 SENSITIVE PARTS

  solidSensitivePartInnerRingDet1Mylar = new G4Tubs("solidInnerRingDet1Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartInnerRingDet2Mylar = new G4Tubs("solidInnerRingDet2Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartInnerRingDet3Mylar = new G4Tubs("solidInnerRingDet3Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartInnerRingDet4Mylar = new G4Tubs("solidInnerRingDet4Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartInnerRingDet5Mylar = new G4Tubs("solidInnerRingDet5Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartInnerRingDet6Mylar = new G4Tubs("solidInnerRingDet6Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  
  solidSensitivePartMiddleRingDet1Mylar = new G4Tubs("solidMiddleRingDet1Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  solidSensitivePartMiddleRingDet2Mylar = new G4Tubs("solidMiddleRingDet2Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  solidSensitivePartMiddleRingDet3Mylar = new G4Tubs("solidMiddleRingDet3Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  solidSensitivePartMiddleRingDet4Mylar = new G4Tubs("solidMiddleRingDet4Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  solidSensitivePartMiddleRingDet5Mylar = new G4Tubs("solidMiddleRingDet5Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  solidSensitivePartMiddleRingDet6Mylar = new G4Tubs("solidMiddleRingDet6Mylar",
						     SensitivePartsRmin,
						     SensitivePartsRmax,
						     SensitivePartsMylarHalfThickness,
						     SensitivePartsStartAngle,
						     SensitivePartsSegmentAngle);
  
  solidSensitivePartOuterRingDet1Mylar = new G4Tubs("solidOuterRingDet1Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartOuterRingDet2Mylar = new G4Tubs("solidOuterRingDet2Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartOuterRingDet3Mylar = new G4Tubs("solidOuterRingDet3Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartOuterRingDet4Mylar = new G4Tubs("solidOuterRingDet4Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartOuterRingDet5Mylar = new G4Tubs("solidOuterRingDet5Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  solidSensitivePartOuterRingDet6Mylar = new G4Tubs("solidOuterRingDet6Mylar",
						    SensitivePartsRmin,
						    SensitivePartsRmax,
						    SensitivePartsMylarHalfThickness,
						    SensitivePartsStartAngle,
						    SensitivePartsSegmentAngle);
  //LOGICAL VOLUMES OF ALL 18 PARTS

  logicSensitivePartInnerRingDet1Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet1Mylar,
							     matMylarRFD,
							     "logicInnerRingDet1Mylar",
							     0,0,0);
  logicSensitivePartInnerRingDet2Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet2Mylar,
							     matMylarRFD,
							     "logicInnerRingDet2Mylar",
							     0,0,0);
  logicSensitivePartInnerRingDet3Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet3Mylar,
							     matMylarRFD,
							     "logicInnerRingDet3Mylar",
							     0,0,0);
  logicSensitivePartInnerRingDet4Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet4Mylar,
							     matMylarRFD,
							     "logicInnerRingDet4Mylar",
							     0,0,0);
  logicSensitivePartInnerRingDet5Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet5Mylar,
							     matMylarRFD,
							     "logicInnerRingDet5Mylar",
							     0,0,0);
  logicSensitivePartInnerRingDet6Mylar = new G4LogicalVolume(solidSensitivePartInnerRingDet6Mylar,
							     matMylarRFD,
							     "logicInnerRingDet6Mylar",
							     0,0,0);

  logicSensitivePartMiddleRingDet1Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet1Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet1Mylar",
							      0,0,0);
  logicSensitivePartMiddleRingDet2Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet2Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet2Mylar",
							      0,0,0);
  logicSensitivePartMiddleRingDet3Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet3Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet3Mylar",
							      0,0,0);
  logicSensitivePartMiddleRingDet4Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet4Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet4Mylar",
							      0,0,0);
  logicSensitivePartMiddleRingDet5Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet5Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet5Mylar",
							      0,0,0);
  logicSensitivePartMiddleRingDet6Mylar = new G4LogicalVolume(solidSensitivePartMiddleRingDet6Mylar,
							      matMylarRFD,
							      "logicMiddleRingDet6Mylar",
							      0,0,0);

  logicSensitivePartOuterRingDet1Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet1Mylar,
							     matMylarRFD,
							     "logicOuterRingDet1Mylar",
							     0,0,0);
  logicSensitivePartOuterRingDet2Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet2Mylar,
							     matMylarRFD,
							     "logicOuterRingDet2Mylar",
							     0,0,0);
  logicSensitivePartOuterRingDet3Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet3Mylar,
							     matMylarRFD,
							     "logicOuterRingDet3Mylar",
							     0,0,0);
  logicSensitivePartOuterRingDet4Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet4Mylar,
							     matMylarRFD,
							     "logicOuterRingDet4Mylar",
							     0,0,0);
  logicSensitivePartOuterRingDet5Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet5Mylar,
							     matMylarRFD,
							     "logicOuterRingDet5Mylar",
							     0,0,0);
  logicSensitivePartOuterRingDet6Mylar = new G4LogicalVolume(solidSensitivePartOuterRingDet6Mylar,
							     matMylarRFD,
							     "logicOuterRingDet6Mylar",
							     0,0,0);
  //rotation angles and matrixes
  RotationAngleSensitivePartInnerRing  = 15.0 * deg;
  RotationAngleSensitivePartMiddleRing = 27.0 * deg;
  RotationAngleSensitivePartOuterRing  = 30.0 * deg;
  RotationMatrixSensitivePartInnerRing.rotateZ(RotationAngleSensitivePartInnerRing);
  RotationMatrixSensitivePartMiddleRing.rotateZ(RotationAngleSensitivePartMiddleRing);
  RotationMatrixSensitivePartOuterRing.rotateZ(RotationAngleSensitivePartOuterRing);

  
  RotationAngleSensitivePartInnerRingDet1x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi1);
  RotationAngleSensitivePartInnerRingDet1y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi1);
  RotationAngleSensitivePartInnerRingDet2x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi2);
  RotationAngleSensitivePartInnerRingDet2y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi2);
  RotationAngleSensitivePartInnerRingDet3x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi3);
  RotationAngleSensitivePartInnerRingDet3y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi3);
  RotationAngleSensitivePartInnerRingDet4x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi4);
  RotationAngleSensitivePartInnerRingDet4y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi4);
  RotationAngleSensitivePartInnerRingDet5x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi5);
  RotationAngleSensitivePartInnerRingDet5y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi5);
  RotationAngleSensitivePartInnerRingDet6x = -RotationAngleSensitivePartInnerRing*cos(SensitivePartInnerRingPhi6);
  RotationAngleSensitivePartInnerRingDet6y = -RotationAngleSensitivePartInnerRing*sin(SensitivePartInnerRingPhi6);

  RotationAngleSensitivePartMiddleRingDet1x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi1);
  RotationAngleSensitivePartMiddleRingDet1y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi1);
  RotationAngleSensitivePartMiddleRingDet2x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi2);
  RotationAngleSensitivePartMiddleRingDet2y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi2);
  RotationAngleSensitivePartMiddleRingDet3x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi3);
  RotationAngleSensitivePartMiddleRingDet3y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi3);
  RotationAngleSensitivePartMiddleRingDet4x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi4);
  RotationAngleSensitivePartMiddleRingDet4y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi4);
  RotationAngleSensitivePartMiddleRingDet5x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi5);
  RotationAngleSensitivePartMiddleRingDet5y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi5);
  RotationAngleSensitivePartMiddleRingDet6x = -RotationAngleSensitivePartMiddleRing*cos(SensitivePartMiddleRingPhi6);
  RotationAngleSensitivePartMiddleRingDet6y = -RotationAngleSensitivePartMiddleRing*sin(SensitivePartMiddleRingPhi6);

  RotationAngleSensitivePartOuterRingDet1x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi1);
  RotationAngleSensitivePartOuterRingDet1y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi1);
  RotationAngleSensitivePartOuterRingDet2x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi2);
  RotationAngleSensitivePartOuterRingDet2y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi2);
  RotationAngleSensitivePartOuterRingDet3x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi3);
  RotationAngleSensitivePartOuterRingDet3y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi3);
  RotationAngleSensitivePartOuterRingDet4x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi4);
  RotationAngleSensitivePartOuterRingDet4y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi4);
  RotationAngleSensitivePartOuterRingDet5x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi5);
  RotationAngleSensitivePartOuterRingDet5y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi5);
  RotationAngleSensitivePartOuterRingDet6x = -RotationAngleSensitivePartOuterRing*cos(SensitivePartOuterRingPhi6);
  RotationAngleSensitivePartOuterRingDet6y = -RotationAngleSensitivePartOuterRing*sin(SensitivePartOuterRingPhi6);

  RotationMatrixSensitivePartInnerRingDet1.rotateX(RotationAngleSensitivePartInnerRingDet1x);
  RotationMatrixSensitivePartInnerRingDet1.rotateY(RotationAngleSensitivePartInnerRingDet1y);
  RotationMatrixSensitivePartInnerRingDet2.rotateX(RotationAngleSensitivePartInnerRingDet2x);
  RotationMatrixSensitivePartInnerRingDet2.rotateY(RotationAngleSensitivePartInnerRingDet2y);
  RotationMatrixSensitivePartInnerRingDet3.rotateX(RotationAngleSensitivePartInnerRingDet3x);
  RotationMatrixSensitivePartInnerRingDet3.rotateY(RotationAngleSensitivePartInnerRingDet3y);
  RotationMatrixSensitivePartInnerRingDet4.rotateX(RotationAngleSensitivePartInnerRingDet4x);
  RotationMatrixSensitivePartInnerRingDet4.rotateY(RotationAngleSensitivePartInnerRingDet4y);
  RotationMatrixSensitivePartInnerRingDet5.rotateX(RotationAngleSensitivePartInnerRingDet5x);
  RotationMatrixSensitivePartInnerRingDet5.rotateY(RotationAngleSensitivePartInnerRingDet5y);
  RotationMatrixSensitivePartInnerRingDet6.rotateX(RotationAngleSensitivePartInnerRingDet6x);
  RotationMatrixSensitivePartInnerRingDet6.rotateY(RotationAngleSensitivePartInnerRingDet6y);

  RotationMatrixSensitivePartMiddleRingDet1.rotateX(RotationAngleSensitivePartMiddleRingDet1x);
  RotationMatrixSensitivePartMiddleRingDet1.rotateY(RotationAngleSensitivePartMiddleRingDet1y);
  RotationMatrixSensitivePartMiddleRingDet2.rotateX(RotationAngleSensitivePartMiddleRingDet2x);
  RotationMatrixSensitivePartMiddleRingDet2.rotateY(RotationAngleSensitivePartMiddleRingDet2y);
  RotationMatrixSensitivePartMiddleRingDet3.rotateX(RotationAngleSensitivePartMiddleRingDet3x);
  RotationMatrixSensitivePartMiddleRingDet3.rotateY(RotationAngleSensitivePartMiddleRingDet3y);
  RotationMatrixSensitivePartMiddleRingDet4.rotateX(RotationAngleSensitivePartMiddleRingDet4x);
  RotationMatrixSensitivePartMiddleRingDet4.rotateY(RotationAngleSensitivePartMiddleRingDet4y);
  RotationMatrixSensitivePartMiddleRingDet5.rotateX(RotationAngleSensitivePartMiddleRingDet5x);
  RotationMatrixSensitivePartMiddleRingDet5.rotateY(RotationAngleSensitivePartMiddleRingDet5y);
  RotationMatrixSensitivePartMiddleRingDet6.rotateX(RotationAngleSensitivePartMiddleRingDet6x);
  RotationMatrixSensitivePartMiddleRingDet6.rotateY(RotationAngleSensitivePartMiddleRingDet6y);

  RotationMatrixSensitivePartOuterRingDet1.rotateX(RotationAngleSensitivePartOuterRingDet1x);
  RotationMatrixSensitivePartOuterRingDet1.rotateY(RotationAngleSensitivePartOuterRingDet1y);
  RotationMatrixSensitivePartOuterRingDet2.rotateX(RotationAngleSensitivePartOuterRingDet2x);
  RotationMatrixSensitivePartOuterRingDet2.rotateY(RotationAngleSensitivePartOuterRingDet2y);
  RotationMatrixSensitivePartOuterRingDet3.rotateX(RotationAngleSensitivePartOuterRingDet3x);
  RotationMatrixSensitivePartOuterRingDet3.rotateY(RotationAngleSensitivePartOuterRingDet3y);
  RotationMatrixSensitivePartOuterRingDet4.rotateX(RotationAngleSensitivePartOuterRingDet4x);
  RotationMatrixSensitivePartOuterRingDet4.rotateY(RotationAngleSensitivePartOuterRingDet4y);
  RotationMatrixSensitivePartOuterRingDet5.rotateX(RotationAngleSensitivePartOuterRingDet5x);
  RotationMatrixSensitivePartOuterRingDet5.rotateY(RotationAngleSensitivePartOuterRingDet5y);
  RotationMatrixSensitivePartOuterRingDet6.rotateX(RotationAngleSensitivePartOuterRingDet6x);
  RotationMatrixSensitivePartOuterRingDet6.rotateY(RotationAngleSensitivePartOuterRingDet6y);

  //PHYSICAL VOLUMES OF ALL 18 SENSITIVE PARTS
  physSensitivePartInnerRingDet1Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet1,
									G4ThreeVector(SensitivePartInnerRingDet1MylarPositionX,
										      SensitivePartInnerRingDet1MylarPositionY,
										      SensitivePartInnerRingDet1MylarPositionZ)),
							  "physSensitivePartInnerRingDet1Mylar",
							  logicSensitivePartInnerRingDet1Mylar,
							  theDetector->HallPhys(), false, 3);  
  physSensitivePartInnerRingDet2Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet2, 
									G4ThreeVector(SensitivePartInnerRingDet2MylarPositionX,
										      SensitivePartInnerRingDet2MylarPositionY,
										      SensitivePartInnerRingDet2MylarPositionZ)),
							  "physSensitivePartInnerRingDet2Mylar",
							  logicSensitivePartInnerRingDet2Mylar,
							  theDetector->HallPhys(), false, 4);
  physSensitivePartInnerRingDet3Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet3, 
									G4ThreeVector(SensitivePartInnerRingDet3MylarPositionX,
										      SensitivePartInnerRingDet3MylarPositionY,
										      SensitivePartInnerRingDet3MylarPositionZ)),
							  "physSensitivePartInnerRingDet3Mylar",
							  logicSensitivePartInnerRingDet3Mylar,
							  theDetector->HallPhys(), false, 5);
  physSensitivePartInnerRingDet4Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet4, 
									G4ThreeVector(SensitivePartInnerRingDet4MylarPositionX,
										      SensitivePartInnerRingDet4MylarPositionY,
										      SensitivePartInnerRingDet4MylarPositionZ)),
							  "physSensitivePartInnerRingDet4Mylar",
							  logicSensitivePartInnerRingDet4Mylar,
							  theDetector->HallPhys(), false, 6);
  physSensitivePartInnerRingDet5Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet5, 
									G4ThreeVector(SensitivePartInnerRingDet5MylarPositionX,
										      SensitivePartInnerRingDet5MylarPositionY,
										      SensitivePartInnerRingDet5MylarPositionZ)),
							  "physSensitivePartInnerRingDet5Mylar",
							  logicSensitivePartInnerRingDet5Mylar,
							  theDetector->HallPhys(), false, 1);
  physSensitivePartInnerRingDet6Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartInnerRingDet6, 
									G4ThreeVector(SensitivePartInnerRingDet6MylarPositionX,
										      SensitivePartInnerRingDet6MylarPositionY,
										      SensitivePartInnerRingDet6MylarPositionZ)),
							  "physSensitivePartInnerRingDet6Mylar",
							  logicSensitivePartInnerRingDet6Mylar,
							  theDetector->HallPhys(), false, 2);

  physSensitivePartMiddleRingDet1Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet1,
									 G4ThreeVector(SensitivePartMiddleRingDet1MylarPositionX,
										       SensitivePartMiddleRingDet1MylarPositionY,
										       SensitivePartMiddleRingDet1MylarPositionZ)),
							   "physSensitivePartMiddleRingDet1Mylar",
							   logicSensitivePartMiddleRingDet1Mylar,
							   theDetector->HallPhys(), false, 10);
  physSensitivePartMiddleRingDet2Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet2,
									 G4ThreeVector(SensitivePartMiddleRingDet2MylarPositionX,
										       SensitivePartMiddleRingDet2MylarPositionY,
										       SensitivePartMiddleRingDet2MylarPositionZ)),
							   "physSensitivePartMiddleRingDet2Mylar",
							   logicSensitivePartMiddleRingDet2Mylar,
							   theDetector->HallPhys(), false, 12);
  physSensitivePartMiddleRingDet3Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet3,
									 G4ThreeVector(SensitivePartMiddleRingDet3MylarPositionX,
										       SensitivePartMiddleRingDet3MylarPositionY,
										       SensitivePartMiddleRingDet3MylarPositionZ)),
							   "physSensitivePartMiddleRingDet3Mylar",
							   logicSensitivePartMiddleRingDet3Mylar,
							   theDetector->HallPhys(), false, 14);
  physSensitivePartMiddleRingDet4Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet4,
									 G4ThreeVector(SensitivePartMiddleRingDet4MylarPositionX,
										       SensitivePartMiddleRingDet4MylarPositionY,
										       SensitivePartMiddleRingDet4MylarPositionZ)),
							   "physSensitivePartMiddleRingDet4Mylar",
							   logicSensitivePartMiddleRingDet4Mylar,
							   theDetector->HallPhys(), false, 16);
  physSensitivePartMiddleRingDet5Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet5,
									 G4ThreeVector(SensitivePartMiddleRingDet5MylarPositionX,
										       SensitivePartMiddleRingDet5MylarPositionY,
										       SensitivePartMiddleRingDet5MylarPositionZ)),
							   "physSensitivePartMiddleRingDet5Mylar",
							   logicSensitivePartMiddleRingDet5Mylar,
							   theDetector->HallPhys(), false, 18);
  physSensitivePartMiddleRingDet6Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartMiddleRingDet6,
									 G4ThreeVector(SensitivePartMiddleRingDet6MylarPositionX,
										       SensitivePartMiddleRingDet6MylarPositionY,
										       SensitivePartMiddleRingDet6MylarPositionZ)),
							   "physSensitivePartMiddleRingDet6Mylar",
							   logicSensitivePartMiddleRingDet6Mylar,
							   theDetector->HallPhys(), false, 8);

  physSensitivePartOuterRingDet1Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet1,
									G4ThreeVector(SensitivePartOuterRingDet1MylarPositionX,
										      SensitivePartOuterRingDet1MylarPositionY,
										      SensitivePartOuterRingDet1MylarPositionZ)),
							  "physSensitivePartOuterRingDet1Mylar",
							  logicSensitivePartOuterRingDet1Mylar,
							  theDetector->HallPhys(), false, 11);
  physSensitivePartOuterRingDet2Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet2,
									G4ThreeVector(SensitivePartOuterRingDet2MylarPositionX,
										      SensitivePartOuterRingDet2MylarPositionY,
										      SensitivePartOuterRingDet2MylarPositionZ)),
							  "physSensitivePartOuterRingDet2Mylar",
							  logicSensitivePartOuterRingDet2Mylar,
							  theDetector->HallPhys(), false, 13);
  physSensitivePartOuterRingDet3Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet3,
									G4ThreeVector(SensitivePartOuterRingDet3MylarPositionX,
										      SensitivePartOuterRingDet3MylarPositionY,
										      SensitivePartOuterRingDet3MylarPositionZ)),
							  "physSensitivePartOuterRingDet3Mylar",
							  logicSensitivePartOuterRingDet3Mylar,
							  theDetector->HallPhys(), false, 15);
  physSensitivePartOuterRingDet4Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet4,
									G4ThreeVector(SensitivePartOuterRingDet4MylarPositionX,
										      SensitivePartOuterRingDet4MylarPositionY,
										      SensitivePartOuterRingDet4MylarPositionZ)),
							  "physSensitivePartOuterRingDet4Mylar",
							  logicSensitivePartOuterRingDet4Mylar,
							  theDetector->HallPhys(), false, 17);
  physSensitivePartOuterRingDet5Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet5,
									G4ThreeVector(SensitivePartOuterRingDet5MylarPositionX,
										      SensitivePartOuterRingDet5MylarPositionY,
										      SensitivePartOuterRingDet5MylarPositionZ)),
							  "physSensitivePartOuterRingDet5Mylar",
							  logicSensitivePartOuterRingDet5Mylar,
							  theDetector->HallPhys(), false, 7);
  physSensitivePartOuterRingDet6Mylar = new G4PVPlacement(G4Transform3D(RotationMatrixSensitivePartOuterRingDet6,
									G4ThreeVector(SensitivePartOuterRingDet6MylarPositionX,
										      SensitivePartOuterRingDet6MylarPositionY,
										      SensitivePartOuterRingDet6MylarPositionZ)),
							  "physSensitivePartOuterRingDet6Mylar",
							  logicSensitivePartOuterRingDet6Mylar,
							  theDetector->HallPhys(), false, 9);

  logicSensitivePartInnerRingDet1Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartInnerRingDet2Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartInnerRingDet3Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartInnerRingDet4Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartInnerRingDet5Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartInnerRingDet6Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet1Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet2Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet3Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet4Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet5Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartMiddleRingDet6Mylar -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet1Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet2Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet3Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet4Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet5Mylar  -> SetSensitiveDetector(ancSD);
  logicSensitivePartOuterRingDet6Mylar  -> SetSensitiveDetector(ancSD);

  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));//zielony
  zielony->SetVisibility(true);

  logicSensitivePartInnerRingDet1Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartInnerRingDet2Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartInnerRingDet3Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartInnerRingDet4Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartInnerRingDet5Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartInnerRingDet6Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet1Mylar -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet2Mylar -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet3Mylar -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet4Mylar -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet5Mylar -> SetVisAttributes(zielony);
  logicSensitivePartMiddleRingDet6Mylar -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet1Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet2Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet3Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet4Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet5Mylar  -> SetVisAttributes(zielony);
  logicSensitivePartOuterRingDet6Mylar  -> SetVisAttributes(zielony);

}

//this method isnt working at all
G4int AgataAncillaryRFD::GetSegmentNumber(G4int offset, G4int detCode, G4ThreeVector position)
{
  G4int ring = 0;
  G4int sth;
  //i don't like warnings
  sth = offset;
  sth = detCode;
  sth = sth;
  if((position.z() < 111350.)&&(position.z() > -111330.)) ring=1;// 1340.78
  if((position.z() <  620.)&&(position.z() >  500.)) ring=2;// 1321.15
  if((position.z() < 1150.)&&(position.z() > 1040.)) ring=3;// 1314.59
  if((position.z() <   7.)&&(position.z() > -7.   )) ring=4;
  return (10*ring);
  //  return detCode/100;
}


void AgataAncillaryRFD::PlaceIonGuide()
{
  //  RFDIonGuide_Length     default =  0.5 *  m
  //  RFDIonGuide_Phi        default =  2.  * mm
  //  RFDIonGuide_Thickness  default = 40.  * mm

  G4double RFDIonsTube_Rmin        =  RFDIonGuide_Phi * 0.5; 
  G4double RFDIonsTube_Rmax        =  RFDIonGuide_Phi * 0.5 + RFDIonGuide_Thickness;
  G4double RFDIonsTube_offset      =  RFDTargetChamber_Phi/2. * sqrt( 1 - pow( (RFDIonGuide_Phi + 2*RFDIonGuide_Thickness)/RFDTargetChamber_Phi ,2. )  );

  G4double RFDIonsTube_HalfLength  =  RFDIonGuide_Length * 0.5;
  G4double RFDIonsTube_PosX        =  ZeroPositionX;
  G4double RFDIonsTube_PosY        =  ZeroPositionY;
  G4double RFDIonsTube_PosZ        =  ZeroPositionZ - RFDIonsTube_offset -  RFDIonsTube_HalfLength;

  G4double RFDIonsTube_StAngle     =    0. * deg;
  G4double RFDIonsTube_SegAngle    =  360. * deg;

  G4RotationMatrix rmRFDIonsTube;
  rmRFDIonsTube.set(0, 0, 0);


  G4Tubs* solidRFDIonsTube = new G4Tubs("RFDIonsTube",
					RFDIonsTube_Rmin,
					RFDIonsTube_Rmax,
					RFDIonsTube_HalfLength,
					RFDIonsTube_StAngle,
					RFDIonsTube_SegAngle);
  
  G4LogicalVolume* logicRFDIonsTube = new G4LogicalVolume(solidRFDIonsTube,
							  matRFDIonsTube,
							  "RFDIonsTube",
							  0,0,0);
  
  //  G4VPhysicalVolume* physRFDIonsTube ;
  /*physRFDIonsTube =*/
  new G4PVPlacement(G4Transform3D(rmRFDIonsTube,
				  G4ThreeVector(RFDIonsTube_PosX,
						RFDIonsTube_PosY,
						RFDIonsTube_PosZ)),
		    "RFDIonsTube",
		    logicRFDIonsTube,
		    theDetector->HallPhys(), true, 60);
  
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  czerwony->SetVisibility(true);
  
  G4VisAttributes* bialy = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  bialy->SetVisibility(true);
  
  logicRFDIonsTube->SetVisAttributes(bialy);
}

void AgataAncillaryRFD::PlaceRFDCones()
{
  //G4double RFDSmallCone_Length       =  417.7  * mm;
  //G4double RFDSmallCone_TopAngle     =    7.   * deg;
  //G4double RFDSmallCone_InnerPhiSt   =   31.74 * mm;
  //G4double RFDSmallCone_InnerPhiEnd  =  118.   * mm;
  //G4double RFDLargeCone_Length       =  506.8  * mm;
  //G4double RFDLargeCone_InnerPhiSt   =  118.   * mm;
  //G4double RFDLargeCone_TopAngle     =    7.   * deg;
  //G4double RFDLargeCone_InnerPhiEnd  =  242.5  * mm; 
  //G4double RFDOneCone_Thickness   =    2.   * mm;
  //G4double RFDOneCone_InnerPhiEnd =  RFDLargeCone_InnerPhiEnd;
  //G4double RFDOneCone_Length      =  RFDSmallCone_Length + RFDLargeCone_Length;

  G4double RFDOneCone_Length      =  RFDMylarFoilsDistanceToTarget - RFDTargetChamber_Phi/2 - RFDTargetChamber_Thickness - LengthOfBellow - LengthOfBaniak;

  G4double RFDOneCone_HalfLength  =  RFDOneCone_Length * 0.5;
  G4double RFDOneCone_StRmin      =  RFDOneCone_InnerPhiSt * 0.5;
  G4double RFDOneCone_StRmax      =  RFDOneCone_StRmin + RFDOneCone_Thickness;
  
  G4double RFDOneCone_EndRmin     =  RFDOneCone_InnerPhiEnd * 0.5;
  G4double RFDOneCone_EndRmax     =  RFDOneCone_EndRmin + RFDOneCone_Thickness;
    
  G4double RFDOneCone_StAngle     =   0. * deg;
  G4double RFDOneCone_SegAngle    = 360. * deg;

  G4double RFDOneCone_PosX        = ZeroPositionX;
  G4double RFDOneCone_PosY        = ZeroPositionY;
  G4double RFDOneCone_PosZ        = ZeroPositionZ + RFDTargetChamber_Phi/2 + RFDTargetChamber_Thickness + LengthOfBellow + RFDOneCone_HalfLength;

  G4RotationMatrix rmRFDOneCone;
  rmRFDOneCone.set(0, 0, 0);

  G4Cons* solidRFDOneCone = new G4Cons("RFDOneCone",
				       RFDOneCone_StRmin,
				       RFDOneCone_StRmax,
				       RFDOneCone_EndRmin,
				       RFDOneCone_EndRmax,
				       RFDOneCone_HalfLength,
				       RFDOneCone_StAngle,
				       RFDOneCone_SegAngle);

  G4LogicalVolume* logicRFDOneCone = new G4LogicalVolume(solidRFDOneCone,
							 matRFDOneCone,
							 "RFDOneCone",
							 0,0,0);

  //  G4VPhysicalVolume* physRFDOneCone;
  //physRFDOneCone =
  new G4PVPlacement(G4Transform3D(rmRFDOneCone,
				  G4ThreeVector(RFDOneCone_PosX,
						RFDOneCone_PosY,
						RFDOneCone_PosZ)),
		    "RFDOneCone",
		    logicRFDOneCone,
		    theDetector->HallPhys(), false, 40);
  
  G4VisAttributes* bialy = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  bialy->SetVisibility(true);

  logicRFDOneCone->SetVisAttributes(bialy);

}

void AgataAncillaryRFD::PlaceTargetChamber()
{
  G4double RFDTargetChamber_Rmin   = RFDTargetChamber_Phi * 0.5;
  G4double RFDTargetChamber_Rmax   = RFDTargetChamber_Rmin + RFDTargetChamber_Thickness;
  G4double RFDTargetChamber_SPhi   =   0. * deg;
  G4double RFDTargetChamber_DPhi   = 360. * deg;
  
  G4double RFDTargetChamberHoleAngle4RFDCone = asin( (RFDOneCone_InnerPhiSt/2/mm) /  (RFDTargetChamber_Rmin/mm) ); // /M_PI * 180. * deg;


  G4double RFDTargetChamber_STheta = RFDTargetChamberHoleAngle4RFDCone;
  G4double RFDTargetChamber_DTheta;

  G4double RFDTargetChamberHoleAngle4IonGuide = asin( (((RFDIonGuide_Phi/mm)+ 2.*(RFDIonGuide_Thickness/mm))/2.) / (RFDTargetChamber_Rmin/mm) ) ; // / M_PI * 180.*deg;

  G4RotationMatrix rmRFDTargetChamber;
  rmRFDTargetChamber.set(0, 0, 0);

  RFDTargetChamber_DTheta = 180.*deg - RFDTargetChamberHoleAngle4RFDCone - RFDTargetChamberHoleAngle4IonGuide;
  

  G4Sphere* solidRFDTargetChamber = new G4Sphere("RFDTargetChamber",
						 RFDTargetChamber_Rmin,
						 RFDTargetChamber_Rmax,
						 RFDTargetChamber_SPhi,
						 RFDTargetChamber_DPhi,
						 RFDTargetChamber_STheta,
						 RFDTargetChamber_DTheta);
  
  G4LogicalVolume* logicRFDTargetChamber = new G4LogicalVolume(solidRFDTargetChamber,
							       matRFDTargetChamber,
							       "RFDTargetChamber",
							       0,0,0);

  //  G4VPhysicalVolume* physRFDTargetChamber;
  //  physRFDTargetChamber =
  new G4PVPlacement(G4Transform3D(rmRFDTargetChamber,
				  G4ThreeVector(ZeroPositionX,
						ZeroPositionY,
						ZeroPositionZ)),
		    "RFDTargetChamber",
		    logicRFDTargetChamber,
		    theDetector->HallPhys(), true, 50);
  //****************
  // visualization *
  //****************

  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));
  zielony->SetVisibility(true); //green
  G4VisAttributes* Niewidzialny = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  Niewidzialny->SetVisibility(false); //unvisible
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  czerwony->SetVisibility(true); //red
  G4VisAttributes* bialy = new G4VisAttributes( G4Colour(255/255. ,255/255. ,255/255. ));
  bialy->SetVisibility(true); //white

  logicRFDTargetChamber->SetVisAttributes(bialy);
}

void AgataAncillaryRFD::PlaceTarget()
{
  G4double RFDTarget_Rmin        =    0.  * mm;
  G4double RFDTarget_Rmax        =    0.5 * RFDTarget_Diameter;
  G4double RFDTarget_HalfThick   =    0.5 * RFDTarget_Thick;
  G4double RFDTarget_PosX        =    0.  * mm;
  G4double RFDTarget_PosY        =    0.  * mm;
  G4double RFDTarget_PosZ        =    0.  * mm + RFDTarget_HalfThick + RFDTarget_Offset;
  G4double RFDTarget_StAngle     =    0.  * deg;
  G4double RFDTarget_SegAngle    =  360.  * deg;

  G4RotationMatrix rmRFDTarget;
  rmRFDTarget.set(0, 0, 0);


  G4Tubs* solidRFDTarget = new G4Tubs("RFDTarget",RFDTarget_Rmin,RFDTarget_Rmax,
				      RFDTarget_HalfThick,RFDTarget_StAngle,RFDTarget_SegAngle);
  
  G4LogicalVolume* logicRFDTarget = new G4LogicalVolume(solidRFDTarget,matRFDTarget,
							"RFDTarget",0,0,0);
  
  //  G4VPhysicalVolume* physRFDTarget;
  /*physRFDTarget = */
  new G4PVPlacement(G4Transform3D(rmRFDTarget,G4ThreeVector(RFDTarget_PosX,
							    RFDTarget_PosY,
							    RFDTarget_PosZ)),
		    "RFDTarget", logicRFDTarget,
		    theDetector->HallPhys(), true, 55);
  
  G4VisAttributes* czerwony =
    new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));
  czerwony->SetVisibility(true);
  logicRFDTarget -> SetVisAttributes(czerwony);
}


void AgataAncillaryRFD::Placement()
{
  PlaceMylar();

  if(useIonGuide)
    PlaceIonGuide();
  if(useRFDCones)
    PlaceRFDCones();
  if(useTargetChamber)
    PlaceTargetChamber();
  if(useTarget)
    PlaceTarget();
  return;
}

void AgataAncillaryRFD::ShowStatus()
{
  G4cout << " RFD has been constructed." << G4endl;
  G4cout << " RFD material is " << matMylarRFD->GetName() << "." << G4endl;
  if( useIonGuide )
    G4cout << " Ion guide  has been defined." << G4endl;
  else
    G4cout << " No ion guide has been defined." << G4endl;  
  if( useRFDCones )
    G4cout << " RFD cones have been defined." << G4endl;
  else
    G4cout << " No RFD cones have been defined." << G4endl;  
  if( useTargetChamber )
    G4cout << " Target chamber has been defined." << G4endl;
  else
    G4cout << " No target chamber has been defined." << G4endl;  

}

void AgataAncillaryRFD::SetUseIonGuide( G4bool value )
{
  useIonGuide = value;
  if( useIonGuide )
    G4cout << " ----> The ion guide will be generated." << G4endl;
  else  
    G4cout << " ----> The ion guide will not be generated." << G4endl;
}

void AgataAncillaryRFD::SetUseRFDCones( G4bool value )
{
  useRFDCones = value;
  if( useRFDCones )
    G4cout << " ----> The RFD cones will be generated." << G4endl;
  else  
    G4cout << " ----> The RFD cones will not be generated." << G4endl;
}

void AgataAncillaryRFD::SetUseTargetChamber( G4bool value )
{
  useTargetChamber = value;
  if( useTargetChamber )
    G4cout << " ----> Target chamber will be generated." << G4endl;
  else  
    G4cout << " ----> Target chamber will not be generated." << G4endl;
}

void AgataAncillaryRFD::SetTargetChamberDiameter(G4double srednica)
{
  if(srednica < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDTargetChamber_Phi/mm << " mm)" << G4endl;
  else if(srednica < (RFDIonGuide_Phi + 2 * RFDIonGuide_Thickness))
    G4cout << "Warning!! Your value is not permitted. Diameter of the target chamber can't be smaller then amount of ion guide diameter and double thickness.Keeping previous value (" << RFDTargetChamber_Phi/mm << " mm)" << G4endl;  
  else
    {
      if( tan( 7.*deg) > (RFDOneCone_InnerPhiSt / (sqrt(pow(srednica,2.) - pow(RFDOneCone_InnerPhiSt,2.))+2.*LengthOfBellow)))
	G4cout << "Look out!!! This value of target chamber diameter decreases solid angle of RFD detector!!!" <<G4endl;
      if( srednica + 2. * RFDTargetChamber_Thickness > 460. * mm)
	G4cout << "Look out!!! Your target chamber is overlaping AGATA!!!" << G4endl;
      RFDTargetChamber_Phi = srednica;
    }
}

void AgataAncillaryRFD::SetTargetChamberThickness(G4double grubosc)
{
  if(grubosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDTargetChamber_Thickness/mm << " mm)" << G4endl;
  else
    {
      if( RFDTargetChamber_Phi + 2. * grubosc > 460. * mm)
	G4cout << "Look out!!! Your target chamber is overlaping AGATA!!!" << G4endl;
      RFDTargetChamber_Thickness = grubosc;
    }
}

void AgataAncillaryRFD::SetIonGuideLength(G4double dlugosc)
{
  if(dlugosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDIonGuide_Length/cm << " cm)" << G4endl;
  else
    RFDIonGuide_Length = dlugosc;
}

void AgataAncillaryRFD::SetIonGuideThickness(G4double grubosc)
{
  if(grubosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDIonGuide_Thickness/mm << " mm)" << G4endl;
  else
    RFDIonGuide_Thickness = grubosc;
}

void AgataAncillaryRFD::SetIonGuideInnerDiameter(G4double srednica)
{
  if(srednica < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDIonGuide_Phi/mm << " mm)" << G4endl;
  else
    RFDIonGuide_Phi = srednica;
  //mozna by dodac ostrzezenie, ze sie nie miesci w dziure agatowa =)
}

void AgataAncillaryRFD::SetRFDMylarFoilThickness(G4double grubosc)
{
  if(grubosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << SensitivePartsMylarThickness/mm << " mm)" << G4endl;
  else
    SensitivePartsMylarThickness = grubosc;
}

void AgataAncillaryRFD::SetRFDMylarFoilsDistanceToTarget(G4double odleglosc)
{
  if( odleglosc < (LengthOfBellow + LengthOfBaniak + RFDTargetChamber_Phi/2))
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDMylarFoilsDistanceToTarget/mm << " mm)" << G4endl;
  else
    {
      if(tan(7.*deg) > ((RFDOneCone_InnerPhiEnd /2 )/ (odleglosc - LengthOfBaniak) ))
	G4cout << "Look out!!! This value decreases solid angle of RFD detector!!!" <<G4endl;
      RFDMylarFoilsDistanceToTarget = odleglosc;
    }
}

void AgataAncillaryRFD::SetLengthOfBellow(G4double dlugosc)
{
  if(dlugosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << LengthOfBellow/mm << " mm)" << G4endl;
  else if(dlugosc > (RFDMylarFoilsDistanceToTarget - LengthOfBaniak -  RFDTargetChamber_Phi/2 - RFDTargetChamber_Thickness))
    G4cout << "Warning!! Your value is not permitted - bellow can't be so long. Keeping previous value (" << LengthOfBellow/mm << " mm)" << G4endl;
  else
    {
      if(tan(7.*deg)>(RFDOneCone_InnerPhiSt/(sqrt(pow(RFDTargetChamber_Phi,2.)-pow(RFDOneCone_InnerPhiSt,2.)) +  2. * dlugosc)))
	G4cout << "Look out!!! This value decreases solid angle of RFD detector!!!" << G4endl;
      LengthOfBellow = dlugosc;
    }
}

void AgataAncillaryRFD::SetUseTarget(G4bool value)
{
  useTarget = value;
  if(useTarget)
    G4cout << " ----> Target will be generated." << G4endl;
  else  
    G4cout << " ----> No target has been defined." << G4endl;
}
void AgataAncillaryRFD::SetRFDTargetDiameter(G4double srednica)
{
  if(srednica < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDTarget_Diameter/mm << " mm)" << G4endl;
  else if (!(srednica<RFDTargetChamber_Phi))
    {
      G4cout << "Warning!! Target diameter can't be major then target chamber diameter." << G4endl;
      G4cout << "Keeping previous value (" << RFDTarget_Diameter/mm << " mm)" << G4endl;
    }
  else
    RFDTarget_Diameter = srednica;
}

void AgataAncillaryRFD::SetRFDTargetThickness(G4double grubosc)
{
  if(grubosc < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDTarget_Thick/mm << " mm)" << G4endl;
  else
    RFDTarget_Thick = grubosc;
}

void AgataAncillaryRFD::SetRFDTargetMaterial( G4String material )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( material );
  
  if( ptMaterial ) {
    matRFDTargetName = material;
    G4cout << " ----> Target material has been set to " << matRFDTargetName << G4endl;
  }
  else {
    G4cout << " Could not change material, keeping previous material ("
           << matRFDTarget->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryRFD::SetRFDTargetOffset(G4double przesuw)
{
  if(przesuw < 0.)
    G4cout << "Warning!! Your value is not permitted. Keeping previous value (" << RFDTarget_Offset/mm << " mm)" << G4endl;
  else
    RFDTarget_Offset = przesuw;
}


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include  "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillaryRFDMessenger::AgataAncillaryRFDMessenger(AgataAncillaryRFD* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/ancillary/RFD/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of RFD detector construction.");  
 
  // ION GUIDE
  commandName = directoryName + "enableIonGuide";
  aLine = commandName.c_str(); 
  enableIonGuide = new G4UIcmdWithABool(commandName, this);
  enableIonGuide->SetGuidance("Creating ion guide.");
  enableIonGuide->SetGuidance("Required parameters: none.");
  enableIonGuide->SetParameterName("useIonGuide",true);
  enableIonGuide->SetDefaultValue(true);
  enableIonGuide->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableIonGuide";
  aLine = commandName.c_str(); 
  disableIonGuide = new G4UIcmdWithABool(commandName, this);
  disableIonGuide->SetGuidance("Ion guide will not be generated.");
  disableIonGuide->SetGuidance("Required parameters: none.");
  disableIonGuide->SetParameterName("useIonGuide",true);
  disableIonGuide->SetDefaultValue(false);
  disableIonGuide->AvailableForStates(G4State_PreInit,G4State_Idle);

  // RFD CONES
  commandName = directoryName + "enableRFDCones";
  aLine = commandName.c_str(); 
  enableRFDCones = new G4UIcmdWithABool(commandName, this);
  enableRFDCones->SetGuidance("Creating RFD cones.");
  enableRFDCones->SetGuidance("Required parameters: none.");
  enableRFDCones->SetParameterName("useRFDCones",true);
  enableRFDCones->SetDefaultValue(true);
  enableRFDCones->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableRFDCones";
  aLine = commandName.c_str(); 
  disableRFDCones = new G4UIcmdWithABool(commandName, this);
  disableRFDCones->SetGuidance("No RFD cones will be generated.");
  disableRFDCones->SetGuidance("Required parameters: none.");
  disableRFDCones->SetParameterName("useRFDCones",true);
  disableRFDCones->SetDefaultValue(false);
  disableRFDCones->AvailableForStates(G4State_PreInit,G4State_Idle);

  // TARGET CHAMBER
  commandName = directoryName + "enableTargetChamber";
  aLine = commandName.c_str(); 
  enableTargetChamber = new G4UIcmdWithABool(commandName, this);
  enableTargetChamber->SetGuidance("Creating target chamber.");
  enableTargetChamber->SetGuidance("Required parameters: none.");
  enableTargetChamber->SetParameterName("useTargetChamber",true);
  enableTargetChamber->SetDefaultValue(true);
  enableTargetChamber->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableTargetChamber";
  aLine = commandName.c_str(); 
  disableTargetChamber = new G4UIcmdWithABool(commandName, this);
  disableTargetChamber->SetGuidance("No target chamber will be generated.");
  disableTargetChamber->SetGuidance("Required parameters: none.");
  disableTargetChamber->SetParameterName("useTargetChamber",true);
  disableTargetChamber->SetDefaultValue(false);
  disableTargetChamber->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "TargetChamberDiameter";
  aLine = commandName.c_str(); 
  TargetChamberDiameter =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  TargetChamberDiameter -> SetGuidance("Sets inner diameter of target chamber.");
  TargetChamberDiameter -> SetParameterName("Size",false);
  TargetChamberDiameter -> SetRange("Size>0.");
  TargetChamberDiameter -> SetUnitCategory("Length");
  TargetChamberDiameter -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "TargetChamberThickness";
  aLine = commandName.c_str(); 
  TargetChamberThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  TargetChamberThickness -> SetGuidance("Sets target chamber thickness.");
  TargetChamberThickness -> SetParameterName("Size",false);
  TargetChamberThickness -> SetRange("Size>0.");
  TargetChamberThickness -> SetUnitCategory("Length");
  TargetChamberThickness -> AvailableForStates(G4State_Idle);

  // ION GUIDE 
  commandName = directoryName + "IonGuideLength";
  aLine = commandName.c_str(); 
  IonGuideLength =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  IonGuideLength -> SetGuidance("Sets ion guide length.");
  IonGuideLength -> SetParameterName("Size",false);
  IonGuideLength -> SetRange("Size>0.");
  IonGuideLength -> SetUnitCategory("Length");
  IonGuideLength -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "IonGuideThickness";
  aLine = commandName.c_str(); 
  IonGuideThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  IonGuideThickness -> SetGuidance("Sets ion guide thickness.");
  IonGuideThickness -> SetParameterName("Size",false);
  IonGuideThickness -> SetRange("Size>0.");
  IonGuideThickness -> SetUnitCategory("Length");
  IonGuideThickness -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "IonGuideInnerDiameter";
  aLine = commandName.c_str(); 
  IonGuideInnerDiameter =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  IonGuideInnerDiameter -> SetGuidance("Sets ion guide inner diameter.");
  IonGuideInnerDiameter -> SetParameterName("Size",false);
  IonGuideInnerDiameter -> SetRange("Size>0.");
  IonGuideInnerDiameter -> SetUnitCategory("Length");
  IonGuideInnerDiameter -> AvailableForStates(G4State_Idle);

  // RFD
  commandName = directoryName + "RFDMylarFoilThickness";
  aLine = commandName.c_str(); 
  RFDMylarFoilThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  RFDMylarFoilThickness -> SetGuidance("Sets RFD mylar foils thickness.");
  RFDMylarFoilThickness -> SetParameterName("Size",false);
  RFDMylarFoilThickness -> SetRange("Size>0.");
  RFDMylarFoilThickness -> SetUnitCategory("Length");
  RFDMylarFoilThickness -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "RFDMylarFoilsDistanceToTarget";
  aLine = commandName.c_str(); 
  RFDMylarFoilsDistanceToTarget =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  RFDMylarFoilsDistanceToTarget -> SetGuidance("Sets RFD mylar foils distance to target.");
  RFDMylarFoilsDistanceToTarget -> SetParameterName("Size",false);
  RFDMylarFoilsDistanceToTarget -> SetRange("Size>0.");
  RFDMylarFoilsDistanceToTarget -> SetUnitCategory("Length");
  RFDMylarFoilsDistanceToTarget -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "LengthOfBellow";
  aLine = commandName.c_str(); 
  LengthOfBellow =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  LengthOfBellow -> SetGuidance("Sets length of the bellow.");
  LengthOfBellow -> SetParameterName("Size",false);
  LengthOfBellow -> SetRange("Size>0.");
  LengthOfBellow -> SetUnitCategory("Length");
  LengthOfBellow -> AvailableForStates(G4State_Idle);

  //Target
  commandName = directoryName + "enableTarget";
  aLine = commandName.c_str(); 
  enableTarget = new G4UIcmdWithABool(commandName, this);
  enableTarget->SetGuidance("Creating target.");
  enableTarget->SetGuidance("Required parameters: none.");
  enableTarget->SetParameterName("useTarget",true);
  enableTarget->SetDefaultValue(true);
  enableTarget->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableTarget";
  aLine = commandName.c_str(); 
  disableTarget = new G4UIcmdWithABool(commandName, this);
  disableTarget->SetGuidance("No target chamber will be generated.");
  disableTarget->SetGuidance("Required parameters: none.");
  disableTarget->SetParameterName("useTargetChamber",true);
  disableTarget->SetDefaultValue(false);
  disableTarget->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "RFDTargetDiameter";
  aLine = commandName.c_str(); 
  RFDTargetDiameter =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  RFDTargetDiameter -> SetGuidance("Sets diameter of target.");
  RFDTargetDiameter -> SetParameterName("Size",false);
  RFDTargetDiameter -> SetRange("Size>0.");
  RFDTargetDiameter -> SetUnitCategory("Length");
  RFDTargetDiameter -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "RFDTargetThickness";
  aLine = commandName.c_str(); 
  RFDTargetThickness =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  RFDTargetThickness -> SetGuidance("Sets thickness of the target.");
  RFDTargetThickness -> SetParameterName("Size",false);
  RFDTargetThickness -> SetRange("Size>0.");
  RFDTargetThickness -> SetUnitCategory("Length");
  RFDTargetThickness -> AvailableForStates(G4State_Idle);

  commandName = directoryName + "RFDTargetMaterial";
  aLine = commandName.c_str();
  RFDTargetMaterial =  new G4UIcmdWithAString(aLine, this);
  RFDTargetMaterial -> SetGuidance("Select material of the target.");
  RFDTargetMaterial -> SetGuidance("Required parameters: 1 string.");
  RFDTargetMaterial -> SetParameterName("choice",false);
  RFDTargetMaterial -> AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "RFDTargetOffset";
  aLine = commandName.c_str();
  RFDTargetOffset =  new G4UIcmdWithADoubleAndUnit(commandName,this);
  RFDTargetOffset -> SetGuidance("Sets offset of the target from (0,0,0) point.");
  RFDTargetOffset -> SetParameterName("Size",false);
  RFDTargetOffset -> SetRange("Size>0.");
  RFDTargetOffset -> SetUnitCategory("Length");
  RFDTargetOffset -> AvailableForStates(G4State_Idle);
}


AgataAncillaryRFDMessenger::~AgataAncillaryRFDMessenger()
{
  delete enableIonGuide;
  delete disableIonGuide;
  delete enableRFDCones;
  delete disableRFDCones;
  delete enableTargetChamber;
  delete disableTargetChamber;
  delete TargetChamberDiameter;
  delete TargetChamberThickness;
  delete IonGuideLength;
  delete IonGuideThickness;
  delete IonGuideInnerDiameter;
  delete RFDMylarFoilThickness;
  delete RFDMylarFoilsDistanceToTarget;
  delete LengthOfBellow;
  delete enableTarget;
  delete disableTarget;
  delete RFDTargetDiameter;
  delete RFDTargetThickness;
  delete RFDTargetMaterial;
  delete RFDTargetOffset;
}


void AgataAncillaryRFDMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == enableIonGuide ) 
    myTarget->SetUseIonGuide( enableIonGuide->GetNewBoolValue(newValue) );
  if( command == disableIonGuide ) 
    myTarget->SetUseIonGuide( disableIonGuide->GetNewBoolValue(newValue) );

  if( command == enableRFDCones ) 
    myTarget->SetUseRFDCones( enableRFDCones->GetNewBoolValue(newValue) );
  if( command == disableRFDCones ) 
    myTarget->SetUseRFDCones( disableRFDCones->GetNewBoolValue(newValue) );

  if( command == enableTargetChamber ) 
    myTarget->SetUseTargetChamber( enableTargetChamber->GetNewBoolValue(newValue) );
  if( command == disableTargetChamber ) 
    myTarget->SetUseTargetChamber( disableTargetChamber->GetNewBoolValue(newValue) );

  if(command==TargetChamberDiameter)
    myTarget -> SetTargetChamberDiameter(TargetChamberDiameter -> GetNewDoubleValue(newValue));

  if(command==TargetChamberThickness)
    myTarget -> SetTargetChamberThickness(TargetChamberThickness -> GetNewDoubleValue(newValue));

  if(command==IonGuideLength)
    myTarget -> SetIonGuideLength(IonGuideLength -> GetNewDoubleValue(newValue));

  if(command==IonGuideThickness)
    myTarget -> SetIonGuideThickness(IonGuideThickness -> GetNewDoubleValue(newValue));

  if(command==IonGuideInnerDiameter)
    myTarget -> SetIonGuideInnerDiameter(IonGuideInnerDiameter -> GetNewDoubleValue(newValue));

  if(command==RFDMylarFoilThickness)
    myTarget -> SetRFDMylarFoilThickness(RFDMylarFoilThickness -> GetNewDoubleValue(newValue));

  if(command==RFDMylarFoilsDistanceToTarget)
    myTarget -> SetRFDMylarFoilsDistanceToTarget(RFDMylarFoilsDistanceToTarget -> GetNewDoubleValue(newValue));

  if(command==LengthOfBellow)
    myTarget -> SetLengthOfBellow(LengthOfBellow -> GetNewDoubleValue(newValue));
  
  if(command==enableTarget) 
    myTarget->SetUseTarget(enableTarget->GetNewBoolValue(newValue));
  if(command==disableTarget) 
    myTarget->SetUseTarget(disableTarget->GetNewBoolValue(newValue));

  if(command==RFDTargetDiameter)
    myTarget -> SetRFDTargetDiameter(RFDTargetDiameter->GetNewDoubleValue(newValue));

  if(command==RFDTargetThickness)
    myTarget -> SetRFDTargetThickness(RFDTargetThickness->GetNewDoubleValue(newValue));

  if(command==RFDTargetMaterial)
    myTarget -> SetRFDTargetMaterial(newValue);

  if(command==RFDTargetOffset)
    myTarget -> SetRFDTargetOffset(RFDTargetOffset->GetNewDoubleValue(newValue));
}

#endif
