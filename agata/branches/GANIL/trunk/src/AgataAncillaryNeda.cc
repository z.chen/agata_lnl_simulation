#ifdef ANCIL
#include "AgataAncillaryNeda.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "NedaPositions.hh"

AgataAncillaryNeda::AgataAncillaryNeda(G4String path, G4String name )
{
    // dummy assignment needed for compatibility with other implementations of this class
    G4String iniPath = path;
    
    dirName          = name;
    
    nam_vacuum       = "Vacuum";
    mat_vacuum       = NULL;
    
    nam_alum         = "Aluminum";
    mat_alum         = NULL;
    
    nam_scint        = "BC501A";
    mat_scint        = NULL;
    
    nam_glass        ="Aluminum";
    mat_glass        = NULL;
    
    neda_translation = G4ThreeVector(0,0,0);
    neda_rotation    = G4ThreeVector(0,0,0);
    neda_geometry    = 0;
    nistManager= G4NistManager::Instance();
    ancSD            = NULL;
    ancName          = G4String("NEDA");
    ancOffset        = 12000;
    pNedaPmt.clear();
    gdml_path        = G4String("/mnt/hgfs/Documents/geant4/gdml/AGATA/"); //G4String("/home/goasduff/gdml-files/AGATA/NEDA/"); (modified by Marc Sept2017)
    numAncSd         = 0;

    myMessenger = new AgataAncillaryNedaMessenger(this,name);
    G4cout << "AgataAncillaryNeda constructed ... " << G4endl;
}

AgataAncillaryNeda::~AgataAncillaryNeda()
{}

G4Material* AgataAncillaryNeda::FindMaterial (G4String Name){
    // search the material by its name
    //G4Material* ptMaterial = nistManager->FindOrBuildMaterial(Name);
        G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> NEDA uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary NEDA! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int AgataAncillaryNeda::FindMaterials()
{
    mat_vacuum  = FindMaterial(nam_vacuum) ;  if (!mat_vacuum)  return 1;
    mat_alum    = FindMaterial(nam_alum)   ;  if (!mat_alum)    return 1;
    mat_scint   = FindMaterial(nam_scint)  ;  if (!mat_scint)   return 1;
    mat_glass   = FindMaterial(nam_glass)  ;  if (!mat_glass)   return 1;
    return 0;
}

void AgataAncillaryNeda::InitSensitiveDetector()
{
    G4int offset = ancOffset;
#ifndef FIXED_OFFSET
    offset = theDetector->GetAncillaryOffset();
#endif
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
    if( !ancSD ) {
        ancSD = new AgataSensitiveDetector(dirName, "/anc/NEDA", "NedaCollection", offset,/* depth */ 0,/* menu */ false );
        SDman->AddNewDetector( ancSD );
        numAncSd++;
    }
}

G4bool AgataAncillaryNeda::GetPmt()
{
  G4bool status = false;
  string gdml_file = "NEDA/neda_pmt.gdml";   // "neda_pmt.gdml";  (modified by Marc Sept2017)
  std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
  if(!check_file.is_open()){
    G4cerr << "=== Cannot open NEDA PMT GDML file: " << (gdml_path+gdml_file).c_str() <<G4endl;
    return status;
    std::exit(10);
  }
  std::cout << nb_neda_modules << std::endl;
  check_file.close();
  gdmlparser.Read(gdml_path+gdml_file);
  std::ostringstream el_name;
  std::ostringstream name;
  G4VisAttributes *pDetVTC;
   for(int j=0;j<2;++j){
    el_name.str("");name.str("");
    el_name << "neda_pmt_" << j << "_vol";
    std::cout << el_name.str().c_str() << std::endl;
    name << "neda_PMT_" << j ;
    if(pNedaPmt.find(j)==pNedaPmt.end()){
      pNedaPmt[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
					mat_scint,"nedaPMT",0,0,0);
      pDetVTC = new G4VisAttributes( G4Colour(0.3,0.4,0.8));
      pNedaPmt[j]->SetVisAttributes(pDetVTC);
    }
  }
  return status;
}

G4bool AgataAncillaryNeda::GetVessel()
{
  G4bool status = false;
  string gdml_file = "NEDA/neda_vessel.gdml"; //"neda_vessel.gdml"; (modified by Marc Sept2017)
  std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
  if(!check_file.is_open()){
    G4cerr << "=== Cannot open NEDA vessel GDML file" << G4endl;
    return status;
    std::exit(10);
  }
  check_file.close();
  gdmlparser.Read(gdml_path+gdml_file);
  std::ostringstream el_name;
  std::ostringstream name;
  G4VisAttributes *pDetVTC;
    for(int j=0;j<4;++j){
    el_name.str("");name.str("");
    el_name << "neda_vessel_" << j << "_vol";
    std::cout << el_name.str().c_str() << std::endl;
    name << "neda_Vessel_" << j ;
    if(pNedaVessel.find(j)==pNedaVessel.end()){
      pNedaVessel[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
					   mat_alum,"NedaVessel",0,0,0);
      pDetVTC = new G4VisAttributes( G4Colour(0.2, 0.2, 0.2) );
      pDetVTC->SetForceWireframe(true);
      pNedaVessel[j]->SetVisAttributes(pDetVTC);
    }
  }
  return status;
}


G4bool AgataAncillaryNeda::GetScintillator()
{
  G4bool status = false;
  string gdml_file = "NEDA/neda_scint.gdml"; // "neda_scint.gdml"; (modified by Marc Sept2017)
  std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
  if(!check_file.is_open()){
    G4cerr << "=== Cannot open NEDA scintillator GDML file" << G4endl;
    return status;
    std::exit(10);
  }
  check_file.close();
  gdmlparser.Read(gdml_path+gdml_file);
  G4VisAttributes *pDetVTC;
  pNedaScint[0] = new G4LogicalVolume((gdmlparser.GetVolume("neda_scint_vol"))->GetSolid(),
				      mat_scint,"mat_scint",0,0,0);
  pDetVTC = new G4VisAttributes( G4Colour(0.65, 1, 1) );
  pNedaScint[0]->SetVisAttributes(pDetVTC);

  return status;
}



G4bool AgataAncillaryNeda::GetTop()
{
  G4bool status = false;
  string gdml_file = "NEDA/neda_top.gdml";  // "neda_top.gdml"; (modified by Marc Sept2017)
  std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
  if(!check_file.is_open()){
    G4cerr << "=== Cannot open NEDA top GDML file" << G4endl;
    return status;
    std::exit(10);
  }
  check_file.close();
  gdmlparser.Read(gdml_path+gdml_file);
  std::ostringstream el_name;
  std::ostringstream name;
  G4VisAttributes *pDetVTC;
    for(int j=0;j<14;++j){
    el_name.str("");name.str("");
    el_name << "neda_top_" << j << "_vol";
    std::cout << el_name.str().c_str() << std::endl;
    name << "neda_top_" << j ;
    if(pNedaTop.find(j)==pNedaTop.end()){
      pNedaTop[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),
					   mat_alum,"NedaTop",0,0,0);
      pDetVTC = new G4VisAttributes( G4Colour(0.3, 0.3, 0.3) );
      pDetVTC->SetForceWireframe(true);
      pNedaTop[j]->SetVisAttributes(pDetVTC);
    }
  }
  return status;
}

void AgataAncillaryNeda::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryNeda::Placement()
{
    ///////////////////////
    /// construct a shell
    ///////////////////////
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theTarget  = 
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
//  if(pNedaPmt.size()==0)
//   GetPmt();
//  if(pNedaVessel.size()==0)
//    GetVessel();
//  if(pNedaTop.size()==0)
//    GetTop();
  if(pNedaScint.size()==0)
    GetScintillator();

  for(it_log=pNedaScint.begin();it_log!=pNedaScint.end();it_log++){
    it_log->second->SetSensitiveDetector(ancSD);
  }
  G4RotationMatrix rm ;
  double x_off(0.),y_off(0.),z_off(0.);
  for(int mod=0;mod<nb_neda_modules;mod++){
    rm=G4RotationMatrix::IDENTITY;
    //    rm.rotateZ(det_pos[mod][0]*deg);
    //AGATA Frame is rotated 90 deg with respect to the 
    //mechanical frame of NEDA
    // the mechanical frame position are given with respect to the front face of the
    // detectors while the placement in the GDML file is with respect to the PMT
    // so we added an offset of 200 mm.

    rm.rotateZ(30*deg);
    x_off=-det_pos[mod][1]*mm+neda_translation.x();
    y_off=-det_pos[mod][3]*mm+neda_translation.y();;
    z_off= det_pos[mod][2]*mm + 200 + neda_translation.z();;
    for(it_log=pNedaPmt.begin();it_log!=pNedaPmt.end();it_log++){
      new G4PVPlacement(G4Transform3D(rm, (G4ThreeVector(x_off,y_off,z_off))),
			G4String("PMT"),
			it_log->second,
			(theTarget)->HallPhys(),
			false,0);
    }
    
    for(it_log=pNedaVessel.begin();it_log!=pNedaVessel.end();it_log++){
      new G4PVPlacement(G4Transform3D(rm, (G4ThreeVector(x_off,y_off,z_off))),
			G4String("Vessel"),
			it_log->second,
			(theTarget)->HallPhys(),
			false,0);
    }
    
    for(it_log=pNedaScint.begin();it_log!=pNedaScint.end();it_log++){
      new G4PVPlacement(G4Transform3D(rm, (G4ThreeVector(x_off,y_off,z_off))),
			G4String("PMT"),
			it_log->second,
			(theTarget)->HallPhys(),
			false,ancOffset+mod);
    }
    for(it_log=pNedaTop.begin();it_log!=pNedaTop.end();it_log++){
      new G4PVPlacement(G4Transform3D(rm, (G4ThreeVector(x_off,y_off,z_off))),
			G4String("Top"),
			it_log->second,
			(theTarget)->HallPhys(),
			false,0);
    }
  }

  return;
}

void AgataAncillaryNeda::ShowStatus()
{
  G4cout << " ANCILLARY NEDA has been constructed." << G4endl;
  G4cout << "     Ancillary Vacuum      material is " << mat_vacuum->GetName() << G4endl;
  G4cout << "     NEDA scintillator     material is " << mat_scint->GetName() << G4endl;
  G4cout << "     NEDA housing          material is " << mat_alum->GetName() << G4endl;
}



void AgataAncillaryNeda::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{
}


void AgataAncillaryNeda::SetTranslation(G4ThreeVector trans)
{
  neda_translation = trans;
}

void AgataAncillaryNeda::SetRotation(G4ThreeVector rot)
{
  neda_rotation[0] = rot.x()*deg;
  neda_rotation[1] = rot.y()*deg;
  neda_rotation[2] = rot.z()*deg;
}

void AgataAncillaryNeda::SetGeometry(G4int geo)
{
  if(geo==0){
    G4cout << "  ---> Setting the geometry of NEDA to the standard GANIL configuration ..." << G4endl;
  }else{
    G4cout << " Only GANIL configuration is available so far ... sorry " << G4endl;
    geo=0;
  }
  neda_geometry = geo;
}




//////////////////////////////////
// The Messenger
/////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"
//#include "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillaryNedaMessenger::AgataAncillaryNedaMessenger(AgataAncillaryNeda* pTarget, G4String name)
  :myTarget(pTarget)
{ 

  G4String directoryName = name + "/detector/ancillary/NEDA/";
  NedaDir = new G4UIdirectory(directoryName);
  NedaDir->SetGuidance("Control of NEDA detector construction.");
  
  G4String commandName;
  const char *aLine;
  
  commandName=directoryName+"setTranslation";
  aLine=commandName.c_str();
  setNedaTraCmd = new G4UIcmdWith3Vector(aLine,this);  
  setNedaTraCmd->SetGuidance("Set translation of the NEDA array with respect to its nominal position");
  setNedaTraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setRotation";
  aLine = commandName.c_str();
  setNedaRotCmd = new G4UIcmdWith3Vector(aLine,this);
  setNedaRotCmd -> SetGuidance("Set rotation of the NEDA array with respect to its nominal position");
  setNedaRotCmd -> AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "setGeometry";
  aLine = commandName.c_str();
  setNedaGeoCmd = new G4UIcmdWithAnInteger(aLine,this);
  setNedaGeoCmd ->SetGuidance("Set NEDA array geometry: 0 - standard GANIL configuration with 54 detectors");
  setNedaGeoCmd ->SetDefaultValue(0);
  setNedaGeoCmd ->AvailableForStates(G4State_PreInit,G4State_Idle);

  //  commandName = directoryName + "setNedaLightThr";
//  aLine = commandName.c_str();
//  setNedaLightThrCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
//  setNedaLightThrCmd ->SetGuidance("Set NEDA array threshold on light [value*unit]");
//  setNedaLightThrCmd ->SetDefaultValue(20*keV);
//  setNedaLightThrCmd ->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}
AgataAncillaryNedaMessenger::~AgataAncillaryNedaMessenger()
{
  delete setNedaTraCmd;
  delete setNedaRotCmd;
  delete setNedaGeoCmd;
  //delete setNedaLightThrCmd;
  delete NedaDir;
}

void AgataAncillaryNedaMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if(command==setNedaTraCmd){
    myTarget->SetTranslation(setNedaTraCmd->GetNew3VectorValue(newValue));
  }
  if(command==setNedaRotCmd){
    myTarget->SetRotation(setNedaRotCmd->GetNew3VectorValue(newValue));
  }
  if(command==setNedaGeoCmd){
    myTarget->SetGeometry(setNedaGeoCmd->GetNewIntValue(newValue));
  }
  
}

#endif
