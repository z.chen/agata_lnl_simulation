#include "AgataGeneratorOmega.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"

#if defined G4V10
using namespace CLHEP;
#endif

AgataGeneratorOmega::AgataGeneratorOmega(G4double sourceposx,
					 G4double sourceposy,
					 G4double sourceposz)
{

  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName = "geantino";
  particleGun->SetParticleDefinition(particleTable->FindParticle(particleName));
  particleGun->SetParticleEnergy(1.0*MeV);
  particleGun->
    SetParticlePosition(G4ThreeVector(sourceposx,sourceposy,sourceposz));
}

AgataGeneratorOmega::~AgataGeneratorOmega()
{
  delete particleGun;
}

void AgataGeneratorOmega::GeneratePrimaries(G4Event* anEvent)
{ 
  G4double costheta = 2.0 * G4UniformRand() - 1.0;
  G4double sintheta = sqrt( 1 - costheta * costheta );
  G4double phi      = 360. * G4UniformRand() * deg;
  
  G4ThreeVector direction;
  direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );

  ///////////////////////////////////////////////
  // finally, call particle gun 
  ///////////////////////////////////////////////////
  particleGun->SetParticleMomentumDirection( direction );
  particleGun->GeneratePrimaryVertex(anEvent);
  costheta = 2.0 * G4UniformRand() - 1.0;
  sintheta = sqrt( 1 - costheta * costheta );
  phi      = 360. * G4UniformRand() * deg;
  direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );
  particleGun->SetParticleMomentumDirection( direction );
  particleGun->GeneratePrimaryVertex(anEvent);

}
