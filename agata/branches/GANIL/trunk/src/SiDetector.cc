/***************************************************************************
                          SiDetector.cc  -  description
                             -------------------
    begin                : Thu Sep 29 2005
    copyright            : (C) 2005 by Mike Taylor
    email                : mjt502@york.ac.uk
    
Modified by Pavel Golubev (pavel.golubev@nuclear.lu.se) 
LYCCA0 configuration implemented
-----
Modified by MJT to incorporated messenger methods (6/11/07)
 ***************************************************************************/
// Adapted for Agta code, October 2009 by P. Joshi, The University of York.
// Added 4 extra segments as per current setup at GSI, June 2011 by D. Bloor, The University of York.
#include "SiDetector.h"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "globals.hh"
#include "G4ios.hh"

 SiDetector::SiDetector(G4double defTgtDetDist) :
 Si_log(0), Si_phys(0)
 {
   detDist = defTgtDetDist;
   }

 SiDetector::~SiDetector()
 {
}

void SiDetector::createDetector(G4VPhysicalVolume *lab_phy, G4Material* detMaterial)
{

  G4double Si_z = 0.15*mm;  //   1/2 z length (thickness i.e. 300 um)
  G4double Si_y = 3.0*cm;     //   1/2 y length
  G4double Si_x = 3.0*cm;     //   1/2 x length
  G4double SupportDist1 = 1.0;
  //  G4double SupportDist2 = 2.0;

  G4Box* Si_box = new G4Box("Si_box", Si_x, Si_y, Si_z);

  Si_log = new G4LogicalVolume(Si_box, detMaterial, "Si_log", 0,0,0);

  G4VisAttributes* SiVisATT
                 = new G4VisAttributes(G4Colour(0.0,0.0,1.0));      // Set detector colour for visualisation

  SiVisATT->SetForceSolid(true);     // Solid appearance NOT wireframe

  Si_log->SetVisAttributes(SiVisATT);

/*  placement[1]  = G4ThreeVector( (62.25/2.0)      ,  SupportDist1+62.25/2.0        , detDist);
  placement[2]  = G4ThreeVector( (62.25/2.0)      , -SupportDist1-62.25/2.0        , detDist);
  placement[3]  = G4ThreeVector(-(62.25/2.0)      ,  SupportDist1+62.25/2.0        , detDist);
  placement[4]  = G4ThreeVector(-(62.25/2.0)      , -SupportDist1-62.25/2.0        , detDist);

  placement[5]  = G4ThreeVector(-(62.25/2.0)      , -SupportDist2-62.25-(62.25/2.0), detDist);
  placement[6]  = G4ThreeVector(-(62.25/2.0)      ,  SupportDist2+62.25+(62.25/2.0), detDist);
  placement[7]  = G4ThreeVector( (62.25/2.0)      , -SupportDist2-62.25-(62.25/2.0), detDist);
  placement[8]  = G4ThreeVector( (62.25/2.0)      ,  SupportDist2+62.25+(62.25/2.0), detDist);

  placement[9]  = G4ThreeVector( (62.25/2.0)+62.25, -SupportDist1-62.25/2.0        , detDist);
  placement[10] = G4ThreeVector( (62.25/2.0)+62.25,  SupportDist1+62.25/2.0        , detDist);
  placement[11] = G4ThreeVector(-(62.25/2.0)-62.25, -SupportDist1-62.25/2.0        , detDist);
  placement[12] = G4ThreeVector(-(62.25/2.0)-62.25,  SupportDist1+62.25/2.0        , detDist); */

  placement[1]  = G4ThreeVector(-(62.25/2.0)-62.25,  SupportDist1+62.25, detDist);
  placement[2]  = G4ThreeVector(-(62.25/2.0)      ,  SupportDist1+62.25, detDist);
  placement[3]  = G4ThreeVector((62.25/2.0)       ,  SupportDist1+62.25, detDist);
  placement[4]  = G4ThreeVector((62.25/2.0)+62.25 ,  SupportDist1+62.25, detDist);

  placement[5]  = G4ThreeVector(-(62.25/2.0)-62.25,                   0, detDist);
  placement[6]  = G4ThreeVector(-(62.25/2.0)      ,                   0, detDist);
  placement[7]  = G4ThreeVector( (62.25/2.0)      ,                   0, detDist);
  placement[8]  = G4ThreeVector( (62.25/2.0)+62.25,                   0, detDist);

  placement[9]  = G4ThreeVector(-(62.25/2.0)-62.25, -SupportDist1-62.25, detDist);
  placement[10] = G4ThreeVector(-(62.25/2.0)      , -SupportDist1-62.25, detDist);
  placement[11] = G4ThreeVector((62.25/2.0)       , -SupportDist1-62.25, detDist);
  placement[12] = G4ThreeVector((62.25/2.0)+62.25 , -SupportDist1-62.25, detDist);

  char phys_name[20];

  for(G4int i=1; i<13; i++)
    {
      sprintf(phys_name, "Si_Segment%d", i);
        Si_phys_cpy[i] = new  G4PVPlacement(0,
                 placement[i],
                 phys_name, Si_log, lab_phy, false, i+600 );
      G4cout << "Lycca Placement for Si Segment#" << i+600 <<"=" << placement[i] << G4endl;
             }
}

