/***************************************************************************
                          DiamondLycca.cc  -  description
                             -------------------
    begin                : Thu Jul 13 2006
    copyright            : (C) 2006 by Mike Taylor
    email                : mjt502@york.ac.uk
 ***************************************************************************/
// October 2009 Adapted for Agata code by P. Joshi, The University of York.
#include "DiamondLycca.h"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "globals.hh"
#include "G4ios.hh"

 DiamondLycca::DiamondLycca(G4double defTgtDetDist) :
 DiamondLycca_log(0), DiamondLycca_phys(0)
 {
  detDist = defTgtDetDist - 10.0;
   }

 DiamondLycca::~DiamondLycca()
 {
   }

void DiamondLycca::createDetector(G4VPhysicalVolume *lab_phy, G4Material *detMaterial)
{
 G4int counter,h,i,j,k;

  G4double DiamondLycca_z = 0.1*mm;  //   1/2 z length (thickness i.e. 100 um)
  G4double DiamondLycca_y = 9.5*mm;     //   1/2 y length (3cm x 3cm)
  G4double DiamondLycca_x = 9.5*mm;     //   1/2 x length
                            
  G4Box* DiamondLycca_box = new G4Box("DiamondLycca_box", DiamondLycca_x, DiamondLycca_y, DiamondLycca_z);

  DiamondLycca_log = new G4LogicalVolume(DiamondLycca_box, detMaterial, "DiamondLycca_log", 0,0,0);

  G4VisAttributes* DiamondLyccaVisATT
                 = new G4VisAttributes(G4Colour(0.72,0.72,0.99));      // Set detector colour for visualisation

  DiamondLyccaVisATT->SetForceSolid(true);     // Solid appearance NOT wireframe

  DiamondLycca_log->SetVisAttributes(DiamondLyccaVisATT);

  counter = 1;

for(h=-93;h<94;h=h+62) {
	for(i=-93;i<94;i=i+62) {
		if (abs(i)==abs(h) && abs(i) == 93 ) continue;
	for(j=-1; j<2 ; j++) {
		for(k=-1 ; k<2 ; k++) {
  			placement[counter] = G4ThreeVector((i+(k*20))*mm, (h+(j*20))*mm, detDist);
			counter++;
			}
		}
	}
}
                                         
 char phys_name[20];                     
                                         
  for(G4int ii=1; ii<109; ii++)              
    {                                    
      sprintf(phys_name, "DiamondLycca%d", ii);
        Diamond_phys_cpy[ii] = new  G4PVPlacement(0, 
                 placement[ii], 
                 phys_name, DiamondLycca_log, lab_phy, false, i+400);
G4cout << "Lycca Placement for DiamondLycca Segment#" << i+400 <<"=" << placement[ii] << G4endl;
	}

  }
