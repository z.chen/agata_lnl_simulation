#include "AgataGeneratorAction.hh"

#include "AgataDefaultGenerator.hh"
#include "AgataAlternativeGenerator.hh"
#include "AgataGPSGenerator.hh"
#include "AgataTestGenerator.hh"
#include "AgataDetectorAncillary.hh"
#include <dlfcn.h>


//AgataGeneratorAction::AgataGeneratorAction( G4String path, G4int genType, G4bool hadr, G4bool polar, G4String name, G4bool AddOn )
AgataGeneratorAction::AgataGeneratorAction( G4String path, G4int genType, G4bool hadr, G4bool polar, G4String name )
{
  G4bool inter = true;
  if( genType ) inter = false;
  
  AgataDefaultGenerator*     theDefaultGenerator     = NULL;
  AgataAlternativeGenerator* theAlternativeGenerator = NULL;  
  AgataGPSGenerator*         theGPSGenerator         = NULL;  
  AgataTestGenerator* theTestGenerator = NULL;  
  G4bool UsingAddOnGenerator = false;
  //The first thing we will do is to see if we can find a 
  //genartor in the AddOn libraries, if so we use that first of it that
  //we found
  //if(AddOn){  // Added by Marc for PrismaFP
  std::vector<void*>::iterator itAddOns = 
    AgataDetectorAncillary::AddOns.begin();
  void*(*Generator)() = 0;
  for(; itAddOns!=AgataDetectorAncillary::AddOns.end(); ++itAddOns){
    Generator=(void*(*)())dlsym(*itAddOns,"Generator");
    if(Generator){
      theGeneration = static_cast<AgataGeneration*>(Generator());
      UsingAddOnGenerator = true;
      break;
    }
  }
  //} //end of if(AddOn) 
  if(!UsingAddOnGenerator){
    switch( genType ) {
    case 0:
      theDefaultGenerator = new AgataDefaultGenerator( path, inter, hadr, polar, name );  
      theGeneration = (AgataGeneration*)theDefaultGenerator;
      break;
    case 1:
      theDefaultGenerator = new AgataDefaultGenerator( path, inter, hadr, polar, name );  
      theGeneration = (AgataGeneration*)theDefaultGenerator;
      break;
    case 2:
      theAlternativeGenerator = new AgataAlternativeGenerator(name);  
      theGeneration = (AgataGeneration*)theAlternativeGenerator;
      break;
    case 3:
      theTestGenerator = new AgataTestGenerator();  
      theGeneration = (AgataGeneration*)theTestGenerator;
      break;
    case 4:
      theAlternativeGenerator = new AgataAlternativeGenerator(name,1);  
      theGeneration = (AgataGeneration*)theAlternativeGenerator;
      break;
    case 5:
      theGPSGenerator = new AgataGPSGenerator();  
      theGeneration = (AgataGeneration*)theGPSGenerator;
      break;

    }
  }
}

AgataGeneratorAction::~AgataGeneratorAction()
{
  delete theGeneration;
}

void AgataGeneratorAction::BeginOfRun()
{
  theGeneration->BeginOfRun();
}  

void AgataGeneratorAction::EndOfRun()
{
  theGeneration->EndOfRun();
}  

void AgataGeneratorAction::BeginOfEvent()
{
  theGeneration->BeginOfEvent();
}  

void AgataGeneratorAction::EndOfEvent()
{
  theGeneration->EndOfEvent();
}  

void AgataGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  if(AgataDistributedParameters::resume &&
     anEvent->GetEventID()<AgataDistributedParameters::resume_eventnb){
    anEvent->SetEventID(AgataDistributedParameters::resume_eventnb);
  }
  theGeneration->GeneratePrimaries(anEvent);
}

void AgataGeneratorAction::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  theGeneration->PrintToFile( outFileLMD, unitL, unitE );
}

void AgataGeneratorAction::GetStatus()
{
  theGeneration->GetStatus();  
}

