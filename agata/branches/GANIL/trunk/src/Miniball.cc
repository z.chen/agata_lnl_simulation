#ifdef MINIBALL

#include "Miniball.hh"
#include "MiniBallTripleDetector.hh"
#include "MBglobals.hh"
#include "G4UIdirectory.hh"
#include "AgataSensitiveDetector.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4NistManager.hh"
#include "G4TwoVector.hh"
#include "G4ExtrudedSolid.hh"


Miniball::Miniball()

{
  ancSD = NULL;
  ancName   = G4String("MINIBALL");
  numAncSd = 0;
  ancOffset = 19000;
  myMessenger = new MiniballMessenger(this,"Miniball");
  matAntiCompton = 0;
  matCol = 0;
  logBGO = 0;
  logCol = 0;
  AddBGO=false;
}

Miniball::~Miniball()

{
  delete myMessenger;
}



void Miniball::Placement()

{
  /*
    BGOs ( ;))
  */
  G4double cryoheight=CRYOSTAT_SIDE_HEIGHT1+CRYOSTAT_SIDE_HEIGHT2+40*mm;
  G4double cryoside = CRYOSTAT_FRONT+2*mm; 
  std::vector<G4TwoVector> corners;

  corners.push_back(G4TwoVector(0.25,sqrt(3.)/4)); //1
  corners.push_back(G4TwoVector(0,sqrt(3.)/4)); //2
  corners.push_back(G4TwoVector(-.125,sqrt(3.)/8));//3
  corners.push_back(G4TwoVector(-0.375,sqrt(3.)/8));//4
  corners.push_back(G4TwoVector(-.5,0)); //5
  corners.push_back(G4TwoVector(-0.375,-sqrt(3.)/8));//6
  corners.push_back(G4TwoVector(-.125,-sqrt(3.)/8)); //7
  corners.push_back(G4TwoVector(0,-sqrt(3.)/4));//8
  corners.push_back(G4TwoVector(.25,-sqrt(3.)/4));//9
  corners.push_back(G4TwoVector(.375,-sqrt(3.)/8));//10
  corners.push_back(G4TwoVector(.255,0));//11
  corners.push_back(G4TwoVector(0.375,sqrt(3.)/8));//12
 
 if(logBGO==0){
    std::cout << "Making logBGO\n";
    G4NistManager* man = G4NistManager::Instance();
    matAntiCompton =  man->FindOrBuildMaterial("G4_BGO");
    if(matAntiCompton==0){
      std::cout << "Fatal error: No BGO mat found!!!\n";
      exit(-1);
    }
    G4double sc1=4./sqrt(3)*cryoside,sc2=4./sqrt(3)*cryoside+4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*cryoheight;
    G4ExtrudedSolid *part1exBGO = 
      new G4ExtrudedSolid("part1exBGO",corners,cryoheight/2,
			  G4TwoVector(0,0),sc1,
			  G4TwoVector(0,0),sc2);
    sc1=4./sqrt(3)*(cryoside+20*mm);
    sc2=4./sqrt(3)*(cryoside+20*mm)+4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*cryoheight;
    G4ExtrudedSolid *part2exBGO = 
      new G4ExtrudedSolid("part2exBGO",corners,cryoheight/2-5*mm,
			  G4TwoVector(0,0),sc1,
			  G4TwoVector(0,0),sc2);
    G4SubtractionSolid *part3exBGO = 
      new G4SubtractionSolid("BGOsolid",part2exBGO,part1exBGO);
    //Backplug
    sc1=4./sqrt(3)*cryoside+4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*(cryoheight);
    sc2=4./sqrt(3)*cryoside+4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*(cryoheight);
    G4ExtrudedSolid *part1BGOBackPlug = 
      new G4ExtrudedSolid("part1BGOBackPlug",corners,10*mm,
			  G4TwoVector(0,0),sc1,
			  G4TwoVector(0,0),sc2);
    G4RotationMatrix rmbackplug;
    G4Transform3D tranbackplug(rmbackplug,
			       G4ThreeVector(0,0,cryoheight/2-15*mm));
    G4UnionSolid *unionbackplugBGO = 
      new G4UnionSolid("unionbackplugBGO",part3exBGO,part1BGOBackPlug,
		       tranbackplug);
    G4Tubs *backplugsubtub = new G4Tubs("backplugsubtub",0,37*mm,20*mm,
					0,360*deg);
    G4SubtractionSolid *solidBGO = 
      new G4SubtractionSolid("solidBGO",
			     unionbackplugBGO,
			     backplugsubtub,
			     tranbackplug);

    logBGO  = 
      new G4LogicalVolume(solidBGO,matAntiCompton,"logBGOMiniball",0,0,0);
    G4VisAttributes *pBGOVA = new G4VisAttributes(G4Colour(0.7, 0.7, 0,.3));
    logBGO->SetVisAttributes(pBGOVA);
    if(ancSD) {
      logBGO->SetSensitiveDetector(bgoancSD); 
      std::cout << "Added to ancSD pointer " <<std::endl;
    } else {
      std::cout << "No ancSD pointer, exit(-1)\n";
      exit(-1);
    }
  }
  G4double collheight=35*mm;
  if(logCol==0){
    if(matCol==0){
      matCol = G4Material::GetMaterial("Tungsten");
      if(matCol==0){
	std::cout <<"Did not find \"Tungsten\" exit(-1)\n";
	exit(-1);
      }
    }
    G4double sc1=4./sqrt(3)*cryoside-4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*collheight,
      sc2=4./sqrt(3)*cryoside;
    G4ExtrudedSolid *part1exCol = 
      new G4ExtrudedSolid("part1exCol",corners,(collheight+.1*mm)/2,
			  G4TwoVector(0,0),sc1,G4TwoVector(0,0),sc2);
    sc1=4./sqrt(3)*(cryoside+20*mm)-4./sqrt(3.)*
      tan(CAP_DELTA_THETA-DETECTOR_ANGLE)*(collheight+20*mm);
    sc2=4./sqrt(3)*(cryoside+20*mm);
    G4ExtrudedSolid *part2exCol = 
      new G4ExtrudedSolid("part2exCol",corners,collheight/2,
			  G4TwoVector(0,0),sc1,
			  G4TwoVector(0,0),sc2);
    G4SubtractionSolid *solidCol = 
      new G4SubtractionSolid("Colsolid",part2exCol,part1exCol);
    logCol = new G4LogicalVolume(solidCol,matCol,"logColMiniball");
    G4VisAttributes *pColVA = new G4VisAttributes(G4Colour(0.3, 0.3, .3,.3));
    logCol->SetVisAttributes(pColVA);
  }
  //to make sure it is added each time
  std::vector<MiniBallClusterPos>::iterator itclusters = PosOfClusters.begin();
  for(; itclusters!=PosOfClusters.end(); ++itclusters){
   G4ThreeVector r(cos(itclusters->phi*deg)*sin(itclusters->theta*deg),
		    sin(itclusters->phi*deg)*sin(itclusters->theta*deg),
		    cos(itclusters->theta*deg));
   r*=itclusters->r*mm;
   G4RotationMatrix rm;
   rm.rotateZ(itclusters->psi*deg);
   rm.rotateY(itclusters->eta*deg);
   rm.rotateZ(itclusters->gamma*deg);
   G4Transform3D *p_transform = new G4Transform3D(rm,r);
   std::ostringstream miniballname;
   miniballname << "MiniballTriple_Nb_" << (itclusters-PosOfClusters.begin());
   std::cout << miniballname.str() << "\n";
   MiniBallTripleDetector *temp = 
     new MiniBallTripleDetector(miniballname.str(),
				 theDetector->HallPhys(), p_transform,
				true, true,0);
   Clusters.push_back(temp);
   temp->Construct();
    if(AddBGO){
      if(logBGO){
	G4ThreeVector r2(cos(itclusters->phi*deg)*sin(itclusters->theta*deg),
			 sin(itclusters->phi*deg)*sin(itclusters->theta*deg),
			 cos(itclusters->theta*deg));
	r2*=itclusters->r*mm-15*mm+cryoheight/2;
	G4RotationMatrix rm2;
	rm2.rotateY(180*deg);
	rm2.rotateZ(itclusters->psi*deg);
	rm2.rotateY(itclusters->eta*deg);
	rm2.rotateZ(itclusters->gamma*deg);
	G4Transform3D bgotrans(rm2,r2);
	miniballname.str("");
	miniballname << "BGOMiniball_Nb_" << 
	  (itclusters-PosOfClusters.begin());
	G4VPhysicalVolume *ambminiball= 
	  new G4PVPlacement(bgotrans,miniballname.str(),logBGO,
			    theDetector->HallPhys(),false,
			    300+(itclusters-PosOfClusters.begin()));
	G4ThreeVector r3(cos(itclusters->phi*deg)*sin(itclusters->theta*deg),
			 sin(itclusters->phi*deg)*sin(itclusters->theta*deg),
			 cos(itclusters->theta*deg));
	r3*=itclusters->r*mm-10*mm-collheight/2;
	G4RotationMatrix rm3;
	rm3.rotateY(180*deg);
	rm3.rotateZ(itclusters->psi*deg);
	rm3.rotateY(itclusters->eta*deg);
	rm3.rotateZ(itclusters->gamma*deg);
	G4Transform3D coltrans(rm3,r3);
	miniballname.str("");
	miniballname << "ColMiniball_Nb_" 
		     << (itclusters-PosOfClusters.begin());
	new G4PVPlacement(coltrans,miniballname.str(),logCol,
			  theDetector->HallPhys(),false,0);
      }
    }
  }
}


G4int Miniball::FindMaterials()

{
  /*This is done by Miniball lib.*/
  return 0;
}


void Miniball::WriteHeader(std::ofstream &outFileLMD, G4double unit)

{
  outFileLMD << "MINIBALL WITH " << PosOfClusters.size() 
	     << " CLUSTERS Cluster Nb r theta phi psi eta gamma [mm deg...]\n";
  std::vector<MiniBallClusterPos>::iterator itclusters = PosOfClusters.begin();
  for(; itclusters!=PosOfClusters.end(); ++itclusters){
    outFileLMD << std::setiosflags(ios::fixed) << std::setprecision(2) 
	       << std::setw(10) << (itclusters-PosOfClusters.begin())
	       << std::setw(10) << itclusters->r
	       << std::setw(10) << itclusters->theta
	       << std::setw(10) << itclusters->phi
	       << std::setw(10) << itclusters->psi
	       << std::setw(10) << itclusters->eta
	       << std::setw(10) << itclusters->gamma << "\n";
  }
  outFileLMD << " coded as " << ancOffset << "+Crystal Id, seg id 0-5\n";
  outFileLMD << " BGO sheild coded as " << ancOffset+300 << "+Cluster ID\n";
  outFileLMD << "END MINIBALL\n";
}

void Miniball::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) 
    runManager->GetUserDetectorConstruction();
}


void Miniball::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
  std::cout << "Miniball offset: " << offset << "\n";
#endif    
  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 1;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/miniball", 
					"MiniballAncCollection", offset, 
					depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
    depth  = 0;
    bgoancSD = new AgataSensitiveDetector( dirName, "/anc/miniball/bgo", 
					   "MiniballBGOAncCollection", offset, 
					   depth, menu );
    SDman->AddNewDetector( bgoancSD );
    numAncSd++;

  }
  if(ancSD) MiniBallSingleDetector::SD = static_cast<void*>(ancSD);

}



void Miniball::ClearListOfClusters()

{
  while(Clusters.size()>0){
    MiniBallTripleDetector *tmp = Clusters.back();
    if(tmp) delete tmp;
    Clusters.pop_back();
  }
  PosOfClusters.clear();
}

void Miniball::AddACluster(G4String par)

{
  MiniBallClusterPos apos;
  std::istringstream is(par);
  is >> apos.r >> apos.theta >> apos.phi >> apos.psi >> apos.eta >> apos.gamma;
  PosOfClusters.push_back(apos);
}


G4int Miniball::GetSegmentNumber( G4int offset, G4int detnb, 
				  G4ThreeVector pos)

{
  //Here we try to find out the correct segment, observe that we get
  if(detnb>299){
    pos.rotateY(180*deg);
    G4double phi = pos.phi()+60*deg;
    while(phi<0) phi += 2*M_PI;
    while(phi>2*M_PI) phi -= 2*M_PI;
    G4int segment_id = (G4int) (phi/(120.*deg));
    return segment_id;
  }
  //the pos in frame of crystal...
  G4double phi = pos.phi() + 30.*deg;
  while(phi<0) phi += 2*M_PI;
  while(phi>2*M_PI) phi -= 2*M_PI;
  G4int segment_id = (G4int) (phi/(60.*deg));
  return segment_id;

}


MiniballMessenger::MiniballMessenger(Miniball* MDet, G4String /*name*/):MiniballDetector(MDet)
{
  MiniballDetDir = new G4UIdirectory("/Orgam/detector/ancillary/Miniball/");
  MiniballDetDir -> SetGuidance("Control of Miniball detector construction.");
  MiniballClearDets = new G4UIcmdWithoutParameter("/Orgam/detector/ancillary/Miniball/ClearListOfClusters",this);
  MiniballClearDets->SetGuidance("Clears list of Miniball clusters");
  MiniballClearDets->AvailableForStates(G4State_PreInit,G4State_Idle);
  MiniballAddDet = new G4UIcmdWithAString("/Orgam/detector/ancillary/Miniball/AddACluster",this);
  MiniballAddDet->SetGuidance("Add a cluster at r theta phi psi eta gamma [mm deg]");
  MiniballAddDet->AvailableForStates(G4State_PreInit,G4State_Idle);
  MiniballAddDetToMBFrame  = new G4UIcmdWithAString("/Orgam/detector/ancillary/Miniball/AddAClusterToMBFrame",this);
  MiniballAddDetToMBFrame->
    SetGuidance("Add a cluster to MB Frame at r(det) theta (frame arm) phi (angle to det from xz plane) psi.");
  MiniballAddDetToMBFrame->AvailableForStates(G4State_PreInit,G4State_Idle);
  UseBGO = new G4UIcmdWithABool("/Orgam/detector/ancillary/Miniball/UseBGOSheilds",this);
  UseBGO->SetGuidance("turn on/off bgo (bool)");
  UseBGO->AvailableForStates(G4State_PreInit,G4State_Idle);
}

MiniballMessenger::~MiniballMessenger()
{
  ;
}

void MiniballMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if(command==MiniballClearDets)  MiniballDetector->ClearListOfClusters();
  if(command==MiniballAddDet){
    MiniballDetector->AddACluster(newValue);
  }
  if(command==UseBGO){
    MiniballDetector->SetAddBGO(UseBGO->GetNewBoolValue(newValue));
  }
  if(command==MiniballAddDetToMBFrame){
    //We make the correct string and calls AddACluster
    std::ostringstream clusterstring;
    std::istringstream indata(newValue);
    double r,thetaframe,phiinframe,psidet;
    indata >> r >> thetaframe >> phiinframe >> psidet;
    G4RotationMatrix rm;
    double corrfactor = (phiinframe>180? 0 : acos(-1.));
     if(phiinframe>180){
       thetaframe=-thetaframe;
       phiinframe=2*acos(-1.)/deg-phiinframe;
     }
    rm.rotateX(phiinframe*deg);
    rm.rotateY(-thetaframe*deg);
    G4ThreeVector avec(0,1,0);
    avec = rm*avec;
    G4double theta,phi;
    theta  = avec.theta();
    phi = (avec.phi()>0 ? 
	   avec.phi() : 
	   avec.phi()+2.*acos(-1));
    G4ThreeVector NormDetPlane = G4ThreeVector(0,0,1).cross(avec);
    /*
      Here the rotation around the detector axis as a function of 
      rotating around z axis to position the detector
      is calculated. This so that the miniball alpha angle can be 
      given as read. That is if a detector is position 
      with alpha=0 the A detector is always pointing upward.

      For this to work there is a sign change when miniball phi>180 
      the "cos(corrfactor)" (we calulate the angle of a vector pointing up when
      starting the rotation with respect to the a plane defined by y-hat and 
      the position vector of the detector. See sage script below). And a 
      180 deg rotation extra as we have, in spherical system, rotated 
      the top into the bottom, the "corrfactor".

      from sage.misc.reset import *
      reset()
      from sympy.utilities.codegen import codegen
      from sage.libs.ecl import *
      sage.libs.ecl.ecl_eval("(ext:set-limit 'ext:heap-size 0)")
      theta,phi,a,b=var('theta','phi','a','b')
      v1=vector([0,0,a])
      v2=vector([0,b,a])
      yh=vector([0,1,0])
      Ry=Matrix([[cos(theta),0,sin(theta)],[0,1,0],[-sin(theta),0,cos(theta)]])
      Rz=Matrix([[cos(phi),-sin(phi),0],[sin(phi),cos(phi),0],[0,0,1]])
      v1p=Ry*v1
      v2p=Ry*v2
      v1pp=Rz*v1p
      v2pp=Rz*v2p
      nf=yh.cross_product(v1pp)
      nf=nf.normalized()
      v=v2pp-v1pp
      a=acos(nf*v/sqrt(v*v))
      a_s=a.subs(a=1,b=1)._sympy_()
      [(c_name, c_code), (h_name, c_header)] = codegen(("a",a_s),"C","a"

      On top off this,  miniball cluster without rotations the way it is 
      implemented in this simulation points in the z-hat direction. So we 
      need to rotate an extra 180 deg around the y axis when positioning 
      the detector. Howeever, the first rotation around the z axis, which 
      very corresponds to "alpha" in miniball coordinates is performed before
      this rotation.  Alpha is psidet in this code.
    
    */
    G4double corrpsi = acos(-1)/2.+corrfactor
      -acos(cos(corrfactor)*
	    -sin(phi)*cos(theta)/(sqrt(pow(sin(phi), 2) +
				       pow(cos(phi), 2))*
				  sqrt(pow(fabs(sin(theta)*cos(phi)), 2) +
				       pow(fabs(cos(theta)), 2))));
    theta/=deg;
    phi/=deg;
    std::cout << thetaframe << " " << phiinframe << " " << atan(avec.z()/avec.x()) << " " << corrpsi/deg << "\n";
    std::cout << theta << " " << phi << "\n";
    clusterstring << r << " " << theta << " " <<  phi << " " 
     		  << psidet-corrpsi/deg+90 << " " << theta-180 << " " << phi;
    std::cout << clusterstring.str() << "\n";
    MiniballDetector->AddACluster(clusterstring.str());
  }
}


#endif
