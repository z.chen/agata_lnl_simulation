#ifdef ANCIL
#include "AgataAncillaryPrismaFP.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trap.hh"


#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

#include "MyMagneticField.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4ChordFinder.hh"
#include "G4MagIntegratorStepper.hh"

AgataAncillaryPrismaFP::AgataAncillaryPrismaFP(G4String path, G4String name )
{

    // dummy assignment needed for compatibility with other implementations of this class
    G4String iniPath    = path;
    
    dirName             = name;

    nam_air          = "Air";
    mat_air          = NULL;
    
    nam_vacuum          = "Vacuum";
    mat_vacuum          = NULL;

    nam_methane         = "Methane";
    mat_methane         = NULL;

    nam_isobut          = "isoButane";
    mat_isobut          = NULL;
    
    nam_aluminium       = "Aluminum";
    mat_aluminium       = NULL;
    
    nam_lead            = "Lead";
    mat_lead            = NULL;
    
   
    nam_mylar          = "Mylar";
    mat_mylar          = NULL;

    ancSD               = NULL;
    ancName             = G4String("PrismaFP");
    ancOffset           = 35000;
    
    numAncSd            = 0;
}

AgataAncillaryPrismaFP::~AgataAncillaryPrismaFP()
{}

G4Material* FindMatFP (G4String Name){
    // search the material by its name
    G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> PrismaFP uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary PrismaFP! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int AgataAncillaryPrismaFP::FindMaterials()
{
    mat_vacuum      = FindMatFP(nam_vacuum);     if (!mat_vacuum)    return 1;
    mat_air         = FindMatFP(nam_air);        if (!mat_air)       return 1;
    mat_isobut      = FindMatFP(nam_isobut);     if (!mat_isobut)    return 1;
    mat_methane     = FindMatFP(nam_methane);     if (!mat_methane)    return 1;
    mat_aluminium   = FindMatFP(nam_aluminium);  if (!mat_aluminium) return 1;
    mat_lead        = FindMatFP(nam_lead);       if (!mat_lead)      return 1;
    mat_mylar      = FindMatFP(nam_mylar);     if (!mat_mylar)    return 1;
    return 0;
}

void AgataAncillaryPrismaFP::InitSensitiveDetector()
{
    G4int offset = ancOffset;
#ifndef FIXED_OFFSET
    offset = theDetector->GetAncillaryOffset();
#endif
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    if( !ancSD ) {
        ancSD = new AgataSensitiveDetector(dirName, "/anc/PrismaFP",
          "PrismaFPCollection", offset,/* depth */ 0,/* menu */ false );
        SDman->AddNewDetector( ancSD );
        numAncSd++;
    }

  // Adding my magnetic field
/*      double By=0.*tesla;
      MyMagneticField* myField = new MyMagneticField(G4ThreeVector(0.,By,0.));
      //G4UniformMagField* myField = new G4UniformMagField(G4ThreeVector(0.,By,0.));
      fFieldMgr= G4TransportationManager::GetTransportationManager()->GetFieldManager();



      fFieldMgr->SetDetectorField(myField);       
      fFieldMgr->CreateChordFinder(myField);

      G4MagIntegratorStepper *pItsStepper;
      G4ChordFinder* pChordFinder= new G4ChordFinder(myField,
		                                     1.0e-2*mm,  // stepper size
						     pItsStepper=0);
      fFieldMgr->SetChordFinder(pChordFinder);
      fFieldMgr->GetChordFinder()->SetDeltaChord(0.001*um);
      fFieldMgr->SetDeltaIntersection(0.001*um);

  	//fFieldMgr->GetChordFinder()->SetDeltaChord( .001 * mm ); // miss distance (default is 2.5 mm) 
  	fFieldMgr->SetMinimumEpsilonStep( .001*mm );
  	fFieldMgr->SetMaximumEpsilonStep( .001*mm );
  	fFieldMgr->SetDeltaOneStep( .001 * mm );
*/
}


void AgataAncillaryPrismaFP::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();

  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());

}


void AgataAncillaryPrismaFP::Placement()
{
    ///////////////////////
    /// construct a shell
    ///////////////////////
    
    double    PrismaMWPP_Width = 1100.*mm;         // width of the MWPP (110cm)
    double    PrismaMWPP_Depth = 20.*mm;           // width of the MWPP (20cm)  tbc
    double    PrismaMWPP_Height = 130.*mm;         // width of the MWPP (13cm)
    const double    MWPAD_Width = 100.*mm;           // length of the MWPAD (10cm)
    const double    MWPAD_Depth = 20.*mm;            // length of the MWPAD (13cm)
    const double    MWPAD_Height    = 130.0*mm;      // radius of the crystal (*2 = 1.5 inch)



    double    PrismaIC_Width = 1200.*mm;           // width of the IC frame (120cm)
    double    PrismaIC_Depth = 1000.002*mm;           // width of the IC frame (120cm)
    double    PrismaIC_Height = 140.*mm;           // width of the IC frame (14cm)
    const double    PAD_Width = 100.*mm;           // length of the PAD (10cm)
    const double    PAD_Depth = 250.*mm;          // length of the PAD (25cm)
    const double    PAD_Height    = 130.0*mm;      // radius of the crystal (*2 = 1.5 inch)
 
    const double    Window_Mylar = .001*mm;        // Mylar window/frame
    const double    ThickFrame = 5.*mm;           // Aluminium frame
//    const double    Pi  = 3.14159265358979323846;  // you realy have to read this?


    double    VacVessel1_Width = PrismaIC_Width;     // width of the IC frame (120cm)
    double    VacVessel1_Depth = 720.*mm;           // depth of the IC frame 
    double    VacVessel1_Height = PrismaIC_Height;   // height of the IC frame (14cm)
    double    VacVessel2_Depth = 450.*cm;           // depth of the entire frame (4m)

    
    //geometries in G4: see
    // http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch04.html
    
    //Building the Aluminium case

    // MWPPAC
    G4Box *solid_MWFrame = new G4Box("MWPPAC", PrismaMWPP_Width/2, PrismaMWPP_Height/2, PrismaMWPP_Depth/2);
    G4LogicalVolume *logicMWFrame = new G4LogicalVolume( solid_MWFrame, mat_vacuum, "log_MWFrame", 0, 0, 0 );

    G4Box *solid_MWPAD = new G4Box("MWPAD", MWPAD_Width/2, MWPAD_Height/2, MWPAD_Depth/2);
    G4LogicalVolume *logicMWPAD = new G4LogicalVolume( solid_MWPAD, mat_isobut, "log_MWPAD", 0, 0, 0 );

    // Vaccum volume between MWPPAC and IC
    G4Box *solid_VacVessel1 = new G4Box("VacVessel1", VacVessel1_Width/2, VacVessel1_Height/2, VacVessel1_Depth/2);
    G4LogicalVolume *logicVacVessel1 = new G4LogicalVolume( solid_VacVessel1, mat_vacuum, "log_VacVessel1", 0, 0, 0 );
      // associated Al frame
      G4Box *solid_AluFrameVessel1 = new G4Box("AluFrameVessel1", VacVessel1_Width/2 + ThickFrame, VacVessel1_Height/2 +ThickFrame, VacVessel1_Depth/2);
      G4SubtractionSolid *solid_AluVessel1 = new G4SubtractionSolid("AluVessel1", solid_AluFrameVessel1, solid_VacVessel1);
      G4LogicalVolume *logicAluVessel1 = new G4LogicalVolume( solid_AluVessel1, mat_aluminium, "log_AluVessel1", 0, 0, 0 );	


    // Vaccum volume between MWPPAC and Dipole
    G4Box *solid_VacVessel2 = new G4Box("VacVessel2", VacVessel1_Width/2, VacVessel1_Height/2, VacVessel2_Depth/2);
    flogicVacVessel2 = new G4LogicalVolume( solid_VacVessel2, mat_vacuum, "log_VacVessel2", 0, 0, 0 );
      // associated Al frame
      G4Box *solid_AluFrameVessel2 = new G4Box("AluFrameVessel2", VacVessel1_Width/2 + ThickFrame, VacVessel1_Height/2 +ThickFrame, VacVessel2_Depth/2);
      G4SubtractionSolid *solid_sqrAluVessel2 = new G4SubtractionSolid("AluSqrVessel2", solid_AluFrameVessel2, solid_VacVessel2);

      //G4double SmallTrapSide=65.5*cm;
      //G4double LargeTrapSide=70.5*cm;
      G4double SmallTrapSide= 86.4*cm; // 
      G4double LargeTrapSide= 86.4*cm; // same size fas small side for a rectangle shape
      G4double TrapPolAng=atan((LargeTrapSide-SmallTrapSide)/(2*(PrismaIC_Width/2 + ThickFrame)));

      // Removing part overlapping with the dipole
      G4Trap *solid_trap= new G4Trap("trap1", PrismaIC_Width/2 + ThickFrame*2  // half Z length
					    , TrapPolAng            // polar angle of centre of the 2 faces at +-pDz
					    , 0.*deg             // azymuthal angle
					    , PrismaIC_Height/2 + ThickFrame*2 // half y length at -pDz
					    , LargeTrapSide // half x length at -pDy and -pDz   (Dx1)
					    , LargeTrapSide // half x length at +pDy and -pDz   (Dx2)
					    , 0.*deg // angle
					    , PrismaIC_Height/2 + ThickFrame*2 // half y length at +pDz
					    , SmallTrapSide // half x at -pDy and +pDz (Dx3)
					    , SmallTrapSide // half x at +pDy and +pdz  (Dx4)
					    , 0. // angle
				    );
      //G4LogicalVolume *logicTrap = new G4LogicalVolume( solid_trap, mat_vacuum, "log_trap", 0, 0, 0 );  // use only for visualisation	

      G4RotationMatrix RyTrap;
      RyTrap.rotateY(90*deg);  
      G4Transform3D T_Trap(RyTrap,G4ThreeVector( 0.,  0., -187.5*cm));   

      G4SubtractionSolid *solid_AluVessel2 = new G4SubtractionSolid("AluVessel2", solid_sqrAluVessel2, solid_trap, T_Trap);

      G4LogicalVolume *logicAluVessel2 = new G4LogicalVolume( solid_AluVessel2, mat_aluminium, "log_AluVessel2", 0, 0, 0 );	



    //IC:
    // IC frame:
    G4Box *solid_FrameOut = new G4Box("ICFrameOut", PrismaIC_Width/2, PrismaIC_Height/2, PrismaIC_Depth/2);
    G4Box *solid_FrameIn = new G4Box("ICFrameIn", PrismaIC_Width/2 - ThickFrame, PrismaIC_Height/2 - ThickFrame , PrismaIC_Depth/2);
    G4SubtractionSolid *solid_Frame = new G4SubtractionSolid("ICFrame", solid_FrameOut, solid_FrameIn);
    G4LogicalVolume *logicFrame = new G4LogicalVolume( solid_Frame, mat_aluminium, "log_Frame", 0, 0, 0 ); 
    // IC Mylar window
    G4Box *solid_Window = new G4Box("ICWindow", PrismaIC_Width/2 - ThickFrame, PrismaIC_Height/2 -ThickFrame, Window_Mylar);
    G4LogicalVolume *logicWindow = new G4LogicalVolume( solid_Window, mat_mylar, "log_Window", 0, 0, 0 ); 
    // IC sensitive
    G4Box *solid_PAD = new G4Box("PAD", PAD_Width/2, PAD_Height/2, PAD_Depth/2);
    G4LogicalVolume *logicPAD = new G4LogicalVolume( solid_PAD, mat_methane, "log_PAD", 0, 0, 0 );



//
// Assembly of IC
//

    //Assembly of PrismaIC Pad x 4 :
    G4RotationMatrix Rid = G4RotationMatrix::IDENTITY;
    G4AssemblyVolume* PrismaIC_PADQuad = new G4AssemblyVolume();
    G4Transform3D T1(Rid,G4ThreeVector(0., 0., PAD_Depth*-1.5));    PrismaIC_PADQuad->AddPlacedVolume(logicPAD, T1);
    G4Transform3D T2(Rid,G4ThreeVector(0., 0., PAD_Depth*-0.5));    PrismaIC_PADQuad->AddPlacedVolume(logicPAD, T2);
    G4Transform3D T3(Rid,G4ThreeVector(0., 0., PAD_Depth*0.5));     PrismaIC_PADQuad->AddPlacedVolume(logicPAD, T3);
    G4Transform3D T4(Rid,G4ThreeVector(0., 0., PAD_Depth*1.5));     PrismaIC_PADQuad->AddPlacedVolume(logicPAD, T4);
  

    //Assembly of PrismaIC_Det:
    G4AssemblyVolume* PrismaIC_Det = new G4AssemblyVolume();
    /* For 10 raw of 4 PADs */
    G4Transform3D T5(Rid,G4ThreeVector( PAD_Width*-4.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T5);
    G4Transform3D T6(Rid,G4ThreeVector( PAD_Width*-3.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T6);
    G4Transform3D T7(Rid,G4ThreeVector( PAD_Width*-2.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T7);
    G4Transform3D T8(Rid,G4ThreeVector( PAD_Width*-1.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T8);
    G4Transform3D T9(Rid,G4ThreeVector( PAD_Width*-0.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T9);
    G4Transform3D T10(Rid,G4ThreeVector( PAD_Width*0.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T10);
    G4Transform3D T11(Rid,G4ThreeVector( PAD_Width*1.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T11);
    G4Transform3D T12(Rid,G4ThreeVector( PAD_Width*2.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T12);
    G4Transform3D T13(Rid,G4ThreeVector( PAD_Width*3.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T13);
    G4Transform3D T14(Rid,G4ThreeVector( PAD_Width*4.5,  0., 0.)); PrismaIC_Det->AddPlacedAssembly(PrismaIC_PADQuad, T14);


   // Assembly of IC sensitive part are in frame
    G4AssemblyVolume* PrismaIC = new G4AssemblyVolume();
    G4Transform3D T0(Rid,G4ThreeVector( 0.,  0., 0.)); 
	PrismaIC->AddPlacedVolume(logicFrame, T0); 
    G4Transform3D T0b(Rid,G4ThreeVector( 0.,  0., -1*(PrismaIC_Depth+Window_Mylar)/2)); 
	PrismaIC->AddPlacedVolume(logicWindow, T0b); 

        PrismaIC->AddPlacedAssembly(PrismaIC_Det, T0);

    //G4Transform3D T0(Rid,G4ThreeVector( 0.,  0., 0.)); // PrismaIC->AddPlacedVolume(logicFrame, T0);
    //PrismaIC->AddPlacedAssembly(PrismaIC_Det, T0);


//
// Assembly of PrismaMW:
//

    G4AssemblyVolume* PrismaMW_Sect = new G4AssemblyVolume(); // 10 sectors
    /* For 1 raw of 10 Sectors */
    G4Transform3D T15(Rid,G4ThreeVector( MWPAD_Width*-4.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T15);
    G4Transform3D T16(Rid,G4ThreeVector( MWPAD_Width*-3.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T16);
    G4Transform3D T17(Rid,G4ThreeVector( MWPAD_Width*-2.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T17);
    G4Transform3D T18(Rid,G4ThreeVector( MWPAD_Width*-1.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T18);
    G4Transform3D T19(Rid,G4ThreeVector( MWPAD_Width*-0.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T19);
    G4Transform3D T20(Rid,G4ThreeVector( MWPAD_Width*0.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T20);
    G4Transform3D T21(Rid,G4ThreeVector( MWPAD_Width*1.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T21);
    G4Transform3D T22(Rid,G4ThreeVector( MWPAD_Width*2.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T22);
    G4Transform3D T23(Rid,G4ThreeVector( MWPAD_Width*3.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T23);
    G4Transform3D T24(Rid,G4ThreeVector( MWPAD_Width*4.5,  0., 0.)); PrismaMW_Sect->AddPlacedVolume(logicMWPAD, T24);


    G4AssemblyVolume* PrismaMW = new G4AssemblyVolume();
    PrismaMW->AddPlacedVolume(logicMWFrame, T0); 
    PrismaMW->AddPlacedAssembly(PrismaMW_Sect, T0);


//
// VacVessel between IC and PPAC
//

    G4AssemblyVolume* VacV1 = new G4AssemblyVolume();
    VacV1->AddPlacedVolume(logicVacVessel1, T0);
    VacV1->AddPlacedVolume(logicAluVessel1, T0);



//
// VacVessel between PPAC and Dipole
//


    G4AssemblyVolume* VacV2 = new G4AssemblyVolume();
     VacV2->AddPlacedVolume(flogicVacVessel2, T0);
     VacV2->AddPlacedVolume(logicAluVessel2, T0);

//     VacV2->AddPlacedVolume(logicTrap,


//
// Positionning
//
   
    //Placing the IC in the hall:
    G4RotationMatrix Ry0, Ry1;
    Ry0.rotateY(-60.*deg);  // 60 degree Dipole
    Ry1.rotateY(5*deg);

    double PrismaIC_X = 520*sin(-60.*deg)*cm; // 527 is the estimated path from center of Dipole (z axis) to center of IC 
    double PrismaIC_Z = (228+520*cos(-60.*deg))*cm;
    //G4Transform3D TF(Ry0, Ry1*G4ThreeVector(PrismaIC_X, 0., PrismaIC_Z));
    G4Transform3D TF(Ry0, G4ThreeVector(PrismaIC_X, 0., PrismaIC_Z));

    PrismaIC->MakeImprint(theDetector->HallLog(),TF, 0);

    // Placing the PPAC in the hall:
    double Dist_MW_IC = PrismaMWPP_Depth/2+ VacVessel1_Depth+ PrismaIC_Depth/2; // distance center-to-center of the MWPPAC and IC volume. VacVessel_Depth (720) is the distance face-to-face

    double PrismaMW_X =  PrismaIC_X - Dist_MW_IC*sin(-60.*deg);
    double PrismaMW_Z =  PrismaIC_Z - Dist_MW_IC*cos(-60.*deg);
    //G4Transform3D TF_MWPP(Ry0, Ry1*G4ThreeVector(PrismaMW_X, 0., PrismaMW_Z));
    G4Transform3D TF_MWPP(Ry0, G4ThreeVector(PrismaMW_X, 0., PrismaMW_Z));

    PrismaMW->MakeImprint(theDetector->HallLog(),TF_MWPP, 0);

   // Placing the vacuum parts:
   double VacVessel_X =  PrismaIC_X - (Dist_MW_IC - PrismaMWPP_Depth/2- VacVessel1_Depth/2)*sin(-60.*deg);
   double VacVessel_Z =  PrismaIC_Z - (Dist_MW_IC - PrismaMWPP_Depth/2- VacVessel1_Depth/2)*cos(60.*deg);
   G4Transform3D TF_VacVessel1(Ry0, G4ThreeVector(VacVessel_X, 0., VacVessel_Z));
   //VacV1->MakeImprint(theDetector->HallLog(),TF_VacVessel1, 0);

   VacVessel_X =  PrismaIC_X - (Dist_MW_IC + PrismaMWPP_Depth + VacVessel2_Depth/2)*sin(-60.*deg);
   VacVessel_Z =  PrismaIC_Z - (Dist_MW_IC - PrismaMWPP_Depth + VacVessel2_Depth/2)*cos(60.*deg);
   G4Transform3D TF_VacVessel2(Ry0, G4ThreeVector(VacVessel_X, 0., VacVessel_Z));
   //VacV2->MakeImprint(theDetector->HallLog(),TF_VacVessel2, 0);

   //   new G4PVPlacement(TF_VacVessel2, "VacVessl2",
   //			flogicVacVessel2, 
   //			theDetector->HallPhys(), true, 0); 

   //Have a look at the CopyNumbers of PPAC sector (10):
    G4cout << "\n \n Listing of Prisma PPAC's sensitive sectors \n";
    std::vector<G4VPhysicalVolume*>::iterator VI_MW = PrismaMW->GetVolumesIterator();
    G4cout << "\n \n Number of printed volume (= Nbsectors+mothervolume) : " << PrismaMW->TotalImprintedVolumes() << "\n" << G4endl;
 
    for (unsigned int i=0; i<PrismaMW->TotalImprintedVolumes(); i++){
        int CopyNumber = (*VI_MW)->GetCopyNo();
	G4cout << " Copy number" << (*VI_MW)->GetCopyNo()  << " Name:" <<  (*VI_MW)->GetName() << G4endl;
	(*VI_MW)->SetCopyNo(CopyNumber -1); // -1 to stay in the range 100-109.
	G4cout << " Cluster Copy number (reassigned)" << (*VI_MW)->GetCopyNo() << " Name:" <<  (*VI_MW)->GetName() << G4endl;


        //if (CopyNumber %100 == 10) { // this allows to map the PPAC sensitive area and discard the mother volume.
        //   (*VI_MW)->SetCopyNo( (CopyNumber/1)); // this select the PPAC number CopyNo (1)( coding will be "AB": with A = PPAC number (0), B = Sector number 1 to 10
        //   G4cout << " Copy number" << (*VI_MW)->GetCopyNo() << " Name:" << (*VI_MW)->GetName() << G4endl;
        //}
        VI_MW++;
    }
 

    //Have a look at the CopyNumbers of IC PAD:
    G4cout << "\n \n Listing of PrismaIC's sensitive volumes \n";
    std::vector<G4VPhysicalVolume*>::iterator VI = PrismaIC->GetVolumesIterator();
      G4cout << "\n \n Number of printed volumes (=Nb PAD + mother volume): " << PrismaIC->TotalImprintedVolumes() << "\n" << G4endl;
 
    for (unsigned int i=0; i<PrismaIC->TotalImprintedVolumes(); i++){
        int CopyNumber = (*VI)->GetCopyNo();
	G4cout << " Cluster Copy number (automatic) " << (*VI)->GetCopyNo() << " Name:" <<  (*VI)->GetName() << G4endl;
	(*VI)->SetCopyNo(CopyNumber -90); // -90 to stay in the range 0-999.
	G4cout << " Cluster Copy number (reassigned)" << (*VI)->GetCopyNo() << " Name:" <<  (*VI)->GetName() << G4endl;

        VI++;
    }

    // Add user limits
    G4UserLimits* InVessel= new G4UserLimits();
    InVessel->SetMaxAllowedStep(0.5*mm);
    flogicVacVessel2->SetUserLimits(InVessel);
    //theDetector->HallLog()->SetUserLimits(InVessel);

    // sensitive detector:
      logicMWPAD -> SetSensitiveDetector(ancSD);
      logicPAD -> SetSensitiveDetector(ancSD);
      // adding VacVessel to SD for Mag field
      //flogicVacVessel2->SetSensitiveDetector(ancSD);

    // Vis Attributes
    G4VisAttributes *pVA1 = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) ); logicPAD->SetVisAttributes( pVA1 );
    G4VisAttributes *pVA2 = new G4VisAttributes( G4Colour(1.0, 0.0, 0.0) ); logicMWPAD->SetVisAttributes( pVA2 );
    G4VisAttributes *pVA3 = new G4VisAttributes( G4Colour(0.0, 1.0, 0.0) ); logicWindow->SetVisAttributes( pVA3 );
    G4VisAttributes *pVA4 = new G4VisAttributes( G4Colour(1.0, 1.0, 0.0) ); //logicVacVessel1->SetVisAttributes( pVA4 );
    									    //flogicVacVessel2->SetVisAttributes( pVA4 );
									    logicVacVessel1->SetVisAttributes( G4VisAttributes::GetInvisible());
   									    flogicVacVessel2->SetVisAttributes( G4VisAttributes::GetInvisible());
									    //logicTrap->SetVisAttributes( pVA2 );  // use only for visu

									    //logicAluVessel1->SetVisAttributes( G4VisAttributes::GetInvisible());
   									    //logicAluVessel2->SetVisAttributes( G4VisAttributes::GetInvisible());


    return;
}


void AgataAncillaryPrismaFP::ShowStatus()
{
  G4cout << " ANCILLARY PrismaFP has been constructed." << G4endl;
  G4cout << "     Ancillary Vacuum      material is " << mat_vacuum->GetName() << G4endl;
  G4cout << "     Ancillary PPAC material is " << mat_isobut->GetName() << G4endl;
  G4cout << "     Ancillary IC   material is " << mat_methane->GetName() << G4endl;
  G4cout << "     Ancillary Window      material is " << mat_mylar->GetName() << G4endl;

}

void AgataAncillaryPrismaFP::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
