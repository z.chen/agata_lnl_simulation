#include "Incoming_Beam.hh"
#ifdef G4V10
using namespace CLHEP;
#endif

Incoming_Beam::Incoming_Beam( G4String name )
{
  A=20;
  Z=12;
  KEu=100*MeV;
  KE=KEu*A;
  Dpp=0.0;
  Ex = 0.;
  fcZ=-50.*cm;
  iniPosition = fcZ * G4ThreeVector(0., 0., 1.);
  spotSize = 0*cm;
  avgDir = G4ThreeVector(0., 0., 1.);
  opAng  = 0.;
  beamRotation.set(0,0,0);
  
  
  myMessenger = new Incoming_Beam_Messenger(this,name);
  
}

Incoming_Beam::~Incoming_Beam()
{
  delete myMessenger;
}
//---------------------------------------------------------
void Incoming_Beam::Report()
{
  G4cout<<"----> Z of the incoming beam set to  "<<Z<< G4endl;
  G4cout<<"----> A of the incoming beam set to  "<<A<< G4endl;
  G4cout<<"----> Kin. En. of the incoming beam set to "<<
    G4BestUnit(KE,"Energy")<<G4endl;
  G4cout<<"----> Kin. En. per nucleon of the incoming beam set to "<<
    G4BestUnit(KEu,"Energy")<<G4endl;
  G4cout<<"----> momentum acceptance for the incoming beam set to  "<<Dpp
	<< G4endl;
  G4cout<<"----> focal point Z position for the incoming beam set to  "
	<<G4BestUnit(fcZ,"Length")<< G4endl;
  G4cout<<"----> Average direction for the incoming beam set to  "
	<<G4BestUnit(avgDir,"Length")<< G4endl;
}
//---------------------------------------------------------
void Incoming_Beam::setA(G4int Ain)
{

  A=Ain;
  G4cout<<"----> A of the incoming beam set to  "<<A<< G4endl;
  
}
//---------------------------------------------------------
void Incoming_Beam::setZ(G4int Zin)
{

  Z=Zin;
  G4cout<<"----> Z of the incoming beam set to  "<<Z<< G4endl;
  
}
//---------------------------------------------------------
void Incoming_Beam::setDpp(G4double d)
{
  Dpp=d/100.;
  G4cout<<"----> momentum acceptance for the incoming beam set to  "<< Dpp
	<< G4endl;
}
//---------------------------------------------------------
void Incoming_Beam::setfcZ(G4double d)
{
  fcZ=d;
  iniPosition = fcZ * avgDir;
  G4cout<<"----> focal point Z position for the incoming beam set to  "
	<<G4BestUnit(fcZ,"Length")<< G4endl;
}


//---------------------------------------------------------
void Incoming_Beam::setKE(G4double KEin)
{

  KE=KEin;
  KEu=KE/A;
  G4cout<<"----> Kin. En. of the incoming beam set to "<<
    G4BestUnit(KE,"Energy")<<G4endl;
  G4cout<<"----> Kin. En. per nucleon of the incoming beam set to "<<
    G4BestUnit(KEu,"Energy")<<G4endl;
}
//-----------------------------------------------------------------
void Incoming_Beam::setKEu(G4double KEuin)
{

  KEu=KEuin;
  KE=KEu*A;
  G4cout<<"----> Kin. En. of the incoming beam set to "<<
    G4BestUnit(KE,"Energy")<<G4endl;
  G4cout<<"----> Kin. En. per nucleon of the incoming beam set to "<<
    G4BestUnit(KEu,"Energy")<<G4endl; 
}
//---------------------------------------------------------
G4ThreeVector Incoming_Beam::getDirection()
{
  G4ThreeVector direction;
  
  G4double cosR = 1.0 - pow(G4RandGauss::shoot( 0., opAng ),2.)/2.;
  G4double sinR = pow( (1.-cosR*cosR), 0.5 );
  G4double phiR = G4UniformRand() * 360. * deg;
  direction.set( sinR*cos(phiR), sinR*sin(phiR), cosR );
  direction = beamRotation( direction );

  return direction;
  
}

G4ThreeVector    Incoming_Beam::getPosition() 

{ 
  G4double cost = cos(G4UniformRand() * 360. * deg);
  G4double sint = pow(1.-cost*cost,0.5);
  G4double R = G4RandGauss::shoot(0.,spotSize);
  G4ThreeVector r(R*cost,R*sint);
  r = beamRotation(r);
  return iniPosition+r;            
}

//---------------------------------------------------------
G4double Incoming_Beam::getKE(G4ParticleDefinition *ion)
{
  G4DynamicParticle dynamic;
  G4ThreeVector momentum_vector(0,0,0);
  G4double momentum;
  G4double rand;
  G4double KinEne;
  momentum_vector.setZ(1.); //beam on z axis 
  dynamic=G4DynamicParticle(ion,momentum_vector,KE);
  momentum=dynamic.GetTotalMomentum();
  rand=G4UniformRand()-0.5;
  momentum*=(1+rand*Dpp);
  momentum_vector.setMag(momentum);
  dynamic.SetMomentum(momentum_vector);  
  KinEne=dynamic.GetKineticEnergy();

  return KinEne;
  
}
//---------------------------------------------------------
void Incoming_Beam::setAvgDirection( G4ThreeVector direction )
{
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  if( direction.mag() > 0. ) {
    avgDir = direction.unit();
    G4cout  << " ----> Average beam direction set to ("
            << std::setw(8) << avgDir.x()
            << std::setw(8) << avgDir.y()
            << std::setw(8) << avgDir.z() << " ) " << G4endl;
    
    if( avgDir.theta() ) {
      beamRotation.set(0,0,0);
      beamRotation.rotateY(avgDir.theta());
      beamRotation.rotateZ(avgDir.phi());
    }
    iniPosition = fcZ * avgDir;
  }
  else {
    G4cout << " ----> Invalid value, keeping previous one (" 
           << std::setw(8) << avgDir.x()
           << std::setw(8) << avgDir.y()
           << std::setw(8) << avgDir.z() << ") " << G4endl;
  }
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

//---------------------------------------------------------
void Incoming_Beam::setOpAngle( G4double angle )
{
  if( angle > 0. && angle < 180. )
    opAng = angle*deg;  // angle in degrees
  else
    opAng = 0.;
  G4cout << " ----> Opening angle for the beam is " << opAng/deg << " deg." 
	 << G4endl;   
}
//---------------------------------------------------------
void Incoming_Beam::setEx(G4double ex)
{
  Ex = ex * MeV;
  G4cout<<"----> Excitation energy of the incoming beam set to "<<
    G4BestUnit(Ex,"Energy")<<G4endl;
}

////////////////////////////
// The Messenger
////////////////////////////
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"


Incoming_Beam_Messenger::Incoming_Beam_Messenger( Incoming_Beam* pTarget, G4String name )
  :myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/emitter/BeamIn/";
 
  BeamInDir = new G4UIdirectory(directoryName);
  BeamInDir->SetGuidance("Incoming beam control.");

  commandName = directoryName + "A";
  aLine = commandName.c_str();
  ACmd = new G4UIcmdWithAnInteger(aLine,this);
  ACmd->SetGuidance("Select the mass number for the incoming beam.");
  ACmd->SetGuidance("Required parameter: 1 integer.");
  ACmd->SetParameterName("choice",false);
  ACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Z";
  aLine = commandName.c_str();
  ZCmd = new G4UIcmdWithAnInteger(aLine,this);
  ZCmd->SetGuidance("Select the atomic number for the incoming beam.");
  ZCmd->SetGuidance("Required parameter: 1 integer.");
  ZCmd->SetParameterName("choice",false);
  ZCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "KE";
  aLine = commandName.c_str();
  KECmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  KECmd->SetGuidance("Set kinetic energy for the incoming beam.");
  KECmd->SetGuidance("Required parameters: 1 double, 1 unit.");
  KECmd->SetParameterName("choice",false);
  KECmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "KEu";
  aLine = commandName.c_str();
  KEuCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  KEuCmd->SetGuidance("Set kinetic energy per nucleon for the incoming beam.");
  KEuCmd->SetGuidance("Required parameters: 1 double, 1 unit.");
  KEuCmd->SetParameterName("choice",false);
  KEuCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "fcZ";
  aLine = commandName.c_str();
  fcZCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  fcZCmd->SetGuidance("Set distance from target of the origin for the incoming beam.");
  fcZCmd->SetGuidance("Required parameter: 1 double, 1 unit.");
  fcZCmd->SetParameterName("choice",false);
  fcZCmd->AvailableForStates(G4State_PreInit,G4State_Idle);



  commandName = directoryName + "Dpp";
  aLine = commandName.c_str();
  DppCmd = new G4UIcmdWithADouble(aLine,this);
  DppCmd->SetGuidance("Set momentum acceptance for the incoming beam (%).");
  DppCmd->SetGuidance("Required parameter: 1 double.");
  DppCmd->SetParameterName("choice",false);
  DppCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Ex";
  aLine = commandName.c_str();
  ExCmd = new G4UIcmdWithADouble(aLine,this);
  ExCmd->SetGuidance("Set excitation energy for the incoming beam (MeV).");
  ExCmd->SetGuidance("Required parameter: 1 double.");
  ExCmd->SetParameterName("choice",false);
  ExCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "Report";
  aLine = commandName.c_str();
  RepCmd = new G4UIcmdWithoutParameter(aLine,this);
  RepCmd->SetGuidance("Report parameters for the incoming beam");
  RepCmd->SetGuidance("Required parameter: none.");

  commandName = directoryName + "opA";
  aLine = commandName.c_str();
  opACmd = new G4UIcmdWithADouble(aLine,this);
  opACmd->SetGuidance("Set opening angle (degrees) for the incoming beam.");
  opACmd->SetGuidance("Required parameter: 1 double (angle in degrees).");
  opACmd->SetParameterName("choice",false);
  opACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "spotSize";
  aLine = commandName.c_str();
  spotSCmd = new G4UIcmdWithADoubleAndUnit(aLine,this);
  spotSCmd->SetGuidance("Set spotsize incoming beam.");
  spotSCmd->SetGuidance("Required parameter: 1 double and unit.");
  spotSCmd->SetParameterName("choice",false);
  spotSCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  


  commandName = directoryName + "bDir";
  aLine = commandName.c_str();
  bDirCmd = new G4UIcmdWithAString(aLine,this);
  bDirCmd->SetGuidance("Set beam direction\
 (theta, phi, degrees) for the incoming beam.");
  bDirCmd->SetGuidance("Required parameters: 2 double (angle in degrees).");
  bDirCmd->SetParameterName("choice",false);
  bDirCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}



Incoming_Beam_Messenger::~Incoming_Beam_Messenger()
{
  delete ZCmd;
  delete ACmd;
  delete opACmd;
  delete spotSCmd;
  delete bDirCmd;
  delete DppCmd;
  delete KEuCmd;
  delete KECmd;
  delete ExCmd;
  delete RepCmd;
  delete fcZCmd;
  delete BeamInDir;
}

void Incoming_Beam_Messenger::SetNewValue(G4UIcommand* command,
					  G4String newValue)
{ 

  if( command == ACmd ) {
    myTarget->setA(ACmd->GetNewIntValue(newValue));
  }
  if( command == ZCmd ) {
    myTarget->setZ(ZCmd->GetNewIntValue(newValue));
  }
  if( command == KECmd ) {
    myTarget->setKE(KECmd->GetNewDoubleValue(newValue));
  }
  if( command == ExCmd ) {
    myTarget->setEx(ExCmd->GetNewDoubleValue(newValue));
  }
  if( command == KEuCmd ) {
    myTarget->setKEu(KEuCmd->GetNewDoubleValue(newValue));
  }
  if( command == fcZCmd ) {
    myTarget->setfcZ(fcZCmd->GetNewDoubleValue(newValue));
  }
  if( command == DppCmd ) {
    myTarget->setDpp(DppCmd->GetNewDoubleValue(newValue));
  }
  if( command == RepCmd ) {
    myTarget->Report();
  }
  if( command == spotSCmd){
    myTarget->setSpotSize(spotSCmd->GetNewDoubleValue(newValue));
  }
  if( command == bDirCmd ) {
    G4double th, ph;
    sscanf( newValue, "%lf %lf", &th, &ph);
    th *= deg;
    ph *= deg;
    G4ThreeVector direzione( sin(th)*cos(ph), sin(th)*sin(ph), cos(th) );
    myTarget->setAvgDirection(direzione);
  }
  if( command == opACmd ) {
    myTarget->setOpAngle(opACmd->GetNewDoubleValue(newValue));
  }
}



