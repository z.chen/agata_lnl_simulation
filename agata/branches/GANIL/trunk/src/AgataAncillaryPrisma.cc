#ifdef ANCIL
#include "AgataAncillaryPrisma.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4Polyhedra.hh"

#include "G4MagIntegratorDriver.hh"

#include "PrismaField.hh"
#include "G4UniformMagField.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4ChordFinder.hh"
#include "G4MagIntegratorStepper.hh"
// #include "G4MagIntegratorDriver.hh"
#include "G4PropagatorInField.hh"
#include "G4MagIntegratorDriver.hh"
 

#include "G4PhysicalConstants.hh"

#include <algorithm>

#define quadrupole
//#define realDipole
#define simpleDipole

// G4ThreadLocal EMField* DetectorConstruction::fField = nullptr;

AgataAncillaryPrisma::AgataAncillaryPrisma(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of
  // this class
  G4String iniPath    = path;
    
  dirName             = name;
    
  nam_vacuum          = "Vacuum";
  mat_vacuum          = NULL;
    
  nam_aluminium       = "Aluminum";
  mat_aluminium       = NULL;
    
  ancSD               = NULL;
  ancName             = G4String("Prisma");
  ancOffset           = 34000;
    
  numAncSd            = 0;
}

AgataAncillaryPrisma::~AgataAncillaryPrisma()
{
  
}

G4Material* FindMatPrisma (G4String Name)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(Name);

  if (ptMaterial)
  {
    G4String nome = ptMaterial->GetName();
    G4cout << "----> PRISMA uses material: " << nome << G4endl;
  }
  
  else
  {
    G4cout << "Could not find the material" << Name << G4endl;
    G4cout << "Could not build the ancillary PRISMA" << G4endl;
    
    return NULL;
  }
  
  return ptMaterial;
}

G4int AgataAncillaryPrisma::FindMaterials()
{
  mat_vacuum      = FindMatPrisma(nam_vacuum);     
  if (!mat_vacuum)    return 1;
  
  mat_aluminium   = FindMatPrisma(nam_aluminium);  
  if (!mat_aluminium) return 1;
  
  return 0;
}

void AgataAncillaryPrisma::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
  if(!ancSD)
  {
    ancSD = new AgataSensitiveDetector(dirName, "/anc/Prisma",
     "PrismaCollection", offset,/* depth */ 0,/* menu */ false );
    
    SDman->AddNewDetector(ancSD);
    numAncSd++;
  }

  PrismaField* dipoleField = new PrismaField();

  G4FieldManager* fFieldMgr =
   G4TransportationManager::GetTransportationManager()->GetFieldManager();
  fFieldMgr->SetDetectorField(dipoleField);
  fFieldMgr->CreateChordFinder(dipoleField);
  //fFieldMgr->GetChordFinder()->SetDeltaChord(.001*mm);  
  fFieldMgr->SetMinimumEpsilonStep(1e-5*mm);
  fFieldMgr->SetMaximumEpsilonStep(1e-4*mm);
  fFieldMgr->SetDeltaOneStep(.5e-3*mm);
  fFieldMgr->GetChordFinder()->SetDeltaChord(1e-5*mm);
  fFieldMgr->SetDeltaIntersection(1e-5*mm);


  // G4EqMagElectricField* fEquation = new G4EqMagElectricField(dipoleField);
  // G4MagIntegratorStepper* fStepper = new G4ClassicalRK4 (fEquation, 8);
  // G4FieldManager* fFieldMgr = 
  //   G4TransportationManager::GetTransportationManager()->GetFieldManager();

  // // Relaxed
  // G4MagInt_Driver* fIntgrDriver = 
  //   new G4MagInt_Driver(.1*mm, fStepper, fStepper->GetNumberOfVariables());

  // G4ChordFinder* fChordFinder = new G4ChordFinder(fIntgrDriver);
  // fFieldMgr->SetChordFinder(fChordFinder);
  // fFieldMgr->SetDetectorField(dipoleField);
}

void AgataAncillaryPrisma::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  =
   (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}

void AgataAncillaryPrisma::Placement()
{
  ///////////////////////
  /// Quadrupole
  ///////////////////////

  // Comprised of two parts: structure + field

  // Field radius and quadrupole length come from
  // Montagnoli et al. 2002 AIP Conf. Proc. AIP 601(1): 942-946.

#ifdef quadrupole
  // Structure
  G4double quad_structRadius = 21.*cm; // NEED TO CHECK THE OUTER RADIUS
  G4double quad_structHalfLength =25.*cm;
//  G4double quad_halfLength = 25.*cm;

  // Note that to avoid tracking issue at the edge of the field, its logical
  // volume radius and length are extended by 0.1 cm 
  G4double quad_fieldRadius = 15.1*cm;
  G4double quad_fieldHalfLength = 60.1*cm;

  //G4double quad_chamberRadius = 15.*cm;

  G4double quadX = 0.*cm;
  G4double quadY = 0.*cm;
  G4double quadZ = 75.*cm;  

  // Quadrupole    
  G4VSolid* quadrupoleStruct = new G4Tubs("QuadrupoleStruct",
    quad_fieldRadius, quad_structRadius, quad_structHalfLength, 0., twopi*rad);
//  quad_chamberRadius, quad_structRadius, quad_halfLength, 0., twopi*rad);
    
  G4LogicalVolume* quadrupoleStruct_log = new G4LogicalVolume(quadrupoleStruct,
   mat_aluminium, "QuadrupoleStructLog", 0, 0, 0);

  new G4PVPlacement(0, G4ThreeVector(quadX, quadY, quadZ),
   "QuadrupoleStructPhys", quadrupoleStruct_log, theDetector->HallPhys(), 0,
   true);

 // Field
  G4VSolid* quadrupoleField = new G4Tubs("QuadrupoleField",
   0., quad_fieldRadius, quad_fieldHalfLength, 0., twopi*rad);
    
  G4LogicalVolume* quadrupoleField_log = new G4LogicalVolume(quadrupoleField,
   mat_vacuum, "QuadrupoleFieldLog", 0, 0, 0);

  new G4PVPlacement(0, G4ThreeVector(quadX, quadY, quadZ),
   "QuadrupoleFieldPhys", quadrupoleField_log, theDetector->HallPhys(), 0,
   true);

  // Visualisation attributes
  // G4VisAttributes* quadrupoleStructAtt =
  //  new G4VisAttributes(G4Colour(1., 0., 0., .2));
  // quadrupoleStructAtt->SetForceSolid(true);
  // quadrupoleStruct_log->SetVisAttributes(quadrupoleStructAtt);

  G4VisAttributes* quadrupoleFieldAtt =
   new G4VisAttributes(G4Colour(1., 0., 0., .2));
  quadrupoleFieldAtt->SetForceSolid(true);
  quadrupoleField_log->SetVisAttributes(quadrupoleFieldAtt);
#endif

  ///////////////////////
  /// Dipole
  ///////////////////////
  
  // Geometry data come from Montagnoli et al. 2002 AIP Conf. Proc. AIP 601(1):
  // 942-946.

  // The dipole is a 60 degrees section of cylinder. The intersection of its
  // entrance (tilted by 20 degrees) and the z-axis (x-axis in Montagnoli et al.)
  // is at 160 cm from the origin

  // Structure of the dipole
#if defined(realDipole) || defined(simpleDipole)
  G4double dipole_structInRadius = 65.*cm; // NEED TO CHECK THE INNER RADIUS
  G4double dipole_structOutRadius = 175.*cm; // NEED TO CHECK THE OUTER RADIUS
  G4double dipole_structHalfHeight = 15.*cm; // NEED TO CHECK THE HEIGHT

  G4double dipole_chamberInRadius = 70.*cm; 
  G4double dipole_chamberOutRadius = 170.*cm; 
  G4double dipole_chamberHalfHeight = 10.*cm;

  // To place the cylinder along the y-axis
  G4RotationMatrix dipoleRot = G4RotationMatrix();
  dipoleRot.rotateX(90.*deg);
//  dipoleRot.rotateX(M_PI/2.*rad);




  /*********** REAL DIPOLE GEOMETRY ***********/

#ifdef realDipole
  // Position of the center of the cylinder defining the dipole
  G4ThreeVector dipoleCylinderPos = G4ThreeVector(-cos(20.*deg) * 120.*cm, 0.,
   sin(20.*deg) * 120.*cm + 160.*cm); 

  G4Transform3D dipoleStructTransform =
   G4Transform3D(dipoleRot, dipoleCylinderPos); 

  G4VSolid* dipoleStruct = new G4Tubs("DipoleStruct", dipole_structInRadius,
   dipole_structOutRadius, dipole_structHalfHeight, 340.*deg, 75.*deg);
    
  G4LogicalVolume* dipoleStruct_log = new G4LogicalVolume(dipoleStruct,
   mat_vacuum, "DipoleStructLog", 0, 0, 0);

  G4Transform3D dipoleChamberTransform =
   G4Transform3D(dipoleRot, dipoleCylinderPos);

  new G4PVPlacement(dipoleStructTransform, "DipoleStructPhys", dipoleStruct_log,
   theDetector->HallPhys(), 0, true);

  G4VSolid* dipoleChamber = new G4Tubs("DipoleChamber",
   dipole_chamberInRadius, dipole_chamberOutRadius, dipole_chamberHalfHeight,
   340.*deg, 75.*deg);
    
  G4LogicalVolume* dipoleChamber_log = new G4LogicalVolume(dipoleChamber,
   mat_vacuum, "DipoleChamberLog", 0, 0, 0);

  new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), dipoleChamber_log,
   "DipoleChamberPhys", dipoleStruct_log, false, 0, true);
#endif
  

  /*********** SIMPLER DIPOLE GEOMETRY FOR TEST ***********/
#ifdef simpleDipole
  // Position of the center of the cylinder defining the dipole
  G4ThreeVector dipoleCylinderPos = G4ThreeVector(-120.*cm, 0., 160.*cm); 

  G4Transform3D dipoleStructTransform =
   G4Transform3D(dipoleRot, dipoleCylinderPos); 

  G4VSolid* dipoleStruct = new G4Tubs("DipoleStruct", dipole_structInRadius,
   dipole_structOutRadius, dipole_structHalfHeight, 0.*deg, 60.*deg);
    
  G4LogicalVolume* dipoleStruct_log = new G4LogicalVolume(dipoleStruct,
   mat_vacuum, "DipoleStructLog", 0, 0, 0);

  G4Transform3D dipoleChamberTransform =
   G4Transform3D(dipoleRot, dipoleCylinderPos);

  new G4PVPlacement(dipoleStructTransform, "DipoleStructPhys", dipoleStruct_log,
   theDetector->HallPhys(), 0, true);    

  G4VSolid* dipoleChamber = new G4Tubs("DipoleChamber",
   dipole_chamberInRadius, dipole_chamberOutRadius, dipole_chamberHalfHeight,
   0.*deg, 60.*deg);
    
  G4LogicalVolume* dipoleChamber_log = new G4LogicalVolume(dipoleChamber,
   mat_vacuum, "DipoleChamberLog", 0, 0, 0);

  new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), dipoleChamber_log,
   "DipoleChamberPhys", dipoleStruct_log, false, 0, true);
#endif

  // Visualisation attributes
  //G4VisAttributes* quadrupoleStructAtt =
  // new G4VisAttributes(G4Colour(1., 0., 0.));
  //quadrupoleStructAtt->SetForceSolid(true);
  //quadrupoleStruct_log->SetVisAttributes(quadrupoleStructAtt);

  G4VisAttributes* dipoleStructAtt =
   new G4VisAttributes(G4Colour(1., 0., 0.));
  dipoleStructAtt->SetForceSolid(true);
  dipoleStruct_log->SetVisAttributes(dipoleStructAtt);
  dipoleStructAtt->SetForceWireframe(true);

  //dipoleChamber_log->SetVisAttributes(G4VisAttributes::GetInvisible());

#endif

  return;
}

void AgataAncillaryPrisma::ShowStatus()
{
  G4cout << " Ancillary PRISMA has been constructed." << G4endl;
  G4cout << "Ancillary Vacuum material is:" << mat_vacuum->GetName()
   << G4endl;
  G4cout << "Ancillary Shielding material is: " << mat_aluminium->GetName()
   << G4endl;
}

void AgataAncillaryPrisma::WriteHeader(std::ofstream &/*outFileLMD*/,
 G4double /*unit*/)
{}

#endif
