#include "AgataTestGenerator.hh"
#include "G4Event.hh"
#include "G4Ions.hh"
#include "G4IonTable.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"


AgataTestGenerator::AgataTestGenerator( )
{

  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName = "gamma";
  particleGun->SetParticleDefinition(particleTable->FindParticle(particleName));
  particleGun->SetParticleEnergy(1.0*MeV);
  particleGun->SetParticlePosition(G4ThreeVector());
  myMessenger = new AgataTestMessenger(this, "/Agata");  
}

AgataTestGenerator::~AgataTestGenerator()
{
  delete particleGun;
}

void AgataTestGenerator::GeneratePrimaries(G4Event* anEvent)
{ 
  if(ShootIsotropic){//Added by Joa Ljungvall 9/2012 to allow sending particles in specific directions
    G4double costheta = 2.0 * G4UniformRand() - 1.0;
    G4double sintheta = sqrt( 1 - costheta * costheta );
    G4double phi      = 360. * G4UniformRand() * deg;
    
    G4ThreeVector direction;
    direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );
    
    ///////////////////////////////////////////////
    // finally, call particle gun 
    ///////////////////////////////////////////////////
    particleGun->SetParticleMomentumDirection( direction );
  }
  if(particleGun->GetParticleDefinition()->GetParticleType()=="nucleus"){
    //Check dynamic mass
    G4double eex = dynamic_cast< G4Ions*>(particleGun->GetParticleDefinition())->GetExcitationEnergy();
    if(eex>0){//Excited ions are not stable
      particleGun->GetParticleDefinition()->SetPDGStable(false);
    }
  }
  particleGun->GeneratePrimaryVertex(anEvent);


}

G4String AgataTestGenerator::GetParticleHeader( const G4Event* evt, G4double /* unitLength */, G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "pi-" )
      partType = 95;
    else if( nome == "mu-" )
      partType = 96;
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", -partType, epart/unitEnergy, direction.x(), direction.y(), direction.z(), evt->GetEventID());
  return G4String(aLine);

}



//Added by Joa Ljungvall 9/2012 to allow sending particles in specific directions
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"

AgataTestMessenger::AgataTestMessenger(AgataTestGenerator* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/alternativegenerator/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of the event generation.");
  
    commandName = directoryName + "IsotropicEmission";
  aLine = commandName.c_str();
  SetTargetIsotropic = new G4UIcmdWithABool(aLine, this);
  SetTargetIsotropic->SetGuidance("Turn on/off isotropic emission.");
  SetTargetIsotropic->SetGuidance("Required parameters: 1 bool.");
  SetTargetIsotropic->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataTestMessenger::~AgataTestMessenger()
{
  delete   myDirectory;
  delete   SetTargetIsotropic;
}

void AgataTestMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command ==  SetTargetIsotropic) {
    myTarget->SetShootIsotropic(SetTargetIsotropic->GetNewBoolValue(newValue));
  }
}
