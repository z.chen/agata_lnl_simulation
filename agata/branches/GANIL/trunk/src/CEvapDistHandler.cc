#include "CEvapDistHandler.hh"
#include "Randomize.hh"
#ifdef G4V10
#include "CLHEP/Units/SystemOfUnits.h"
//using namespace CLHEP;
#endif

EvapDistHandler::EvapDistHandler( G4String inFileName )
{
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open evaporated particle energy distribution file " << inFileName << G4endl;
//            << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;
//   G4double th_min, th_max, dth, e_min, e_max, de;
//   inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
//   this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  G4cout << "Reading " << inFileName << G4endl;
  this->ReadDistribution(inputFile);
  inputFile.close();
  
//   phi_min =   0.*deg;
//   phi_dif = 360.*deg;
//   
//   fixedTheta = false;
    
}

void EvapDistHandler::ReNew( G4String inFileName )
{
//   for( G4int ii=0; ii<(G4int)difEvapDist.size(); ii++ ) {
//     difEvapDist[ii].clear();
//     intEvapDist[ii].clear();  
//   }
  kinEnVal.clear();
  kinEnPb.clear();
  kinEnPbNorm.clear();
  intEvapDist.clear();
  
  goodDistribution = false;	   
  std::ifstream inputFile;
  inputFile.open(inFileName);
  if( !inputFile.is_open() ) {
    G4cout << " --> Warning! Could not open evaporated particle energy distribution file " << inFileName << G4endl;
//           << inFileName << ", will not consider angular distributions." << G4endl;
    goodDistribution = false;	   
    return;
  }
  goodDistribution = true;
//   G4double th_min, th_max, dth, e_min, e_max, de;
//   inputFile >> th_min >> th_max >> dth >> e_min >> e_max >> de;
//  this->InitData( e_min*MeV, e_max*MeV, de*MeV, th_min*deg, th_max*deg, dth*deg );
  G4cout << "Reading " << inFileName << G4endl;
  this->ReadDistribution(inputFile);
  
  inputFile.close();
}

// void EvapDistHandler::InitData( G4double emin, G4double emax, G4double de, G4double thmin, G4double thmax, G4double dth)
// {
//   if( emax > emin ) {
//     E_min = emin;
//     E_max = emax;
//   }
//   else {
//     E_min = emax;
//     E_max = emin;
//   }
//   
//   E_dif = de;
//   if( E_dif == 0. )
//     E_dif = 1.0*MeV;
//   
//   if( thmax > thmin ) {
//     th_min = thmin;
//     th_max = thmax;
//   }
//   else {
//     th_min = thmax;
//     th_max = thmin;
//   }
//   th_dif = dth;
//   if( th_dif == 0. )
//     th_dif = 1.0*deg;
//   
//   G4int n_en = (G4int)((E_max-E_min)/E_dif)+1;
//   G4int n_th = (G4int)((th_max-th_min)/th_dif)+1;
//   
//   difEvapDist.resize(n_en);
//   intEvapDist.resize(n_en);
//   eneDifDist.resize(n_en);
//   eneIntDist.resize(n_en);
//   
//   for( G4int ii=0; ii<n_en; ii++ ) {
//     difEvapDist[ii].resize(n_th);
//     intEvapDist[ii].resize(n_th);
//     for( G4int jj=0; jj<n_th; jj++ ) {
//       difEvapDist[ii][jj] = 0.;
//       intEvapDist[ii][jj] = 0.;    
//     }
//     eneDifDist[ii] = 0.;  
//     eneIntDist[ii] = 0.;  
//   }
// }

EvapDistHandler::~EvapDistHandler()
{
  kinEnVal.clear();
  kinEnPb.clear();
  kinEnPbNorm.clear();
  intEvapDist.clear();
}

void EvapDistHandler::ReadDistribution(std::ifstream& inFile)
{
  G4int ii=0;
  G4double dumm1, dumm2;
  
  G4cout << "inside ReadDistribution" << G4endl;
  
  // Read distribution from file, it ends with 0 0...
  inFile >> dumm1 >> dumm2;
  kinEnVal.push_back(dumm1*MeV);
  kinEnPb.push_back(dumm2);
  
//  G4cout << "reading line " << ii << G4endl;
  
  while( kinEnVal[ii]!=0 ) {
   ii++;
   inFile >> dumm1 >> dumm2;
   kinEnVal.push_back(dumm1*MeV);
   kinEnPb.push_back(dumm2);
//   G4cout << "reading line " << ii << G4endl;
   
  }
  kinEnVal.pop_back();
  kinEnPb.pop_back();
  G4cout << ii << " lines read from input file" << G4endl;

  intEvapDist.resize(kinEnPb.size());
  intEvapDist[0]=kinEnPb[0];
  for( ii=1; ii<(G4int)kinEnVal.size(); ii++ ) {
      intEvapDist[ii] = kinEnPb[ii]+intEvapDist[ii-1];
   }
   
  kinEnPbNorm.resize(kinEnPb.size());

  //nomalizes max value to 1
  for( int jj=0; jj<(G4int)kinEnVal.size(); jj++ ) {
      kinEnPbNorm[jj] =intEvapDist[jj]/intEvapDist[(G4int)kinEnVal.size()-1] ;
      
      
//      G4cout << " pb norm " << kinEnPbNorm[jj] << endl;
  }
   

}

G4double EvapDistHandler::GetEnergy()
{
  G4double z = G4UniformRand() * 1.0;
  G4double ener;
  G4int    jj = 0;
  
  while( kinEnPbNorm[jj] < z )     
    jj++;
  jj--;
//  ener =  kinEnVal[jj];
  ener = (z*(kinEnVal[jj+1]-kinEnVal[jj])-kinEnVal[jj+1]*kinEnPbNorm[jj]+kinEnVal[jj]*kinEnPbNorm[jj+1])/(kinEnPbNorm[jj+1]-kinEnPbNorm[jj]);

  return ener;
}


