#include "AgataSensitiveDetector.hh"
#include "AgataDetectorConstruction.hh"

#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include <string>
#include <cmath>

#ifdef G4V10
using namespace CLHEP;
#endif


#define TOLERANCE (0.0001*mm)
#define TOLERANEN (0.001*keV)

AgataSensitiveDetector::AgataSensitiveDetector(G4String directory, G4String name, G4String HCname, 
         G4int offs, G4int dep, G4bool createMenu )
:G4VSensitiveDetector(name)
{
  InitData( HCname, offs, dep, directory );
  if( createMenu )
    myMessenger = new AgataSensitiveDetectorMessenger(this,directory);

  if(offs==30000) trackMethod=1; // only gammas for sigma

  cout << " #%#% trackMethod=" << trackMethod << endl;

}

AgataSensitiveDetector::AgataSensitiveDetector(G4String directory, G4String name, G4String HCname, G4bool createMenu)
:G4VSensitiveDetector(name)
{
  G4int offs = 0;
  G4int dep  = 0;
  InitData( HCname, offs, dep, directory );
  if( createMenu )
    myMessenger = new AgataSensitiveDetectorMessenger(this,directory);
}

AgataSensitiveDetector::~AgataSensitiveDetector()
{
  if( myMessenger )
    delete myMessenger;
}

void AgataSensitiveDetector::InitData( G4String HCname, G4int offs, G4int dep, G4String /*directory*/ )
{
  HCID            = -1;
  collectionName.insert(HCname);
  
  offset          = offs;
  depth           = dep;
  
  trackMethod     = 0;  // default for Agata    
  
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  cout << " ################# SD Initialisation done !! ################# " << endl; 
  
  myMessenger     = NULL;
}

void AgataSensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{
  if( !HCE ) return;
  theHits     = new AgataHitDetectorCollection(SensitiveDetectorName,collectionName[0]);
  //EventIsGood = true;
  annihilationHappened = false; 
  residualEnergy = 0.;  

  //cout << "%%%%%%%%%%%% Initialise HCE for " << SensitiveDetectorName << endl;

}

G4bool AgataSensitiveDetector::ProcessHits(G4Step* aStep,G4TouchableHistory* /*ROhist*/)
{
  //cout << " ################## Entering ProcessHits ################ " << endl;
  //cout << " trackmethod "<< trackMethod << endl;
  G4double edep = 0.;
  G4double edep_gamma = 0.;
  G4double additionalEnergy = 0.;
  G4String ChiEStato;
  G4String ilProcesso;
  G4String ilGeneratore = " ";
  G4int padre;
  //gj:
  //  G4double PostStepEn =0;


  ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
  if(ChiEStato=="gamma" && aStep->GetTrack()->GetParentID()==0){
    edep_gamma = - aStep->GetDeltaEnergy();
    if(fabs(edep_gamma)>1e-10){
      ///////////////////////////////////////////////////////////////////////////////////
      /// Process name (and number via a LUT) which generated the track is retrieved.
      /// In the case of secondary particles, the process which generated the secondary
      /// particle is actually retrieved.
      ///////////////////////////////////////////////////////////////////////////////////
      padre = aStep->GetTrack()->GetParentID();
      if( padre > 0 ) {
	const G4VProcess* ChiHaGenerato = aStep->GetTrack()->GetCreatorProcess();
	if( ChiHaGenerato )
	  ilProcesso = ChiHaGenerato->GetProcessName();
	else
	  ilProcesso = "NotAProcess";
      }
      else {
	ilProcesso = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
      }
      G4int interNum = GetProcessNum( ilProcesso )+10000;
    
      ////////////////////////////////////////////////////////////
      /// Direction (in the Lab system) of the primary particle
      ////////////////////////////////////////////////////////////
      G4ThreeVector direction = aStep->GetPreStepPoint()->GetMomentumDirection();
    
      ////////////////////////////////////////////////////////////
      /// Position (in the Lab system) of the interaction point
      ////////////////////////////////////////////////////////////
      G4ThreeVector position = aStep->GetPostStepPoint()->GetPosition();
    
      ////////////////////////////////////////////////////////////
      /// Time at which the interaction point was generated,
      /// counted from the moment the primary particle was
      /// shoot.
      ////////////////////////////////////////////////////////////
      G4double time  = aStep->GetPostStepPoint()->GetGlobalTime();
      //  G4double time  = aStep->GetPostStepPoint()->GetGlobalTime() - aStep->GetPostStepPoint()->GetLoperTime();
    
      ////////////////////////////////////////////////////////////
      /// Name of the volume in which the interaction took place
      ////////////////////////////////////////////////////////////
      G4String nomevolume = aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName();
      //  G4cout << nomevolume << G4endl;
    
      //////////////////////
      /// Detector number
      //////////////////////
      G4int detCode = aStep->GetPreStepPoint()->GetTouchable()->GetReplicaNumber(depth);

      G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
      G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
      G4VPhysicalVolume* topVolume = theTouchable->GetVolume(depth);
    
      //cout << "depth" << depth << endl;

      G4ThreeVector frameTrans = topVolume->GetFrameTranslation();  // top volume is the crystal volume GeX ( X=A,B,C,D)
                                  
      const G4RotationMatrix* rot = topVolume->GetFrameRotation();

      AgataHitDetector* newHit = new AgataHitDetector();
      ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
      G4int segCode = 0;
      G4ThreeVector posSol;
      G4RotationMatrix frameRot;
      if( rot )
	frameRot = *rot;
      if(offset!=9000 && offset!=30000) posSol = frameRot( position );
      else posSol = frameRot(posSol);
      posSol += frameRot( frameTrans );
     
      segCode = theDetector->GetSegmentNumber( offset, detCode, posSol );
    
    
      newHit->SetTrackID( aStep->GetTrack()->GetTrackID() );  //> trackID
      newHit->SetDetNb( offset+ (detCode%1000) );             //> the detector number here includes the offset
      //>  so that ancillaries can be distinguished from
      //>  germanium detectors
      newHit->SetCluNb( (detCode/1000) );                     //> cluster number 
      newHit->SetSegNb( segCode );                            //> segment number 
      newHit->SetEdep( edep_gamma );                                //> energy release
      newHit->SetTime( time );                                //> time of the interaction (starting from the t0=0 reference)
      newHit->SetPos( position );                             //> position of the interaction
      newHit->SetRelPos( posSol );                            //> position of the interaction (relative to the solid)
      newHit->SetDir( direction );                            //> position of the interaction
      newHit->SetName( nomevolume );                          //> volume name
      newHit->SetInterNb( interNum );                         //> interaction
      //gj:
      newHit->SetChiEStato (ChiEStato);
      newHit->SetkineticEn (0);
      newHit->SetLight( 0.);
      theHits->insert( newHit );
    }
  }
  
  switch (trackMethod)
  {
 
    ///////////////////////////////////////////////////////////////////
    //// Idea: primary gamma is followed
    //// store the position where an e+ annihilated
    //// accept only secondary gammas coming from that position!!!
    ///////////////////////////////////////////////////////////////////
    case 1:
      /// Consider only gammas and positrons
      ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
      ilProcesso = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
      //G4cout << ChiEStato << " interacted through a " << ilProcesso << G4endl;
      /// positrons: just store the position!!!
      if( ChiEStato == "e+" && ilProcesso == "annihil" ) {
	positionOfAnnihilation = aStep->GetPostStepPoint()->GetPosition();
	/// some positron decays in flight, should correct for that ...
	///////////////residualEnergy = -1.*(aStep->GetDeltaEnergy());
	residualEnergy = aStep->GetTrack()->GetKineticEnergy();
	annihilationHappened = true;
	//G4cout << " e+ annihilated at " << positionOfAnnihilation/mm << G4endl; 
	//G4cout << " e+ residual energy is " << residualEnergy/keV << G4endl;
	if( residualEnergy ) {
          //G4cout << ChiEStato << " interacted through a " << ilProcesso << G4endl;
	  //G4cout << " e+ residual energy is " << residualEnergy/keV << G4endl;
	  G4int ii = theHits->entries();
	  //G4cout << " number of entries is " << ii << G4endl;
	  if( ii ) {
	    //G4cout << " Deposited energy was " << (*theHits)[ii-1]->GetEdep()/keV << G4endl;
	    edep = (*theHits)[ii-1]->GetEdep() - residualEnergy;
	    if( edep <= 0. ) edep = 0.;
	      (*theHits)[ii-1]->SetEdep(edep); 
	      //G4cout << " Deposited energy now is " << (*theHits)[ii-1]->GetEdep()/keV << G4endl;
	      residualEnergy = 0.;
	  }
	}
      }
      if( ChiEStato != "gamma" ) return false;
   

      ///////////////////////////////////////////////
      ///////// Treatment of secondary gammas
      ///////////////////////////////////////////////
      padre = aStep->GetTrack()->GetParentID();
      if( padre ) {
	////////////////////////////////////////////////////////////////////////
	//// Should not count very-low-energy gammas from photoelectric
	///  or bremsstrahlung
	////////////////////////////////////////////////////////////////////////
	if( aStep->GetTrack()->GetCreatorProcess() )
          ilGeneratore = aStep->GetTrack()->GetCreatorProcess()->GetProcessName();
	if( ilGeneratore != "annihil" )
	  return false;
	////////////////////////////////////////////////////////////////////////
	//// Only secondary gammas from annihilation vertexes!!!
	////////////////////////////////////////////////////////////////////////
	if( annihilationHappened ) {
          G4ThreeVector originOfTrack = aStep->GetTrack()->GetVertexPosition();
	  //G4cout << " originOfTrack is " << originOfTrack/mm << G4endl;
	  if( (positionOfAnnihilation-originOfTrack).mag() > TOLERANCE ) return false;
	}
      }
      ///////////////////////////////////////////////////////////////////
      /////// Should take care of in-flight annihilations!
      //////////////////////////////////////////////////////////////////
      if( ilProcesso == "conv" || ilProcesso == "LowEnConversion" ) {
        additionalEnergy = residualEnergy;
	//if( additionalEnergy )
	  //G4cout << " additionalEnergy is " << additionalEnergy << G4endl;
      }  
      
      if( ilProcesso != "conv" && ilProcesso != "LowEnConversion" )
        edep  = -1.*(aStep->GetDeltaEnergy());
      else {      // pair production
        edep  =  -1.*(aStep->GetDeltaEnergy()) - 2. * electron_mass_c2 - additionalEnergy;  // E_gamma - 2 m_e
	if( edep < 0. ) edep = 0.;
      }	
      if(edep==0.)
        return false;
      break;

    /////////////////////////////////////////////////////////////
    /// default trackMethod: the electron tracks are followed
    /////////////////////////////////////////////////////////////
    default:
      edep = aStep->GetTotalEnergyDeposit();
      if(edep==0.)
        return false;
      // emulate pulse height defect in case of energy deposition by heavy ions
      // as suggested by Joa Ljungvall
#ifdef PULSE_DEFECT      	
      // heavy ion ==> particle mass larger than alpha particle
      //G4cout << " Tracked " << aStep->GetTrack()->GetDefinition()->GetParticleName() << G4endl;
      //G4cout << " Mass is " << aStep->GetTrack()->GetDefinition()->GetPDGMass()/MeV << G4endl;
      if( aStep->GetTrack()->GetDefinition()->GetPDGMass() > 4.* proton_mass_c2 )
        edep = 0.210 * pow( edep, 1.099 );
#ifdef FLAGGED_NEUTRON
      G4RunManager* runManager = G4RunManager::GetRunManager();
      const G4Event* evt = runManager->GetCurrentEvent();
      G4cout << " Primary is " << evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() << G4endl;
      if( evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() == "neutron" )
        edep = -edep;
#endif	
#endif	
  }
  
  ///////////////////////////////////////////////////////////////////////////////////
  /// Process name (and number via a LUT) which generated the track is retrieved.
  /// In the case of secondary particles, the process which generated the secondary
  /// particle is actually retrieved.
  ///////////////////////////////////////////////////////////////////////////////////
  padre = aStep->GetTrack()->GetParentID();
  if( padre > 0 ) {
    const G4VProcess* ChiHaGenerato = aStep->GetTrack()->GetCreatorProcess();
    if( ChiHaGenerato )
      ilProcesso = ChiHaGenerato->GetProcessName();
    else
      ilProcesso = "NotAProcess";
  }
  else {
    ilProcesso = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
  }
  G4int interNum = GetProcessNum( ilProcesso );
  
  ////////////////////////////////////////////////////////////
  /// Direction (in the Lab system) of the primary particle
  ////////////////////////////////////////////////////////////
  G4ThreeVector direction = aStep->GetPreStepPoint()->GetMomentumDirection();

  ////////////////////////////////////////////////////////////
  /// Position (in the Lab system) of the interaction point
  ////////////////////////////////////////////////////////////
  G4ThreeVector position = aStep->GetPostStepPoint()->GetPosition();
  
  ////////////////////////////////////////////////////////////
  /// Time at which the interaction point was generated,
  /// counted from the moment the primary particle was
  /// shoot.
  ////////////////////////////////////////////////////////////
  G4double time  = aStep->GetPostStepPoint()->GetGlobalTime();
//  G4double time  = aStep->GetPostStepPoint()->GetGlobalTime() - aStep->GetPostStepPoint()->GetLoperTime();
  
  ////////////////////////////////////////////////////////////
  /// Name of the volume in which the interaction took place
  ////////////////////////////////////////////////////////////
  G4String nomevolume = aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName();
//  G4cout << nomevolume << G4endl;
  
  //////////////////////
  /// Detector number
  //////////////////////
  G4int detCode = aStep->GetPreStepPoint()->GetTouchable()->GetReplicaNumber(depth);



  //if( offset ){
  //  G4cout << " offset " << offset << " " << detCode << G4endl;
  //  G4cout << nomevolume << G4endl;
  //}
  //if( (detCode%1000) == 1 )
  //  G4cout << nomevolume << G4endl;

  ////////////////////////////////////////////////////////////////////
  /// Position of the interaction point in the solid reference frame
  ///////////////////////////////////////////////////////////////////
  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
  G4VPhysicalVolume* topVolume = theTouchable->GetVolume(depth);
  
  //cout << "depth" << depth << endl;

  G4ThreeVector frameTrans = topVolume->GetFrameTranslation();  // top volume is the crystal volume GeX ( X=A,B,C,D)
                                  
  const G4RotationMatrix* rot = topVolume->GetFrameRotation();


  G4String gjVolumeName;
  gjVolumeName = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(depth)->GetName();

  //gj start
  //2010/02/05 na razie tu nie wchodzi, bo ma offset==0 dla catcherowych!
  G4ThreeVector posSol;  
  if(offset==9000){
    G4int VolumeID  =-1;
    G4int VMotherID =-1;
    G4int Gen2ID    =-1;
    G4int Gen3ID    =-1;
    G4int Gen4ID    =-1;

    //G4String gjVolumeName;
    gjVolumeName = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(depth)->GetName();  // = GeX (X=A,B,C,D)
    // Volume ID :
    VolumeID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber();   // = 0(GeA) or 1(GeB) or 2(GeC) or 3 (GeD)
    // Volume Mother ID :
    VMotherID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1); // = physiVac/solidVac (=0 always)
    // Volume ancester Gen2ID: Grand Mother ID = 2 Generation above:
    Gen2ID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);    // = physiCloverCan/SolidCloverCan (=0 always)
    
    if(gjVolumeName=="GeA"||gjVolumeName=="GeB"||gjVolumeName=="GeC"||gjVolumeName=="GeD"
       ||//gj 2010/02/05 to have small clovers and use 4 of them around the catcher
       gjVolumeName=="SmallGeA"||gjVolumeName=="SmallGeB"||gjVolumeName=="SmallGeC"||gjVolumeName=="SmallGeD"
       ){
      //cout << " Ge crystal name: " << gjVolumeName<< endl; // = GeA or GeB or GeC or GeD
      //cout << " Volume Id: " << VolumeID<< endl;   // > GeA:0 or GeB:1 or GeC:2 or GeD:3
      //cout << " VMother Id: " << VMotherID<< endl; // = VacA or VacB or Vac C or VacD (All =0)
      //cout << " VGMother Id: " << Gen2ID<< endl;   // = PhysiVac Id (All =0)

      Gen3ID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(3); // = physiSupClover/SolidSupclover = Clover Id (see EXOGAM_INPUT file)
      //cout << " VGGMother Id: " << Gen3ID << endl; // = Clover Id (see EXOGAM_INPUT file)

      detCode=4*Gen3ID+VolumeID;

      //cout << " detCode: " << detCode << endl;

      G4ThreeVector frameTransbis = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(3)->GetFrameTranslation();
      const G4RotationMatrix *rotbis = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(3)->GetFrameRotation();
      G4RotationMatrix frameRotBis;
      if(rotbis){
		frameRotBis=*rotbis;
      }
      //cout << "Volume 3 name:" << aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(3)->GetName() << endl;
      //cout << "Vol3 Rot theta:" << rotbis->getTheta() << endl;
      //cout << "Vol3 Rot Phi:" << rotbis->getPhi() << endl;
      //cout << "Vol3 Translation X:" << frameTransbis.getX() << endl;
      //cout << "Vol3 Translation Y:" << frameTransbis.getY() << endl;
      //cout << "Vol3 Translation Z:" << frameTransbis.getZ() << endl;

      //cout << "PosXlab= "<< position.x() << " PosYlab= " << position.y() << " PosZlab= " << position.z() << endl;

      posSol = frameRotBis( position );
      //cout << "PosSolXrot= "<< posSol.x() << " PosSolY= " << posSol.y() << " PosSolZ= " << posSol.z() << endl;

      posSol += frameRotBis( frameTransbis );
      //cout << "PosSolXtrans= "<< posSol.x() << " PosSolY= " << posSol.y() << " PosSolZ= " << posSol.z() << endl;

    }
    else if(gjVolumeName=="SCatcherA") detCode=100+4*Gen2ID+VMotherID;
    else if(gjVolumeName=="SCatcherB") detCode=200+4*Gen2ID+VMotherID;
    else if(gjVolumeName=="SShieldA")  detCode=300+4*Gen2ID+VMotherID;
    else if(gjVolumeName=="SShieldB")  detCode=400+4*Gen2ID+VMotherID;
    else if(gjVolumeName=="BackCsI")   detCode=500+4*Gen2ID+VMotherID;
    else if(gjVolumeName=="AGe1A"||gjVolumeName=="AGe1B"||gjVolumeName=="AGe1C"||gjVolumeName=="AGe1D"||
	    gjVolumeName=="SmallAGe1A"||gjVolumeName=="SmallAGe1B"||gjVolumeName=="SmallAGe1C"||gjVolumeName=="SmallAGe1D"){
      Gen4ID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(4);
      detCode=600+4*Gen4ID+VolumeID;
    }
    else cout<<"Potential error! Not known Exogam part named:  "<<gjVolumeName<<endl;

  }
  //gj stop

  //4 SIGMA start
  //2015/07/17
  
  if(offset==30000){
    // G4int VolumeID  =-1;
    // G4int VMotherID =-1;
    // G4int Gen2ID    =-1;
    G4int Gen3ID    =-1;
    // G4int Gen4ID    =-1;

    // //G4String gjVolumeName;
    // gjVolumeName = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(depth)->GetName();
    // // Volume ID :
    // VolumeID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber();
    // // Volume Mother ID :
    // VMotherID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(1);
    // // Volume ancester Gen2ID: Grand Mother ID = 2 Generation above:
    // Gen2ID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(2);
    
    if(gjVolumeName=="GeA"){
      Gen3ID = aStep->GetPreStepPoint()->GetTouchableHandle()->GetReplicaNumber(3);
      //detCode=4*Gen3ID+VolumeID;
      detCode=Gen3ID;
      G4ThreeVector frameTransbis = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(3)->GetFrameTranslation();
      const G4RotationMatrix *rotbis = aStep->GetPreStepPoint()->GetTouchableHandle()->GetVolume(3)->GetFrameRotation();
      G4RotationMatrix frameRotBis;
      if(rotbis){
		frameRotBis=*rotbis;
      }
      posSol = frameRotBis( position );
      posSol += frameRotBis( frameTransbis );
   
      
    }

    else cout<<"Potential error! Not known Sigma part named:  "<<gjVolumeName<<endl;

  }
  //4 SIGMA stop


                                  
  G4RotationMatrix frameRot;
  if( rot )
    frameRot = *rot;
  if(offset!=9000 && offset!=30000) posSol = frameRot( position );
  else posSol = frameRot(posSol);

  //cout << "topVol Rot theta:" << rot->getTheta() << endl;
  //cout << "topVol Rot Phi:" << rot->getPhi() << endl;
  //cout << "topVol Translation X:" << frameTrans.getX() << endl;
  //cout << "topVol Translation Y:" << frameTrans.getY() << endl;
  //cout << "topVol Translation Z:" << frameTrans.getZ() << endl;

  //cout << "posSolX=" << posSol.x() << " posSolY=" << posSol.y() << " posSolZ=" << posSol.z() << endl;
 	 posSol += frameRot( frameTrans );
  //cout << "posSolX=" << posSol.x() << " posSolY=" << posSol.y() << " posSolZ=" << posSol.z() << endl;
    	
    	
  //////////////////////
  /// Segment number
  //////////////////////
  G4int segCode = 0;
  G4double light=0.;
  segCode = theDetector->GetSegmentNumber( offset, detCode, posSol );
  
  //G4cout << " Ho trovato qualcosa? " << offset+ (detCode%1000) << " " << segCode << " " << edep/keV << G4endl;
  /////////////////////////////////////////////////////
  /// Stores the information in the "hit" structure
  ////////////////////////////////////////////////////
  AgataHitDetector* newHit = new AgataHitDetector();

  //this condition should include all neutron detectors
  if(offset==66000){
    G4RunManager* runManager = G4RunManager::GetRunManager();
    const G4Event* evt = runManager->GetCurrentEvent();
    G4String particle_name = evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName();
    /**/ if(particle_name=="neutron"){segCode=-10;}
    else if(particle_name=="gamma"  ){segCode=-20;}
    else {segCode=-66;}
  }

  //gj:1:
  ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
  residualEnergy = aStep->GetTrack()->GetKineticEnergy();

  
  newHit->SetTrackID( aStep->GetTrack()->GetTrackID() );  //> trackID
  newHit->SetDetNb( offset+ (detCode%1000) );             //> the detector number here includes the offset
                                                          //>  so that ancillaries can be distinguished from
							  //>  germanium detectors
  newHit->SetCluNb( (detCode/1000) );                     //> cluster number 
  newHit->SetSegNb( segCode );                            //> segment number 
  newHit->SetEdep( edep );                                //> energy release
  newHit->SetTime( time );                                //> time of the interaction (starting from the t0=0 reference)
  newHit->SetPos( position );                             //> position of the interaction
  newHit->SetRelPos( posSol );                            //> position of the interaction (relative to the solid)
  newHit->SetDir( direction );                            //> position of the interaction
  newHit->SetName( nomevolume );                          //> volume name
  newHit->SetInterNb( interNum );                         //> interaction
  //gj:
  newHit->SetChiEStato (ChiEStato);
  newHit->SetkineticEn (residualEnergy);
  if( (offset==7000 || offset==12000)
      && ChiEStato!="Al27" && ChiEStato!="neutron" ){
    light = LightProduced(newHit);
    //    if(offset==12000 && light < theNeda)
    newHit->SetLight(light);
    
  }else{
    newHit->SetLight( 0.);
  }
  theHits->insert( newHit );

  return true;
}

#include "AgataRunAction.hh"
void AgataSensitiveDetector::EndOfEvent(G4HCofThisEvent* HCE)
{
  G4int i;
  
  if(HCID<0) { 
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); 
  }
  HCE->AddHitsCollection( HCID, theHits );
  
  G4RunManager* runManager = G4RunManager::GetRunManager();
  const G4Event* evt = runManager->GetCurrentEvent();
  G4int evNum = evt->GetEventID();

  //////////////////////////////////////////////////////////////////////////////////////
  /// The characteristics of the primary particle are retrieved for printout on screen
  ///  in case of high verbosity levels
  //////////////////////////////////////////////////////////////////////////////////////
  G4double enpartlab = evt->GetPrimaryVertex()->GetPrimary()->GetMomentum().mag();
  G4double masspart  = evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetPDGMass();
  if( masspart > 0. )    // massive particles --> kinetic energy (relativistic)
    enpartlab  = sqrt( masspart * masspart + enpartlab  * enpartlab ) - masspart;

  G4int nHits = theHits->entries();

  G4cout.precision(3);
  G4cout.setf(ios::fixed);
  if(verboseLevel>0) {
    G4cout << " Ev# "   << std::setw(4) << evNum << "  "
           << "  Ein  " << std::setw(8) << enpartlab/keV  << " keV" << G4endl;
  }

  if(nHits) {
    G4double etot = 0.;
     G4int    detNumber;
     G4int    cluNumber;
    for(i = 0; i < nHits; i++)  {
       detNumber = (*theHits)[i]->GetDetNb();
       cluNumber = (*theHits)[i]->GetCluNb();
      etot     += (*theHits)[i]->GetEdep();
      if(verboseLevel>0) {
        G4cout << " Ev# "   << std::setw(4) << evNum << "  "
                            << std::setw(2) << i << " " 
                            << std::setw(2) << detNumber << " "
                            << std::setw(2) << cluNumber;
        (*theHits)[i]->Print();
      }
    }
    //G4cout << " Ev#" << evNum << " Etot is " << etot/keV << " and epartlab is " << enpartlab/keV << G4endl;
    if( etot > (enpartlab+TOLERANEN) && trackMethod ) {
      AgataRunAction* theRun  = (AgataRunAction*) runManager->GetUserRunAction();
      theRun->IncrementErrors();
      ///////////////////////////////////////////////////////////////
      /// Uncomment next line if you like event-by-event warning ///
      //////////////////////////////////////////////////////////////
      
      //G4cout << " Warning! Ev# " << evNum << " Etot is " << etot/keV << " and enpartlab is " << enpartlab/keV << " keV" << G4endl;
      
      //for(i = 0; i < nHits; i++)  {
      //  G4cout << " Hit " << (*theHits)[i]->GetEdep()/keV << " " << (*theHits)[i]->GetPos()/mm << G4endl;;
      //}
      
      // seems like there is no .clear() available ...
      delete theHits;
      theHits     = new AgataHitDetectorCollection(SensitiveDetectorName,collectionName[0]);
    }
    if(verboseLevel>0) {
      G4cout << std::setw(50) << "TotEdep ";
      G4cout.setf(ios::fixed);
      G4cout << std::setw(7) << etot/keV << " keV" << G4endl;
    }
  }
  annihilationHappened = false; 
  positionOfAnnihilation = G4ThreeVector();
  residualEnergy = 0.; 
}  

void AgataSensitiveDetector::DeleteHits()
{
  delete theHits;
}

void AgataSensitiveDetector::SetDepth( G4int dep )
{
  if( dep >= 0 )
    depth = dep;
  else
    depth = 0;
    
  G4cout << " --> Sensitive Detector depth has been set to " << depth << G4endl; 
}

////////////////////////////////
// Methods for the Messenger
////////////////////////////////
void AgataSensitiveDetector::SetMethod( G4int method )
{
  if( method > 0 )
    trackMethod = 1;
  else
    trackMethod = 0;  
  switch( trackMethod )
  {
    case 0:
      G4cout << " ----> Performing standard tracking" << G4endl;
      break;
    case 1:
      G4cout << " ----> Considering only gammas (including pair production)" << G4endl;
      break;
    default:
      G4cout << " ----> Performing standard tracking" << G4endl;
      break;
  }
}

void AgataSensitiveDetector::ShowStatus()
{
  switch( trackMethod )
  {
    case 0:
      G4cout << " Performing standard tracking" << G4endl;
      break;
    case 1:
      G4cout << " Considering only gammas (including pair production)" << G4endl;
      break;
    default:
      G4cout << " Performing standard tracking" << G4endl;
      break;
  }
}

G4int AgataSensitiveDetector::GetProcessNum( G4String nome )
{
  /////////////////////////////////////////////////////////////
  //> LUT for the processes which generated a step
  //> 1 ---> Compton
  //> 2 ---> photoelectric
  //> 3 ---> pair production
  //> 4 ---> Rayleigh
  //> 5 ---> transportation
  //> 99 --> other process (not a gamma interaction directly)
  ////////////////////////////////////////////////////////////
  
  if( nome=="compt" )
    return 1;
  if( nome=="LowEnCompton" )
    return 1;
  if( nome=="G4LECSCompton" )
    return 1;
  if( nome=="polarCompt" )
    return 1;
  if( nome=="polarLowEnCompt" )
    return 1;
  
  if( nome=="phot" )
    return 2;
  if( nome=="LowEnPhotoElec" )
    return 2;

  if( nome=="conv" )
    return 3;
  if( nome=="LowEnConversion" )
    return 3;

  if( nome=="LowEnRayleigh" )
    return 4;
  if( nome=="G4LECSRayleigh" )
    return 4;

  if( nome=="Transportation" )
    return 5;

  return 99;
}

//GJ:
G4double AgataSensitiveDetector::EnergyToLight(G4double en){
  if (en <  0.4*MeV)                  return (0.00+(0.06798*en)+(0.06034*en*en)+(0.05527*en*en*en))*10.;
  if (en >= 0.4*MeV && en <  1.1*MeV) return (0.0089+(0.01275*en)+(0.18149*en*en)-(0.03206*en*en*en))*10.;
  if (en >= 1.1*MeV && en <  3.0*MeV) return (-0.02060+(0.09088*en)+(0.11066*en*en)-(0.01055*en*en*en))*10.;
  if (en >= 3.0*MeV && en <  6.4*MeV) return (-0.07129+(0.18958*en)+(0.06138*en*en)-(0.00326*en*en*en))*10.;
  if (en >= 6.4*MeV && en <  8.9*MeV) return (3.85138-(1.36975*en)+(0.26122*en*en)-(0.01138*en*en*en))*10.;
  if (en >= 8.9*MeV && en < 16.0*MeV) return (-0.76801+(0.57306*en)+(0.0*en*en)-(0.0*en*en*en))*10.;
  G4cout<<endl<<"ERROR\nEnergy loss range exceeded (while calculated light production) - energy = "<<en/MeV<<" MeV"<<endl;
  return (-0.76801+(0.57306*en)+(0.0*en*en)-(0.0*en*en*en))*10.;
  //return -100.;
}

//GJ:
G4double AgataSensitiveDetector::LightProduced(AgataHitDetector *theHit){//, const G4Event* evt){
  
  G4String particle   = theHit->GetChiEStato();
  G4double edep       = theHit->GetEdep();
  G4double PostStepEn = theHit->GetKineticEn();
  G4double PreStepEn  = PostStepEn + edep;
  G4double Light      = 0.;
  G4double PreLight,PostLight;
  G4double FinalLightOutput;

  /**/ if(particle == "proton"    ) Light = EnergyToLight(PreStepEn)   - EnergyToLight(PostStepEn);
  else if(particle == "deuteron"  ) Light = 2.*(EnergyToLight(PreStepEn/2.)- EnergyToLight(PostStepEn/2.));
  else if(particle.rfind("C12")==0) Light = 0.017*edep*10.;
  else if(particle.rfind("Be9")==0) Light = 0.013*edep*10.;
  else if(particle == "alpha"     ){
    if (PreStepEn  < 6.76*MeV)  PreLight  = (0.02017*pow(PreStepEn, 1.871))*10.;
    else                        PreLight  = (-0.6728+0.1994*PreStepEn )*10.;
    if (PostStepEn < 6.76*MeV)  PostLight = (0.02017*pow(PostStepEn,1.871))*10.;
    else                        PostLight = (-0.6728+0.1994*PostStepEn)*10.;
    Light = PreLight - PostLight;
  }
  else if(particle == "e-"        ) Light = edep*10.;
  else if(particle == "gamma"     ) Light = 0.99*edep*10.;
  else{
    //G4cout<<endl<<"I dont know how to produce light for this particle: "<<particle<<endl;
    return 0.;
  }

  FinalLightOutput = Light;

  //?!?!
  FinalLightOutput = G4RandGauss::shoot(Light, 1./2.35482* sqrt((0.045*0.045*Light*Light)+(0.075*0.075*Light)+(0.002*0.002)));

  if(FinalLightOutput<0.)FinalLightOutput=0.;

  FinalLightOutput/=10.;
  
  return FinalLightOutput;
}

//GJ:

G4int AgataSensitiveDetector::ParticleID(AgataHitDetector *theHit)
{
  G4String particle   = theHit->GetChiEStato();
  if(particle == "proton"    ) return 1;
  if(particle == "deuteron"  ) return 2;
  if(particle.rfind("C12")==0) return 3;
  if(particle.rfind("Be9")==0) return 4;
  if(particle == "alpha"     ) return 5;
  if(particle == "e-"        ) return 6;
  if(particle == "gamma"     ) return 7;
  G4cout<<endl<<"I dont know how to produce light for this particle: "<<particle<<endl;
  return 0;
}



///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataSensitiveDetectorMessenger::AgataSensitiveDetectorMessenger( AgataSensitiveDetector* pTarget, G4String name )
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/tracking/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of tracking method.");
    
  commandName = directoryName + "method";
  aLine = commandName.c_str();
  SetMethodCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetMethodCmd->SetGuidance("Define method for tracking.");
  SetMethodCmd->SetGuidance("  0--> Standard tracking (default)");
  //  SetMethodCmd->SetGuidance("  1--> Standard tracking (events with primary gammas only)");
  SetMethodCmd->SetGuidance("  1--> Gammas only (including pair production)");
  //  SetMethodCmd->SetGuidance("  3--> Gammas and neutrons (including pair production and inelastic scattering)");
  SetMethodCmd->SetGuidance("Required parameters: 1 integer (0--1).");
  SetMethodCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters");
  StatusCmd->SetGuidance("Required parameters: none.");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataSensitiveDetectorMessenger::~AgataSensitiveDetectorMessenger()
{
  delete myDirectory;
  delete StatusCmd;
  delete SetMethodCmd;
}

void AgataSensitiveDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetMethodCmd ) {
    myTarget->SetMethod(SetMethodCmd->GetNewIntValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus();
  }
}

