#include "AgataNuclearDecay.hh"
#include "G4HadronicProcessType.hh"


AgataNuclearDecay::AgataNuclearDecay(const G4String& processName) : 
  G4VRestDiscreteProcess(processName, fDecay)

{
  SetProcessSubType(fRadioactiveDecay); 
  char* env = getenv("G4LEVELGAMMADATA");
  if(env == 0){
      G4cout << "G4NuclarLevelStore: please set the G4LEVELGAMMADATA environment variable\n";
      dirName = "";
  } else {
    dirName = env;
    dirName += '/';
  }
}

AgataNuclearDecay::~AgataNuclearDecay()

{
  ;
}


G4double AgataNuclearDecay::GetMeanFreePath(const G4Track& aTrack,
					    G4double previousStepSize,
					    G4ForceCondition* condition)

{
  condition=condition;
  previousStepSize=previousStepSize;
  aTrack.GetGlobalTime();
  const G4DynamicParticle* theParticle = aTrack.GetDynamicParticle();
  G4ParticleDefinition* theParticleDef = theParticle->GetDefinition();
  G4int A = ((const G4Ions*)(theParticleDef))->GetAtomicMass();
  G4int Z = ((const G4Ions*)(theParticleDef))->GetAtomicNumber();
  if(LoadGammaDecays(Z,A)){
    return DBL_MAX;
  } else  return DBL_MAX;
}


G4double AgataNuclearDecay::GetMeanLifeTime(const G4Track& aTrack,
					    G4ForceCondition* condition)

{
  aTrack.GetGlobalTime();
  condition=condition;
  return DBL_MAX;
}




G4double AgataNuclearDecay::AtRestGetPhysicalInteractionLength(const G4Track& track,G4ForceCondition* condition)

{
  track.GetGlobalTime();
  condition=condition;
  return DBL_MAX;
}

G4VParticleChange* AgataNuclearDecay::AtRestDoIt(const G4Track& theTrack,
						 const G4Step& theStep)

{
  theTrack.GetGlobalTime();
  theStep.GetStepLength();
  return 0;
}

G4VParticleChange* AgataNuclearDecay::PostStepDoIt(const G4Track& theTrack,
						   const G4Step& theStep)


{
  theTrack.GetGlobalTime();
  theStep.GetStepLength();
  return 0;
}


bool AgataNuclearDecay::LoadGammaDecays(G4int Z,G4int A)

{
  if(fGammas.find(1000*Z+A)!=fGammas.end()) return true;
  std::ostringstream filename;
  filename << dirName << "z" << Z << ".a" << A;
  std::ifstream datafile(filename.str().c_str());
  if(datafile.good()){
    G4cout << " Reading data from " << filename.str() << G4endl;
    std::vector<AgataGamma> &gammas = fGammas[1000*Z+A];
    std::string oneline = "first";
    std::istringstream thisline;
    G4double fEi; G4double fEgamma; G4double fprob; G4int fdJ;
    G4double ftau; G4int fJi;  G4double ftot_ic;  G4double ficK;
    G4double ficL1;  G4double ficL2;  G4double ficL3;  G4double ficM1;
    G4double ficM2;  G4double ficM3;  G4double ficM4;  G4double ficM5;
    G4double ficOS;  G4double fMul_mixing;
    while(datafile.good() && oneline!=""){
      oneline=="";
      std::getline(datafile,oneline);
      if(oneline!="" && datafile.good()){
	thisline.clear();
	thisline.str(oneline);
	thisline >> fEi >> fEgamma >> fprob >> fdJ >> ftau  >> fJi   >> ftot_ic  >> ficK
		 >> ficL1  >> ficL2  >> ficL3  >> ficM1 >> ficM2  >> ficM3  >> ficM4  >> ficM5
		 >> ficOS; fMul_mixing=DBL_MIN;  
	gammas.push_back(AgataGamma(fEi,fEgamma,fprob,fdJ,ftau,fJi,ftot_ic,ficK,ficL1, 
				    ficL2,ficL3,ficM1,ficM2,ficM3,ficM4,ficM5,ficOS,
				    fMul_mixing));
      }
    }
    G4cout << "Done" << G4endl;
    return true;
  } else {
    G4cout << " Could not fine " << filename.str() << G4endl;
    return false;
  }
}
