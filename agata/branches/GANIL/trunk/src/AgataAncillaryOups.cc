#ifdef ANCIL
#include "AgataAncillaryOups.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"




AgataAncillaryOups::AgataAncillaryOups(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  dirName     = name;

  
  matShell    = NULL;
  matName     = "Aluminum";


  ancSD       = NULL;
  ancName     = G4String("Oups");
  ancOffset   = 28000;
  
  numAncSd = 0;
}

AgataAncillaryOups::~AgataAncillaryOups()
{}

G4int AgataAncillaryOups::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryOups::InitSensitiveDetector()
{}


void AgataAncillaryOups::GetDetectorConstruction()
{  
	
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
  
}


void AgataAncillaryOups::Placement()
{	
  //    m_gdmlparser.Read("gdml/HoneyComb.gdml");
  std::string path="/home/joa/AGATA/agataganil/4Oups";
  if(getenv("G4OUPSPATH")) path=std::string(getenv("G4OUPSPATH"));
  path+="/Oups.gdml";
  m_gdmlparser.Read(path);
  m_LogicalVol0=0;
  m_LogicalVol1=0;
  m_LogicalVol2=0;
  m_LogicalVol3=0;
  m_LogicalVol4=0;
  m_LogicalVol5=0;
  m_LogicalVol6=0;
  //m_LogicalVol2= m_gdmlparser.GetVolume("Oups_world");
  m_LogicalVol0= m_gdmlparser.GetVolume("Element_Step_0__vol");
  m_LogicalVol1= m_gdmlparser.GetVolume("Element_Step_1__vol");
  m_LogicalVol2= m_gdmlparser.GetVolume("Element_Step_2__vol");
  m_LogicalVol3= m_gdmlparser.GetVolume("Element_Step_3__vol");
  m_LogicalVol4= m_gdmlparser.GetVolume("Element_Step_4__vol");
  m_LogicalVol5= m_gdmlparser.GetVolume("Element_Step_5__vol");
  m_LogicalVol6= m_gdmlparser.GetVolume("Element_Step_6__vol");
  if(!m_LogicalVol2){
    G4cout << "Did not find Oups_world!!!\n";
    exit(-1);
  }

    G4RotationMatrix* rm= new G4RotationMatrix();
    G4RotationMatrix rmY, rmZ;
    rmZ.rotateZ(0.*deg);
    rmY.rotateY(180.*deg);
    
    *rm=rmY*rmZ;
    
	//G4Transform3D TF(rm, rm*G4ThreeVector(0., 0., 0.));

    // gdml World box
    //m_LogicalVol2->SetVisAttributes(G4VisAttributes::Invisible); 
    //G4VisAttributes *AttVol2 = new G4VisAttributes(G4Colour(1.0, 1.0, 0.0)); 
    //AttVol2->SetForceSolid(true);
    //m_LogicalVol2->SetVisAttributes(AttVol2);
    
    G4VisAttributes *AttVol0 = new G4VisAttributes(G4Colour(1.0,0,0)); 
    AttVol0->SetForceSolid(true);
    G4VisAttributes *AttVol1 = new G4VisAttributes(G4Colour(0, 1,0)); 
    AttVol1->SetForceSolid(true);
    G4VisAttributes *AttVol2 = new G4VisAttributes(G4Colour( 0, .0, 1)); 
    AttVol2->SetForceSolid(true);
    G4VisAttributes *AttVol3 = new G4VisAttributes(G4Colour(1.0, 1, 0)); 
    AttVol3->SetForceSolid(true);
    G4VisAttributes *AttVol4 = new G4VisAttributes(G4Colour(0, 1, 1)); 
    AttVol4->SetForceSolid(true);
    G4VisAttributes *AttVol5 = new G4VisAttributes(G4Colour(1, 0, 1)); 
    AttVol5->SetForceSolid(true);
    G4VisAttributes *AttVol6 = new G4VisAttributes(G4Colour(1.0, 1, 1)); 
    AttVol6->SetForceSolid(true);
    m_LogicalVol0->SetVisAttributes(AttVol0);
    m_LogicalVol1->SetVisAttributes(AttVol1);
    m_LogicalVol2->SetVisAttributes(AttVol2); 
    m_LogicalVol3->SetVisAttributes(AttVol3);
    m_LogicalVol4->SetVisAttributes(AttVol4);
    m_LogicalVol5->SetVisAttributes(AttVol5);
    m_LogicalVol6->SetVisAttributes(AttVol6); 
      
    new G4PVPlacement(rm, G4ThreeVector(0.,
					-18.090724298813,
					0.076957591297),
		      "Oups0", m_LogicalVol0, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(-0.309094196138,
					30.8185475621400,
					14.348933848200),
		      "Oups1", m_LogicalVol1, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(-0.309089763211,
					4.693547562145,
					8.531373380425),
		      "Oups2", m_LogicalVol2, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(25.911126634500,
					7.802693955974,
					-7.116835345450),
		      "Oups3", m_LogicalVol3, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(26.537697785450,
					-15.681449860400,
					14.348933848200),
		      "Oups4", m_LogicalVol4, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(-27.155877249140,
					-15.681455015331,
					14.348933848200),
		      "Oups5", m_LogicalVol5, theDetector->HallPhys(),
		      false, 0 );
    new G4PVPlacement(rm, G4ThreeVector(40.088060639610,
					-40.499999999995,
					69.434557804700),
		      "Oups6", m_LogicalVol6, theDetector->HallPhys(),
		      false, 0 );
	return ;
  
}


void AgataAncillaryOups::ShowStatus()
{
  G4cout << " Oups has been constructed." << G4endl;
}

void AgataAncillaryOups::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
