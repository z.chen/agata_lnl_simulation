#ifdef ANCIL
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataAncillaryEuclides.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Polyhedra.hh"
#include "G4Tubs.hh"
#include "G4SubtractionSolid.hh"
#include "CConvexPolyhedron.hh"
#include "G4LogicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include <vector>

using namespace std;

AgataAncillaryEuclides::AgataAncillaryEuclides( G4String path, G4String name )
{
  iniPath  = path;
  dirName  = name;
  
  pAbsorPS = NULL;
  pAbsorPL = NULL;
  pAbsorPP = NULL;

  pDetDIPS = NULL;
  pDetDIPL = NULL;
  pDetDIPP = NULL;

  pDetDOPS = NULL;
  pDetDOPL = NULL;
  pDetDOPP = NULL;

  pDetEIPS = NULL;
  pDetEIPL = NULL;
  pDetEIPP = NULL;

  pDetEOPS = NULL;
  pDetEOPL = NULL;
  pDetEOPP = NULL;

  pFramePS = NULL;
  pframePS = NULL;
  pFramePL = NULL;
  pFramePP = NULL;

  pAbsorHS = NULL;
  pAbsorHL = NULL;
  pAbsorHP = NULL;

  pDetDIHS = NULL;
  pDetDIHL = NULL;
  pDetDIHP = NULL;

  pDetDOHS = NULL;
  pDetDOHL = NULL;
  pDetDOHP = NULL;

  pDetEIHS = NULL;
  pDetEIHL = NULL;
  pDetEIHP = NULL;

  pDetEOHS = NULL;
  pDetEOHL = NULL;
  pDetEOHP = NULL;

  pFrameHS = NULL;
  pFrameHL = NULL;
  pFrameHP = NULL;

  pAbsorSS = NULL;
  pAbsorSL = NULL;
  pAbsorSP = NULL;

  pDetDISS = NULL;
  pDetDISL = NULL;
  pDetDISP = NULL;

  pDetDOSS = NULL;
  pDetDOSL = NULL;
  pDetDOSP = NULL;

  pDetEISS = NULL;
  pDetEISL = NULL;
  pDetEISP = NULL;

  pDetEOSS = NULL;
  pDetEOSL = NULL;
  pDetEOSP = NULL;

  pFrameSS = NULL;
  pFrameSL = NULL;
  pFrameSP = NULL;
  
  matCryst = NULL;
  matAbsor = NULL;
  matAbsoI = NULL;
  matFrame = NULL;

  for( G4int ij=0; ij<5; ij++ ) {
     pAbsoIHS[ij] = NULL;
     pAbsoIHL[ij] = NULL;
     pAbsoIHP[ij] = NULL;
     pAbsoIPS[ij] = NULL;
     pAbsoIPL[ij] = NULL;
     pAbsoIPP[ij] = NULL;
     spesAbI [ij] = 0.;
  }
  
  ancSD    = NULL;
  ancSE    = NULL;
  
  ancName   = G4String("EUCLIDES");
  ancOffset = 4000;
  
  matCrystName = "Silicon";
  matAbsorName = "Upilex";
  matAbsoIName = "Vacuum";
  matFrameName = "FR4";
//  matFrameName = "Vacuum";
  
  geomType = 0; // Euclides; 1 --> ISIS

  spesAbs  = 0.006*mm;
  distADE  = 0.100*mm;
  distEDE  = 0.100*mm;
  distAAI  = 0.001*mm;
  
  innerRadius = 16.*mm;   

  cylAbs.clear();

  nLayers = 1;
  cylAbs.push_back(cylLayer());

  if( geomType )
    eulerFile = iniPath + "Ancillary/numbering.isis";
  else
    eulerFile = iniPath + "Ancillary/numbering.euclides";
  
  numAncSd = 0;
  
  myMessenger = new AgataAncillaryEuclidesMessenger(this,name);
}

AgataAncillaryEuclides::~AgataAncillaryEuclides()
{
  cylAbs.clear();
  delete myMessenger;
}

#if 0
// This material definition should be included into AgataDetectorConstruction
// otherwise the program will crash (cross sections are loaded after DetectorConstruction
// is instantiated)
void AgataAncillaryEuclides::DefineSpecificMaterials()
{
  G4double a, z, density;
  G4double fractionmass;
  G4String name, symbol;
  G4int    nelements, natoms;
  G4int    ncomponents;

  std::vector<G4Element*>  myElements;    // save pointers here to avoid
  std::vector<G4Material*> myMaterials;   // warnings of unused variables

  G4Element* elO  = new G4Element(name="Oxigen",   symbol="O",  z=8.,  a= 15.9994 *g/mole);
  myElements.push_back(elO);

  G4Element* elH  = new G4Element(name="Hydrogen", symbol="H",  z=1.,  a= 1.007940*g/mole);
  myElements.push_back(elH);

  G4Element* elC  = new G4Element(name="Carbon",   symbol="C",  z=6.,  a= 12.00000*g/mole);
  myElements.push_back(elC);

  G4Element* elSi = new G4Element(name="Silicon",  symbol="Si", z=14., a= 28.0855*g/mole);
  myElements.push_back(elSi);

  G4Element* elN  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a=14.01*g/mole);
  myElements.push_back(elN);

  G4Material* Upilex = new G4Material(name="Upilex", density=1.470*g/cm3, ncomponents=4);
  Upilex->AddElement(elH, natoms=6);
  Upilex->AddElement(elO, natoms=4);
  Upilex->AddElement(elC, natoms=16);
  Upilex->AddElement(elN, natoms=2);
  myMaterials.push_back(Upilex);
  
  G4Material* Quarz = new G4Material(name="Quarz", density=2.66*g/cm3, nelements=2);
  Quarz->AddElement(elSi, natoms=1);
  Quarz->AddElement(elO,  natoms=2);
  myMaterials.push_back(Quarz);
  
  G4Material* Epoxy = new G4Material(name="Epoxy", density=1.470*g/cm3, ncomponents=3);
  Epoxy->AddElement(elH, natoms=18);
  Epoxy->AddElement(elO, natoms= 4);
  Epoxy->AddElement(elC, natoms=21);
  myMaterials.push_back(Epoxy);
  
  G4Material* Cu = new G4Material(name="Copper", z=29., a=63.546*g/mole, density=8.920*g/cm3);
  myMaterials.push_back(Cu);
  
  G4Material* FR4 = new G4Material(name="FR4", density=1.670*g/cm3, ncomponents=3);
  FR4->AddMaterial(Quarz, fractionmass=70.*perCent);
  FR4->AddMaterial(Epoxy, fractionmass=22.*perCent);
  FR4->AddMaterial(Cu,    fractionmass= 8.*perCent);
  myMaterials.push_back(FR4);

  G4cout << *(G4Material::GetMaterialTable()) << G4endl;
}
#endif

G4int AgataAncillaryEuclides::FindMaterials()
{
  // search the material by its name: crystals
  G4Material* ptMaterial = G4Material::GetMaterial(matCrystName);
  if (ptMaterial) {
    matCryst = ptMaterial;
    G4cout << " ----> The crystals material is " << matCryst->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matCrystName << G4endl;
    G4cout << " Could not build the array! " << G4endl;
    return 1;
  }
  
  // fixed absorber
  ptMaterial = G4Material::GetMaterial(matAbsorName);
  if (ptMaterial) {
    matAbsor = ptMaterial;
    G4cout << " ----> The fixed absorber material is " << matAbsor->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matAbsorName << G4endl;
    G4cout << " Could not build the fixed absorbers! " << G4endl;
    return 1;
  }
  
  // individual absorbers
  ptMaterial = G4Material::GetMaterial(matAbsoIName);
  if (ptMaterial) {
    matAbsoI = ptMaterial;
    G4cout << " ----> The individual absorber material is " << matAbsoI->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matAbsoIName << G4endl;
    G4cout << " Could not build the individual absorbers! " << G4endl;
    return 1;
  }
  
  // Cylindrical absorber(s)
  cylLayer* pCyl = NULL;
  for( G4int ii=0; ii<nLayers; ii++ ) {
    pCyl = &cylAbs[ii];
    ptMaterial = G4Material::GetMaterial(pCyl->matAbsName);
    if (ptMaterial) {
      pCyl->matAbs = ptMaterial;
      G4cout << " ----> The cylinder material for layer " << ii << " is " << pCyl->matAbs->GetName() << G4endl;
    }
    else {
      G4cout << " Could not find the material " << pCyl->matAbsName << G4endl;
      G4cout << " Could not build layer " << ii << G4endl;
    }
  }

  // frames
  ptMaterial = G4Material::GetMaterial(matFrameName);
  if (ptMaterial) {
    matFrame = ptMaterial;
    G4cout << " ----> The frame material is " << matFrame->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matFrameName << G4endl;
    G4cout << " Could not build the frames! " << G4endl;
  }

  return 0;  
}

void AgataAncillaryEuclides::InitSensitiveDetector()
{
  G4int depth = 1;
  G4bool menu = false;
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  G4int offset, offset1;
#ifdef FIXED_OFFSET
  offset  = ancOffset;
  offset1 = offset + 1000;
#else
  offset  = theDetector->GetAncillaryOffset();
  offset1 = theDetector->GetAncillaryOffset();
#endif    

  if( !ancSD ) {
    ancSD = new AgataSensitiveDetector( dirName, "/anc/dE", "AncDECollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
  if( !ancSE ) {
    ancSE = new AgataSensitiveDetector( dirName, "/anc/E", "AncECollection", offset1, depth, menu );
    SDman->AddNewDetector( ancSE );
    numAncSd++;
  } 
}

void AgataAncillaryEuclides::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}


void AgataAncillaryEuclides::Placement()
{
  nSegmented = 0;
  nDets = 0;
  iGMin = 10000000;  
  
  ReadEulerFile();  
  ConstructAll();
  PlaceAll();

}

void AgataAncillaryEuclides::ReadEulerFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2;
  float     psi, th, ph;
  
  if( (fp = fopen(eulerFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << eulerFile << G4endl;
    exit(-1);
  }

  eulero.clear();

  G4cout << "\nReading Euler angles from file " << eulerFile << " ..." << G4endl;
  nEuler = 0;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 10) continue;
    if(line[0] == '#') continue;
//    if(sscanf(line,"%d %d %lf %lf %lf", &i1, &i2, &psi, &th, &ph) != 5)
    if(sscanf(line,"%d %d %f %f %f", &i1, &i2, &psi, &th, &ph) != 5)
      break;
    eulero.push_back( angles() );
    eulero[nEuler].numPhys = i1-1;
    eulero[nEuler].whichGe = i2;
    eulero[nEuler].ps      = ((G4double)psi)*deg;
    eulero[nEuler].th      = ((G4double)th)*deg;
    eulero[nEuler].ph      = ((G4double)ph)*deg;
    nEuler++;
  }

  fclose(fp);
  G4cout << nEuler << " Euler angles read." << G4endl;
}

void AgataAncillaryEuclides::ConstructThePentagons()
{
  G4cout << " Building pentagonal Si-detectors ..." << G4endl;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);

  G4double l1;
  G4double l4 = 0.5*spesAbs;
  G4double theta = 90.*deg;
  
  G4int    i;
  //  G4bool   movePlane;
  
  std::vector<G4Point3D> vertex;
  vertex.resize(10);
  
  l1 = 17.7940719251*mm;
  l1 = l1/cos(36.*deg);
  
  for( i=0; i<5; i++ ) {
    vertex[  i].set( l1*cos(theta), l1*sin(theta), -l4);
    vertex[i+5].set( l1*cos(theta), l1*sin(theta),  l4);
    theta += 72.*deg;
  }
  
  // Fixed absorber

  pAbsorPS = new CConvexPolyhedron( G4String("AbsPS"), vertex );

  pAbsorPL = new G4LogicalVolume(pAbsorPS, matAbsor, "AbsPL", 0, 0, 0);
  
  // Delta E (passive)
  l4 = 0.065 * mm;
  for( i=0; i<5; i++ ) {
    vertex[  i].setZ(-l4);
    vertex[i+5].setZ( l4);
  }  


   pDetDOPS = new CConvexPolyhedron( G4String("dEOPS"), vertex );
   pDetDOPL = new G4LogicalVolume(pDetDOPS, matCryst, "dEOPL", 0, 0, 0);
  
  // Delta E (active)
  G4double dist = -1. * mm;

   pDetDIPS = new CConvexPolyhedron( G4String("dEIPS"), vertex );

   for( i=2; i<7; i++ )
     /*movePlane = */pDetDIPS->MovePlane( i, dist );
  pDetDIPL = new G4LogicalVolume(pDetDIPS, matCryst, "dEIPL", 0, 0, 0);
  pDetDIPL->SetSensitiveDetector( ancSD );
  pDetDIPP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDIPL,
                                  "dEIPP", pDetDOPL, false, 0 );
  
  // E (passive)
  l4 = 0.5 * mm;
  for( i=0; i<5; i++ ) {
    vertex[  i].setZ(-l4);
    vertex[i+5].setZ( l4);
  }  

  pDetEOPS = new CConvexPolyhedron( G4String("EOPS"), vertex );

  pDetEOPL = new G4LogicalVolume(pDetEOPS, matCryst, "EOPL", 0, 0, 0);
  // E (active)

   pDetEIPS = new CConvexPolyhedron( G4String("EIPS"), vertex );

   for( i=2; i<7; i++ )
     /*movePlane = */pDetEIPS->MovePlane( i, dist );
  pDetEIPL = new G4LogicalVolume(pDetEIPS, matCryst, "EIPL", 0, 0, 0);
  pDetEIPL->SetSensitiveDetector( ancSE );
  pDetEIPP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEIPL,
                                  "EIPP", pDetEOPL, false, 0 );

  vertex.clear();
                                  
  G4VisAttributes* dEVA = new G4VisAttributes( G4Color(1., 0., 0.) );                                
  G4VisAttributes* dIVA = new G4VisAttributes( G4Color(1., 0., 0.) );                                
  G4VisAttributes*  EVA = new G4VisAttributes( G4Color(0., 0., 1.) );                                
  G4VisAttributes* abVA = new G4VisAttributes( G4Color(0.5, 0.5, 0.5) );
  dIVA->SetForceWireframe(true);
  dIVA->SetVisibility(false);
  
  pAbsorPL->SetVisAttributes( abVA );                              
  pDetDIPL->SetVisAttributes( dIVA );                              
  pDetDOPL->SetVisAttributes( dEVA );                              
  pDetEIPL->SetVisAttributes(  EVA );                              
  pDetEOPL->SetVisAttributes(  EVA );                              
}

void AgataAncillaryEuclides::ConstructTheHexagons()
{
  G4cout << " Building hexagonal Si-detectors ..." << G4endl;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);

  G4double l1, l2, l3;
  G4double l4 = 0.5*spesAbs;
  
  G4int    i;
  //  G4bool   movePlane;
  
  std::vector<G4Point3D> vertex;
  vertex.resize(12);
  
  l1 = 22.994051051 * mm;
  l2 = 13.131000000 * mm;
  l3 = 15.957178963 * mm;
  
  vertex[ 0].set( l1, 0.,-l4 );
  vertex[ 1].set( l2, l3,-l4 );
  vertex[ 2].set(-l2, l3,-l4 );
  vertex[ 3].set(-l1, 0.,-l4 );
  vertex[ 4].set(-l2,-l3,-l4 );
  vertex[ 5].set( l2,-l3,-l4 );
  vertex[ 6].set( l1, 0., l4 );
  vertex[ 7].set( l2, l3, l4 );
  vertex[ 8].set(-l2, l3, l4 );
  vertex[ 9].set(-l1, 0., l4 );
  vertex[10].set(-l2,-l3, l4 );
  vertex[11].set( l2,-l3, l4 );
  
  // Fixed absorber

   pAbsorHS = new CConvexPolyhedron( G4String("AbsHS"), vertex );

   pAbsorHL = new G4LogicalVolume(pAbsorHS, matAbsor, "AbsHL", 0, 0, 0);
  
  // Delta E (passive)
  l4 = 0.065 * mm;
  for( i=0; i<6; i++ )
    vertex[i].setZ(-l4);
  for( i=6; i<12; i++ )
    vertex[i].setZ( l4);

  pDetDOHS = new CConvexPolyhedron( G4String("dEOHS"), vertex );

  pDetDOHL = new G4LogicalVolume(pDetDOHS, matCryst, "dEOHL", 0, 0, 0);
  
  // Delta E (active)
  G4double dist = -1. * mm;

   pDetDIHS = new CConvexPolyhedron( G4String("dEIHS"), vertex );

  for( i=2; i<8; i++ )
    /*movePlane = */pDetDIHS->MovePlane( i, dist );
  pDetDIHL = new G4LogicalVolume(pDetDIHS, matCryst, "dEIHL", 0, 0, 0);
  pDetDIHL->SetSensitiveDetector( ancSD );
  pDetDIHP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDIHL,
                                  "dEIHP", pDetDOHL, false, 0 );
  
  // E (passive)
  l4 = 0.5 * mm;
  
  for( i=0; i<6; i++ )
    vertex[i].setZ(-l4);
  for( i=6; i<12; i++ )
    vertex[i].setZ( l4);


  pDetEOHS = new CConvexPolyhedron( G4String("EOHS"), vertex );

  pDetEOHL = new G4LogicalVolume(pDetEOHS, matCryst, "EOHL", 0, 0, 0);
  // E (active)

  pDetEIHS = new CConvexPolyhedron( G4String("EIHS"), vertex );

  for( i=2; i<8; i++ )
    /*movePlane = */pDetEIHS->MovePlane( i, dist );
  pDetEIHL = new G4LogicalVolume(pDetEIHS, matCryst, "EIHL", 0, 0, 0);
  pDetEIHL->SetSensitiveDetector( ancSE );
  pDetEIHP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEIHL,
                                  "EIHP", pDetEOHL, false, 0 );

  vertex.clear();
                                  
  G4VisAttributes* dEVA = new G4VisAttributes( G4Color(1., 0., 0.) );                                
  G4VisAttributes* dIVA = new G4VisAttributes( G4Color(1., 0., 0.) );                                
  G4VisAttributes*  EVA = new G4VisAttributes( G4Color(0., 0., 1.) );                                
  G4VisAttributes* abVA = new G4VisAttributes( G4Color(0.5, 0.5, 0.5) );
  dIVA->SetForceWireframe(true);
  dIVA->SetVisibility(false);
  
  pAbsorHL->SetVisAttributes( abVA );                              
  pDetDIHL->SetVisAttributes( dIVA );                              
  pDetDOHL->SetVisAttributes( dEVA );                              
  pDetEIHL->SetVisAttributes(  EVA );                              
  pDetEOHL->SetVisAttributes(  EVA );                              
}

void AgataAncillaryEuclides::ConstructTheSegmented()
{
  G4cout << " Building hexagonal segmented Si-detectors ..." << G4endl;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);

  G4double l1, l2, l3;
  G4double l4 = 0.5*spesAbs;
  
  G4int    i;
  //  G4bool   movePlane;
  std::vector<G4Point3D> vertex;
  vertex.resize(12);
  
  l1 = 22.994051051 * mm;
  l2 = 13.131000000 * mm;
  l3 = 15.957178963 * mm;
  
  vertex[ 0].set( l1, 0.,-l4 );
  vertex[ 1].set( l2, l3,-l4 );
  vertex[ 2].set(-l2, l3,-l4 );
  vertex[ 3].set(-l1, 0.,-l4 );
  vertex[ 4].set(-l2,-l3,-l4 );
  vertex[ 5].set( l2,-l3,-l4 );
  vertex[ 6].set( l1, 0., l4 );
  vertex[ 7].set( l2, l3, l4 );
  vertex[ 8].set(-l2, l3, l4 );
  vertex[ 9].set(-l1, 0., l4 );
  vertex[10].set(-l2,-l3, l4 );
  vertex[11].set( l2,-l3, l4 );
  
  // Fixed absorber

   pAbsorSS = new CConvexPolyhedron( G4String("AbsSS"), vertex );

   pAbsorSL = new G4LogicalVolume(pAbsorSS, matAbsor, "AbsSL", 0, 0, 0);
  
  // Delta E (passive)
  l4 = 0.065 * mm;
  for( i=0; i<6; i++ )
    vertex[i].setZ(-l4);
  for( i=6; i<12; i++ )
    vertex[i].setZ( l4);

  pDetDOSS = new CConvexPolyhedron( G4String("dEOSS"), vertex );

  pDetDOSL = new G4LogicalVolume(pDetDOSS, matCryst, "dEOSL", 0, 0, 0);
  
  // Delta E segments (active)
  std::vector<G4Point3D> vertex1;
  vertex1.resize(8);
  
  vertex1[0] = vertex[ 0];
  vertex1[1] = vertex[ 1];
  vertex1[2].set( 0., l3, -l4);
  vertex1[3].set( 0., 0., -l4);
  vertex1[4] = vertex[ 6];
  vertex1[5] = vertex[ 7];
  vertex1[6].set( 0., l3,  l4);
  vertex1[7].set( 0., 0.,  l4);
  
  G4double dist1 = -1.0 * mm;
  G4double dist2 = -0.1 * mm;

  pDetDISS = new CConvexPolyhedron( G4String("dEISS"), vertex1 );

  
  for( i=2; i<4; i++ )
    /*movePlane = */pDetDISS->MovePlane( i, dist1 );
  for( i=4; i<6; i++ )
    /*movePlane = */pDetDISS->MovePlane( i, dist2 );
    
  pDetDISL = new G4LogicalVolume(pDetDISS, matCryst, "dEISL", 0, 0, 0);
  pDetDISL->SetSensitiveDetector( ancSD );

  // E (passive)
  l4 = 0.5 * mm;
  
  for( i=0; i<6; i++ )
    vertex[i].setZ(-l4);
  for( i=6; i<12; i++ )
    vertex[i].setZ( l4);
  for( i=0; i<4; i++ )
    vertex1[i].setZ(-l4);
  for( i=4; i<8; i++ )
    vertex1[i].setZ( l4);

  pDetEOSS = new CConvexPolyhedron( G4String("EOSS"), vertex );

  pDetEOSL = new G4LogicalVolume(pDetEOSS, matCryst, "EOSL", 0, 0, 0);

  // E segments (active)
  pDetEISS = new CConvexPolyhedron( G4String("EISS"), vertex1 );

  
  for( i=2; i<4; i++ )
    /*movePlane = */pDetEISS->MovePlane( i, dist1 );
  for( i=4; i<6; i++ )
    /*movePlane = */pDetEISS->MovePlane( i, dist2 );
    
  pDetEISL = new G4LogicalVolume(pDetEISS, matCryst, "EISL", 0, 0, 0);
  pDetEISL->SetSensitiveDetector( ancSE );
  
  
  // placements of the active parts
  pDetDISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDISL,
                                  "dEISP", pDetDOSL, false, 0 );

  pDetEISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEISL,
                                  "EISP", pDetEOSL, false, 0 );
  
  rm.rotateY(180.*deg);
  pDetDISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDISL,
                                  "dEISP", pDetDOSL, false, 1 );
  
  pDetEISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEISL,
                                  "EISP", pDetEOSL, false, 1 );
  
  rm.rotateX(180.*deg);
  pDetDISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDISL,
                                  "dEISP", pDetDOSL, false, 2 );

  pDetEISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEISL,
                                  "EISP", pDetEOSL, false, 2 );

  rm.rotateY(180.*deg);
  pDetDISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetDISL,
                                  "dEISP", pDetDOSL, false, 3 );

  pDetEISP = new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pDetEISL,
                                  "EISP", pDetEOSL, false, 3 );

  vertex.clear();
  vertex1.clear();
}

void AgataAncillaryEuclides::ConstructTheFrames()
{
  G4RotationMatrix rm;
  rm.set(0, 0, 0);
  
  G4double dist = 0.2 * mm;
  CConvexPolyhedron* pDet = NULL;
  
  G4int i = 0;
  //  G4bool movePlane;
  
  std::vector<G4Point3D> vertex;
  G4double l1=0, l2=0, l3=0, l4=0, l5=0, l6=0, l7=0, theta=0;
  
  
  l1 = 17.7940719251*mm/cos(36.*deg);
  theta = 90.*deg;
  l4 = 0.065*mm;
  
  // pentagonal frame
  // Euclides
  G4double    posizZ[2] = {-6.45*mm, 6.45*mm};
  G4double    outRad[2] = {18.033914987707*mm, 22.29050520*mm};
  G4double    innRad[2] = {16.733914987707*mm, 17.29050520*mm};

  // pentagonal frame
  // ISIS
  // equivalent dimensions to generate E, DeltaE frames
  G4double    PosizZ[2] = {-7.5*mm, 7.5*mm};
  G4double    OutRad[2] = {19.344708447927*mm, 23.6049499361*mm};
  G4double    InnRad[2] = {17.444708447927*mm, 17.1049499361*mm};
  //G4double    InnRad[2] = {17.1049499361*mm, 17.444708447927*mm};

  switch( geomType )
  {
    case 0:
      pFramePS = new G4Polyhedra("FramePS", 18.*deg, 360.*deg, 5, 2, posizZ, innRad, outRad);
      pFramePL = new G4LogicalVolume(pFramePS, matFrame, "FramePL", 0, 0, 0);
      break;
    case 1:
      pFramePS = new G4Polyhedra("FramePS", 18.*deg, 360.*deg, 5, 2, PosizZ, InnRad, OutRad);
      /// subtract DE detector
      vertex.resize(10);
      for( i=0; i<5; i++ ) {
	vertex[  i].set( l1*cos(theta), l1*sin(theta), -l4);
	vertex[i+5].set( l1*cos(theta), l1*sin(theta),  l4);
	theta += 72.*deg;
      }


      pDet = new CConvexPolyhedron( G4String("DummyDE"), vertex );

      for( i=0; i<pDet->GetnPoints()/2; i++ )
        /*movePlane =*/ pDet->MovePlane( i, dist );
      pframePS = new G4SubtractionSolid("FramePS", pFramePS, pDet, 
                                          G4Transform3D(rm, G4ThreeVector(0., 0., -7.435*mm+distADE)) );
      
      /// subtract E detector
      l4 = 0.5*mm;
      theta = 90.*deg;
      for( i=0; i<5; i++ ) {
	vertex[  i].set( l1*cos(theta), l1*sin(theta), -l4);
	vertex[i+5].set( l1*cos(theta), l1*sin(theta),  l4);
	theta += 72.*deg;
      }

      pDet = new CConvexPolyhedron( G4String("DummyE"), vertex );

      for( i=0; i<pDet->GetnPoints()/2; i++ )
        /*movePlane = */pDet->MovePlane( i, dist );
      pframePS = new G4SubtractionSolid("FramePS", pframePS, pDet, 
                                          G4Transform3D(rm, G4ThreeVector(0., 0., -6.87*mm+distADE+distEDE)) );
      pFramePL = new G4LogicalVolume(pframePS, matFrame, "FramePL", 0, 0, 0);
      break;
  }

  // hexagonal frame
  // obtained as subtraction of two CConvexPolyhedron
  vertex.resize(12);
  switch( geomType )
  {
    case 0:
      l4 = 6.5*mm;
      break;
    case 1:
      l4 = 7.5*mm;
      break;
  }      
  
  // inner face
  switch( geomType )
  {
    case 0:
      l1 = 23.676657780021 * mm;
      l2 = 13.846918499175 * mm;
      l3 = 15.903284698907 * mm;
      break;
    case 1:
      l1 = 24.257917856809 * mm;
      l2 = 14.411829037572 * mm;
      l3 = 15.929736200447 * mm;
      break;
  }  
  vertex[ 0].set( l1, 0.,-l4 );
  vertex[ 1].set( l2, l3,-l4 );
  vertex[ 2].set(-l2, l3,-l4 );
  vertex[ 3].set(-l1, 0.,-l4 );
  vertex[ 4].set(-l2,-l3,-l4 );
  vertex[ 5].set( l2,-l3,-l4 );
  
  // outer face
  switch( geomType )
  {
    case 0:
      l5 = 28.6807160133 * mm;
      l6 = 16.2200000000 * mm;
      l7 = 20.1598749112 * mm;
      break;
    case 1:
      l5 = 30.0318312029 * mm;
      l6 = 17.1500000000 * mm;
      l7 = 20.8411864454 * mm;
      break;
  }  
  vertex[ 6].set( l5, 0., l4 );
  vertex[ 7].set( l6, l7, l4 );
  vertex[ 8].set(-l6, l7, l4 );
  vertex[ 9].set(-l5, 0., l4 );
  vertex[10].set(-l6,-l7, l4 );
  vertex[11].set( l6,-l7, l4 );

 
  CConvexPolyhedron* pOuterF = new CConvexPolyhedron( G4String("OuterF"), vertex );
  
  // inner face
  switch( geomType )
  {
    case 0:
      l1 = 21.741230244341 * mm;
      l2 = 12.715014342912 * mm;
      l3 = 14.603284698907 * mm;
      break;
    case 1:
      l1 = 21.364584072246 * mm;
      l2 = 12.692875576772 * mm;
      l3 = 14.029736200447 * mm;
      break;
  }  
  vertex[ 0].set( l1, 0.,-l4 );
  vertex[ 1].set( l2, l3,-l4 );
  vertex[ 2].set(-l2, l3,-l4 );
  vertex[ 3].set(-l1, 0.,-l4 );
  vertex[ 4].set(-l2,-l3,-l4 );
  vertex[ 5].set( l2,-l3,-l4 );
  
  // outer face
  switch( geomType )
  {
    case 0:
      l5 = 21.567399055821 * mm;
      l6 = 12.197157578743 * mm;
      l7 = 15.159874911200 * mm;
      break;
    case 1:
      l5 = 20.665430526516 * mm;
      l6 = 11.801216220725 * mm;
      l7 = 14.341186445400 * mm;
      break;
  }  
  vertex[ 6].set( l5, 0., l4 );
  vertex[ 7].set( l6, l7, l4 );
  vertex[ 8].set(-l6, l7, l4 );
  vertex[ 9].set(-l5, 0., l4 );
  vertex[10].set(-l6,-l7, l4 );
  vertex[11].set( l6,-l7, l4 );
  
  CConvexPolyhedron* pInnerF = new CConvexPolyhedron( G4String("InnerF"), vertex);
  
  dist = 1.0 * mm;
  
  for( i=0; i<2; i++ )

    
    /*movePlane = */pInnerF->MovePlane( i, dist );
   pFrameHS = new G4SubtractionSolid( "FrameHS", pOuterF, pInnerF, G4Transform3D(rm, G4ThreeVector()) );
   pFrameSS = new G4SubtractionSolid( "FrameSS", pOuterF, pInnerF, G4Transform3D(rm, G4ThreeVector()) );
  
 
  dist = 0.2 * mm;
  
  
  if( geomType ) {
    /// subtract dummy DE detector
    l1 = 22.994051051 * mm;
    l2 = 13.131000000 * mm;
    l3 = 15.957178963 * mm;
    l4 = 0.065*mm;

    vertex[ 0].set( l1, 0.,-l4 );
    vertex[ 1].set( l2, l3,-l4 );
    vertex[ 2].set(-l2, l3,-l4 );
    vertex[ 3].set(-l1, 0.,-l4 );
    vertex[ 4].set(-l2,-l3,-l4 );
    vertex[ 5].set( l2,-l3,-l4 );
    vertex[ 6].set( l1, 0., l4 );
    vertex[ 7].set( l2, l3, l4 );
    vertex[ 8].set(-l2, l3, l4 );
    vertex[ 9].set(-l1, 0., l4 );
    vertex[10].set(-l2,-l3, l4 );
    vertex[11].set( l2,-l3, l4 );
    
 
    pDet = new CConvexPolyhedron( G4String("DummyDE"), vertex );

    for( i=0; i<pDet->GetnPoints()/2; i++ )
      /*movePlane = */pDet->MovePlane( i, dist );
    pFrameHS = new G4SubtractionSolid( "FrameHS", pFrameHS, pDet, 
                                           G4Transform3D(rm, G4ThreeVector(0., 0., -7.435*mm+distADE)) );
    pFrameSS = new G4SubtractionSolid( "FrameSS", pFrameSS, pDet, 
                                           G4Transform3D(rm, G4ThreeVector(0., 0., -7.435*mm+distADE)) );

    /// subtract dummy E detector
    l4 = 0.5*mm;

    vertex[ 0].set( l1, 0.,-l4 );
    vertex[ 1].set( l2, l3,-l4 );
    vertex[ 2].set(-l2, l3,-l4 );
    vertex[ 3].set(-l1, 0.,-l4 );
    vertex[ 4].set(-l2,-l3,-l4 );
    vertex[ 5].set( l2,-l3,-l4 );
    vertex[ 6].set( l1, 0., l4 );
    vertex[ 7].set( l2, l3, l4 );
    vertex[ 8].set(-l2, l3, l4 );
    vertex[ 9].set(-l1, 0., l4 );
    vertex[10].set(-l2,-l3, l4 );
    vertex[11].set( l2,-l3, l4 );
    
    pDet = new CConvexPolyhedron( G4String("DummyE"), vertex );

    for( i=0; i<pDet->GetnPoints()/2; i++ )
      /*movePlane = */pDet->MovePlane( i, dist );

    pFrameHS = new G4SubtractionSolid( "FrameHS", pFrameHS, pDet, 
                                           G4Transform3D(rm, G4ThreeVector(0., 0., -6.87*mm+distADE+distEDE)) );
    pFrameSS = new G4SubtractionSolid( "FrameSS", pFrameSS, pDet, 
                                           G4Transform3D(rm, G4ThreeVector(0., 0., -6.87*mm+distADE+distEDE)) );
  }
  pFrameHL = new G4LogicalVolume(pFrameHS, matFrame, "FrameHL", 0, 0, 0);
  pFrameSL = new G4LogicalVolume(pFrameSS, matFrame, "FrameSL", 0, 0, 0);
  vertex.clear();

  G4VisAttributes* frVA = new G4VisAttributes( G4Color(0., 1., 0.) );                                

  pFramePL->SetVisAttributes( frVA );				   
  pFrameHL->SetVisAttributes( frVA );				   
  pFrameSL->SetVisAttributes( frVA );				   

}

void AgataAncillaryEuclides::ConstructTheIndAbs()
{
  G4int ii;
  for( G4int ij=0; ij<5; ij++ ) {
     pAbsoIHS[ij] = NULL;
     pAbsoIHL[ij] = NULL;
     pAbsoIHP[ij] = NULL;
     pAbsoIPS[ij] = NULL;
     pAbsoIPL[ij] = NULL;
     pAbsoIPP[ij] = NULL;
  }
  BuildRingLUT();
  
  if( matAbsoIName == "Vacuum" ) return;
  if( geomType ) return;   // only EUCLIDES!!!

  G4cout << " Building hexagonal individual absorbers ..." << G4endl;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);

  G4double l1, l2, l3, l4;
  
  std::vector<G4Point3D> vertex;
  vertex.resize(12);
  
  l1 = 22.994051051 * mm;
  l2 = 13.131000000 * mm;
  l3 = 15.957178963 * mm;
  
  for( ii=0; ii<5; ii++ ) {
    l4 = 0.5*spesAbI[ii];
    if( !l4 ) continue;   ///// builds the aborber only if thickness > 0.!!!

    vertex[ 0].set( l1, 0.,-l4 );
    vertex[ 1].set( l2, l3,-l4 );
    vertex[ 2].set(-l2, l3,-l4 );
    vertex[ 3].set(-l1, 0.,-l4 );
    vertex[ 4].set(-l2,-l3,-l4 );
    vertex[ 5].set( l2,-l3,-l4 );
    vertex[ 6].set( l1, 0., l4 );
    vertex[ 7].set( l2, l3, l4 );
    vertex[ 8].set(-l2, l3, l4 );
    vertex[ 9].set(-l1, 0., l4 );
    vertex[10].set(-l2,-l3, l4 );
    vertex[11].set( l2,-l3, l4 );

    pAbsoIHS[ii] = new CConvexPolyhedron( G4String("AbIHS"), vertex );

    pAbsoIHL[ii] = new G4LogicalVolume(pAbsoIHS[ii], matAbsoI, "AbIHL", 0, 0, 0);
  }

  G4cout << " Building pentagonal individual absorbers ..." << G4endl;
  
  G4double theta = 90.*deg;
  G4int jj;
  vertex.resize(10);
  
  l1 = 17.7940719251*mm;
  l1 = l1/cos(36.*deg);
  
  for( ii=0; ii<5; ii++ ) {
    l4 = 0.5*spesAbI[ii];
    if( !l4 ) continue;   ///// builds the aborber only if thickness > 0.!!!

    for( jj=0; jj<5; jj++ ) {
      vertex[  jj].set( l1*cos(theta), l1*sin(theta), -l4);
      vertex[jj+5].set( l1*cos(theta), l1*sin(theta),  l4);
      theta += 72.*deg;
    }

    pAbsoIPS[ii] = new CConvexPolyhedron( G4String("AbIPS"), vertex );
    pAbsoIPL[ii] = new G4LogicalVolume(pAbsoIPS[ii], matAbsoI, "AbIPL", 0, 0, 0);
  }  
}

void AgataAncillaryEuclides::BuildRingLUT()
{
  maxIndex = 0;
  ringLUT.clear();
  
  G4int nEu = 0;
  G4int size = eulero.size();

  for( nEu = 0; nEu<size; nEu++ ) {
    if( eulero[nEu].numPhys > maxIndex )
      maxIndex = eulero[nEu].numPhys;
  }
  maxIndex++; // indexes start from zero! 
  
  G4double theta;
  ringLUT.resize(maxIndex);
  for( nEu = 0; nEu<size; nEu++ ) {
    theta = eulero[nEu].th;
    if( theta < 40.*deg )
      ringLUT[nEu] = 1;
    else if( theta < 80.*deg )  
      ringLUT[nEu] = 2;
    else if( theta < 100.*deg )  
      ringLUT[nEu] = 3;
    else if( theta < 130.*deg )  
      ringLUT[nEu] = 4;
    else  
      ringLUT[nEu] = 5;
  }
  //for( nEu = 0; nEu<size; nEu++ ) {
  //  G4cout << " RingLUT " << nEu << " " << ringLUT[nEu] << G4endl;
  //}
}

void AgataAncillaryEuclides::ConstructAll()
{
  if( geomType )
    G4cout << G4endl << " Building the Si-ball in the ISIS configuration ..." << G4endl;
  else
    G4cout << G4endl << " Building the Si-ball in the EUCLIDES configuration ..." << G4endl;  
  
  
  ConstructThePentagons();
  ConstructTheHexagons();
  ConstructTheSegmented();
  if( matFrameName != "Vacuum" )
    ConstructTheFrames();
  ConstructTheCylinder();
  ConstructTheIndAbs();
}

void AgataAncillaryEuclides::PlaceAll()
{
  G4int nEu = 0;
  G4int type;
  
  G4int size = eulero.size();
  
  G4cout << G4endl << " Placing " << size << " Si-telescopes ..." << G4endl;

  for( nEu = 0; nEu<size; nEu++ ) {
    type = eulero[nEu].whichGe;
    if( eulero[nEu].numPhys < iGMin )
      iGMin = eulero[nEu].numPhys;
    switch(type)
    {
      case 0:
        PlaceAPentagon(nEu);
        break;
      case 1:
        PlaceAHexagon(nEu);
        break;
      case 2:
        PlaceASegmented(nEu);
        break;    
    }
    nDets++;
  }
  
  G4cout << " Placed successfully." << G4endl << G4endl;

  if( nSegmented ) {
    readOut = true;
    BuildSegmentLUT();
  }
  else
    readOut = false;  
  
  PlaceTheCylinder();
}

void AgataAncillaryEuclides::BuildSegmentLUT()
{
  maxIndex = 0;
  segmLUT.clear();
  
  G4int nEu = 0;
  G4int size = eulero.size();

  for( nEu = 0; nEu<size; nEu++ ) {
    if( eulero[nEu].numPhys > maxIndex )
      maxIndex = eulero[nEu].numPhys;
  }
  maxIndex++; // indexes start from zero! 
  
  segmLUT.resize(maxIndex);
  for( nEu = 0; nEu<size; nEu++ ) {
    if( eulero[nEu].whichGe == 2 )
      segmLUT[eulero[nEu].numPhys] = true;
    else  
      segmLUT[eulero[nEu].numPhys] = false;
   // G4cout << " segLUT " << nEu << " " << segmLUT[eulero[nEu].numPhys] << G4endl;   
  }
}

void AgataAncillaryEuclides::PlaceAPentagon( G4int i )
{
  G4double dist=0, dist1;
  
  switch( geomType )
  {
    case 0:
      dist = 60.865*mm;
      break;
    case 1:
      dist = 65.295*mm;
      break;  
  }
  dist1 = dist;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);
  rm.rotateZ(eulero[i].ps);
  rm.rotateY(eulero[i].th);
  rm.rotateZ(eulero[i].ph);
  
  G4ThreeVector rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetDOPP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "dEPP", pDetDOPL, theDetector->HallPhys(), false, eulero[i].numPhys );

  dist = dist1 - 0.065*mm - distADE - 0.5*spesAbs;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pAbsorPP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "AbsorP", pAbsorPL, theDetector->HallPhys(), false, eulero[i].numPhys );

  dist = dist1 + 0.5065*mm + distEDE;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetEOPP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "EPP", pDetEOPL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  switch( geomType )
  {
    case 0:
      dist = dist + 0.5*mm + 6.45*mm;
      break;
    case 1:
      dist = dist1 + ( 7.435*mm - distADE );
      break;
  }
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  if( matFrameName != "Vacuum" )
    pFramePP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "FramePP", pFramePL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  
  char line[128];
//  sprintf(line, " %2d\t%2d\t%9.5lf\t%9.5lf\t%9.5lf", i, eulero[i].numPhys,eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
  sprintf(line, " %2d\t%2d\t%9.5f\t%9.5f\t%9.5f", i, eulero[i].numPhys,eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
   
  G4cout << G4String(line) << G4endl;

  //////////////////////////////////////////////////////////////////////
  ////// Individual absorber (EUCLIDES only) ///////////////////////////
  //////////////////////////////////////////////////////////////////////
  G4int index = ringLUT[i]-1;

  if( geomType ) return;
  if( matAbsoIName == "Vacuum" ) return;
  if( !pAbsoIPS[index] ) return;

  //G4cout << " placing ... " << eulero[i].numPhys << G4endl;

  dist = dist1 - 0.065*mm - distADE - spesAbs - distAAI - 0.5*spesAbI[index];
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  pAbsoIPP[index] = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 	"AbsoIPP", pAbsoIPL[index], theDetector->HallPhys(), false, eulero[i].numPhys );


}

void AgataAncillaryEuclides::PlaceAHexagon( G4int i )
{
  G4double dist=0, dist1;
  
  switch( geomType )
  {
    case 0:
      dist = 61.465*mm;
      break;
    case 1:
      dist = 65.945*mm;
      break;  
  }
  dist1 = dist;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);
  rm.rotateZ(eulero[i].ps);
  rm.rotateY(eulero[i].th);
  rm.rotateZ(eulero[i].ph);
  
  G4ThreeVector rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetDOHP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "dEHP", pDetDOHL, theDetector->HallPhys(), false, eulero[i].numPhys );

  dist = dist1 - 0.065*mm - distADE - 0.5*spesAbs;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
 
  pAbsorHP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "AbsorH", pAbsorHL, theDetector->HallPhys(), false, eulero[i].numPhys );

  dist = dist1 + 0.5065*mm + distEDE;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetEOHP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "EHP", pDetEOHL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  switch( geomType )
  {
    case 0:
      dist = dist + 0.5*mm + 6.45*mm;
      break;
    case 1:
      dist = dist1 + ( 7.435*mm - distADE );
      break;
  }
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  if( matFrameName != "Vacuum" )
    pFrameHP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
    							      "FrameHP", pFrameHL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  
  char line[128];
//  sprintf(line, " %2d\t%2d\t%9.5lf\t%9.5lf\t%9.5lf", i, eulero[i].numPhys, eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
  sprintf(line, " %2d\t%2d\t%9.5f\t%9.5f\t%9.5f", i, eulero[i].numPhys, eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
   

  G4cout << G4String(line) << G4endl;

  //////////////////////////////////////////////////////////////////////
  ////// Individual absorber (EUCLIDES only) ///////////////////////////
  //////////////////////////////////////////////////////////////////////
  G4int index = ringLUT[i]-1;

  if( geomType ) return;
  if( matAbsoIName == "Vacuum" ) return;
  if( !pAbsoIHS[index] ) return;

  //G4cout << " placing ... " << eulero[i].numPhys << G4endl;

  dist = dist1 - 0.065*mm - distADE - spesAbs - distAAI - 0.5*spesAbI[index];
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  pAbsoIHP[index] = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 	"AbsoIHP", pAbsoIHL[index], theDetector->HallPhys(), false, eulero[i].numPhys );

}

void AgataAncillaryEuclides::PlaceASegmented( G4int i )
{
  G4double dist=0, dist1=0;
  
  switch( geomType )
  {
    case 0:
      dist = 61.465*mm;
      break;
    case 1:
      dist = 65.945*mm;
      break;  
  }
  dist1 = dist;
  
  G4RotationMatrix rm;
  rm.set(0, 0, 0);
  rm.rotateZ(eulero[i].ps);
  rm.rotateY(eulero[i].th);
  rm.rotateZ(eulero[i].ph);
  
  G4ThreeVector rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetDOSP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "dESP", pDetDOSL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
#if 0
  // segment angle calculation
  G4Point3D segCenter;                                       
  G4RotationMatrix rm1;
  rm1.set(0,0,0);
  
  G4double phi = 0.;  
  
  segCenter = G4Point3D( rm(G4Point3D(0., 0., dist)+rm1(G4Point3D(*(pDetDISS->GetCenter())))) );
  phi = segCenter.phi();
  if( phi < 0. ) 
    phi += 360. * deg;
  G4cout << " Segmented #" << eulero[i].numPhys << " 0 " << segCenter.theta()/deg << " " << phi/deg << G4endl;
  
  rm1.rotateY(180.*deg);
  segCenter = G4Point3D( rm(G4Point3D(0., 0., dist)+rm1(G4Point3D(*(pDetDISS->GetCenter())))) );
  phi = segCenter.phi();
  if( phi < 0. ) 
    phi += 360. * deg;
  G4cout << " Segmented #" << eulero[i].numPhys << " 1 " << segCenter.theta()/deg << " " << phi/deg << G4endl;

  rm1.rotateX(180.*deg);
  segCenter = G4Point3D( rm(G4Point3D(0., 0., dist)+rm1(G4Point3D(*(pDetDISS->GetCenter())))) );
  phi = segCenter.phi();
  if( phi < 0. ) 
    phi += 360. * deg;
  G4cout << " Segmented #" << eulero[i].numPhys << " 2 " << segCenter.theta()/deg << " " << phi/deg << G4endl;

  rm1.rotateY(180.*deg);
  segCenter = G4Point3D( rm(G4Point3D(0., 0., dist)+rm1(G4Point3D(*(pDetDISS->GetCenter())))) );
  phi = segCenter.phi();
  if( phi < 0. ) 
    phi += 360. * deg;
  G4cout << " Segmented #" << eulero[i].numPhys << " 3 " << segCenter.theta()/deg << " " << phi/deg << G4endl;
  // end of segment angle calculation
#endif                                        

  dist = dist1 - 0.065*mm - distADE - 0.5*spesAbs;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pAbsorSP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "AbsorS", pAbsorSL, theDetector->HallPhys(), false, eulero[i].numPhys );

  dist = dist1 + 0.5065*mm + distEDE;
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  pDetEOSP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "ESP", pDetEOSL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  switch( geomType )
  {
    case 0:
      dist = dist + 0.5*mm + 6.45*mm;
      break;
    case 1:
      dist = dist1 + ( 7.435*mm - distADE );
      break;
  }
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  
  if( matFrameName != "Vacuum" )
    pFrameSP = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 				                                 "FrameSP", pFrameSL, theDetector->HallPhys(), false, eulero[i].numPhys );
  
  
  char line[128];
//  sprintf(line, " %2d\t%2d\t%9.5lf\t%9.5lf\t%9.5lf", i, eulero[i].numPhys, eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
  sprintf(line, " %2d\t%2d\t%9.5f\t%9.5f\t%9.5f", i, eulero[i].numPhys, eulero[i].ps/deg, eulero[i].th/deg, eulero[i].ph/deg );
   
  G4cout << G4String(line) << G4endl;
  
  nSegmented++;

  //////////////////////////////////////////////////////////////////////
  ////// Individual absorber (EUCLIDES only) ///////////////////////////
  //////////////////////////////////////////////////////////////////////
  G4int index = ringLUT[i]-1;

  if( geomType ) return;
  if( matAbsoIName == "Vacuum" ) return;
  if( !pAbsoIHS[index] ) return;

  //G4cout << " placing ... " << eulero[i].numPhys << G4endl;

  dist = dist1 - 0.065*mm - distADE - spesAbs - distAAI - 0.5*spesAbI[index];
  rotatedPosition = rm(G4ThreeVector(0., 0., dist));
  pAbsoIHP[index] = new G4PVPlacement( G4Transform3D(rm, rotatedPosition),
 	"AbsoIHP", pAbsoIHL[index], theDetector->HallPhys(), false, eulero[i].numPhys );

}

void AgataAncillaryEuclides::ConstructTheCylinder()
{
  G4double Z1, Z2;
  G4double length;
  G4double innRadius = innerRadius;
  G4double outRadius = innerRadius;
  G4int ii;
  char sName[128];
  
  cylLayer* pCyl = NULL;
  
  for( ii=0; ii<nLayers; ii++ ) {
    pCyl = &cylAbs[ii];
    if( pCyl->matAbsName == "Vacuum" ) continue;
    innRadius  = outRadius;
    outRadius += pCyl->thickAbs; 

    Z1 = innRadius / tan( pCyl->thet1Abs );
    if( Z1 > 7.000*cm )  Z1 =  7.000*cm;					        
    if( Z1 < -7.000*cm ) Z1 = -7.000*cm;					        

    Z2 = innRadius / tan( pCyl->thet2Abs );        
    if( Z2 > 7.000*cm )  Z2 =  7.000*cm;					        
    if( Z2 < -7.000*cm ) Z2 = -7.000*cm;					        
    G4double dummy;					        
    if( Z2 > Z1 ) {							        
      dummy = Z1;					        
      Z1 = Z2;						        
      Z2 = dummy;					        
    }	      						        
    length       = 0.5 * ( Z1 - Z2 ); // half-length 
    pCyl->positAbs = G4ThreeVector(0., 0., 0.5*(Z1+Z2));
    sprintf(sName, "solidLayer%3d", ii);      
    pCyl->solidAbs = new G4Tubs(G4String(sName), innRadius, outRadius, length, 0., 360.*deg);
    if( pCyl->matAbs ) {
      sprintf(sName, "logicLayer%3d", ii);      
      pCyl->logicAbs = new G4LogicalVolume(pCyl->solidAbs,pCyl->matAbs, G4String(sName), 0, 0, 0);
    }
  }
}

void AgataAncillaryEuclides::PlaceTheCylinder()
{
  cylLayer* pCyl = NULL;
  char sName[128];
  
  for( G4int ii=0; ii<nLayers; ii++ ) {
    pCyl = &cylAbs[ii];
    if( pCyl->matAbsName != "Vacuum" ) {
      sprintf( sName, "physiLayer%3d", ii );
      pCyl->physiAbs = new G4PVPlacement(0, pCyl->positAbs, G4String(sName), pCyl->logicAbs, theDetector->HallPhys(), false, 0);
    }  
  }
}

void AgataAncillaryEuclides::SetGeometry( G4int geom )
{
  switch( geom )
  {
    case 0:
      geomType     = geom;
      spesAbs      = 0.006*mm;
      distADE      = 0.100*mm;
      distEDE      = 0.100*mm;
      matAbsorName = "Upilex";
      innerRadius  = 16.*mm; 
      eulerFile    = iniPath + "Ancillary/numbering.euclides";
      break;
      
    case 1:  
      geomType     = geom;
      spesAbs      = 0.012*mm;
      distADE      = 2.000*mm;
      distEDE      = 4.000*mm;
      matAbsorName = "Aluminium";
      innerRadius  = 17.*mm; 
      eulerFile    = iniPath + "Ancillary/numbering.isis";
      break;
      
    default:
      geomType     = 0;
      spesAbs      = 0.006*mm;
      distADE      = 0.100*mm;
      distEDE      = 0.100*mm;
      matAbsorName = "Upilex";
      innerRadius  = 16.*mm; 
      eulerFile    = iniPath + "Ancillary/numbering.euclides";
      break;
  }
  if( geomType )
    G4cout << " Geometry has been set to ISIS " << G4endl;
  else  
    G4cout << " Geometry has been set to EUCLIDES " << G4endl;
}

void AgataAncillaryEuclides::setFrameMate( G4String name )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( name );
  
  if( ptMaterial ) {
    matFrameName = name;
    G4cout << " ----> Frame material has been set to " << matFrameName << G4endl;
  }
  else {
    G4cout << " Could not change material, keeping previous material ("
           << matFrame->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryEuclides::SetNumberOfLayers( G4int number )
{
  G4int ii;
  
  if( number < 0 ) {
    G4cout << " --> Invalid value, keeping previous one (" << nLayers << ")" << G4endl;
    return;
  }
  if( number == nLayers ) {
    G4cout << " --> Requested same value as previous one (" << nLayers << ")" << G4endl;
    return;
  }
  if( number > nLayers ) {
    for( ii=nLayers; ii<number; ii++ ) {
      cylAbs.push_back(cylLayer());
    }
  }
  if( number < nLayers ) {
    for( ii=nLayers; ii>number; ii-- ) {
      cylAbs.pop_back();
    }
  }
  nLayers = number;
  G4cout << " ---> Number of layers has been set to " << nLayers << G4endl;
  return;
}

void AgataAncillaryEuclides::setAbsThet1( G4int layer, G4double angolo )
{
  if( (layer < 0) || (layer > nLayers) ) {
    G4cout << " --> Invalid layer number!" << G4endl;
    return;
  }

  if( angolo < 0. ) {
    angolo = 5.;
  }
  if( angolo > 180. ) {
    angolo = 175.;
  }
  cylLayer* pCyl = &cylAbs[layer];
  pCyl->thet1Abs = angolo * deg;
  G4cout << " ----> Layer " << layer << " begins at " << pCyl->thet1Abs/deg << " degrees" << G4endl;
}

void AgataAncillaryEuclides::setAbsThet2( G4int layer, G4double angolo )
{
  if( (layer < 0) || (layer > nLayers) ) {
    G4cout << " --> Invalid layer number!" << G4endl;
    return;
  }

  if( angolo < 0. ) {
    angolo = 5.;
  }
  if( angolo > 180. ) {
    angolo = 175.;
  }
  cylLayer* pCyl = &cylAbs[layer];
  pCyl->thet2Abs = angolo * deg;
  G4cout << " ----> Layer " << layer << " ends at " << pCyl->thet2Abs/deg << " degrees" << G4endl;
}

void AgataAncillaryEuclides::setAbsMate( G4int layer, G4String name )
{
  if( (layer < 0) || (layer > nLayers) ) {
    G4cout << " --> Invalid layer number!" << G4endl;
    return;
  }
  
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( name );
  cylLayer* pCyl = &cylAbs[layer];
  
  if( ptMaterial ) {
    pCyl->matAbsName = name;
    G4cout << " ----> Material for layer " << layer << " has been set to " << pCyl->matAbsName << G4endl;
  }
  else {
    G4cout << " Could not change material for layer " << layer << ", keeping previous material ("
           << pCyl->matAbs->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryEuclides::setAbsThick( G4int layer, G4double thickness )
{
  if( (layer < 0) || (layer > nLayers) ) {
    G4cout << " --> Invalid layer number!" << G4endl;
    return;
  }
  
  if( thickness < 0. ) {
    thickness = 0.;
  }
  if( thickness > 500. ) {
    thickness = 0.5;
  }
  cylLayer* pCyl = &cylAbs[layer];
  pCyl->thickAbs = 0.001 * thickness * mm;
  G4cout << " ----> Layer " << layer << " is " << 1000.*pCyl->thickAbs/mm << " micron thick" << G4endl;
}

void AgataAncillaryEuclides::setAbIMate( G4String name )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( name );
  if( ptMaterial ) {
    matAbsoIName = name;
    G4cout << " ----> Material for individual absorbers has been set to " << matAbsoIName << G4endl;
  }
  else {
    G4cout << " Could not change material for individual absorbers, keeping previous material ("
           << matAbsoI->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryEuclides::setAbIThick( G4int ring, G4double thickness )
{
  if( (ring < 0) || (ring > 4) ) {
    G4cout << " --> Invalid ring number!" << G4endl;
    return;
  }
    
  if( thickness < 0. ) {
    thickness = 0.;
  }
  if( thickness > 500. ) {
    thickness = 0.5;
  }
  spesAbI[ring] = 0.001 * thickness * mm;
  G4cout << " ----> Thickness of individual absorber of ring " << ring << " is " << 1000.*spesAbI[ring]/mm << " micron." << G4endl;
}

G4int AgataAncillaryEuclides::GetSegmentNumber( G4int /*offset*/, G4int detCode, G4ThreeVector posSol )
{
  if( !readOut )
    return 0;

  if( !segmLUT[detCode] )
    return 0;
  
  G4double phi = posSol.phi()/deg;
  
  if( phi < 0. ) phi += 360.;
  
  return ((G4int)floor(phi/90.));
}

void AgataAncillaryEuclides::ShowStatus()
{
  if( geomType )
    G4cout << " Array ISIS composed of " << nDets << " telescopes." << G4endl;
  else
    G4cout << " Array EUCLIDES composed of " << nDets << " telescopes." << G4endl;
  if( nSegmented )  
    G4cout << " Placed " << nSegmented << " segmented telescopes." << G4endl;
  
  if( matFrameName != "Vacuum" )
    G4cout << " Detector frames have been constructed out of " << matFrameName << "." << G4endl;
  
  cylLayer* pCyl = NULL;
  for( G4int ii=0; ii<nLayers; ii++ ) {
    pCyl = &cylAbs[ii];
    if( pCyl->matAbsName != "Vacuum" ) {
      G4cout << " Cylindrical absorber has a layer made of " <<  pCyl->matAbsName;
      G4cout << ", " << pCyl->thickAbs/mm << " mm thick, covering the range " << pCyl->thet1Abs/deg << "-";
      G4cout << pCyl->thet2Abs/deg << " degrees." << G4endl;
    }     
  }
  if( geomType ) return;
  if( matAbsoIName == "Vacuum" ) return;
  for( G4int ij=0; ij<5; ij++ ) {
    if( spesAbI[ij] )
      G4cout << " Individual absorber of ring #" << ij << " is " << 1000.*spesAbI[ij]/mm << " micron of "
             << matAbsoIName << " thick." << G4endl;
  }
}

void AgataAncillaryEuclides::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#ifdef EUCLIDES

#include "AgataRunAction.hh"
#include "AgataGeneratorAction.hh"
#include "AgataAnalysis.hh"
#include "AgataGeneratorEuclides.hh"

void AgataAncillaryEuclides::GetEnergyLoss( G4int nEvents, G4int nEnergies, G4double eStep, G4String nome )
{

  G4RunManager * runManager = G4RunManager::GetRunManager();
  AgataGeneratorAction* theGenerator = (AgataGeneratorAction*) runManager->GetUserPrimaryGeneratorAction();
  AgataRunAction*       theRun       = (AgataRunAction *)      runManager->GetUserRunAction();
  G4bool                doWrite      = theRun->DoWrite();
  // to avoid conflicts, disable write to lmd file and spectra
  if( doWrite )
    theRun->EnableWrite( false );

  AgataDetectorConstruction* theDetector = (AgataDetectorConstruction *)runManager->GetUserDetectorConstruction();
  theDetector->SetCalcOmega(true);


  /// enable the on-line analysis
  theRun->GetTheAnalysis()->EnableAnalysis(true);
  
  G4bool* lut = new G4bool[segmLUT.size()];
  G4int nParticles = 3;
  for( G4int ii=0; ii<segmLUT.size(); ii++ ) {
    if( readOut )
      lut[ii] = segmLUT[ii];
    else
      lut[ii] = false;  
  }
  theRun->GetTheAnalysis()->SetTarget( nParticles, nEvents, nEnergies, eStep, geomType, lut, nome );

  AgataGeneratorEuclides* newGenerator = new AgataGeneratorEuclides( eStep, nEnergies, nEvents );
  //G4cout << " Arrivo a questo punto?" << G4endl;
  
  runManager->SetUserAction( newGenerator );
  
  runManager->BeamOn(nEvents*nEnergies*nParticles);

  theRun->GetTheAnalysis()->EnableAnalysis(false);
  if( doWrite )
    theRun->EnableWrite( true );
  theDetector->SetCalcOmega(false);

  delete newGenerator;
  runManager->SetUserAction( theGenerator );  
  
}
#endif

/////////////////////////////////////////////////////////////////////////
// The Messenger
////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
//#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataAncillaryEuclidesMessenger::AgataAncillaryEuclidesMessenger(AgataAncillaryEuclides* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/ancillary/Euclides/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of EUCLIDES detector construction.");

  commandName = directoryName + "layerNumber";
  aLine = commandName.c_str();
  absLayersCmd = new G4UIcmdWithAnInteger(aLine, this);
  absLayersCmd->SetGuidance("Selects the number of layers composing the cylindrical absorber.");
  absLayersCmd->SetGuidance("Required parameters: 1 integer.");
  absLayersCmd->SetParameterName("choice",false);
  absLayersCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "layerMaterial";
  aLine = commandName.c_str();
  absMatCmd = new G4UIcmdWithAString(aLine, this);
  absMatCmd->SetGuidance("Select material of a given layer of the cylindrical absorber.");
  absMatCmd->SetGuidance("Required parameters: 1 integer, 1 string (layer number and material name).");
  absMatCmd->SetParameterName("choice",false);
  absMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "frameMaterial";
  aLine = commandName.c_str();
  frmMatCmd = new G4UIcmdWithAString(aLine, this);
  frmMatCmd->SetGuidance("Select material of the detector frames.");
  frmMatCmd->SetGuidance("Required parameters: 1 string.");
  frmMatCmd->SetParameterName("choice",false);
  frmMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "layerThick";
  aLine = commandName.c_str();
  absThickCmd = new G4UIcmdWithAString(aLine, this);  
  absThickCmd->SetGuidance("Define thickness of a given layer of the cylindrical absorber.");
  absThickCmd->SetGuidance("Required parameters: 1 integer, 1 double (layer number and thickness of the absorber in micron).");
  absThickCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "layerRange";
  aLine = commandName.c_str();
  absAngCmd = new G4UIcmdWithAString(aLine, this);  
  absAngCmd->SetGuidance("Define angular range of a given layer of the cylindrical absorber.");
  absAngCmd->SetGuidance("Required parameters: 1 integer, 2 double (layer number and theta min, max in degrees).");
  absAngCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "geometry";
  aLine = commandName.c_str();
  setGeomCmd = new G4UIcmdWithAnInteger(aLine, this);
  setGeomCmd->SetGuidance("Chooses the ISIS or the EUCLIDES geometries.");
  setGeomCmd->SetGuidance("Required parameters: 1 integer (0-->EUCLIDES; 1-->ISIS).");
  setGeomCmd->SetParameterName("choice",false);
  setGeomCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "individualMaterial";
  aLine = commandName.c_str();
  abIMatCmd = new G4UIcmdWithAString(aLine, this);
  abIMatCmd->SetGuidance("Select material of the individual absorbers.");
  abIMatCmd->SetGuidance("Required parameters: 1 string (material name).");
  abIMatCmd->SetParameterName("choice",false);
  abIMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "individualThick";
  aLine = commandName.c_str();
  abIThickCmd = new G4UIcmdWithAString(aLine, this);  
  abIThickCmd->SetGuidance("Define thickness of a given ring of the individual absorbers.");
  abIThickCmd->SetGuidance("Required parameters: 1 integer, 1 double (ring number and thickness of the absorber in micron).");
  abIThickCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  

#ifdef EUCLIDES
  commandName = directoryName + "eLoss";
  aLine = commandName.c_str();
  ElossCmd = new G4UIcmdWithAString(aLine, this);
  ElossCmd->SetGuidance("Calculates average energy loss inside the absorbers.");
  ElossCmd->SetGuidance("Required parameters: 2 integers (number of events and energies), 1 double (step in energy), 1 string (output file name).");
  ElossCmd->SetParameterName("choice",false);
  ElossCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
#endif
}

AgataAncillaryEuclidesMessenger::~AgataAncillaryEuclidesMessenger()
{
  delete myDirectory;
  delete absLayersCmd;
  delete absMatCmd;
  delete frmMatCmd;
  delete absThickCmd;
  delete absAngCmd;
  delete setGeomCmd;
  delete abIMatCmd;
  delete abIThickCmd;
#ifdef EUCLIDES
  delete ElossCmd;
#endif
}

void AgataAncillaryEuclidesMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == absMatCmd ) {
    G4int n1;
    char lin1[128];
    if( sscanf(newValue, "%d %s", &n1, lin1 ) != 2 ) {
      G4cout << " Wrong number of parameters (1 integer and 1 string required)." << G4endl;
    }
    else {
      myTarget->setAbsMate(n1-1, lin1);
    }  
  }
  if( command == absThickCmd ) {
    G4int n2;
    float t2;
//    if( sscanf(newValue, "%d %lf", &n2, &t2 ) != 2 ) {
    if( sscanf(newValue, "%d %f", &n2, &t2 ) != 2 ) {
      G4cout << " Wrong number of parameters (1 integer and 1 double required)." << G4endl;
    }
    else {
      myTarget->setAbsThick(n2-1, (G4double)t2);
    }  
  }
  if( command == abIMatCmd ) {
      myTarget->setAbIMate(newValue);
  }
  if( command == abIThickCmd ) {
    G4int n2;
    float t2;
//    if( sscanf(newValue, "%d %lf", &n2, &t2 ) != 2 ) {
    if( sscanf(newValue, "%d %f", &n2, &t2 ) != 2 ) {
      G4cout << " Wrong number of parameters (1 integer and 1 double required)." << G4endl;
    }
    else {
      myTarget->setAbIThick(n2-1, (G4double)t2);
    }  
  }
  if( command == absAngCmd ) {
    float th1, th2;
    G4int n3;
//    if( sscanf(newValue, "%d %lf %lf", &n3, &th1, &th2 ) != 3 ) {
    if( sscanf(newValue, "%d %f %f", &n3, &th1, &th2 ) != 3 ) {
      G4cout << " Wrong number of parameters (3 numbers required)." << G4endl;
    }
    else {
      myTarget->setAbsThet1(n3-1, (G4double)th1);
      myTarget->setAbsThet2(n3-1, (G4double)th2);
    }  
  }
  if( command == frmMatCmd ) {
    myTarget->setFrameMate(newValue);
  }
  if( command == absLayersCmd ) {
    myTarget->SetNumberOfLayers(absLayersCmd->GetNewIntValue(newValue));
  }  
  if( command == setGeomCmd ) {
    myTarget->SetGeometry(setGeomCmd->GetNewIntValue(newValue));
  }  
#ifdef EUCLIDES
  if( command == ElossCmd ) {
    G4int n1, n2;
    float step;
    char name[128];
//    if( sscanf(newValue, "%d %d %lf %s", &n1, &n2, &step, name ) != 4 ) {
    if( sscanf(newValue, "%d %d %f %s", &n1, &n2, &step, name ) != 4 ) {
      G4cout << " Wrong number of parameters (3 numbers and 1 string required)." << G4endl;
    }
    else {
      myTarget->GetEnergyLoss( n1, n2, ((G4double)step)*keV, G4String(name) );
    }  
  }
#endif
}
#endif

