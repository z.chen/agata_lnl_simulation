AgataAlternativeGenerator.cc:4:#include "G4ParticleGun.hh"
AgataAlternativeGenerator.cc:5:#include "G4ParticleTable.hh"
AgataAlternativeGenerator.cc:6:#include "G4ParticleDefinition.hh"
AgataAlternativeGenerator.cc:13:  G4int n_particle = 1;
AgataAlternativeGenerator.cc:14:  particleGun = new G4ParticleGun(n_particle);
AgataAlternativeGenerator.cc:16:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataAlternativeGenerator.cc:17:  G4String particleName = "gamma";
AgataAlternativeGenerator.cc:18:  particleGun->SetParticleDefinition(particleTable->FindParticle(particleName));
AgataAlternativeGenerator.cc:19:  particleGun->SetParticleEnergy(1.0*MeV);
AgataAlternativeGenerator.cc:20:  particleGun->SetParticlePosition(G4ThreeVector());
AgataAlternativeGenerator.cc:26:  delete particleGun;
AgataAlternativeGenerator.cc:39:  // finally, call particle gun 
AgataAlternativeGenerator.cc:41:  particleGun->SetParticleMomentumDirection( direction );
AgataAlternativeGenerator.cc:42:  particleGun->GeneratePrimaryVertex(anEvent);
AgataAlternativeGenerator.cc:46:G4String AgataAlternativeGenerator::GetParticleHeader( const G4Event* evt, G4double /* unitLength */, G4double unitEnergy )
AgataAlternativeGenerator.cc:51:  G4PrimaryParticle* pPart     = pVert->GetPrimary();
AgataAlternativeGenerator.cc:62:  G4String nome = pPart->GetG4code()->GetParticleName();
AgataAlternativeGenerator.cc:87:  // Finally writes out the particles
AgataAncillaryEuclides.cc:1706:  G4int nParticles = 3;
AgataAncillaryEuclides.cc:1713:  theRun->GetTheAnalysis()->SetTarget( nParticles, nEvents, nEnergies, eStep, geomType, lut, nome );
AgataAncillaryEuclides.cc:1720:  runManager->BeamOn(nEvents*nEnergies*nParticles);
AgataDefaultGenerator.cc:8:#include "G4ParticleGun.hh"
AgataDefaultGenerator.cc:9:#include "G4ParticleTable.hh"
AgataDefaultGenerator.cc:10:#include "G4ParticleDefinition.hh"
AgataDefaultGenerator.cc:25:  G4int n_particle = 1;
AgataDefaultGenerator.cc:26:  particleGun = new G4ParticleGun(n_particle);
AgataDefaultGenerator.cc:44:  delete particleGun;
AgataDefaultGenerator.cc:79:  /// Asks the AgataEmission object what will be the next emitted particle
AgataDefaultGenerator.cc:84:    particleGun->GeneratePrimaryVertex(anEvent);
AgataDefaultGenerator.cc:89:  theEmission->NextParticle();
AgataDefaultGenerator.cc:107:    particleGun->GeneratePrimaryVertex(anEvent);
AgataDefaultGenerator.cc:113:  // finally, call particle gun 
AgataDefaultGenerator.cc:116:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataDefaultGenerator.cc:117:  G4ParticleDefinition* emitted;
AgataDefaultGenerator.cc:120:    emitted = particleTable->GetIon( emittedAtomNum, emittedMassNum, 0. );
AgataDefaultGenerator.cc:122:    emitted = particleTable->FindParticle(emittedName);
AgataDefaultGenerator.cc:124:  particleGun->SetParticleDefinition       ( emitted );
AgataDefaultGenerator.cc:125:  particleGun->SetParticleMomentumDirection( emittedDirection );
AgataDefaultGenerator.cc:126:  particleGun->SetParticleEnergy           ( emittedEnergy );
AgataDefaultGenerator.cc:127:  particleGun->SetParticlePosition         ( emitterPosition );
AgataDefaultGenerator.cc:129:  particleGun->SetParticleTime( emitterTime );
AgataDefaultGenerator.cc:132:    particleGun->SetParticlePolarization   ( emittedPolarization );
AgataDefaultGenerator.cc:134:  particleGun->GeneratePrimaryVertex(anEvent);
AgataEmitted.cc:28:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataEmitted.cc:29:  G4ParticleDefinition* emitted = particleTable->FindParticle(emittedName);
AgataEmitted.cc:55:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataEmitted.cc:56:  G4ParticleDefinition* emitted = particleTable->GetIon( emittedChargeNumber, emittedMassNumber, 0. );
AgataEmitted.cc:62:    emitted = particleTable->GetIon( emittedChargeNumber, emittedMassNumber, 0. );
AgataEmitted.cc:170:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataEmitted.cc:171:  G4ParticleDefinition* emitted = particleTable->GetIon( atNum, massNum, 0. );
AgataEmitted.cc:1445:  SetGunCmd->SetGuidance("Define behaviour of the particle gun.");
AgataEventAction.cc:204:    /// emitted particle
AgataEventAction.cc:206:    theRun->LMwrite( theGenerator->GetParticleHeader( evt, unitLength, unitEnergy ) );
AgataEventAction.cc:222:    G4int         particle_id;
AgataEventAction.cc:237:	G4int     *particle_id_table = new G4int    [nhits_total];
AgataEventAction.cc:277:	    particle_id_table[i_hit_all]  = ParticleId(aHit);
AgataEventAction.cc:313:	    particle_id = particle_id_table[i_sorted];
AgataEventAction.cc:321:	    theRun->LMwrite( PrepareLineWithLight(detnum, edep, light, posA, posI, segnum, time, inter, particle_id) );
AgataEventAction.cc:345:	  G4int         first_particle_in_det = 8;
AgataEventAction.cc:359:	  first_particle_in_det=particle_id_table[i_sorted]; //gj 2010.11.13
AgataEventAction.cc:375:	    if(first_particle_in_det==8){first_particle_in_det=particle_id_table[i_sorted];}
AgataEventAction.cc:382:		if(segnum!=0){cout<<endl<<"DUPA\t segnum!=0"<<endl;}else{segnum=first_particle_in_det;}
AgataEventAction.cc:404:	      first_particle_in_det = 8;
AgataEventAction.cc:429:	    if(segnum!=0){cout<<endl<<"DUPA\t segnum!=0"<<endl;}else{segnum=first_particle_in_det;}
AgataEventAction.cc:443:	delete [] particle_id_table;
AgataEventAction.cc:609:  G4String particle   = theHit->GetChiEStato();
AgataEventAction.cc:617:  /**/ if(particle == "proton"    ) Light = EnergyToLight(PreStepEn)   - EnergyToLight(PostStepEn);
AgataEventAction.cc:618:  else if(particle == "deuteron"  ) Light = 2.*(EnergyToLight(PreStepEn/2.)- EnergyToLight(PostStepEn/2.));
AgataEventAction.cc:619:  else if(particle.rfind("C12")==0) Light = 0.017*edep*10.;
AgataEventAction.cc:620:  else if(particle.rfind("Be9")==0) Light = 0.013*edep*10.;
AgataEventAction.cc:621:  else if(particle == "alpha"     ){
AgataEventAction.cc:628:  else if(particle == "e-"        ) Light = edep*10.;
AgataEventAction.cc:629:  else if(particle == "gamma"     ) Light = 0.99*edep*10.;
AgataEventAction.cc:630:  else{G4cout<<endl<<"I dont know how to produce light for this particle: "<<particle<<endl;}
AgataEventAction.cc:645:G4int AgataEventAction::ParticleId(AgataHitDetector *theHit){
AgataEventAction.cc:646:  G4String particle   = theHit->GetChiEStato();
AgataEventAction.cc:647:  if(particle == "proton"    ) return 1;
AgataEventAction.cc:648:  if(particle == "deuteron"  ) return 2;
AgataEventAction.cc:649:  if(particle.rfind("C12")==0) return 3;
AgataEventAction.cc:650:  if(particle.rfind("Be9")==0) return 4;
AgataEventAction.cc:651:  if(particle == "alpha"     ) return 5;
AgataEventAction.cc:652:  if(particle == "e-"        ) return 6;
AgataEventAction.cc:653:  if(particle == "gamma"     ) return 7;
AgataEventAction.cc:654:  G4cout<<endl<<"I dont know how to produce light for this particle: "<<particle<<endl;
AgataEventAction.cc:714://with particle id
AgataEventAction.cc:718:						 G4ThreeVector pos2, G4int nseg, G4double time, G4int inter, G4int particle_id )
AgataEventAction.cc:768:    sprintf( dummy2, "%2.2d ", particle_id );
AgataExternalEmission.cc:136:      abortMessage = " Emitted particle is not compatible with residual nucleus, aborting run ...";
AgataExternalEmission.cc:391:/// InitEmitted allocates an array of AgataEmitted objects, each of them corresponding to a particle
AgataExternalEmission.cc:392:/// type which can be emitted in the following run. If during run a particle not corresponding to one
AgataExternalEmission.cc:433:    else {  // check for repeated particles!
AgataExternalEmission.cc:457:    abortMessage = " No emitted particles defined, aborting run ...\n";
AgataExternalEmission.cc:560:      name = "unknown particle";
AgataExternalEmission.cc:561:      G4cout << " Undefined particle #" << type << ", run will be aborted ..." << G4endl;
AgataExternalEmission.cc:568:/// Chooses next emitted particle and stores its name as emittedName, momentum as emittedMomentum
AgataExternalEmission.cc:570:void AgataExternalEmission::NextParticle()
AgataExternalEmission.cc:710:/// DecodeEmitted: retrieves from input file the information on the next emitted particle
AgataExternalEmission.cc:812:  // update emitted particle
AgataExternalEmission.cc:917:/// FetchEmitter: saves residual nucleus as the next emitted particle
AgataExternalEmission.cc:926:  cascadeDeltaTime  = 0.; // same emission time as the last emitted particle!!!
AgataExternalEmission.cc:959:    G4cout << " Emitted particles: ";
AgataExternalEmission.cc:1036:    // writes out the time difference between the emission of two particles
AgataExternalEmission.cc:1037:    // in the cascade (or the time for the first particle) only when it is
AgataExternalEmission.cc:1060:    // writes out the time difference between the emission of two particles
AgataExternalEmission.cc:1061:    // in the cascade (or the time for the first particle) only when it is
AgataExternalEmission.cc:1072:G4String AgataExternalEmission::GetParticleHeader( const G4Event* evt, G4double /*unitLength*/, G4double unitEnergy )
AgataExternalEmission.cc:1077:  G4PrimaryParticle* pPart     = pVert->GetPrimary();
AgataExternalEmission.cc:1088:  G4String nome = pPart->GetG4code()->GetParticleName();
AgataExternalEmission.cc:1113:  // Finally writes out the particles
AgataExternalEmitter.cc:85:    G4cout << " Warning! Emitted particle is not compatible with the residual nucleus." << G4endl;
AgataExternalEmitter.cc:94:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:95:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:97:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:99:    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
AgataExternalEmitter.cc:126:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:127:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:129:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:131:    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
AgataExternalEmitter.cc:146:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:147:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:150:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:152:    emitted = particleTable->GetIon( zBeam, aBeam, 0. );
AgataExternalEmitter.cc:158:    emitted = particleTable->GetIon( zBeam, aBeam, 0. );
AgataExternalEmitter.cc:163:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:165:    emitted = particleTable->GetIon( zTarg, aTarg, 0. );
AgataExternalEmitter.cc:171:    emitted = particleTable->GetIon( zTarg, aTarg, 0. );
AgataExternalEmitter.cc:176:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:178:    emitted = particleTable->GetIon( zTota, aTota, 0. );
AgataExternalEmitter.cc:258:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:259:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:261:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:263:    emitted = particleTable->GetIon( z1, a1, 0. );
AgataExternalEmitter.cc:278:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:279:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:281:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:283:    emitted = particleTable->GetIon( z1, a1, 0. );
AgataExternalEmitter.cc:323:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataExternalEmitter.cc:324:  G4ParticleDefinition* emitted;
AgataExternalEmitter.cc:326:    emitted = particleTable->FindParticle("neutron");
AgataExternalEmitter.cc:328:    emitted = particleTable->GetIon( emitterAtomNum, emitterMassNum, 0. );
AgataGeneratorOmega.cc:4:#include "G4ParticleGun.hh"
AgataGeneratorOmega.cc:5:#include "G4ParticleTable.hh"
AgataGeneratorOmega.cc:6:#include "G4ParticleDefinition.hh"
AgataGeneratorOmega.cc:13:  G4int n_particle = 1;
AgataGeneratorOmega.cc:14:  particleGun = new G4ParticleGun(n_particle);
AgataGeneratorOmega.cc:16:  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
AgataGeneratorOmega.cc:17:  G4String particleName = "geantino";
AgataGeneratorOmega.cc:18:  particleGun->SetParticleDefinition(particleTable->FindParticle(particleName));
AgataGeneratorOmega.cc:19:  particleGun->SetParticleEnergy(1.0*MeV);
AgataGeneratorOmega.cc:20:  particleGun->SetParticlePosition(G4ThreeVector());
AgataGeneratorOmega.cc:26:  delete particleGun;
AgataGeneratorOmega.cc:39:  // finally, call particle gun 
AgataGeneratorOmega.cc:41:  particleGun->SetParticleMomentumDirection( direction );
AgataGeneratorOmega.cc:42:  particleGun->GeneratePrimaryVertex(anEvent);
AgataInternalEmission.cc:83:    /// There should be at least a particle!
AgataInternalEmission.cc:85:      G4cout << " Warning! At least one particle should be emitted!" << G4endl;
AgataInternalEmission.cc:144:void AgataInternalEmission::NextParticle()
AgataInternalEmission.cc:217:    // For the following particles, writes out source position only when it has changed
AgataInternalEmission.cc:224:    // writes out the time difference between the emission of two particles
AgataInternalEmission.cc:225:    // in the cascade (or the time for the first particle) only when it is
AgataInternalEmission.cc:236:G4String AgataInternalEmission::GetParticleHeader( const G4Event* evt, G4double /*unitLength*/, G4double unitEnergy )
AgataInternalEmission.cc:241:  G4PrimaryParticle* pPart     = pVert->GetPrimary();
AgataInternalEmission.cc:252:  G4String nome = pPart->GetG4code()->GetParticleName();
AgataInternalEmission.cc:277:  // Finally writes out the particles
AgataLowEnergyPolarizedCompton.cc:3:#include "G4ParticleDefinition.hh"
AgataLowEnergyPolarizedCompton.cc:9:#include "G4DynamicParticle.hh"
AgataLowEnergyPolarizedCompton.cc:10:#include "G4VParticleChange.hh"
AgataLowEnergyPolarizedCompton.cc:73:void AgataLowEnergyPolarizedCompton::BuildPhysicsTable(const G4ParticleDefinition& )
AgataLowEnergyPolarizedCompton.cc:86:G4VParticleChange* AgataLowEnergyPolarizedCompton::
AgataLowEnergyPolarizedCompton.cc:89:  aParticleChange.Initialize(aTrack);
AgataLowEnergyPolarizedCompton.cc:91:  const G4DynamicParticle* incidentPhoton = aTrack.GetDynamicParticle();
AgataLowEnergyPolarizedCompton.cc:95:    aParticleChange.ProposeTrackStatus(fStopAndKill);
AgataLowEnergyPolarizedCompton.cc:96:    aParticleChange.ProposeEnergy(0.);
AgataLowEnergyPolarizedCompton.cc:97:    aParticleChange.ProposeLocalEnergyDeposit(gammaEnergy0);
AgataLowEnergyPolarizedCompton.cc:185:  G4ParticleMomentum gammaDirection0    = incidentPhoton->GetMomentumDirection();
AgataLowEnergyPolarizedCompton.cc:261:// Update G4VParticleChange for the scattered gamma //
AgataLowEnergyPolarizedCompton.cc:265:    aParticleChange.ProposeEnergy( gammaEnergy1 ) ;
AgataLowEnergyPolarizedCompton.cc:266:    aParticleChange.ProposeMomentumDirection( gammaDirection1 );
AgataLowEnergyPolarizedCompton.cc:267:    aParticleChange.ProposePolarization( gammaPolarization1 );
AgataLowEnergyPolarizedCompton.cc:270:    aParticleChange.ProposeEnergy(0.) ;
AgataLowEnergyPolarizedCompton.cc:271:    aParticleChange.ProposeTrackStatus(fStopAndKill);
AgataLowEnergyPolarizedCompton.cc:287:    G4DynamicParticle* electron = new G4DynamicParticle (G4Electron::Electron(),ElecDirection.unit(),ElecKineEnergy) ;
AgataLowEnergyPolarizedCompton.cc:288:    aParticleChange.SetNumberOfSecondaries(1);
AgataLowEnergyPolarizedCompton.cc:289:    aParticleChange.AddSecondary(electron);
AgataLowEnergyPolarizedCompton.cc:290:    aParticleChange.ProposeLocalEnergyDeposit(0.); 
AgataLowEnergyPolarizedCompton.cc:293:    aParticleChange.SetNumberOfSecondaries(0);
AgataLowEnergyPolarizedCompton.cc:294:    aParticleChange.ProposeLocalEnergyDeposit(ElecKineEnergy);
AgataLowEnergyPolarizedCompton.cc:302:G4bool AgataLowEnergyPolarizedCompton::IsApplicable(const G4ParticleDefinition& particle)
AgataLowEnergyPolarizedCompton.cc:304:  return ( &particle == G4Gamma::Gamma() ); 
AgataLowEnergyPolarizedCompton.cc:312:  const G4DynamicParticle* photon = track.GetDynamicParticle();
AgataPhysicsList.cc:6:#include "G4ParticleDefinition.hh"
AgataPhysicsList.cc:7:#include "G4ParticleWithCuts.hh"
AgataPhysicsList.cc:8:#include "G4ParticleTypes.hh"
AgataPhysicsList.cc:9:#include "G4ParticleTable.hh"
AgataPhysicsList.cc:42:// for all particles which are used.
AgataPhysicsList.cc:43:// This ensures that objects of these particle types will be
AgataPhysicsList.cc:54:void AgataPhysicsList::ConstructParticle()
AgataPhysicsList.cc:57:  pBosonConstructor.ConstructParticle();
AgataPhysicsList.cc:60:  pLeptonConstructor.ConstructParticle();
AgataPhysicsList.cc:63:  pMesonConstructor.ConstructParticle();
AgataPhysicsList.cc:66:  pBaryonConstructor.ConstructParticle();
AgataPhysicsList.cc:69:  pIonConstructor.ConstructParticle();
AgataPhysicsList.cc:72:  pShortLivedConstructor.ConstructParticle();  
AgataPhysicsList.cc:77:void AgataPhysicsList::ConstructParticle()
AgataPhysicsList.cc:141:/// all charged particles
AgataPhysicsList.cc:160:  theParticleIterator->reset();
AgataPhysicsList.cc:161:  while( (*theParticleIterator)() ){
AgataPhysicsList.cc:162:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:163:    G4ProcessManager* pmanager = particle->GetProcessManager();
AgataPhysicsList.cc:164:    G4String particleName = particle->GetParticleName();
AgataPhysicsList.cc:165:    G4String particleType = particle->GetParticleType();
AgataPhysicsList.cc:166:    if (particleName == "gamma") { //> gamma
AgataPhysicsList.cc:218:    else if (particleName == "e-") { //> electron
AgataPhysicsList.cc:244:    else if (particleName == "e+") {  //> positron
AgataPhysicsList.cc:273:  theParticleIterator->reset();
AgataPhysicsList.cc:274:  while( (*theParticleIterator)() ){
AgataPhysicsList.cc:275:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:276:    if( particle->GetPDGCharge() == 0. ) continue;
AgataPhysicsList.cc:278:    if( particle->GetParticleName() == "e-" ) continue; //> multiple scattering for e+/e- has been already registered
AgataPhysicsList.cc:279:    if( particle->GetParticleName() == "e+" ) continue;
AgataPhysicsList.cc:281:    G4ProcessManager* pmanager = particle->GetProcessManager();
AgataPhysicsList.cc:302:  theParticleIterator->reset();
AgataPhysicsList.cc:303:  while( (*theParticleIterator)() ){
AgataPhysicsList.cc:304:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:306:    if( particle->GetPDGCharge() == 0. ) continue;
AgataPhysicsList.cc:308:    if( particle->GetParticleName() == "e-" ) continue; // processes for e+/e- has been already registered
AgataPhysicsList.cc:309:    if( particle->GetParticleName() == "e+" ) continue;
AgataPhysicsList.cc:311:    G4ProcessManager* pmanager = particle->GetProcessManager();
AgataPhysicsList.cc:315:    // on the particle type
AgataPhysicsList.cc:321:      if (particle->GetParticleName() == "proton")  
AgataPhysicsList.cc:323:      else if (particle->GetParticleName() == "antiproton")  
AgataPhysicsList.cc:325:      else if (particle->GetParticleName() == "deuteron")  
AgataPhysicsList.cc:327:      else if (particle->GetParticleName() == "triton")  
AgataPhysicsList.cc:329:      else if (particle->GetParticleName() == "He3")  
AgataPhysicsList.cc:331:      else if (particle->GetParticleName() == "alpha")  
AgataPhysicsList.cc:333:      else if (particle->GetParticleName() == "GenericIon")  
AgataPhysicsList.cc:335:      else if (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  
AgataPhysicsList.cc:337:      else if ((!particle->IsShortLived()) && (particle->GetPDGCharge() != 0.0))  
AgataPhysicsList.cc:346:      if (particle->GetParticleName() == "alpha"      ||
AgataPhysicsList.cc:347:          particle->GetParticleName() == "He3"        ||
AgataPhysicsList.cc:348:          particle->GetParticleName() == "GenericIon" || 
AgataPhysicsList.cc:349:         (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  ) {
AgataPhysicsList.cc:354: //           other particles (the  !particle->IsShortLived()  condition)
AgataPhysicsList.cc:356:/*      else if ( particle->GetParticleName() == "proton"   ||
AgataPhysicsList.cc:357:                particle->GetParticleName() == "deuteron" ||
AgataPhysicsList.cc:358:                particle->GetParticleName() == "triton"   ||
AgataPhysicsList.cc:359:	       !particle->IsShortLived()  ) {
AgataPhysicsList.cc:363:      else if ( particle->GetParticleName() == "proton"   ||
AgataPhysicsList.cc:364:                particle->GetParticleName() == "deuteron" ||
AgataPhysicsList.cc:365:                particle->GetParticleName() == "triton" ) {
AgataPhysicsList.cc:433:  theParticleIterator->reset();
AgataPhysicsList.cc:434:  while( (*theParticleIterator)() ){
AgataPhysicsList.cc:435:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:437:    if( particle->GetParticleName() == "gamma" ) continue;
AgataPhysicsList.cc:438:    if( particle->GetParticleName() == "e-" )    continue; // processes for e+/e- has been already registered
AgataPhysicsList.cc:439:    if( particle->GetParticleName() == "e+" )    continue;
AgataPhysicsList.cc:441:    G4ProcessManager* pManager = particle->GetProcessManager();
AgataPhysicsList.cc:443:    if( particle->GetParticleName() == "neutron" ) {
AgataPhysicsList.cc:455://    else if ( !particle->IsShortLived() ) { 
AgataPhysicsList.cc:456:    else if ( particle->GetParticleName() == "anti_neutron" ||
AgataPhysicsList.cc:457:              particle->GetParticleName() == "proton"       ||
AgataPhysicsList.cc:458:              particle->GetParticleName() == "anti_proton"  ||
AgataPhysicsList.cc:459:              particle->GetParticleName() == "deuteron"     ||
AgataPhysicsList.cc:460:              particle->GetParticleName() == "triton"       ||
AgataPhysicsList.cc:461:              particle->GetParticleName() == "alpha"        || 
AgataPhysicsList.cc:462:              particle->GetParticleName() == "He3"          ||
AgataPhysicsList.cc:463:              particle->GetParticleName() == "GenericIon"   || 
AgataPhysicsList.cc:464:             (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)) {
AgataPhysicsList.cc:478:  theParticleIterator->reset();
AgataPhysicsList.cc:479:  while( (*theParticleIterator)() ){
AgataPhysicsList.cc:480:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:482:    if( particle->GetParticleName() == "gamma" ) continue;
AgataPhysicsList.cc:483:    if( particle->GetParticleName() == "e-" )    continue; // processes for e+/e- has been already registered
AgataPhysicsList.cc:484:    if( particle->GetParticleName() == "e+" )    continue;
AgataPhysicsList.cc:486:    G4ProcessManager* pmanager = particle->GetProcessManager();
AgataPhysicsList.cc:488:    if(particle->GetParticleName() == "proton") {
AgataPhysicsList.cc:496:    else if(particle->GetParticleName() == "anti_proton") {
AgataPhysicsList.cc:504:    else if(particle->GetParticleName() == "neutron") {
AgataPhysicsList.cc:527:    else if(particle->GetParticleName() == "anti_neutron") {
AgataPhysicsList.cc:536:    else if(particle->GetParticleName() == "deuteron") {
AgataPhysicsList.cc:543:    else if(particle->GetParticleName() == "triton") {
AgataPhysicsList.cc:550:    else if(particle->GetParticleName() == "alpha") {
AgataPhysicsList.cc:569:  theParticleIterator->reset();
AgataPhysicsList.cc:570:  while( (*theParticleIterator)() ) {
AgataPhysicsList.cc:571:    G4ParticleDefinition* particle = theParticleIterator->value();
AgataPhysicsList.cc:572:    G4ProcessManager* pmanager = particle->GetProcessManager();
AgataPhysicsList.cc:574:    if (theDecayProcess->IsApplicable(*particle) && !particle->IsShortLived()) { 
AgataPolarizedComptonScattering.cc:21:G4VParticleChange* AgataPolarizedComptonScattering::
AgataPolarizedComptonScattering.cc:24:  aParticleChange.Initialize(aTrack);
AgataPolarizedComptonScattering.cc:26:  const G4DynamicParticle* aDynamicGamma = aTrack.GetDynamicParticle();
AgataPolarizedComptonScattering.cc:102:  G4ParticleMomentum gammaDirection0    = aDynamicGamma->GetMomentumDirection();
AgataPolarizedComptonScattering.cc:178:// Update G4VParticleChange for the scattered gamma //
AgataPolarizedComptonScattering.cc:181:  aParticleChange.ProposeMomentumDirection(gammaDirection1);
AgataPolarizedComptonScattering.cc:182:  aParticleChange.ProposePolarization(gammaPolarization1);
AgataPolarizedComptonScattering.cc:186:    aParticleChange.ProposeEnergy(gammaEnergy1);
AgataPolarizedComptonScattering.cc:190:    aParticleChange.ProposeEnergy(0.) ;
AgataPolarizedComptonScattering.cc:191:    aParticleChange.ProposeTrackStatus(fStopAndKill);
AgataPolarizedComptonScattering.cc:202:    G4DynamicParticle* aElectron= new G4DynamicParticle (G4Electron::Electron(), ElecDirection,ElecKineEnergy);
AgataPolarizedComptonScattering.cc:203:    aParticleChange.SetNumberOfSecondaries(1);
AgataPolarizedComptonScattering.cc:204:    aParticleChange.AddSecondary( aElectron );
AgataPolarizedComptonScattering.cc:207:    aParticleChange.SetNumberOfSecondaries(0);
AgataPolarizedComptonScattering.cc:211:  aParticleChange.ProposeLocalEnergyDeposit(localEnergyDeposit);
AgataSensitiveDetector.cc:87:      ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
AgataSensitiveDetector.cc:168:      // heavy ion ==> particle mass larger than alpha particle
AgataSensitiveDetector.cc:169:      //G4cout << " Tracked " << aStep->GetTrack()->GetDefinition()->GetParticleName() << G4endl;
AgataSensitiveDetector.cc:176:      G4cout << " Primary is " << evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() << G4endl;
AgataSensitiveDetector.cc:177:      if( evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() == "neutron" )
AgataSensitiveDetector.cc:185:  /// In the case of secondary particles, the process which generated the secondary
AgataSensitiveDetector.cc:186:  /// particle is actually retrieved.
AgataSensitiveDetector.cc:202:  /// Direction (in the Lab system) of the primary particle
AgataSensitiveDetector.cc:213:  /// counted from the moment the primary particle was
AgataSensitiveDetector.cc:309:  ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
AgataSensitiveDetector.cc:350:  /// The characteristics of the primary particle are retrieved for printout on screen
AgataSensitiveDetector.cc:355:  if( masspart > 0. )    // massive particles --> kinetic energy (relativistic)
AgataSteppingOmega.cc:9:  // calculation it will be no more possible to track properly the other particles
AgataSteppingOmega.cc:10:  if( theStep->GetTrack()->GetDefinition()->GetParticleName() == "geantino" ) {
