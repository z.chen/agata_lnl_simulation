file(REMOVE_RECURSE
  "Agata"
  "Agata.pdb"
  "CMakeFiles/Agata.dir/Agata.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAlternativeGenerator.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAnalysis.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryADCA.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryAida.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryBrick.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryCassandra.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryCluster.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryDiamant.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryDiamant_FP.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryDiamant_FTgt.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryEuclides.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryExogam.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryFatima.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryGSIChambRing.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryGanilChamb.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryHC.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryHelena.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryKoeln.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryLNLChamb.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryLycca.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryMcp.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryNDet.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryNeda.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryNeutronWall.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryNordBallNDet.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryOups.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryParis.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryPrisma.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryPrismaFP.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryRFD.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillaryShell.cc.o"
  "CMakeFiles/Agata.dir/src/AgataAncillarySpider.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDefaultGenerator.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorAncillary.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorArray.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorConstruction.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorShell.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorSimple.cc.o"
  "CMakeFiles/Agata.dir/src/AgataDetectorTest.cc.o"
  "CMakeFiles/Agata.dir/src/AgataEmitted.cc.o"
  "CMakeFiles/Agata.dir/src/AgataEmitter.cc.o"
  "CMakeFiles/Agata.dir/src/AgataEventAction.cc.o"
  "CMakeFiles/Agata.dir/src/AgataExternalEmission.cc.o"
  "CMakeFiles/Agata.dir/src/AgataExternalEmitter.cc.o"
  "CMakeFiles/Agata.dir/src/AgataGPSGenerator.cc.o"
  "CMakeFiles/Agata.dir/src/AgataGeneratorAction.cc.o"
  "CMakeFiles/Agata.dir/src/AgataGeneratorOmega.cc.o"
  "CMakeFiles/Agata.dir/src/AgataHitDetector.cc.o"
  "CMakeFiles/Agata.dir/src/AgataInternalEmission.cc.o"
  "CMakeFiles/Agata.dir/src/AgataInternalEmitter.cc.o"
  "CMakeFiles/Agata.dir/src/AgataLowEnergyPolarizedCompton.cc.o"
  "CMakeFiles/Agata.dir/src/AgataNuclearDecay.cc.o"
  "CMakeFiles/Agata.dir/src/AgataPhysicsList.cc.o"
  "CMakeFiles/Agata.dir/src/AgataPolarizedComptonScattering.cc.o"
  "CMakeFiles/Agata.dir/src/AgataReactionPhysics.cc.o"
  "CMakeFiles/Agata.dir/src/AgataRunAction.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSNCuts.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSensitiveDetector.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSpecialCuts.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSteppingAction.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSteppingOmega.cc.o"
  "CMakeFiles/Agata.dir/src/AgataSteppingPrisma.cc.o"
  "CMakeFiles/Agata.dir/src/AgataTestGenerator.cc.o"
  "CMakeFiles/Agata.dir/src/AgataVisManager.cc.o"
  "CMakeFiles/Agata.dir/src/CAngDistHandler.cc.o"
  "CMakeFiles/Agata.dir/src/CConvexPolyhedron.cc.o"
  "CMakeFiles/Agata.dir/src/CEvapDistHandler.cc.o"
  "CMakeFiles/Agata.dir/src/CGaspBuffer.cc.o"
  "CMakeFiles/Agata.dir/src/CProfileDistHandler.cc.o"
  "CMakeFiles/Agata.dir/src/CSpec1D.cc.o"
  "CMakeFiles/Agata.dir/src/CSpec2D.cc.o"
  "CMakeFiles/Agata.dir/src/Charge_State.cc.o"
  "CMakeFiles/Agata.dir/src/CsIDetector.cc.o"
  "CMakeFiles/Agata.dir/src/DiamondLycca.cc.o"
  "CMakeFiles/Agata.dir/src/DiamondTgt.cc.o"
  "CMakeFiles/Agata.dir/src/FPDetector.cc.o"
  "CMakeFiles/Agata.dir/src/G4LindhardPartition.cc.o"
  "CMakeFiles/Agata.dir/src/G4Transportation.cc.o"
  "CMakeFiles/Agata.dir/src/GalileoDetector.cc.o"
  "CMakeFiles/Agata.dir/src/GalileoPlunger.cc.o"
  "CMakeFiles/Agata.dir/src/Incoming_Beam.cc.o"
  "CMakeFiles/Agata.dir/src/Miniball.cc.o"
  "CMakeFiles/Agata.dir/src/MyMagneticField.cc.o"
  "CMakeFiles/Agata.dir/src/Nuball2Detector.cc.o"
  "CMakeFiles/Agata.dir/src/NuballClover.cc.o"
  "CMakeFiles/Agata.dir/src/NuballCloverBGO.cc.o"
  "CMakeFiles/Agata.dir/src/NuballLaBr3.cc.o"
  "CMakeFiles/Agata.dir/src/NuballLaBr3Valencia.cc.o"
  "CMakeFiles/Agata.dir/src/OrgamDetector.cc.o"
  "CMakeFiles/Agata.dir/src/OrsayOPSA.cc.o"
  "CMakeFiles/Agata.dir/src/OrsayPlastic.cc.o"
  "CMakeFiles/Agata.dir/src/Outgoing_Beam.cc.o"
  "CMakeFiles/Agata.dir/src/Outgoing_Beam_TargetEx.cc.o"
  "CMakeFiles/Agata.dir/src/PrismaField.cc.o"
  "CMakeFiles/Agata.dir/src/Reaction.cc.o"
  "CMakeFiles/Agata.dir/src/SiDetector.cc.o"
  "CMakeFiles/Agata.dir/src/SiTgtDetector.cc.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/Agata.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
