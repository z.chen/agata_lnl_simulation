#include <iostream>

using namespace std;

double EnergyToLight(double en){
  if (en <  0.4)              return (0.00+(0.06798*en)+(0.06034*en*en)+(0.05527*en*en*en))*10.;
  if (en >= 0.4 && en <  1.1) return (0.0089+(0.01275*en)+(0.18149*en*en)-(0.03206*en*en*en))*10.;
  if (en >= 1.1 && en <  3.0) return (-0.02060+(0.09088*en)+(0.11066*en*en)-(0.01055*en*en*en))*10.;
  if (en >= 3.0 && en <  6.4) return (-0.07129+(0.18958*en)+(0.06138*en*en)-(0.00326*en*en*en))*10.;
  if (en >= 6.4 && en <  8.9) return (3.85138-(1.36975*en)+(0.26122*en*en)-(0.01138*en*en*en))*10.;
  if (en >= 8.9 && en < 16.0) return (-0.76801+(0.57306*en)+(0.0*en*en)-(0.0*en*en*en))*10.;
  cout<<endl<<"ERROR\nEnergy loss range exceeded (while calculated light production)"<<endl;return -100.;
}

int main(){
  double e0,edep,ek,light;
  cout<<"Podaj energie poczatkowa w keVach"<<endl;
  cin>>e0;
  cout<<"Podaj strate energii w keVach"<<endl;
  cin>>edep;
  cout<<"<zalozenie protonu>"<<endl;
  cout<<"swiatlo: "<<(EnergyToLight(e0/1000.)-EnergyToLight((e0-edep)/1000.))*1000.<<" keVee"<<endl;
}
