#ifdef ANCIL
/////////////////////////////////////////////////////////////
/// This class handles the geometry of the NWall detector.
/// Written and mantained by Joa Ljungvall (joa@tsl.uu.se).
/// Last Modified 23/11/2005
/// Minor changes by E.Farnea 24/04/2006 to cope with
/// new geometry definition
/////////////////////////////////////////////////////////////


#ifndef AgataAncillaryNeutronWall_h
#define AgataAncillaryNeutronWall_h 1

#include <iostream>
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Trap.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh" 
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4AssemblyVolume.hh"
#include "G4RunManager.hh"
#include "G4ThreeVector.hh"
#include "G4Polyhedra.hh"
#include "G4Assembly.hh"
#include "G4PlacementVector.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4PlacedSolid.hh"
#include "G4BREPSolidPolyhedra.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VisAttributes;
class AgataAncillaryNeutronWallMessenger;
class AgataSensitiveDetector;
class AgataDetectorAncillary;

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class AgataAncillaryNeutronWall : public AgataAncillaryScheme 
{

public:
  AgataAncillaryNeutronWall(G4String,G4String);
  ~AgataAncillaryNeutronWall();

  //void Placement();
  void SetTranslatation(G4ThreeVector);
  void SetRotation(G4ThreeVector);
  void SetDistanceToFocus(G4double);
  void SetDepthOfLiquid(G4double);
  
  G4int GetSegmentNumber( G4int, G4int, G4ThreeVector );

private:

  G4String iniPath;
  //AgataSensitiveDetector* ancNW;
  AgataAncillaryNeutronWallMessenger* myMessenger;

  //  void ConstructAll(G4VPhysicalVolume*);
  void fixcorners(G4ThreeVector[]);
  G4Transform3D GetPentagonTransform ()    {return PentTransform;}
  G4Transform3D GetH10Transform      ()    {return H10Transform; }
  G4Transform3D GetH11Transform      ()    {return H11Transform; }
  G4Transform3D GetH12Transform      ()    {return H12Transform; }
  G4Transform3D GetH13Transform      ()    {return H13Transform; }
  G4Transform3D GetH14Transform      ()    {return H14Transform; }
  G4Transform3D GetH15Transform      ()    {return H15Transform; }
  G4Transform3D GetH26Transform      ()    {return H26Transform; }
  G4Transform3D GetH17Transform      ()    {return H17Transform; }
  G4Transform3D GetH28Transform      ()    {return H28Transform; }
  G4Transform3D GetH19Transform      ()    {return H19Transform; }
  G4Transform3D GetH210Transform     ()    {return H210Transform;}
  G4Transform3D GetH111Transform     ()    {return H111Transform;}
  G4Transform3D GetH212Transform     ()    {return H212Transform;}
  G4Transform3D GetH113Transform     ()    {return H113Transform;}
  G4Transform3D GetH214Transform     ()    {return H214Transform;}

private:
  //AgataDetectorConstruction* theDetector;
  //G4int FindMaterials();
  void PlaceDetector();

 G4int PentSeg(G4ThreeVector position);

 G4int H2Seg(G4ThreeVector position);

  G4int H1Seg(G4ThreeVector position);
  G4int ConvertSegmentNumber(G4int);


  G4String matNWallName,matAluminiumName,matVacuumName;
  G4Material *matNWall, *matAluminium, *matVacuum; 
  G4double densityDet, Plastic_thickness;
  G4ThreeVector PentTrans;
  G4RotationMatrix PentRot;
  G4Transform3D PentTransform, PentZRot;
  G4Transform3D H10Transform;
  G4Transform3D H11Transform;
  G4Transform3D H12Transform;
  G4Transform3D H13Transform;
  G4Transform3D H14Transform;
  G4Transform3D H15Transform,  H15ZRot;
  G4Transform3D H26Transform,  H26ZRot;
  G4Transform3D H17Transform,  H17ZRot;
  G4Transform3D H28Transform,  H28ZRot;
  G4Transform3D H19Transform,  H19ZRot;
  G4Transform3D H210Transform,  H210ZRot;
  G4Transform3D H111Transform,  H111ZRot;
  G4Transform3D H212Transform,  H212ZRot;
  G4Transform3D H113Transform,  H113ZRot;
  G4Transform3D H214Transform,  H214ZRot;
  G4ThreeVector NWallTrans;
  G4double nwallangle[3];
  G4RotationMatrix NWallRot;
  G4double Distance_From_Focus_To_Backplane;
  G4double Hexagon1_inside_height;

  public:
    inline G4int GetCrystalType( G4int /*detnum*/ ) { return -1; };

//  public:
//    inline G4String GetName () { return G4String("NWALL"); };

  public:
    G4int FindMaterials();
    void GetDetectorConstruction();
    void InitSensitiveDetector();
    void WriteHeader(std::ofstream &/*outFileLMD*/, G4double=1.*mm) {};
    void ShowStatus() {};
    void Placement();

};


#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
 
class AgataAncillaryNeutronWallMessenger: public G4UImessenger
{
  public:
    AgataAncillaryNeutronWallMessenger(AgataAncillaryNeutronWall*,G4String);
   ~AgataAncillaryNeutronWallMessenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataAncillaryNeutronWall* NWallDetector;
  G4UIdirectory* NWallDetDir;
  G4UIcmdWithADoubleAndUnit* PlasticRadius;
  G4UIcmdWithADoubleAndUnit* DistanceToFocus;
  G4UIcmdWithADoubleAndUnit* DepthOfLiquid;
  G4UIcmdWith3Vector* NWallTranslation;
  G4UIcmdWith3Vector* NWallRotation;
};

#endif

#endif
