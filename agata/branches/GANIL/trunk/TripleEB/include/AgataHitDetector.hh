//////////////////////////////////////////////////////////////////////
/// This class stores the relevant information concerning energy
/// depositions within the active parts of the geometry
/////////////////////////////////////////////////////////////////////
#ifndef AgataHitDetector_h
#define AgataHitDetector_h 1

#include "G4VHit.hh"
#include "G4THitsCollection.hh"
#include "G4Allocator.hh"
#include "G4ThreeVector.hh"

#include "G4Types.hh"

using namespace std;

class AgataHitDetector : public G4VHit
{
  public:
      AgataHitDetector();
     ~AgataHitDetector();
      AgataHitDetector(const AgataHitDetector&);
      
  private:
      G4int         trackID;        //> track number
      G4int         detNb;          //> detector number (where the interaction took place)
      G4int         cluNb;          //> cluster number (where the interaction took place)
      G4int         segNb;          //> segment number (where the interaction took place)
      G4int         interNum;       //> process which generated the interaction point
      G4double      edep;           //> energy deposition
      G4double      time;           //> time from the begin of event (single particle)
      G4ThreeVector dir;            //> initial direction of the primary particle which generated the track
      G4ThreeVector pos;            //> position of the interaction point (laboratory frame)
      G4ThreeVector relPos;         //> position of the interaction point (detector reference frame)
      G4String      nameVolume;     //> name of the solid where the interaction took place

  public:
      inline void* operator new(size_t);
      inline void  operator delete(void*);

  public:
      void Draw();
      void Print();

  public:
      void SetTrackID   (const G4int track)        { trackID = track;   };
      void SetDetNb     (const G4int det)          { detNb = det;       };  
      void SetCluNb     (const G4int clu)          { cluNb = clu;       };
      void SetSegNb     (const G4int seg)          { segNb = seg;       };
      void SetInterNb   (const G4int num)          { interNum = num;    };
      void SetEdep      (const G4double de)        { edep = de;         };
      void SetTime      (const G4double t0)        { time = t0;         };
      void SetDir       (const G4ThreeVector &abc) { dir = abc;         };
      void SetPos       (const G4ThreeVector &xyz) { pos = xyz;         }; 
      void SetRelPos    (const G4ThreeVector &xyz) { relPos = xyz;      }; 
      void SetName      (const G4String &name)     { nameVolume = name; };
      
  public:
      inline G4int         GetTrackID()   const    { return trackID;    };
      inline G4int         GetDetNb()     const    { return detNb;      };
      inline G4int         GetCluNb()     const    { return cluNb;      };
      inline G4int         GetSegNb()     const    { return segNb;      };
      inline G4int         GetInterNb()   const    { return interNum;   };
      inline G4double      GetEdep()      const    { return edep;       };  
      inline G4double      GetTime()      const    { return time;       };  
      inline G4ThreeVector GetDir()       const    { return dir;        };
      inline G4ThreeVector GetPos()       const    { return pos;        };
      inline G4ThreeVector GetRelPos()    const    { return relPos;     };
      inline G4String      GetName()      const    { return nameVolume; };
};

typedef G4THitsCollection<AgataHitDetector> AgataHitDetectorCollection;

extern G4Allocator<AgataHitDetector> AgataHitDetectorAllocator;

inline void* AgataHitDetector::operator new(size_t)
{
  void *aHit = (void *) AgataHitDetectorAllocator.MallocSingle();
  return aHit;
}

inline void AgataHitDetector::operator delete(void *aHit)
{
  AgataHitDetectorAllocator.FreeSingle((AgataHitDetector*) aHit);
}

#endif


