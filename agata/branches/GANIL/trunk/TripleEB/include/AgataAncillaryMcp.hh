#ifndef AgataAncillaryMcp_h
#define AgataAncillaryMcp_h 1

#include "globals.hh"
#include "G4Point3D.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include <vector>
#include "G4Point3DVector.hh"
#include "AgataDetectorAncillary.hh"

class CConvexPolyhedron;
class G4LogicalVolume;
class G4VisAttributes;
class G4VPhysicalVolume;

class CmcpPoints
{
  // default empty creator/destructor
  public:
     CmcpPoints() {};
    ~CmcpPoints() {};
  
  // all members are public  
  public:  
    G4int  whichMcp;
    
  public:  
    G4int  npoints;
    G4int  nfaces;
    
  public:  
    std::vector<G4Point3D> vertex;
    std::vector<G4int>     ifaces;
    
  // Mcp data
  public:  
    G4double  lx, ly, lz;

  public:  
    G4double  colx, coly, colz;    // its color components
    
  public:  
    CConvexPolyhedron   *pPoly;    // Mcp shape
    G4LogicalVolume     *pDetL;    // its logical
    
  public:  
    G4VisAttributes     *pDetVA;
};

class G4AssemblyVolume;

//////////////////////////////////////////////////////////////////////////////////////////
/// The Mcp are made with a CConvexPolyhedron having two sides on the x, y axes       
/// and its center placed in the centre of a G4AssemblyVolume. This way the
/// AssemblyVolume is placed in an "easy" way, while the intrinsic coordinates
/// within the Mcp are always larger than zero, simplifiying the analysis
/////////////////////////////////////////////////////////////////////////////////////
class CmcpAngles
{
  // default empty creator/destructor
  public:
     CmcpAngles() {};
    ~CmcpAngles() {};

  // all members are public  
  public:
    G4int                      whichMcp;  
    
  public:
    G4AssemblyVolume*          pAssV;       // pointer to the G4AssemblyVolume forming the Mcp
};

class CeulAngles
{
  // default empty creator/destructor
  public:
     CeulAngles() {};
    ~CeulAngles() {};

  // all members are public  
  public:  
    G4int     whichMcp;     
    G4int     numPhys;     

  public:  
    G4double  ps, th, ph;    // Rotation angles; they are needed since one cannot easily extract
                             // them from the rotation matrix

  public:
    G4ThreeVector trasl;      // traslation 

  public:
    G4RotationMatrix rotMat; // rotation matrix

  public:
    G4Transform3D *pTransf;  // full transformation
};

class G4Material;

class AgataAncillaryMcpMessenger;
class AgataAncillaryMcp : public AgataAncillaryScheme
{
  public:
    AgataAncillaryMcp(G4String,G4String);
    ~AgataAncillaryMcp();
    
  private:
    void ReadSolidFile();
    void ReadEulerFile();
    void ConstructTheMcps();
    void PlaceTheMcps();

  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();
    
  public:
    G4int GetSegmentNumber ( G4int, G4int, G4ThreeVector ) { return 0; };

  public:
    inline G4int GetCrystalType( G4int ) { return -1; };

  public:
    void SetSolidFile ( G4String );
    void SetAngleFile ( G4String );
    
  private:
    G4Material* matDet;
    G4String    matDetName;  
    
  private:
    AgataAncillaryMcpMessenger *myMessenger;
    
  // structures needed to store geometry data during the construction  
  private:
    std::vector<CeulAngles>   euler; 
    std::vector<CmcpPoints>   pgons; 
    std::vector<CmcpAngles>   clust;

  private:
    G4int                       nEuler;
    G4int                       nPgons;

  private:
    G4String                    iniPath;
    G4String                    eulerFile;
    G4String                    solidFile;
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class AgataAncillaryMcpMessenger: public G4UImessenger
{
  public:
    AgataAncillaryMcpMessenger(AgataAncillaryMcp*,G4String);
   ~AgataAncillaryMcpMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataAncillaryMcp*         myTarget;
    
    G4UIdirectory*             myDirectory;
    
    G4UIcmdWithAString*        SetSolidCmd;
    G4UIcmdWithAString*        SetAngleCmd;
};

#endif
