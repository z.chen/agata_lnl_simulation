////////////////////////////////////////////////////////////////
/// This class handles the visualization. It is fully copied
/// from one of the novice examples.
////////////////////////////////////////////////////////////////
#ifndef AgataVisManager_h
#define AgataVisManager_h 1

#ifdef G4VIS_USE

#include "G4VisManager.hh"

class AgataVisManager: public G4VisManager {

  public:
    AgataVisManager ();

  private:
    void RegisterGraphicsSystems ();

};

#endif

#endif
