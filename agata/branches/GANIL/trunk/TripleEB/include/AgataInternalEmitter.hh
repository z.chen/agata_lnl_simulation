/////////////////////////////////////////////////////////////////////////////
/// This class, inheriting from AgataEmitter, is used in the case of the
/// built-in event generation. Basically it only provides an interface
/// (messenger) to the AgataEmitter class.
/////////////////////////////////////////////////////////////////////////////

#ifndef AgataInternalEmitter_h
#define AgataInternalEmitter_h 1

#include "AgataEmitter.hh"

#include "globals.hh"

using namespace std;

class AgataInternalEmitterMessenger;

class AgataInternalEmitter : protected AgataEmitter
{
  public:
    AgataInternalEmitter(G4String);
   ~AgataInternalEmitter();
   
  private:
    AgataInternalEmitterMessenger* myMessenger; 
    
  public:
    void SetEmitterBetaAverage( G4double );
    void SetEmitterBetaSigma  ( G4double );
    
  public:
    void GetStatus();
    void PrintToFile( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV );
    
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithAString;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcmdWithoutParameter;

class AgataInternalEmitterMessenger: public G4UImessenger
{
  public:
    AgataInternalEmitterMessenger(AgataInternalEmitter*,G4String);
   ~AgataInternalEmitterMessenger();
    
  private:
    AgataInternalEmitter*      myTarget;
    G4UIdirectory*             myDirectory;

  private:
    G4UIcmdWithAnInteger*      SetSourceCmd;
    G4UIcmdWithADouble*        SetBetaCmd;
    G4UIcmdWithADouble*        SetBetaSigmaCmd;
    G4UIcmdWithADouble*        SetAngleCmd;
    G4UIcmdWith3Vector*        SetPositionCmd;
    G4UIcmdWith3Vector*        SetRecoilCmd;
    G4UIcmdWithAString*        SetThPhiCmd;
    G4UIcmdWithABool*          GaussAngleCmd;
    G4UIcmdWithABool*          UnifoAngleCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
};


#endif

