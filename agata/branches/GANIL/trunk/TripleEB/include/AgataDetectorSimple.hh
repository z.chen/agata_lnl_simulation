////////////////////////////////////////////////////////
/// This class handles a very simple geometry:
////////////////////////////////////////////////////////

#ifndef AgataDetectorSimple_h
#define AgataDetectorSimple_h 1

#include "AgataDetectorConstruction.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"

using namespace std;

class G4Material;
class AgataDetectorSimpleMessenger;

class AgataDetectorSimple : protected AgataDetectorConstructed
{
  public:
    AgataDetectorSimple(G4String);
    ~AgataDetectorSimple();

  private:
    G4double         cylRadius;
    G4double         cylHeight;
    G4double         capSpace;
    G4double         capThick;
    G4ThreeVector    position;
    
  private:
    G4String         matCrystName;
    G4Material      *matCryst;

  private:
    G4String         matCapName;
    G4Material      *matCap;

  private:
    G4String         matVacName;
    G4Material      *matVac;

  private:
    AgataDetectorSimpleMessenger*  myMessenger;

  private:
    G4int FindMaterials  ();
      
  public:
    void SetRadius     ( G4double );
    void SetHeight     ( G4double );
    void SetSpace      ( G4double );
    void SetThick      ( G4double );
    void SetPosition   ( G4ThreeVector );

  public:
    void Placement       ();
    void WriteHeader     ( std::ofstream &outFileLMD, G4double=1.*mm );
    void ShowStatus      ();
    
  public:
    inline G4int GetSegmentNumber ( G4int /*offset*/, G4int /*nGe*/, G4ThreeVector /*position*/ ) { return  0; };
    inline G4int GetCrystalType   ( G4int /*detNum*/ )                                            { return -1; };
};


#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;

class AgataDetectorSimpleMessenger: public G4UImessenger
{
  public:
    AgataDetectorSimpleMessenger(AgataDetectorSimple*,G4String);
   ~AgataDetectorSimpleMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataDetectorSimple*       myTarget;
    
    G4UIdirectory*             myDirectory;
    G4UIcmdWithADouble*        SetRadiusCmd;
    G4UIcmdWithADouble*        SetHeightCmd;
    G4UIcmdWithADouble*        SetSpaceCmd;
    G4UIcmdWithADouble*        SetThickCmd;
    G4UIcmdWith3Vector*        SetGePositionCmd;
          
};

#endif
