#include "AgataGeneratorAction.hh"

#include "AgataDefaultGenerator.hh"
#include "AgataAlternativeGenerator.hh"


AgataGeneratorAction::AgataGeneratorAction( G4String path, G4int genType, G4bool hadr, G4bool polar, G4String name )
{
  G4bool inter = true;
  if( genType ) inter = false;
  
  AgataDefaultGenerator*     theDefaultGenerator     = NULL;
  AgataAlternativeGenerator* theAlternativeGenerator = NULL;  
  
  switch( genType ) {
    case 0:
      theDefaultGenerator = new AgataDefaultGenerator( path, inter, hadr, polar, name );  
      theGeneration = (AgataGeneration*)theDefaultGenerator;
      break;
    case 1:
      theDefaultGenerator = new AgataDefaultGenerator( path, inter, hadr, polar, name );  
      theGeneration = (AgataGeneration*)theDefaultGenerator;
      break;
    case 2:
      theAlternativeGenerator = new AgataAlternativeGenerator();  
      theGeneration = (AgataGeneration*)theAlternativeGenerator;
      break;
    
  }
}

AgataGeneratorAction::~AgataGeneratorAction()
{
  delete theGeneration;
}

void AgataGeneratorAction::BeginOfRun()
{
  theGeneration->BeginOfRun();
}  

void AgataGeneratorAction::EndOfRun()
{
  theGeneration->EndOfRun();
}  

void AgataGeneratorAction::BeginOfEvent()
{
  theGeneration->BeginOfEvent();
}  

void AgataGeneratorAction::EndOfEvent()
{
  theGeneration->EndOfEvent();
}  

void AgataGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  theGeneration->GeneratePrimaries(anEvent);
}

void AgataGeneratorAction::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  theGeneration->PrintToFile( outFileLMD, unitL, unitE );
}

void AgataGeneratorAction::GetStatus()
{
  theGeneration->GetStatus();  
}

