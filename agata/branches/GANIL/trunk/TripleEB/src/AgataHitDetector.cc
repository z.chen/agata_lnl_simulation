#include "AgataHitDetector.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4ios.hh"
#include <iomanip>

G4Allocator<AgataHitDetector> AgataHitDetectorAllocator;

AgataHitDetector::AgataHitDetector() {}

AgataHitDetector::~AgataHitDetector() {}

AgataHitDetector::AgataHitDetector(const AgataHitDetector& theHit) : G4VHit()
{
  trackID        = theHit.trackID;
  detNb          = theHit.detNb;
  cluNb          = theHit.cluNb;
  segNb          = theHit.segNb;
  interNum       = theHit.interNum;
  edep           = theHit.edep;
  time           = theHit.time;
  dir            = theHit.dir;
  pos            = theHit.pos;
  relPos         = theHit.relPos;
  nameVolume     = theHit.nameVolume;
}

////////////////////////////////////////////////////////////////////////
/// This method draws the interaction points. It is obviously used only
/// in case the graphics is enebled!!!
////////////////////////////////////////////////////////////////////////
void AgataHitDetector::Draw()
{
#ifdef G4VIS_USE
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if(pVVisManager)
  {
    G4Circle circle(pos);
    circle.SetScreenSize(1.);
    circle.SetFillStyle(G4Circle::filled);
    G4Colour colour(1.,1.,1.);
    G4VisAttributes attribs(colour);
    circle.SetVisAttributes(attribs);
    pVVisManager->Draw(circle);
  }
#endif
}

//////////////////////////////////////////////////////////////////
/// This method is called only in cases of high verbosity levels
/// and is not well mantained. It should be changed in case
/// somebody really needs it.
//////////////////////////////////////////////////////////////////
void AgataHitDetector::Print()
{

  G4int prec = G4cout.precision(2);
  G4cout.setf(ios::fixed);

  G4cout  << "   trackID "  << std::setw(2) << trackID
          << "   det# "     << std::setw(6) << detNb
          << "   Edep "     << std::setw(7) << edep/keV << " keV"
          << "   pos ("     << std::setw(7) << pos.x()/cm
                            << std::setw(7) << pos.y()/cm
                            << std::setw(7) << pos.z()/cm << ") cm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

