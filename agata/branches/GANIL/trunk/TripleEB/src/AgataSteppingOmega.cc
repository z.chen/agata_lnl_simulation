#include "AgataSteppingOmega.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "G4RunManager.hh"

void AgataSteppingOmega::UserSteppingAction(const G4Step* theStep)
{
  // This "if" condition is needed, otherwise after the volume/solid angle
  // calculation it will be no more possible to track properly the other particles
  if( theStep->GetTrack()->GetDefinition()->GetParticleName() == "geantino" ) {
    G4RunManager * runManager = G4RunManager::GetRunManager();
    theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    G4int depthSensitive = theDetector->GeSD()->GetDepth();
    G4int detCode;
#ifdef GASP
    if( theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Tungsten" ||
        theStep->GetPreStepPoint()->GetMaterial()->GetName() == "BGO"      ||
        theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Lead"        )
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);  
#endif
    /// Material must be active germanium !!!
    if( theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Germanium" &&
        theStep->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetSensitiveDetector() != NULL ) { 
      detCode = theStep->GetPreStepPoint()->GetTouchable()->GetReplicaNumber(depthSensitive);
      theDetector->IncrementHits( detCode%1000 );
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);
    }
  }
}
