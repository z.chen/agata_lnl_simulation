#include "AgataDetectorSimple.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataDetectorSimple::AgataDetectorSimple()
{
  matCryst  = NULL;
  matName   = "Germanium";
  
  readOut   = false;
  
  size     = 50.*cm;
  position = G4ThreeVector(0., 0., 70.*cm);
  
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;

  myMessenger = new AgataDetectorSimpleMessenger(this);
}

AgataDetectorSimple::~AgataDetectorSimple()
{
  delete myMessenger;
}

G4int AgataDetectorSimple::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matCryst = ptMaterial;
    G4String nome = matCryst->GetName();
    G4cout << "\n ----> The detector material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  return 0;  
}


void AgataDetectorSimple::Placement()
{
  if( FindMaterials() ) return;
  
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  char sName[128];

  sprintf(sName, "gePbox%2.2d", 0);
  G4Box *pBox  = new G4Box(G4String(sName), size, size, size );

  sprintf(sName, "geDetL%2.2d", 0);
  G4LogicalVolume *pDetL  = new G4LogicalVolume( pBox, matCryst, G4String(sName), 0, 0, 0 );

  G4VisAttributes *pDetVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  pDetL->SetVisAttributes( pDetVA );

  // Sensitive Detector
  pDetL->SetSensitiveDetector( theDetector->GeSD() );

  // position
  G4RotationMatrix rm;
  rm.set(0,0,0);

  sprintf(sName, "geDetP%3.3d", 1);
  new G4PVPlacement(G4Transform3D(rm, position), 
                    G4String(sName), pDetL, theDetector->HallPhys(), false, 0);
  nDets++;
  nClus++;                  
  G4cout << " ----> Placed a single germanium crystal" << G4endl;
}

void AgataDetectorSimple::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  char line[256];
  outFileLMD << "BOX3" << G4endl << "SUMMARY 1" << G4endl;
  sprintf(line, "%3d %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f 1 1 1",
               0, 2.*size/unit, 2.*size/unit, 2.*size/unit, position.x()/unit, position.y()/unit, position.z()/unit); 
  
  outFileLMD << G4String(line) << G4endl;
}

void AgataDetectorSimple::ShowStatus()
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " Placed a single germanium crystal" << G4endl;
  G4cout << " Size " << 2.*size/cm << " cm" << G4endl;
  G4cout << " Position " << position/cm << " cm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////// methods for the messenger
/////////////////////////////////////////////////////////////////////////
void AgataDetectorSimple::SetSize( G4double siz )
{
  if( siz < 0. ) {
    G4cout << " Warning! Keeping previous value (" << size/mm << " mm)" << G4endl;
  }
  else {
    size = siz * mm;
    G4cout << " ----> Size is " << size/mm << " mm" << G4endl;
    size *= 0.5;
  }
}

void AgataDetectorSimple::SetPosition( G4ThreeVector pos )
{
  position = pos*mm;
  G4cout << " ----> Single Ge will be placed at " << position/mm << " mm" << G4endl;
}  

////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"

AgataDetectorSimpleMessenger::AgataDetectorSimpleMessenger(AgataDetectorSimple* pTarget)
:myTarget(pTarget)
{ 

  SetGeSizeCmd = new G4UIcmdWithADouble("/Agata/detector/sizeGe",this);  
  SetGeSizeCmd->SetGuidance("Define size of the single Ge detector.");
  SetGeSizeCmd->SetGuidance("Required parameters: 1 double (size in mm)");
  SetGeSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetGePositionCmd = new G4UIcmdWith3Vector("/Agata/detector/positionGe",this);  
  SetGePositionCmd->SetGuidance("Define position of the single Ge detector.");
  SetGePositionCmd->SetGuidance("Required parameters: 3 double (x, y, z in mm).");
  SetGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataDetectorSimpleMessenger::~AgataDetectorSimpleMessenger()
{

  delete SetGePositionCmd;
  delete SetGeSizeCmd;
}

void AgataDetectorSimpleMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetGePositionCmd ) {
    myTarget->SetPosition(SetGePositionCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetGeSizeCmd ) {
    myTarget->SetSize(SetGeSizeCmd->GetNewDoubleValue(newValue));
  }
  
}
  

