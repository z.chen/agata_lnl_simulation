############################################
### example of continuous (evaporative) 
### CM neutron spectrum
###########################################
### Format: E(MeV) intensity
############################################
# lines of comments begin with '#' (no blanks before!!!)
#######################################################
  0   28.739
  1   42.269
  2   41.010
  3   34.926
  4   25.426
  5   17.461
  6   11.928
  7    8.081
  8    5.383
  9    3.542
 10    2.326
 11    1.534
 12    1.014
 13     .666
 14     .430
 15     .273
 16     .170
 17     .105
 18     .064
 19     .039
 20     .023
 21     .014
 22     .008
 23     .005
 24     .003
 25     .002
 26     .001
 27     .001
 28     .000
 29     .000
