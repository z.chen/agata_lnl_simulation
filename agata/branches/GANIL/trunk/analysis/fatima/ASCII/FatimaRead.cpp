#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <stdint.h>
using std::setw;
using namespace std;

int main( int argc, char** argv ) 
{  
	string filestr;  //the filename to read (GammaEvents.0000)
	FILE *readingFile;
	char line[512], line2[512]; 
	int n=0;  //counter used during reading of COMSOL data file
	double Edep, geantX, geantY, geantZ;  //COMSOL data is double data type
	int Key, dummy;
	int  ClusterId=0;
	int RingId =0;
	int Evt=-1;
	int TotEvt=0;
	int FatimaEvt=0;
	      
	double Energy[8][3];  //empty array to hold output data during readout of events - one element per HEXITEC pixel

	filestr = "../../GammaEvents.Fatima.0000";  //the COMSOL data

	readingFile = fopen(filestr.c_str(),"r");  //open the file
	
	//Create an output file:
	char outputFile[512] = "FatimaResponse.dat";
	ofstream outFile(outputFile);
	outFile << "FrameNumber " << "Cluster " << "Ring " << "Energy" << endl;  //column headers
	


// Reading header
    while(line[0] != '$')
	{
    		fgets(line,512,readingFile);  //get a line from the input file
		cout << line << endl;
	}

   while(!feof(readingFile))  //keep going until end of input file
   //while(n!=100)  //keep going until end of input file
	{
		fgets(line,512,readingFile);  //get a line from the COMSOL file
		sscanf(line,"\t%i\t%lf\t%lf\t%lf\t%lf\t%i", &Key, &Edep, &geantX, &geantY, &geantZ, &dummy);  //extract values from the line
		n++;   //increment line counter

	if (Key==-1 && Evt!=dummy){  // new event with a incident gamma ray
	  //cout << "new event" << endl;
	TotEvt++;

	  // but first let's fill the output file with previous event data (if any)
	   if (ClusterId!=0)
	   {
	     FatimaEvt++;  // a new event seen by FATIMA (!! Not the multiplicity per event in FATIMA)

	  	for( int i=0; i<8; i++)
	  	{
	     	  for( int j=0; j<3; j++)
     	     	  {
		    if(Energy[i][j]!=0){

		// Todo: add the energy resolution of LaBr3 here.
		      outFile << FatimaEvt << " " << i << " " << j << " " << Energy[i][j] << endl;  //column headers

			}
		  }
                }
	    }

	//Reseting values:
	  Evt=dummy;
	  for( int i=0; i<8; i++)
	  {
	     for( int j=0; j<3; j++)
     	     {
		Energy[i][j]= 0;	
	     }	
	  }
	  ClusterId=RingId=0;


	} else if(Key>18000)
		{

		   ClusterId= int((Key -18000)/10);
		   RingId = int((Key -18000) % 10);
		   Energy[ClusterId-1][RingId-1]= Energy[ClusterId-1][RingId-1]+Edep;
		   //cout << Key << " " << ClusterId << " " << RingId << " " << Edep << " " << Energy[ClusterId-1][RingId-1] << endl;
		} 
	}	

	//cout << "No. of data lines values: " << n << endl;	//Report number of values extracted		
	cout << "No. of incoming particles: " << TotEvt << endl;	//Report number of values extracted		
	cout << "No. of Event seen in Fatima: " << FatimaEvt << endl;	//Report number of values extracted		


outFile.close();

return 0;  
} 

