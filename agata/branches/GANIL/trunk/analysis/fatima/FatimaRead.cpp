#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <stdint.h>

#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TRandom.h>

//using std::setw;
using namespace std;

int main( int argc, char** argv ) 
{  
	string filestr;  //the filename to read (GammaEvents.0000)
	FILE *readingFile;
	char line[512], line2[512]; 
	int n=0;  //counter used during reading of COMSOL data file
	double Edep, geantX, geantY, geantZ,tof;  //COMSOL data is double data type
	int Key, dummy;
	int  ClusterId=0;
	int RingId =0;
	int Evt=-1;
	int TotEvt=0;
	int FatimaEvt=0;

	int FTsigma=9.86; // in keV (3.5% at 662 keV)

	double beta;	

    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " BetaValue " << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }

   // Print the beta value:
   //std::cout << " " << std::endl;
   //std::cout << "Beta is " << argv[1] << "!" << std::endl;
   //std::cout << " " << std::endl;

	beta= atoi(argv[1]);

   // Print the beta value:
    std::cout << " " << std::endl;
    std::cout << "Beta is " << beta << "% !" << std::endl;
    beta=beta/100.; // 
    std::cout << "Beta = " << beta  << std::endl;
    std::cout << " " << std::endl;


	float Energy[8][3];  //empty array to hold output data during readout of events
	float Time[8][3];  //empty array to hold output data during readout of events

// Define Fatima data structure
	typedef struct {
		int FTevent, FTcluster, FTring;
		float FTenergy;
		float FTtime;
	}FTDATA;

	static FTDATA fatima;

// Create ROOT ntuple
	TFile* output_File= new TFile("FatimaTree.root","recreate");

	TTree* output_tree= new TTree("FATIMATree","Fatima");
	output_tree->Branch("Fatima", &fatima, "FTevent/I:FTcluster/I:FTring/I:FTEnergy/F:FTTime/F");

	//filestr = "../../GammaEvents.Fatima.0000";  //
	filestr = "../../GammaEvents.0000";  //
	//filestr = "../../GammaEvents.0ps";  //
	//filestr = "../../GammaEvents.200ps";  //
	//filestr = "../../GammaEvents.500ps";  //
	//filestr = "../../GammaEvents.1000ps";  //
	//filestr = "../../GammaEvents.1500ps";  //
	//filestr = "../../GammaEvents.2000ps";  //

	readingFile = fopen(filestr.c_str(),"r");  //open the file
	
	//Create an output file:
	char outputFile[512] = "FatimaResponse.dat";
	ofstream outFile(outputFile);
	outFile << "FrameNumber " << "Cluster " << "Ring " << "Energy" << endl;  //column headers
	


// Reading header
    while(line[0] != '$')
	{
    		fgets(line,512,readingFile);  //get a line from the input file
		//cout << line << endl;
	}

   while(!feof(readingFile))  //keep going until end of input file
   //while(n!=100)  //keep going until end of input file
	{
		fgets(line,512,readingFile);  //get a line from the COMSOL file
		sscanf(line,"\t%i\t%lf\t%lf\t%lf\t%lf\t%i\t%lf", &Key, &Edep, &geantX, &geantY, &geantZ, &dummy, &tof);  //extract values from the line
		n++;   //increment line counter

	if (Key==-1 && Evt!=dummy){  // new event with a incident gamma ray
	  //cout << "new event" << endl;
	TotEvt++;

	  // but first let's fill the output file with previous event data (if any)
	   if (ClusterId>0)
	   {
	     FatimaEvt++;  // a new event seen by FATIMA (!! Not the multiplicity per event in FATIMA)

	     for( int i=0; i<8; i++)  // 8det per ring
	  	{
	     	  for( int j=0; j<3; j++) // 3 rings
     	     	  {

		    if(Energy[i][j]>0.){

		        // convolution of the energy resolution of LaBr3 here.
			Energy[i][j]=gRandom->Gaus(Energy[i][j],FTsigma);

			//Doppler correction:
			if(j==0)  // forward ring
			{
			   Energy[i][j]=Energy[i][j]*(1-beta*cos((16.1)*3.14159/180.))/sqrt(1-beta*beta);
			}

			if(j==2)  //backward ring
			{
			   Energy[i][j]=Energy[i][j]*(1+beta*cos((16.1)*3.14159/180.))/sqrt(1-beta*beta);
			}

			//cout << gRandom->Gaus(Energy[i][j],FTsigma) << endl;

		// fill the ascii file 
		        outFile << FatimaEvt << " " << i+1 << " " << j+1 << " " << Energy[i][j] << endl;  //column headers

		// fill the tree:
			fatima.FTevent=FatimaEvt;
			fatima.FTcluster=i+1;  // 1 to 8
			fatima.FTring=j+1;     // 1 to 3
			fatima.FTenergy=Energy[i][j];
			fatima.FTtime=Time[i][j];
			output_tree->Fill();
		    }
		  }
                }
	   }

	//Reseting values:
	  Evt=dummy;
	  for( int i=0; i<8; i++)
	  {
	     for( int j=0; j<3; j++)
     	     {
		Energy[i][j]= 0;
		Time[i][j]=0;
	     }	
	  }
	  ClusterId=RingId=0;


	} else if(Key>18000)  // Fatima key(=18) *1000
		{

		   ClusterId= int((Key -18000)/10);    // 1 to 8 in input file
		   RingId = int((Key -18000) % 10);    // 0 to 2 in input file
		   Energy[ClusterId-1][RingId]= Energy[ClusterId-1][RingId]+Edep;
		   if(Time[ClusterId-1][RingId]==0) Time[ClusterId-1][RingId]=tof;
		   //cout << Key << " " << ClusterId << " " << RingId << " " << Edep << " " << Energy[ClusterId-1][RingId-1] << endl;
		} 
	}	

	//cout << "No. of data lines values: " << n << endl;	//Report number of values extracted		
	cout << "No. of incoming particles: " << TotEvt << endl;	//Report number of values extracted		
	cout << "No. of Event seen in Fatima: " << FatimaEvt << endl;	//Report number of values extracted		

// closing ascii outputfile
	outFile.close();

// writing and closing root file
	output_File->Write();
	output_File->Close();

return 0;  
} 

