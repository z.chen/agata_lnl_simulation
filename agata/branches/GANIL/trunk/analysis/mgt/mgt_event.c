/////////////////////////////////////////////////////////////////
////////////////// I/O and preprocessing of events //////////////
/////////////////////////////////////////////////////////////////

#include "mgt.h"

int     readAlreadyDone;                    // =1 se ee,xx,yy,zz,nd gia'letti in readgamma
int     linetag;                            // tag word of each new data line
char    linebuf[256];                       // read buffer

int     idmax16 = 0;                        // la massima distanza prodotta da smearpos (in mm/10)

double  poserr[MAXEKEV];                    // errori sulla posizione in funzione dell'energia

point   obuf[10*MAXEVLEN];                  // buffer dei dati come letti dal file
int     isgam[MAXMULT];                     // object is a gamma
int     oigam[MAXMULT];                     // posizione del gamma originale
int     ongam[MAXMULT];                     // quanti punti nel gamma originale
int     ohit1[MAXMULT];                     // quale dei punti e' la prima interazione
double  oegam[MAXMULT];                     // l'energia del gamma originale
double  oener[MAXMULT];                     // energia rivelata
double  ocdir[MAXMULT];                     // coseno dell'angolo di emissione rispetto alla direzione di rinculo
int     nobuf;                              // numero di punti originali

point   pbuf[10*MAXEVLEN];                  // buffer globale dei punti (compresi gli Egam < 0 e i compattati)
int     npbuf;                              // numero di punti usati

point  *ppgam[10*MAXEVLEN];                 // puntatori ai punti dei gamma dopo preprocessamento individuale
int     ipgam[MAXMULT];                     // inizio del gamma preprocessato
int     npgam[MAXMULT];                     // quanti punti nel gamma preprocessato
double  epgam[MAXMULT];                     // energia totale rivelata del gamma preprocessato
int     nppgam;                             // numero di punti dopo preprocessamento individuale
int    *nevdets;                            // per contare il numero di rivelatori e di punti in ogniuno di essi
int    *nevsegs;                            // per contare il numero di segmenti e di punti in ogniuno di essi
double *enedets;                            // per contare l'energia di ogni rivelatore
double *enesegs;                            // per contare l'energia di ogni segmento
point  *plink[MAXEVLEN];                    // puntatori per impaccamento "link"

void   gostartofdata    ();                     // go to start of data (i.e. after $ mark) in input file
int   (*readgamma)      (point *, int);         // select read format for GEANT data file
int    readgammaM       (point *, int);         // old MARS  format 
int    readgammaA       (point *, int);         // AGATA format: GENERATOR 0
int    readgammaG       (point *, int);         // AGATA format: GENERATOR 1
int    buildgamma       (point *, int);         // basic processing of gamma
int    counthitdets      ();                    // how many different detectors in original event
double openingAngle     (point *, int);         // determine opening angle of gamma
double openingDistance  (point *, int);         // determine opening distance of gamma
void   preprgammas      ();                     // gamma specific processing
void   preprevent       ();                     // event specific processing
void   smearpos         (point **, int);        // smearing of points position
void   smearener        (point **, int);        // smearing of points energy
void   smeareseg        (point **, int);        // smearing of points energy for multiple hit in segment
int   (*distpack)       (point **, int);        // selects packing function
int    distpackL        (point **, int);        // packing together points close to each other "link" method
int    distpackP        (point **, int);        // packing together points close to each other "pairwise"
int    segpack          (point **, int);        // packing together points in same segment
void   merge2points     (point * , point *);    // merges 2 points weighting positions with energy. Result in 1st
void   mergeNpoints     (point **, int N);      // merges N points weighting positions with energy. Result in 1st
int    packplist        (point **, int);        // remove NULL pointers from the list of point
int    checkemin        (point **, int);        // energy threshold
int    checkxyz         (point **, int);        // restrict coordinates range
void   printgamoriginal (point **, int, int);   // print original gamma
void   stathitdets      (point **, int);        // how many different detectors in preprocessed event
void   stathitsegs      (point **, int);        // how many different segments in event
void   statcross        (point **, int, int);   // statistics of interaction lengths

///////////////////////////////////////////////////////////
int    flushAncBuffer;


#ifdef FORPRISMA
int    eventCounter = 0;                    // contatore di eventi letti da file

int    get_event_counter();
void   prismaEventHeader();
void   prismaEventFooter();

int    
get_event_counter()
{
  return eventCounter;
}

void
prismaEventHeader()
{
  fprintf( ofp, "-100 %7d\n", get_event_counter() );
  return;
}

void
prismaEventFooter()
{
  fprintf( ofp, "$\n" );
  return;
}
#endif


///////////////////////////////////////////////////////////

void
initevent()
{
  int ii;

  distpack = distpackP;     // pack points "pair"-wise
//distpack = distpackL;     // pack points "link"-wise

  if(!disterr) {
    memset(poserr, 0, sizeof(double)*MAXEKEV);
  }
  else {
    for(ii = 1; ii < MAXEKEV; ii++) {
#if FIXEDSMEARDIST == 1
      poserr[ii] = disterr;                         // for energy independent position error
#else
      poserr[ii] = disterr * sqrt(100.0 / ii);      // if energy    dependent position error
      poserr[ii] = (disterr + poserr[ii]) / 2;      // to smooth the extreme behaviour of sqrt
      poserr[ii] = MAX(poserr[ii], MINPOSERR);      //
      poserr[ii] = MIN(poserr[ii], MAXPOSERR);      //
#endif
    }
    poserr[0] = poserr[1];
#if   SMEARPOS == CUBE
    for(ii = 0; ii < MAXEKEV; ii++) poserr[ii] *= 2.;
#elif SMEARPOS == SPHE
    for(ii = 0; ii < MAXEKEV; ii++) poserr[ii] *= 2.;
#elif SMEARPOS == GAUSS1
    for(ii = 0; ii < MAXEKEV; ii++) poserr[ii] /= SIG2FW;
#elif SMEARPOS == GAUSS2
    for(ii = 0; ii < MAXEKEV; ii++) poserr[ii] *= 2./SIG2FW;
#elif SMEARPOS == GAUSS3

#elif SMEARPOS == GAUSS4

#endif
  }
  
  nevdets = (int    *)malloc(ngedets*sizeof(int));
  enedets = (double *)malloc(ngedets*sizeof(double));
  
  if(nsegtot > 1) {
    nevsegs = (int    *)malloc(nsegtot*sizeof(int));
//  enesegs = (double *)malloc(nsegtot*sizeof(double));
  }

  switch (dataFileVersion) {
  case FMARS:
    readgamma = readgammaM;
    break;
  case FAGATA:
    if(evgen)
      readgamma = readgammaG;
    else
      readgamma = readgammaA;
    break;
  default:
    printf("File format not recognized: %d\n", dataFileVersion);
    errexit("initevent");
  }
  
  if( doAncillary )
    flushAncBuffer = 1;
  else
    flushAncBuffer = 0;  

  if(doWriteEvs+doPrismaEvs)
    writeheader();
  gostartofdata();

}

void
finitevent()
{
  int *ptspec;
  int ii, sum;
  int peak, back;
  double rho, rhomax1, rhomax2;

  ptspec = specPointer(16, 0);

  sum = 0;
  rhomax1 = rhomax2 = 0.;
  for(ii = 0; ii < MIN(2000, idmax16); ii++) {
    rho  = ptspec[ii] / (pow(ii+1., 3.)-pow((double)ii, 3.));   // densita' locale
    rhomax1 = MAX(rho, rhomax1);
    sum += ptspec[ii];
    rho  = sum / pow(ii+1., 3.);                                // densita' integrata
    rhomax2 = MAX(rho, rhomax2);
  }

  if(sum) {
    rhomax1 = 1000./rhomax1;
    rhomax2 = 1000./rhomax2;
    sum = 0;
    for(ii = 0; ii < MIN(1000, idmax16); ii++) {                // per non sovrascrivere la densita' integrata
      rho = rhomax1 * ptspec[ii] / (pow(ii+1., 3.) - pow((double)ii, 3.));
      ptspec[5000 + ii] = (int)(rho+0.5);
      sum += ptspec[ii];
      rho  = rhomax2 * sum / pow(ii+1., 3.);
      ptspec[6000 + ii] = (int)(rho+0.5);
    }
  }

  if(!evgen) {
    printf("\n  Individual Total Response       *\n");    // response function ai singoli gamma
    printf(" Energy  Emitt   Effi    P/T      *\n");
    ptspec = specPointer(15, 0);
    for(ii = 0; ii < datmult; ii++) {
      sum  = ptspec[ii];
      if(sum) {
        printf("%7.1f%7d", dategam[ii], sum);
        peak = ptspec[ii + 100];
        back = ptspec[ii + 200];
        if(peak+back) printf("%7.2f%7.2f      *", (100.*peak)/sum, (100.*peak)/(peak+back));
        printf("\n");
      }
    }
  }
}

void
gostartofdata()
{
  if(ifp == NULL) {
    if( (ifp = fopen(ifname, "r")) == NULL) {
      printf("Error opening %s\n", ifname);
      errexit("gostartofdata");
    }
  }
  else {
    fseek(ifp, 0, SEEK_SET);
  }
  while(1) {
    if(fgets(linebuf, 255, ifp) == NULL) {
      printf("Error reading %s while searching for beginning of data mark $\n", ifname);
      errexit("gostartofdata");
    }
    if(linebuf[0] == '$') {
      readAlreadyDone = 0;
      return;
    }
  }
}

point *
getptogam(int gamnum)
{
  return obuf + oigam[gamnum];
}

void
nextevent()
{
  int     gamlen, ngtot, ndtot, ngood, isgood, done;
  double  eseen, cdopp, opclu;
  point  *ppt;

  npev   = 0;

#ifdef TAPERED    
  onlyAncil = -1;
#endif  

#ifdef FORPRISMA
  eventCounter++;
#endif

///////////////////////////////////
//////// select multiplicity //////
///////////////////////////////////

  if(!varmult)
    evmult = mult;
  else {
    do {
      evmult = (int) (varmult + sigmult * gaussrand()); // distribuzione gaussiana attorno a varmult
    } while (evmult > mult || evmult < 1);
  }

//////////////////////////////////////////////////////////////
//////////// read evmult gammas into obuf ////////////////////
//////////////////////////////////////////////////////////////

  nobuf = gamnum = done = 0;

  while(1) {
    if( doAncillary && flushAncBuffer ) {
      //printf( " Flushing ancillary buffer ...\n" );
      resetAncBuffer();
      flushAncBuffer = 0;
    }

    ppt = obuf + nobuf;
    gamlen = readgamma(ppt, 10*MAXEVLEN-nobuf); // read next gamma
    if(gamlen == 0) {                           // EOD or read error
      npev = -1;
      return;
    }
    if(gamlen < 0) {                          // end of GENERATOR defined event
      done   = 1;
      gamlen = -gamlen;
    }
    isgam[gamnum] = (ppt->nd == -1) ? 1 : 0;  // gamma or another particle
    oigam[gamnum] = nobuf;                    // object begins here
    ongam[gamnum] = gamlen;                   // and contains gamlen points (header included)
    nobuf     += gamlen;
    ndataread += gamlen;
    nobufmax   = MAX(gamlen, nobufmax);
    gamnum++;
    if(gamnum >= MAXMULT) {
      printf("More than MAXMULT (%d) gammas in event\n", MAXMULT);
      errexit("nextevent");
    }
    if(evgen) {
      if(done) {
        evmult = gamnum;
        flushAncBuffer = 1;
        break;
      }
    }
    else {
      if(gamnum >= evmult) {
        flushAncBuffer = 1;
        break;
      }  
    }
  }
  
  if(evnum < evskip1) return;

#ifdef TAPERED    
  if( onlyAncil < 0 ) {
    if( doSort ) {
      if( doAncillary ) {
	eventHeader();
	addAncillaryToEvent();
	sendEventToBuffer();
      }
    }
  }
#endif    

////////////////////////////////////////////////////////////
//////////// basic processing of gammas ////////////////////
////////////////////////////////////////////////////////////
  for(gamnum = 0; gamnum < evmult; gamnum++) {
    gamlen =        ongam[gamnum];
    ppt    = obuf + oigam[gamnum];
    gamlen = buildgamma(ppt, gamlen); // basic processing of gamma (or other emitted objects)
  }

  specIncr(evmult + 300, 15, 0);      // distribuzione di molteplicita'
  gamtot += evmult;
  
///////////////////////////////////
// detector multiplicity trigger //
///////////////////////////////////

  if(kdetMin) {
    ndtot = counthitdets();
    if(ndtot < kdetMin) {
      evrejected++;
      return;
    }
  }

  if(llist) printf("\n=================   Event# %4d    Mult = %3d  =======================\n", evnum, evmult);

//////////////////////////////////////////////////////////////
///////// analysis of the original gammas ////////////////////
//////////////////////////////////////////////////////////////
  
  ngtot = ngood = 0;
  for (gamnum = 0; gamnum < evmult; gamnum++) {
    gamlen = ongam[gamnum];
    if(gamlen > 1 && isgam[gamnum]) {             // a non empty gamma
      ngtot++;
      eseen  = oener[gamnum];
      isgood = fabs(eseen -oegam[gamnum]) < peakwidth[(int)eseen];
      if(isgood) ngood++;
      specIncrGain(eseen, 7, 0);                  //////// 7 = spettro originale GEANT non corretto doppler
      if(recoil.beta) {
        cdopp  = recoil.gamma * (1. - recoil.beta * ocdir[gamnum]);
        eseen *= cdopp;
      }
      specIncrGain(eseen, 1, 0);                  //////// 1  = spettro originale GEANT (evt. corretto doppler)
      ppt = obuf + oigam[gamnum];

      //printf("\n DC2TH= %f\n", DC2TH(ppt));   
      specIncr((int)(DC2TH(ppt)), 17, 0);         //////// 18 = distribuzione angolare in LAB (solo se rivelati)

      if(gamlen > 2) {
        opclu = openingAngle(obuf+oigam[gamnum]+1, gamlen-1);
        if(isgood)
          specIncr((int)opclu       , 14, 0);     // apertura angolare dei gamma buoni   (esclusi single hit)
        else
          specIncr((int)opclu + 2000, 14, 0);     // apertura angolare dei gamma cattivi (esclusi single hit)
        opclu = openingDistance(obuf+oigam[gamnum]+1, gamlen-1);
        if(isgood)
          specIncr((int)(10*opclu) + 4000, 14, 0);// apertura angolare dei gamma buoni   (esclusi single hit)
        else
          specIncr((int)(10*opclu) + 6000, 14, 0);// apertura angolare dei gamma cattivi (esclusi single hit)
      }
    }
  }
  specIncr(ngtot + 900, 15, 0);                   // numero di gamma visti
  if(llist) printf("\n%4d gammas detected\n%4d of them in peak\n", ngtot, ngood);

  if(nobuf == evmult) {
    specIncr(350, 15, 0);                         // conta gli eventi vuoti
    return;
  }
  specIncr(nobuf-evmult + 500, 15, 0);            // distribuzione #punti dell'evento GEANT

////////////////////////////////////////////////////////
/////// Preprocess the data as individual gammas  //////
////////////////////////////////////////////////////////

  memcpy(pbuf, obuf, nobuf * sizeof(point));      // copy original event into (pre)processing buffer
  npbuf  = nobuf;                                 // starting with the same number of point buffers
  nppgam = 0;                                     // numero di puntatori ai punti preprocessati
  preprgammas();                                  // preprocess the gammas individually
  if(nppgam == 0) {
    specIncr(360, 15, 0);                         // conta gli eventi vuoti
    return;
  }
  specIncr(nppgam + 1200, 15, 0);                 // distribuzione #punti dell'evento preprocessato individualmente
  
//////////////////////////////////////////////
/////// Preprocess the data as an event //////
//////////////////////////////////////////////

  npev = nppgam;
  if(npev) {
    memcpy(ppev, ppgam, npev * sizeof(point *));
    preprevent();                                 // preprocess the event globally
  }
  if(npev == 0) {
    specIncr(370, 15, 0);                         // conta gli eventi vuoti
    return;
  }
  specIncr(npev + 1500, 15, 0);                   // distribuzione #punti dell'evento preprocessato globalmente
  stathitdets(ppev, npev);                        // numero di rivelatori e punti per rivelatore dell'evento
  if(nsegtot > 1) stathitsegs(ppev, npev);        // numero di segmenti e punti per segmeneto dell'evento

  npevmax  = MAX(npev, npevmax);
  npacked += npev;

  return;
}

// old MARS format
// fixed-format single-gamma events
int
readgammaM(point *ppp, int maxpts)
{
  int    npts;
  point *lpp;
  static double ee, xx, yy, zz;
  static int    nd, nr = 0;
  double egamma, eseen;

A:
  if(readAlreadyDone) {
    readAlreadyDone = 0;              // don't read but reset to normal
  }
  else {
    nr = fscanf(ifp, "%lf %lf %lf %lf %d", &ee, &xx, &yy, &zz, &nd);
    if(nr == EOF) return 0;
    if(nr != 5) {
      printf("Error reading event\n");
      errexit("readgammaM");
    }
  }
  if(nd >= 0) {
    printf("Sync error reading event (nd = %d) should be negative\n", nd);
    errexit("readgammaM");
  }
  if(nd < -100) {
    if(nd == -101) {                  // new beta and direction of source
	    recoil.beta  = ABS(ee);
	    recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);
	    recoil.cx    = xx;
	    recoil.cy    = yy;
	    recoil.cz    = zz;
      goto A;
    }
    else if(nd == -102) {             // new position of emission point
	    origin.xx  = xx*distfactor;
	    origin.yy  = yy*distfactor;
	    origin.zz  = zz*distfactor;
	    origin.rr  = sqrt(xx*xx + yy*yy + zz*zz)*distfactor;
      goto A;
    }
    else {
      printf("Unknown emission tag (nd =%d)\n", nd);
      errexit("readgammaM");
    }
  }

  if(ee >= 0.) {
    printf("Sync error reading event (ee = %f)  should be negative\n", ee);
    errexit("readgammaM");
  }

  lpp     = ppp;                      // start of the new gamma (or any other emitted object)
  lpp->ee = ee;
  lpp->xx = origin.xx;
  lpp->yy = origin.yy;
  lpp->zz = origin.zz;
  lpp->cx = xx;
  lpp->cy = yy;
  lpp->cz = zz;
  lpp->nd = nd;
  lpp++;

  egamma  = ABS(ee);
  eseen   = 0;
  npts    = 0;
  while (1) {
    nr = fscanf(ifp, "%lf %lf %lf %lf %d", &ee, &xx, &yy, &zz, &nd);
    if(nr != 5 ) {
      if(nr == EOF) break;
    printf("Error reading event\n");
    errexit("readgammaM");
    }
    lpp->ee = ee*enerfactor;
    lpp->xx = xx*distfactor;
    lpp->yy = yy*distfactor;
    lpp->zz = zz*distfactor;
    lpp->nd = nd;
    if(lpp->nd >= 0 ) {               // usual interaction point
      if(lpp->ee < 0.) {
        printf("Sync error reading event (ee = %f) should not be negative\n", nd);
        errexit("readgammaM");
      }
      eseen += lpp->ee;
      npts++;
      if(npts+1 >= maxpts) {
        printf("Too many interaction points (%d)\n", npts);
        errexit("readgammaM");
      }
      lpp++;
    }
    else {
      readAlreadyDone = 1;            // mark as already done
      break;
    }
  }

// decide what to do with the off-range gammas
  if(egamma < minegam || egamma > maxegam) {
    if(egamma < minegam) egamma = minegam;
    else                 egamma = maxegam;
    ppp->ee = egamma;
  }

// possibility to read only peak or background events
#if SELECTEVENTS != EVALL
  if(eseen > maxegam) goto A;
#if   SELECTEVENTS == EVPEAK
  if(!peaklut[(int)eseen]) goto A;    // to select peak events
#elif SELECTEVENTS == EVBACK
  if( peaklut[(int)eseen]) goto A;    // to select background events
#endif
#endif

  return ++npts;

}

// AGATA format: GENERATOR 0
// variable length events terminated by -100 (or EOF)
int
readgammaA(point *ppp, int maxpts)
{
  int    npts, nna;
  point *lpp;
  static int nd, nr = 1;
  double egamma, eseen;
//double tt;
  double ee, xx, yy, zz;
  int    ss;

A:
  if(readAlreadyDone) {
    readAlreadyDone = 0;              // don't read but reset to normal
  }
  else {
    nr = fscanf(ifp, "%d", &linetag);
    if(nr != 1 && nr != EOF) goto errout;
    if(fgets(linebuf, 255, ifp) == NULL) {
      if(feof(ifp)) return 0;
      goto errout;
    }
  }

  if(linetag >= 0) {
    printf("Sync error reading event: emission tag should be negative in line %d %s\n", linetag, linebuf);
    errexit("readgammaA");
  }

  if(linetag < -100) { 
    switch (linetag) {
    case -101:            // new beta and direction of source
      if( !recForced ) {
        nr = sscanf(linebuf, "%lf %lf %lf %lf", &recoil.beta, &recoil.cx, &recoil.cy, &recoil.cz);
        if(nr != 4) goto errout;
        recoil.beta  = ABS(recoil.beta);
        recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);
      }
      goto A;
    case -102:            // new position of emission point
      if( !posForced ) {
        nr = sscanf(linebuf, "%lf %lf %lf", &origin.xx, &origin.yy, &origin.zz);
        if(nr != 3) goto errout;
        origin.xx *= distfactor;
        origin.yy *= distfactor;
        origin.zz *= distfactor;
        origin.rr  = sqrt(origin.xx*origin.xx + origin.yy*origin.yy + origin.zz*origin.zz);
      }
      goto A;
    case -103:            // new emission time (ignored)
      //nr = fscanf(ifp, "%lf", &tt);
      //if(nr != 1) goto errout;
      goto A;
    default:
      printf("Unknown emission tag in line %d %s\n", linetag, linebuf);
      errexit("readgammaG");
    }
  }

  // start of a new gamma (or any other emitted object)
  lpp = ppp;
  nr = sscanf(linebuf, "%lf %lf %lf %lf %d", &lpp->ee, &lpp->cx, &lpp->cy, &lpp->cz, &evnumMC);
  if(nr != 5) goto errout;
  lpp->nd  = linetag;
  lpp->ee *= enerfactor;
  lpp->xx  = origin.xx;
  lpp->yy  = origin.yy;
  lpp->zz  = origin.zz;

  egamma = lpp->ee;
  eseen  = 0;
  npts   = nna = 0;
  while (1) {
    nr = fscanf(ifp, "%d", &linetag);
    if(nr != 1) {
      if (nr == EOF) break;
      else           goto errout;
    }
    if(fgets(linebuf, 255, ifp) == NULL) {
      if(feof(ifp)) return 0;
      goto errout;
    }
    if(linetag < 0) {
      // new gamma or emission tag
      readAlreadyDone = 1;      // mark as already done
      break;                    // and finish reading
    }

    if( linetag >= minOffset ) {
      // ignores points not coming from germanium detectors,
      // rather dispatch data to the ancillary buffer
      if(doAncillary) {
        addToAncBuffer( linetag, linebuf );
      }	
      continue;
    }
    // decode and record the interaction point in the germanium detectors
    nr = sscanf(linebuf, "%lf %lf %lf %lf %d", &ee, &xx, &yy, &zz, &ss);
    if(nr != 5) goto errout;
    if(ee > 0.) {             // ignore completely non positive-energy deposits
      lpp++;
      lpp->nd = linetag;
      lpp->ee = ee*enerfactor;
      lpp->xx = xx*distfactor;
      lpp->yy = yy*distfactor;
      lpp->zz = zz*distfactor;
      lpp->ss = ss;
      lpp->ns = 6*(lpp->ss/10) + lpp->ss%10;    // to be made dependent on nsegsP and nsegsZ !!!!!!!!!!!!!!!!!
      eseen  += lpp->ee;
 
      // this check is to fix exxessive writing by geant4 for hadrons
      // only if this is the first point or it does not exactly overlap the previous point
      if(!npts || lpp->xx != lpp[-1].xx || lpp->yy != lpp[-1].yy || lpp->zz != lpp[-1].zz) {
        npts++;
        if(npts+1 >= maxpts) {  // protection against too-long events
          printf("Too many interaction points (%d)\n", npts);
          errexit("readgammaG");
        }
        nna = 0;                                  // reset overlap counter
      }
      else {
        // take only energy and ignore this point if it is exactly overlapping the previous one
        lpp[-1].ee += lpp->ee;
        lpp--;
        nna++;
      }
#ifdef TAPERED    
      onlyAncil++;  
#endif
    }
  }
  
  // decide what to do with the off-range gammas
  if(egamma < minegam || egamma > maxegam) {
    if(egamma < minegam) egamma = minegam;
    else                 egamma = maxegam;
    ppp->ee = egamma;
  }

// possibility to read only peak or background events
#if SELECTEVENTS != EVALL
  if(eseen > maxegam) goto A;
#if   SELECTEVENTS == EVPEAK
  if(!peaklut[(int)eseen]) goto A;    // to select peak events
#elif SELECTEVENTS == EVBACK
  if( peaklut[(int)eseen]) goto A;    // to select background events
#endif
#endif

  return ++npts;

errout:
  errexit("Error reading event in readgammaG");
  return 0;

}

// AGATA format: GENERATOR 1
// variable length events terminated by -100 (or EOF)
int
readgammaG(point *ppp, int maxpts)
{
  int    npts;
  point *lpp;
  double egamma, eseen;
  double ee, xx, yy, zz;
//double tt;
  int    nr, ss;

A:
  if(readAlreadyDone) {
    readAlreadyDone = 0;              // don't read but reset to normal
  }
  else {
    nr = fscanf(ifp, "%d", &linetag);
    if(nr != 1 && nr != EOF) goto errout;
    if(fgets(linebuf, 255, ifp) == NULL) {
      if(feof(ifp)) return 0;
      goto errout;
    }
  }
  
  // emission tags
  if(linetag <= -100) {
    switch (linetag) {
    case -100:            // a leftover event separator ??
#ifdef FORPRISMA
      nr = sscanf(linebuf, "%d", &eventCounter);
      if(nr != 1) goto errout;
#endif
      goto A;
    case -101:            // new beta and direction of source
      if( !recForced ) {
        nr = sscanf(linebuf, "%lf %lf %lf %lf", &recoil.beta, &recoil.cx, &recoil.cy, &recoil.cz);
        if(nr != 4) goto errout;
        recoil.beta  = ABS(recoil.beta);
        recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);
      }
      goto A;
    case -102:            // new position of emission point
      if( !posForced ) {
        nr = sscanf(linebuf, "%lf %lf %lf", &origin.xx, &origin.yy, &origin.zz);
        if(nr != 3) goto errout;
        origin.xx *= distfactor;
        origin.yy *= distfactor;
        origin.zz *= distfactor;
        origin.rr  = sqrt(origin.xx*origin.xx + origin.yy*origin.yy + origin.zz*origin.zz);
      }
      goto A;
    case -103:            // new emission time (ignored)
      //nr = fscanf(ifp, "%lf", &tt);
      //if(nr != 1) goto errout;
      goto A;
    default:
      printf("Unknown emission tag in line %d %s\n", linetag, linebuf);
      errexit("readgammaG");
    }
  }

  // start of a new gamma (or any other emitted object)
  lpp = ppp;
  nr = sscanf(linebuf, "%lf %lf %lf %lf %d", &lpp->ee, &lpp->cx, &lpp->cy, &lpp->cz, &evnumMC);
  if(nr != 5) goto errout;
  lpp->nd  = linetag;
  lpp->ee *= enerfactor;
  lpp->xx  = origin.xx;
  lpp->yy  = origin.yy;
  lpp->zz  = origin.zz;

  egamma = lpp->ee;
  eseen  = 0;
  npts   = 0;
  while (1) {
    nr = fscanf(ifp, "%d", &linetag);
    if(nr != 1) {
      if (nr == EOF) break;
      else           goto errout;
    }
    if(fgets(linebuf, 255, ifp) == NULL) {
      if(feof(ifp)) return 0;
      goto errout;
    }
    if(linetag < 0) {
      // new gamma or emission tag
      readAlreadyDone = 1;      // mark as already done
      break;                    // and finish reading
    }
    if( linetag >= minOffset ) {
      // ignores points not coming from germanium detectors,
      // rather dispatch data to the ancillary buffer
      if(doAncillary)
        addToAncBuffer( linetag, linebuf );
      continue;
    }
    // decode and record the interaction point in the germanium detectors
    nr = sscanf(linebuf, "%lf %lf %lf %lf %d", &ee, &xx, &yy, &zz, &ss);
    if(nr != 5) goto errout;
    if(ee > 0.) {             // ignore completely non positive-energy deposits
      lpp++;
      lpp->nd = linetag;
      lpp->ee = ee*enerfactor;
      lpp->xx = xx*distfactor;
      lpp->yy = yy*distfactor;
      lpp->zz = zz*distfactor;
      lpp->ss = ss;
      lpp->ns = 6*(lpp->ss/10) + lpp->ss%10;    // to be made dependent on nsegsP and nsegsZ !!!!!!!!!!!!!!!!!

      npts++;
      if(npts+1 >= maxpts) {  // protection against too-long events
        printf("Too many interaction points (%d)\n", npts);
        errexit("readgammaG");
      }

      eseen  += lpp->ee;
#ifdef TAPERED    
      onlyAncil++;  
#endif
    }
  }

// decide what to do with the off-range gammas
  if(egamma < minegam || egamma > maxegam) {
    if(egamma < minegam) egamma = minegam;
    else                 egamma = maxegam;
    ppp->ee = egamma;
  }

  npts++;
  if(linetag == -100)
    return -npts;
  else
    return  npts;

errout:
  errexit("Error reading event in readgammaG");
  return 0;

}

int
buildgamma(point *ppt, int gamlen)
{
  int     ii, nbs, ng, ifirst;
  double  egam, etrue, eseen, val, cthmin;
  double th, ph;
  point  *ppu, *ppo;

  oegam[gamnum] = egam = etrue = fabs(ppt->ee);

/////////////////////////////////////////////////
// processing of emission point of this object //
/////////////////////////////////////////////////
  if(ppt->nd == -1) {                       // fixed origin is not written in the data but stored in origin
    ppt->xx = origin.xx;
    ppt->yy = origin.yy;
    ppt->zz = origin.zz;
    ppt->rr = origin.rr;
  }
  val = ppt->cx*ppt->cx + ppt->cy*ppt->cy + ppt->cz*ppt->cz; // rinormalizza i coseni direttori
  if(val && val < 1.e20) {
    val = 1./sqrt(val);
    ppt->cx *= val;
    ppt->cy *= val;
    ppt->cz *= val;
    th = RAD2DEG*acos(ppt->cz);             // statistica direzione di emissione
    ph = RAD2DEG*atan2(ppt->cy, ppt->cx);
    if(ph < 0) ph += 360.;
    specIncr((int)th + 5000, 6, 0);
    specIncr((int)ph + 5500, 6, 0);
  }
  else {
    ppt->cx = ppt->cy = ppt->cz = 0;
  }
  ppt->ng = gamnum;
  ng = peaklut[(int)(egam)] - 1;            // peaklut is marked + 1
  ppt->chid[0] = 'A' + gamnum;
  ppt->chid[2] = 0;
  if(recoil.beta) {
    ocdir[gamnum] = recoil.cx*ppt->cx + recoil.cy*ppt->cy + recoil.cz*ppt->cz;
    etrue *= recoil.gamma * (1. - recoil.beta * ocdir[gamnum]);
    ng = peaklut[(int)(etrue)] - 1;         // peaklut is marked + 1
  }
  if(ppt->nd == -1)
    specIncrGain(etrue, 0, 0);              /// lo spettro di emissione dei gamma (corretto doppler)

/////////////////////////////////////////////////////
// processing of interaction points of this object //
/////////////////////////////////////////////////////
  ppo =   ppt;    // the emission point
  ppu = ++ppt;    // the first interaction point
  nbs = 0;
  eseen = 0.;
  ifirst = -1;
  cthmin = -1.;
  for(ii = 1; ii < gamlen; ii++) {
    setcosdir(ppt);
    findsegment(ppt);
    if(ppt->seg < nsegMin || ppt->seg > nsegMax
#if SEGBAD == 1
      || segbad[ppt->seg]
#endif
    ) {
      nbs++;                // marked for removal
      ppt->ee = 0.;
    }

    eseen += ppt->ee;
    ppt->ng = gamnum;
    ppt->chid[0] = 'A' + gamnum;
    if(ii < 31) 
      ppt->chid[1] = 'a' + ii - 1;    // the first 30 points labeled as a...z<|>~
    else if (ii < 60)
      ppt->chid[1] = ' ' + ii - 30;   // the next  30 points labeled as 0...9
    else
      ppt->chid[1] = '-';             // all others are labeled as "-"
    ppt->chid[2] = 0;
    if(GEOMETRY) {
      if(!insidegedet(ppt)) {
        noutside1++;
//      printf("1 %8.2f %8.2f %8.2f %2d %d\n", ppt->xx, ppt->yy, ppt->zz, ppt->ns, evnum);
      }
#if STATINSIDEDET == 1
      statinsidedet(ppt);
#endif
    }
    val = ppt->cx*ppo->cx + ppt->cy*ppo->cy + ppt->cz*ppo->cz;
    if(ifirst == -1 || val > cthmin) {
      ifirst = ii;
      cthmin = val;
    }
    ppt++;
  }
  ohit1[gamnum] = ifirst;

#if SEGBAD == 1
  if(nbs) {                         // ci sono dei punti da eliminare
    ppt = ppu;
    for(ii = 1; ii < gamlen; ii++, ppt++) {
      if(ppt->ee > 0.) {
        if(ppt != ppu) {
          memcpy(ppu, ppt, sizeof(point));
          ppu++;
        }
      }
    }
    nbadpoints += nbs;
    gamlen     -= nbs;
  }
#endif

  specIncr(ng, 15, 0);              // statistica della cascata in emissione
  if(eseen > MINEGAM) {
    if(fabs(eseen-egam) < 5)        // bisognerebbe fare una verifica migliore
      specIncr(ng + 100, 15, 0);    // statistica della cascata rivelata sul picco
    else
      specIncr(ng + 200, 15, 0);    // statistica della cascata rivelata sul fondo
  }

  ongam[gamnum] = gamlen;
  oener[gamnum] = eseen;
  return gamlen;

}

int
counthitdets()
{
  int ngtot, ndtot, gamlen, ii;
  point  *ppt;

  ngtot = 0;
  ndtot = 0;
  memset(nevdets, 0, ngedets*sizeof(int));
  memset(enedets, 0, ngedets*sizeof(double));
  for (gamnum = 0; gamnum < evmult; gamnum++) {
    gamlen = ongam[gamnum];
    if(gamlen > 1) {
      ngtot++;
      ppt = obuf + oigam[gamnum];
      ppt++;
      for(ii = 1; ii < gamlen; ii++, ppt++) {
        nevdets[ppt->nd]++;
        enedets[ppt->nd] += ppt->ee;
      }
    }
  }
  for(ii = 0; ii < ngedets; ii++) {
    if(nevdets[ii] && enedets[ii] > eminD)
      ndtot++;;
  }
  return ndtot;
}

double
openingAngle(point *pp, int nn)
{
  point  *ipp, *jpp;
  int ii, jj;
  double  xx, cmin, angmax;

  cmin = 1.;
  for (ii = 0; ii < nn-1; ii++) {
    ipp = pp+ii;
    for (jj = ii+1; jj < nn; jj++) {
      jpp = pp+jj;
      xx = ipp->cx*jpp->cx + ipp->cy*jpp->cy + ipp->cz*jpp->cz;
      cmin = MIN(cmin, xx);
    }             
  }
  angmax = RAD2DEG * acos(cmin);
  return angmax;
}

double
openingDistance(point *pp, int nn)
{
  point  *ipp, *jpp;
  int ii, jj;
  double  xx, ddmax;

  ddmax = 0.;
  for (ii = 0; ii < nn-1; ii++) {
    ipp = pp+ii;
    for (jj = ii+1; jj < nn; jj++) {
      jpp = pp+jj;
      xx = pow(ipp->xx-jpp->xx, 2.) + pow(ipp->yy-jpp->yy, 2.) + pow(ipp->zz-jpp->zz, 2.);
      ddmax = MAX(ddmax, xx);
    }             
  }
  ddmax = sqrt(ddmax);
  return ddmax;
}

void
preprgammas()
{
  int     nn;
  int     gamlen, iegam;
  double  eseen, cdopp;
  point  *ppt, **lppgam;

  for (gamnum = 0; gamnum < evmult; gamnum++) {
    gamlen = ongam[gamnum] -1;                            // escluso punto di emissione
    if(gamlen < 1) {npgam[gamnum] = 0; continue;}         // no data points
    ppt    = pbuf + oigam[gamnum] +1;                     // punta al primo punto di interazione del gamma
    lppgam = ppgam + nppgam;                              // puntatore tmp ai puntatori dei punti preprocessati
    for(nn = 0; nn < gamlen; nn++) *lppgam++ = ppt++;     // copy pointers to the original gamma-points
    lppgam = ppgam + nppgam;
    if(llist > 1) printgamoriginal(lppgam, gamlen, gamnum);

//////////////////////////////////////////////////////////////////
//// packing for individual gammas to speed-up global packing ////
//////////////////////////////////////////////////////////////////

//  statcross(lppgam, gamlen, 0);         // statistics before packing

    gamlen = distpack(lppgam, gamlen);    // packing of points of gamma

//  statcross(lppgam, gamlen, 1);         // statistics after packing

#if SEGPACK == 1                          // pack together points in the same segment
    gamlen   = segpack(lppgam, gamlen);
#endif

#if SEGCENTER==1 && SEGPACK==1
  for(nn = 0; nn < gamlen; nn++) setsegcenter(lppgam[nn]);
#endif

  eseen = 0.;
    for (nn = 0; nn < gamlen; nn++) eseen += lppgam[nn]->ee;
    epgam[gamnum] = eseen;
    pgamlenmax  = MAX(gamlen, pgamlenmax);
    ipgam[gamnum] = nppgam;
    npgam[gamnum] = gamlen;
    nppgam += gamlen;
  }

//////////////////////////////////////////////////////////////
///////// analysis of the preprocessed gammas ////////////////
//////////////////////////////////////////////////////////////

  for (gamnum = 0; gamnum < evmult; gamnum++) {
    gamlen = npgam[gamnum];
    if(!gamlen) continue;
    lppgam  = ppgam + ipgam[gamnum];
    eseen  = epgam[gamnum];
    specIncrGain(eseen, 8, 0);                  //////// 8 = spettro dei gamma preprocessati, non corretto doppler
    if(recoil.beta) {
      cdopp  = recoil.gamma * (1. - recoil.beta * ocdir[gamnum]);
      eseen  *= cdopp;
      iegam  = (int)(oegam[gamnum] * cdopp);
    }
    else {
      iegam  = (int)oegam[gamnum];
    }
    specIncrGain(eseen, 2, 0);                  //////// 2 = spettro dei gamma preprocessati, corretto doppler
  }

}

void
preprevent()
{
  int nn;
  point *pp;

  if(!npev) return;

////////////////////////////////////////////////////////////
//////// packing, smearing, centering at event level ///////
////////////////////////////////////////////////////////////

  nn = distpack(ppev, npev);            // re-pack points at segment level
  nmixed += npev - nn;                  // count points if mixed gamma
  npev = nn;
#if SEGPACK == 1
  nn = segpack(ppev, npev);             // pack points in segment at event level
  nmixed += npev - nn;                  // count points if mixed gamma               
  npev = nn;
# endif
  if(disterr > 0.) {                    // apply position smearing
    smearpos(ppev, npev);
    nn = distpack(ppev, npev);          // repack points after smearing
    npev = nn;
  }
  smearener(ppev, npev);                // now smear the energies
# if ESMEARSEG == 1                     // extra energy-smearing in segments
  smeareseg(ppev, npev);
# endif
  npev = checkemin(ppev, npev);         // and apply energy threshold

#if SEGCENTER==1 && SEGPACK==1
  for(nn = 0; nn < npev; nn++) setsegcenter(ppev[nn]);
#endif

//statVtx(ppev, npev);                  // this is here to apply it avoiding tracking (move it to acceptcluster??)

  if(check_xyz)                         // to keep only a part (box) of the detector
    npev = checkxyz(ppev, npev);

#if ORDERPOINTS == 1
  orderpoints(ppev, npev);
#endif

  for(nn = 0; nn < npev; nn++) {
    pp = ppev[nn];
    pp->ind = nn;     // needed by the reconstruction part
    pp->clu = -1;     //       "
    specIncr((int)(pp->xx /*+ DEL(pp->xx)*/) + 1000, 6, 0); // distribuzione spaziale dei punti preprocessati
    specIncr((int)(pp->yy /*+ DEL(pp->yy)*/) + 2000, 6, 0); // con +DEL() canali centrati a i+-0.5
    specIncr((int)(pp->zz /*+ DEL(pp->zz)*/) + 3000, 6, 0);
    specIncr((int)(pp->rr /*+ DEL(pp->rr)*/) + 4000, 6, 0);
  }  

}

int
checkEgam(int ig, double ee)
{
  int    ii = (int)ee;
  int    tt = 0;
  double pw;

  if(ii < MAXEKEV) {
    pw = peakwidth[ii];
    if(fabs(oegam[ig] - ee) < pw)
      tt = 3;
    else if(fabs(oener[ig] - ee) < pw)
      tt = 1;
  }
  return tt;
  
}

// packs points in a "link" mode
int
distpackL(point **pp, int nn)
{
  int     ii, jj, kk, ll, nd, skip, packed, nplink;
  point  *ipp, *jpp;
  double  dist2, d2min;
  double  xx, yy, zz;

  if(!distmin) return nn;
  if(nn < 2) return nn;
  d2min = distmin * distmin;
  packed = 0; 
  for (ii = 0; ii < nn; ii++) {         // for each (surviving) point
    if(pp[ii] == NULL) continue;        // point already packed
    plink[0] = pp[ii];                  // start filling the list
    nplink = 1;
    nd = plink[0]->nd;                  // all of them refer to this detector
    for(kk = 0; kk < nplink; kk++) {    // for all points in the plink list
      ipp = plink[kk];
      for (jj = 0; jj < nn; jj++) {     // find all neighbours of ipp
        jpp = pp[jj];
        if(jpp == NULL) continue;       // already packed;
        if(jpp == ipp)  continue;
        skip = 0;
        for(ll = 0; ll < nplink; ll++) {
          if(jpp == plink[ll]) {
            skip = 1;
            break;
          }
        }
        if(skip) continue;
        if(jpp->nd != nd) continue;
        if(fabs(jpp->rr - ipp->rr) > distmin) continue;   // don't need to calculate distance
        xx = jpp->xx - ipp->xx;
        dist2 = xx*xx;
        if(dist2 > d2min) continue;
        yy = jpp->yy - ipp->yy;
        dist2 += yy*yy;
        if(dist2 > d2min) continue;
        zz = jpp->zz - ipp->zz;
        dist2 += zz*zz;
        if(dist2 < d2min) {
          plink[nplink++] = jpp;          // add to plink collection
          pp[jj] = NULL;                  // mark it as packed
        }
      }
    }
    if(nplink > 1) {                      // merge the points
      mergeNpoints(plink, nplink);
      packed = 1;
    }
  }
  
  if(packed) {
    jj = 0;
    for(ii = 0; ii < nn; ii++) {
      if(pp[ii] == NULL) continue;
      if(ii != jj) pp[jj] = pp[ii];
      jj++;
    }
  }
  else {
    jj = nn;
  }
  return jj;
  
}

// packs points pairwise
int
distpackP(point **pp, int nn)
{
  int     ii, jj, np, retry;
  point  *ipp, *jpp;
  double  dist2, d2min;
  double xx, yy, zz;

  if(!distmin) return nn;
  if(nn < 2) return nn;

  d2min = distmin * distmin;
  np = 0;
  
  retry = 1;
  while(retry) {
    retry = 0;
    for (ii = 0; ii < nn - 1; ii++) {
      ipp = pp[ii];
      if(ipp == NULL) continue;
      for (jj = ii + 1; jj < nn; jj++) {
        jpp = pp[jj];
        if(jpp == NULL) continue;
        if(jpp->nd != ipp->nd) continue;
        if(fabs(jpp->rr - ipp->rr) > distmin) continue;   // don't need to calculate distance
        xx = jpp->xx - ipp->xx;
        dist2 = xx*xx;
        if(dist2 > d2min) continue;
        yy = jpp->yy - ipp->yy;
        dist2 += yy*yy;
        if(dist2 > d2min) continue;
        zz = jpp->zz - ipp->zz;
        dist2 += zz*zz;
        if(dist2 < d2min) {
          merge2points(ipp, jpp);
          pp[jj] = NULL;
          np++;
          retry = 1;
        }
      }
    }
  }

  if(np) {
    jj = 0;
    for(ii = 0; ii < nn; ii++) {
      if(pp[ii] == NULL) continue;
      if(ii != jj) pp[jj] = pp[ii];
      jj++;
    }    
  }
  return jj;
  
}

int
segpack(point **pp, int nn)
{
  int     ii, jj, np;
  point  *ipp, *jpp;
  
  if(nn < 2) return nn;
    
  np = 0;
  for (ii = 0; ii < nn - 1 ; ii++) {
    ipp = pp[ii];
    if(ipp == NULL) continue;
    for (jj = ii + 1; jj < nn ; jj++) {
      jpp = pp[jj];
      if(jpp == NULL) continue;
      if(jpp->nd != ipp->nd) continue;
      if(jpp->ns != ipp->ns) continue;
      merge2points(ipp, jpp);
      pp[jj] = NULL;
      np++;
    }
  }

  if(np) {
    jj = 0;
    for(ii = 0; ii < nn; ii++) {
      if(pp[ii] == NULL) continue;
      if(ii != jj) pp[jj] = pp[ii];
      jj++;
    }    
  }
  return jj;

}

void
merge2points(point *p1, point *p2)
{
  double e1, e2, e3;
  
  e1 = p1->ee;
  e2 = p2->ee;
  e3 = e1 + e2;
  p1->ee  = e3;
  p1->xx  = (p1->xx*e1 + p2->xx*e2) / e3;
  p1->yy  = (p1->yy*e1 + p2->yy*e2) / e3;
  p1->zz  = (p1->zz*e1 + p2->zz*e2) / e3;
  setcosdir(p1);
  // check to avoid overflow in chid
  if(strlen(p1->chid)+strlen(p2->chid) < 64)
    strcat(p1->chid, p2->chid);
  else {
    memset(p1->chid, '=', 63);
    p1->chid[64] = 0;
  }
  findsegment(p1);
}

void
mergeNpoints(point **pp, int nn)
{
  int ii;
  double xx, yy, zz, ee, e2;
  point *p1, *p2;

  ee = xx = yy = zz = 0.;
  p1 = pp[0];
  for(ii = 0; ii < nn; ii++) {
    p2 = pp[ii];
    e2 = p2->ee;
    ee += e2;
    xx += e2*p2->xx;
    yy += e2*p2->yy;
    zz += e2*p2->zz;
    if(ii) {
      // check to avoid overflow in chid
      if(strlen(p1->chid)+strlen(p2->chid) < 64)
        strcat(p1->chid, p2->chid);
      else {
        memset(p1->chid, '=', 63);
        p1->chid[64] = 0;
      }
    }
  }
  p1->ee  = ee;
  p1->xx = xx/ee;
  p1->yy = yy/ee;
  p1->zz = zz/ee;
  setcosdir(p1);
  findsegment(p1);
}

void
orderpoints(point **pp, int nn)
{
  int   i1, i2, again;
  point *tp;
  
  if(nn < 2) return;

  again = 1;
  i2    = nn;
  while(again) {
    again = 0;
    i2--;
    for(i1 = 0; i1 < i2; i1++) {
      if(pp[i1]->ee < pp[i1+1]->ee) {
        SWAP(pp[i1],pp[i1+1],tp)
        again = 1;
      }
    }
  }
}

int
checkemin(point **pp, int nn)
{
  int ii, jj;

  if(ngedets > 1 && eminD > eminP) {
    memset(enedets, 0, ngedets*sizeof(double));
    for (ii = 0; ii < nn; ii++) {
      enedets[pp[ii]->nd] += pp[ii]->ee;
    }
    for (ii = jj = 0; ii < nn; ii++) {
      if((enedets[pp[ii]->nd] > eminD) && (pp[ii]->ee > eminP))
        pp[jj++] = pp[ii];
    }
  }
  else {
    for (ii = jj = 0; ii < nn; ii++) {
      if(pp[ii]->ee > eminP)
        pp[jj++] = pp[ii];
    }
  }
  return jj;
}

int
checkxyz(point **pp, int nn)
{
  int ii, jj;
  
  for (ii = jj = 0; ii < nn; ii++) {
    if( pp[ii]->xx >= xx_min && pp[ii]->xx <= xx_max &&
        pp[ii]->yy >= yy_min && pp[ii]->yy <= yy_max &&
        pp[ii]->zz >= zz_min && pp[ii]->zz <= zz_max    )
      pp[jj++] = pp[ii];
  }
  return jj;
}

void
smearener(point **pp, int nn)
{
  point *ipp;
  int ii;

  if(fwhm >=0.) {
    for (ii = 0; ii < nn; ii++) {
      ipp = pp[ii];
      ipp->ee = gaussres(ipp->ee);
    }
  }
}

#if ESMEARSEG == 1

void
smeareseg(point **pp, int nn)
{
  static int initit = 1;
  double eseg, fract1, fract2, delta;
  int    ii, jj, kk, iseg, nseg;
  point *ipp, *jpp, *kpp;
  
  if(initit) {
    seghits = (int *)calloc(nsegtot, sizeof(int));
    initit  = 0;
  }
  else {
    memset(seghits, 0, sizeof(int)    * nsegtot);
  }

  for(ii = 0; ii < nn; ii++) seghits[pp[ii]->seg]++;

  for (ii = 0; ii < nn; ii++)  {
    ipp  = pp[ii];
    iseg = ipp->seg;
    nseg = seghits[iseg];
    if(nseg > 1) {
      kpp = ipp;
      kk = nseg - 1;
      for(jj = ii+1; jj < nn; jj++) {
        jpp = pp[jj];
        if(jpp->seg == iseg) {
          eseg = kpp->ee + jpp->ee;
          fract1 = kpp->ee / eseg;
          fract2 = jpp->ee / eseg;
          delta = fract1 * fract2 * RAND();
          if(RAND() < 0.5) delta = -delta;
//        matIncr((int)(1000.*fract1), (int)(1000.*(fract1+delta)), 0);
//        matIncr((int)(1000.*fract2), (int)(1000.*(fract2-delta)), 0); 
          fract1 += delta;
          fract2 -= delta;
          kpp->ee = eseg * fract1;      // per non disturbare i gamma preprocessati individuali, queste modifiche
          jpp->ee = eseg * fract2;      // andrebbero riportate su punti aggiunti in coda a pbuf (vedi distpack)
          kpp = jpp;
          if(--kk == 0) break;
        }
      }
      seghits[iseg] = 0;
    }
  }
}

#endif // #if ESMEARSEG == 1

void
smearpos(point **pp, int nn)
{
  point  *ipp;
  int     ii, id16;
  double  xx, yy, zz;
  double  dx, dy, dz, dd;
#if SMEARPOS == GAUSS2 || SMEARPOS == GAUSS3 || SMEARPOS == GAUSS4
  double  rr, th, ph, xy;
#endif
  
  for (ii = 0; ii < nn; ii++) {
    ipp = pp[ii];
    if(!insidegedet(ipp)) {
      noutside2++;
//    printf("2 %8.2f %8.2f %8.2f %2d %d\n", ipp->xx, ipp->yy, ipp->zz, ipp->ns, evnum);
      continue;
    }
    xx = ipp->xx;
    yy = ipp->yy;
    zz = ipp->zz;
    dd = poserr[(int)ipp->ee];
    ipp->s2 = dd*dd;                      // salva la sigma2
    do {
#if   SMEARPOS == CUBE                    // uniformemente in un cubo di semilato disterr
      dx = dd * (0.5 - RAND());
      dy = dd * (0.5 - RAND());
      dz = dd * (0.5 - RAND());
#elif SMEARPOS == SPHE                    // uniformemente in una sfera di raggio disterr
      do {                                // produce una distribuzione di distanze r^3
        dx = 0.5 - RAND();
        dy = 0.5 - RAND();
        dz = 0.5 - RAND();
      } while ((dx*dx + dy*dy + dz*dz) > 0.25);
      dx *= dd;
      dy *= dd;
      dz *= dd;
#elif  SMEARPOS == GAUSS1                 // 3 gaussiane indipedenti di fwhm disterr
      dx = dd * gaussrand();
      dy = dd * gaussrand();
      dz = dd * gaussrand();
#elif  SMEARPOS == GAUSS2
      th = acos(1.0 - 2.0*RAND());        // direzione casuale
      ph = (2.*M_PI) * RAND();            // 
      rr = dd * gaussrand();              // e distanza dal punto distribuita gaussianamente con whm = dd
      xy = rr * sin(th);                  // non e' necessario ABS(rr) perche' rr<0 cambia solo i segni
      dx = xy * cos(ph);
      dy = xy * sin(ph);
      dz = rr * cos(th);
#elif  SMEARPOS == GAUSS3
      th = acos(1.0 - 2.0*RAND());        // direzione casuale
      ph = (2.*M_PI) * RAND();            //
      rr = dd * RAND();                   // e distanza dal punto distribuita uniformemente in     0...disterr
      xy = rr * sin(th);
      dx = xy * cos(ph);
      dy = xy * sin(ph);
      dz = rr * cos(th);
#elif  SMEARPOS == GAUSS4
      th = acos(1.0 - 2.0*RAND());        // direzione casuale
      ph = (2.*M_PI) * RAND();            // 
      do {                                // e distanza che segue secondo la funzione nella while()
        rr = RAND();                      //
        xy = RAND();                      //
      } while (xy > pow(rr, 1.5));        //
//    } while (xy > 0.8*(0.25+rr));       //
      rr *= dd;                           //
      xy = rr * sin(th);
      dx = xy * cos(ph);
      dy = xy * sin(ph);
      dz = rr * cos(th);
#endif
      ipp->xx = xx + dx;
      ipp->yy = yy + dy;
      ipp->zz = zz + dz;
      setcosdir(ipp);
    } while(!insidegedet(ipp));
    findsegment(ipp);
    specIncr((int)dx + 2000, 16, 0);    // x smearing
    specIncr((int)dy + 3000, 16, 0);    // y smearing
    specIncr((int)dz + 4000, 16, 0);    // z smearing
    dx = xx-ipp->xx;
    dy = yy-ipp->yy;
    dz = zz-ipp->zz;
    dd = sqrt(dx*dx + dy*dy + dz*dz);
    id16 = (int)(100. * dd);
    idmax16 = MAX(idmax16, id16);
    specIncr(id16, 16, 0);                        // distanza tra punto originale e punto smeared
//  matIncr((int)(ipp->ee/10)+10, (int)(dd* 10) + 10, 0);  // matrice Epoint/10 -- Dist(mm)
//  matIncr((int)(ipp->ee/10),    (int)(dd*100)     , 0);  // matrice Epoint/10 -- Dist(mm/10)
  }
}

void
stathitdets(point **pp, int nn)
{
  int ii, jj, kk;
  
  if(ngedets > 1) {
    memset(nevdets, 0, ngedets*sizeof(int));
    for (ii = 0; ii < nn; ii++) {
      nevdets[pp[ii]->nd]++;
    }
    jj = 0;
    for (ii = 0; ii < ngedets; ii++) {
      if(kk = nevdets[ii]) {
        specIncr(ii + 2000, 17, 0);              // distribuzione dei rivelatori colpiti
        specIncr(kk + 4400, 15, 0);              // numero di punti per rivelatore
        jj++;
      }
    }
  }
  else {
    specIncr(1  + 2000, 17, 0);               // distribuzione dei rivelatori colpiti
    specIncr(nn + 4400, 15, 0);               // numero di punti per rivelatore
    jj = 1;
  }
  specIncr(jj + 1000, 15, 0);                 // numero di rivelatori nell'evento
  specIncr(jj + 4000, 15, 0);                 // numero di rivelatori nell'evento
}

void
stathitsegs(point **pp, int nn)
{
  int ii, jj, kk;

  memset(nevsegs, 0, nsegtot*sizeof(int));
  for (ii = 0; ii < nn; ii++) {
    nevsegs[pp[ii]->seg]++;
  }
  jj = 0;
  for (ii = 0; ii < nsegtot; ii++) {
    if(kk = nevsegs[ii]) {
      specIncr(ii,        18, 0);             // distribuzione dei segmenti colpiti
      specIncr(kk + 4600, 15, 0);             // numero di punti per segmento
      jj++;
    }
  }
  specIncr(jj + 4200, 15, 0);                 // numero di segmenti nell'evento

  for (ii = 0; ii < nsegtot; ii += nsegdet) {
    kk = 0;
    for(jj = ii; jj < ii+nsegdet; jj++) {
      if(nevsegs[jj])
        kk++;
    }
    if(kk) specIncr(kk + 4800, 15, 0);        // numero di segmenti per rivelatore
  }

}

void
printgamoriginal(point **pp, int nn, int ng)
{
  int     ii, jj;
  point  *ipp, *jpp;
  double  et;
  double  dd, th, ddm, thm;
  
  printf("\n");
  ipp = getptogam(ng);
  printf("%10.3f %8.2f %8.2f %8.2f %9.3f %7.2f %7.2f      %c\n",
    ipp->ee, ipp->xx, ipp->yy, ipp->zz, ipp->rr, DC2TH(ipp), DC2PH(ipp), ipp->chid[0]);
  if (!nn) return;
  
  ipp = pp[0];
  et  = 0.;
  for (ii = 0; ii < nn; ii++, ipp++) {
    printf("%10.3f %8.2f %8.2f %8.2f %9.3f %7.2f %7.2f %3d  ",
      ipp->ee, ipp->xx, ipp->yy, ipp->zz, ipp->rr, DC2TH(ipp), DC2PH(ipp), ipp->nd);
    printchid(ipp->chid);
    printf("\n");
    et += ipp->ee;
  }

  ipp = pp[0];
  ddm = thm = 0.0;
  for (ii = 0; ii < nn - 1; ii++, ipp++) {
    jpp = ipp + 1;
    for (jj = ii + 1; jj < nn; jj++, jpp++) {
      th = RAD2DEG * acos(ipp->cx*jpp->cx + ipp->cy*jpp->cy + ipp->cz*jpp->cz);
      dd = sqrt(pow(jpp->xx - ipp->xx, 2.) + pow(jpp->yy - ipp->yy, 2.) + pow(jpp->zz - ipp->zz, 2.));
      thm = MAX(thm, th);
      ddm = MAX(ddm, dd);
    }
  }
  printf("%10.3f %8.2fd%8.3ft\n", et, ddm, thm);
}

void
writeheader()
{
  char line[256];
  int lline, ii;

  if (ofp == NULL) {
    if( (ofp = fopen(ofname, "w")) == NULL) {
      printf("Error opening %s\n", ofname);
      errexit("writeheader");
    }
  }
  
  if(ifp == NULL) {
    if( (ifp = fopen(ifname, "r")) == NULL) {
      printf("Error opening %s\n", ifname);
      errexit("writeheader");
    }
  }
  else {
    fseek(ifp, 0, SEEK_SET);
  }

  line[255] = 0;
  while(fgets(line, 255, ifp) != NULL) {
    lline = strlen(line);
    ii = 0;
    while(ii < lline && isspace(line[ii]))    // trim leading blanks
      ii++;
    if(line[ii] == '$') {
      if( doWriteEvs ) {
	fprintf(ofp,"PREPROCESSED\n");
	writeparams();
	fprintf(ofp,"ENDPREPROCESSED\n");
      }
      if( doPrismaEvs ) {
	fprintf(ofp,"TRACKED\n");
	writeparams();
	fprintf(ofp,"ENDTRACKED\n");
      }
      fprintf(ofp, "$\n");
      return;
    }
    fprintf(ofp, "%s",line+ii);
  }
}

void
writeparams()
{
  fprintf(ofp, "IFILE %s\n", ifname);
  fprintf(ofp, "OFILE %s\n", ofname);
  fprintf(ofp, "HEADERS %d\n", outputType);
  fprintf(ofp, "MULTIPLICITY %d %d %d\n", mult, varmult, 0);
  fprintf(ofp, "NEVENTS %d\n", nevents);
  fprintf(ofp, "SEGMENTATION %s\n", segmentstring);
#if SEGPACK == 1
  fprintf(ofp, "SEGPACKING\n");
#else
  fprintf(ofp, "PACKING  %f\n", distmin);
#endif
#if SEGCENTER==1 && SEGPACK==1
  fprintf(ofp, "SEGCENTERING\n");
#else
  fprintf(ofp, "SMEARING %f %d\n", disterr, SMEARPOS);
#endif
  fprintf(ofp, "ETHRESHOLD %f %f\n", eminP, eminD);
  fprintf(ofp, "ERESOLUTION %f %f\n", fwhm, fwhn);
}

void
writeevent(point **pp, int nn)
{
  int     ii, jj, ll;
  point  *p1;
  double  et;
  char    ch;
  
  if (ofp == NULL) {
    if( (ofp = fopen(ofname, "w")) == NULL) {
      printf("Error opening %s\n", ofname);
      errexit("writeevent");
    }
    fprintf(ofp, "$\n");
  }
  
  et = 0.;
  for (ii = 0; ii < nn; ii++) {
    et += pp[ii]->ee;
  }
  
  if(dataFileVersion == FMARS) {
    fprintf(ofp, "%9.3f %3d %3d %3d -1\n", -et, evmult, (outputType) ? (evmult) : 0, nn);
    if(outputType) {
      for (ii = 0; ii < evmult; ii++) {
        p1 = getptogam(ii);
        ll = (outputType>1) ? (ongam[ii]-1) : (0);
        fprintf(ofp, "%9.3f %8.5f %8.5f %8.5f %s%c %9.3f %d\n",
          p1->ee, p1->cx, p1->cy, p1->cz, p1->chid, 'a' + ohit1[ii]-1, oener[ii], ll);
        p1++;
        for(jj = 0; jj < ll; jj++, p1++) {
          fprintf(ofp, "%9.3f %7.2f %7.2f %7.2f %3d %s\n",
            p1->ee, p1->xx, p1->yy, p1->zz, p1->nd, p1->chid+1);
        }
      }
    }
    
    for (ii = 0; ii < nn; ii++) {
      p1 = pp[ii];
      fprintf(ofp, "%9.3f %7.2f %7.2f %7.2f %3d ",
        p1->ee, p1->xx, p1->yy, p1->zz, p1->nd);
      ll = strlen(p1->chid);
      ch = 0;
      for (jj = 0; jj < ll; jj++) {
        if(p1->chid[jj] != ch) {
          ch = p1->chid[jj];
          fprintf(ofp, "%c", ch);
        }
        fprintf(ofp, "%c", p1->chid[++jj]);
      }
      fprintf(ofp, "\n");
    }
  }
  else if(dataFileVersion == FAGATA) {
    if( evgen )
      fprintf( ofp, "-100\n" );
    fprintf(ofp, "%4d %9.3f %3d %3d %3d  %d\n", -1, et, evmult, (outputType) ? (evmult) : 0, nn, evnum);
    if(outputType) {
      for (ii = 0; ii < evmult; ii++) {
        p1 = getptogam(ii);
        ll = (outputType>1) ? (ongam[ii]-1) : (0);
        fprintf(ofp, "%4d %9.3f %8.5f %8.5f %8.5f %s%c %8.3f\n",
                ll, p1->ee, p1->cx, p1->cy, p1->cz, p1->chid, 'a' + ohit1[ii]-1, oener[ii]);
        p1++;
        for(jj = 0; jj < ll; jj++, p1++) {
          fprintf(ofp, "%4d %9.3f %7.2f %7.2f %7.2f %2d %s\n",
                  p1->nd, p1->ee, p1->xx, p1->yy, p1->zz, p1->ss, p1->chid+1);
        }
      }
    }
    
    for (ii = 0; ii < nn; ii++) {
      p1 = pp[ii];
      fprintf(ofp, "%4d %9.3f %7.2f %7.2f %7.2f %2d ",
              p1->nd, p1->ee, p1->xx, p1->yy, p1->zz, p1->ss);
      if(outputType) {
        ll = strlen(p1->chid);
        ch = 0;
        for (jj = 0; jj < ll; jj++) {
          if(p1->chid[jj] != ch) {
            ch = p1->chid[jj];
            fprintf(ofp, "%c", ch);
          }
          fprintf(ofp, "%c", p1->chid[++jj]);
        }
      }
      fprintf(ofp, "\n");
    }
  }
}

//makes sense only for m=1
void
statcross(point **pp, int nn, int ll)
{

  static int initialize = 1, firstmat = 0;
  double ee, dd;
  int    ii, good, base;

  if(initialize) {
    firstmat = matHowMany();
    matDefine(MATSIZE, MATSIZE, "matPa.dat", "Egan-Dist Phot. original");
    matDefine(MATSIZE, MATSIZE, "matCa.dat", "Egan-Dist Comp. original");
    matDefine(MATSIZE, MATSIZE, "matPb.dat", "Egan-Dist Phot. packed");
    matDefine(MATSIZE, MATSIZE, "matCb.dat", "Egan-Dist Comp. packed");
    initialize = 0;
  }

  base = firstmat;
  if(ll) base += 2;

  for(ee = 0., ii = 0; ii < nn; ii++) ee += pp[ii]->ee;
  good    = peaklut[(int)ee];
  dd = dist3Dp(pp[0], &origin);
  
  if( good && (nn == 1) )
    matIncr( (int)(ee/10.), (int)(dd*10.), 0+base);
  else
    matIncr( (int)(ee/10.), (int)(dd*10.), 1+base);
    
  for(ii = 1; ii < nn; ii++) {
    ee -= pp[ii-1]->ee;
//  dd  = gedistance(pp[ii-1], pp[ii]);
    dd  = dist3Dp(pp[ii-1], pp[ii]);
    if( good && (ii == nn-1) )
      matIncr( (int)(ee/10.), (int)(dd*10.), 0+base);
    else
      matIncr( (int)(ee/10.), (int)(dd*10.), 1+base);
  }
}
