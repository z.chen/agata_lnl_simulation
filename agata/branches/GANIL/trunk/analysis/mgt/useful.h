#ifndef MY_USEFUL_INCLUDED
#define MY_USEFUL_INCLUDED

#include <math.h>

////////////////////////////////////////////////////////////////////////////////////////
///////////////////// some useful macros and functions /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

#ifndef M_PI
# define M_PI       3.1415926535897932385E0
#endif
#ifndef M_SQRT2
# define M_SQRT2    1.4142135623730950488E0
#endif

#define DEG2RAD       (M_PI/180.)           // conversion degrees  -> radiants
#define RAD2DEG       (180./M_PI)           // conversion radiants -> degrees

#define RAD(d)        (d*M_PI/180.)         // conversion degrees  -> radiants
#define DEG(r)        (r*180./M_PI)         // conversion radiants -> degrees

#define MIN(a,b)      (((a)<(b))?(a):(b))
#define MAX(a,b)      (((a)>(b))?(a):(b))

#define DEL(a)        ((a<0)?(-0.5):(0.5))  // to get nearest integer with (int)
#define ABS(a)        (((a)>0)?(a):-(a))
#define SWAP(a,b,t)   {(t)=(a);(a)=(b);(b)=(t);}

#define M0C2          510.9989              // mass of electron in keV = 510.9989(2)
#define M0C2I         0.001956951           // 1/ mass of electron in keV
#define M0C2P        1021.9978              // twice   "
#define SIG2FW      (sqrt(8.*log(2.)))      // factor 2.355...  from sigma to fwhm for gaussian peaks

#define RAND()      (rand()/(RAND_MAX+1.0))

#endif // MY_USEFUL_INCLUDED

