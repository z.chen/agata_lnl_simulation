
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "vectorlib.h"


#ifndef M_PI
# define M_PI       3.1415926535897932385
#endif
#define DEG2RAD    (M_PI/180.)          // degrees  -> radiants
#define RAD2DEG    (180./M_PI)          // radiants -> degrees

#define MIN(a,b)      (((a)<(b))?(a):(b))
#define MAX(a,b)      (((a)>(b))?(a):(b))
#define ABS(a)        (((a)>0)?(a):-(a))
#define SWAP(a,b,t)   (t)=(a);(a)=(b);(b)=(t);
#define RAND()        (rand()/(RAND_MAX+1.0))


/////////////////////////////////////////////////////////////////////////////////
///////////////////// vector stuff //////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

double
dotprod(V3D* v1, V3D* v2)
{
  return v1->xx*v2->xx +v1->yy*v2->yy +v1->zz*v2->zz;
}

double
dotprodNorm(V3D* v1, V3D* v2)
{
  double xx;

  xx = modulo2(v1) * modulo2(v2);
  if(xx == 0.) return 0;
  xx = dotprod(v1, v2) / sqrt(xx);
  if(xx >  1.) xx =  1.;
  if(xx < -1.) xx = -1.;
  return xx;
}

double
acosdotprod(V3D* v1, V3D* v2)
{
  double xx;

  xx = modulo2(v1) * modulo2(v2);
  if(xx == 0.) return 0;
  xx = dotprod(v1, v2) / sqrt(xx);
  if(xx >  1.) xx =  1.;
  if(xx < -1.) xx = -1.;
  return acos(xx);
}

void
crossprod(V3D* v1, V3D* v2, V3D* prod)
{
  prod->xx = v1->yy * v2->zz - v1->zz * v2->yy;
  prod->yy = v1->zz * v2->xx - v1->xx * v2->zz;
  prod->zz = v1->xx * v2->yy - v1->yy * v2->xx;
}

void
normalize(V3D* v)
{
  double d;
  
  d = modulo2(v);
  if (d > 0.) {
    d = 1. / sqrt(d);
    v->xx *= d;
    v->yy *= d;
    v->zz *= d;
  }
  else {
    v->xx = 0.;
    v->yy = 0.;
    v->zz = 0.;
  }
}

double
modulo(V3D* v)
{
  return sqrt(v->xx*v->xx + v->yy*v->yy + v->zz*v->zz);
}

double
modulo2(V3D* v)
{
  return v->xx*v->xx + v->yy*v->yy + v->zz*v->zz;
}

void
cart2pol(V3D* cp, P3D* pp)
{
  double rr;
  double th = 0.;
  double ph = 0.;

  rr = modulo(cp);
  if(rr > 0.) {
    th = acos (cp->zz / rr);
    ph = atan2(cp->yy , cp->xx);
  }
  pp->rr = rr;
  pp->th = th;
  pp->ph = ph;

//pp->xx = modulo(cp);
//if(pp->xx > 0.) {
//  pp->yy = acos (cp->zz / pp->xx);
//  pp->zz = atan2(cp->yy , cp->xx);
//}
//else {
//  pp->yy = 0.;
//  pp->zz = 0.;
//}
}

void
pol2cart(P3D* pp, V3D* cp)
{
  cp->xx = pp->rr * sin(pp->th) * cos(pp->ph);
  cp->yy = pp->rr * sin(pp->th) * sin(pp->ph);
  cp->zz = pp->rr * cos(pp->th);
}


double
distance(V3D* a, V3D* b)
{
  return sqrt(pow(a->xx-b->xx,2.) + pow(a->yy-b->yy,2.) + pow(a->zz-b->zz,2.) );
}

double
distance2(V3D* a, V3D* b)
{
  return pow(a->xx-b->xx,2.) + pow(a->yy-b->yy,2.) + pow(a->zz-b->zz,2.);
}

double
point2line(V3D* p, V3D* a, V3D* b)
{
  V3D ab, ap;
  double acosth;

  sub3D(b, a, &ab);
  sub3D(p, a, &ap);
  acosth = acosdotprod(&ab, &ap);
  return distance(a, p)*sin(acosth);
}

double
point2plane(V3D* pp, V4D* vm)
{
  return dotprod((V3D *)vm, pp) + vm->tt;
}

void
swap3D(V3D* a, V3D* b)
{
  double dd;
  dd = a->xx; a->xx = b->xx; b->xx = dd;
  dd = a->yy; a->yy = b->yy; b->yy = dd;
  dd = a->zz; a->zz = b->zz; b->zz = dd;
}

void
copy3D(V3D* a, V3D* b)
{
  b->xx = a->xx;
  b->yy = a->yy;
  b->zz = a->zz;
}

void
add3D(V3D* a, V3D* b, V3D* c)
{
  c->xx = a->xx + b->xx;
  c->yy = a->yy + b->yy;
  c->zz = a->zz + b->zz;
}

void
sub3D(V3D* a, V3D* b, V3D* c)
{
  c->xx = a->xx - b->xx;
  c->yy = a->yy - b->yy;
  c->zz = a->zz - b->zz;
}

void
line3D(V3D* a, V3D* b, V3D* c)
{
  c->xx = b->xx - a->xx;
  c->yy = b->yy - a->yy;
  c->zz = b->zz - a->zz;
}

void
midpoint3D(V3D* a, V3D* b, V3D* c)
{
  c->xx = 0.5 * (a->xx + b->xx);
  c->yy = 0.5 * (a->yy + b->yy);
  c->zz = 0.5 * (a->zz + b->zz);
}

void
rot3D1x(double deg, V3D* v)
{
  double s, c;
  double y, z;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  y = v->yy;
  z = v->zz;

  v->yy = c*y - s*z;
  v->zz = s*y + c*z;
}

void
rot3D1y(double deg, V3D* v)
{
  double s, c;
  double x, z;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  x = v->xx;
  z = v->zz;

  v->zz = c*z - s*x;
  v->xx = s*z + c*x;

}

void
rot3D1z(double deg, V3D* v)
{
  double s, c;
  double x, y;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  x = v->xx;
  y = v->yy;

  v->xx = c*x - s*y;
  v->yy = s*x + c*y;
}

void
rot3D1d(double deg, V3D* d, V3D* v)
{
  double rr, th, ph;

  rr = modulo2(d);
  if(rr == 0.) return;

  th = RAD2DEG * acos(d->zz/sqrt(rr));
  ph = RAD2DEG * atan2(d->yy, d->xx);

  rot3D1z(-ph, v);
  rot3D1y(-th, v);
  rot3D1z(deg, v);
  rot3D1y( th, v);
  rot3D1z( ph, v);
}

void
rot3D1E(double eps, double eth, double eph, V3D* v)
{
  rot3D1z(eps, v);
  rot3D1y(eth, v);
  rot3D1z(eph, v);
}

void
rot3D1EE(Euler *eu, V3D* v)
{
  rot3D1z(eu->ps, v);
  rot3D1y(eu->th, v);
  rot3D1z(eu->ph, v);
}

void
rot3D1EEI(Euler *eu, V3D* v)
{
  rot3D1z(-eu->ph, v);
  rot3D1y(-eu->th, v);
  rot3D1z(-eu->ps, v);
}

void
rot3D2x(double deg, V3D* v, V3D* r)
{
  double s, c;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  r->xx =   v->xx;
  r->yy = c*v->yy - s*v->zz;
  r->zz = s*v->yy + c*v->zz;
}

void
rot3D2y(double deg, V3D* v, V3D* r)
{
  double s, c;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  r->yy =   v->yy;
  r->zz = c*v->zz - s*v->xx;
  r->xx = s*v->zz + c*v->xx;
}

void
rot3D2z(double deg, V3D* v, V3D* r)
{
  double s, c;

  c = cos(DEG2RAD*deg);
  s = sin(DEG2RAD*deg);

  r->zz =   v->zz;
  r->xx = c*v->xx - s*v->yy;
  r->yy = s*v->xx + c*v->yy;
}

void
rot3D2d(double deg, V3D* d, V3D* v, V3D* r)
{
  double rr, th, ph;

  rr = modulo2(d); 
  if(rr == 0.) {
    r->xx = v->xx; r->yy = v->yy; r->zz = v->zz;
    return;
  }

  th = RAD2DEG * acos(d->zz/sqrt(rr));
  ph = RAD2DEG * atan2(d->yy, d->xx);

  r->xx = v->xx; r->yy = v->yy; r->zz = v->zz;

  rot3D1z(-ph, r);
  rot3D1y(-th, r);
  rot3D1z(deg, r);
  rot3D1y( th, r);
  rot3D1z( ph, r);
}

void
rot3D2E(double eps, double eth, double eph, V3D* v,  V3D* r)
{
  r->xx = v->xx; r->yy = v->yy; r->zz = v->zz;

  rot3D1z(eps, r);
  rot3D1y(eth, r);
  rot3D1z(eph, r);
}

void
rot3D2EE(Euler *eu, V3D* v,  V3D* r)
{
  r->xx = v->xx; r->yy = v->yy; r->zz = v->zz;

  rot3D1z(eu->ps, r);
  rot3D1y(eu->th, r);
  rot3D1z(eu->ph, r);
}

void
rot3D2EEI(Euler *eu, V3D* v,  V3D* r)
{
  r->xx = v->xx; r->yy = v->yy; r->zz = v->zz;

  rot3D1z(-eu->ph, r);
  rot3D1y(-eu->th, r);
  rot3D1z(-eu->ps, r);
}


// A = r-p1  B = p2-p1  C = p3-p1
// A dot (B cross C) = 0
void
plane3P(V3D* p1, V3D* p2, V3D* p3, V4D* vv)
{
  V3D B, C;
  double dd;

  sub3D(p1, p2, &B);
  sub3D(p1, p3, &C);

  vv->xx = B.yy * C.zz - B.zz * C.yy;
  vv->yy = B.zz * C.xx - B.xx * C.zz;
  vv->zz = B.xx * C.yy - B.yy * C.xx;
  vv->tt = -vv->xx * p1->xx - vv->yy * p1->yy - vv->zz * p1->zz; 

  dd = modulo((V3D *)vv);
  vv->xx /= dd;
  vv->yy /= dd;
  vv->zz /= dd;
  vv->tt /= dd;
}

int
X3Planes(V4D* v0, V4D* v1, V4D* v2, V3D* pX)
{
  double vm[3][3], mv[3][3];
  double vd[3], pd[3];
  double det;
  int    ii, i1, i2, jj, j1, j2;

  vm[0][0] = v0->xx; vm[0][1] = v0->yy; vm[0][2] = v0->zz; 
  vm[1][0] = v1->xx; vm[1][1] = v1->yy; vm[1][2] = v1->zz; 
  vm[2][0] = v2->xx; vm[2][1] = v2->yy; vm[2][2] = v2->zz; 

  vd[0] = -v0->tt; vd[1] = -v1->tt; vd[2] = -v2->tt; 

  det = 0;
  ii = 0;
  i1 = 1;
  i2 = 2;
  for(jj = 0; jj < 3; jj++) {
    j1 = (jj+1)%3;
    j2 = (jj+2)%3;
    det += vm[ii][jj]*(vm[i1][j1]*vm[i2][j2] - vm[i1][j2]*vm[i2][j1]);
  }

  if(det == 0.) {
//  printf("Determinant of matrix is 0.\n");
    return 0;
  }

  det = 1. / det;
  for(ii = 0; ii < 3; ii++) {
    i1 = (ii+1)%3;
    i2 = (ii+2)%3;
    for(jj = 0; jj < 3; jj++) {
      j1 = (jj+1)%3;
      j2 = (jj+2)%3;
      mv[jj][ii] = det * (vm[i1][j1]*vm[i2][j2] - vm[i1][j2]*vm[i2][j1]);
    }
  }

#if 0
  for(ii = 0; ii < 3; ii++) {               // per verificare la matrice inversa
    for(jj = 0; jj < 3; jj++) {
      det = 0;
      for(i1 = 0; i1 < 3; i1++) {
        det += vm[ii][i1] * mv[i1][jj];
      }
      det = det;
    }
  }
#endif

  for(ii = 0; ii < 3; ii++) {
    det = 0;
    for(jj = 0; jj < 3; jj++) {
      det += mv[ii][jj] * vd[jj];
    }
    pd[ii] = det;
  }
  pX->xx = pd[0]; pX->yy = pd[1]; pX->zz = pd[2];
  
  return 1;

}
// plane vv = (a,b,c,d)  pV=(a,b,c)  d=vv.tt
// line A-->B = AB
// intercept pX = (pV cross (pA cross pB) - d * AB ) / pV dot AB
int
XPlaneLine  (V4D* vv,  V3D* pA,  V3D* pB,  V3D* pX)
{
  V3D AB, pV;
  V3D AxB, VAB;
  double xp;

  pV.xx = vv->xx;
  pV.yy = vv->yy;
  pV.zz = vv->zz;

  line3D(pA, pB, &AB);

  xp = dotprod(&pV, &AB);
  if(!xp) return 0;

  crossprod(pA, pB, &AxB);
  crossprod(&pV, &AxB, &VAB);

  pX->xx = (VAB.xx - vv->tt * AB.xx) / xp;
  pX->yy = (VAB.yy - vv->tt * AB.yy) / xp;
  pX->zz = (VAB.zz - vv->tt * AB.zz) / xp;

  return 1;
}

// p1 e p2 punti di minima distanza delle rette a1a2 e b1b2
int
X2Lines (V3D* a1, V3D* a2, V3D* b1, V3D* b2, V3D* pa, V3D* pb)
{
  double ta, tb, dd;
  double cos_aa_bb, cos_aa_ab, cos_bb_ab;
  V3D    aa, bb, ab;

  line3D(a1, b1, &ab);
  dd = modulo(&ab);
  if(dd == 0.) {
    copy3D(a1, pa);
    copy3D(b1, pb);
    return 1;
  }
  line3D(a1, a2, &aa);
  normalize(&aa);
  line3D(b1, b2, &bb);
  normalize(&bb);
  normalize(&ab);

  cos_aa_bb = dotprod(&aa, &bb);
  if(ABS(cos_aa_bb) >= 1.) {
    return 0;
  }
  cos_aa_ab = dotprod(&aa, &ab);
  cos_bb_ab = dotprod(&bb, &ab);

  ta = dd * ( cos_aa_ab - cos_bb_ab*cos_aa_bb)/(1.-cos_aa_bb*cos_aa_bb);
  tb = dd * (-cos_bb_ab + cos_aa_ab*cos_aa_bb)/(1.-cos_aa_bb*cos_aa_bb);

  pa->xx = a1->xx + ta * aa.xx;
  pa->yy = a1->yy + ta * aa.yy;
  pa->zz = a1->zz + ta * aa.zz;

  pb->xx = b1->xx + tb * bb.xx;
  pb->yy = b1->yy + tb * bb.yy;
  pb->zz = b1->zz + tb * bb.zz;

  dd = distance(pa, pb);

  return 1;
}
