#ifdef GEANT2GASP
#include "geant2Gasp.h"
#else
#include "mgt.h"
#endif

#define BUFLENGTH 8192
#define BUFGAIN   0.5
#define LENGAIN   0.1

unsigned short int *pData;
unsigned short int  nGam = 0;
unsigned short int *nAnc;
unsigned short int *eventBuffer; 
unsigned short int  eventPosition;

int  currentRecord;
int  bufferPosition;

char *bfname;
FILE *bfp;

int   maxFileSize;
int   fileNumber;

int getFileSize();
void initGaspBuffer();
void finitGaspBuffer();
void resetBuffer();
void recordHeader();
void beginOfRun( int );
void openFile();
void closeFile();
void splitFile();
void endOfRun();
void flushBuffer();
void sendEventToBuffer();
void addToBuffer(unsigned short int*, int );
void setMaxFileSize(int );
int calculateEvLength();
void resetEventBuffer();
void eventHeader();
void addAncillaryToEvent();
#ifdef GEANT2GASP
void addGermaniumToEvent();
void newEvent();
#else
void addGammaToEvent( clust *pcl );
#endif

/*************************************************************
*  Calculates binary output file size                      ***
*************************************************************/
int getFileSize()
{
  return ftell(bfp);
}

/*************************************************************
*  Initializes some variables                              ***
*************************************************************/
void initGaspBuffer()
{ 
  pData       = (unsigned short int *)malloc(BUFLENGTH*sizeof(unsigned short int));
  eventBuffer = (unsigned short int *)malloc(BUFLENGTH*sizeof(unsigned short int));
  resetBuffer();
  printf("buffer allocated and ready\n");
  currentRecord  = 0;
  if( !runNumber )
    runNumber = 1;
  bufferPosition = 0;
  maxFileSize    = 2042626048; // 2GB - 100MB
  fileNumber     = 0;
  nAnc           = (unsigned short int *)malloc(numAncil*sizeof(unsigned short int));
}

/*************************************************************
*  Frees allocated memory                                  ***
*************************************************************/
void finitGaspBuffer()
{
  if(pData)       free(pData);
  if(eventBuffer) free(eventBuffer);
}

/*************************************************************
*  Empties record buffer before starting with a new record ***
*************************************************************/
void resetBuffer()
{
  memset( pData, 0, BUFLENGTH*sizeof(unsigned short int) );
}

/*************************************************************
*  Prepares the header of the new record                   ***
*************************************************************/
void recordHeader()
{
#define headerSize 16
  short int header[headerSize];
  memset( header, 0, headerSize*sizeof(short int) );
  
  header[1] = (unsigned short int)(currentRecord >> 8);
  header[2] = (unsigned short int) runNumber;
  header[3] = (unsigned short int)0x4758;     // reduced GASP format
  
  memcpy( pData, header, headerSize*sizeof(short int) );
  
  bufferPosition = headerSize;  
#undef headerSize
}

/*************************************************************
*  Performs the required operations to start a new run     ***
*************************************************************/
void beginOfRun( int number )
{
  runNumber  = number;
  fileNumber = 0;
  
  openFile();
  recordHeader();
  flushBuffer();
}

/*************************************************************
*  Opens new binary file                                   ***
*************************************************************/
void openFile()
{
  char bfpname[32];
  
  if( fileNumber > 0 )
    sprintf(bfpname, "Run.%4.4d.%2.2d", runNumber, fileNumber);
  else  
    sprintf(bfpname, "Run.%4.4d", runNumber);
  
  if( !(bfp = fopen(bfpname,"wb")) ) {
    printf("Error opening file %s\n", bfpname);
    return;
  }
  printf("%s  opened.\n", bfpname);
  fileNumber++;
}

/*************************************************************
*  Closes binary file                                      ***
*************************************************************/
void closeFile()
{
  fclose(bfp);
  printf("Run file closed\n");
}

/*************************************************************
*  Splits binary file when file exceeds the limit          ***
*************************************************************/
void splitFile()
{
  closeFile();
  openFile();
  recordHeader();
  flushBuffer();
}

/*************************************************************
*  End of run operations                                   ***
*************************************************************/
void endOfRun()
{
  flushBuffer();
  closeFile();
}

/*************************************************************
*  Adds current record to the binary file                  ***
*************************************************************/
void flushBuffer()
{
  int nn;
  
  if( ( sizeof(short int)*BUFLENGTH + (unsigned int)getFileSize() ) > (unsigned int)maxFileSize )
    splitFile();
  
  nn = fwrite((char *)pData, sizeof(short int), BUFLENGTH, bfp);
  
  memset( pData, 0, BUFLENGTH*sizeof(short int) );
  currentRecord++;
  recordHeader();
  
}

/********************************************************************
*  Adds the current event to the current record                   ***
********************************************************************/
void sendEventToBuffer()
{
#ifdef GEANT2GASP
  if( calculateEvLength() <= (1 + doAncillary) ) return; 
#endif
  addToBuffer(eventBuffer, eventPosition);
}

/********************************************************************
*  Adds the content of a memory location to the current record    ***
********************************************************************/
void addToBuffer(unsigned short int* event, int size )
{
  if( (bufferPosition + size) >= BUFLENGTH )
    flushBuffer();
  
  memcpy( pData+bufferPosition, event, size*sizeof(short int) );
  bufferPosition += size;
  
}

/********************************************************************
*  Sets maximum file size                                         ***
********************************************************************/
void setMaxFileSize(int size )
{
  if( size > 0 )
    maxFileSize = size;
}

/***************************************************************************
*  Calculates length of the current event.                                 *
*  For each gamma, 9 parameters are written (ndet, egam, r, theta, phi,    *
*   (first point), r, theta, phi (second point), nseg)                     *
*  The contribution of the ancillary detectors is calculated elsewhere     *
***************************************************************************/
int calculateEvLength()
{
  int nn;
#ifndef GEANT2GASP
  clust *pcl;
#endif  
  int length = 1 + doAncillary;

  nGam = 0;
#ifdef GEANT2GASP
  nGam = getNumberOfGermanium();
  if( nGam )
    length += lengthOfGermanium( nGam );
  else
    return -1; /* only gamma-particle coincidences! */  
#else  
#ifdef TAPERED
  if( onlyAncil >= 0 ) {
    if(vertexStyle == VCHISQUARE) {
      for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
	if(pcl->npoints <= MAXACCEPT && pcl->chibest <= chi2Accept ) {
	   length += 9; //numero di rivelatore, energia, raggio, 10*th, 10*ph, raggio, 10*th, 10*ph, numero di segmento
	   nGam++;
	}
      }
    }
    else if(vertexStyle == VLIKELIHOOD) {
      for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
	if(pcl->npoints <= MAXACCEPT && pcl->chibest >  chi2Accept ) {
	   length += 9; //numero di rivelatore, energia, raggio, 10*th, 10*ph, raggio, 10*th, 10*ph, numero di segmento
	   nGam++;
	}
      }
    }
  }
#else
  if(vertexStyle == VCHISQUARE) {
    for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
      if(pcl->npoints <= MAXACCEPT && pcl->chibest <= chi2Accept ) {
	 length += 9; //numero di rivelatore, energia, raggio, 10*th, 10*ph, raggio, 10*th, 10*ph, numero di segmento
	 nGam++;
      }
    }
  }
  else if(vertexStyle == VLIKELIHOOD) {
    for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
      if(pcl->npoints <= MAXACCEPT && pcl->chibest >  chi2Accept ) {
	 length += 9; //numero di rivelatore, energia, raggio, 10*th, 10*ph, raggio, 10*th, 10*ph, numero di segmento
	 nGam++;
      }
    }
  }
#endif  
#endif  
  if( doAncillary ) {
    for( nn=0; nn<numAncil; nn++ ) {
      nAnc[nn] = 0;
      nAnc[nn] = getNumberOfAncillary( nn );
      if(nAnc[nn])
	length += lengthOfAncillary( nn, nAnc[nn] );
    }
  }
  return length;
}

/********************************************************************
*  Empties current event                                          ***
********************************************************************/
void resetEventBuffer()
{
  memset( eventBuffer, 0, BUFLENGTH*sizeof(unsigned short int) );
}

/********************************************************************
*  Prepares current event header                                  ***
********************************************************************/
void eventHeader()
{
  int ii;
  int evLength = calculateEvLength();
  
  eventPosition = 0;
  if( evLength > (1 + doAncillary)) {  // begin of event tag
    eventBuffer[eventPosition++] = (unsigned short int)0xf000 + evLength;
    eventBuffer[eventPosition++] = nGam;
    if( doAncillary ) {
      for( ii=0; ii<numAncil; ii++ )
        eventBuffer[eventPosition++] = nAnc[ii];
    }
  }
  if(eventPosition >= BUFLENGTH) {
    printf("Buffer overrun\n");
    errexit("eventHeader");
  }
}

#ifdef GEANT2GASP
/********************************************************************
*  Gets the germanium detector data                               ***
********************************************************************/
void addGermaniumToEvent()
{
  int length;

  if( !nGam ) return;
  
  length = lengthOfGermanium( nGam );
  
  memcpy( eventBuffer+eventPosition, (unsigned short int *)getGermaniumEvent(), length*sizeof(short int) );
  eventPosition += length;
}
#else
/***************************************************************************
*  Adds a gamma to the current event.                                      *
*  For each gamma, 5 parameters are written (ndet, egam, theta, phi, nseg) *
***************************************************************************/
void addGammaToEvent( clust *pcl )
{
  double int1th, int1ph, int2th, int2ph, int1r, int2r;

  if( !doSort ) return;

  int1th = RAD2DEG*acos(pcl->ppc[0]->cz);
  int1ph = RAD2DEG*atan2(pcl->ppc[0]->cy, pcl->ppc[0]->cx);
  if (int1ph < 0.) int1ph += 360.;
  int1r  = sqrt( pcl->ppc[0]->cx*pcl->ppc[0]->cx + pcl->ppc[0]->cy*pcl->ppc[0]->cy + pcl->ppc[0]->cz*pcl->ppc[0]->cz )/LENGAIN;
  
  /* assuming coordinates in mm, we apply a gain of 0.1 mm/ch */
  if( pcl->npoints < 2 ) {
    int2r  = 0.;
    int2th = 0.;
    int2ph = 0.;
  }
  else {
    int2th = RAD2DEG*acos(pcl->ppc[1]->cz);
    int2ph = RAD2DEG*atan2(pcl->ppc[1]->cy, pcl->ppc[1]->cx);
    if (int2ph < 0.) int2ph += 360.;
    int2r  = sqrt( pcl->ppc[1]->cx*pcl->ppc[1]->cx + pcl->ppc[1]->cy*pcl->ppc[1]->cy + pcl->ppc[1]->cz*pcl->ppc[1]->cz )/LENGAIN;
  }
  
  eventBuffer[eventPosition++] = (unsigned short int)pcl->ppc[0]->nd;
  eventBuffer[eventPosition++] = (unsigned short int)(pcl->eclust/BUFGAIN);
  eventBuffer[eventPosition++] = (unsigned short int)(int1r);
  eventBuffer[eventPosition++] = (unsigned short int)(10 * int1th + 0.5);
  eventBuffer[eventPosition++] = (unsigned short int)(10 * int1ph + 0.5);
  eventBuffer[eventPosition++] = (unsigned short int)(int2r);
  eventBuffer[eventPosition++] = (unsigned short int)(10 * int2th + 0.5);
  eventBuffer[eventPosition++] = (unsigned short int)(10 * int2ph + 0.5);
  eventBuffer[eventPosition++] = (unsigned short int)(pcl->ppc[0]->ss);
  if(eventPosition >= BUFLENGTH) {
    printf("Buffer overrun");
    errexit("addGammaToEvent()");
  }
}
#endif

/********************************************************************
*  Gets the ancillary detector data                               ***
********************************************************************/
void addAncillaryToEvent()
{
  int ii;
  if( !doAncillary ) return;
  
  for( ii=0; ii<numAncil; ii++ ) {
    memcpy( eventBuffer+eventPosition, (unsigned short int *)getAncillaryEvent(ii), lengthOfAncillary( ii, nAnc[ii] )*sizeof(short int) );
    eventPosition += lengthOfAncillary( ii, nAnc[ii] );
  }
}

#ifdef GEANT2GASP
void newEvent()
{
  if( calculateEvLength() > (1 + doAncillary) ) {
    eventHeader();
    addGermaniumToEvent();
    addAncillaryToEvent();
    sendEventToBuffer();
  }
}
#endif
