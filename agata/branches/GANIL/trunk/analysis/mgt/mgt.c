/*****************************************************************
*                                                                *
*   Programma di tracking di MARS/AGATA (versione Marzo 2006)    *
*                                                                *
*   Unita' interne: mm  per distanze                             *
*                   keV per energie                              *
*                                                                *
*   Files usati:                                                 *
*   mgt.h           definizioni e variabili globali              *
*   mgt.c           main, inizializzazioni e funzioni comuni     *
*   mgt_ancillary.c trattamento rivelatori ancillari             *
*   mgt_buffer.c    output dei dati processati in LM             *
*   mgt_eclust.c    clusterizzazioni                             *
*   mgt_etrack.c    tracking                                     *
*   mgt_event.c     lettura e preprocessamento degli eventi      *
*   mgt_geoagata.c  geometria AGATA (parzialmente ancora shell)  *
*   mgt_geobox3.c   geometria planar sui piani delle coordinate  *
*   mgt_geocbar.c   geometria barrel cilindrici                  *
*   mgt_geohbar.c   geometria barrel esagonali                   *
*   mgt_geomxxx.c   geometria volumi convessi brep'd da piani    *
*   mgt_geonone.c   pseudo geometria per non usarne nessuna      *
*   mgt_geoshell.c  geometria shell                              *
*   useful.h        macro e definizioni generiche                *
*   speclib.h       spettri                                      *
*   speclib.c                                                    *
*   matlib.h        matrici                                      *
*   matlib.c                                                     *
*   ge_phot.dat     lunghezze di interazione gamma-ge            *
*   ge_compt.dat                                                 *
*   ge_pair.dat                                                  *
*                                                   DB           *
*****************************************************************/

#include "mgt.h"
#include <time.h>

int   oascii1 = -1;           // first spectrum to write in ASCII format
int   oascii2 = -1;           // last  spectrum to write in ASCII format

#define GAUSSDIM  (32*1024)   // buffer size for gaussian distributed random numbers
//#define NSPECTRA  140
#define NSPECTRA  143
#define EGAIN     2.0         // default gain factor for gamma spectra

/*****************************************************************************

  Meaning of the spectra

 0 emission spectrum (doppler corrected)
 1 "GEANT" spectrum of the emitted gammas (doppler corrected)
 2 spectrum of the gammas after pre-processing (doppler corrected)
 3 spectrum of the interaction points after pre-processing
 4 sum energy of the event (divided by emission multiplicity to avoid overflow)
 5 single spectrum of the detectors 
 6 distribution of preprocessed x(+1000), y(+2000), z(+3000) r(+4000) and
   emission direction th(+5000), ph(+5500)
 7 "GEANT" spectrum of the emitted gammas (non doppler corrected)
 8 spectrum of the gammas after pre-processing (non doppler corrected)
 9 the look-up table of the peaks (=1000) and of the sum-peaks (+=100)
10 energy of initial the clusters (a point can be in more than one cluster)
11 number of packed points for peak-gammas  (offset at Egamma) ????????
12 number of packed points for background gammas  (offset at Egamma) ????????
13 opening angle (re origin) between pairs of points;  distace (at +2000), distance in germanium (at +5000)
14 opening angle of the gamma (+4000 if background)
15 statistics:
 offset  meaning
      0  emission of the cascade
    100  of gammas with full release
    200  of gammas partly escaped
    300  number of gammas in event
    350  empty events
    360  empty events after individual pre-processing
    370  empty events after global pre-processing
    400  estimate of number of gammas in the event
    500  number of original points in event 
    900  number of detected gammas
   1000  number of detectors fired 
   1200  number of points in event after individual packing-smearing
   1500  number of points in event after global packing-smearing
   2000  number of clusters in pool
   2200  number of points in clusters of the pool
   2400  number of gammas in clusters of the pool
   3000  number of accepted clusters
   3100  number of "good" accepted clusters 
   3200  number of points in accepted clusters
   3300  number of points in accepted clusters if peak
   3400  number of points in accepted clusters if background
   3500  number of gammas in accepted clusters
   3600  number of gammas in accepted clusters if peak
   3700  number of gammas in accepted clusters if background
   4000  number of fired detectors
   4200  number of fired segments
   4400  number of interaction points per detector
   4600  number of interaction points per segment
   4800  number of fired segments per fired detector
16 smearing distance and projection on x, y and z  (scaled *10 --> 0.1mm)
17 LAB angular distribution re z of the original gammas (normalized to 1000/sin(theta))
18 statistics of segments
19 statistics of segments (STATINSIDESEG)

20 100*X2 for clusterpool if  peak 
21 100*X2 for clusterpool if  backgr 
22 100*atan(chi) for initial peak clusters
23 100*atan(chi) for initial backgr clusters
24 100*atan(chi) for accepted peak clusters
25 100*atan(chi) for accepted backgr clusters
28 100*X2 accepted peak gammas
29 100*X2 accepted backgr gammas

30 RECONSTRUCTED SPECTRUM
31 RECONSTRUCTED SPECTRUM as photoelectric
32 RECONSTRUCTED SPECTRUM as Compton
33 RECONSTRUCTED SPECTRUM as pair production (Etot - 1022)
34 RECONSTRUCTED SPECTRUM with points belonging to one gamma 
35 RECONSTRUCTED SPECTRUM with chi2<chi2Accept/10 (chi2>chi2Accept*10 if Likelihood) 
36 spectrum of residual clusters (points can be in more than one)
39 opening angle of accepted clusters; (+2k)distance to next point if peak;; (+4k) e (+6k) se fondo
40 spectrum of cluster accepted bu tracking but not used as good (see CHI2TIMES)

41... reconstructed spectra sorted according to number of interaction points
...

51 reconstructed cluster with mechanism 0  (Link)
52 reconstructed cluster with mechanism 1  (Leader)
53 reconstructed cluster with mechanism 2  (Merge)
54 reconstructed cluster with mechanism 3  (Pair0)
55 reconstructed cluster with mechanism 4
56 deviation and difference (+4000) re incident direction (peak)
57 deviation and difference (+4000) re incident direction (background)
58 spectrum of first point for compton reconstructed (+4000 backgr)
59 spectrum of last  point for compton reconstructed (+4000 backgr)

60 recombined cluster
61 recombined clusters good
62 recombined clusters bad
63 recombined clusters new

100...139 reconstructed spectra, sorted according to segment of first interaction point 


140 RECONSTRUCTED SPECTRUM LAB conditioned by the first 15 crystals
141 RECONSTRUCTED SPECTRUM re z of the original gammas (normalized to 1000/sin(theta)) once detected (in photopeak)
142 LAB angular distribution re z of the reconstructed gammas (normalized to 1000/sin(theta))

*/




void    defaultvals     ();                           // setta valori default per varibili di comando
void    getparams       (int, char **);               // decoding of the command line
void    numparams       (char *, int);                // errore sul numero parametri
void    printparams     ();                           // prints the parameters of the run
void    readFileHeaders ();                           // read data file headers
void    initialize      ();                           // initialization of various data structures
void    initgeometry    ();                           // read geometry information and initialisation
void    initgammas      ();                           // read gammas & create LookUp Table of peaks
void    initparticles   (char *);                     // read energy of emitted particles
void    initsourcebeta  ();                           // read recoil velocity
void    initsourcedir   ();                           // read recoil direction
void    initsourcepos   ();                           // reads position of gamma source
void    inittargetsiz   ();                           // dummy function - reads line and continues
void    initoutputmask  (char *);                     // reads description of data-fields
void    smearsource     ();                           // smears source position and velocity
void    printallpoints  (point **, int);              // print list of globally preprocessed points
void    orderintarray   (int *, int);                 // ordina in modo creshente gli elementi dell'array
void    analisi         ();                           // analysis of some generated spectra

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

int
main(int argc, char *argv[])
{
  int     ii, nn;
  int    *pts;
  char    specname[20];
  double  val, atot1, atot2;
  clock_t start, finish;

  defaultvals();
  getparams(argc, argv);
  initialize();
  printparams();
  if(nsegMax < 0) nsegMax = nsegtot-1;

  start = clock();

///////////////////////////////////////////////////////////
////////////////// the event loop  ////////////////////////
///////////////////////////////////////////////////////////

  for (evnum = 0; evnum < nevents; evnum++) {
    if(evnum && !(evnum%1000)) {
      finish = clock();
      val = (double)(finish - start) / CLOCKS_PER_SEC;
      printf("%10dev %7.1fs\r",evnum, val);
    }

    nextevent();
    if(npev < 0) break;

////////////////////////////////////////////////////////////
//////////// Event header////////////// ////////////////////
////////////////////////////////////////////////////////////
#ifdef FORPRISMA
    if( doPrismaEvs )
      prismaEventHeader();
#endif    

    if(evnum < evskip1 || evnum < evskip2) {
#ifdef FORPRISMA
      if( doPrismaEvs )
	prismaEventFooter();
#endif    
      continue;
    }  

    if(npev < npevMin) {
      if( doWriteEvs ) {
        writeevent(ppev, npev);
        if( doAncillary )
          writeAncillaryEvent();
      }
#ifdef FORPRISMA
      if( doPrismaEvs )
	prismaEventFooter();
#endif    
      continue;
    }

    if(npevMax && npev>npevMax) {
#ifdef FORPRISMA
      if( doPrismaEvs )
	prismaEventFooter();
#endif    
      continue;
    }

    if(llist) printallpoints(ppev, npev);
    projections();

    if( doWriteEvs ) {
      writeevent(ppev, npev);
      if( doAncillary )
        writeAncillaryEvent();
    }

    if(doTracking) {
      if(doSmearSource) {
        smearsource();
        for(nn = 0; nn < npev; nn++)
          setcosdir(ppev[nn]);
      }

      reconstruct();

////////////////////////////////////////////////////////////
//////////// Event footer///////////////////////////////////
////////////////////////////////////////////////////////////
#ifdef FORPRISMA
    if( doPrismaEvs )
      prismaEventFooter();
#endif    

      if(doSmearSource) {
        memcpy(&origin, &oorigin, sizeof(point));
        memcpy(&recoil, &orecoil, sizeof(recoil));
      }

    }

  }

///////////////////////////////////////////////////////////
//////////////// end of event loop ////////////////////////
///////////////////////////////////////////////////////////

  finish = clock();
  if(evnum)
    val = 1000./evnum * (double)(finish - start) / CLOCKS_PER_SEC;
  else
    val = 0;

  printf("\n%8d Events (%.1f ms/ev)", evnum, val);
  if(varmult)   printf("  (%d Gammas)", gamtot);
  if(kdetMin)   printf("\n%8d Events below threshold", evrejected);
                printf("\n%8d Energy points %7d Packed points  %d Mixed points",
                              ndataread-evnum,  npacked,          nmixed);
  if(nclumixed) printf("\n%8d Events with mixed points", nclumixed);
  if(ncombined) printf("\n%8d Recombined gammas (%d %d %d)", ncombined, ncombgood, ncombbad, ncombnew);
  printf("\n");

#if STATINSIDEDET == 1
  statinsidedet((point *)NULL);
#endif

  finitevent();
  finitclust();
  finittrack();
  
  if(!evgen) {
    analisi();
    printf("\n");
  }

  if(doTracking) {
    pts = specPointer(17, 0);
    atot1 = atot2 = 0.;
    for(ii = 0; ii < 180; ii++) {
      val = pts[ii];
      atot1 += val;
      val   /= sin(DEG2RAD*(ii + 0.5));
      atot2 += val;
    }
    if(atot1) {
      val = atot1/atot2;
      for(ii = 0; ii < 180; ii++) {
        pts[ii+1024] = (int)( val * pts[ii] / sin(DEG2RAD*(ii + 0.5)));
      }
    }
  }

  specSave(1);
  
  if( doSort )
    endOfRun();
  
  for(ii = oascii1; ii <= oascii2; ii++) {
    if(ii < 0 || ii >= NSPECTRA) continue;
    pts = specPointer(ii, 0);
    memset(specname, 0, 20);
    sprintf(specname, "spec%2.2d.asc", ii);
    specWriteASCII(pts, 0, SPECLEN-1, specname);
  }
  
  matSave(1);
  
  printf("\nLargest value of: nobuf = %d,  pgamlen = %d,  npev = %d,  nclupool = %d,  ncludone = %d,  nclugood = %d",
                              nobufmax,    pgamlenmax,    npevmax,    nclupoolmax,    ncludonemax,    nclugoodmax);
  if(nbadpoints) printf("\n %d  points discarded as found inside bad segments", nbadpoints);
  if(noutside1)  printf("\n %d  original points found outside detector volume", noutside1);
  if(noutside2)  printf("\n %d  points found outside detector volume in smearpos", noutside2);
  printf("\n");

  return 0;
}

void errexit(char * where)
{
  printf("exiting in  %s  (evnum = %d -- evnumMC = %d)\n", where, evnum, evnumMC);
  exit(-1);
}

void
defaultvals()
{
  srand(1);                             // inizializza il generatore di numeri random
  GEOMETRY      = NONVALID;             // per forzare l'inizializzazione della geometria
  mult          = 1;                    // molteplicita' della cascata gamma
  evgen         = 0;                    // generator di tipo semplice
  kdetMin       = 0;                    // dont trigger on number of detectors in original event
  cluGood       = 0;                    // don't fix number of clusters to 1
  nevents       = 100000;                // quanti eventi verranno analizzati
  evskip1       = 0;                    // eventi da non trattare completamente
  evskip2       = 0;                    // eventi da non ricostruire
  llist         = 0;                    // scritte estese sul terminale se != 0
  vertexFunct   = 1;                    // usa la versione VE per testare il vertice Compton
  vertexStyle   = VCHISQUARE;           // usa il metodo del chi2 per il vertice
  chi2Accept    = 1.0;                  // max Chi2 per accettare un cluster
  cluAngMin     =   6.;                 // angolo di clusterizzazione minimo nella procedura ricorsiva
  cluAngMax     = -40.;                 // angolo di clusterizzazione in gradi (defaults to varang)
  cluDistMin    =  20.;                 // distanza minima di clusterizzazione in mm
  cluDistMax    = 200.;                 // distanza massima di clusterizzazione in mm
  distmin       = 5.;                   // distanza (mm) di impaccamento dei punti (packing)
  disterr       = 5.;                   // errore   (mm) sulla posizione           (smearing)
  eminP         = 5.0;                  // soglia energetica (in keV) sui punti (dopo impaccamento)
  eminD         = 5.0;                  // soglia energetica (in keV) sui rivelatori (dopo impaccamento)
  fwhm          = 1.9;                  // risoluzione energetica a 1333 keV
  fwhn          = 1.0;                  // componente di rumore della risoluzione energetica
  doTracking    = 1;                    // tracking is performed
  doWriteEvs    = 0;                    // don't write the preprocessed data to file
  outputType    = 0;                    // only preprocessed points written to output file
  recoil.beta   = 0.;                   // recoil v/c
  recoil.gamma  = 1.;                   // 1/sqrt(1-beta^2)
  posForced     = 0;
  enerfactor    = 1.;                   // for untagged files energies  are in keV
  distfactor    = 10.;                  // for untagged files distances are in cm
  recForced     = 0;
  minegam       = MINEGAM;
  maxegam       = MAXEGAM;
  dEgain        = EGAIN;
  npevMin       = 1;
  npevMax       = 0;                    // nocheck
  readsegs      = 0;
  nsegMin       = 0;
  nsegMax       = -1;                   // i.e. nocheck
  check_xyz     = 0;
  sposX = sposY = sposZ = 0.;
  doSort        = 0;
  runNumber     = 1;
  doAncillary   = 0;
  doAncAngles   = 0;
  doAncPixels   = 0;
  numAncil      = 1;
  doPrismaEvs   = 0;
  minOffset     = 1000;
  memset(outputmask, 0, sizeof(outputmask));
}

void
getparams(int argc, char *argv[])
{
  int     nn, ok, ii;
  double  xx;
  char   *cmd;

  if (argc > 1) {
    nn = 1;
    ok = 1;
  }
  else {
    ok = 0;
  }
  while(nn < argc && ok == 1) {
    cmd = argv[nn++];
    ok  = 1;
    if(!strcmp(cmd, "-h")) {
      ok = -1;
    }
    else if(!strcmp(cmd, "-f")) {
      numparams(cmd, argc-nn-1);
      ifname = calloc(strlen(argv[nn]) + 1, 1);
      strcpy(ifname,  argv[nn++]); 
    }
    else if(!strcmp(cmd, "-m")) {
      numparams(cmd, argc-nn-1);
      mult = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-n")) {
      numparams(cmd, argc-nn-1);
      nevents = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-egam")) {
      numparams(cmd, argc-nn-2);
      minegam = atof(argv[nn++]);
      maxegam = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-gain")) {
      numparams(cmd, argc-nn-1);
      dEgain = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-kd")) {
      numparams(cmd, argc-nn-1);
      kdetMin = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-np")) {
      numparams(cmd, argc-nn-2);
      npevMin = atoi(argv[nn++]);
      npevMax = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-ns")) {
      numparams(cmd, argc-nn-2);
      nsegMin = atoi(argv[nn++]);
      nsegMax = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-ll")) {
      numparams(cmd, argc-nn-1);
      llist = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-cluth")) {
      numparams(cmd, argc-nn-2);
      cluAngMin = atof(argv[nn++]);
      cluAngMax = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-cludd")) {
      numparams(cmd, argc-nn-2);
      cluDistMin = atof(argv[nn++]);
      cluDistMax = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-cluall")) {
      cluGood = 1;
    }
    else if(!strcmp(cmd, "-vtx")) {
      numparams(cmd, argc-nn-2);
      vertexFunct   = atoi(argv[nn++]);
      vertexStyle   = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-lim")) {
      numparams(cmd, argc-nn-1);
      chi2Accept = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-dd")) {
      numparams(cmd, argc-nn-2);
      distmin = atof(argv[nn++]);
      disterr = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-exp")) {
      distmin = 0.;
      disterr = 0.;
      fwhm    = -15;
      fwhn    =  -5;
    }
    else if(!strcmp(cmd, "-sp")) {
      numparams(cmd, argc-nn-3);
      sposX = atof(argv[nn++]);
      sposY = atof(argv[nn++]);
      sposZ = atof(argv[nn++]);
      posForced = 1;
    }
    else if(!strcmp(cmd, "-sr")) {
      numparams(cmd, argc-nn-3);
      recVc = atof(argv[nn++]);
      recTh = atof(argv[nn++]);
      recPh = atof(argv[nn++]);
      recForced = 1;
    }
    else if(!strcmp(cmd, "-ss")) {
      numparams(cmd, argc-nn-5);
      ssDx  = atof(argv[nn++]);
      ssDy  = atof(argv[nn++]);
      ssDz  = atof(argv[nn++]);
      ssDth = atof(argv[nn++]);
      ssDv  = atof(argv[nn++]);
      doSmearSource = 1;
    }
    else if(!strcmp(cmd, "-xyz")) {
      numparams(cmd, argc-nn-6);
      xx_min = atof(argv[nn++]);
      xx_max = atof(argv[nn++]);
      yy_min = atof(argv[nn++]);
      yy_max = atof(argv[nn++]);
      zz_min = atof(argv[nn++]);
      zz_max = atof(argv[nn++]);
      check_xyz = 1;
    }
    else if(!strcmp(cmd, "-em")) {
      numparams(cmd, argc-nn-2);
      eminP = atof(argv[nn++]);
      eminD = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-fw")) {
      numparams(cmd, argc-nn-2);
      fwhm  = atof(argv[nn++]);
      fwhn  = atof(argv[nn++]);
    }
    else if(!strcmp(cmd, "-oa")) {
      numparams(cmd, argc-nn-2);
      oascii1 = atoi(argv[nn++]);
      oascii2 = atoi(argv[nn++]);
    }
    else if(!strcmp(cmd, "-nt")) {
      doTracking = 0;
    }
    else if(!strcmp(cmd, "-of")) {
      numparams(cmd, argc-nn-1);
      ofname = calloc(strlen(argv[nn]) + 1, sizeof(char));
      strcpy(ofname, argv[nn++]);
      doWriteEvs = 1;
    }
    else if(!strcmp(cmd, "-ot")) {
      numparams(cmd, argc-nn-1);
      outputType = atoi(argv[nn++]);
      outputType = ABS(outputType);
    }
    else if(!strcmp(cmd, "-sk")) {
      numparams(cmd, argc-nn-2);
      evskip1 = atoi(argv[nn++]);
      evskip2 = atoi(argv[nn++]);
    }
#if SEGBAD == 1
    else if(!strcmp(cmd, "-bs")) {
      numparams(cmd, argc-nn-1);
      nbadseg = atoi(argv[nn++]);
    }
#endif
#ifdef FORPRISMA
    else if(!strcmp(cmd, "-pf")) {
      numparams(cmd, argc-nn-1);
      ofname = calloc(strlen(argv[nn]) + 1, sizeof(char));
      strcpy(ofname, argv[nn++]);
      doPrismaEvs = 1;
    }
#endif
    else if(!strcmp(cmd, "-sort")) {
      numparams(cmd, argc-nn-1);
      runNumber = atoi(argv[nn++]);
      doSort = 1;
    }
    else if(!strcmp(cmd, "-anc")) {
      /* first read how many kind of ancillaries and the gain which will be used */
      numparams(cmd, argc-nn-2);
      numAncil   = atoi(argv[nn++]);
      ancBufGain = atof(argv[nn++]);
      if( numAncil < 0 ) numAncil = 1;
      /* are there enough parameters ?*/
      numparams(cmd, argc-nn-4*numAncil);
      doAncillary = 1;
      /* allocates memory */
      ancOffset = (int *) calloc( numAncil, sizeof(int) );
      ancNParam = (int *) calloc( numAncil, sizeof(int) );
      ancNDetec = (int *) calloc( numAncil, sizeof(int) );
      ancNSegme = (int *) calloc( numAncil, sizeof(int) );
      ancSize   = 1;
      maxOffset = 0;
      minOffset = 1000000;
      /* reads each ancillary */
      for( ii=0; ii<numAncil; ii++ ) {
	ancOffset[ii] = atoi(argv[nn++]);
	ancNParam[ii] = atoi(argv[nn++]);
	ancNDetec[ii] = atoi(argv[nn++]);
	ancNSegme[ii] = atoi(argv[nn++]);
	if( ancOffset[ii] < minOffset )
	  minOffset = ancOffset[ii];
	if( ancNDetec[ii] * ancNSegme[ii] > ancSize ) 
	  ancSize = ancNDetec[ii] * ancNSegme[ii];
	if( ancOffset[ii] + 1000 * ancNParam[ii] > maxOffset )
	  maxOffset = ancOffset[ii] + 1000 * ancNParam[ii];  
      }
      maxOffset += 1000;
      ancBufLen = ancSize * maxOffset/1000;
    }
    else if(!strcmp(cmd, "-ang")) {
      numparams(cmd, argc-nn-1);
      afname = calloc(strlen(argv[nn]) + 1, 1);
      strcpy(afname,  argv[nn++]);
      doAncAngles = 1; 
    }
    else if(!strcmp(cmd, "-pix")) {
      numparams(cmd, argc-nn-1);
      afname = calloc(strlen(argv[nn]) + 1, 1);
      strcpy(afname,  argv[nn++]);
      doAncPixels = 1; 
    }
    else {
      ok = 0;
    }
  }
  
  if( doAncPixels ) {
    if( doAncAngles ) {
      printf( " --> Warning! Selected options are not compatible! Ignoring them.\n");
      doAncAngles = 0;
      doAncPixels = 0;
    }
  }
  
#ifdef FORPRISMA
  if( doPrismaEvs ) {
    if( doWriteEvs ) {
      printf( " --> Warning! Selected options are not compatible! Ignoring them.\n");
      doPrismaEvs = 0;
      doWriteEvs  = 0;
    }
  }
#endif

  if(ok < 1) {
    if(ok == 0 && argc > 1) printf("\nInvalid switch %s\n", argv[nn-1]);
    printf("\nUsage: %s  -[...]\n\n", argv[0]);
    printf("    -f     file      datafile\n");
    printf("    -m     mult      multiplicity of gamma cascades\n");
    printf("    -n     nn        number of events to analize\n");
    printf("    -kd    n         trigger on number of hit detectors\n");
    printf("    -sk    n1 n2     skip first n1 events, dont reconstruct first n2 events\n");
    printf("    -egam  e1 e2     allowed energy range for emitteg gammas\n");
    printf("    -gain  g         gain factor (ch/keV) for energy spectra\n");
    printf("    -ll    ll        listing level (0 = minimal)\n");
    printf("    -dd    dp ds     packing and smearing distance (mm)\n");
    printf("    -exp             no packing and smearing (e.g. for exp. data)\n");
    printf("    -fw    fwhm fwhn energy resolution at 1333 keV and noise component (keV)\n");
    printf("    -cluth min max   min, max angular opening of clusters (degrees)\n");
    printf("    -cludd min max   min, max clusterization distance (mm)\n");
    printf("    -cluall          clusterization of all original gammas\n");
    printf("    -lim   val       chisquare or likelihood acceptance value\n");
    printf("    -vtx   f s       Compton Vertex function and style\n");
    printf("                          f=1 ==> E1    f=2 ==> e1   f=3 ==> cos\n");
    printf("                          s=1 ==> Chi2  s=2 ==> Likelihood\n");
    printf("    -sp    x y z     gamma source position (overrides SOURCE in datafile)\n");
    printf("    -sr    vc th ph  source recoil velocity and direction (overrides BETA)\n");
    printf("    -ss    dx dy dz dth dv  smearing of source position, recoil direction and velocity\n");
    printf("    -em    emP emD   energy threshold of preprocessed Points, Detector (keV)\n");
    printf("    -xyz   xm xM ym yM zm zM   restrict range of x, y, z values\n");
    printf("    -np    n1 n2     allowed number of points in prepocessed event\n");
    printf("    -ns    n1 n2     discard segments outside this range\n");
    printf("    -nt              skip tracking (i.e. do only preprocessing of data)\n");
    printf("    -of    file      output datafile\n");
#ifdef FORPRISMA
    printf("    -pf    file      output datafile for combined AGATA+PRISMA simulation\n");
#endif
    printf("    -ot    nn        how much to write on output(0=only packed points,\n");
    printf("                                                 1=also headers of original gammas,\n");
    printf("                                                 2=also list of  original points)\n");
    printf("    -oa    n1 n2     write spectra n1...n2 in ASCII format\n");
    printf("    -bs    nn        number of bad segments\n");
    printf("    -sort  #nn       write out data in GASP format in file Run.#nn\n");
    printf("    -anc nAnc gain offset_1 npar_1 ndets_1 nsegs_1 ... offset_N npar_N ndets_N nsegs_N\n");
    printf("             considers the possibility to have nAnc ancillary detectors each with npar_i parameters\n");
    printf("             composed of ndets_i detectors (each with nsegs_i segments), with detector number having\n");
    printf("             an offset offset_i, using a gain gain to send to the GASP format datafile\n");
    printf("    -ang file  read angles of the ancillary detectors from file\n");
    printf("    -pix file   splits ancillary detector into pixels and writes out their angles\n");
    printf("                       to data according to the Euler angles contained in file\n");
    printf("    -h               print this list\n");
    printf("\n");
    if(argc > 1 || ok < 0)
      errexit("getparams");
  }

  if(!ifname) {
    printf("Please input name of data file  (-f filename)\n");
    errexit("getparams");
  }

  varmult  = (mult > 0) ? (0) : (1);
  mult     = MIN(MAX(ABS(mult), 1), MAXMULT);
  if(mult == 1) varmult = 0;
  if(varmult) {
    varmult = MAX(1, (mult * 4) / 5);
    sigmult = mult/4./SIG2FW;
  }

  thmult[0] = thmult[1] = 180.;
  for (nn = 2; nn <= MAXMULT; nn++) thmult[nn] = RAD2DEG * acos(1.0 - 2.0/pow((double)nn,0.9));

  nevents  = MAX(nevents, 1);

  if(minegam > maxegam) SWAP(minegam, maxegam, xx)
  minegam    = MAX(minegam, MINEGAM);
  maxegam    = MIN(maxegam, MAXEGAM);

  if(vertexStyle == 1) {
    vertexStyle   = VCHISQUARE;
    if(chi2Accept < 0.01)
      printf("\nWARNING: very low acceptance value (%f) for CHISQUARE\n", chi2Accept);
    chi2Accept = MAX(chi2Accept, 0.);
  }
  else if(vertexStyle == 2) {
    vertexStyle   = VLIKELIHOOD;
    if(chi2Accept > 0.1)
      printf("\nWARNING: very high acceptance value (%f) for LIKELIHOOD\n", chi2Accept);
    chi2Accept = MIN(chi2Accept, 1.);
  }
  else {
    printf("Invalid vertex style %d (should be 1 or 2)\n", vertexStyle);
    errexit("getparams");
  }

  varang     = (cluAngMax > 0) ? (0) : (1);
  cluAngMax  = MIN(MAX(ABS(cluAngMax), 1.0), 180.);

  distmin    = MAX(distmin, 0.0);         // <= 0 means no packing
  disterr    = MAX(disterr, 0.0);         // <= 0 means no smearing

  eminP      = MAX(eminP, 0.1);           // at least this energy threshold
  eminD      = MAX(eminD, 0.1);           // at least this energy threshold

  npevMin    = MAX(npevMin, 1);
  npevMax    = MAX(npevMax, 0);

  nsegMin    = MAX(nsegMin, 0);
  if(nsegMax  >= 0) nsegMax = MAX(nsegMax, nsegMin);

  nbadseg    = MAX(nbadseg, 0);
}

void
numparams(char * cmd, int nn)
{
  if(nn >= 0) return;
  printf("error decoding command %s ==> %d parameter(s) missing \n", cmd, ABS(nn));
  errexit("numparams");
}

void
printparams()
{
  int ii;
  printf("\n");
  
  printf("Input data file              %s\n", ifname);
  
  if(evgen) {
    printf("Multiplicity                 ==> defined by GENERATOR\n");
#ifdef FORPRISMA
    printf("Input data file should contain proper event numbering.\n");
#endif
  }
  else if(varmult)
    printf("Multiplicity                 %10d     (variable around %d)\n", mult, varmult);
  else {        
    printf("Multiplicity                 %10d\n", mult);
  }
  
  printf("Number of events             %10d\n", nevents);
  if(kdetMin)
    printf("Min. number of hit detectors %10d\n", kdetMin);
  
  printf("Using crystal segmentation   %s\n", segmentstring);
  
  if(nsegMax>=0)
    printf("Allowed range of segments    %10d %d\n", nsegMin, nsegMax);

#if SEGBAD == 1
  if(nbadseg)
    printf("Number of bad segments       %10d\n", nbadseg);
#endif

#if SEGPACK == 1
  printf("Packing  distance            ==> packing points in segments\n");
#else
  printf("Packing  distance            %10.2f mm", distmin);
  
  if(distmin)
    printf("\n");
  else
    printf("   ==> no packing\n");
#endif
  
#if SEGCENTER==1 && SEGPACK==1
  printf("Smearing distance            ==> moving  point  to segment center (if possible)\n");
#else
  printf("Smearing distance @ 100 keV  %10.2f mm", disterr);
  if(disterr)
    printf("\n");
  else
    printf("   ==> no position smearing\n");
#endif
  
  printf("FWHM at 1332 keV             %10.3f keV", fwhm);
  
  if(fwhm < 0)
    printf("  ==> no energy smearing\n");
  else
    printf("\n");
  printf("FWHM of noise                %10.3f keV", fwhn);
  
  if(fwhn < 0)
    printf("  ==> no noise smearing\n");
  else
    printf("\n");
    
  printf("Min. Energy of packed points %10.3f keV\n", eminP);
  printf("Min. Energy in a detector    %10.3f keV\n", eminD);
  
#if    SELECTEVENTS == EVPEAK
  printf("ONLY PEAK EVENTS WILL BE TAKEN FROM DATA FILE !!!!\n");
#elif  SELECTEVENTS == EVBACK
  printf("ONLY BACKGROUND EVENTS WILL BE TAKEN FROM DATA FILE !!!!\n");
#endif
  
  if(npevMax)
    printf("Allowed number of prepr. points%8d %d\n", npevMin, npevMax);
  
  if(doTracking) {
    if(vertexStyle == VCHISQUARE) {
      printf("Compton vertex function      %s  ==> CHISQUARE\n", cvertexstring);
      printf("MAX accepted CHISQUARE     %14.5f", chi2Accept);
      if(chi2Accept < 0.01) printf(" <== ???????????");
      printf("\n");
    }
    else if(vertexStyle == VLIKELIHOOD) {
      printf("Compton vertex function      %s  ==> LIKELIHOOD\n", cvertexstring);
      printf("MIN accepted LIKELIHOOD    %14.5f", chi2Accept);
      if(chi2Accept > 0.1) printf(" <== ???????????");
      printf("\n");
    }
    else {
      printf("Invalid vertex style %d\n", vertexStyle);
      errexit("printparams");
    }
    if(varang)
      printf("Clusterization angle         %10.3f -- %.3f (variable) deg\n", cluAngMin, ((int)(10 * thmult[mult]))/10.);
    else
      printf("Clusterization angle         %10.3f -- %.3f deg\n", cluAngMin, cluAngMax);
    printf("Clusterization distance      %10.2f -- %.2f mm\n", cluDistMin, cluDistMax);
    if(cluGood)
      printf("Clusterizing ALL detected gammas\n");
  }
  else {
    printf("Tracking will NOT be done\n");
  }
  
  if(doWriteEvs) {
    printf("Preprocessed data written to %s\n", ofname);
    if(outputType==1)
      printf("                             together with headers of original gammas\n");
    if(outputType >1)
      printf("                             together with full list of original gammas\n");
  }

#ifdef FORPRISMA
  if(doPrismaEvs) {
    printf("Tracked data written to %s\n", ofname);
  }
#endif
  
  if(datmult>1)
    printf("Energy of the %2d transitions ", datmult);
  else if(datmult > 0)
    printf("Energy of the transition   ");
  for(ii = 0; ii < datmult; ii++) {
    if(ii && ii%10 == 0) printf("\n                             ");
    printf(" %6.1f", dategam[ii]);    
  }
  printf("\n");
  
  printf("Listing level is %d\n", llist);
  printf("\n");
  
  if(evskip1>0)
    printf("SKIPPING THE FIRST %d EVENTS !!!!\n", evskip1);
  if(evskip2>0)
    printf("SKIPPING THE RECONSTRUCTION OF THE FIRST %d EVENTS !!!!\n", evskip2);
  
}

void
initialize()
{
  int jj;
#if SEGBAD == 1
  int ii;
#endif
  
  specDefine(SPECLEN, NSPECTRA, "spec.dat", "results");
  specSetGain(dEgain);

//matDefine(MATSIZE, MATSIZE, "mat.dat", "e-sigma");

  memset(&origin, 0, sizeof(point));
  origin.nd = origin.ind = -1;
  strcpy(origin.chid, "Source");
  origin.s2 = pow(1. / SIG2FW, 2.); // assume source position error = 1 mm FWHM

  memset(&recoil, 0, sizeof(pvect));
  recoil.cz    = 1.;     // assume recoil along +z

  if(posForced) {
    origin.xx = sposX;
    origin.yy = sposY;
    origin.zz = sposZ;
    origin.rr = sqrt(sposX*sposX + sposY*sposY + sposZ*sposZ);
  }

  if(recForced) {
    recoil.beta = recVc;
    recoil.cx   = sin(DEG2RAD*recTh)*cos(DEG2RAD*recPh);
    recoil.cy   = sin(DEG2RAD*recTh)*sin(DEG2RAD*recPh);
    recoil.cz   = cos(DEG2RAD*recTh);
  }

  recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);

  for (jj = 0; jj < MAXEKEV; jj++)
    peakwidth[jj] = MAX(PEAKWIDTH * sqrt(fwhn*fwhn + fwhm*fwhm * jj/1332.51), 3.);

  readFileHeaders();

  if(GEOMETRY < 0) {
    GEOMETRY = NONE;
    geominit_none(0);
  }

#if SEGBAD == 1
  segbad = (int *)calloc(nsegtot, sizeof(int));
  if(nsegdet > 1 && nbadseg > 0) {
    ii = MIN(nbadseg, nsegtot);
    printf("Disabling %d of the %d segments\n", ii, nsegtot);
    while(ii) {
      jj = rand()%nsegtot;
      if(!segbad[jj]) {
        segbad[jj] = 1;
        ii--;
      }
    }
  }
#endif

  initclust();
  inittrack();
  initevent();      // this call also rewinds the input to the beginning of simulated data ($)
  
  if( doSort ) {
    initGaspBuffer();
    beginOfRun( runNumber );
  }  
  
  if( doAncillary ) {
    initAncillary();
    if( doAncAngles || doAncPixels )
      readAncAngles();        /* the angles have different meaning depending on the flag!!! */
  }  

  gamtot = evrejected = nobufmax = pgamlenmax = npevmax = 0;
  nclupoolmax = ncludonemax = nclugood = nclugoodmax = nclumixed = 0;
  noutside1 = noutside2 = ndataread = npacked = nmixed = 0;
  ncombined = ncombgood = ncombbad = ncombnew = 0;
}

void
readFileHeaders()
{
  int dowhat;
  int   ii, nn;
  char  dummy[256];
	int   dummy1, dummy2, dummy3;

  if( (ifp = fopen(ifname, "r")) == NULL) {
    printf("Error opening %s\n", ifname);
    errexit("readFileHeaders");
  }
	printf("\n");

  if(fscanf(ifp, "%s", dummy) != 1) {
    printf("Error reading %s\n", ifname);
    errexit("readFileHeaders");
  }
  if(!strcmp(dummy, "MARS")) {
    dataFileVersion = FMARS;
    printf("Format of the file is %s\n", dummy);
  }
  else if(!strcmp(dummy, "AGATA")) {
    dataFileVersion = FAGATA;
    if(fscanf(ifp, "%d.%d.%d ", &dummy1, &dummy2, &dummy3) == 3) {
      printf("Format of the file is %s, Version %d.%d.%d\n", dummy, dummy1, dummy2, dummy3);
    }
    else {
      printf("%s: Warning: No format version!\n", dummy);
    }
  }
  else {
    printf("File must begin with keyword AGATA or MARS\n");
    errexit("readFileHeaders");
  }

  dowhat = 0;
  while(1) {

    if(fscanf(ifp, "%s", dummy) != 1) {
      printf("Error reading %s\n", ifname);
      errexit("readFileHeaders");
    }
    if(!strcmp(dummy, "$")) {
      break;
    }
    if(!strcmp(dummy, "#")) {
      fgets(dummy, 250, ifp);   // read rest of line
      continue;
    }

    nn = strlen(dummy);
    for(ii = 0; ii < nn; ii++)
      dummy[ii] = toupper(dummy[ii]);

    if(!strcmp(dummy, "GAMMA")) {
      initgammas();
      continue;
    }
    if( !strcmp(dummy, "NEUTRON")  ||
        !strcmp(dummy, "PROTON")  ||
        !strcmp(dummy, "ALPHA") ||
        !strcmp(dummy, "E-") ||
        !strcmp(dummy, "E+") ) {
      initparticles(dummy);
      continue;
    }
    else if(!strcmp(dummy, "G4TRACKING")) {
      if(fscanf(ifp, "%d", &dummy1) != 1) {
        printf("Error reading method\n");
        errexit("readFileHeaders");
      }
      printf("GEANT4 Tracking method is %d\n", dummy1);
      continue;
    }
    else if(!strcmp(dummy, "DATE")) {
      if(fgets(dummy, 256, ifp) == NULL) {
        printf("Error reading DATE\n");
        errexit("readFileHeaders");
      }
      nn = strlen(dummy);
      if(dummy[nn-1] == 10) dummy[nn-1] = 0;
      printf("DATE %s\n", dummy);
      continue;
    }
    if(!strcmp(dummy, "OUTPUT_MASK")) {
      printf("%s", dummy);
      fgets(dummy, 255, ifp);
      nn = strlen(dummy);
      if(dummy[nn-1] == 10) dummy[nn-1] = 0;
      initoutputmask(dummy);
      continue;
    }
    else if(!strcmp(dummy, "SOURCE")) {
      initsourcepos();
      continue;
    }
    else if(!strcmp(dummy, "TARGET")) {
      inittargetsiz();
      continue;
    }
    else if(!strcmp(dummy, "BETA")) {
      initsourcebeta();
      continue;
    }
    else if(!strcmp(dummy, "RECOIL")) {
      initsourcedir();
      continue;
    }
    else if(!strcmp(dummy, "DISTFACTOR")) {
      if(fscanf(ifp, "%lf", &distfactor) != 1) {
        printf("Error reading DISTFACTOR\n");
        errexit("readFileHeaders");
      }
      printf("DISTFACTOR %f\n", distfactor);
      if(distfactor <= 0.) distfactor = 1.;
      continue;
    }
    else if(!strcmp(dummy, "ENERFACTOR")) {
      if(fscanf(ifp, "%lf", &enerfactor) != 1) {
        printf("Error reading ENERFACTOR\n");
        errexit("readFileHeaders");
      }
      printf("ENERFACTOR %f\n", distfactor);
      if(enerfactor <= 0.) enerfactor = 1.;
      continue;
    }
    else if(!strcmp(dummy, "GEOMETRY")) {
      dowhat = 1;
      initgeometry();
      continue;
    }
    else if(!strcmp(dummy, "GENERATOR")) {
      if(fscanf(ifp, "%d", &evgen) != 1) {
        printf("Error reading GENERATOR\n");
        errexit("readFileHeaders");
      }
      printf("%s %d\n", dummy, evgen);
      if(evgen) varmult = mult = 0;
      while(1) {
        if (fscanf(ifp, "%s", dummy) != 1) {
          printf("Error reading %s while skipping GENERATOR data\n", ifname);
          errexit("readFileHeaders");
        }
        if(!strcmp(dummy, "ENDGENERATOR")) {
          break;
        }
      }
      continue;
    }
    else if(!strcmp(dummy, "PREPROCESSED")) {
      // attention the (standard) readgamma[MAG]() work correctly only for data
      // preprocessed with -ot 0  (i.e. HEADERS 0 keyword should follow)
      // should make a better decoding of PREPROCESSED header and introduce a
      // readgammaP() (but how about ancillaries?)
      printf("\n%s\n", dummy);
      while(1) {
        if (!fgets(dummy, 255, ifp)) {
          printf("Error reading %s \n", ifname);
          errexit("readFileHeaders");
        }
        if(dummy[0] < ' ')
          continue;
        nn = strlen(dummy);
        if(dummy[nn-1] == 10) dummy[nn-1] = 0;
        printf("%s\n", dummy);
        if(!strcmp(dummy, "ENDPREPROCESSED") || !strcmp(dummy, "$")) {
          printf("==============> setting -exp\n");
          distmin = 0.;   // set it to -exp
          disterr = 0.;
          fwhm    = -15;
          fwhn    =  -5;
          break;
        }
      }
      if(!strcmp(dummy, "$")) {
        break;
      }
      continue;
    }
    else {
      printf("Unrecognized keyword: %s\n", dummy);
      continue;
    }

  }   // while

}

void
initgammas()
{
  int     ii, jj, ll, l1, l2;
  int    *pts;
  double  ee, ww;
	double  Emin, Emax;

  if(fscanf(ifp, "%d", &jj) != 1) {
    printf("Error reading # of gammas from %s\n", ifname); 
    errexit("initgammas");
  }

	if(jj > 0) {
		for(datmult = ii = 0; ii < jj; ii++) {
			if(fscanf(ifp, "%lf", &ee) != 1) {
        printf("Error reading gammas from %s\n", ifname);
        errexit("initgammas");
      }
      ee *= enerfactor;
			if(ee < minegam) continue;
			if(ee > maxegam) continue;
			dategam[datmult] = ee;
			datmult++;
		}
	}
  else if(jj < 0) {
    switch(jj) {
    case -2:
      if(fscanf(ifp, "%lf %lf", &Emin, &Emax) == 2) {
        Emin *= enerfactor;
        Emax *= enerfactor;
        printf("The energy spectrum is a flat distribution between %8.3f and %8.3f \n", Emin, Emax);
      }
      else {
        printf("Two limits for the flat distribution are needed!\n");
        errexit("initgammas");
      }
      break;
    case -1:
      printf("The energy spectrum is continuous\n");
      break;
    default:
      printf("Invalid value for the multiplicity\n");
      errexit("initgammas");
      break;
    }
		}
  else {
    printf("Error: multiplicity equal to 0!\n");
    errexit("initgammas");
  }
		
  memset(peaklut, 0, sizeof(int) * MAXEKEV);
  pts = specPointer(9, 0);
  for(ii = 0; ii < datmult; ii++) {
    ee = dategam[ii];
    dategam[ii] = ee;
    ww = peakwidth[(int)ee];
    l1 = MAX((int)(ee - ww), 0);
    l2 = MIN((int)(ee + ww), MAXEKEV - 1);
    for(ll = l1; ll <= l2; ll++) peaklut[ll] = ii + 1;  // LUT region marked with gamma# (+1 to be != 0)
    l1 = MAX((int)(l1 * dEgain), 0);
    l2 = MIN((int)(l2 * dEgain), SPECLEN - 1);
    for(ll = l1; ll <= l2; ll++) pts[ll] = 1000;        // (scaled) LUT reported in this spectrum
  }
  for(ii = 0; ii < datmult-1; ii++) {
  for(jj = ii+1; jj < datmult; jj++) {
    ee = dategam[ii] + dategam[jj];
    ww = peakwidth[(int)ee];
    l1 = MAX((int)(ee - ww), 0);
    l2 = MIN((int)(ee + ww), MAXEKEV - 1);
    l1 = MAX((int)(l1 * dEgain), 0);
    l2 = MIN((int)(l2 * dEgain), SPECLEN - 1);
    for(ll = l1; ll <= l2; ll++) pts[ll] += 100;        // LUT of pairwise summed energies
  }
  }
}

void
initparticles(char * part)
{
  int     ii, jj;
  double  ee;

  if(fscanf(ifp, "%d", &jj) != 1) {
    printf("Error reading # of %s from %s\n", part, ifname); 
    errexit("initparticles");
  }
  printf("%s %d\n", part, jj);

  if(jj > 0) {
		for(ii = 0; ii < jj; ii++) {
			if(fscanf(ifp, "%lf", &ee) != 1) {
        printf("Error reading %s from %s\n", part, ifname);
        errexit("initparticles");
      }
      ee *= enerfactor;
      printf("%f\n", ee);
		}
	}
}

void
initsourcebeta()
{
  double dummy;

  if(fscanf(ifp, "%lf", &dummy) != 1) {
    printf("Error reading recoil velocity from %s\n", ifname);
    errexit("initsourcebeta");
  }
  if(!recForced) {
    recoil.beta = dummy;
  }
  recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);

  if(recoil.beta) {
    printf("Recoil v/c %8.3f\n", recoil.beta);
  }
}

void
initsourcedir()
{
  double ww, dum1, dum2, dum3, dum4, dum5, dum6;

  if(fscanf(ifp, "%lf %lf %lf %lf %lf %lf ", &dum1, &dum2, &dum3, &dum4, &dum5, &dum6) != 6) {
    printf("Error reading recoil direction from %s\n", ifname);
    errexit("initsourcedir");
  }
  
	// leggo il valore di beta da dum1 (se non viene passata dalla riga di comando)
	if(!recForced) {
    recoil.beta = dum1;
  }
  recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);

  if(recoil.beta) {
    printf("Recoil v/c %8.3f\n", recoil.beta);
  }
  
	// leggo la direzione del rinculo (i tre coseni direttori) da dum3, dum4, dum5 
	// (se non viene passata dalla riga di comando)
	if(!recForced) {
    recoil.cx = dum3;
    recoil.cy = dum4;
    recoil.cz = dum5;
  }

  ww = sqrt(recoil.cx*recoil.cx + recoil.cy*recoil.cy + recoil.cz*recoil.cz);
  if(ww) {recoil.cx /= ww; recoil.cy /= ww; recoil.cz /= ww;}
  else   {recoil.cx = 0.0; recoil.cy = 0.0; recoil.cz = 1.0;}

  if(recoil.cx || recoil.cy)
    printf("Recoil direction theta & phi %8.3f %.3f deg\n",
            RAD2DEG*acos(recoil.cz), RAD2DEG*atan2(recoil.cy, recoil.cx));
  else
    printf("Recoil direction along z-axis\n");

}

void
initsourcepos()
{
  double dum1, dum2, dum3, dum4, dum5;
  
  switch (dataFileVersion) {
  case FMARS:
    if(fscanf(ifp, "%lf %lf %lf ", &dum3, &dum4, &dum5) != 3) {
      printf("Error reading source position from %s\n", ifname);
      errexit("initsourcepos");
    }
    break;
  case FAGATA:
    if(fscanf(ifp, "%lf %lf %lf %lf %lf ", &dum1, &dum2, &dum3, &dum4, &dum5) != 5) {
      printf("Error reading source position from %s\n", ifname);
      errexit("initsourcepos");
    }
    printf("Source type is %d\n", dum1);
    if(dum2) {
      printf("The source is moving during gamma emission\n");
    }
    break;
  default:
    printf("File format not recognized: %d\n", dataFileVersion);
    errexit("initsourcepos");
  }

  if(!posForced) {
    origin.xx = dum3*distfactor;
    origin.yy = dum4*distfactor;
    origin.zz = dum5*distfactor;
    origin.rr = sqrt(dum3*dum3 + dum4*dum4 + dum5*dum5)*distfactor;
  }

//  if(origin.xx || origin.yy || origin.zz)
	printf("Gammas emitted from %8.2f %8.2f %8.2f mm\n", origin.xx, origin.yy, origin.zz);

}

void
inittargetsiz()
{
  double dum1, dum2, dum3;
  
  if(fscanf(ifp, "%lf %lf %lf ", &dum1, &dum2, &dum3) != 3) {
    printf("Error reading source position from %s\n", ifname);
    errexit("inittargetsiz");
  }  
}

void
initoutputmask(char * mask)
{
  int  ii, ll;

// Enrico's definition of OUTPUT_MASK    0--> disabled; 1--> enabled
// 0  nDet 
// 1  Energy
// 2  AbsolutePosition(x y z)
// 3  RelativePosition(x' y' z') 
// 4  RelativePosition for mgs (x'' y'' z'')
// 5  nSeg
// 6  time
// 7  interaction
// 8  4 Sigma detector

  ll = strlen(mask);
  // trim leading blanks
  for(ii = 0; ii < ll; ii++) {
    if(mask[ii] != ' ') break;
  }

  //if(ll-ii != 8) {
  if(ll-ii != 9) {  // Changed by Marc for Sigma detector
    printf("Error decoding OUTPUT_MASK (%s)\n", mask);
    errexit("initoutputmask");
  }

  strcpy(outputmask, mask+ii);
}

void
smearsource()
{
  double oth, oph, xth, xph, xy;

  memcpy(&oorigin, &origin, sizeof(point));
  memcpy(&orecoil, &recoil, sizeof(pvect));

  origin.xx += ssDx * gaussrand();
  origin.yy += ssDy * gaussrand();
  origin.zz += ssDz * gaussrand();
  origin.rr  = sqrt(origin.xx*origin.xx + origin.yy*origin.yy + origin.zz*origin.zz);

  recoil.beta += ssDv * gaussrand();
	recoil.gamma = 1. / sqrt(1. - recoil.beta * recoil.beta);
  
  oth = RAD2DEG*acos(recoil.cz);
  oph = RAD2DEG*atan2(recoil.cy, recoil.cx);

  xth = oth + ssDth * gaussrand();
  xph = oph + ssDth * gaussrand();
  xth *= DEG2RAD;
  xph *= DEG2RAD;
  xy  = sin(xth);
  recoil.cx = xy * cos(xph);
  recoil.cy = xy * sin(xph);
  recoil.cz =      cos(xth);
}

void
projections()
{
  static int initit = 1;
  static double  *evect;
  int    ii, nn;
  double eseen, etot;
  
  if(ngedets <= 1) {
    for (etot = 0.0, ii = 0; ii < npev; ii++) {
      eseen = ppev[ii]->ee;
      specIncrGain(eseen, 3, 0);                //////// 3 = spettro dei punti dopo preprocessamento
      etot += eseen;
    }
    specIncrGain(etot/evmult, 4, 0);            //////// 4 = spettro totale scalato sulla molteplicita'
    return;
  }

  if(initit) {
    evect = (double *)calloc(ngedets, sizeof(double));
    initit = 0;
  }
  else {
    memset(evect, 0, ngedets * sizeof(double));
  }
  for (etot = 0.0, ii = 0; ii < npev; ii++) {
    eseen = ppev[ii]->ee;
    specIncrGain(eseen, 3, 0);                  //////// 2 = spettro dei punti dopo preprocessamento
    evect[ppev[ii]->nd] += eseen;  
    etot += eseen;
  }
  specIncrGain(etot/evmult, 4, 0);              //////// 4 = spettro totale (scalato sulla molteplicita' dell'evento)
  for(nn = 0; nn < ngedets; nn++) {
    if(evect[nn] > 0.) {
      eseen = evect[nn];
      specIncrGain(eseen, 5, 0);                //////// 5 = spettro singolo dei rivelatori
    }
  }

}

double
dist3Dp(point *p1, point *p2) 
{
  double xx, yy, zz;
  xx = p2->xx - p1->xx;
  yy = p2->yy - p1->yy;
  zz = p2->zz - p1->zz;
  return sqrt(xx*xx + yy*yy + zz*zz);
}

void
setcosdir(point *pp)
{
  double rr, xx, yy, zz;

  xx = pp->xx;
  yy = pp->yy;
  zz = pp->zz;
  rr = xx*xx + yy*yy + zz*zz;
  pp->rr = sqrt(rr);

  xx -= origin.xx;
  yy -= origin.yy;
  zz -= origin.zz;
  rr = xx*xx + yy*yy + zz*zz;;
  if(rr > 1.e-20) {
    rr = 1. / sqrt(rr);
    pp->cx = xx * rr;
    pp->cy = yy * rr;
    pp->cz = zz * rr;
  }
  else {
    pp->cx = pp->cy = 0.;
    pp->cz = 1;
  }
}

double
gaussrand()
{
  static int    initit = 1;         // indice nella sequenza
  static double gsrand[GAUSSDIM];   // una sequenza di numeri pseudorandom gauss-distribuiti attorno a zero, sigma=1
  int    ii;
  double r1, r2, rr;
  
  if(initit) {                      // inizializzazione   
    for (ii = 0; ii < GAUSSDIM; ii++) {
      do {
        do r1 = RAND(); while(!r1);
        r2 = RAND();
        rr = sqrt(-2.0 * log(r1) ) * cos( M_PI * r2 );
      } while (rr > 4.);  // to avoid (unlikely) outliers (4. should become a parameter)
      gsrand[ii] = rr;
    }
    initit = 0;
  }
  
  ii = rand()%GAUSSDIM;
  rr = gsrand[ii];
  
  return rr;
}

double
gaussres(double e)
{
  static double esigma[MAXEKEV];    // larghezza picco in funzione dell'energia
  static int    initit = 1;

  int ch;

  if(initit) {
    for (ch = 0; ch < MAXEKEV; ch++) {
      esigma[ch] = sqrt(pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * ch/1332.51);
    }
    initit = 0;
  }

  ch = (int)e;
  ch = (ch < 0) ? (0) : ( (ch < MAXEKEV) ? (ch) : (MAXEKEV - 1) );
  e += gaussrand() * esigma[ch];

  return e;
}

void
orderintarray(int *ia, int nn)
{
  int i1, i2, again, t;
  
  if(nn < 2) return;
  
  again = 1;
  i2    = nn;
  while(again) {
    again = 0;
    i2--;
    for(i1 = 0; i1 < i2; i1++) {
      if(ia[i1] > ia[i1+1]) {
        SWAP(ia[i1],ia[i1+1],t)
        again = 1;
      }
    }
  }
}

/*
double
costhrec(point *pp)
{
  point *po;
  double xx, yy, zz;

  po = getptogam(pp->ng);
  xx = pp->xx - po->xx;
  yy = pp->yy - po->yy;
  zz = pp->zz - po->zz;
  return (pp->zz - po->zz) / sqrt(xx*xx + yy*yy + zz*zz);
}
*/

double
costhrec(point *pp)
{
  double costh;

  costh = pp->cx*recoil.cx + pp->cy*recoil.cy + pp->cz*recoil.cz;
  costh = MIN(costh,  1.);
  costh = MAX(costh, -1.);
  return costh;
}

void
printchid(char *chid)
{
  int  ii, ll;
  char ch;
  
  ll = strlen(chid);
  ch = 0;
  for (ii = 0; ii < ll; ii++) {
    if(chid[ii] != ch) {ch = chid[ii]; printf("%c", ch);}
    printf("%c", chid[++ii]);
  }
}

void
printallpoints(point **pp, int nn)
{
  int     ii;
  point  *p1;
  double  et = 0.0;
  
  printf("\n");
  
  for (ii = 0; ii < nn; ii++) {
    p1 = pp[ii];
    printf("%4d %7.1f %7.1f %7.1f %7.1f %7.1f %7.1f %7.1f %3d  ",
      ii, p1->ee, p1->xx, p1->yy, p1->zz, p1->rr, DC2TH(p1), DC2PH(p1), p1->nd);
    printchid(p1->chid);
    printf("\n");
    et += p1->ee;
  }
}

void
analisi()
{
  int NP0, NP1, NTR, NEM, NEF;
  int    ii, jj, kk, j1, j2, l1, l2, l2m, nn, np;
  int   *pts;
  int    ind[20];                         // dimensione deve essere almeno NP1
  int    picco[20], fondo[20], total[20]; // dimensione deve essere almeno NP1
  double apeak[20], tpeak[20];            // dimensione deve essere almeno NP1
  double trackeff, xx;
  double egam, eaver;
  
  printf("\n");
  printf(" Energy");

  ind[0] =  0; printf("  Emitt");                 // emission spectrum
  ind[1] =  1; printf("  GEANT");                 // Geant spectrum
  NEM = 0;                                        // efficienza di tracking calcolata rispetto allo spettro GEANT
  NEF = 1;                                        // efficienza di tracking calcolata rispetto allo spettro GEANT
  NP0 = 2;                                        // numero di colonne definite finora
  if(ngedets > 1) {
    ind[NP0++] =  5; printf("  Proje");           // Spectrum of detectors (Preprocessed) 
  }

  if(doTracking) {
    NTR = NP0;                                    // la prossima e' la posizione della colonna di tracking
    ind[NP0++] = 30; printf(" Tracked Tr.Eff");   // Tracked and space for column with reconstruction efficiency
    ind[NP0++] = 31; printf("  Photo");           // Photo
    ind[NP0++] = 32; printf("  Compt");           // Compt
    ind[NP0++] = 33; printf("  PairP");           // Pair0
    ind[NP0++] = 10; printf("  ClusI");           // Cluster del pool iniziale
    ind[NP0++] = 36; printf("  ClusE");           // Cluster residui
    ind[NP0++] = 34; printf(" OneGam");           // tracked, points in only one gamma
  //ind[NP0++] = 35; printf(" Chi2TM");           // Best 10% of tracked
  //ind[NP0++] = 50; printf("  Clus0");           // Cluster accettati tipo 000
  //ind[NP0++] = 51; printf("  Clus1");           // Cluster accettati tipo 100
  //ind[NP0++] = 40; printf("  ClusF");           // Cluster da "CHI2FUDGE"
    NP1 = NP0;                                    // numero di colonne totali 
    for (ii = 2; ii <= MAXACCEPT; ii++) {         // per considerare anche le ultime che sono quelle
      ind[NP1++] = 40 + ii;                       // scomposte per numero di punti
      printf("    M%d", ii);
    }
  }
  else {
    NP1 = NP0;                                    // numero di colonne totali
    NTR = NP1+1;                                  // posizione della colonna di tracking == not present
  }
  
  printf("\n");
  
  for(kk = 0; kk < NP1; kk++) {
    apeak[kk] = tpeak[kk] = 0.0;
  }
  np = 0;
  trackeff = eaver = 0.0;
  ii = l2m = 0;
  while(ii < MAXEKEV) {
    if(peaklut[ii]) {
      j1 = jj = ii;
      while(jj < MAXEKEV) {
        if(peaklut[jj]) {
          jj++;
        }
        else {
          np++;
          j2 = jj - 1;
          egam = (j1 + j2)/2.0;
          printf("%7.1f", egam); // energy of peak
          l1 = (int)(j1 * dEgain);
          l2 = (int)(j2 * dEgain);
          l2m = MAX(l2, l2m);
          nn = l2 - l1 + 1;
          for(kk = 0; kk < NP1; kk++) {
            pts = specPointer(ind[kk], 0);
            picco[kk] = 0;
            fondo[kk] = 0;
            for (jj = 0; jj < nn; jj++) {
              picco[kk] += pts[l1 + jj];
              fondo[kk] += pts[l2 + jj + 3] + pts[l1 - jj - 3];
            }
            apeak[kk]  = MAX(0.0, picco[kk] - fondo[kk]/2.0);
            tpeak[kk] += apeak[kk];
            if(kk <  NP0)  printf("%7d", (int)apeak[kk]);
            if(kk == NTR) {xx = (apeak[NEF] > 0.) ? (apeak[NTR]/apeak[NEF]) : (0.); printf("%8.3f", xx); trackeff += xx;}
            if(kk >= NP0)  printf("%6d", (int)apeak[kk]);
          }
          eaver += egam*apeak[NEM];
          printf("\n");
          ii = j2 + 1;
          break;
        }
      }
    }
    else ii++;
  }
  if(!np) return;

  for(kk = 0; kk < NP1; kk++) {
    pts = specPointer(ind[kk], 0);
    for(total[kk] = 0, jj = 1; jj < SPECLEN; total[kk] += pts[jj++]);
  }
  
  printf("\n");
  if(np > 1) {
    eaver /= tpeak[NEM];
    printf("<%4d.>", (int)(eaver+0.5));
    for(kk = 0; kk < NP1; kk++) {
      if(kk <  NP0) printf("%7d", (int)(tpeak[kk]/(double)np));
      if(kk == NTR) printf("%8.3f", trackeff/np);
      if(kk >= NP0) printf("%6d", (int)(tpeak[kk]/(double)np));
    }
    printf("\n");
  }
  
  printf("  P/T  ");
  for(kk = 0; kk < NP1; kk ++) {
    if(kk <  NP0) printf("%7.3f", (total[kk]) ? (tpeak[kk]/(double)total[kk]) : (0.0) );
    if(kk == NTR) printf("        ");
    if(kk >= NP0) printf("%6.3f", (total[kk]) ? (tpeak[kk]/(double)total[kk]) : (0.0) );
  }
  printf("\n");
  
  printf("\n");
  printf("TotArea");
  for(kk = 0; kk < NP1; kk++) {
    if(kk <  NP0) printf("%7d", total[kk]);
    if(kk == NTR) printf("        ");
    if(kk >= NP0) printf("%6d", total[kk]);
  }
  printf("\n");
  
  printf("TotPeak");
  for(kk = 0; kk < NP1; kk++) {
    if(kk <  NP0) printf("%7d", (int)tpeak[kk]);
    if(kk == NTR) printf("        ");
    if(kk >= NP0) printf("%6d", (int)tpeak[kk]);
  }
  printf("\n");
  
  printf("Pileup ");
  l2m++;
  for(kk = 0; kk < NP1; kk++) {
    pts = specPointer(ind[kk], 0);
    for(j1 = 0, jj = l2m; jj < SPECLEN; j1 += pts[jj++]);
    if(kk <  NP0) printf("%7d", j1);
    if(kk == NTR) printf("        ");
    if(kk >= NP0) printf("%6d", j1);
  }
  printf("\n");
}

void
initgeometry()
{
  int    ii, nn;
  char   geotype[100];

  memset(geotype, 0, sizeof(geotype));

  if (fscanf(ifp, "%s", geotype) != 1) {
    printf("Error reading %s\n", ifname);
    errexit("initgeometry");
  }

  nn = strlen(geotype);
  for(ii = 0; ii < nn; ii++)
    geotype[ii] = toupper(geotype[ii]);

  if(!strcmp(geotype, "NONE")) {
    geominit_none(1);
    GEOMETRY = NONE;
  }
  else if(!strcmp(geotype, "SHELL")) {
    geominit_shell();
    GEOMETRY = SHELL;
  }
  else if(!strcmp(geotype, "CBAR")) {
    geominit_cbar();
    GEOMETRY = CBAR;
  }
  else if(!strcmp(geotype, "HBAR")) {
    geominit_hbar();
    GEOMETRY = HBAR;
  }
  else if(!strcmp(geotype, "BOX3")) {
    geominit_box3();
    GEOMETRY = BOX3;
  }
  else if(!strcmp(geotype, "MXXX")) {
    geominit_mxxx();
    GEOMETRY = MXXX;
  }
  else if(!strcmp(geotype, "AGATA")) {
    geominit_agata();
  }
  else {
    printf("\nPlease select a valid geometry among {NONE SHELL CBAR HBAR BOX3 MXXX AGATA}\n");
    errexit("initgeometry");
  }

  return;
}

#if STATINSIDEDET == 1
void
statinsidedet(point *pp)
{
  static int     initit = 1;
  static int    *npnt;
  static int    *nout;
  static double *xmin, *xmax;
  static double *ymin, *ymax;
  static double *zmin, *zmax;
  static double *xdet, *ydet, *zdet, *dist;
  static double  rmin, rmax;

  int    mm, nn, kk, ll;
  double px, py, pz;
  double dx, dy, dz;
  double Dx, Dy, Dz, Lx, Ly, Lz;
  double dd;
  int    maxdet, nnmax, ndet, npout;

  if(initit) {
    npnt = (int    *)calloc(ngedets, sizeof(int));
    nout = (int    *)calloc(ngedets, sizeof(int));
    xmin = (double *)calloc(ngedets, sizeof(double));
    xmax = (double *)calloc(ngedets, sizeof(double));
    ymin = (double *)calloc(ngedets, sizeof(double));
    ymax = (double *)calloc(ngedets, sizeof(double));
    zmin = (double *)calloc(ngedets, sizeof(double));
    zmax = (double *)calloc(ngedets, sizeof(double));
    xdet = (double *)calloc(ngedets, sizeof(double));
    ydet = (double *)calloc(ngedets, sizeof(double));
    zdet = (double *)calloc(ngedets, sizeof(double));
    dist = (double *)calloc(ngedets, sizeof(double));
    rmin = 10000.;
    rmax = 0.;
    initit = 0;
  }

  if(pp != NULL) {
    nn = pp->nd;
    if(nn < 0) return;
    dd   = dist3Dp(pp, &origin);
    rmin = MIN(rmin, dd);
    rmax = MAX(rmax, dd);
    if(npnt[nn] == 0) {
      xmin[nn] = pp->xx;
      xmax[nn] = pp->xx;
      ymin[nn] = pp->yy;
      ymax[nn] = pp->yy;
      zmin[nn] = pp->zz;
      zmax[nn] = pp->zz;
    }
    else {
      xmin[nn] = MIN(xmin[nn], pp->xx);
      xmax[nn] = MAX(xmax[nn], pp->xx);
      ymin[nn] = MIN(ymin[nn], pp->yy);
      ymax[nn] = MAX(ymax[nn], pp->yy);
      zmin[nn] = MIN(zmin[nn], pp->zz);
      zmax[nn] = MAX(zmax[nn], pp->zz);
    }
    if(insidegedet(pp) != 1) nout[nn]++;
    npnt[nn]++;
    return;
  }

// print results

  for(ndet = maxdet = nn = 0; nn < ngedets; nn++) {
    if(npnt[nn]) {
      maxdet = nn;
      ndet++;
    }
  }
  if(ndet == 0) return;

  printf("\n Statistics of points positions of the %d seen detector(s)\n", ndet);
  printf("\n  #  points  <X>     <Y>     <Z>     <phi>       Xmin   Xmax   Dx        Ymin   Ymax   Dy        Zmin   Zmax   Dz \n");
  npout = 0;
  Dx = Dy = Dz = Lx = Ly = Lz = 0.0;
  for(nn = 0; nn <= maxdet; nn++) {
    if(!npnt[nn]) continue;
    xdet[nn] = (xmax[nn]+xmin[nn])/2.;
    ydet[nn] = (ymax[nn]+ymin[nn])/2.;
    zdet[nn] = (zmax[nn]+zmin[nn])/2.;
    printf("%3d %6d %7.2f %7.2f %7.2f %8.3f  x[%6.1f %6.1f %6.2f] y[%6.1f %6.1f %6.2f] z[%6.1f %6.1f %6.2f]\n",
          nn, npnt[nn],
          xdet[nn], ydet[nn], zdet[nn], RAD2DEG*atan2(ydet[nn], xdet[nn]),
          xmin[nn], xmax[nn], xmax[nn]-xmin[nn],
          ymin[nn], ymax[nn], ymax[nn]-ymin[nn],
          zmin[nn], zmax[nn], zmax[nn]-zmin[nn] );
    Lx += xmax[nn]-xmin[nn];
    Ly += ymax[nn]-ymin[nn];
    Lz += zmax[nn]-zmin[nn];
    px = CDet[nn].xx;
    py = CDet[nn].yy;
    pz = CDet[nn].zz;
    dx = px - xdet[nn];
    dy = py - ydet[nn];
    dz = pz - zdet[nn];
    printf("           %7.2f %7.2f %7.2f %8.3f\n",         px,py,pz,RAD2DEG*atan2(py,px) );
    printf("    %6d %7.2f %7.2f %7.2f %8.3f\n\n", nout[nn],dx,dy,dz,RAD2DEG*fabs(atan2(ydet[nn],xdet[nn])-atan2(py,px)));
    Dx += dx * dx;
    Dy += dy * dy;
    Dz += dz * dz;
    npout += nout[nn];
  }
  printf("<D> %6d %7.2f %7.2f %7.2f", npout, sqrt(Dx)/ndet, sqrt(Dy)/ndet, sqrt(Dz)/ndet);
  printf("                          %7.2f                 %7.2f                 %7.2f\n",Lx/ndet, Ly/ndet, Lz/ndet);

  if(ndet > 1) {
    nnmax = MIN(ndet - 1, 10);
    printf("\n The first %d neighbours of each detector\n", nnmax);
    for(mm = 0; mm <= maxdet; mm++) {
      if(!npnt[mm]) continue;
      for(nn = 0; nn <= maxdet; nn++) {
        dist[nn] = 0;
        if(!npnt[nn]) continue;
        if(nn == mm) continue;
        dist[nn] = sqrt(pow(xdet[nn]-xdet[mm], 2.) + pow(ydet[nn]-ydet[mm], 2.) + pow(zdet[nn]-zdet[mm], 2.));
      }
      printf("\n%3d ", mm);
      ll = 0;
      while(ll < nnmax) {
        dd = 1.E20;
        kk = -1;
        for(nn = 0; nn <= maxdet; nn++) {
          if(!npnt[nn]) continue;
          if(nn == mm) continue;
          if(dist[nn] > 0. && dist[nn] < dd) {
            dd = dist[nn];
            kk = nn;
          }
        }
        if(kk < 0) break;
        printf("  %3d %4.1f", kk, dd);
        dist[kk] = 0.0;
        ll++;
      }
    }
  }
  printf("\n\n");

  printf(" Inner radius is %7.2f mm\n", rmin);
  printf(" Outer radius is %7.2f mm\n", rmax); 
}
#endif      //    #if STATINSIDEDET == 1
