#include "mgt.h"

////////////////////////////////////////////////////////////////////////////////////////
// Per rappresentare i cluster in modo intuitivo viene usata una stringa di caratteri //
// di lungezza pari al numeri di punti dell'evento (+1 per lo 0 finale).              //
// Il carattere '0' (ASCII 48) e il carattere '1' (ASCII 49) nella locazione          //
// n-sima indicano se il punto n-simo dell'evento appartiene ('1') o no ('0')         //
// al cluster rappresentato dalla stringa.                                            //
////////////////////////////////////////////////////////////////////////////////////////

void  distmatset    (int);                    // setup pointers to used distance metric
void  clumatlink    (double);                 // clusterization matrix with Link   algorithm
void  clumatlead    (double);                 // clusterization matrix with Leader algorithm
void  cluMatLink    (double, double);         // all linked clusters from min to max opening
void  cluMatLead    (double, double);         // all leaded clusters from min to max opening
void  cluMatGood    ();                       // clusterization of all gammas (only for test)
void  rowLinkPoints (int, double);            // link all close points
void  rowLeadPoints (int, int, double);       // lead close points
int   rowIsPresent  (int);                    // check if row already present
void  printmatclust (int);                    // print clusterization matrix
int   poolPresent   (char  *, int);           // check if cluster already present in clusterspool or clustersdone
void  poolCluMat    (int, int, int);          // generate clusters from clusterization matrix

char  cstring[MAXEVLEN + 2];
int   ctag, cdepth;

void
initclust()
{
}

void
finitclust()
{
}

void
distmatmake(point **pp, int nn)
{
  int     ii, jj;
  point  *ipp, *jpp;
  double  xx;
//int     iie, jje;
//double  iix, jjx, iid, jjd;

  for (ii = 0; ii < nn; ii++) {
    ipp = pp[ii];
    distmat_th[ii][ii] = 0.0;
    distmat_dd[ii][ii] = 0.0;
    distmat_ge[ii][ii] = gedistance(&origin, ipp);
//  distmat_xx[ii][ii] = 0.0;
    for (jj = ii + 1; jj < nn; jj++) {
      jpp = pp[jj];

      xx = RAD2DEG * acos(ipp->cx*jpp->cx + ipp->cy*jpp->cy + ipp->cz*jpp->cz);
      distmat_th[ii][jj] = distmat_th[jj][ii] = xx;
      specIncr((int)(10 * xx),          13, 0);     // angolo al centro tra i punti dell'evento

      xx = dist3Dp(ipp, jpp);
      distmat_dd[ii][jj] = distmat_dd[jj][ii] = xx;
      specIncr((int)xx + 2000  , 13, 0);            // distanza tra i punti dell'evento (mm)

      xx = gedistance(ipp, jpp);
      distmat_ge[ii][jj] = distmat_ge[jj][ii] = xx;
      specIncr((int)xx + 5000  , 13, 0);            // distanza in germanio tra i punti dell'evento
/*
      iie = (int)ipp->ee;
      jje = (int)jpp->ee;

      iix = distmat_ge[ii][jj] * mutot[jje];
      iix = MIN(20., iix);                      // to avoid underflow in exp(-xx)
      iid = 1. - exp(-iix);
      distmat_xx[ii][jj] = iid;

      jjx = distmat_ge[jj][ii] * mutot[iie];
      jjx = MIN(20., jjx);                      // to avoid underflow in exp(-xx)
      jjd = 1. - exp(-jjx);
      distmat_xx[jj][ii] = jjd;
*/
    }             
  }
}

void
distmatprint(int type)
{
  int ii, jj;
  
  printf("\n      ");
  for (ii = 0; ii < npev ; ii++) printf("%3d", ii);
  printf("\n\n");
  
  if(type == 0) {
    for (ii = 0; ii < npev ; ii++) {
      printf("%3d   ", ii);
      for (jj = 0; jj < ii; jj++)        printf("%3d", (int)(distmat_dd[ii][jj]) );
      printf("   ");
      for (jj = ii + 1; jj < npev; jj++) printf("%3d", (int)(distmat_th[ii][jj]) );
      printf("\n");
    }
  }
  else {
    for (ii = 0; ii < npev ; ii++) {
      printf("%3d   ", ii);
      for (jj = 0; jj <= ii; jj++)       printf("%3d", (int)(distmat_ge[ii][jj]) );
      printf("   %3d", (int)(10. * distmat_ge[ii][ii]) );
      printf("\n");
    }
  }
/*
  printf("\n      ");
  for (ii = 0; ii < npev ; ii++) printf("%3d", ii);
  printf("\n\n");
  for (ii = 0; ii < npev ; ii++) {
    printf("%3d   ", ii);
    for (jj = 0; jj < npev; jj++) printf("%3d", (int)(10. * distmat_xx[ii][jj]) );
    printf("\n");
  }
*/
  
}

void
distmatset(int tipo)
{
  int ii;

  if(tipo == DISTANGULAR) {
    for(ii = 0; ii < npev; ii++) distmat[ii] = distmat_th[ii];
    cluDMin = cluAngMin;
    cluDMax = cluAngMax;
  }
  else if(tipo == DISTEUCLIDEAN) {
    for(ii = 0; ii < npev; ii++) distmat[ii] = distmat_dd[ii];
    cluDMin = cluDistMin;
    cluDMax = cluDistMax;
  }
  else {
    printf("Unknown distance metric (%d)\n", tipo);
    errexit("distmatset");
  }
}

void
clumatlink(double thclust)
{
  int    ii, jj, ntoset, nfound;
  int    first, last, more;
  double th;
  char  *pstring;

  memset(cstring, '1', npev);
  cstring[npev] = 0;
  memset(matnbits, 0, sizeof(int) * npev);
  ntoset = npev;

  for (ii = 0; ii < npev ; ii++) {
    matclust[ii][npev] = 0;
    if(ppev[ii]->clu >= 0) {cstring[ii] = '0'; ntoset--;}
  }

  matnrows = 0;
  first = 0;
  last  = npev - 1;
  for(; first <= last; first++) {
    if(cstring[first] < '1') continue;
    pstring = matclust[matnrows];
    memset(pstring, '0', npev);
    pstring[npev]  = 0;
    pstring[first] = '1'; nfound = 1;
    cstring[first] = '0'; ntoset--;
    for(; last >= first; last--) if(cstring[last] > '0') break;
    do {
      more = 0;
      for(ii = first; ii <= last; ii++) {
        if(pstring[ii] != '1') continue;
        for(jj = first; jj <= last; jj++) {
          if(cstring[jj] < '1' ) continue;
          th = distmat[ii][jj];
          if(th <= thclust) {
            pstring[jj] = '1'; nfound++;
            cstring[jj] = '0'; ntoset--;
            more++;
          }
        }
        pstring[ii] = '2';
      }
    } while (more);
    matnbits[matnrows] = nfound;
    matnrows++;
    for(ii = first; ii <= last; ii++) if(pstring[ii] > '1') pstring[ii] = '1';
    if(!ntoset) break;
  }
}

void
clumatlead(double thclust)
{
  int    ii, jj;
  double th;

  for (ii = 0; ii < npev ; ii++) {
    memset(matclust[ii], '0', npev);
    matclust[ii][npev] = 0;
    if(ppev[ii]->clu < 0) {
      matclust[ii][ii] = '1';
      matnbits[ii] = 1;
    }
    else matnbits[ii] = 0;
  }
  
  for (ii = 0; ii < npev - 1 ; ii++) {
    if(ppev[ii]->clu >= 0) continue;
    for (jj = ii + 1; jj < npev; jj++) {
      if(ppev[jj]->clu >= 0) continue;
      th = distmat[ii][jj];
      if(th < thclust) {
        matclust[ii][jj] = '1'; matnbits[ii]++;
        matclust[jj][ii] = '1'; matnbits[jj]++;
      }
    }
  }
  matnrows = npev;
}

void
cluMatLink(double dMin, double dMax)
{
  int    ii, jj, nn, imin;
  double dd, dmin;
  char  *rnext;

  matnrows = 0;
  for(nn = 0; nn < npev; nn++) {
    rnext = matclust[matnrows];           // new seed
    memset(rnext, '0', npev);
    rnext[nn]   = '1';
    rnext[npev] = 0;
    matnbits[matnrows] = 1;
    if(dMin > 0.)                         // complete it if needed
      rowLinkPoints(matnrows, dMin);
    if(rowIsPresent(matnrows)) continue;  // link partition is unique ==> rest already present
    matcdist[matnrows] = dMin;
    matnrows++;
    while(1) {
      rnext = matclust[matnrows];         // using last row as starting point
      memcpy(rnext, matclust[matnrows-1], npev+1);
      matnbits[matnrows] = matnbits[matnrows-1];
      dmin = 2*dMax;                      // look for closest point to any of present points
      imin = -1;
      for(ii = 0; ii < npev; ii++) {
        if(rnext[ii] == '0') continue;
        for(jj = 0; jj < npev; jj++) {
          if(rnext[jj] != '0') continue;
          dd = distmat[ii][jj];
          if(dd < dmin) {
            dmin = dd;
            imin = jj;
          }
        }
      }
      if(dmin > dMax || imin == -1) break;
      rnext[imin] = '1';
      matnbits[matnrows]++;
      if(matnbits[matnrows] > MAXPOINTS) break;
      rowLinkPoints(matnrows, dMin);      // and complete it
      if(rowIsPresent(matnrows)) break;   // the rest is already present
      matcdist[matnrows] = dmin;
      matnrows++;
    }
  }
}

void
cluMatLead(double dMin, double dMax)
{
  int    jj, nn, imin;
  double dd, dmin;
  char  *rnext;

  matnrows = 0;
  for(nn = 0; nn < npev; nn++) {
    rnext = matclust[matnrows];           // new seed
    memset(rnext, '0', npev);
    rnext[nn]   = '1';
    rnext[npev] = 0;
    matnbits[matnrows] = 1;
    if(dMin > 0.) {                       // complete it if needed
      for(jj = 0; jj < npev; jj++) {
        if(jj == nn) continue;
        if(distmat[nn][jj] < dMin) {
          rnext[jj] = '1';
          matnbits[matnrows]++;
        }
      }
    }
    matcdist[matnrows] = dMin;
    if(!rowIsPresent(matnrows)) {         // add it to list
      matnrows++;
      rnext = matclust[matnrows];         // and prepare next row
      memcpy(rnext, matclust[matnrows-1], npev+1);
      matnbits[matnrows] = matnbits[matnrows-1];
    }
    while(1) {
      dmin = 2*dMax;     // using last row as starting point, look for closest point
      imin = -1;
      for(jj = 0; jj < npev; jj++) {
        if(rnext[jj] != '0') continue;
        dd = distmat[nn][jj];
        if(dd < dmin) {
          dmin = dd;
          imin = jj;
        }
      }
      if(dmin > dMax || imin == -1) break;
      rnext[imin] = '1';
      matnbits[matnrows]++;
      matcdist[matnrows] = dmin;
      if(matnbits[matnrows] > MAXPOINTS) break;
      if(!rowIsPresent(matnrows)) {         // add it to list
        matnrows++;
        rnext = matclust[matnrows];         // and prepare next row
        memcpy(rnext, matclust[matnrows-1], npev+1);
        matnbits[matnrows] = matnbits[matnrows-1];
      }
    }
  }
}

void
rowLinkPoints(int nn, double dist)
{
  int ii, jj, np, again;
  char *rtest;

  np = matnbits[nn];
  rtest = matclust[nn];
  do {
    again = 0;
    for(ii = 0; ii < npev; ii++) {
      if(rtest[ii] == '1') {                    // present but possibly not complete
        for(jj = 0; jj < npev; jj++) {          // add all its neighbours
          if(jj == ii) continue;
          if(rtest[jj] != '0') continue;        // already present
          if(distmat[ii][jj] > dist) continue;  // too far away
          rtest[jj] = '1';
          np++;
          again = 1;
        }
        rtest[ii] = '2';                          // mark it as complete
      }
    }
  } while(again);
  matnbits[nn] = np;
  for(ii = 0; ii < npev; ii++) {
    if(rtest[ii] > '1')
      rtest[ii] = '1';
  }
}

void
rowLeadPoints(int nn, int ii, double dist)
{
  int   jj, np;
  char *rtest;
  
  np = matnbits[nn];
  rtest = matclust[nn];
  for(jj = 0; jj < npev; jj++) {          // add all its neighbours
    if(jj == ii) continue;
    if(rtest[jj] != '0') continue;        // already present
    if(distmat[ii][jj] > dist) continue;  // too far away
    rtest[jj] = '1';
    np++;
  }
  matnbits[nn] = np;
}

int
rowIsPresent(int nn)
{
  int ii, np;
  char *rtest;

  np = matnbits[nn];
  rtest = matclust[nn];
  for(ii = 0; ii < matnrows; ii++) {
    if(matnbits[ii] == np) {
      if(!strcmp(rtest, matclust[ii])) {
        return 1;
      }
    }
  }
  return 0;
}

void
cluMatGood()
{
  int    ii, jj;
  point *pp;
  char  *chid;
  
  for (ii = 0; ii < npev ; ii++) {
    memset(matclust[ii], '0', npev);
    matclust[ii][npev] = 0;
    matnbits[ii] = 0;
  }
  
  matnrows = 0;
  for (ii = 0; ii < evmult ; ii++) {
    for(jj = 0; jj < npev; jj++) {
      pp = ppev[jj];
      if(pp->clu >= 0) continue;
      chid = pp->chid;
      for(;;chid += 2) {
        if(*chid == 0) break;
        if(*chid == 'A'+ii) {
          matclust[matnrows][jj] = '1';
          matnbits[matnrows]++;
          break;
        }
      }
    }
    if(matnbits[matnrows]) matnrows++;
  }
}

void
printmatclust(int nc)
{
  int    ii, jj;
  double eclust;
  
  printf("\n       ");
  for (ii = 0; ii < npev ; ii++) printf("%1d", ii%10);
  printf("\n       ");
  for (ii = 0; ii < npev ; ii++) {
    if(ii%10 != 0) printf(" ");
    else           printf("%1d",ii/10);
  }
  printf("\n");
  for (ii = 0; ii < nc; ii++) {
    if(matnbits[ii]) {
      eclust = 0.0; 
      for(jj = 0; jj < npev; jj++) if(matclust[ii][jj] > '0') eclust += ppev[jj]->ee;
      printf("%3d %2d %s %6.1f\n", ii, matnbits[ii], matclust[ii], eclust);
    }
  }
}

void
poolGood()
{  
  cluMatGood();
  if(llist > 2) printmatclust(matnrows);
  poolCluMat(matnrows, 0, 1);
}

void
poolLinkedS(int metric)
{
  int clutype;
  double dd;

  distmatset(metric);

  // e' piu' produttivo partire dall'alto !!! ( effetto di ->tag e X2Ftype in testcomptonC ??)

  for(dd = cluDMax; dd >= cluDMin; dd *= 0.8) {
    if(metric == DISTANGULAR)
      clutype = (dd > 9.) ? (0) : (100);
    else
      clutype = 0;
    clumatlink(dd);
    if(llist > 2) printmatclust(matnrows);
    poolCluMat(matnrows, clutype, 0);
  }
}

void
poolLeaderS(int metric)
{
  int    cPh, cPP;
  double dd;

  distmatset(metric);

  cPh = checkPh;
  cPP = checkPP;

  checkPh = 0;                        // disallow checking for photoeffect
  checkPP = 0;                        // disallow checking for pair production

  dd = cluDMax;
  do {                                // this way, to be sure to execute at least once
    clumatlead(dd);
    if(llist > 2) printmatclust(matnrows);
    poolCluMat(matnrows, 100, 0);
    dd *= 0.8;
  } while (dd >  cluDMin);

  checkPh = cPh;                      // back to default
  checkPP = cPP;                      // back to default
}

void
poolLinkedC(int metric)
{
  int   ii, clutype;
  clust *pcl;
  int keep = 0;

  distmatset(metric);

  cluMatLink(cluDMin, cluDMax);
  if(llist > 2) printmatclust(matnrows);

  for (ii = 0; ii < matnrows; ii++) {
    if(matnbits[ii] == 0) continue;
    if(matnbits[ii] > MAXPOINTS) continue;
    if(poolPresent(matclust[ii], matnbits[ii])) continue;
    pcl = clusterspool + nclupool;
    strcpy(pcl->cstring, matclust[ii]);
    if(metric == DISTANGULAR)
      clutype = (matcdist[ii] > 9.) ? (0) : (100);
    else
      clutype = 0;
    pcl->tag     = ii + clutype;
    pcl->npoints = matnbits[ii];
    trackCluster(pcl);
    if( keep ||
       (vertexStyle == VCHISQUARE  && pcl->chibest < chi2Track) ||
       (vertexStyle == VLIKELIHOOD && pcl->chibest > chi2Track) )
      nclupool++;
  }

}

/*
void
poolLinkedC(int metric)
{
  distmatset(metric);

  cluMatLink(cluDMin, cluDMax);
  if(llist > 2) printmatclust(matnrows);

  poolCluMat(matnrows, 0, 0);

}
*/

void
poolLeaderC(int metric)
{
  distmatset(metric);

  cluMatLead(cluDMin, cluDMax);
  if(llist > 2) printmatclust(matnrows);

  poolCluMat(matnrows, 100, 0);

}

void
poolCluMat(int nc, int clutype, int keep)
{
  int   ii;
  clust *pcl;

  for (ii = 0; ii < nc; ii++) {
    if(matnbits[ii] == 0) continue;
    if(matnbits[ii] > MAXPOINTS) continue;
    if(poolPresent(matclust[ii], matnbits[ii])) continue;
    pcl = clusterspool + nclupool;
    strcpy(pcl->cstring, matclust[ii]);
    pcl->tag     = ii + clutype;
    pcl->npoints = matnbits[ii];
    trackCluster(pcl);
    if( keep ||
       (vertexStyle == VCHISQUARE  && pcl->chibest < chi2Track) ||
       (vertexStyle == VLIKELIHOOD && pcl->chibest > chi2Track) )
      nclupool++;
  }
}

int
poolPresent(char *pch, int npoints)
{
  int ii;
  clust *pcl;

  pcl  = clusterspool; 
  for (ii = 0; ii < nclupool; ii++, pcl++) {
    if(npoints == pcl->npoints) {
      if(strcmp(pch, pcl->cstring) == 0) return 1;
    }
  }

  if(!ncludone) return 0;

  pcl  = clustersdone; 
  for (ii = 0; ii < ncludone; ii++, pcl++) {
    if(npoints == pcl->npoints) {
      if(strcmp(pch, pcl->cstring) == 0) return 1;
    }
  }
  return 0;
}
