///////////////////////////////////////////////////////////////////
//*-- AUTHOR : Marc Labiche    marc.labiche@stfc.ac.uk
//*-- Date: 11/2019
//*-- Last Update: 
//*-- Copyright:
//
// --------------------------------------------------------------
//
// analysOFTevent4G.C
//Analyse OFT event with multi-gating
//
// -------------------------------------------------------------- 
// --------------------------------------------------------------
// How to run this program:
//
// 1 - Run the simulation in the directory trunk/macros:
//      ./buid/Agata ...
//
// 2 - Run the OFT++ code to convert agata output to root and tracked data

// 3 - Run the analysEvent macros to perform gamma-gates
//      .L analysEvent4G.C
//      analysEvent4G()
//
//  the number within brackets means the analysis type. See the code
//  for the description of each analysis type.
//
// --------------------------------------------------------------
/////////////////////////////////////////////////////////////////


#include "TFile.h"
#include "TTree.h"
#include "TBrowser.h"
#include "TH1.h"
#include "TH2.h"
#include "TRandom.h"
//#include "TRandom3.h"
#include "TVector3.h"
#include "TMath.h"


void AnalysOFTEvent4G() {

  /*
    struct AGDATA{
       int AGevent;
       int AGcrystMult;
       int  AGcrystalId; //
       float AGenergy;
       float AGenergyCoreDop;
    };
    AGDATA agata;
  */

	typedef struct {
		int event;
		float TBeta;
		float TX1;
		float TY1;
		float TZ1;
		float TELabDopTracked;
		int TMult;
	}TreeDATA;

	static TreeDATA agata;



  //
  // Function containing the event loop
  //

  //gROOT->Reset();
  //gROOT->SetStyle("Default");

  gStyle->SetOptTitle(0);
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(0);
  
      // -----   For x-gate combination amongst the 21 gamma-ray   -------------------------------------------
      Double_t TSD1[21]={724,767,802,842,875,902,933,973,1018,1064,1110,1156,1205,1256,1310,1369,1430,1491,1562,1625,1702};
      Int_t CombCounter=0;
      Int_t RandIndex1, RandIndex2, RandIndex3, RandIndex4;

      Int_t TSD1Len=sizeof(TSD1)/sizeof(Double_t);  // length od TSD1 array

      Bool_t CombLock[21][21][21][21]={false}; // 4-gamma gate combination open (false) or locked (true)
      Bool_t flag_NewComb=false;   


  // -----------------------------------------------------------------------------------------------------


    Double_t AGsigma; // sigma intrinsic resolution in keV 
    Double_t AGsigmaCore, AGsigmaSeg; // sigma resolution at Core and segment level in keV 


  //-------    opening the simulated File and Tree     ----------//

  TFile *file1 = TFile::Open("/run/media/local1/data/JoseJavier/OFToutput_GSBSD1SD3_1e8events_tree0.root");
  // if only one input file:
  TTree* T = (TTree*)file1->Get("OFTTree");  // OFTTree is the name of the Tree
   T->SetBranchAddress("OFT",&agata);
    
  //output File and Tree for the analysis result
  TFile* outFile = new TFile("/run/media/local1/data/JoseJavier/AnalysOFTOutput.root","recreate");  
  //TFile outFile("AnalysOutput.root","recreate");  
  //outFile.cd();

  // histograms:
   TH1F *EneGate4Dop= new TH1F("EneGate4Dop", "spectrum Gated on energy 4 gamma after Doppler at core level", 3000, 0., 3000.);
 
// 
  Int_t GamMult=0; // multiplicity in tracker

  Double_t Egam1,Egam2, Egam3, Egam4;
  Double_t EgamGate[4]; // 4 if four gates
 
  //if one input trees
  Long64_t nevents = T->GetEntries();
  cout << "nevents: " << nevents << endl;

  Int_t nb = 0;

  Int_t nGateCounter=0;
 
  //  Double_t E[20];
  //  Bool_t EGate[20];
  Double_t* E;
  Bool_t* EGate;
  Bool_t* EFilled;

  //Int_t EvtNb=0;
  //Int_t CurEvt=0;
  //Int_t j=0;

// Read the tree evnt by event:
   
  for(Int_t i=0;i<nevents;i++){
  //for(Int_t i=0;i<1000000;i++){

   if(i%1000 == 0) printf("Event:%d\n",i);

    //cout << " ############################### " << endl;
    //cout << " New Event " << endl;
    //cout << "Event #" << i << endl;

    //if one input tree:     
    //nb += T->GetEvent(i);
    //T->GetEvent(i); 
    //Int_t i=0;
    T->GetEvent(i);

    //cout << "event= " <<  agata.event << endl;
    //cout << "Mult= " <<   agata.TMult << endl;// gamma multiplicity index (not crystal like in AgataRead)
    //cout << "energy=" <<  agata.TELabDopTracked << endl;


    GamMult=  agata.TMult;
    //cout << "GamMult= " <<     agata.TMult << endl;

    E= new Double_t[GamMult];
    EGate= new Bool_t[GamMult];
    EFilled= new Bool_t[GamMult];

    if(GamMult>1){

      for(int j=0;j<GamMult;j++)
	{
	  E[j]=agata.TELabDopTracked;
	  EGate[j]=false; // initalisation for the gates
	  EFilled[j]=false; // initialisation 
	  //cout << "CrystalId= " <<     agata.AGcrystalId << endl;
	  //cout << "Energy= " <<        agata.AGenergy << endl;
	  //cout << "Energy= " <<        E[j] << endl;

	  T->GetEvent(i+(j+1)); 

	}
      i= i + (GamMult-1);
    }else // ie: GamMult=1
      {
	E[0]=agata.TELabDopTracked;
	//cout << "CrystalId= " <<     agata.AGcrystalId << endl;
	//cout << "Energy= " <<        agata.AGenergy << endl;
	//cout << "Energy= " <<        E[0] << endl;
	//cout << "EnergyCoreDop= " << agata.AGenergyCoreDop << endl;    
      }

    // Now loop over all 4-gate combinations from 21 energies table and keep all the other energies 
    // of the event in histogram each time if amonsgt 4 energies in this event  matches one of the gate combitnation.

    // but first chose a random unlocked combination of 4 gamma rays amongs 21:

    if(GamMult>4){ // to gate on 4 gamma-rays 


      // -----   For x-gate combination amongst the 21 gamma-ray   -------------------------------------------
      //CombCounter=0;     

      // choose a gate randomly as loong as ther are not all locked

      //bool CombLock[21][21][21][21]={false}; // 4-gamma gate combination open (false) or locked (true)
      //bool flag_NewComb=false;   

      // reset CombLock to false for new event with CrystMult>4:
      //if(CombCounter!=0){
	for(int m=0;m<TSD1Len;m++){
	  for(int n=0;n<TSD1Len;n++){
	    for(int o=0;o<TSD1Len;o++){
	      for(int p=0;p<TSD1Len;p++){
		CombLock[m][n][o][p]=false;
	      }
	    }
	  }
	}
      //}

     for (int n=0;n<TSD1Len;n++)CombLock[n][n][n][n]=true; // locking already these combinations since not possible

      srand( time(NULL)); // initialise seed


     while( CombCounter <= 5985){  // 5985 is the maximum number of combination of 4 amongst 21 !

      // Let choose randomly a set of 4 gamma-ray
	   // first random index:
 	   RandIndex1= rand() % TSD1Len;
	   Egam1=TSD1[RandIndex1];
	   //cout << "RandIndex1: " << RandIndex1 << " Egam1= "<< Egam1 << endl;

	   // Second random index
 rand2:	   RandIndex2= rand() % TSD1Len;
           if(RandIndex2!=RandIndex1)
	   { 
	     Egam2=TSD1[RandIndex2];
	     //cout << "RandIndex2: " << RandIndex2 << " Egam2= "<< Egam2 << endl;
	   }else
	     {goto rand2;
	     }

	   // Third random index:
 rand3:	   RandIndex3= rand() % TSD1Len;
	   if(RandIndex3!=RandIndex2 && RandIndex3!=RandIndex1)
	   { 
	     Egam3=TSD1[RandIndex3];
	     //cout << "RandIndex3: " << RandIndex3 << " Egam3= "<< Egam3 << endl;
	   }else
	     { goto rand3;
	     }

	   // Fourth random index:
 rand4:	   RandIndex4= rand() % TSD1Len;
	   if(RandIndex4!=RandIndex3 && RandIndex4!=RandIndex2 && RandIndex4!=RandIndex1)
	   {
	     Egam4=TSD1[RandIndex4];
	     //cout << "RandIndex4: " << RandIndex4 << " Egam4= "<< Egam4 << endl;


	     if(CombLock[RandIndex1][RandIndex2][RandIndex3][RandIndex4]==false){
	       //cout << " Find new combination:" << endl;

                flag_NewComb=true;

		CombCounter++;

	        //cout << " Doing Combination:" << TSD1[RandIndex1] << " " << TSD1[RandIndex2] << " " <<  TSD1[RandIndex3] << " " << TSD1[RandIndex4] << " !!"<< endl;

		 // lock all corresponding permutation (to go faster):     
		 CombLock[RandIndex1][RandIndex2][RandIndex3][RandIndex4]=true;
		 CombLock[RandIndex1][RandIndex2][RandIndex4][RandIndex3]=true;
		 CombLock[RandIndex1][RandIndex3][RandIndex2][RandIndex4]=true;
		 CombLock[RandIndex1][RandIndex3][RandIndex4][RandIndex2]=true;
		 CombLock[RandIndex1][RandIndex4][RandIndex2][RandIndex3]=true;
		 CombLock[RandIndex1][RandIndex4][RandIndex3][RandIndex2]=true;

		 CombLock[RandIndex2][RandIndex1][RandIndex3][RandIndex4]=true;
		 CombLock[RandIndex2][RandIndex1][RandIndex4][RandIndex3]=true;
		 CombLock[RandIndex2][RandIndex3][RandIndex1][RandIndex4]=true;
		 CombLock[RandIndex2][RandIndex3][RandIndex4][RandIndex1]=true;
		 CombLock[RandIndex2][RandIndex4][RandIndex1][RandIndex3]=true;
		 CombLock[RandIndex2][RandIndex4][RandIndex3][RandIndex1]=true;

		 CombLock[RandIndex3][RandIndex2][RandIndex1][RandIndex4]=true;
		 CombLock[RandIndex3][RandIndex2][RandIndex4][RandIndex1]=true;
		 CombLock[RandIndex3][RandIndex1][RandIndex2][RandIndex4]=true;
		 CombLock[RandIndex3][RandIndex1][RandIndex4][RandIndex2]=true;
		 CombLock[RandIndex3][RandIndex4][RandIndex1][RandIndex2]=true;
		 CombLock[RandIndex3][RandIndex4][RandIndex2][RandIndex1]=true;

		 CombLock[RandIndex4][RandIndex1][RandIndex2][RandIndex3]=true;
		 CombLock[RandIndex4][RandIndex1][RandIndex3][RandIndex2]=true;
		 CombLock[RandIndex4][RandIndex2][RandIndex1][RandIndex3]=true;
		 CombLock[RandIndex4][RandIndex2][RandIndex3][RandIndex1]=true;
		 CombLock[RandIndex4][RandIndex3][RandIndex1][RandIndex2]=true;
		 CombLock[RandIndex4][RandIndex3][RandIndex2][RandIndex1]=true;

	     }else // CombLock is already true
	       {
		 flag_NewComb=false;
		 if(CombCounter==5985) break;
	       }
	     
	     //cout << "CombCounter= " << CombCounter << " Event: " << i << endl; 

	    }else
	     { goto rand4;
		  }
	       

	//for a new combination:

	if(flag_NewComb){

	  //std::cout << "Egam1: " << Egam1 << " Egam2: " << Egam2 //
	    //	    << " Egam3: " << Egam3 << " Egam4: " << Egam4 <<std::endl;

           EgamGate[0]=Egam1;
           EgamGate[1]=Egam2;
           EgamGate[2]=Egam3;
           EgamGate[3]=Egam4;

	   // Now that a gate has been chosen:  
	   // let's find out if measured gamma-rays are used for the gate 
	   for(int l1=0;l1<GamMult;l1++){  // loop l1 over all the energy measured in the event
	     for(int l2=0;l2<4;l2++){        // loop l2 over the 4 energy of a given random combination  

	       if( E[l1]> EgamGate[l2]-2.0 && E[l1]< EgamGate[l2]+2.0 ){
		 EGate[l1]=true; // gamma belong to gate
		 nGateCounter++;
	       }
	     }
	     
	   }

	   // Now filling the histogram:
	   if(nGateCounter==4){ // should not be high than 4 for 4-gamma gate
	     cout << "New gggg; GamMult=" << GamMult << endl;

	    for(int l1=0;l1<GamMult;l1++){  // second loop l1 over all the energy measured in the event

	      if(EGate[l1]==false && EFilled[l1]==false){ 
	        EneGate4Dop->Fill(E[l1]); // fill histogram only with the gamma-rays that we didn't gate on.
		EFilled[l1]=true;  // to avoid multi counting of gamma-rays not in the TSD1 list
		cout << " Gate is: " << EgamGate[0] << " " << EgamGate[1] << " " << EgamGate[2]<< " " << EgamGate[3] << endl;  
		cout << "and filling  histo with: " << E[l1] << endl;
	      }else // EGate[l1]==true
	        { 
		 cout << " while gating on:" << E[l1]<< endl; 
		 EGate[l1]=false; // reset to false
	        }
	    }
	   }

	   nGateCounter=0; // reset nGate
           flag_NewComb=false;  // Treatment of new comb completed
	   for(int l1=0;l1<GamMult;l1++)EGate[l1]=false; // reset EGate
	  
	   //cout << " Combination:" << TSD1[RandIndex1] << " " << TSD1[RandIndex2] << " " <<  TSD1[RandIndex3] << " " << TSD1[RandIndex4] << " done !!"<< endl;

	   if(CombCounter==5985) break;


	} // end of flag_NewComb	     

	//cout << "CombCounter=" << CombCounter  <<  endl; 

      } // end of while loop over CombCounter


     //cout << "CombCounter=" << CombCounter << " Event" << i << endl; 
      if(CombCounter<5985) cout << "Attention, some combination must have been missed" << endl;  
      CombCounter=0; 

    } // end of GamMult>4
   
    delete [] E;
    delete [] EGate;
    delete [] EFilled;

    /*
    CurEvt=agata.event; // Current event
    //EvtNb=agata.event;
    if(EvtNb==0 && agata.TELabDopTracked>0)EvtNb=agata.event;  // for first event with energy deposited.

  store: cout << "Event: " << CurEvt << endl;
           while(CurEvt==EvtNb){ // fill E[j] with tracked energy
             E[j]=agata.TELabDopTracked;
	     if(EvtNb==164)cout << "Event: " << CurEvt << " Energy:" << E[j] << endl;
	     // move tree to next entry:
	     //cout << "cava1" << endl;
	     T->GetEvent(i+j+1);
             //cout << "cava2"<< endl;
             CurEvt=agata.event; // Current event
             //cout << "cava3"<< endl;
	     j++;
           }

    i=i+j; // current value of i
    GamMult=j; // only keep the max value of TMult

    if(CurEvt!= EvtNb) // = new event
      {

	// Treat the previous event that just ended:

        // Now loop over all 4-gate combinations from 21 energies table and keep all the other energies 
        // of the event in histogram each time if amonsgt 4 energies in this event  matches one of the gate combitnation.

        // but first chose a random unlocked combination of 4 gamma rays amongs 21:
	
         if(GamMult>4){ // to gate on 4 gamma-rays 

         // -----   For x-gate combination amongst the 21 gamma-ray   -------------------------------------------
         //CombCounter=0;     

         // choose a gate randomly as loong as ther are not all locked

         // reset CombLock to false for new event with CrystMult>4:
         //if(CombCounter!=0){
	   for(int m=0;m<TSD1Len;m++){
	     for(int n=0;n<TSD1Len;n++){
	       for(int o=0;o<TSD1Len;o++){
	         for(int p=0;p<TSD1Len;p++){
		   CombLock[m][n][o][p]=false;
	         }
	       }
	     }
	   }
        //}

        for (int n=0;n<TSD1Len;n++)CombLock[n][n][n][n]=true; // locking already these combinations since not possible

         srand( time(NULL)); // initialise seed


        while( CombCounter <= 5985){  // 5985 is the maximum number of combination of 4 amongst 21 !

         // Let choose randomly a set of 4 gamma-ray
	   // first random index:
 	   RandIndex1= rand() % TSD1Len;
	   Egam1=TSD1[RandIndex1];
	   //cout << "RandIndex1: " << RandIndex1 << " Egam1= "<< Egam1 << endl;

	   //cout << "cava4"<< endl;

	   // Second random index
 rand2:	   RandIndex2= rand() % TSD1Len;
           if(RandIndex2!=RandIndex1)
	   { 
	     Egam2=TSD1[RandIndex2];
	     //cout << "RandIndex2: " << RandIndex2 << " Egam2= "<< Egam2 << endl;
	   }else
	     {goto rand2;
	     }

	   // Third random index:
 rand3:	   RandIndex3= rand() % TSD1Len;
	   if(RandIndex3!=RandIndex2 && RandIndex3!=RandIndex1)
	   { 
	     Egam3=TSD1[RandIndex3];
	     //cout << "RandIndex3: " << RandIndex3 << " Egam3= "<< Egam3 << endl;
	   }else
	     { goto rand3;
	     }

	   // Fourth random index:
 rand4:	   RandIndex4= rand() % TSD1Len;
	   if(RandIndex4!=RandIndex3 && RandIndex4!=RandIndex2 && RandIndex4!=RandIndex1)
	   {
	     Egam4=TSD1[RandIndex4];
	     //cout << "RandIndex4: " << RandIndex4 << " Egam4= "<< Egam4 << endl;


	     if(CombLock[RandIndex1][RandIndex2][RandIndex3][RandIndex4]==false){
	       //cout << " Find new combination:" << endl;

                flag_NewComb=true;

		CombCounter++;

	        //cout << " Doing Combination:" << TSD1[RandIndex1] << " " << TSD1[RandIndex2] << " " <<  TSD1[RandIndex3] << " " << TSD1[RandIndex4] << " !!"<< endl;

		 // lock all corresponding permutation (to go faster):     
		 CombLock[RandIndex1][RandIndex2][RandIndex3][RandIndex4]=true;
		 CombLock[RandIndex1][RandIndex2][RandIndex4][RandIndex3]=true;
		 CombLock[RandIndex1][RandIndex3][RandIndex2][RandIndex4]=true;
		 CombLock[RandIndex1][RandIndex3][RandIndex4][RandIndex2]=true;
		 CombLock[RandIndex1][RandIndex4][RandIndex2][RandIndex3]=true;
		 CombLock[RandIndex1][RandIndex4][RandIndex3][RandIndex2]=true;

		 CombLock[RandIndex2][RandIndex1][RandIndex3][RandIndex4]=true;
		 CombLock[RandIndex2][RandIndex1][RandIndex4][RandIndex3]=true;
		 CombLock[RandIndex2][RandIndex3][RandIndex1][RandIndex4]=true;
		 CombLock[RandIndex2][RandIndex3][RandIndex4][RandIndex1]=true;
		 CombLock[RandIndex2][RandIndex4][RandIndex1][RandIndex3]=true;
		 CombLock[RandIndex2][RandIndex4][RandIndex3][RandIndex1]=true;

		 CombLock[RandIndex3][RandIndex2][RandIndex1][RandIndex4]=true;
		 CombLock[RandIndex3][RandIndex2][RandIndex4][RandIndex1]=true;
		 CombLock[RandIndex3][RandIndex1][RandIndex2][RandIndex4]=true;
		 CombLock[RandIndex3][RandIndex1][RandIndex4][RandIndex2]=true;
		 CombLock[RandIndex3][RandIndex4][RandIndex1][RandIndex2]=true;
		 CombLock[RandIndex3][RandIndex4][RandIndex2][RandIndex1]=true;

		 CombLock[RandIndex4][RandIndex1][RandIndex2][RandIndex3]=true;
		 CombLock[RandIndex4][RandIndex1][RandIndex3][RandIndex2]=true;
		 CombLock[RandIndex4][RandIndex2][RandIndex1][RandIndex3]=true;
		 CombLock[RandIndex4][RandIndex2][RandIndex3][RandIndex1]=true;
		 CombLock[RandIndex4][RandIndex3][RandIndex1][RandIndex2]=true;
		 CombLock[RandIndex4][RandIndex3][RandIndex2][RandIndex1]=true;

	     }else // CombLock is already true
	       {
		 flag_NewComb=false;
		 if(CombCounter==5985) break;
	       }
	     
	     //cout << "CombCounter= " << CombCounter << " Event: " << i << endl; 

	    }else
	     { goto rand4;
	     }
	       

	 //for a new combination:

	 if(flag_NewComb){

	  //std::cout << "Egam1: " << Egam1 << " Egam2: " << Egam2 //
	    //	    << " Egam3: " << Egam3 << " Egam4: " << Egam4 <<std::endl;

           Egam[0]=Egam1;
           Egam[1]=Egam2;
           Egam[2]=Egam3;
           Egam[3]=Egam4;

	   // Now that a gate has been chosen:  
	   // let's find out if measured gamma-rays are used for the gate 
	   for(int l1=0;l1<GamMult;l1++){  // loop l1 over all the energy measured in the event
	     for(int l2=0;l2<4;l2++){        // loop l2 over the 4 energy of a given random combination  

	       if( E[l1]> Egam[l2]-2.5 && E[l1]< Egam[l2]+2.5 ){
		 EGate[l1]=true;
		 nGateCounter++;
	       }
	     }
	     
	   }
	   // Now filling the histogram:
	   if(nGateCounter>4){
	    for(int l1=0;l1<GamMult;l1++){  // second loop l1 over all the energy measured in the event
	      if(!EGate[l1]){ 
	        EneGate4Dop->Fill(E[l1]); // fill histogram only with the gamma-rays that we didn't gate on.  
	      }else
	        { 
		 EGate[l1]=false; // reset to false
	        }
	    }
	   }

	   nGateCounter=0; // reset nGate
           flag_NewComb=false;  // Treatment of new comb completed
	   //cout << " Combination:" << TSD1[RandIndex1] << " " << TSD1[RandIndex2] << " " <<  TSD1[RandIndex3] << " " << TSD1[RandIndex4] << " done !!"<< endl;

	   if(CombCounter==5985) break;


	  } // end of flag_NewComb	     

	  //cout << "CombCounter=" << CombCounter  <<  endl; 

	 } // end of while loop over CombCounter


        //cout << "CombCounter=" << CombCounter << " Event" << i << endl; 
         if(CombCounter<5985) cout << "Attention, some combination must have been missed" << endl;  
         CombCounter=0; 

	} // end of CrystMult>4

	

	//
	// continue to next event:
	//
	cout << "GamMult=" << GamMult << " i=" << i << " Event:" << EvtNb<< endl;
	j=0; // reseting j to 0
        EvtNb=CurEvt; // set EvtNb to new Current event

        if(EvtNb<9999999){
	  goto store;
	}else
	  {
	    goto fin;
	  }
	
      }
    */


      }  // next tree entry 

  // fin:
  cout << "c'est fini !" << endl;

  outFile->cd();
  EneGate4Dop->Write();
 
  outFile->Close();


}
