//
//  This program reads the AGATA simulation output file and build spectra for Summed cores, with and without ponderation from 
//  Canberra crystal efficiency values and with or whitout doppler correction at core level and segment level
//
//  !!! Important Note:
//
//  -1- The simulation output file is expected to contain the position of the crystal and segments in lab and thus been 
//      created with command: /Agata/file/verbose 2 (or 3)
//
//  
//  -2- if, the A180SolidExp.list file that defines extra Ge passive area in the Agata crystal geometry is used in
//  the Agata simulation input, then the pondered spectra should be ignored and the non podered spectra should be used to
//  estimate the efficiecncy and P/T ratio.
//  if, on the contrary, the A180Solid.list file is used, then you can use the pondered spectra for the estimate of the efficiency 
//  and P/T.
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  How To:
//	For monoenergetic source:	./agataRead beta     	(beta in %)
//	For rotational band:            ./agataRead beta Egam1	(Egam1 is the gamma energy used for the core-core gated spectrum
//	or:                             ./agataRead beta Egam1 EGam2 (display number of Egam2 in coincidence with EGam1)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#define MaxCryst 180  // Maximum number of crystals 
#define MaxClust  60  // Maximum number of clusters 
#define MaxMult   50  // Maximum gamma-ray multiplicity (should be set equal or higher than input gamma-ray multiplicity


#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <stdint.h>
#include <vector>

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TH2.h>
#include <TMath.h>
#include <TRandom.h>

//using std::setw;
using namespace std;

int main( int argc, char** argv ) 
{  
	string filestr;  //the filename to read (GammaEvents.0000)
	string Canberrafile;  //Canberra efficiency file to read (CanberraLut.txt)
	char dummy1[3], dummy2[4]; 
	FILE *readingFile;
	FILE *readingCanberra;
	char line[512], line2[512], line1XtalsPos[512], line2XtalsPos[512];
	char line1SegsPos[512], line2SegsPos[512];
	char lineMult[20];
	double nrj, geantX, geantY, geantZ, tof;  
	int Key, dummy;
	int  CrystalId=-1;
	int Evt=-1;

	int CryId, dum, Slice, Sector, SliSeg;
	int jSlice, jSeg;
	double Xpos, Ypos, Zpos;
	double CrystX[MaxCryst], CrystY[MaxCryst], CrystZ[MaxCryst];
	bool XstPosRead=false;
	bool SegPosRead=false;
	
	double CanberraEff[MaxCryst];  // efficiency factor of crystal provided by Canberra
	double CanbEffClustEvt[MaxClust];  // efficiency factor of crystal provided by Canberra whithin a cluster ATC or ADC
	double CanbEffgClustEvt[MaxClust];  // efficiency factor of crystal provided by Canberra whithin a gamma cluster
	double CanbEffEvt=1.;     // efficiency factor = product of CanberraEff[i] for all crystals i hit in the evt. 
	
	strcpy(line1XtalsPos,"POSITION_CRYSTALS\n");
	strcpy(line2XtalsPos,"ENDPOSITION_CRYSTALS\n");
	strcpy(line1SegsPos,"POSITION_SEGMENTS\n");
	strcpy(line2SegsPos,"ENDPOSITION_SEGMENTS\n");
	strcpy(lineMult,"GAMMA\n");



//Counters:
	int TotEvt=0;
	int AgataEvt=0;
        int Ncoinc=0;
        int ModMult=0;
        int AgataCryst=0;
	int Nbgam1 = 0;
	int Nbgam2 = 0;
	int Nbgam1_add = 0;
	int Nbgam2_add = 0;

	bool gam1=false;
	bool gam2=false;
	bool hit=false;
	int  nhit=0;

	int InMult=-1;  // InMult will be set to 0 after an incident gamma-ray is seen in the input file

	double beta;	
	double Egam1=0;
	double Egam2=0;
	int inGammaMult;	// Multiplicity gamma in simulation input
	double Egam1st=0.;      // first of the listed gamma in the simulation input


    if (argc < 2) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " Input_file_name  BetaValue" << std::endl;
        std::cerr << "or: " << argv[0] << " Input_file_name BetaValue 1st_Gamma_Energy" << std::endl;
        std::cerr << "or: " << argv[0] << " Input_file_name BetaValue 1st_Gamma_Energy 2nd_Gamma_Energy" << std::endl;
       /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }
    

	filestr=argv[1];

	beta= atoi(argv[2]);

    if (argc == 4) {	
	Egam1=atof(argv[3]);
	}
    if (argc == 5) {
	Egam1=atof(argv[3]);
	Egam2=atof(argv[4]);
	}

	
   // Print the beta value:

	if(beta!=0){
	std::cout << "!!" << std::endl;	
	std::cout << "WARNING: beta !=0 so Crystal/Segment Positions are expected to be in the header part of GammaEvents.xxxx " << std::endl;
	std::cout << "WARNING: If not, Doppler correction at Crystal/Segment level will not be applied correctly" << std::endl;
	std::cout << "WARNING: Reminder: to get Crystal position use the command << /Agata/file/verbose 2 >> when running the simulation "<< std::endl;
	std::cout << "!!" << std::endl;
    		std::cout << " " << std::endl;
    		std::cout << "Beta is " << beta << "% !" << std::endl;
    		beta=beta/100.; // 
    		std::cout << "Egam1: " << Egam1 << " Egam2: " << Egam2 << std::endl;
    		std::cout << " " << std::endl;
	}



	double AGsigma=1.5; // sigma intinsic resolution in keV 
	double AGsigmaCore, AGsigmaSeg; // sigma resolution at Core and segment level in keV 
	AGsigmaCore=4*AGsigma; //AGsigma+87.137*pow(beta,1.13);  // parameters determined via simulation
	AGsigmaSeg=3*AGsigma; // 1.8546*exp(4.97*beta); //AGsigma+35.00*pow(beta,1.043);

	cout << "Sigma Core estimate (keV): " << AGsigmaCore << endl;
	cout << "Sigma Segment estimate (keV): " << AGsigmaSeg << endl;

// for +-1.*sigma:
	double MinlimCore_gam2= Egam2-1.*AGsigmaCore;
	double MinlimCore_gam1= Egam1-1.*AGsigmaCore;
 	double MaxlimCore_gam2= Egam2+1.*AGsigmaCore;
	double MaxlimCore_gam1= Egam1+1.*AGsigmaCore;

	double MinlimSeg_gam2= Egam2-1.*AGsigmaSeg;
	double MinlimSeg_gam1= Egam1-1.*AGsigmaSeg;
 	double MaxlimSeg_gam2= Egam2+1.*AGsigmaSeg;
	double MaxlimSeg_gam1= Egam1+1.*AGsigmaSeg;


	double Energy[MaxCryst];  //empty array to hold output data during readout of events (180 crystals)
	double EnergyMul[MaxCryst][MaxMult];  //empty array to hold output data during readout of events (180 crystals and max of 50 gamma-ray multiplicity )
	double EnergySeg[MaxCryst][6][6];  //empty array to hold output data during readout of events (180 crystals)
	double EnergyMulSeg[MaxCryst][6][6][MaxMult];  //empty array to hold output data during readout of events (180 crystals)

	double Ecluster[MaxClust];  //empty array to hold output data during readout of events (60 clusters) <=> addback between crystal of same cluster
	double EclusterDop[MaxClust];  //empty array to hold output data during readout of events (60 clusters) <=> addback between crystal of same cluster and Doppler corrected (crystal level)
	double EclusterDopSeg[MaxClust];

	double EnergyCoreDop[MaxCryst];     // energ (core singles) and Doppler corrected from crystal position
	double EnergyCoreDopSeg[MaxCryst];  // energy (core singles) and Doppler corrected from segment position

	double EnergyDopAdBck=0.;     // energy after addback and Doppler corrected from crystal position
	double EnergyDopSegAdBck=0.;  // energy after addback and Doppler corrected from segment position
	double EnergyCaloDop=0.;     // energy after full addback and Doppler corrected from crystal position
	double EnergyCaloDopSeg=0.;  // energy after full addback and Doppler corrected from segment position

	//float Time[32];  //empty array to hold output data during readout of events

	int indice=-1;

	double Theta[MaxCryst];  //empty array to hold output data during readout of events (180 crystals)
	double ThetaSeg[MaxCryst][6][6];  //empty array to hold output data during readout of events (180 crystals)


	for( int i=0; i<MaxCryst; i++)
	{
	   Energy[i]= 0.;
           EnergyCoreDop[i]=0.;
           EnergyCoreDopSeg[i]=0.;
	   for(int ii=0;ii<MaxMult;ii++)EnergyMul[i][ii]= 0.;

	   Theta[i]=0.;
		
	   CanberraEff[i]=1.;

	   for (int j=0; j<6;j++)
	   {
		for (int jj=0; jj<6;jj++)
		{
		   EnergySeg[i][j][jj]=0.;
		   ThetaSeg[i][j][jj]=0.;
		   for(int ii=0;ii<MaxMult;ii++)EnergyMulSeg[i][j][jj][ii]= 0.; 
		}
	   }

	}
	for( int i=0; i<MaxClust; i++)
	{
		Ecluster[i]= 0.;
		CanbEffClustEvt[i]=1.;  // initialisation for 100% efficiency
		CanbEffgClustEvt[i]=1.;  // initialisation for 100% efficiency
		EclusterDop[i]=0.;
		EclusterDopSeg[i]=0.;

	}

	Canberrafile = "CanberraLut.txt";  //
	readingCanberra = fopen(Canberrafile.c_str(),"r");  //open the file


        int j=0;
        while(!feof(readingCanberra))  //keep going until end of input file
	{
	    fgets(line,512,readingCanberra);  //get a line from the file
           // extract just the first value
	  	sscanf(line,"%s\t%s\t%lf", dummy1, dummy2,  &CanberraEff[j]);  //extract 2 first values from the line, what ever it is

		//cout << "dummy1= " << dummy1 << endl;
 		//cout << "dummy2= " << dummy2 << endl;
  		//cout << "CanberraEff= " << CanberraEff[j] << endl;
           j++;		
	
	}

	//for( int i=0; i<MaxCryst; i++)
	//{
        //		cout << "canberra eff det# " << i << "= " << CanberraEff[i] << endl;
	//}


// Define Sigma data structure

	struct AGDATA;
	struct AGDATA{
	  int AGevent;
	  //int AGcrystMult;
	  int  AGcrystalId; //
	  float AGenergy;
	};

	AGDATA agata;

// Create ROOT ntuple
	TFile* output_File= new TFile("agataTree.root","recreate");
	TTree* output_tree= new TTree("AGATATree","Agata");
	//output_tree->Branch("Agata", &agata, "AGevent/I:AGcrystMult/I:AGcrystalId/I:AGenergy/F");
	output_tree->Branch("Agata", &agata, "AGevent/I:AGcrystalId/I:AGenergy/F");

// Create some Histograms
	  //TH1F *Sect= new TH1F("Sect","Sector distribution", 10, 0., 10.);
	  //TH1F *Slice= new TH1F("Slice","Slice distribution", 10, 0., 10.);
	  //TH1F *Anneau= new TH1F("Anneau","Inner Ring distribution", 5, 0., 5.);
      TH2F *XstalXY= new TH2F("CrystalXY","Crystal lab. position in XY plane", 100, -500., 500, 1000, -500., 500.);


// histo for Singles:
      TH1F *Ene= new TH1F("EneCore","Energy deposited in all modules", 3000, 0., 3000.);
      TH1F *EneW= new TH1F("EneCoreW","Energy deposited in all cores pondered by each crystal efficiency", 3000, 0., 3000.);
      TH2F *EneCC= new TH2F("EnCore_Core","Core-Core", 3000, 0., 3000, 3000, 0., 3000.);
	TH1F *EneGate= new TH1F("EneGated", "spectrum Gated on energy Egam1", 3000, 0., 3000.);

      TH1F *EneCoreDop= new TH1F("EneCoreDop","Energy deposited in core (Singles) & Doppler corrected at Crystal level", 3000, 0., 3000.);
      TH1F *EneWCoreDop= new TH1F("EneCoreWDop","Energy deposited in cores (Singles), pondered by efficiency product of each hit crystal & Doppler corrected at Crystal level", 3000, 0., 3000.);
      TH2F *EneCCDop= new TH2F("EnCore_Core_Dop","Core-Core after Doppler correction at crystal level", 3000, 0., 3000, 3000, 0., 3000.);
	TH1F *EneGateDop= new TH1F("EneGatedDop", "spectrum Gated on energy Egam1 after Doppler at crystal level", 3000, 0., 3000.);

      TH1F *EneCoreDopSeg= new TH1F("EneCoreDopSeg","Energy deposited in core (Singles) & Doppler corrected at Segment level", 3000, 0., 3000.);
      TH1F *EneWCoreDopSeg= new TH1F("EneCoreWDopSeg","Energy deposited in cores (Singles), pondered by efficiency product of each hit crystal & Doppler corrected at Segment level", 3000, 0., 3000.);
      TH2F *EneCCDopSeg= new TH2F("EnCore_Core_DopSeg","Core-Core after Doppler correction at segment level", 3000, 0., 3000, 3000, 0., 3000.);
	TH1F *EneGateDopSeg= new TH1F("EneGatedDopSeg", "spectrum Gated on energy Egam1 after Doppler at segment level", 3000, 0., 3000.);


// histo for full Add-back (calorimeter):
      TH1F *EneCaloDop= new TH1F("EneCaloDop","Energy deposited in core with full addback & Doppler corrected at Crystal level", 3000, 0., 3000.);
      TH1F *EneWCaloDop= new TH1F("EneCaloWDop","Energy deposited in cores with full addback, pondered by efficiency product of each hit crystals & Doppler corrected at Crystal level", 3000, 0., 3000.);

      TH1F *EneCaloDopSeg= new TH1F("EneCaloDopSeg","Energy deposited in core with full addback & Doppler corrected at Segment level", 3000, 0., 3000.);
      TH1F *EneWCaloDopSeg= new TH1F("EneCaloWDopSeg","Energy deposited in cores with full addback, pondered by efficiency product of each hit crystal & Doppler corrected at Segment level", 3000, 0., 3000.);



// histo for Add-back (neighbouring crystals):
     TH1F *EnegClust= new TH1F("EneCoreAdBck","Energy deposited in core with addback", 3000, 0., 3000.);
      TH1F *EneWgClust= new TH1F("EneCoreWAdBck","Energy deposited in cores with addback, pondered by efficiency product of each hit crystals & Doppler corrected at Crystal level", 3000, 0., 3000.);
     TH1F *EnegClustDop= new TH1F("EneCoreAdBckDop","Energy deposited in core with addback & Doppler corrected at Crystal level", 3000, 0., 3000.);
      TH1F *EneWgClustDop= new TH1F("EneCoreWAdBckDop","Energy deposited in cores with addback & Doppler corrected at Crystal level, pondered by efficiency product of each hit crystals & Doppler corrected at Crystal level", 3000, 0., 3000.);
     TH1F *EnegClustDopSeg= new TH1F("EneCoreAdBckDopSeg","Energy deposited in core & Doppler corrected at Segment level", 3000, 0., 3000.);
      TH1F *EneWgClustDopSeg= new TH1F("EneCoreWAdBckDopSeg","Energy deposited in cores, pondered by efficiency product of each hit crystal & Doppler corrected at Segment level", 3000, 0., 3000.);



// histo for Cluster Add-back within clusters
/*      TH1F *EneClust= new TH1F("EneClust","Energy deposited in all clusters (addback within cluster) with no Dop Corr. ", 3000, 0., 3000.);
      TH1F *EneWClust= new TH1F("EneWClust","Energy deposited in all clusters (addback within cluster) with no Dop. Corr., pondered by efficiency product of each hit crystal", 3000, 0., 3000.);

      TH1F *EneClustDop= new TH1F("EneClustDop","Energy deposited in all clusters (addback within cluster)", 3000, 0., 3000.);
      TH1F *EneWClustDop= new TH1F("EneWClustDop","Energy deposited in all clusters (addback within cluster), pondered by efficiency product of each hit crystal + Dop corrected", 3000, 0., 3000.);


      TH1F *EneClustDopSeg= new TH1F("EneClustDopSeg","Energy deposited in all clusters (addback within cluster)", 3000, 0., 3000.);
      TH1F *EneWClustDopSeg= new TH1F("EneWClustDopSeg","Energy deposited in all clusters (addback within cluster), pondered by efficiency product of each hit crystal + Dop corrected", 3000, 0., 3000.);
*/


      TH1F *Mult= new TH1F("CoreMult","Core Multiplicity", 30, 0., 30.);
      TH1F *Theta1stCryst= new TH1F("Theta1stCryst","Theta of first crystal Crystal", 3150, 0., 3.14159);
      TH1F *Theta1stSeg= new TH1F("Theta1stSeg","Theta of first Segment", 3150, 0., 3.14159);

	//filestr = "../../GammaEvents.0000";  //
	//filestr = "/mnt/hgfs/Echanges/4Agata/GammaEvents.0005";  //
	//filestr = "../../GammaEvents.0ps";  //
	//filestr = "../../GammaEvents.200ps";  //
	//filestr = "../../GammaEvents.500ps";  //
	//filestr = "../../GammaEvents.1000ps";  //
	//filestr = "../../GammaEvents.1500ps";  //
	//filestr = "../../GammaEvents.2000ps";  //

	readingFile = fopen(filestr.c_str(),"r");  //open the file
	
	//Create an ascii output file:
	//char outputFile[512] = "AgataResponse.dat";
	//ofstream outFile(outputFile);
	//outFile << "EventNumber " << "Module " << "Ring " << "Energy" << endl;  //column headers

// Create binary output file
//	ofstream BinFile("AgataResponse.bin", ios::binary |  ios::out);
	


// Reading header
    while(line[0] != '$')
	{
   		fgets(line,512,readingFile);  //get a line from the input file
		     //cout << line << endl;

		// Reading input gamma multiplicity
		string thisline(line);
		 thisline.resize(10);
		string truncline = thisline.substr(5,3);

		//char* trkline= new char[truncline.length()+1];  // add if not using C++11
		//strcpy(trkline, truncline.c_str());             // add if not using C++11

		 thisline.resize(5);

		if( thisline=="GAMMA"){  // One read the multipliciti and the 1st gamma-ray of the list 
		  //cout << thisline << truncline << endl;

		  //inGammaMult=atoi(trkline);  // if not using C++11  
		  inGammaMult=stoi(truncline);
		  cout << "Input gamma multiplicity found to be: " << inGammaMult << endl;

   		  fgets(line,512,readingFile);  //get a line from the input file
		  Egam1st=atof(line);  // first gamma in the list 
		  cout << "Energy of the first listed input gamma-ray is: " << Egam1st  << " keV" << endl; 
		}

 		// Reading Segments Positions:

		if(strcmp(line,line1XtalsPos)==0){
			cout << "Reading Crystal positions..." << endl;
			XstPosRead=true;
			// read following line:
			fgets(line,512,readingFile);
		}

		if(strcmp(line,line2XtalsPos)==0){
			cout << "Crystal positions Read" << endl;
                	XstPosRead=false;
		}

		if(XstPosRead){
 			sscanf(line,"\t%i\t%i\t%lf\t%lf\t%lf", &CryId, &dum, &Xpos, &Ypos, &Zpos);
			CrystX[CryId]=Xpos;
			CrystY[CryId]=Ypos;
			CrystZ[CryId]=Zpos;
                 	Theta[CryId]= acos( CrystZ[CryId]/sqrt(pow(CrystX[CryId],2)+ pow(CrystY[CryId],2) + pow(CrystZ[CryId],2) )); // in Rad
			for(int i=0; i<=CryId; i++)XstalXY->Fill(Xpos,Ypos);

			//cout << " X= "<< CrystX[CryId] << " Y= "<< CrystY[CryId] << " CryId= "<< CryId << " Theta=" << Theta[CryId]<< endl;
			// read the next 3 lines but do nothingelse:
			fgets(line,512,readingFile);
			fgets(line,512,readingFile);
			fgets(line,512,readingFile);
		}
		
		// Reading Segments Positions:

		if(strcmp(line,line1SegsPos)==0){
			cout << "Reading Segment positions..." << endl;
			SegPosRead=true;
			// read following line:
			fgets(line,512,readingFile);
		}

		if(strcmp(line,line2SegsPos)==0){
			cout << "Seg positions Read" << endl;
                	SegPosRead=false;
		}

		if(SegPosRead){
 			sscanf(line,"\t%i\t%i\t%i\t%lf\t%lf\t%lf\t%i", &CryId, &Slice, &Sector, &Xpos, &Ypos, &Zpos, &dum);
                 	ThetaSeg[CryId][Slice][Sector]= acos( Zpos/sqrt(pow(Xpos,2)+ pow(Ypos,2) + pow(Zpos,2) )); // in Rad
			//cout << " Crystal id= " << CryId << " Slice= "<< Slice << " Sector= "<< Sector << " X= "<< Xpos << " Y= "<< Ypos << " Z= "<< Zpos << " ThetaSeg=" << ThetaSeg[CryId][Slice][Sector]<< endl;

		}

	}




// Setting initial event number:
    agata.AGevent=0;  


	cout << "I'm working .... please be patient ... " << endl;


// Reading the rest of file
//        while(TotEvt!=20)  // 1 evt (includes g multiplicity)
	while(!feof(readingFile))  //keep going until end of input file
	{
		
	    fgets(line,512,readingFile);  //get a line from the file
	    //cout <<line << endl;
            // extract just the first value
	    sscanf(line,"\t%i\t%lf\t%lf\t%lf\t%lf\t%i", &Key, &nrj, &geantX, &geantY, &geantZ, &SliSeg);  //extract 6 first values from the line, what ever it is
	    //cout << "Reading Key= " << Key << endl;

	    //
			
           //if(nrj==Egam1st)InMult=-1;  // Reset InMult counter to -1 for the first gamma of each event. This works only for beta=0
		//cout << "TotEvt%inGammaMult=" << TotEvt%inGammaMult << endl;
             if(TotEvt%inGammaMult==0)InMult=0; // Reset InMult counter to 0 for the first gamma of each event. Works for all beta !!
	    
//
// First, let's fill the output file with previous event data (if any), once event input multiplicity is reached:
//

	    if((Key==-100 || Key==-8 || Key==-1 || feof(readingFile))){  //-1 included when built in evt gen is used   
	    //if ((Key==-100 || feof(readingFile))){  // a new event with a incident gamma ray (-100) or a new decay (-8) has been found so we record the previous event

		if(InMult<=inGammaMult-1)InMult++;  // counter of gamma-rays that fire AGATA (should be smaller or equals to input gamma-ray multiplicity)

		if(Key==-100 || Key==-8 || Key==-1)TotEvt++; // when a new event start, InMult change from -1 to 0.  
		//if(InMult==0)TotEvt++; // when a new event start, InMult change from -1 to 0.  
		//cout << " Current Evt= " << TotEvt << " *********** "<< endl;
		//cout << "InMult=" << InMult << endl;	   

//
// But first let's fill the output file with previous event data (if any):
//
//		if(hit && nrj==Egam1st)  // To ensure there is at least one hit crystal and input gamma multiplicity is complete.. This works only for beta=0
		if(hit && TotEvt%inGammaMult==0) // To ensure there is at least one hit crystal and input gamma multiplicity is complete. This works for any beta
		{
			hit=false; // reset hit to false for current event


			//
			// Deposited energy in each crystal (= accumulated in case of event with input multiplicity gamma >1)
			//
	
			for (int cryst=0; cryst<MaxCryst; cryst++)  // 1st loop over all crystals to add energies deposited in the same crystal by different g-ray emitted in the same event.
			{

			  for (int mul=0; mul<inGammaMult; mul++)   // loop over all input gamma multiplicity
			  {  

			    // For Cores:
			    if(EnergyMul[cryst][mul]>0. ){  
				if(Energy[cryst]==0.){ // then increment Module multiplicity:

					ModMult++;   // ModMultreset at the end of the event = Detector multiplicity
					AgataCryst++;  // accumulated for the all run
					Energy[cryst]= EnergyMul[cryst][mul];

				}else  // accumulate energy whether it is the same gamma or not :
				{
					Energy[cryst]= Energy[cryst]+EnergyMul[cryst][mul];
				}
				//cout << "Energy= " << Energy[cryst] << " in crystal " << cryst << endl;			
					
				if(ModMult==1){  // ModMult can be higher than 1 but each time ModMult==1, one has a new event
					AgataEvt++;  // a new event seen by Agata 
					agata.AGevent=AgataEvt; // Event number 
				}
			     }

			    // For Segments

			    for(int j=0; j<6; j++)
				for(int jj=0; jj<6; jj++)
				{				
			    		//if(EnergyMulSeg[cryst][j][jj][mul]>0. ){  
					//	if(EnergySeg[cryst][j][jj]==0.){ // then increment Seg multiplicity:
					//	SegMult++;   // ModMultreset at the end of the event = Detector multiplicity
					//	AgataSeg++;  // accumulated for the all run
					//	Energy[cryst][j][jj]= EnergyMul[cryst][mul];
					//}else  // accumulate energy whether it is the same gamma or not :
					//{
					EnergySeg[cryst][j][jj]= EnergySeg[cryst][j][jj]+EnergyMulSeg[cryst][j][jj][mul];
					//}
					//cout << "Energy= " << Energy[cryst] << " in crystal " << cryst << endl;			
				}

			   } // end of loop over mul

			  // First one adds detector intrinsic resolution
			  Energy[cryst]=gRandom->Gaus(Energy[cryst],AGsigma);
			 
			}  // end of loop over MaxCryst crystals	

			//agata.AGcrystMult=ModMult; // Core Multiplicity in this event
				
			//
			//  Search for gamma cluster - gClust (= add back on closer neighbour) 
			//  & determination of Theta from highest energy  crystals
			//

			double EFirstgClust=0.;
			double EgClustSegFirst[MaxMult];
			double ThetaFirstgClust[MaxMult];
			double ThetaFirstgClustSeg[MaxMult];
	                int gClust=0;
			bool Checked[180];
			for (int i=0; i<180;i++)Checked[i]=false;
			double EgClust[MaxMult];
			double EgClustDop[MaxMult];
			double EgClustDopSeg[MaxMult];
			for (int i=0; i<MaxMult;i++){
				EgClust[i]=0.;
				EgClustDop[i]=0.;
				EgClustDopSeg[i]=0.;
				ThetaFirstgClust[i]=0.;
				EgClustSegFirst[i]=0.;
				ThetaFirstgClustSeg[i]=0.;
			} 

			for (int i=0; i<MaxCryst-1; i++){  // 2nd loop over all crystals to 



			   if(Energy[i]>20 && Checked[i]==false){   // 20keV threshold
				EgClust[gClust]=Energy[i];
				EFirstgClust=Energy[i];
				ThetaFirstgClust[gClust]=Theta[i];
				CanbEffgClustEvt[gClust]=CanbEffClustEvt[gClust]*CanberraEff[i]; // to pondere by crystal efficiency from Canberra
				for(int j=0;j<6;j++)  // To find the first Segment hit in this crystal
				  for(int jj=0;jj<6;jj++)
				  {
	           			if(EnergySeg[i][j][jj]>EgClustSegFirst[gClust]){  // To find the first segment hit:
			  			EgClustSegFirst[gClust]= EnergySeg[i][j][jj];
						ThetaFirstgClustSeg[gClust]= ThetaSeg[i][j][jj];  // 1st Segment hit in the coresponding g cluster
					}
				  }


			   	for (int ii=i+1; ii<MaxCryst ; ii++){
					if(Energy[ii]>20 && (Theta[i]-Theta[ii])<.4 ){  // neighbour also hit so possibly same gamma-ray
						EgClust[gClust] += Energy[ii] ;
						if(Energy[ii]>EFirstgClust){
							ThetaFirstgClust[gClust]=Theta[ii];  // = determinaton of crystal with highest energy in cluster and the corresponding theta 
				   			 for(int j=0;j<6;j++)  // To find the new first Segment hit in this crystal
							  for(int jj=0;jj<6;jj++)
							  {
				           			if(EnergySeg[ii][j][jj]>EgClustSegFirst[gClust]){  // To find the first segment hit:
					  			EgClustSegFirst[gClust]=     EnergySeg[ii][j][jj];
								ThetaFirstgClustSeg[gClust]= ThetaSeg[ii][j][jj];  // 1st Segment hit in the coresponding g cluster
					   		  	}
							   }
						}
						Checked[ii]=true;  // Flag to avoid double counting
						CanbEffgClustEvt[gClust]=CanbEffClustEvt[gClust]*CanberraEff[ii]; 
	
					}
				}
				gClust++;  // increment, for next gamma cluster if any
			    }
			}

			//if(gClust>inGammaMult) cout << "Warning: found g multiplicity=" << gClust << "> input g multiplicity =" << inGammaMult << endl;


			// Tree:
			//agata.AGnHits=nHits;
			double EnergyTot=0.;
			double EFirst=0.;
			double ThetaFirst=0.;
			double ESegFirst=0.;
			double ThetaSegFirst=0.;
			double ESeg1st[MaxCryst];		
			double ThetaSeg1st[MaxCryst];
			for(int i=0; i<MaxCryst;i++)
			{
				ESeg1st[i]=0.;
				ThetaSeg1st[i]=0.;
			}

/*			double ThetaClustCryst1st[MaxClust], ThetaClustSeg1st[MaxClust];  // used for det addback in det. cluster (ATC/ADC)
			for(int i=0; i<MaxClust;i++){
				ThetaClustCryst1st[i]=0;
				ThetaClustSeg1st[i]=0;
			}
*/
			int indic=-1;


			for (int cryst=0; cryst<MaxCryst; cryst++)	// Loop To find the crystal/Segment hit with highest energy
			{

			  indic=int(cryst/3);  // indice of corresponding cluster

			  // To find the first crystal hit and first hit segment in that first crystal (based on highest deposited energy)
				if(Energy[cryst]>EFirst){          // To find the first crystal hit:
				    EFirst=     Energy[cryst];
	 			    ThetaFirst= Theta[cryst];
//				    ThetaClustCryst1st[indic]= Theta[cryst];      // 1st Crystal hit in the coresponding cluster

				    for(int j=0;j<6;j++)		   // To find the first Segment hit in this crystal
					for(int jj=0;jj<6;jj++)
					{
				           if(EnergySeg[cryst][j][jj]>ESegFirst){          // To find the first segment hit:
					  	ESegFirst=     EnergySeg[cryst][j][jj];
						ThetaSegFirst= ThetaSeg[cryst][j][jj];     // the first Segment hit amongst all hit crystals
//						ThetaClustSeg1st[indic]= ThetaSeg[cryst][j][jj];      // 1st Segment hit in the coresponding cluster
					   }
					}		
				}



			 // To find first segment in each hit crystals:
				if(Energy[cryst]>20.){          

				    for(int j=0;j<6;j++)		   // To find the first Segment hit
					for(int jj=0;jj<6;jj++)
					{
				           if(EnergySeg[cryst][j][jj]>ESeg1st[cryst]){          // To find the first segment hit
					  	ESeg1st[cryst]=     EnergySeg[cryst][j][jj];
						ThetaSeg1st[cryst]= ThetaSeg[cryst][j][jj];     // 1st Segment hit in that Xtal

					   }
					}
			         }
					

			} // end of loop over crystals

		               //cout << "energy Cryst=" << EFirst << " Theta Cryst=" << ThetaFirst << " energy Seg=" << ESegFirst << " Theta Seg="<<  ThetaSegFirst << endl;



			for (int cryst=0; cryst<MaxCryst; cryst++)  // To do add back within cluster and Doppler correction from first core or first segment
			{
				if(Energy[cryst]>20){
						
					agata.AGcrystalId=cryst; //
					//Energy[cryst]=gRandom->Gaus(Energy[cryst],AGsigma); // already done above
					EnergyTot += Energy[cryst];   // full Addback

					CanbEffEvt= CanbEffEvt*CanberraEff[cryst];  // to pondere with crystal efficiency from canberra

				        EnergyCoreDop[cryst]=Energy[cryst]*(1-beta*cos((Theta[cryst])))/sqrt(1-beta*beta);
					EneCoreDop->Fill(EnergyCoreDop[cryst]);                       // = Singles doppler corrected at Xtal level
					EneWCoreDop->Fill(EnergyCoreDop[cryst],CanberraEff[cryst]);   // = Single factored by measure intrinsic efficiency from Canberra and Dop corrected at Xtal level
			
				        EnergyCoreDopSeg[cryst]=Energy[cryst]*(1-beta*cos((ThetaSeg1st[cryst])))/sqrt(1-beta*beta);
					EneCoreDopSeg->Fill(EnergyCoreDopSeg[cryst]);                     // = Singles doppler corrected at Xtal level
					EneWCoreDopSeg->Fill(EnergyCoreDopSeg[cryst],CanberraEff[cryst]);  // = Single factored by measure intrinsic efficiency from Canberra and Dop corrected at Xtal level


					// Fill the tree:
					agata.AGenergy=Energy[cryst]; // Exact energy deposited at the hit location
					//cout << "Energy in crystal" << cryst << " is " << Energy[cryst] << endl;
					
					output_tree->Fill();  // 
		            
					// Find Corresponding Cluster:
	                                indice=int(cryst/3);
	                                Ecluster[indice]+= Energy[cryst]; // addback of the 3 crystals in same cluster
					CanbEffClustEvt[indice]=CanbEffClustEvt[indice]*CanberraEff[cryst]; // to pondere by crystal efficiency from Canberra


					// Let's look for coincidences:
					if(beta==0){
						if(Energy[cryst]> MinlimCore_gam1 && Energy[cryst]<MaxlimCore_gam1) 
						{
							gam1=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam1++; 
						}
						if(Energy[cryst]> MinlimCore_gam2 && Energy[cryst]<MaxlimCore_gam2) 
						{
							gam2=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam2++;
						}
							// with addback:
						if(Ecluster[indice]> MinlimCore_gam1 && Ecluster[indice]<MaxlimCore_gam1) 
						{
							//gam1=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam1_add++; 
						}
						if(Ecluster[indice]> MinlimCore_gam2 && Ecluster[indice]<MaxlimCore_gam2) 
						{
							//gam2=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam2_add++;
						}

					}else
					{

					/*	if(EnergyDop> MinlimCore_gam1 && EnergyDop<MaxlimCore_gam1) 
						{
							gam1=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam1++; 
						}
						if(EnergyDop> MinlimCore_gam2 && EnergyDop<MaxlimCore_gam2) 
						{
							gam2=true;
							//cout << "crystal: " << cryst << " Energy= " << Energy[cryst] << endl;
							Nbgam2++;
						}
					*/	
					}
				}
					
											
			        // at the end of the loop we do the doppler correction and fill the branch:
				if(cryst==179){

					//EnergyDopAdBck=EnergyTot*(1-beta*cos((ThetaFirst)))/sqrt(1-beta*beta);
					EnergyCaloDop=EnergyTot*(1-beta*cos((ThetaFirst)))/sqrt(1-beta*beta);
				        //cout << "Energy Doppler corrected is " << EnergyCaloDop << endl;
				        EneCaloDop->Fill(EnergyCaloDop);
				        EneWCaloDop->Fill(EnergyCaloDop, CanbEffEvt);
				        Theta1stCryst->Fill(ThetaFirst);

					//EnergyDopSegAdBck=EnergyTot*(1-beta*cos((ThetaSegFirst)))/sqrt(1-beta*beta);
					EnergyCaloDopSeg=EnergyTot*(1-beta*cos((ThetaSegFirst)))/sqrt(1-beta*beta);
				 	//cout << "Energy Doppler corrected is " << EnergyCaloDopSeg << endl;
				        EneCaloDopSeg->Fill(EnergyCaloDopSeg);
				        EneWCaloDopSeg->Fill(EnergyCaloDopSeg, CanbEffEvt);
				        Theta1stSeg->Fill(ThetaSegFirst);
						
				}
					
			}  // end of Loop over the crystals


			if(gam1 && gam2) 
			{ 
				Ncoinc++;
				//cout << "Coincidence observed at input evt #" << TotEvt << endl;
				//cout << "Coincidence observed at detected evt #" << AgataEvt << endl;
			}


     		// Fill other histo file with the total energy in each module
			for( int i=0; i<MaxCryst; i++)  // 8 module in central ring
			{
				if(Energy[i]>20.){

				// fill energy histogram
					Ene->Fill(Energy[i]);
					EneW->Fill(Energy[i],CanberraEff[i]); // for source at rest

				}
				   
			}


			// filling the Core multiplicity histogram
			    Mult->Fill(ModMult); // Core multiplicity
			    //cout << "Evt # =" << AgataEvt << " Mult=" << ModMult << endl;
			    //cout << " " << endl;
			    //cout << "##############################" << endl;



	               //
                       // For addback with closer neighbours
                       //
			for( int i=0; i<gClust; i++)
			{
			   if(EgClust[i]> 0.){
				 EnegClust->Fill(EgClust[i]);
				 EneWgClust->Fill(EgClust[i], CanbEffgClustEvt[i]);  

				 EgClustDop[i]=EgClust[i]*(1-beta*cos((ThetaFirstgClust[i])))/sqrt(1-beta*beta); // Dop. cor. at Crystal level; ToDo: ThetaCrystalfirst[i] for each cluster
				 EgClustDopSeg[i]=EgClust[i]*(1-beta*cos((ThetaFirstgClustSeg[i])))/sqrt(1-beta*beta); // Dop. cor. at Segment level;


				 EnegClustDop->Fill(EgClustDop[i]);
				 EneWgClustDop->Fill(EgClustDop[i], CanbEffgClustEvt[i]);

				 EnegClustDopSeg->Fill(EgClustDopSeg[i]);
				 EneWgClustDopSeg->Fill(EgClustDopSeg[i], CanbEffgClustEvt[i]);
			   }

			}


/*
	               //
                       // For addback within each ATC/ADC
                       //
			for( int i=0; i<MaxClust; i++)
			{
			   if(Ecluster[i]> 0.){
				 EneClust->Fill(Ecluster[i]);
				 EneWClust->Fill(Ecluster[i], CanbEffClustEvt[i]);


				 //EclusterDop[i]=Ecluster[i]*(1-beta*cos((ThetaFirst)))/sqrt(1-beta*beta); // Dop. cor. at Crystal level; ToDo: ThetaCrystalfirst[i] for each cluster
				 EclusterDop[i]=Ecluster[i]*(1-beta*cos((ThetaClustCryst1st[i])))/sqrt(1-beta*beta); // Dop. cor. at Crystal level; ToDo: ThetaCrystalfirst[i] for each cluster
				 EclusterDopSeg[i]=Ecluster[i]*(1-beta*cos((ThetaClustSeg1st[i])))/sqrt(1-beta*beta); // Dop. cor. at Segment level;


				 EneClustDop->Fill(EclusterDop[i]);
				 EneWClustDop->Fill(EclusterDop[i], CanbEffClustEvt[i]);

				 EneClustDopSeg->Fill(EclusterDopSeg[i]);
				 EneWClustDopSeg->Fill(EclusterDopSeg[i], CanbEffClustEvt[i]);
			   }

			}
*/
		      // Core - Core sprectrum:

	              if(ModMult>1){  // Core multiplicity is at least 2
 			for (int k=0;k<179; k++)
			  {
			        for(int kk=k+1; kk<MaxCryst; kk++) 
				   {
					if(Energy[k]>20 && Energy[kk]>20){
					
						// Non Doppler corrected Core-Core spectra
						EneCC->Fill(Energy[k],Energy[kk]);
	
						if(Energy[k]> (Egam1-AGsigmaCore) && Energy[k]< (Egam1+ AGsigmaCore)) EneGate->Fill(Energy[kk]); // Gated on EGam1
						if(Energy[kk]> (Egam1-AGsigmaCore) && Energy[kk]< (Egam1+ AGsigmaCore)) EneGate->Fill(Energy[k]); 


						// Doppler corrected (at crystal level) Core-Core spectra:
						EneCCDop->Fill(EnergyCoreDop[k],EnergyCoreDop[kk]);
	
						if(EnergyCoreDop[k]> (Egam1-AGsigmaCore) && EnergyCoreDop[k]< (Egam1+ AGsigmaCore)) EneGateDop->Fill(EnergyCoreDop[kk]); // Gated on EGam1
						if(EnergyCoreDop[kk]> (Egam1-AGsigmaCore) && EnergyCoreDop[kk]< (Egam1+ AGsigmaCore)) EneGateDop->Fill(EnergyCoreDop[k]); 

						// Doppler corrected (at Segment level) Core-Core spectra:
						EneCCDopSeg->Fill(EnergyCoreDopSeg[k],EnergyCoreDopSeg[kk]);
	
						if(EnergyCoreDopSeg[k]> (Egam1-AGsigmaCore) && EnergyCoreDopSeg[k]< (Egam1+ AGsigmaCore)) EneGateDopSeg->Fill(EnergyCoreDopSeg[kk]); // Gated on EGam1
						if(EnergyCoreDopSeg[kk]> (Egam1-AGsigmaCore) && EnergyCoreDopSeg[kk]< (Egam1+ AGsigmaCore)) EneGateDopSeg->Fill(EnergyCoreDopSeg[k]); 



					}
				   }
				
			   }
			}

			//Now we can reset values:
			Evt=dummy;
			CrystalId=-1;
			indice=-1;
			CanbEffEvt=1.;
			

			for( int i=0; i<MaxCryst; i++)
			{
			   Energy[i]= 0.;
			   for(int ii=0;ii<MaxMult;ii++)EnergyMul[i][ii]=0.;
			   //Time[i]=0;

			   for( int j=0; j<6; j++)
			      {
				for( int jj=0; jj<6; jj++)
				{
			          EnergySeg[i][j][jj]=0.;
				  for(int ii=0;ii<MaxMult;ii++)EnergyMulSeg[i][j][jj][ii]=0.;
				}
		 	     }
			}

			for( int i=0; i<MaxClust; i++)
			{
		 	   Ecluster[i]= 0.;
			   CanbEffClustEvt[i]=1.;	
		 	   EclusterDop[i]= 0.;
		 	   EclusterDopSeg[i]= 0.;
		 	   //Time[i]=0;
			}

			gam1=gam2=false;
			ModMult=0;

		} // end of hit true and Egam1st



	   }
           //if ((Key==-100) && Evt!=dummy){  // a new event with a incident gamma ray has been found so we record the previous event
	   else if(Key>=0)  // key(=Crystal) ; In that case dummy=Slice*10+Sector
	   {

	   //
	   //  Let's go through all the event of the input file
	   //

		hit=true;
		CrystalId= Key;    //

		//cout<< "Key=" << Key << endl;

		EnergyMul[CrystalId][InMult]+=nrj;  // sum the energy deposited in that crystal

		//cout << "energy= " << EnergyMul[CrystalId][InMult] << " for InMult=" << InMult << endl; 

		jSlice=int(SliSeg/10.);
		jSeg=SliSeg-jSlice*10;

		EnergyMulSeg[CrystalId][jSlice][jSeg][InMult]+=nrj;  // sum the energy deposited in that segment

		// cout << "energy=" << EnergySeg[CrystalId][jSlice][jSeg] << " CrystId=" << CrystalId << " Slice=" <<jSlice<< " Seg="<< jSeg << " SliSeg=" << SliSeg << endl;

	   }
	
 
	}	// end of the while loop over the entire file

		
	cout << "No. of input Events: " << TotEvt/inGammaMult << endl;	// = Number of Geant4 events / input gamma multiplicity 
	cout << "No. of detected Events (Evt with at least 1 hit): " << AgataEvt << endl;	//Report number of values   extracted		
	cout << "No. of recorded hit Crystals for the entire run: " << AgataCryst << endl;	//Report number of values extracted		
	if(argc >= 4)cout << "No. of "<< Egam1 << " keV gamma-rays : " << Nbgam1 << endl;	//Report number of values extracted		
	if(argc >= 4)cout << "No. of "<< Egam1 << " keV gamma-rays after add back : " << Nbgam1_add << endl;	//Report number of values extracted		

	if(argc == 5){
	cout << "No. of "<< Egam2 << " keV gamma-rays : " << Nbgam2 << endl;	//Report number of values extracted		
	cout << "No. of "<< Egam2 << " keV gamma-rays after add back : " << Nbgam2_add << endl;	//Report number of values extracted		
	cout << "No. of " << Egam2 << " keV in coincidence with " << Egam1 << " keV: " << Ncoinc << endl;	//Report number of values extracted		
	}

// closing ascii outputfile
//	outFile.close();

// writing and closing root file

	output_File->Write();
	output_File->Close();
	cout << "Root file agataTree.root created " << endl;	//Report number of values extracted		

// closing binary output file
//        BinFile.close();

return 0;  
} 

