
#include <stdlib.h>
#include <string.h> 
#include <stdio.h>

#include "matlib.h"

int     howManyMats;                    // numero di matrici
short  *matData[MMATRICES];             // puntatori alle matrici
int     matEmpty[MMATRICES];            // flag di matrice vuota
int     matLength[MMATRICES][2];        // numero di canali delle matrici
char    matName[MMATRICES][100];        // nome delle matrici
char    matInfo[MMATRICES][100];        // descrizione delle matrici
int     matBadType;                     // bad matrix  number in matincr...

int
matDefine(int nchanX, int nchanY, const char *name, const char *info)
{

  if(howManyMats == MMATRICES) {
    printf("Too many matrices: Max is %d\n", MMATRICES);
    printf("Increase MMATRICES in matlib.h");
    exit(-1);
  }

  matLength[howManyMats][0] = nchanX;
  matLength[howManyMats][1] = nchanY;
  strcpy(matName[howManyMats], name); 
  strcpy(matInfo[howManyMats], info); 
  matData[howManyMats]  = (short*)calloc(matLength[howManyMats][0]*matLength[howManyMats][1], sizeof(short));
  matEmpty[howManyMats] = 1;

  return howManyMats++;
}

short *
matPointer(int number)
{
  if(number < 0 || number >= howManyMats)       return NULL;
  return matData[number];
}

int
matHowMany(){
  return howManyMats;
}

void
matIncr(int mx, int my, int mn)
{
  int matsizex, matsizey;

  if(mn >= howManyMats)  {
   matBadType++;
    return;
  }

  matsizex = matLength[mn][0];
  matsizey = matLength[mn][1];
  if(mx >= matsizex) mx = matsizex - 1; if(mx < 0) mx = 0;
  if(my >= matsizey) my = matsizey - 1; if(my < 0) my = 0;
  matData[mn][mx*matsizey + my]++;
  matEmpty[mn] = 0;
}

void
matSave(int skipEmpty)
{
  int nn;
  
  for(nn = 0; nn < howManyMats; nn++) {
    if(skipEmpty && matEmpty[nn]) 
      printf("Empty %s (%s)\n", matName[nn], matInfo[nn]);
    else
      matWrite(matData[nn], matLength[nn][0], matLength[nn][1], matName[nn], matInfo[nn]);
  }
  
}

void
matWrite(short *matr, int nchanx, int nchany, char *name, char *linfo)
{
  FILE *ofp;
  int nn, totchan;
  
  totchan   = nchanx*nchany;
  
  if( !(ofp = fopen(name,"wb")) ) {
    printf("Error opening file %s\n", name);
    return;
  }
  
  nn = fwrite(matr, sizeof(short), totchan, ofp);
  
  if(nn ==  totchan )
    printf("File  %s  %d * %d matrix (%s)\n", name, nchanx, nchany, linfo);
  else 
    printf("Error writing matrix %s\n", name);  
  
  fclose(ofp);
}
