////////////////////////////////////////////////////
/////////// GEOMETRY: BOX3 /////////////////////////
////////////////////////////////////////////////////

//#include "mgt.h"
#include "mgt.extern.h"
#include "mgt_geobox3.h"

typedef struct {
  double  hsizeX;             // half-length along x
  double  hsizeY;             // half-length along y
  double  hsizeZ;             // half-length along z
  double  r_ibsphere;         // radius of inner bounding sphere
  double  r_ibsphere2;        // radius of inner bounding sphere squared
  double  r_obsphere;         // radius of outer bounding sphere
  double  r_obsphere2;        // radius of outer bounding sphere squared
  double  ssegsX;             // size of segment along x
  double  ssegsY;             // size of segment along y
  double  ssegsZ;             // size of segment along z
  int     nsegsX;             // number of segments along x
  int     nsegsY;             // number of segments along y
  int     nsegsZ;             // number of segments along z
  int     nsegs;              // number of segments
} box3;

box3   *geobox3;

//int     nbshit;               // number of detector bounding spheres hit by the ray 1-2
//int    *ibshit;               // which bounding spheres are hit

//double  dist12;               // spatial distance between the 2 considered points

int     insidegedet_box3    (point *);               //
void    findsegment_box3    (point *);               //
void    setsegcenter_box3   (point *);               //
double  gedistance_box3     (point *, point *);      //

void
geominit_box3()
{
  char   line[256];
  int    ii, nn, nsdet, nsmax;
  double sx, sy, sz, cx, cy, cz;
  int    nx, ny, nz, ns;
  box3  *pb;
  
  insidegedet  = insidegedet_box3;
  findsegment  = findsegment_box3;
  setsegcenter = setsegcenter_box3;
  gedistance   = gedistance_box3;

  if (fscanf(ifp, "%s", line) != 1) {
    printf("Error reading %s\n", ifname);
    printf("Keyword SUMMARY expected\n");
    errexit("geominit_box3");
  }
  if(strcmp(line, "SUMMARY")) {
    printf("Error reading %s\n", ifname);
    printf("Keyword SUMMARY expected\n");
    errexit("geominit_box3");
  }
  
  if(fscanf(ifp, "%d", &ngedets) != 1) {
    printf("Error reading %s\n", ifname);
    errexit("geominit_box3");
  }

  printf("Initializing geometry for BOX3 with %d detectors\n", ngedets);
  
  geobox3 = (box3  *)calloc(ngedets, sizeof(box3));
  CDet    = (coors *)calloc(ngedets, sizeof(coors));
  ibshit  = (int   *)calloc(ngedets, sizeof(int));

  nsmax = 0;
  for(nn = 0; nn < ngedets; nn++) {
    if (fscanf(ifp, "%d", &ii) != 1) {
      printf("Error reading geometry data from %s\n", ifname);
      errexit("geominit_box3");
    }
    if (ii != nn) {
      printf("Problem reading geometry data from %s\n", ifname);
      errexit("geominit_box3");
    }
    else {
      if (fscanf(ifp, "%lf %lf %lf %lf %lf %lf %d %d %d",
              &sx, &sy, &sz, &cx, &cy, &cz, &nx, &ny, &nz) != 9) {
        printf("Error reading geometry data from %s\n", ifname);
        errexit("geominit_box3");
      }
      sx *= distfactor;
      sy *= distfactor;
      sz *= distfactor;
      cx *= distfactor;
      cy *= distfactor;
      cz *= distfactor;
      pb = geobox3 + nn;
      pb->hsizeX  = sx/2;
      pb->hsizeY  = sy/2;
      pb->hsizeZ  = sz/2;
      CDet[nn].xx = cx;
      CDet[nn].yy = cy;
      CDet[nn].zz = cz;
      pb->r_ibsphere  = MIN(pb->hsizeX, pb->hsizeY);
      pb->r_ibsphere  = MIN(pb->r_ibsphere, pb->hsizeZ);
      pb->r_ibsphere2 = pb->r_ibsphere * pb->r_ibsphere;
      pb->r_obsphere  = sqrt(pow(pb->hsizeX,2.) + pow(pb->hsizeY,2.) + pow(pb->hsizeZ,2.)); 
      pb->r_obsphere2 = pb->r_obsphere * pb->r_obsphere;
      pb->nsegsX = MAX(1, nx);
      pb->nsegsY = MAX(1, ny);
      pb->nsegsZ = MAX(1, nz);
      pb->ssegsX = 2*pb->hsizeX / pb->nsegsX;
      pb->ssegsY = 2*pb->hsizeY / pb->nsegsY;
      pb->ssegsZ = 2*pb->hsizeZ / pb->nsegsZ;
      nsdet = pb->nsegsX * pb->nsegsY * pb->nsegsZ;
      nsmax = MAX(nsmax, nsdet);
    }
  }
  
  if (fscanf(ifp, "%s", line) != 1) {
    printf("Error reading %s\n", ifname);
    printf("Keyword ENDGEOMETRY expected\n");
    errexit("geominit_box3");
  }
  if(strcmp(line, "ENDGEOMETRY")) {
    printf("Error reading %s\n", ifname);
    printf("Keyword ENDGEOMETRY expected\n");
    errexit("geominit_box3");
  }
  
  nsegdet = nsmax;              // to equally space them in CSeg
  nsegtot = nsegdet * ngedets;
  CSeg    = (coors *)calloc(nsegtot, sizeof(coors));

  pb = geobox3; 
  for(nn = 0; nn < ngedets; nn++, pb++) {
    for(nx = 0; nx < pb->nsegsX; nx++) {
      for(ny = 0; ny < pb->nsegsY; ny++) {
        for(nz = 0; nz < pb->nsegsZ; nz++) {
          ns  = nz + pb->nsegsZ*(ny + pb->nsegsY*nx);
          ns += nn*nsegdet;
          CSeg[ns].xx = -pb->hsizeX + pb->ssegsX*(1.+2.*nx)/ 2.;
          CSeg[ns].yy = -pb->hsizeY + pb->ssegsY*(1.+2.*ny)/ 2.;
          CSeg[ns].zz = -pb->hsizeZ + pb->ssegsZ*(1.+2.*nz)/ 2.;
        }
      }
    }
  }

  GEOMETRY = BOX3;
}

void
setbshits_box3(coors *p1, coors *p2)
{
  int     ndet1, ndet2, ii;
  double  dbs, dbs2;
  double  c12x, c12y, c12z;
  double  c1cx, c1cy, c1cz;
  double  costh, cosbs;
  double  xx, yy, zz;
  box3   *pb1, *pb2, *pb3;
  
  ndet1  = p1->nd;
  ndet2  = p2->nd;
  pb1    = geobox3 + ndet1;
  pb2    = geobox3 + ndet2;
  nbshit = 0;
  c12x   = (p2->xx - p1->xx)/dist12;
  c12y   = (p2->yy - p1->yy)/dist12;
  c12z   = (p2->zz - p1->zz)/dist12;
  for(ii = 0; ii < ngedets; ii++) {
    if(ii != ndet1 && ii != ndet2) {
      pb3  = geobox3 + ii;
      xx   = CDet[ii].xx - p1->xx;
      yy   = CDet[ii].yy - p1->yy;
      zz   = CDet[ii].zz - p1->zz;
      dbs2 = xx*xx + yy*yy + zz*zz;
      dbs  = sqrt(dbs2);
      if((dbs - pb3->r_obsphere) > dist12) continue;
      c1cx  = (CDet[ii].xx - p1->xx) / dbs;
      c1cy  = (CDet[ii].yy - p1->yy) / dbs;
      c1cz  = (CDet[ii].zz - p1->zz) / dbs;
      costh = c12x*c1cx + c12y*c1cy + c12z*c1cz;
      if(costh < 0.0) continue;
      cosbs = sqrt(dbs2 - pb3->r_obsphere2) / dbs;
      if(costh <= cosbs) continue;
      ibshit[nbshit++] = ii;
    }
  }
}

int
insidegedet_box3(point *pp)
{
  int     num_det;
  double  xr, yr, zr;
  box3   *pb;

  num_det = pp->nd;
  pb      = geobox3 + num_det;
  
  xr = pp->xx - CDet[num_det].xx;
  if(fabs(xr) > pb->hsizeX) return 0;

  yr = pp->yy - CDet[num_det].yy;
  if(fabs(yr) > pb->hsizeY) return 0;

  zr = pp->zz - CDet[num_det].zz;
  if(fabs(zr) > pb->hsizeZ) return 0;
  
  return 1;
}

void
findsegment_box3(point *pp)
{
  int    nd, nx, ny, nz, ns;
  double px, py, pz;
  box3  *pb;

  nd = pp->nd;
  pb = geobox3 + nd;

  px = pp->xx - CDet[nd].xx + pb->hsizeX;
  py = pp->yy - CDet[nd].yy + pb->hsizeY;
  pz = pp->zz - CDet[nd].zz + pb->hsizeZ;

  nx = (int)(px / pb->ssegsX);
  ny = (int)(py / pb->ssegsY);
  nz = (int)(pz / pb->ssegsZ);

  ns = nz + pb->nsegsZ*(ny + pb->nsegsY*nx);

#if STATINSIDESEG == 1
  specIncr(nd       , 19, 0);
  specIncr(nx + 1000, 19, 0);
  specIncr(ny + 1500, 19, 0);
  specIncr(nz + 2000, 19, 0);
#endif

  pp->ns  = ns;
  pp->seg = ns + nsegdet * pp->nd;

}

void
setsegcenter_box3(point *pp)
{
  static int initit = 1;
  box3 *pb;
  coors *cge;
  coors *csg;
  
  pb  = geobox3 + pp->nd;
  cge = CDet    + pp->nd;
  csg = CSeg    + pp->seg;
  pp->xx = csg->xx + cge->xx;
  pp->yy = csg->yy + cge->yy;
  pp->zz = csg->zz + cge->zz;
  setcosdir(pp);
  return;

}

// distance to exit detector p1->nd along p1-p2 line
double
detdist1_box3(coors *p1, coors *p2)
{
  int     nn;
  double  pr1x, pr1y, pr1z;
  double  pr2x, pr2y, pr2z;
  double  d12x, d12y, d12z;
  double  xhit, yhit, zhit, thit, dist;
  box3  *pb;

  nn   = p1->nd;
  pb   = geobox3 + nn;

  pr1x = p1->xx - CDet[nn].xx;
  pr1y = p1->yy - CDet[nn].yy;
  pr1z = p1->zz - CDet[nn].zz;

  pr2x = p2->xx - CDet[nn].xx;
  pr2y = p2->yy - CDet[nn].yy;
  pr2z = p2->zz - CDet[nn].zz;

  d12x  = (pr2x - pr1x);
  d12y  = (pr2y - pr1y);
  d12z  = (pr2z - pr1z);

  if(d12x != 0.) {
    thit = (pb->hsizeX - pr1x) / d12x;        // prova la faccia x superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        yhit = pr1y + d12y*thit;
        zhit = pr1z + d12z*thit;
        yhit = ABS(yhit);
        zhit = ABS(zhit);
        if(yhit < pb->hsizeY && zhit < pb->hsizeZ) {
          dist = thit * dist12;
          return dist;
        }
      }
      thit = (-pb->hsizeX - pr1x) / d12x;     // prova la faccia x inferiore
    }
  }

  if(d12y != 0.) {
    thit = (pb->hsizeY - pr1y) / d12y;        // prova la faccia y superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        zhit = pr1z + d12z*thit;
        xhit = pr1x + d12x*thit;
        zhit = ABS(zhit);
        xhit = ABS(xhit);
        if(zhit < pb->hsizeZ && xhit < pb->hsizeX) {
          dist = thit * dist12;
          return dist;
        }
      }
      thit = (-pb->hsizeY - pr1y) / d12y;     // prova la faccia y inferiore
    }
  }

  if(d12z != 0.) {
    thit = (pb->hsizeZ - pr1z) / d12z;        // prova la faccia z superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        xhit = pr1x + d12x*thit;
        yhit = pr1y + d12y*thit;
        xhit = ABS(xhit);
        yhit = ABS(yhit);
        if(xhit < pb->hsizeX && yhit < pb->hsizeY) {
          dist = thit * dist12;
          return dist;
        }
      }
      thit = (-pb->hsizeZ - pr1z) / d12z;     // prova la faccia z inferiore
    }
  }

  return 0.;
}

// intercept of p1-p2 straight line to box detector nn
double
detdist2_box3(coors *p1, coors *p2, int nn)
{
  double  pr1x, pr1y, pr1z;
  double  pr2x, pr2y, pr2z;
  int     ns;
  double  d12x, d12y, d12z;
  double  xhit, yhit, zhit, thit, dist;
  double  tt[4];
  box3   *pb;

  pb   = geobox3 + nn;

  pr1x = p1->xx - CDet[nn].xx;
  pr1y = p1->yy - CDet[nn].yy;
  pr1z = p1->zz - CDet[nn].zz;

  pr2x = p2->xx - CDet[nn].xx;
  pr2y = p2->yy - CDet[nn].yy;
  pr2z = p2->zz - CDet[nn].zz;

  d12x  = (pr2x - pr1x);
  d12y  = (pr2y - pr1y);
  d12z  = (pr2z - pr1z);

  ns = 0;
  if(d12x != 0.) {
    thit = (pb->hsizeX - pr1x) / d12x;        // prova la faccia x superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        yhit = pr1y + d12y*thit;
        zhit = pr1z + d12z*thit;
        yhit = ABS(yhit);
        zhit = ABS(zhit);
        if(yhit < pb->hsizeY && zhit < pb->hsizeZ) {
          tt[ns++] = thit;
          if(ns == 2) {
            dist = fabs(tt[1] - tt[2]) * dist12;
            return dist;
          }
        }
      }
      thit = (-pb->hsizeX - pr1x) / d12x;     // prova la faccia x inferiore
    }
  }

  if(d12y != 0.) {
    thit = (pb->hsizeY - pr1y) / d12y;        // prova la faccia y superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        zhit = pr1z + d12z*thit;
        xhit = pr1x + d12x*thit;
        zhit = ABS(zhit);
        xhit = ABS(xhit);
        if(zhit < pb->hsizeZ && xhit < pb->hsizeX) {
          tt[ns++] = thit;
          if(ns == 2) {
            dist = fabs(tt[1] - tt[2]) * dist12;
            return dist;
          }
        }
      }
      thit = (-pb->hsizeY - pr1y) / d12y;     // prova la faccia y inferiore
    }
  }

  if(d12z != 0.) {
    thit = (pb->hsizeZ - pr1z) / d12z;        // prova la faccia z superiore
    for(nn = 0; nn < 1; nn++) {
      if(thit > 0.0 && thit <= 1.0) {
        xhit = pr1x + d12x*thit;
        yhit = pr1y + d12y*thit;
        xhit = ABS(xhit);
        yhit = ABS(yhit);
        if(xhit < pb->hsizeX && yhit < pb->hsizeY) {
          tt[ns++] = thit;
          if(ns == 2) {
            dist = fabs(tt[1] - tt[2]) * dist12;
            return dist;
          }
        }
      }
      thit = (-pb->hsizeZ - pr1z) / d12z;     // prova la faccia z inferiore
    }
  }
  return 0.;
}

double
gedistance_box3(point *p1, point *p2)
{
  coors  pev1, pev2;
  int    ndet1, ndet2, ii;
  double dist, totd;
  double xx, yy, zz;
  box3  *pb1, *pb2;

  xx = p2->xx - p1->xx;
  yy = p2->yy - p1->yy;
  zz = p2->zz - p1->zz;
  dist12 = sqrt(xx*xx + yy*yy + zz*zz);

  ndet1  = p1->nd;
  ndet2  = p2->nd;
  if(ndet1 == ndet2 && ndet1 >= 0) return dist12;

  pb1 = geobox3 + ndet1;
  pb2 = geobox3 + ndet2;

  pev1.xx = p1->xx;
  pev1.yy = p1->yy;
  pev1.zz = p1->zz;
  pev1.nd = p1->nd;

  pev2.xx = p2->xx;
  pev2.yy = p2->yy;
  pev2.zz = p2->zz;
  pev2.nd = p2->nd;

  totd = 0;

  if(ndet1 >= 0) totd += detdist1_box3(&pev1, &pev2);
  if(ndet2 >= 0) totd += detdist1_box3(&pev2, &pev1);

  setbshits_box3(&pev1, &pev2);
  
  if(nbshit) {
    for(ii = 0; ii < nbshit; ii++) {
      dist  = detdist2_box3(&pev1, &pev2, ibshit[ii]);
      totd += dist;
    }
  }
  return totd;
}
