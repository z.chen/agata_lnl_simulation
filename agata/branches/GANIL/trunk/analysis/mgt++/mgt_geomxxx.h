
extern int     nsegsZ;               // number of segments along  detector axis
extern int     nsegsP;               // number of segments around detector axis
extern int     nsegsR;               // number of segments along  detector radius


double  rsh_int;              // inner radius of shell
double  rsh_int2;             // inner radius of shell squared
double  rsh_out;              // outer radius of shell

