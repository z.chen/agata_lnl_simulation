#if !defined(SPEC_LIB__INCLUDED_)
#    define  SPEC_LIB__INCLUDED_

#define MSPECTRA     256     // Max number of composite spectra

int     specDefine    (int nch, int nspe, const char *name, const char *info);    // define a new spectrum
void    specSetGain   (double gain);                                  // define gain for specincrEgain
int    *specPointer   (int ss, int nn);                               // get pointer to spectrum
int     specHowMany   ();                                             // returns number of spectra defined
void    specIncr      (int    chan, int ss, int nn);                  // increment subspectrum ss of spectrum nn 
void    specIncrValue (int    chan, int ss, int nn, int value);       // increment += value
void    specIncrGain  (double ener, int ss, int nn);                  // increment with Egain
void    specSave      (int);                                          // save all defined spectra
void    specWrite     (int *, int nchan, int nspe,  char *, char *);  // save spectra as binary LW
void    specWriteASCII(int *, int chan1, int chan2, char *name);      // save spectra as ASCII table
int     specRead      (int *, int nchan, int nspe,  char *name);      // read spectra as binary LW

#endif // !defined(SPEC_LIB__INCLUDED_)
