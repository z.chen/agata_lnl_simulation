////////////////////////////////////////////////////
////////// GEOMETRY: SHELL /////////////////////////
////////////////////////////////////////////////////

//#include "mgt.h"
#include "mgt.extern.h"
#include "mgt_geoshell.h"

//double  rsh_int;              // inner radius of shell
//double  rsh_int2;             // inner radius of shell squared

double  rsh_ext;              // outer radius of shell
double  rsh_ext2;             // outer radius of shell squared
double  rsh_thick;            // thickness    of shell

//int     nsegsR;               // number of segments along  radius
//int     nsegsP;               // number of segments for phi
int     nsegsT;               // number of segments for theta
//double  ssegsR;               // size of segment R
//double  ssegsP;               // size of segment phi
double  ssegsT;               // size of segment theta

double  shell_dd, shell_dp, shell_d1;   // distanza12^2, p1*dir, p1^2 (A, B, C+rr^2 dell'equazione dell'intercetta)
double  shell_t1, shell_t2;             // le due soluzioni dell'equazione di secondo grado

int     insidegedet_shell   (point *);
void    findsegment_shell   (point *);
void    setsegcenter_shell  (point *);
double  gedistance_shell    (point *, point *);
int     hitdet_shell        (double);

void
geominit_shell()
{
  double dim1, dim2;
  char   dummy_line[128];
      
  insidegedet  = insidegedet_shell;
  findsegment  = findsegment_shell;
  setsegcenter = setsegcenter_shell;
  gedistance   = gedistance_shell;

  if(fscanf(ifp, "%lf %lf", &dim1, &dim2) != 2) {
    printf("Error reading %s\n", ifname);
    errexit("geominit_shell");
  }
  dim1 *= distfactor;
  dim2 *= distfactor;
  if( dataFileVersion == FAGATA ) {
    fscanf( ifp, "\n%s", dummy_line );  // ENDGEOMETRY line
  }
  printf("Initializing geometry for the SHELL configuration\n");
  CDet = (coors *)calloc(1, sizeof(coors));
//if(fscanf(ifp, "%lf %lf %lf", &CDet[0].xx, &CDet[0].yy, &CDet[0].zz) != 3) {
//  printf("Error reading geometry data from %s\n", ifname);
//  errexit("geominit_shell");
//}
//CDet[0].xx *= distfactor;
//CDet[0].yy *= distfactor;
//CDet[0].zz *= distfactor;

  ngedets   = 1;
  rsh_int   = dim1 - 0.1;
  rsh_int   = MAX(0., rsh_int);
  rsh_ext   = dim2 + 0.1;
  rsh_int2  = rsh_int * rsh_int;
  rsh_ext2  = rsh_ext * rsh_ext;
  rsh_thick = rsh_ext - rsh_int;
  nsegsR    =  6;
  nsegsP    = 32;
  nsegsT    = 32;
  nsegdet   = nsegsR * nsegsP * nsegsT;
  ssegsR    = (rsh_ext - rsh_int) / nsegsR;
  ssegsP    = (2. * M_PI  ) / nsegsP;
  ssegsT    = 2. / nsegsT;
  nsegtot   = nsegdet * ngedets;
  GEOMETRY  = SHELL;
  sprintf(segmentstring, "(R=%d Theta=%d Phi=%d)", nsegsR, nsegsT, nsegsP);
}

int
insidegedet_shell(point *pp)
{
  return (pp->rr >= rsh_int) && (pp->rr <= rsh_ext);
}

void
findsegment_shell(point *pp)
{
  int nr, nt, np, ns;
  double ph;

  nr = (int)((pp->rr  - rsh_int) / ssegsR);
  nt = (int)((1.      - pp->cz ) / ssegsT);
  ph = atan2(pp->cy, pp->cx);
  if(ph <= 0.) ph +=2.*M_PI;
  np = (int)(ph / ssegsP);
  ns = np + nsegsP * (nt + nsegsP * nr);

#if STATINSIDESEG == 1
  specIncr(nr       , 19, 0);
  specIncr(nt + 1000, 19, 0);
  specIncr(np + 1500, 19, 0);
#endif

  pp->ns = ns;
  pp->seg = ns; // + nsegdet * pp->nd;
}

void
setsegcenter_shell(point *pp)
{
  static int initit = 1;
  int     nr, nt, np, ns;
  double  rr, th, ph;
  double  dist = 0.;
  coors  *csg;

  if(initit) {
    CSeg = (coors *)calloc(nsegtot, sizeof(coors));
    for(nr = 0; nr < nsegsR; nr++) {
    for(nt = 0; nt < nsegsT; nt++) {
    for(np = 0; np < nsegsP; np++) {
      ns = np + nsegsP * (nt + nsegsP * nr);
      rr = nr * ssegsR + 0.5 * ssegsR + rsh_int;
      th = acos(1. - nt * ssegsT - 0.5 * ssegsT);
      ph = np * ssegsP + 0.5 * ssegsP;
      CSeg[ns].xx = rr * cos(ph) * sin(th);
      CSeg[ns].yy = rr * sin(ph) * sin(th);
      CSeg[ns].zz = rr *           cos(th);
      CSeg[ns].ns = ns;
      CSeg[ns].nd = 0;
    }
    }
    }
    initit = 0;
  }

  csg  = CSeg + pp->ns;
#if STATINSIDESEG == 1
  dist = sqrt(pow(pp->xx - csg->xx, 2.) + pow(pp->yy - csg->yy, 2.) +pow(pp->zz - csg->zz, 2.));
  specIncr((int)dist+3000, 19, 0);
#endif
  pp->xx = csg->xx;
  pp->yy = csg->yy;
  pp->zz = csg->zz;
  setcosdir(pp);
  return;
}

double
gedistance_shell(point *p1, point *p2)
{
  point *pt;
  int nn;
  double dx, dy, dz;   // numeri direttori 1->2
  double d12, dist;

  if(p1->rr > p2->rr) SWAP(p1, p2, pt)

// casi particolari
  if(p1->rr <= rsh_int) {
    if(p2->rr <= rsh_int) 
      return 0.0;         // (p1, p2) < Rint
    if(p1->rr == 0.0) {   // p1 al centro della shell
      if(p2->rr < rsh_ext) return p2->rr - rsh_int;
      return rsh_ext - rsh_int;
    }
  }

  dx = p2->xx - p1->xx;                   // i numeri direttori della retta da p1 a p2
  dy = p2->yy - p1->yy;
  dz = p2->zz - p1->zz;

  shell_dd = dx*dx + dy*dy + dz*dz;       // distanza tra i due punti
  if(shell_dd == 0.) return 0;
  d12 = sqrt(shell_dd);

  shell_dp = p1->xx*dx     + p1->yy*dy     + p1->zz*dz;
  shell_d1 = p1->xx*p1->xx + p1->yy*p1->yy + p1->zz*p1->zz;

// il primo punto e' nello spazio vuoto interno
  if(p1->rr <= rsh_int) {
    dist = 1.;    // distanza normalizzata da cui sottrarre i pezzi in vuoto
    if(p2->rr >= rsh_ext) {
      nn = hitdet_shell(rsh_ext);
      if(nn != 1) {
        printf("Does not hit outer surface (1)\n");
        errexit("getdistance_shell");
      }
      dist = shell_t1;
    }
    nn = hitdet_shell(rsh_int);
    if(nn != 1) {
        printf("Does not hit inner surface (1)\n");
        errexit("getdistance_shell");
    }
    dist -= shell_t1;
    return dist * d12;
  }

// il primo punto e' tra le due superfici sferiche
  if(p1->rr <= rsh_ext) {
    dist = 1.;    // distanza normalizzata da cui sottrarre i pezzi in vuoto
    if(p2->rr >= rsh_ext) {
      nn = hitdet_shell(rsh_ext);
      if(nn != 1) {
        printf("Does not hit outer surface (2)\n");
        errexit("getdistance_shell");
      }
      dist = shell_t1;
    }
    nn = hitdet_shell(rsh_int);
    if(nn > 1) dist -= shell_t2 - shell_t1;
    return dist * d12;
  }
  
// entrambi i punti sono esterni
  nn = hitdet_shell(rsh_ext);
  if(nn < 2) return 0.0;
  dist = shell_t2 - shell_t1;
  nn = hitdet_shell(rsh_int);
  if(nn > 1) dist -= shell_t2 - shell_t1;
  return dist * d12;

}

int
hitdet_shell(double rr)
{
  double A, B, C, sq;

  A = shell_dd;
  B = shell_dp;
  C = shell_d1 - rr*rr;
  
  sq = B*B - A*C;
  if(sq < 0.) {
    return 0;
  }
  if(sq == 0.) {
    shell_t1 = -B / A;
    return (shell_t1 >= 0.) ? (1) : (0);
  }

  sq = sqrt(sq);
  shell_t1 = (-B + sq) / A;
  if(shell_t1 < 0.)
    return 0;
  shell_t2 = (-B - sq) / A;
  if(shell_t2 < 0.)
    return 1;
  if(shell_t1 > shell_t2) SWAP(shell_t1, shell_t2, sq)
  return 2;
}
