//#include "mgt.h"
#include "mgt.extern.h"

#define PHOTODIST       40.           // min. distance (mm) from other points to accept photoelectric effect
#define PHOTOFACT        5.           // scale factor for chi2 of photoelectric effect (also to consider /dist_dd)
#define CLUSTDIST       60.           // min. distance (mm) around a cluster to increase acceptance
#define CHI2TIMES       1.0           // works with chi2Accept*CHI2TIMES but accepts only until chi2Accept
//#define CHI2TIMES      10.0           // works with chi2Accept*CHI2TIMES but accepts only until chi2Accept
//#define CHI2TIMES     100.0           // works with chi2Accept*CHI2TIMES but accepts only until chi2Accept

#define WEIGHTKN         1            // to use Klein-Nishina weighing

#define CHI2SCALE_CVE   20.0          // empiric scale factor in testcomptonVE  to get results around 1
#define CHI2SCALE_CVe    0.5          // empiric scale factor in testcomptonVe  to get results around 1
#define CHI2SCALE_CVA    5.0          // empiric scale factor in testcomptonVA  to get results around 1

#define CHI2SCALE_CVES   0.05         // empiric scale factor in testcomptonVES to get results around 1
#define CHI2SCALE_CVeS   0.2          // empiric scale factor in testcomptonVeS to get results around 1
#define CHI2SCALE_CVAS   0.2          // empiric scale factor in testcomptonVAS to get results around 1

#define SCALECHI2        1            // to scale chi2 limit to ~1 (in chisquare formulation)
#define EXTRAWEIGHTS     1            // add empiric weights to chisquare formulation

#define MODSPROB         1            // to modify interaction probabilities due to packing

double lcomp[MAXEKEV];                // mean free path for compton scattering
double lphot[MAXEKEV];                // mean free path for photoelectric effect
double lpair[MAXEKEV];                // mean free path for pair production
double latot[MAXEKEV];                // total mean free path
double mutot[MAXEKEV];                // 1/ total mean free path
double scomp[MAXEKEV];                // relative probability for compton scattering
double sphot[MAXEKEV];                // relative probability for photoelectric effect
double spair[MAXEKEV];                // relative probability for pair production 

int    nsharing[MAXEVLEN];  // number of clusters to which a point belongs (sortclusters & combineclust)
int    nshared[MAXEVLEN];   // a quanti cluster appartiene un punto

double Wchi2[MAXPOINTS]   = //  chi2 weights for compton vertices
                            //  {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0}; 
                                {1.0, 1.0, 0.5, 0.2, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1};
                            //  {1.0, 1.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
double Pchi2[MAXPOINTS];    //  and normalization

double X2Ftype[10]        = //  chi2 weights according to cluster type (up to 10 different types)
                            //  {1.0, 1.0, 1.0, 1.0 , 1.0 , 1.0 , 1.0 , 1.0 , 1.0 , 1.0 };
                            //  {1.0, 0.1, 0.5, 0.05, 0.02, 0.01, 0.01, 0.01, 0.01, 0.01};
                                {2.0, 0.3, 0.5, 0.05, 0.02, 0.01, 0.01, 0.01, 0.01, 0.01};
double X2Fmult[MAXPOINTS] = //  chi2 weights according to number of points in cluster ==> should become energy dependent
                            //  {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
                                {1.0, 1.0, 0.2, 2.0, 4.0, 3.0, 1.5, 0.8, 0.6, 0.4};   // for 1333
                            //  {1.0, 1.0, 0.5, 3.0, 3.0, 1.5, 1.0, 1.0, 1.0, 1.0};   // for wide range

double cluth[11]          = //  max cluster opening every 100 keV
                                {10., 20., 23., 28., 31., 33., 34., 36., 38., 39., 40.};
                            //  {12., 25., 29., 35., 39., 41., 43., 45., 47., 49., 50.};

int     firstVtx = 0;       // global variable for first vertex
int     lastVtx  = 0;       // global variable for last vertex

// function prototypes

void     initlambda     ();                             // read scattering length in germanium
void     checkgoodclust (clust *);                      // check which gammas involved in cluster
int      sortclusters   ();                             // sort pool of clusters
void     acceptsorted   (int);                          // accept a cluster while sorting clusterpool
int      checkmultiple  ();                             // verifica che i punti accettati non siano in piu' gamma
void     statclusters   ();                             // statistics of the clusters in the pool
void     clusterspectra (clust *);                      // various spectra for accepted clusters
void     printclustpool ();                             // print pool of "track-checked" clusters
void     printclustgood ();                             // print accepted clusters
void     printclustrest ();                             // print residual clusters
void     printaccepted  (clust *);                      // print cluster-tracked gammas
void     printsummary2  (int, int, int);                // print summary of tracked clusters
void     printgamgood   (point **, int);                // print cluster-reconstructed gammas
double   wkn1           (double e1, double e2);         // Klein-Nishina cross section
double   wkn2           (double ee, double s2);         // Klein-Nishina cross section

// function pointers to unify the various versions
void   (*testphoto)     (clust *);                            // "tracking" of 1-point clusters (depends on geometry functions)
void   (*testpair1)     (clust *);                            // pair production "tracking" of a cluster
void   (*testcompt)     (clust *);                            // compton scattering tracking of a cluster
double (*tcomptVtx)     (point *, point *, point *, double);  // compton vertex function
double (*figOfMerit)    (point **pp, int np);                 // figure of merit for the np points in sequence

// Chisquare version of the functions
void     testphotoC     (clust *);                            // "tracking" of 1-point clusters (depends on geometry functions)
void     testpair1C     (clust *);                            // pair production "tracking"
void     testcomptC     (clust *);                            // starts compton scattering tracking of a cluster
void     testcomptCR    (int, double, double);                // recursively find best permutation of Compton vertices
double   tcomptVtxCE    (point *, point *, point *, double);  // Compton Vertex using normalized delta E'
double   tcomptVtxCe    (point *, point *, point *, double);  // Compton Vertex using normalized delta e1
double   tcomptVtxCA    (point *, point *, point *, double);  // Compton Vertex using delta angle
double   tcomptVtxCES   (point *, point *, point *, double);  // Compton Vertex using weighted delta E'
double   tcomptVtxCeS   (point *, point *, point *, double);  // Compton Vertex using eeighted delta e1
double   tcomptVtxCAS   (point *, point *, point *, double);  // Compton Vertex using weighted delta angle
double   figOfMeritC    (point **pp, int np);                 // figure of merit for the np points in sequence

// Likelihood version of the functions
void     testphotoL     (clust *);                            // "tracking" of 1-point clusters (depends on geometry functions)
void     testpair1L     (clust *);                            // pair production "tracking"
void     testcomptL     (clust *);                            // starts compton scattering tracking of a cluster
void     testcomptLR    (int, double, double);                // recursively find best permutation of Compton vertices
double   tcomptVtxLE    (point *, point *, point *, double);  // Compton Vertex using normalized delta E'
double   tcomptVtxLe    (point *, point *, point *, double);  // Compton Vertex using normalized delta e1
double   tcomptVtxLA    (point *, point *, point *, double);  // Compton Vertex using delta angle
double   tcomptVtxLES   (point *, point *, point *, double);  // Compton Vertex using weighted delta E'
double   tcomptVtxLeS   (point *, point *, point *, double);  // Compton Vertex using eeighted delta e1
double   tcomptVtxLAS   (point *, point *, point *, double);  // Compton Vertex using weighted delta angle
double   figOfMeritL    (point **pp, int np);                 // figure of merit for the np points in sequence

 // an attempt to quantify the membership of points to clusters ==> to be completed??
void     setCluWeights  ();

 // an attempt estimate the energy missing from accepted clusters
double   restEnerCe     (point **pp, int np);


#ifdef FORPRISMA
void addGammaToFile( clust* );
#endif


/////////////////////////////////////////////////////////////////////
///// structure to isolate Compton and PairProduction tracking //////
/////////////////////////////////////////////////////////////////////

typedef struct {
  double X2best;                        // chi2 during permutations
  int    npc;                           // number of points
  int    nnn;                           // permutation number
  clust *clu;                           // relating to this cluster
  point *ppc[MAXPOINTS+1];              // the points, from tc.clu->ppc
  int    bperm[MAXPOINTS+1];            // indices of best permutation
  int    pperm[MAXPOINTS+1];            // recursive generation of permutation
} trackstuff;

trackstuff tc;

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

void
inittrack()
{
  int ii;

#if EXTRAWEIGHTS != 1
  for (ii = 0; ii <  MAXPERM; ii++) Wchi2[ii] = 1.;
#endif

  Pchi2[0] = 0;
  for (ii = 1; ii <  MAXPERM; ii++) Pchi2[ii] = Pchi2[ii - 1] + Wchi2[ii];
 
  strcpy(mechanism[0], "None ");
  strcpy(mechanism[1], "Photo");
  strcpy(mechanism[2], "Compt");
  strcpy(mechanism[3], "Pair0");
  strcpy(mechanism[4], "Pair1");
  strcpy(mechanism[5], "Pair2");

  chi2Track = CHI2TIMES * chi2Accept;

  initlambda();       // read scattering lengths

  checkPh = 1;        // the default is to check for photoelectric effect
  checkPP = 1;        // the default is to check for pair-production

  if(vertexStyle == VCHISQUARE) {
    testphoto  = testphotoC;
    testpair1  = testpair1C;
    testcompt  = testcomptC;
    figOfMerit = figOfMeritC;
    switch (vertexFunct) {
    case  1:
      tcomptVtx = tcomptVtxCE;
      sprintf(cvertexstring, "[E1(ener) - E1(pos)]/E0");
      break;
    case -1:
      tcomptVtx = tcomptVtxCES;
      sprintf(cvertexstring, "[E1(ener) - E1(pos)]/sigma");
      break;
    case  2:
      tcomptVtx = tcomptVtxCe;
      sprintf(cvertexstring, "[e1(ener) - e1(pos)]/e1(ener)");
      break;
    case -2:
      tcomptVtx = tcomptVtxCeS;
      sprintf(cvertexstring, "[e1(ener) - e1(pos)]/sigma");
      break;
    case  3:
      tcomptVtx = tcomptVtxCA;
      sprintf(cvertexstring, "[cosP - cosE]");
      break;
    case -3:
      tcomptVtx = tcomptVtxCAS;
      sprintf(cvertexstring, "[cosP - cosE]/sigma");
      break;
    default:
      printf("Invalid vertex function %d (should be 1, 2, 3 or -1, -2, -3)\n", vertexFunct);
      errexit("inittrack");
    }
  }
  else if(vertexStyle == VLIKELIHOOD) {
    testphoto  = testphotoL;
    testpair1  = testpair1L;
    testcompt  = testcomptL;
    figOfMerit = figOfMeritL;
    switch (vertexFunct) {
    case  1:
    case -1:
      tcomptVtx = tcomptVtxLES;
      sprintf(cvertexstring, "[E1(ener) - E1(pos)]/sigma");
      break;
    case  2:
    case -2:
      tcomptVtx = tcomptVtxLeS;
      sprintf(cvertexstring, "[e1(ener) - e1(pos)]/sigma");
      break;
    case  3:
    case -3:
      tcomptVtx = tcomptVtxLAS;
      sprintf(cvertexstring, "[cosP - cosE]/sigma");
      break;
    default:
      printf("Invalid vertex function %d (should be 1, 2 or 3)\n", vertexFunct);
      errexit("inittrack");
    }
  }
  else {
    printf("Invalid vertex style %s\n", vertexStyle);
    errexit("inittrack");
  }

}

void
finittrack()
{
  int ss, ii, nn;
  int cluP, totcluP;
  int cluB, totcluB;
  int cluT, totcluT;
  int *ptspecP, *ptspecB;

  ptspecP = specPointer(22, 0);
  ptspecB = specPointer(23, 0);
  
  for(ss = 0; ss < 2; ss++) {
    if(vertexStyle == VCHISQUARE) {
      for(nn = 0; nn < 2000; nn += 200) {
        totcluP = totcluB = 0;
        for(ii = nn; ii < nn+200; ii++) {
          cluP = ptspecP[ii];
          cluB = ptspecB[ii];
          cluT = cluP + cluB;
          totcluP += cluP;
          totcluB += cluB;
          totcluT  = totcluP + totcluB;
          if(!cluT) continue;
          // P/T & B/T integrati
          ptspecP[ii + 4096] = (int)(1000. * totcluP / (double)totcluT);
          ptspecB[ii + 4096] = (int)(1000. * totcluB / (double)totcluT);
        }
      }
    }
    else if(vertexStyle == VLIKELIHOOD) {
      for(nn = 0; nn < 2000; nn += 200) {
        totcluP = totcluB = 0;
        for(ii = nn+199; ii >= nn; ii--) {
          cluP = ptspecP[ii];
          cluB = ptspecB[ii];
          cluT = cluP + cluB;
          totcluP += cluP;
          totcluB += cluB;
          totcluT  = totcluP + totcluB;
          if(!cluT) continue;
          // P/T & B/T integrati
          ptspecP[ii + 4096] = (int)(1000. * totcluP / (double)totcluT);
          ptspecB[ii + 4096] = (int)(1000. * totcluB / (double)totcluT);
        }
      }
    }
    else {
      printf("Invalid vertex style %d\n", vertexStyle);
      errexit("finittrack");
    }
    ptspecP = specPointer(24, 0);
    ptspecB = specPointer(25, 0);
  }
}

// still read from file in fixed units keV-cm
void
initlambda()
{
  int     dirlen = 0;
  FILE   *fp;
  char   *filename;
  char   *mgtdir, *workdir, *ptmp;
  int     ii, nn;
  double  e, x, l;

#if MODSPROB == 1
  // Raluca's parametrization to modify cross-sections
  // in order to account for distance packing
  double a1 =  0.041277;
  double b1 =  1.9404;
  double c1 = -0.938605;
  double d1 = -0.0609455;
  double a2 =  0.0179715; 
  double b2 = -0.119695;
  double c2 =  1.1215; 
  double d2 = -0.0609455;
#endif

  mgtdir = getenv("MGTDIR");
  if(mgtdir) dirlen  = strlen(mgtdir);
  if(!mgtdir || !dirlen) {
    printf("Please define the enviroment variabe MGTDIR pointing\n");
    printf("to the directory of the gamma-ge cross section files\n");
    errexit("initlambda");
  }

  // Turn all \ (WIN32) into / (which is valid, inside compiler, also for WIN32)
  // It is not safe to work on the original mgtdir string, so we make our own copy,
  //workdir = malloc(dirlen+2);               // space for optional / (and the 0 terminator)
  workdir = (char*)malloc(dirlen+2);               // space for optional / (and the 0 terminator)
  strcpy(workdir, mgtdir);
  while(ptmp = strchr(workdir, '\\'))
    ptmp[0] = '/';

  if(workdir[dirlen-1] != '/')
    strcat(workdir, "/");
  dirlen = strlen(workdir);
  
  //filename = malloc(dirlen+20);             // space for optional slash and filenames
  filename = (char*)malloc(dirlen+20);             // space for optional slash and filenames

  strcpy(filename, workdir);
  strcat(filename, "ge_comp.dat");
  if( (fp = fopen(filename, "r")) == NULL) {
      printf("\nError opening Compton Scattering file %s\n", filename);
      errexit("initlambda");
  }
  ii = nn = 0;
  while(fscanf(fp, "%lf %lf %lf", &e, &x, &l) == 3) {
    nn++;
    while(ii <= (int)(e + 0.5) && ii < MAXEKEV) lcomp[ii++] = l;
    if( ii >= MAXEKEV) break;
  }
  fclose(fp);

  strcpy(filename, workdir);
  strcat(filename, "ge_phot.dat");
  if( (fp = fopen(filename, "r")) == NULL) {
      printf("\nError opening Photoelectric Absorption file %s\n", filename);
      errexit("initlambda");
  }
  ii = nn = 0;
  while(fscanf(fp, "%lf %lf %lf", &e, &x, &l) == 3) {
    nn++;
    while(ii <= (int)(e + 0.5) && ii < MAXEKEV) lphot[ii++] = l;
    if( ii >= MAXEKEV) break;
  };
  fclose(fp);

  strcpy(filename, workdir);
  strcat(filename, "ge_pair.dat");
  if( (fp = fopen(filename, "r")) == NULL) {
      printf("\nError opening Pair Production file %s\n", filename);
      errexit("initlambda");
  }
  ii = nn = 0;
  while(fscanf(fp, "%lf %lf %lf", &e, &x, &l) == 3) {
    nn++;
    while(ii <= (int)(e + 0.5) && ii < MAXEKEV) lpair[ii++] = l;
    if( ii >= MAXEKEV) break;
  };
  fclose(fp);

  for (ii = 0; ii < MAXEKEV; ii++) {
    latot[ii] = 1. / (1./lphot[ii] + 1./lcomp[ii] + 1./lpair[ii]);
    scomp[ii] = latot[ii] / lcomp[ii];
    sphot[ii] = latot[ii] / lphot[ii];
    spair[ii] = latot[ii] / lpair[ii];
    mutot[ii] = 1. / latot[ii];
#if MODSPROB == 1     // after this sphot+scompt+spair is no more normalized !???
    x = sphot[ii];    // modify sphot
    x = a1 + b1*x + c1*x*x + d1*x*x*x;
    sphot[ii] = x;
    x = scomp[ii];
    x = a2 + b2*x + c2*x*x + d2*x*x*x;
//  scomp[ii] = x;    // modify scompt (excluded as not very effective)
//  printf("%5d, %10.5f\n", ii, sphot[ii]+scomp[ii]+spair[ii]);
#endif
  }

  // go to mm
  for (ii = 0; ii < MAXEKEV; ii++) {
    lphot[ii] *= 10.;
    lcomp[ii] *= 10.;
    lpair[ii] *= 10.;
    latot[ii] *= 10.;
    mutot[ii] /= 10.;
  }

}

void
reconstruct()
{

  int nn;
  clust *pcl;

//#ifdef FORPRISMA
//  if( doPrismaEvs )
//    prismaEventHeader();
//#endif    

  distmatmake(ppev, npev);
  if(llist > 1) distmatprint(0);

  nclupool = ncludone = nclugood = npcludone = 0;
  if(varang) {
    nn = (npev+2)/3;               // rough estimate of multiplicity; valid for 1.3 MeV
    specIncr(nn + 400, 15, 0);     // should be improved to account for energy of cluster
    nn = MAX(1, MIN(nn, MAXMULT));
    cluAngMax = thmult[nn];
  }

  if(cluGood) {
    poolGood();       // only for test, generate the actual clusters
  }
  else {
    poolLinkedS(DISTANGULAR);       // link-method to a sequence of clusterization angles
    poolLeaderS(DISTANGULAR);       // lieder-method to a sequence of clusterization angles
//  poolLinkedS(DISTEUCLIDEAN);     // link-method to a sequence of clusterization distances
//  poolLeaderS(DISTEUCLIDEAN);     // lieder-method to a sequence of clusterization distances
//  poolLinkedC(DISTANGULAR);       // tutti i cluster linked fino a opening cluAngMax
//  poolLeaderC(DISTANGULAR);       // tutti i cluster leaded fino a opening cluDistMax
//  poolLinkedC(DISTEUCLIDEAN);     // tutti i cluster linked fino a opening cluAngMax
//  poolLeaderC(DISTEUCLIDEAN);     // tutti i cluster leaded fino a opening cluDistMax
  }
  
  if(llist) printclustpool();

  nclupoolmax = MAX(nclupool, nclupoolmax);
  statclusters();

//setCluWeights();

// this is where the clusters are accepted
  nn = sortclusters();

// to check for multiple use of the same point in the accepted clusters
  nn = checkmultiple();
  if(nn) nclumixed++;

// some printouts
  if(llist) {
    printclustgood();
    for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
      if(pcl->mechanism == Compt) {
//      figOfMeritC(pcl->ppc, pcl->npoints);
//      restEnerCe(pcl->ppc, pcl->npoints);
      }
    }
  }

// to avoid the 2 separate blocks of code, should transform
// likelihood L in -log(L) so that threshold test is the same

// generate spectra
  if( doSort )
    eventHeader();

  if(vertexStyle == VCHISQUARE) {
    for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
      if(pcl->npoints <= MAXACCEPT && pcl->chibest <= chi2Accept ) {

	//printf( "pcl->size_th= %f\n", pcl->size_th );
	//printf( "pcl->egamma= %f\n", pcl->egamma );
        specIncrGain(pcl->egamma, 30, 0);     // 30 = accepted gammas
	//printf( "DC2TH(pcl->ppc)= %f\n",  DC2TH(pcl->ppc[0]) );
	//printf( "pcl->ppc[]->nd= %i\n",  pcl->ppc[0]->nd );   // = id of the cluster hit
	//printf( "DC2TH(pcl->ppc[0])= %f\n",  DC2TH(pcl->ppc[0]) ); // = reconstructed Theta of the incoming gamma-ray 

	//printf( "beta= %f\n",  recoil.beta ); // = reconstructed Theta of the incoming gamma-ray 

				mgttree.event=evnum;
				mgttree.TBeta=recoil.beta;
				mgttree.TX1=pcl->ppc[0]->xx;
				mgttree.TY1=pcl->ppc[0]->yy;
				mgttree.TZ1=pcl->ppc[0]->zz;
				mgttree.TELabDopTracked = pcl->egamma ; // gamma energy dlopper corrected and tracked
				mgttree.TMult=pcl->npoints; // 
				//mgttree.TMult=pcl->numgam;  // numero du gamma
                                output_tree->Fill();  
	

	if(pcl->ppc[0]->nd<15) specIncrGain(pcl->egamma, 140, 0);     // 140 = accepted gammas for the first ring of clusters (at 0 or 180 degrees

	if( 14<pcl->ppc[0]->nd && pcl->ppc[0]->nd<51) specIncrGain(pcl->egamma, 141, 0);     // 141 = accepted gammas for the first ring of clusters

	specIncrGain((int)(DC2TH(pcl->ppc[0])), 142, 0);

        clusterspectra(pcl);
        if( doSort )
          addGammaToEvent( pcl );
#ifdef FORPRISMA
	if( doPrismaEvs )
	  addGammaToFile( pcl );
#endif    
      }
      else {
        specIncrGain(pcl->egamma, 40, 0);     // 40 = passed to residues
      }
    }
  }
  else if(vertexStyle == VLIKELIHOOD) {
    for(pcl=clustersdone, nn = 0; nn < ncludone; nn++, pcl++) {
      if(pcl->npoints <= MAXACCEPT && pcl->chibest >  chi2Accept ) {
        specIncrGain(pcl->egamma, 30, 0);     // 30 = ACCEPTED GAMMAS
        clusterspectra(pcl);
        if( doSort )
          addGammaToEvent( pcl );
#ifdef FORPRISMA
	if( doPrismaEvs )
	  addGammaToFile( pcl );
#endif    
      }
      else {
        specIncrGain(pcl->egamma, 40, 0);     // 40 = passed to residues
      }
    }
  }
  else {
    printf("Invalid vertex style %d\n", vertexStyle);
    errexit("reconstruct");
  }
  
  for (pcl = clusterspool, nn = 0; nn < nclupool ; nn++, pcl++) {
    if(pcl->status == Bad || pcl->status == ToDo ) {
      specIncrGain(pcl->egamma, 36, 0);     // 36 = spectrum of residual clusters
    }
  }

  ncludonemax = MAX(ncludonemax, ncludone);
  nclugoodmax = MAX(nclugoodmax, nclugood);
  specIncr(ncludone + 3000, 15, 0);         // distribution of reconstructed clusters
  specIncr(nclugood + 3100, 15, 0);         // distribution of reconstructed good-clusters
  
  if( doSort ) {
    if( doAncillary )
      addAncillaryToEvent();
    sendEventToBuffer();  
  }
#ifdef FORPRISMA
  if( doPrismaEvs ) {
    if( doAncillary )
      writeAncillaryEvent();
//    prismaEventFooter();
  }    
#endif    
  
}

int
sortclusters()
{
  int     ii, jj;
  int     quale;
  int     npdone, ncdone, ncgood;
  clust  *pcl;
  char   *pch;
  double  chibest;
  
  memset(nsharing, 0, sizeof(int)*npev);
  for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
    if(pcl->status == Removed) continue;
    pch = pcl->cstring;
    for (jj = pcl->firstbit; jj <= pcl->lastbit; jj++) if(pch[jj] > '0') nsharing[jj]++;
  }

  for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
    if(pcl->status == Removed) continue;
    pch = pcl->cstring;
    for(jj = pcl->firstbit; jj <= pcl->lastbit; jj++) {
      if(pch[jj] > '0') {
        if (nsharing[jj] > 1) {
          pcl->shared = 1;
          break;
        }
      }
    }
  }

  if(vertexStyle == VCHISQUARE) {
    for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
      if(pcl->mechanism == None || pcl->chibest >= chi2Accept)
        pcl->status = Bad;
    }
    if(llist) printf("\n");
    npdone = ncdone = ncgood = 0;
    while(1) {
      quale = -1;
      chibest = chi2Accept;
      for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
        if(pcl->status == ToDo) {
          if(pcl->chibest < chibest) {
            quale = ii;
            chibest = pcl->chibest;
          }
        }
      }
      if(quale < 0) break;            // sort finished
      pcl = clusterspool + quale;
      if(llist) printaccepted(pcl);
      if(peaklut[(int)pcl->egamma]) ncgood++;
      ncdone++;
      npdone += pcl->npoints;
      acceptsorted(quale);
    }
  }
  else if(vertexStyle == VLIKELIHOOD) {
    for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
      if(pcl->mechanism == None || pcl->chibest <  chi2Accept)
        pcl->status = Bad;
    }
    if(llist) printf("\n");
    npdone = ncdone = ncgood = 0;
    while(1) {
      quale = -1;
      chibest = chi2Accept;
      for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
        if(pcl->status == ToDo) {
          if(pcl->chibest > chibest) {
            quale = ii;
            chibest = pcl->chibest;
          }
        }
      }
      if(quale < 0) break;            // sort finished
      pcl = clusterspool + quale;
      if(llist) printaccepted(pcl);
      if(peaklut[(int)pcl->egamma]) ncgood++;
      ncdone++;
      npdone += pcl->npoints;
      acceptsorted(quale);
    }
  }
  else {
    printf("Invalid vertex style %d\n", vertexStyle);
    errexit("sortclusters");
  }
  
  if(llist) printsummary2(npdone, ncdone, ncgood);

  return ncdone;
}

void
acceptsorted(int nn)
{
  static char  modify[MAXCLUST];
  int    ii, jj, kk;
  clust *pcl1, *pcl2;
  char  *pch1, *pch2;
  int    modified = 0;
  
  pcl1 = &clusterspool[nn];
  pcl1->status = Good;
  pcl2 = &clustersdone[ncludone];
  memcpy(pcl2, pcl1, sizeof(clust));
  for(ii = 0; ii < pcl2->npoints; ii++) pcl2->ppc[ii]->clu = ncludone;
  ncludone++;
  npcludone += pcl2->npoints;
  clugood    = peaklut[(int)pcl1->eclust];
  if(clugood) nclugood++;
  
  if(!pcl1->shared) {
    pch1 = pcl1->cstring;
    for (ii = pcl1->firstbit; ii <= pcl1->lastbit; ii++) if(pch1[ii] > '0') nsharing[ii] = 0;
    return;
  }
  
  memset(modify, 0, sizeof(char)*nclupool);
  pch1 = pcl1->cstring;
  for (ii = pcl1->firstbit; ii <= pcl1->lastbit; ii++) {
    if(pch1[ii] > '0') {
      if(nsharing[ii] > 1) {
        pcl2 = clusterspool;
        for (jj = 0; jj < nclupool; jj++, pcl2++) {
          if(pcl2->status == Good || pcl2->status == Removed) continue;
          pch2 = pcl2->cstring;
          if(pch2[ii] > '0') {
            pch2[ii] = '0';
            pcl2->npoints--;
            if(pcl2->npoints) {
              pcl2->status = ToDo;
              modified = 1;
              modify[jj] = 1;
            }
            else pcl2->status = Removed;
          }
        }
      }
      nsharing[ii] = 0;
    }
  }
  
  if(modified) {
    pcl1 = clusterspool;
    for (ii = 0; ii < nclupool; ii++, pcl1++) {
      if(!modify[ii] || pcl1->status == Removed) continue;
      pcl2 = clusterspool;
      for (jj = 0; jj < nclupool; jj++, pcl2++) {
        if(jj == ii || pcl2->npoints != pcl1->npoints) continue;
        if(pcl2->status == Good || pcl2->status == Removed) continue;
        if(strcmp(pcl1->cstring, pcl2->cstring) == 0) {
          pch1 = pcl1->cstring;
          for(kk = pcl1->firstbit; kk <= pcl1->lastbit; kk++) if(pch1[kk] > '0') nsharing[kk]--;
          pcl1->status = Removed;
          break;
        }
      }
      if(pcl1->status == ToDo) {
        trackCluster(pcl1);
        if(pcl1->chibest > chi2Track)
          pcl1->status = Removed;
      }
    }
  }
  return;
}

int
checkmultiple()
{
  int ii, jj;
  clust *pcl;
  char  *pch;

  memset(nshared, 0, sizeof(int)*npev);
  for (ii = 0, pcl = clustersdone; ii < ncludone; ii++, pcl++) {
    pch = pcl->cstring;
    for (jj = pcl->firstbit; jj <= pcl->lastbit; jj++) if(pch[jj] > '0') nshared[jj]++;
  }

  jj = 0;
  for(ii = 0; ii < npev; ii++) if(nshared[ii] > 1) jj++;

  return jj;

}

void
clusterspectra(clust *pcl)
{
  int    ival, tval, mech;
  int    npoints;
  double eclust, efirst, elast;
  double chibest;
  double theta1, theta2;
  int    dtheta1, dtheta2;
  point *pp1, *pp2;

  eclust  = pcl->egamma;
  mech    = pcl->mechanism;
  npoints = pcl->npoints;
  chibest = pcl->chibest;
  clugood = peaklut[(int)eclust];

// gamma spectra

  specIncrGain(eclust, mech    + 30, 0);                // sorted by mechanism

  if(pcl->numgam == 1)
    specIncrGain(eclust, 34, 0);                        // accepted without mixing of points

  if((vertexStyle == VCHISQUARE  && chibest < chi2Accept*0.1) ||
     (vertexStyle == VLIKELIHOOD && chibest > chi2Accept*10.) )
      specIncrGain(eclust, 35, 0);                      // the first 10% of chi2
  
  specIncrGain(eclust, npoints + 40, 0);                // sorted by number of points

  if(pcl->npoints > 1)
    specIncrGain(eclust, pcl->tag/100 + 50, 0);         // sorted by cluster type

  if(nsegdet < 40)
    specIncrGain(eclust, pcl->ppc[0]->ns + 100, 0);     // sorted according segment of first point

// statistiche varie

  specIncr(pcl->npoints + 3200, 15, 0);                 // distribution of points in accepted clusters
  specIncr(pcl->numgam  + 3500, 15, 0);                 // distribution of number of gammas in accepted clusters
  ival = (int)(100. * chibest);
  tval = (int)(100. * atan(chibest));
  if (clugood) {
    specIncr(ival                 , 28, 0);             // Chi2 (peak)
    specIncr(tval                 , 24, 0);             // Chi2 (peak) compressed
    specIncr(tval +  200 * npoints, 24, 0);             // Chi2 (peak) compressed
    specIncr(ival + 1000 * npoints, 28, 0);             // Chi2 (peak) re number of points
    specIncr((int)pcl->size_th        ,        39, 0);  // angular opening
    specIncr((int)(10. * pcl->dist_th) + 2000, 39, 0);  // angular distance to nearest point
    if(ngedets > 1)
      //printf("\n detector angular distribution %i \n", pcl->ppc[0]->nd);  // Clover#
      specIncr(pcl->ppc[0]->nd + 5120, 17, 0);          // angular distribution of detectors
    specIncr(pcl->npoints + 3300, 15, 0);               // number of points in accepted peak-clusters
    specIncr(pcl->numgam  + 3600, 15, 0);               // number of gammas in accepted peak-clusters
  }
  else {
    specIncr(ival                 , 29, 0);             // Chi2 (background)
    specIncr(tval                 , 25, 0);             // Chi2 (background) compressed
    specIncr(tval +  200 * npoints, 25, 0);             // Chi2 (background) compressed
    specIncr(ival + 1000 * npoints, 29, 0);             // Chi2 (background) re number of points
    specIncr((int)pcl->size_th         + 4000, 39, 0);  // angular opening
    specIncr((int)(10. * pcl->dist_th) + 6000, 39, 0);  // angular distance to nearest point
    specIncr(pcl->npoints + 3400, 15, 0);               // number of points in accepted peak-clusters
    specIncr(pcl->numgam  + 3700, 15, 0);            
   // number of gammas in accepted peak-clusters
  }

  if (mech == Compt) {
    efirst = pcl->ppc[0          ]->ee;
    elast  = pcl->ppc[npoints - 1]->ee;
    pp1 = pcl->ppc[0];
    pp2 = getptogam(pp1->ng);
    theta1 = DC2TH(pp1);
    theta2 = DC2TH(pp2);
    dtheta1 = (int)(10. * RAD2DEG*acos(pp1->cx*pp2->cx + pp1->cy*pp2->cy + pp1->cz*pp2->cz));
    dtheta2 = (int)(10. * (theta1 - theta2) + 4000);
    if (clugood) {
//    matIncr((int)(5.*theta1), (int)(5.*theta2), 0);
      specIncr(dtheta1, 56, 0);             // ang. error of reconstruction if peak
      specIncr(dtheta2, 56, 0);             // ang. error of reconstruction if background 
//    specIncrGain(efirst,       58, 0);    // energy of first point
//    specIncrGain(elast + 4000, 58, 0);    // energy of last point
      specIncr((int)(100. * efirst/eclust),      58, 0);
      specIncr((int)(100. * elast/eclust) + 200, 58, 0);
    }
    else {
      specIncr(dtheta1, 57, 0);             // ang. error of reconstruction if background
      specIncr(dtheta2, 57, 0);             // ang. error of reconstruction if background
//    specIncrGain(efirst,        59, 0);   // energy of first point
//    specIncrGain(elast  + 4000, 59, 0);   // energy of last point
      specIncr((int)(100. * efirst/eclust),       59, 0);
      specIncr((int)(100. *  elast/eclust) + 200, 59, 0);
    }
  }
}

void
statclusters()
{
  int    ii, ix, iy, ival, tval;
  clust *pcl;

  specIncr(nclupool + 2000, 15, 0);                 // number of clusters in pool
  for (ii = 0, pcl = clusterspool; ii < nclupool; ii++, pcl++) {
    if(pcl->status == Removed) continue;
    ix = pcl->npoints;
    iy = pcl->numgam;
    specIncr(ix + 2200, 15, 0);                     // points in cluster
    specIncr(iy + 2400, 15, 0);                     // gammas in cluster
//  matIncr(ix, iy, 0);                             // g-g matrix
    specIncrGain(pcl->egamma, 10, 0);               // energy of all clusters
    ival = (int)(100*pcl->chibest);
    if(vertexStyle == VCHISQUARE)
      tval = (int)(100*atan(pcl->chibest));
    else
      tval = (int)(100*pcl->chibest);
    specIncr(tval + 2000, 22, 0);         // Chi2 for all clusters
    if(pcl->fullener > 1) {
      specIncr(ival,        20, 0);       // Chi2 distribution of peak
      specIncr(tval,        22, 0);       // Chi2 distribution of peak compressed
//    if(pcl->npoints < 10)
        specIncr(tval + 200*pcl->npoints, 22, 0);
    }
    else {
      specIncr(ival , 21, 0);             // Chi2 distribution of background (+ non FEP)
      specIncr(tval , 23, 0);             // Chi2 distribution of backg. compressed
//    if(pcl->npoints < 10)
        specIncr(tval + 200*pcl->npoints, 23, 0);
      if(pcl->fullener)
        specIncr(tval + 2000, 23, 0);     // Chi2 per gamma non rivelati completi ma non FE
    }
  }
}

void
trackCluster(clust *pcl)
{
  int     ii, jj;
  char   *pch;
  double  xx, vald, valx;
  
  pch = pcl->cstring;
  
  for (ii = 0; ii < npev; ii++) if(pch[ii] > '0') break;
  pcl->firstbit = ii;
  if(pcl->npoints > 1) {
    for (ii = npev - 1; ii > pcl->firstbit; ii--)
      if(pch[ii] > '0') break;
  }
  pcl->lastbit = ii;
  
  for (xx = 0.0, ii = pcl->firstbit; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') xx += ppev[ii]->ee;
  }
  pcl->eclust = pcl->egamma = xx;

  pcl->status     = ToDo;
  pcl->mechanism  = None;
  pcl->chibest    = chi2Track;
  pcl->ordbest[0] = 0;
  checkgoodclust(pcl);

  if(pcl->npoints == 1) {
    if(checkPh) {
      vald = valx = 10000.;      // start with a very large value
      jj = pcl->firstbit;
      for (ii = 0; ii < npev; ii++) {
        if(ii != jj) {
          xx   = distmat_th[jj][ii];
          vald = MIN(xx, vald);
          xx   = distmat_dd[jj][ii];
          valx = MIN(xx, valx);
        }
      }
      pcl->dist_th = vald;
      pcl->dist_dd = valx;
      pcl->ppc[0]  = ppev[pcl->firstbit];
      if((int)pcl->eclust*dEgain >= SPECLEN) return;
      testphoto(pcl);
    }
    return;
  }

  vald = valx = 0.;
  for(ii = pcl->firstbit; ii < pcl->lastbit; ii++) {
    if(pch[ii] > '0') {
      for (jj = ii + 1; jj <= pcl->lastbit; jj++) {
        if(pch[jj] > '0') {
          xx   = distmat_th[ii][jj];
          vald = MAX(vald, xx);
          xx   = distmat_dd[ii][jj];
          valx = MAX(valx, xx);
        }
      }
    }
  }
  pcl->size_th = vald;
  pcl->size_dd = valx;

  if(pcl->npoints > MAXPOINTS) return;
  if((int)pcl->eclust*dEgain >= SPECLEN) return;

  if(pcl->npoints < npev) {
    vald = valx = 10000.; 
    for(ii = pcl->firstbit; ii <  pcl->lastbit; ii++) {
      if(pch[ii] > '0') {
        for (jj = 0; jj < npev; jj++) {
          if(pch[jj] < '1') {
            xx   = distmat_th[ii][jj];
            vald = MIN(vald, xx);
            xx   = distmat_dd[ii][jj];
            valx = MIN(valx, xx);
          }
        }
      }
    }
    pcl->dist_th = vald;
    pcl->dist_dd = valx;
  }

  if(pcl->eclust > M0C2P  && checkPP) {     // to improve, testing also for Compton
    testpair1(pcl);
    if(pcl->chibest < chi2Accept) return;
  }

  if(pcl->npoints > MAXPERM) return;
//if(pcl->eclust < 1000. && pcl->npoints > npmax[(int)(0.1 * pcl->eclust)]) return;

  testcompt(pcl);
}

void
checkgoodclust(clust * pcl)
{
  int     ii, jj, kk, ngam, same;
  static  int igam[MAXMULT];
  char   *pch;

  pch = pcl->cstring;

  igam[0] = ppev[pcl->firstbit]->ng;
  ngam = 1;
  for(ii = pcl->firstbit+1; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') {
      jj = ppev[ii]->ng;
      same = 0;
      for(kk = 0; kk < ngam; kk++) {
        if(jj == igam[kk]) {
          same = 1;
          break;
        }
      }
      if(!same) {
        igam[ngam] = jj;
        ngam++;
      }
    }
  }
  pcl->numgam   = ngam;
  pcl->fullener = 0;
  
  if(ngam == 1) 
    pcl->fullener = checkEgam(igam[0], pcl->eclust);
  
}

void
printclustpool()
{
  int     ii, jj, mm;
  clust  *pcl;
  char   *pch;
  double  eclust;

  memset(nshared, 0, sizeof(int)*npev);
  for (pcl = clusterspool, ii = 0; ii < nclupool; ii++, pcl++) {
    pch = pcl->cstring;
    for (jj = 0; jj < npev; jj++) if(pch[jj] > '0') nshared[jj]++;
  }

  printf("\n%6d   Clusters with %d Points to be sorted\n\n", nclupool, npev - npcludone);

  printf("               ");
  for (ii = 0; ii < npev ; ii++) printf("%1d", ii%10);
  printf("    Eclust    Chi2    Diam.    Dist");
  printf("\n");
  printf("               ");
  for (ii = 0; ii < npev ; ii++) (ii%10) ? (printf(" ")):(printf("%1d",ii/10));
  printf("\n");

  pcl = clusterspool;
  for (mm = ii = 0; ii < nclupool ; ii++, pcl++) {
    printf("%3d %3d %3d %2d ", ii, pcl->childof, pcl->tag, pcl->npoints);
    for(jj = 0; jj < npev; jj++) {
      if(pcl->cstring[jj] > '0') printf("1");
      else                       printf("-");
    }
    eclust = pcl->egamma;
    if     (pcl->fullener > 1) {printf(" *"); mm++; }
    else if(pcl->fullener    )  printf(" &");
    else                        printf("  ");
    printf(" %7.2f %8.5f %7.0f %7.0f", eclust, pcl->chibest, pcl->size_th, pcl->dist_dd);
    printf("");
    printf("\n");
  
  }

  printf("############## ");
  for (ii = 0; ii < npev; ii++) (nshared[ii] < 10) ? (printf("%1d", nshared[ii])) : (printf("*"));
  printf(" %d\n", mm);
}

void
printclustgood()
{
  int     ii, jj, mm;
  clust  *pcl;
  double  eclust;

  printf("\nThe%4d Reconstructed Clusters\n\n", ncludone);
  printf("           ");
  for (ii = 0; ii < npev ; ii++) printf("%1d", ii%10);
  printf("    Eclust    Chi2    Diam.    Dist   Type");
  printf("\n");
  printf("           ");
  for (ii = 0; ii < npev ; ii++) (ii%10) ? (printf(" ")):(printf("%1d",ii/10));
  printf("\n");

  memset(nshared, 0, sizeof(int)*npev);
  pcl = clustersdone;
  for (mm = ii = 0; ii < ncludone ; ii++, pcl++) {
    printf("%3d %3d %2d ", ii, pcl->tag, pcl->npoints);
    for(jj = 0; jj < npev; jj++) {
      if(pcl->cstring[jj] > '0') {printf("1"); nshared[jj]++;}
      else                        printf("-");
    }
    eclust = pcl->egamma;
    if     (pcl->fullener > 1) {printf(" *"); mm++; }
    else if(pcl->fullener    )  printf(" &");
    else                        printf("  ");

    printf(" %7.1f %8.5f %7.0f %7.0f %s", eclust, pcl->chibest, pcl->size_th, pcl->dist_dd, mechanism[pcl->mechanism]);
    printf("\n");
  
  }

  printf("########## ");
  for (ii = 0; ii < npev; ii++) (nshared[ii] < 10) ? (printf("%1d", nshared[ii])) : (printf("*"));
  printf(" %d\n", mm);
}

void
printclustrest()
{
  int     ii, jj, nn, mm;
  clust  *pcl;
  char   *pch;
  double  eclust;

  memset(nshared, 0, sizeof(int)*npev);
  pcl = clusterspool;
  for (nn = ii = 0; ii < nclupool; ii++, pcl++) {
    if(pcl->status == Bad ||  pcl->status == ToDo) {
      nn++;
      pch = pcl->cstring;
      for (jj = 0; jj < npev; jj++) if(pch[jj] > '0') nshared[jj]++;
    }
  }
  if( nn == 0) return;

  for(mm = ii = 0; ii < npev ; ii++) if(nshared[ii]) mm++;

  printf("\nThe%4d  Remaining Clusters with  %d  Points\n\n", nn, mm);

  printf("           ");
  for (ii = 0; ii < npev ; ii++) printf("%1d", ii%10);
  printf("    Eclust      Chi2    Diam.    Dist");
  printf("\n");
  printf("           ");
  for (ii = 0; ii < npev ; ii++) (ii%10) ? (printf(" ")):(printf("%1d",ii/10));
  printf("\n");

  pcl = clusterspool;
  for (mm = ii = 0; ii < nclupool ; ii++, pcl++) {
    if(pcl->status == Removed ||  pcl->status == Good) continue;
    printf("%3d %3d %2d ", ii, pcl->tag, pcl->npoints);
    for(jj = 0; jj < npev; jj++) {
      if(pcl->cstring[jj] > '0') printf("1");
      else                       printf("-");
    }
    eclust = pcl->egamma;
    if     (pcl->fullener > 1) {printf(" *"); mm++; }
    else if(pcl->fullener)      printf(" &");
    else                        printf("  ");
    printf(" %7.1f %9.2f %7.0f %7.0f", eclust, pcl->chibest, pcl->size_th, pcl->dist_dd);
    printf("\n");
  
  }

  printf("########## ");
  for (ii = 0; ii < npev; ii++) (nshared[ii] < 10) ? (printf("%1d", nshared[ii])) : (printf("*"));
  printf(" %d\n", mm);
}

void
printaccepted(clust *pcl)
{
  int   ii;
  char  *pch;

  pch = pcl->cstring;
  printf("%4d%3d %7.1f ==>", pcl->tag, pcl->npoints, pcl->egamma);
  for (ii = 0; ii < npev; ii++) if(*pch++ > '0') printf("%3d", ii);
  for (ii = pcl->npoints; ii < MAXPOINTS; ii++) printf("   ");
  printf(" [%s] ", pcl->ordbest);
  for (ii = pcl->npoints; ii < MAXPOINTS; ii++) printf(" ");
  if(peaklut[(int)pcl->egamma]) printf("Chi2 = %6.2f *",pcl->chibest);
  else                          printf("Chi2 = %6.2f  ",pcl->chibest);
  printf("\n");
  if (llist > 1) {
    printgamgood(pcl->ppc, pcl->npoints);
//  if(pcl->mechanism == Compt && llist > 2) PtcomptonP(pcl);
    printf("\n");
  }
}

void
printsummary2(int npdone, int ncdone, int ncgood)
{
  int   ii, jj, kk;
  int   ncremain;
  clust *pcl;
  char  *pch;
  char  cstring1[MAXEVLEN + 1];
  char  cstring2[MAXEVLEN + 1];
  char  cstring3[MAXEVLEN + 1];
  
  pcl = clusterspool;

  for (ncremain = ii = 0; ii < nclupool ; ii++, pcl++) if(pcl->status == Bad || pcl->status == ToDo ) ncremain++;

  printf("\n%7d  Points combined in", npdone);
  printf("\n%7d  reconstructed gammas", ncdone);
  printf("\n%7d  of which are in peak", ncgood);
  printf("\n");

  if(ncremain > 0) {
    printclustrest();
    pcl = clusterspool;
    memset(cstring1, '-', npev);
    memset(cstring2, '-', npev);
    memset(cstring3, '-', npev);
    cstring1[npev] = cstring2[npev] = cstring3[npev] = 0;
    for (ii = 0; ii < nclupool ; ii++, pcl++) {
      pch = pcl->cstring;
      if(pcl->status == Good)  {
        for (jj = 0; jj < npev; jj++) if(*pch++ > '0') cstring1[jj] = '1';
      }
      else if (pcl->status == Bad) {
        for (jj = 0; jj < npev; jj++) if(*pch++ > '0') cstring2[jj] = '1';
      }
    }

    for (kk = 0, jj =0; jj < npev; jj++) if(cstring2[jj] == '1') kk++;
    printf("\n%10d %s   points remaining", kk, cstring2);

    for (kk = 0, jj =0; jj < npev; jj++) if(cstring1[jj] == '1') kk++;
    printf("\n%10d %s   points combined", kk, cstring1);

    for (kk = 0, jj =0; jj < npev; jj++) {
      if(cstring1[jj] == '-' && cstring2[jj] == '-' ) {
        cstring3[jj] = '1';
        kk++;
      }
    }
    if(kk)
      printf("\n%10d %s   points lost", kk, cstring3);

    printf("\n");
  }
}

void
printgamgood(point **pp, int nn)
{
  int     ii, gamnum;
  point  *p0, *p1, *p2, *ob;
  double  etot, angP, angE;
  double  d01x, d01y, d01z, d01;
  double  d12x, d12y, d12z, d12, d12g;
  double  eg0, eg1;
  
  for (etot = 0., ii = 0; ii < nn; ii++) etot += pp[ii]->ee;
  
  p0  = &origin;
  p1  = &origin;
  eg0 = etot;
  eg1 = etot;
  for (ii = 0; ii < nn; ii++) {
    p2 = pp[ii];
    d12x = p2->xx - p1->xx;
    d12y = p2->yy - p1->yy;
    d12z = p2->zz - p1->zz;
    d12  = sqrt(d12x*d12x + d12y*d12y + d12z*d12z);
    if(ii == 0) {
      gamnum = pp[0]->ng;
      ob     = getptogam(gamnum);
      angP   = DC2PH(ob);
      angE   = DC2TH(ob);
      printf("       %8.1f (%5.1ft %6.1fp%5.0fd%5.0fg)", eg0, angE, angP, d12, gedistance(p1, p2));
      eg1 = eg0 - p2->ee;
    }
    else {
      d01x = p1->xx - p0->xx;
      d01y = p1->yy - p0->yy;
      d01z = p1->zz - p0->zz;
      d01  = sqrt(d01x*d01x + d01y*d01y + d01z*d01z);
      angP = RAD2DEG * acos( (d01x*d12x + d01y*d12y + d01z*d12z)/(d01*d12) );
      angE = 1. - M0C2 * (eg0 - eg1) / (eg0 * eg1);
      d12g = gedistance(p1, p2);
      if(fabs(angE) <= 1.0) {
        angE = RAD2DEG * acos(angE);
        printf("       %8.1f (%5.1fP %6.1fE%5.0fd%5.0fg)", eg1, angP, angE, d12, d12g);
      }
      else {
        printf("       %8.1f (%5.1fP %6.2f@%5.0fd%5.0fg)", eg1, angP, angE, d12, d12g);
      }
      eg0 = eg1;
      eg1 -= p2->ee;
    }
    printf("%7.1f %6.1f %6.1f %6.1f %6.1f %6.1f %6.1f %3d  ",
      p2->ee, p2->xx, p2->yy, p2->zz, p2->rr, DC2TH(p2), DC2PH(p2), p2->nd);
    printchid(p2->chid);
    printf("\n");
    p0 = p1;
    p1 = p2;
  }
}

double
wkn1(double e1, double e2)
{
  double ee, xx, ww;
  
  ee = M0C2 * (e1 - e2) / (e1 * e2);
  xx = e2 / e1;
  ww = 0.5 * xx * (xx*xx + 1.0 + xx * ee * (ee - 2.0));

  return ww;
}

double
wkn2(double xx, double s2)
{
  double ww;
  
  ww = 0.5 * xx * (xx*xx + 1.0 - xx*s2 );

  return ww;
}


//////////////////////////////////////////////////////////////////////
/////////////// Functions for vertexStyle = VCHISQUARE ///////////////
//////////////////////////////////////////////////////////////////////

void
testphotoC(clust *pcl)
{
  double  dist, xx;
  int     nn, iegam;
  
  if(pcl->npoints != 1) return;
  
  nn    = pcl->ppc[0]->ind;
  dist  = distmat_ge[nn][nn];
  iegam = (int)pcl->eclust;
  if(pcl->dist_dd > PHOTODIST) {
    pcl->mechanism  = Photo;
    pcl->chibest    = chi2Accept * PHOTOFACT;       // to be adjusted to a more sensible value
    xx = dist*mutot[iegam]; xx = MIN(20., xx);
    pcl->chibest   /= exp(-xx) * sphot[iegam] * pcl->dist_dd;
    pcl->chibest    = MIN(pcl->chibest, chi2Track);
    pcl->ordbest[0] = '0';
    pcl->ordbest[1] = 0;
    if(recoil.beta) {
      pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
    }
  }
}

void
testpair1C(clust *pcl)
{
  int     npc, ii, jj, kk, j1, j2, j3, iene;
  double  eclust, epair, dpair, ej1, ej2, em0c2, dm0c2;
  double  dist;
  char   *pch, tch;
  point  **ppc, *ppt;

  npc = pcl->npoints;
  if(npc < 3) return;

  eclust = pcl->eclust;
  if(eclust < M0C2P) return;
  
  pch = pcl->cstring;
  ppc = &pcl->ppc[0];
  for (jj = 0, ii = pcl->firstbit; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') ppc[jj++] = ppev[ii];
  }

  epair = eclust - M0C2P;
  dpair = peakwidth[(int)epair];
  dm0c2 = peakwidth[511];
  for (ii = 0; ii < npc; ii++) {
    if(fabs(ppc[ii]->ee - epair) > dpair) continue;
    for (j1 = 0; j1 < npc; j1++) {            // look for a 511 as a single point
      em0c2 = ppc[j1]->ee;
      if(j1 == ii) continue;
      if(fabs(em0c2-M0C2) < dm0c2) goto OK;
    }
    if(npc > 4) {                             //  look for a 551 summing 2 points
      for (j1 = 0; j1 < npc - 1; j1++) {
        if(j1 == ii) continue;
        ej1 = ppc[j1]->ee;
        for (j2 = j1 + 1; j2 < npc; j2++) {
          if(j2 == ii || j2 == j1) continue;
          em0c2 =  ej1 + ppc[j2]->ee;
          if(fabs(em0c2-M0C2) < dm0c2) goto OK;
        }
      }
    }
    if(npc > 6) {                             //  look for a 551 summing 3 points
      for (j1 = 0; j1 < npc - 2; j1++) {
        if(j1 == ii) continue;
        ej1 = ppc[j1]->ee;
        for (j2 = j1 + 1; j2 < npc -1; j2++) {
          if(j2 == ii || j2 == j1) continue;
          ej2 = ej1 + ppc[j2]->ee;
          for (j3 = j2 + 1; j3 < npc; j3++) {
            if(j3 == ii || j3 == j1 || j3 == j2) continue;
            em0c2 = ej2 + ppc[j3]->ee;
            if(fabs(em0c2-M0C2) < dm0c2) goto OK;
          }
        }
      }
    }
  }
  return;

OK:    
  kk    = pcl->ppc[ii]->ind;
  dist  = distmat_ge[kk][kk];
  iene  = (int)eclust;
  dist *= mutot[iene];
  if(dist > 10)
    return;
  pcl->mechanism = Pair0;
  pcl->chibest   = chi2Accept/1000.;          // to be adjusted to a more sensible value
  pcl->chibest  /= exp(-dist) * spair[iene];
  if(ii) SWAP(ppc[0], ppc[ii], ppt)
  for (jj = 0; jj < npc; jj++) pcl->ordbest[jj] = '0' + jj;
  pcl->ordbest[npc] = 0;
  if(ii) SWAP(pcl->ordbest[0], pcl->ordbest[ii], tch)
  if(recoil.beta) {
    pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
  }
  return;
}

void
testcomptC(clust *pcl)
{
  int     ii, jj, npc;
  double  X2factor, xfactor, X2best;
  char   *pch;
  
  npc = pcl->npoints;
  if(npc < 2 || npc > MAXPERM) return;

  /////////////////////////////////////////////////////////////
  // fill tc structure with pointers and initial permutation //
  /////////////////////////////////////////////////////////////

  tc.npc = npc;
  tc.clu = pcl;
  tc.ppc[0] = &origin;
  pch = pcl->cstring;
  for (jj = 1, ii = pcl->firstbit; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') tc.ppc[jj++] = ppev[ii];
  }
  for (ii = 0; ii <= npc; ii++) tc.pperm[ii] = ii;

  //////////////////////////////////////////////////
  // normalizations and empirical factors to chi2 //
  //////////////////////////////////////////////////

  X2factor  = xfactor = 1;

#if EXTRAWEIGHTS == 1
  // normalization for weights of vertices
  X2factor *= Pchi2[npc - 1];

  // merit factor for creation type
  X2factor *= X2Ftype[pcl->tag/100];

  // merit factor for number of points in cluster
  X2factor *= X2Fmult[npc];

  // merit factor for angular opening of cluster
  ii = (int)(0.01 * pcl->eclust);
  ii = MIN(ii, 10);
  xfactor = cluth[ii] / MAX(5., pcl->size_th);  // rough approximation of spectrum 14
  X2factor *= xfactor;                          // should penalize too-small angles

  // fattore di merito dovuto all'isolamento spaziale
  if(npc < npev) xfactor = pcl->dist_dd / CLUSTDIST; 
  else           xfactor = 10.;                 // merit for completly isolated ???
  X2factor *= xfactor;
#endif

  X2best = X2factor * chi2Track;                // MAXIMUM acceptable chi2

  ///////////////////////////////
  // find out best permutation //
  ///////////////////////////////
  
  tc.X2best = X2best;                           // start assuming existence of one with chi2-max

  testcomptCR(1, pcl->eclust , 0.);             // permutations calculated recursively starting from first point of cluster

  if(tc.X2best >= X2best)                       // did not find a better one
    return;

  ////////////////////////////////////////////////
  // update pcl with values of best permutation //
  ////////////////////////////////////////////////

  pcl->chibest = tc.X2best / X2factor;          // remove weights of vertices
  for (ii = 1; ii <= npc; ii++) {
    pcl->ordbest[ii-1] = '0' + tc.bperm[ii] -1;
    pcl->ppc[ii-1]     = tc.ppc[tc.bperm[ii]];
  }
  pcl->ordbest[npc] = 0;
  pcl->mechanism = Compt;
  if(recoil.beta) {
    pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
  }
}

void
testcomptCR(int ll, double eg0, double X0)
{
  int    ii, jj;
  double eg1, X1;
  
  for(ii = ll; ii <= tc.npc; ii++) {
    if(ii != ll) {jj = tc.pperm[ll]; tc.pperm[ll] = tc.pperm[ii]; tc.pperm[ii] = jj;}
    X1 = X0;
    firstVtx = ll == 2;         // these 2 flags must be recalculated every time because,
    lastVtx  = ll == tc.npc;    // being global, they could be modified during recursive calls
    X1  = X0;
    eg1 = eg0;
    if(ll > 1) {                // (ll-1) vertex calculated only starting from ll=2
      if(Wchi2[ll-1])           // and only if its weight is non-zero
        X1 += tcomptVtx(tc.ppc[tc.pperm[ll-2]], tc.ppc[tc.pperm[ll-1]], tc.ppc[tc.pperm[ll]], eg0) * Wchi2[ll-1];
        eg1 = eg0 - tc.ppc[tc.pperm[ll-1]]->ee;
    }
    if(X1 < tc.X2best) {
      if(lastVtx) {
        tc.X2best = X1;
        for(jj = 0; jj <= tc.npc; jj++) tc.bperm[jj] = tc.pperm[jj];
      }
      else {
        testcomptCR(ll + 1, eg1, X1);
      }
    }
    if(ii != ll) {jj = tc.pperm[ll]; tc.pperm[ll] = tc.pperm[ii]; tc.pperm[ii] = jj;}
  }
}

double
tcomptVtxCE(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double egE, egP;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  egE  = eg0 - p1->ee;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  egP  = eg0 / ( 1.0 + eg0 * M0C2I * ( 1.0 - cosP) );

  chi2 = (egE - egP)/eg0;
  chi2 *= chi2;

#if WEIGHTKN == 1
  // Weight with Klein-Nishina (unpolarized)
  xx = wkn2(egE/eg0, 1-cosP*cosP);
//chi2 /= xx;  // bisognerebbe aumentare chi2 se la probabilita' e' bassa
  chi2 *= xx;  // pero' funziona meglio cosi'  ?? ==> da studiare !!
#endif

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                     // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // weight of last gamma
    i2  = p2->ind;
    ie  = (int)egE;
    xx  = distmat_ge[i1][i2] * mutot[ie];
    xx  = MIN(20., xx);                   // to avoid underflow in exp(-xx)
    xx  = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }

#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVE;
#endif

  return chi2;
}

double
tcomptVtxCe(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double eeE, eeP, eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  eeE  = p1->ee;
  eg1  = eg0 - eeE;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  eeP  = eg0 * eg1 * M0C2I * ( 1.0 - cosP);

  chi2 = (eeE - eeP)/eeE;
  chi2 *= chi2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                      // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    xx = MIN(20., xx);                    // to avoid underflow in exp(-xx)
    xx = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }

#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVe;
#endif

  return chi2;
}

double
tcomptVtxCA(point *p0, point *p1, point *p2, double eg0)
{
  double cosP, cosE;
  double eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  eg1  = eg0 - p1->ee;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  cosE = 1. - M0C2 * (eg0 - eg1) / (eg0 * eg1);

  chi2 = cosP - cosE;
  chi2 *= chi2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                      // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    xx = MIN(20., xx);                    // to avoid underflow in exp(-xx)
    xx = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }

#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVA;
#endif

  return chi2;
}

double
tcomptVtxCES(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double egE, egP;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdee1, dVdegE, dVdcos;
  double sig2ee1, sig2egE, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  egE  = eg0 - p1->ee;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  egP  = eg0 / ( 1.0 + eg0 * M0C2I * (1.0 - cosP) );

  vval = egE - egP;

  dVdee1 = egP/eg0;  dVdee1 *= dVdee1;
  dVdegE = 1. - dVdee1;
  dVdcos = egP*egP*M0C2I;

  sig2ee1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;
  sig2ee1 *= dVdee1*dVdee1;

  sig2egE  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * egE/1332.51;
  sig2egE *= dVdegE*dVdegE;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
//matIncr((int)(RAD2DEG*acos(cosP)), (int)(100.*sqrt(sig2pos)), 0);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2ee1+sig2egE+sig2pos;
//matIncr((int)(fabs(vval)/10), (int)(sqrt(sig2)), 0);

  chi2 = vval*vval/sig2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                     // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2  = p2->ind;
    ie  = (int)egE;
    xx  = distmat_ge[i1][i2] * mutot[ie];
    xx  = MIN(20., xx);                   // to avoid underflow in exp(-xx)
    xx  = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }

#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVES;
#endif

  return chi2;
}

double
tcomptVtxCeS(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double eeE, eeP, eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdeeE, dVdeg1, dVdcos;
  double sig2eeE, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  eeE  = p1->ee;
  eg1  = eg0 - eeE;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  eeP  = eg0 * eg1 * M0C2I * ( 1.0 - cosP);

  vval = eeE - eeP;

  dVdeeE = 1. - eeP / eg0;
  dVdeg1 = (eg0 + eg1) * eeP / ( eg0 * eg1);
  dVdcos = eg0*eg1*M0C2I;

  sig2eeE  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;  // s2(ee1) ~ 2 keV
  sig2eeE *= dVdeeE*dVdeeE;

  sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1/1332.51;
  sig2eg1 *= dVdeg1*dVdeg1;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2eeE+sig2eg1+sig2pos;

  chi2 = vval*vval/sig2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                      // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    xx = MIN(20., xx);                    // to avoid underflow in exp(-xx)
    xx = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }
  
#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVeS;
#endif

  return chi2;
}

double
tcomptVtxCAS(point *p0, point *p1, point *p2, double eg0)
{
  double cosP, cosE;
  double ee1, eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdee1, dVdeg1, dVdcos;
  double sig2ee1, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  ee1  = p1->ee;
  eg1  = eg0 - ee1;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  cosE = 1. - M0C2 * (eg0 - eg1) / (eg0 * eg1);

  vval = cosP - cosE;

  dVdee1 = M0C2/(eg0*eg0);
  dVdeg1 = ee1*M0C2*(eg0+eg1)/(eg0*eg0*eg1*eg1);
  dVdcos = 1.;

  sig2ee1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;  // s2(ee1) ~ 2 keV
  sig2ee1 *= dVdee1*dVdee1;

  sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1/1332.51;
  sig2eg1 *= dVdeg1*dVdeg1;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2ee1+sig2eg1+sig2pos;

  chi2 = vval*vval/sig2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0 = p0->ind;
  i1 = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  xx  = MIN(20., xx);                      // to avoid underflow in exp(-xx)
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 /= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    xx = MIN(20., xx);                     // to avoid underflow in exp(-xx)
    xx = exp(-xx);
//  xx *= sphot[ie];
    chi2 /= xx;
  }

#if SCALECHI2 == 1
  chi2 *= CHI2SCALE_CVAS;
#endif

  return chi2;
}

double
figOfMeritC(point **pp, int np)
{
  static int ne = -1;
  static int nn = 0;
  int ii;
  double chi2, eg0, egg;
  double chi[MAXPOINTS+1];

  if(evnum != ne) {
    ne = evnum;
    nn = 0;
  }
  printf("%3d %3d\n", evnum, nn++);

  if(np < 2)
    return 0;

  eg0 = 0;
  for(ii = 0; ii < np; ii++) {
    eg0 += pp[ii]->ee;
    printf("%3d %3d %3d %10.3f %10.3f\n", evnum, nn-1, ii, pp[ii]->ee, eg0); 
    chi[ii] = 0.;
  }

  chi2 = 0.;
  egg  = eg0;
  for(ii = 0; ii < np-1; ii++) {
    firstVtx = ii == 0;
    lastVtx  = ii == np-2;
    if(firstVtx)
      chi[ii]  = tcomptVtx(&origin,  pp[ii], pp[ii+1], egg);
    else
      chi[ii]  = tcomptVtx(pp[ii-1], pp[ii], pp[ii+1], egg);
    chi2 += chi[ii];
    printf("%3d %3d %3d %10.5f %10.5f %10.3f --> %8.3f\n", evnum, nn-1, ii, chi[ii], chi2, egg, egg-pp[ii]->ee);
    egg  -= pp[ii]->ee;
  }
  return chi2;
}

// assume that part of energy is missing and determine it by minimization of chi2
// expansion and printout to use gnuplot defining:
// gnuplot> f(x) = x0 + x1*x + x2*x**2 + x3*x**3 + x4*x**4
double
restEnerCe(point **qq, int np)
{
  int    ncv, nv, ii;
  point *pp[MAXPOINTS+1];
  double eo[MAXPOINTS+1];       // egout(v]
  double ci[MAXPOINTS+1];       // (1-cosP[v])/m
  double s2[MAXPOINTS+1];       // sigma2[v]
  double eplus[MAXPOINTS+1];    // ego+eg1
  double etime[MAXPOINTS+1];    // ego*eg1
  double ccs2[MAXPOINTS+1];     // ci[nv]*ci[nv]/sigma2
  double ces2[MAXPOINTS+1];     // ci[vn]*eeE/sigma2
  double ees2[MAXPOINTS+1];     // eeE*eeE/sigma2
  double x0, x1, x2, x3, x4;    // expansion of chi2
  double k0, k1, k2, k3;        // expansion of its derivative
  double eg0, eeE, eeP, eg1, cosP, vval, chi2;
  double ener, emin, chim, em1, em2;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdeeE, dVdeg1, dVdcos;
  double sig2eeE, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  point *p0, *p1, *p2;

//np -= 1; // remove last point to test procedure (if called with accepted clusters)

  if(np < 2) return 0.;
  ncv = np - 1;

  pp[0] = &origin;
  eo[0] = 0.;
  for(ii = 0; ii < np; ii++) {
    pp[ii+1] = qq[ii];
    eo[0]   += qq[ii]->ee;
  }

  for(nv = 1; nv <= ncv; nv++) {
    p0 = pp[nv-1];
    p1 = pp[nv];
    p2 = pp[nv+1];

    d01x = p1->xx - p0->xx;
    d01y = p1->yy - p0->yy;
    d01z = p1->zz - p0->zz;
    d01d = d01x*d01x + d01y*d01y + d01z*d01z;
    
    d12x = p2->xx - p1->xx;
    d12y = p2->yy - p1->yy;
    d12z = p2->zz - p1->zz;
    d12d = d12x*d12x + d12y*d12y + d12z*d12z;
    
    d20x = p0->xx - p2->xx;
    d20y = p0->yy - p2->yy;
    d20z = p0->zz - p2->zz;
    d20d = d20x*d20x + d20y*d20y + d20z*d20z;
    
    eg0  = eo[nv-1];
    eeE  = p1->ee;
    eg1  = eg0 - eeE;
    cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
    eeP  = eg0 * eg1 * M0C2I * ( 1.0 - cosP);
    
    vval = eeE - eeP;
    
    dVdeeE = 1. - eeP / eg0;
    dVdeg1 = (eg0 + eg1) * eeP / ( eg0 * eg1);
    dVdcos = eg0*eg1*M0C2I;
    
    sig2eeE  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;
    sig2eeE *= dVdeeE*dVdeeE;
    
    sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1/1332.51;
    sig2eg1 *= dVdeg1*dVdeg1;
    
    sig2p0  = 1./d01d;
    sig2p2  = 1./d12d;
    sig2p1  = d20d*sig2p0*sig2p2;
    
    sig2p0 *= p0->s2;
    sig2p1 *= p1->s2;
    sig2p2 *= p2->s2;
    
    sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
    sig2pos *= dVdcos*dVdcos;
    
    sig2 = sig2eeE+sig2eg1+sig2pos;
    
    chi2 = vval*vval/sig2;
    
    eo[nv] = eg1;
    ci[nv] = (1. - cosP)*M0C2I;
    s2[nv] = sig2;
    eplus[nv] = eo[nv-1] + eo[nv];
    etime[nv] = eo[nv-1] * eo[nv];
    ccs2[nv] = ci[nv] * ci[nv] / s2[nv];
    ces2[nv] = ci[nv] * eeE / s2[nv];
    ees2[nv] = eeE * eeE / s2[nv];
  }
    
  x0 = x1 = x2 = x3 = x4 = 0;
  for(nv = 1; nv <= ncv; nv++) {
    x4 += ccs2[nv];
    x3 += ccs2[nv] * eplus[nv];
    x2 += ccs2[nv]*(2.*etime[nv] + eplus[nv]*eplus[nv]) - 2.*ces2[nv];
    x1 += ccs2[nv] * etime[nv] * eplus[nv] - ces2[nv] * eplus[nv];
    x0 += ccs2[nv] * etime[nv] * etime[nv] - 2.* ces2[nv] * etime[nv] + ees2[nv];
  }
  x3 *= 2.;
  x1 *= 2.;

  k0 =    x1;
  k1 = 2.*x2;
  k2 = 3.*x3;
  k3 = 4.*x4;

// a very brutal mimimization
  em1 = -100.;
  em2 = 1000.;
  ener = em1;
  chi2 = x0 + x1 * ener + x2 * ener*ener + x3 * ener*ener*ener + x4 * ener*ener*ener*ener;
  emin = ener;
  chim = chi2;
  for(ener = em1; ener < em2; ener += 10.) {
    chi2 = x0 + x1 * ener + x2 * ener*ener + x3 * ener*ener*ener + x4 * ener*ener*ener*ener;
    if(chi2 < chim) {
      chim = chi2;
      emin = ener;
    }
  }
  em1 = emin - 10.;
  em2 = emin + 10.;
  for(ener = em1; ener < em2; ener += 1.) {
    chi2 = x0 + x1 * ener + x2 * ener*ener + x3 * ener*ener*ener + x4 * ener*ener*ener*ener;
    if(chi2 < chim) {
      chim = chi2;
      emin = ener;
    }
  }
  em1 = emin - 1.;
  em2 = emin + 1.;
  for(ener = em1; ener < em2; ener += .1) {
    chi2 = x0 + x1 * ener + x2 * ener*ener + x3 * ener*ener*ener + x4 * ener*ener*ener*ener;
    if(chi2 < chim) {
      chim = chi2;
      emin = ener;
    }
  }

  printf("\n esum = %6.1f  emin = %6.1f --> etot = %6.1f  chi2 = %.2f\n", eo[0], emin, eo[0]+emin, chim);
  printf(" f(x) = x0 + x1*x + x2*x**2 + x3*x**3 + x4*x**4\n");
  printf(" x0 = %g\n", x0);
  printf(" x1 = %g\n", x1);
  printf(" x2 = %g\n", x2);
  printf(" x3 = %g\n", x3);
  printf(" x4 = %g\n", x4);
  printf(" plot f(x)\n\n");

  return 0.;
}

//////////////////////////////////////////////////////////////////////
/////////////// Functions for vertexStyle = VLIKELIHOOD /////////////////
//////////////////////////////////////////////////////////////////////

void
testphotoL(clust *pcl)
{
  double  dist, xx;
  int     nn, iegam;
return;  
  if(pcl->npoints != 1) return;
  
  nn    = pcl->ppc[0]->ind;
  dist  = distmat_ge[nn][nn];
  iegam = (int)pcl->eclust;
  if(pcl->dist_dd > PHOTODIST) {
    pcl->mechanism  = Photo;
    pcl->chibest    = chi2Accept * PHOTOFACT;       // to be adjusted to a more sensible value
    xx = dist*mutot[iegam]; xx = MIN(20., xx);
    pcl->chibest   /= exp(-xx) * sphot[iegam] * pcl->dist_dd;
    pcl->chibest    = MIN(pcl->chibest, chi2Track);
    pcl->ordbest[0] = '0';
    pcl->ordbest[1] = 0;
    if(recoil.beta) {
      pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
    }
  }
}

void
testpair1L(clust *pcl)
{
  int     npc, ii, jj, kk, j1, j2, j3, iene;
  double  eclust, epair, dpair, ej1, ej2, em0c2, dm0c2;
  double  dist;
  char   *pch, tch;
  point  **ppc, *ppt;
return;
  npc = pcl->npoints;
  if(npc < 3) return;

  eclust = pcl->eclust;
  if(eclust < M0C2P) return;
  
  pch = pcl->cstring;
  ppc = &pcl->ppc[0];
  for (jj = 0, ii = pcl->firstbit; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') ppc[jj++] = ppev[ii];
  }

  epair = eclust - M0C2P;
  dpair = peakwidth[(int)epair];
  dm0c2 = peakwidth[511];
  for (ii = 0; ii < npc; ii++) {
    if(fabs(ppc[ii]->ee - epair) > dpair) continue;
    for (j1 = 0; j1 < npc; j1++) {            // look for a 511 as a single point
      em0c2 = ppc[j1]->ee;
      if(j1 == ii) continue;
      if(fabs(em0c2-M0C2) < dm0c2) goto OK;
    }
    if(npc > 4) {                             //  look for a 551 summing 2 points
      for (j1 = 0; j1 < npc - 1; j1++) {
        if(j1 == ii) continue;
        ej1 = ppc[j1]->ee;
        for (j2 = j1 + 1; j2 < npc; j2++) {
          if(j2 == ii || j2 == j1) continue;
          em0c2 =  ej1 + ppc[j2]->ee;
          if(fabs(em0c2-M0C2) < dm0c2) goto OK;
        }
      }
    }
    if(npc > 6) {                             //  look for a 551 summing 3 points
      for (j1 = 0; j1 < npc - 2; j1++) {
        if(j1 == ii) continue;
        ej1 = ppc[j1]->ee;
        for (j2 = j1 + 1; j2 < npc -1; j2++) {
          if(j2 == ii || j2 == j1) continue;
          ej2 = ej1 + ppc[j2]->ee;
          for (j3 = j2 + 1; j3 < npc; j3++) {
            if(j3 == ii || j3 == j1 || j3 == j2) continue;
            em0c2 = ej2 + ppc[j3]->ee;
            if(fabs(em0c2-M0C2) < dm0c2) goto OK;
          }
        }
      }
    }
  }
  return;

OK:    
  kk    = pcl->ppc[ii]->ind;
  dist  = distmat_ge[kk][kk];
  iene  = (int)eclust;
  dist *= mutot[iene];
  if(dist > 10)
    return;
  pcl->mechanism = Pair0;
  pcl->chibest   = chi2Accept/1000.;          // to be adjusted to a more sensible value
  pcl->chibest  /= exp(-dist) * spair[iene];
  if(ii) SWAP(ppc[0], ppc[ii], ppt)
  for (jj = 0; jj < npc; jj++) pcl->ordbest[jj] = '0' + jj;
  pcl->ordbest[npc] = 0;
  if(ii) SWAP(pcl->ordbest[0], pcl->ordbest[ii], tch)
  if(recoil.beta) {
    pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
  }
  return;
}

void
testcomptL(clust *pcl)
{
  int     ii, jj, npc;
  double  X2factor, X2best;
  char   *pch;
  
  npc = pcl->npoints;
  if(npc < 2 || npc > MAXPERM) return;

  ///////////////////////////////////////////////////////////////////
  // riempie la struttura tc con puntatori e permutazione iniziale //
  ///////////////////////////////////////////////////////////////////

  tc.npc = npc;
  tc.clu = pcl;
  tc.ppc[0] = &origin;
  pch = pcl->cstring;
  for (jj = 1, ii = pcl->firstbit; ii <= pcl->lastbit; ii++) {
    if(pch[ii] > '0') tc.ppc[jj++] = ppev[ii];
  }
  for (ii = 0; ii <= npc; ii++) tc.pperm[ii] = ii;

  ////////////////////////////////////////////////
  // normalizzazioni e fattori empirici al chi2 //
  ////////////////////////////////////////////////

  X2factor = 1;                                   // niente per adesso

  X2best = X2factor * chi2Track;                  // questo e' il valore della MINIMA likelihood

  ////////////////////////////////////////
  // calcolo della miglior permutazione //
  ////////////////////////////////////////
  
  tc.X2best  = pow(X2best, npc-1.);               // setta la migliore probabilita' consentita

  testcomptLR(1, pcl->eclust, 1.);                // calcola le permutazioni ricorsivamente, partendo dal primo punto del cluster

  tc.X2best = pow(tc.X2best, 1./(npc-1.));        // riscala con la radice del numero di vertici

  if(tc.X2best/X2best < 1.00001)
    return;                                       // non ha trovato niente entro il limite

  ///////////////////////////////////////
  // aggiorna pcl con i valori trovati //
  ///////////////////////////////////////

  pcl->chibest = tc.X2best / X2factor;
  for (ii = 1; ii <= npc; ii++) {
    pcl->ordbest[ii-1] = '0' + tc.bperm[ii] -1;
    pcl->ppc[ii-1]     = tc.ppc[tc.bperm[ii]];
  }
  pcl->ordbest[npc] = 0;
  pcl->mechanism = Compt;
  if(recoil.beta) {
    pcl->egamma *= recoil.gamma * (1. - recoil.beta * costhrec(pcl->ppc[0]) );
  }
}

void
testcomptLR(int ll, double eg0, double X0)
{
  int    ii, jj;
  double eg1, X1;
  
  for(ii = ll; ii <= tc.npc; ii++) {
    if(ii != ll) {jj = tc.pperm[ll]; tc.pperm[ll] = tc.pperm[ii]; tc.pperm[ii] = jj;}
    firstVtx = ll == 2;         // queste due flag vanno ricalcolate ogni volta perche' sono
    lastVtx  = ll == tc.npc;    // globali possono essere cambiate dentro la chiamata ricorsiva
    X1  = X0;
    eg1 = eg0;
    if(ll > 1) {                // il vertice (ll-1) va calcolato solo a partire da ll=2
      X1 *= tcomptVtx(tc.ppc[tc.pperm[ll-2]], tc.ppc[tc.pperm[ll-1]], tc.ppc[tc.pperm[ll]], eg0);
      eg1 = eg0 - tc.ppc[tc.pperm[ll-1]]->ee;
    }
    if(X1 > tc.X2best) {
      if(lastVtx) {
        tc.X2best = X1;
        for(jj = 0; jj <= tc.npc; jj++) tc.bperm[jj] = tc.pperm[jj];
      }
      else {
        testcomptLR(ll + 1, eg1, X1);
      }
    }
    if(ii != ll) {jj = tc.pperm[ll]; tc.pperm[ll] = tc.pperm[ii]; tc.pperm[ii] = jj;}
  }
}

double
tcomptVtxLES(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double egE, egP;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdee1, dVdegE, dVdcos;
  double sig2ee1, sig2egE, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  egE  = eg0 - p1->ee;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  egP  = eg0 / ( 1.0 + eg0 * M0C2I * (1.0 - cosP) );

  vval = egE - egP;

  dVdee1 = egP/eg0;  dVdee1 *= dVdee1;
  dVdegE = 1. - dVdee1;
  dVdcos = egP*egP*M0C2I;

  sig2ee1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;
  sig2ee1 *= dVdee1*dVdee1;

  sig2egE  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * egE/1332.51;
  sig2egE *= dVdegE*dVdegE;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
//matIncr((int)(RAD2DEG*acos(cosP)), (int)(100.*sqrt(sig2pos)), 0);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2ee1+sig2egE+sig2pos;
//matIncr((int)(eg0/10), (int)(sqrt(sig2)), 0);

  chi2 = vval*vval/sig2;
  if(chi2 > 40)
    return 0;

  chi2 = exp(-0.5*chi2);
//if(firstVtx) chi2 *= chi2;

#if WEIGHTKN == 1
  // Weigh with Klein-Nishina (unpolarized)
  xx = wkn2(egE/eg0, 1-cosP*cosP);
  chi2 *= xx;
#endif

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0  = p0->ind;
  i1  = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  if(xx > 10) return 0;                   // cut on long paths
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 *= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)egE;
    xx = distmat_ge[i1][i2] * mutot[ie];
    if(xx > 10) return 0;                  // cut on long paths
    xx  = exp(-xx);
    xx *= sphot[ie];
    chi2 *= xx;
  }
  return chi2;
}

double
tcomptVtxLeS(point *p0, point *p1, point *p2, double eg0)
{
  double cosP;
  double eeE, eeP, eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdeeE, dVdeg1, dVdcos;
  double sig2eeE, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  eeE  = p1->ee;
  eg1  = eg0 - eeE;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  eeP  = eg0 * eg1 * M0C2I * ( 1.0 - cosP);

  vval = eeE - eeP;

  dVdeeE = 1. - eeP / eg0;
  dVdeg1 = (eg0 + eg1) * eeP / ( eg0 * eg1);
  dVdcos = eg0*eg1*M0C2I;

  sig2eeE  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;  // s2(ee1) ~ 2 keV
  sig2eeE *= dVdeeE*dVdeeE;

  sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1/1332.51;
  sig2eg1 *= dVdeg1*dVdeg1;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2eeE+sig2eg1+sig2pos;

  chi2 = vval*vval/sig2;
  if(chi2 > 40)
    return 0;

  chi2 = exp(-0.5*chi2);
//if(firstVtx) chi2 *= chi2;

  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0  = p0->ind;
  i1  = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  if(xx > 10) return 0;                   // cut on long paths
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 *= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    if(xx > 10) return 0;                 // cut on long paths
    xx  = exp(-xx);
    xx *= sphot[ie];
    chi2 *= xx;
  }
  return chi2;
}

double
tcomptVtxLAS(point *p0, point *p1, point *p2, double eg0)
{
  double cosP, cosE;
  double ee1, eg1;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdee1, dVdeg1, dVdcos;
  double sig2ee1, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos, sig2;
  double vval, chi2;
  int    i0, i1, i2, ie;
  double xx;
  
  d01x = p1->xx - p0->xx;
  d01y = p1->yy - p0->yy;
  d01z = p1->zz - p0->zz;
  d01d = d01x*d01x + d01y*d01y + d01z*d01z;

  d12x = p2->xx - p1->xx;
  d12y = p2->yy - p1->yy;
  d12z = p2->zz - p1->zz;
  d12d = d12x*d12x + d12y*d12y + d12z*d12z;

  d20x = p0->xx - p2->xx;
  d20y = p0->yy - p2->yy;
  d20z = p0->zz - p2->zz;
  d20d = d20x*d20x + d20y*d20y + d20z*d20z;

  ee1  = p1->ee;
  eg1  = eg0 - ee1;
  cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
  cosE = 1. - M0C2 * (eg0 - eg1) / (eg0 * eg1);

  vval = cosP - cosE;

  dVdee1 = M0C2/(eg0*eg0);
  dVdeg1 = ee1*M0C2*(eg0+eg1)/(eg0*eg0*eg1*eg1);
  dVdcos = 1.;

  sig2ee1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * p1->ee/1332.51;  // s2(ee1) ~ 2 keV
  sig2ee1 *= dVdee1*dVdee1;

  sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1/1332.51;
  sig2eg1 *= dVdeg1*dVdeg1;

  sig2p0  = 1./d01d;
  sig2p2  = 1./d12d;
  sig2p1  = d20d*sig2p0*sig2p2;

  sig2p0 *= p0->s2;
  sig2p1 *= p1->s2;
  sig2p2 *= p2->s2;

  sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
  sig2pos *= dVdcos*dVdcos;

  sig2 = sig2ee1+sig2eg1+sig2pos;

  chi2 = vval*vval/sig2;
  if(chi2 > 40)
    return 0;

  chi2 = exp(-0.5*chi2);
//if(firstVtx) chi2 *= chi2;
  
  // Weigh chi2 with probability' that the incoming gamma Compton scatters
  // after traveling from p0 to p1
  // The equivalent probability for the outgoing gamma should not be used here
  // as this will be the incoming gamma for the next vertex
  i0  = p0->ind;
  i1  = p1->ind;
  if(i0 < 0) i0 = i1;
  ie  = (int)eg0;
  xx  = distmat_ge[i0][i1] * mutot[ie];
  if(xx > 10) return 0;                   // cut on long paths
  xx  = exp(-xx);
  xx *= scomp[ie];
  chi2 *= xx;

  if(lastVtx) {                           // peso dell'ultimo gamma
    i2 = p2->ind;
    ie = (int)eg1;
    xx = distmat_ge[i1][i2] * mutot[ie];
    if(xx > 10) return 0;                 // cut on long paths
    xx  = exp(-xx);
    xx *= sphot[ie];
    chi2 *= xx;
  }

  return chi2;
}

double
figOfMeritL(point **pp, int np)
{
  static int ne = -1;
  static int nn = 0;
  int ii;
  double chi2, eg0, egg;
  double chi[MAXPOINTS+1];

  if(evnum != ne) {
    ne = evnum;
    nn = 0;
  }
  printf("%3d %3d\n", evnum, nn++);

  if(np < 2)
    return 0;

  eg0 = 0;
  for(ii = 0; ii < np; ii++) {
    eg0 += pp[ii]->ee;
    printf("%3d %3d %3d %10.3f %10.3f\n", evnum, nn-1, ii, pp[ii]->ee, eg0); 
    chi[ii] = 0.;
  }

  chi2 = 1.;
  egg  = eg0;
  for(ii = 0; ii < np-1; ii++) {
    firstVtx = ii == 0;
    lastVtx  = ii == np-2;
    if(firstVtx)
      chi[ii]  = tcomptVtx(&origin,  pp[ii], pp[ii+1], egg);
    else
      chi[ii]  = tcomptVtx(pp[ii-1], pp[ii], pp[ii+1], egg);
    chi2 *= chi[ii];
    printf("%3d %3d %3d %10.5f %10.5f %10.3f --> %8.3f\n", evnum, nn-1, ii, chi[ii], chi2, egg, egg-pp[ii]->ee);
    egg  -= pp[ii]->ee;
  }
  if(chi2 > 0)
    chi2 = pow(fabs(chi2), 1./(np-1.));
  printf("%3d %3d %3d            %10.5f\n", evnum, nn-1, ii, chi2);
  return chi2;
}

/*
double cluchisq[MAXEVLEN];                //
int    clushared[MAXEVLEN];                // 
double cluweights[MAXEVLEN][MAXEVLEN];      // 

void
setCluWeights()
{
  int     ii, jj, mm;
  clust  *pcl;
  double  chisum;

  memset(cluchisq, 0, sizeof(cluchisq));
  memset(clushared, 0, sizeof(clushared));
  for (pcl = clusterspool, ii = 0; ii < nclupool; ii++, pcl++) {
    cluchisq[ii] = pcl->chibest;
    memset(cluweights[ii], 0, sizeof(double)*npev);
    for(jj = 0; jj < pcl->npoints; jj++) {
      mm = pcl->ppc[jj]->ind;
      cluweights[ii][mm] = 1;
      clushared[mm]++;
    }
  }

  for(jj = 0; jj < npev; jj++) {
    if(clushared[jj] < 1) continue;
    chisum = 0.;
    for(ii = 0; ii < nclupool; ii++) {
      if(cluweights[ii][jj])
        chisum += 1./cluchisq[ii];
    }
    for(ii = 0; ii < nclupool; ii++) {
      if(cluweights[ii][jj])
        cluweights[ii][jj] = 1./cluchisq[ii]/chisum;
    }
  }

}
*/


//makes sense only for m=1
void
statVtx(point **pp, int nn)
{

  static int initialize = 1, firstmat = 0;
  int    ii, base, good;
  point *p0, *p1, *p2;
  double eg0E;
  double eg1E, eg1P;
  double ee1E, ee1P;
  double cosP, cosE;
  double d01x, d01y, d01z, d01d;
  double d12x, d12y, d12z, d12d;
  double d20x, d20y, d20z, d20d;
  double dVdee1, dVdeg1, dVdcos;
  double sig2ee1, sig2eg1, sig2p0, sig2p1, sig2p2, sig2pos;
  double sig2e, sig2g, sig2c, sigma;

  if(nn < 2) return;

  if(initialize) {
    firstmat = matHowMany();

    matDefine(MATSIZE, MATSIZE, "mateg-p.dat", "[eg(E) , eg(P)] if peak");
    matDefine(MATSIZE, MATSIZE, "matee-p.dat", "[ee(E) , ee(P)] if peak");
    matDefine(MATSIZE, MATSIZE, "matcc-p.dat", "[cc(P) , cc(E)] if peak");

    matDefine(MATSIZE, MATSIZE, "mategVp.dat", "[D(eg) , S(eg)] if peak");
    matDefine(MATSIZE, MATSIZE, "mateeVp.dat", "[D(ee) , S(ee)] if peak");
    matDefine(MATSIZE, MATSIZE, "matccVp.dat", "[D(cc) , S(cc)] if peak");

    matDefine(MATSIZE, MATSIZE, "mateg-b.dat", "[eg(E) , eg(P)] if backgr");
    matDefine(MATSIZE, MATSIZE, "matee-b.dat", "[ee(E) , ee(P)] if backgr");
    matDefine(MATSIZE, MATSIZE, "matcc-b.dat", "[cc(P) , cc(E)] if backgr");

    matDefine(MATSIZE, MATSIZE, "mategVb.dat", "[D(eg) , S(eg)] if backgr");
    matDefine(MATSIZE, MATSIZE, "mateeVb.dat", "[D(ee) , S(ee)] if backgr");
    matDefine(MATSIZE, MATSIZE, "matccVb.dat", "[D(cc) , S(cc)] if backgr");
    initialize = 0;
  }


  for(eg0E = 0., ii = 0; ii < nn; ii++) eg0E += pp[ii]->ee;
  good    = peaklut[(int)eg0E];
  if(good)
    base = firstmat;
  else
    base = firstmat+6;
  
  p0 = &origin;
  for(ii = 0; ii < nn-1; ii++) {
    p1 = pp[ii];
    p2 = pp[ii+1];

    d01x = p1->xx - p0->xx;
    d01y = p1->yy - p0->yy;
    d01z = p1->zz - p0->zz;
    d01d = d01x*d01x + d01y*d01y + d01z*d01z;

    d12x = p2->xx - p1->xx;
    d12y = p2->yy - p1->yy;
    d12z = p2->zz - p1->zz;
    d12d = d12x*d12x + d12y*d12y + d12z*d12z;

    d20x = p0->xx - p2->xx;
    d20y = p0->yy - p2->yy;
    d20z = p0->zz - p2->zz;
    d20d = d20x*d20x + d20y*d20y + d20z*d20z;

    ee1E = p1->ee;
    eg1E = eg0E - ee1E;
    cosP = (d01x*d12x + d01y*d12y + d01z*d12z)/sqrt(d01d*d12d);
    eg1P = eg0E / ( 1.0 + eg0E * M0C2I * ( 1.0 - cosP) );
    ee1P = eg0E * eg1E * M0C2I * ( 1.0 - cosP);
    cosE = 1. - M0C2 * ee1E / (eg0E * eg1E);

    matIncr( (int)(eg1E),            (int)(eg1P),        base  );
    matIncr( (int)(ee1E),            (int)(ee1P),        base+1);
    matIncr( (int)(100.*cosP+500.), (int)(100*cosE+500), base+2);
    
    sig2p0  = 1./d01d;
    sig2p2  = 1./d12d;
    sig2p1  = d20d*sig2p0*sig2p2;
    sig2p0 *= p0->s2;
    sig2p1 *= p1->s2;
    sig2p2 *= p2->s2;
    sig2pos  = (sig2p0 + sig2p1 + sig2p2)*(1- cosP*cosP);
    
    sig2ee1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * ee1E/1332.51;
    sig2eg1  = pow(fwhn/SIG2FW, 2.) + pow(fwhm/SIG2FW, 2.) * eg1E/1332.51;

    dVdee1 = eg1P/eg0E;  dVdee1 *= dVdee1;
    dVdeg1 = 1. - dVdee1;
    dVdcos = eg1P*eg1P*M0C2I;

    sig2e = sig2ee1*dVdee1*dVdee1;
    sig2g = sig2eg1*dVdeg1*dVdeg1;
    sig2c = sig2pos*dVdcos*dVdcos;
    sigma = sqrt(sig2e+sig2g+sig2c);

    matIncr( (int)(10*fabs(eg1E-eg1P)), (int)(10*sigma), base+3);
    
    dVdee1 = 1. - ee1P / eg0E;
    dVdeg1 = (eg0E + eg1E) * ee1P / ( eg0E * eg1E);
    dVdcos = eg0E*eg1E*M0C2I;
    
    sig2e = sig2ee1*dVdee1*dVdee1;
    sig2g = sig2eg1*dVdeg1*dVdeg1;
    sig2c = sig2pos*dVdcos*dVdcos;
    sigma = sqrt(sig2e+sig2g+sig2c);

    matIncr( (int)(10*fabs(ee1E-ee1P)), (int)(10*sigma), base+4);
    
    dVdee1 = M0C2/(eg0E*eg0E);
    dVdeg1 = ee1E*M0C2*(eg0E+eg1E)/(eg0E*eg0E*eg1E*eg1E);
    dVdcos = 1.;
    
    sig2e = sig2ee1*dVdee1*dVdee1;
    sig2g = sig2eg1*dVdeg1*dVdeg1;
    sig2c = sig2pos*dVdcos*dVdcos;
    sigma = sqrt(sig2e+sig2g+sig2c);
    
    matIncr( (int)(500*fabs(cosP-cosE)), (int)(500*sigma), base+5);

    eg0E = eg1E;
    p0   = p1;
  }
}

#ifdef FORPRISMA
void 
addGammaToFile( clust *pcl )
{
  double int1th, int1ph;

  if( !doPrismaEvs ) return;

  int1th = RAD2DEG*acos(pcl->ppc[0]->cz);
  int1ph = RAD2DEG*atan2(pcl->ppc[0]->cy, pcl->ppc[0]->cx);
  if (int1ph < 0) int1ph += 360.;

  fprintf( ofp, "%3d %9.3lf %7.3lf %7.3lf %3d\n", pcl->ppc[0]->nd, pcl->eclust, int1th, int1ph, pcl->ppc[0]->ss );

  return;
}

#endif
