////////////////////////////////////////////////////
/////////// GEOMETRY: CBAR /////////////////////////
////////////////////////////////////////////////////

//#include "mgt.h"
#include "mgt.extern.h"
#include "mgt_geocbar.h"

/*
int     nbshit;               // number of detector bounding spheres hit by the ray 1-2
int    *ibshit;               // which bounding spheres are hit

double  r_germ;               // radius       of ge crystal
double  h_germ;               // half-length  of ge_crystal
double  r_germ2;              // outer radius of ge crystal squared
double  r_bsphere;            // radius of bounding sphere
double  r_bsphere2;           // radius of bounding sphere squared

double  dist12;               // spatial distance between the 2 considered points

int     nsegsZ;               // number of segments along  detector axis
int     nsegsP;               // number of segments around detector axis
int     nsegsR;               // number of segments along  detector radius
double  ssegsZ;               // size of segment along  crystal axis
double  ssegsP;               // size of segment around crystal axis
double  ssegsR;               // size of segment along  crystal radius
*/

int     insidegedet_cbar    (point *);               //
void    findsegment_cbar    (point *);               //
void    setsegcenter_cbar   (point *);               //
double  gedistance_cbar     (point *, point *);      //

void
geominit_cbar()
{
  int    ii, nn;
  double dim1, dim2;
  char   dummy_line[128];
  
  insidegedet  = insidegedet_cbar;
  findsegment  = findsegment_cbar;
  setsegcenter = setsegcenter_cbar;
  gedistance   = gedistance_cbar;
  if(fscanf(ifp, "%d %lf %lf", &ngedets, &dim1, &dim2) != 3) {
    printf("Error reading %s\n", ifname);
    errexit("geominit_cbar");
  }
  dim1 *= distfactor;
  dim2 *= distfactor;
  if( dataFileVersion == FAGATA ) {
    fscanf( ifp, "\n%s", dummy_line );  // ENDGEOMETRY line
  }
  printf("Initializing geometry for the M%2.2dC configuration\n", ngedets);
  
  CDet   = (coors *)calloc(ngedets, sizeof(coors));
  ibshit = (int   *)calloc(ngedets, sizeof(int));

  for(nn = 0; nn < ngedets; nn++) {
    if (fscanf(ifp, "%d", &ii) != 1) {
      printf("Error reading geometry data from %s\n", ifname);
      errexit("geominit_cbar");
    }
    if (ii != nn) {
      printf("Problem reading geometry data from %s\n", ifname);
      errexit("geominit_cbar");
    }
    else {
      if (fscanf(ifp, "%lf %lf %lf", &CDet[nn].xx, &CDet[nn].yy, &CDet[nn].zz) != 3) {
        printf("Error reading geometry data from %s\n", ifname);
        errexit("geominit_cbar\n");
      }
      CDet[nn].xx *= distfactor;
      CDet[nn].yy *= distfactor;
      CDet[nn].zz *= distfactor;
    }
  }
  
  r_germ     = dim1/2 + 0.1;
  h_germ     = dim2/2 + 0.1;
  r_germ2    = r_germ * r_germ;
  r_bsphere  = sqrt(r_germ * r_germ + h_germ * h_germ); 
  r_bsphere2 = r_bsphere * r_bsphere;
  nsegsZ     = 4;
  nsegsP     = 6;
  nsegsR     = 1;
  nsegdet    = nsegsZ * nsegsP * nsegsR;
  ssegsZ     = (2. * h_germ) / nsegsZ;
  ssegsP     = (2. * M_PI  ) / nsegsP;
  ssegsR     =       r_germ  / nsegsR;
  nsegtot    = nsegdet * ngedets;
  GEOMETRY   = CBAR;
  if(nsegsR <2)
    sprintf(segmentstring, "(Phi=%d Z=%d)", nsegsP, nsegsZ);
  else
    sprintf(segmentstring, "(Phi=%d Z=%d R=%d)", nsegsP, nsegsZ, nsegsR);
}

void
setbshits_cbar(coors *p1, coors *p2)
{
  int     ndet1, ndet2, ii;
  double  dbs, dbs2;
  double  c12x, c12y, c12z;
  double  c1cx, c1cy, c1cz;
  double  costh, cosbs;
  double  xx, yy, zz;

  xx = p2->xx - p1->xx;
  yy = p2->yy - p1->yy;
  zz = p2->zz - p1->zz;
  dist12 = sqrt(xx*xx + yy*yy + zz*zz);
  
  ndet1  = p1->nd;
  ndet2  = p2->nd;
  nbshit = 0;
  c12x   = (p2->xx - p1->xx)/dist12;
  c12y   = (p2->yy - p1->yy)/dist12;
  c12z   = (p2->zz - p1->zz)/dist12;
  for(ii = 0; ii < ngedets; ii++) {
    if(ii != ndet1 && ii != ndet2) {
      xx   = CDet[ii].xx - p1->xx;
      yy   = CDet[ii].yy - p1->yy;
      zz   = CDet[ii].zz - p1->zz;
      dbs2 = xx*xx + yy*yy + zz*zz;
      dbs   = sqrt(dbs2);
      if((dbs - r_bsphere) > dist12) continue;
      c1cx  = (CDet[ii].xx - p1->xx) / dbs;
      c1cy  = (CDet[ii].yy - p1->yy) / dbs;
      c1cz  = (CDet[ii].zz - p1->zz) / dbs;
      costh = c12x*c1cx + c12y*c1cy + c12z*c1cz;
      if(costh < 0.0) continue;
      cosbs = sqrt(dbs2 - r_bsphere2) / dbs;
      if(costh <= cosbs) continue;
      ibshit[nbshit++] = ii;
    }
  }
}

int
insidegedet_cbar(point *pp)
{
  int     num_det;
  double  xr, yr, zr;

  num_det = pp->nd;

  zr = pp->zz - CDet[num_det].zz;
  if(fabs(zr) > h_germ) {
    return 0;
  }

  xr = pp->xx - CDet[num_det].xx;
  yr = pp->yy - CDet[num_det].yy;
  return (xr*xr + yr*yr <= r_germ2) ? 1 : 0;

}

void
findsegment_cbar(point *pp)
{
  int    nd, nz, np, nr, ns;
  double px, py, pz, ph;

  nd = pp->nd;
  px = pp->xx - CDet[nd].xx;
  py = pp->yy - CDet[nd].yy;
  pz = pp->zz - CDet[nd].zz;

  nz = (int)((pz + h_germ) / ssegsZ);
  ns = nz;

  ph = atan2(py, px) + 0.5 * ssegsP;    // first segment starts at -30deg and extends 60deg ccw
  if(ph < 0.) ph += 2 * M_PI;           // to produce 0...360 instead of -180 ... 180
  np =  (int)(ph / ssegsP);             // 60deg slices
  ns = np + nsegsP * ns;

  nr = 0;
  if(nsegsR > 1) {
    nr = (int)(sqrt(px*px + py*py) / ssegsR);
    ns = nr + nsegsR * ns;
  }

#if STATINSIDESEG == 1
  specIncr(nd       , 19, 0);
  specIncr(np + 1000, 19, 0);
  specIncr(nz + 1500, 19, 0);
  specIncr(nr + 2000, 19, 0);
#endif

  pp->ns  = ns;
  pp->seg = ns + nsegdet * pp->nd;
}

void
setsegcenter_cbar(point *pp)
{
  static int initit = 1;
  int     nz, np, nr, ns;
  double  pz, ph, pr, cph, sph;
  coors  *cge;
  coors  *csg;
  
  if(initit) {
    CSeg = (coors *)calloc(nsegtot, sizeof(coors));
    for(np = 0; np < nsegsP; np++) {
      ph  = np * ssegsP - 0.5 * ssegsP;
      cph = cos(ph);
      sph = sin(ph);
      for(nr = 0; nr < nsegsR; nr++) {
        pr = (nsegsR > 1) ? (nr * ssegsR + 0.5 * ssegsR) : (pr = r_germ / M_SQRT2);
        for(nz = 0; nz < nsegsZ; nz++) {
          pz = nz * ssegsZ + 0.5 * ssegsZ - h_germ;
          ns = nr + nsegsR*(np + nsegsP*nz);
          CSeg[ns].xx = pr * cph;
          CSeg[ns].yy = pr * sph;
          CSeg[ns].zz = pz;
        }
      }
      initit = 0;
    }
  }

  cge = CDet + pp->nd;
  csg = CSeg + pp->ns;
  pp->xx = csg->xx + cge->xx;
  pp->yy = csg->yy + cge->yy;
  pp->zz = csg->zz + cge->zz;
  setcosdir(pp);
  return;
}

// distance to exit cylindrical detector p1->nd along p1-p2 line
double
detdist1_cbar(coors *p1, coors *p2)
{
  int     nn;
  double  pr1x, pr1y, pr1z;
  double  pr2x, pr2y, pr2z;
  double  d12x, d12y, d12z;
  double  hx, hy;
  double  thit, rr;
  double  a, b, c, root, dist;

  nn   = p1->nd;

  pr1x = p1->xx - CDet[nn].xx;
  pr1y = p1->yy - CDet[nn].yy;
  pr1z = p1->zz - CDet[nn].zz;

  pr2x = p2->xx - CDet[nn].xx;
  pr2y = p2->yy - CDet[nn].yy;
  pr2z = p2->zz - CDet[nn].zz;

  d12x  = (pr2x - pr1x);
  d12y  = (pr2y - pr1y);
  d12z  = (pr2z - pr1z);

  if(d12z != 0.) {
// prova la faccia superiore
    thit = (h_germ - pr1z) / d12z;
    if(thit > 0.0 && thit <= 1.0) {
      hx = pr1x + d12x * thit;
      hy = pr1y + d12y * thit;
      rr = hx*hx + hy*hy;
      if(rr <= r_germ2) {
        dist = thit * dist12;
        return dist;
      }
    }
// prova la faccia inferiore
    thit = (-h_germ - pr1z) / d12z;
    if(thit > 0.0 && thit <= 1.0) {
      hx = pr1x + d12x * thit;
      hy = pr1y + d12y * thit;
      rr = hx*hx + hy*hy;
      if(rr <= r_germ2) {
        dist = thit * dist12;
        return dist;
      }
    }
  }

//prova ora con la superficie cilindrica
  hx = pr2x - pr1x;
  hy = pr2y - pr1y;
  a    = hx*hx + hy*hy;
  rr   = pr1x*pr1x + pr1y*pr1y;
  b    = pr1x*pr2x + pr1y*pr2y - rr;
  c    = rr - r_germ2;
  root = b * b - a * c;
  if(root > 0.) {
    root = sqrt(root);
    thit  = (-b + root)/a;
    if(thit > 0. && thit <= 1.) {
      dist = pr1z + d12z * thit;
      if(fabs(dist) <= h_germ) {
        dist = thit * dist12;
        return dist;
      }
    }
    thit  = (-b + root)/a;
    if(thit > 0. && thit <= 1.) {
      dist = pr1z + d12z * thit;
      if(fabs(dist) <= h_germ) {
        dist = thit * dist12;
        return dist;
      }
    }
  }
  return 0.;
}

double 
detdist2_cbar(coors *p1, coors *p2, int nn)   // intercept of p1-p2 straight line to cylindrical detector nn
{
  int     ns;
  double  pr1x, pr1y, pr1z;
  double  pr2x, pr2y, pr2z;
  double  d12x, d12y, d12z;
  double  thit, rr;
  double  hx, hy;
  double  a, b, c, root, dist;
  double  tt[4];

  pr1x = p1->xx - CDet[nn].xx;
  pr1y = p1->yy - CDet[nn].yy;
  pr1z = p1->zz - CDet[nn].zz;

  pr2x = p2->xx - CDet[nn].xx;
  pr2y = p2->yy - CDet[nn].yy;
  pr2z = p2->zz - CDet[nn].zz;

  d12x  = (pr2x - pr1x);
  d12y  = (pr2y - pr1y);
  d12z  = (pr2z - pr1z);

  ns = 0;
  if(d12z != 0.) {
// prova la faccia superiore
    thit = (h_germ - pr1z) / d12z;
    if(thit > 0.0 && thit <= 1.0) {
      hx = pr1x + d12x * thit;
      hy = pr1y + d12y * thit;
      rr = hx*hx + hy*hy;
      if(rr <= r_germ2) {
        tt[ns++] = thit;
      }
    }
// prova la faccia inferiore
    thit = (-h_germ - pr1z) / d12z;
    if(thit > 0.0 && thit <= 1.0) {
      hx = pr1x + d12x * thit;
      hy = pr1y + d12y * thit;
      rr = hx*hx + hy*hy;
      if(rr <= r_germ2) {
        tt[ns++] = thit;
      }
    }
  }

  if(ns < 2) {
    //prova ora con la superficie cilindrica
    hx = pr2x - pr1x;
    hy = pr2y - pr1y;
    a    = hx*hx + hy*hy;
    rr   = pr1x*pr1x + pr1y*pr1y;
    b    = pr1x * pr2x + pr1y * pr2y - rr;
    c    = rr - r_germ2;
    root = b * b - a * c;
    if(root > 0.) {
      root = sqrt(root);
      thit  = (-b + root)/a;
      if(thit > 0. && thit <= 1.) {
        dist = pr1z + d12z * thit;
        if(fabs(dist) <= h_germ) {
          tt[ns++] = thit;
        }
      }
      if(ns < 2) {
        thit  = (-b + root)/a;
        if(thit > 0. && thit <= 1.) {
          dist = pr1z + d12z * thit;
          if(fabs(dist) <= h_germ) {
            tt[ns++] = thit;
          }
        }
      }
    }
  }

  if(ns != 2) {
    return 0.;
  }
  dist = fabs(tt[1] - tt[2]) * dist12;
  return dist;
}

double
gedistance_cbar(point *p1, point *p2)
{
  coors     pev1, pev2;
  int       ndet1, ndet2, ii;
  double    dist, totd;
  double xx, yy, zz;

  xx = p2->xx - p1->xx;
  yy = p2->yy - p1->yy;
  zz = p2->zz - p1->zz;
  dist12 = sqrt(xx*xx + yy*yy + zz*zz);

  ndet1  = p1->nd;
  ndet2  = p2->nd;
  if(ndet1 == ndet2 && ndet1 >= 0) return dist12;

  pev1.xx = p1->xx;
  pev1.yy = p1->yy;
  pev1.zz = p1->zz;
  pev1.nd = p1->nd;

  pev2.xx = p2->xx;
  pev2.yy = p2->yy;
  pev2.zz = p2->zz;
  pev2.nd = p2->nd;

  totd = 0;

  if(ndet1 >= 0) totd += detdist1_cbar(&pev1, &pev2);
  if(ndet2 >= 0) totd += detdist1_cbar(&pev2, &pev1);

  setbshits_cbar(&pev1, &pev2);
  
  if(nbshit) {
    for(ii = 0; ii < nbshit; ii++) {
      dist  = detdist2_cbar(&pev1, &pev2, ibshit[ii]);
      totd += dist;
    }
  }
  return totd;
}

