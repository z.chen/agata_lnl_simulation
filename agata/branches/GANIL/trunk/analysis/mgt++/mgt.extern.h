#ifndef __MGT_HEADER__
#define __MGT_HEADER__ 

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>

#include "useful.h"
#include "speclib.h"
#include "matlib.h"

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TGraph.h>
#include <TRandom.h>
using namespace std;


#define MAXEVLEN   1000             // Max size of event
#define MAXMULT     160             // Max gamma-ray multiplicity
#define MAXEKEV    (32*1024)        // maximum energy of everything (in keV)
#define MINEGAM       1             // minimum accepted energy of simulated gammas
#define MAXEGAM    (MAXEKEV-1)      // maximum accepted energy of simulated gammas
#define MAXPOSERR     20.           // Worst position resolution (mm)
#define MINPOSERR      2.           // Best  position resolution (mm)
#define SPECLEN     (8*1024)        // #channels in a spectrum
#define MATSIZE        1024         // size of matrix matr

#define PEAKWIDTH   3.0             // factor to width of peak regions for look-up table

#define MAXCLUST   2000             // Max number of clusters in the pool (and accepted)
#define MAXPOINTS    10             // Max number of point in a cluster to be tracked
#define MAXPERM       7             // Max number of points in a cluster for Compton tracking
#define MAXACCEPT     6             // Max number of points in accepted clusters to increment spec#30

#define FMARS   1                   // data file format MARS
#define FAGATA  2                   // data file format AGATA

// Different ways of generating position errors
#define CUBE    1                   // points position smeared uniformly in a cube
#define SPHE    2                   // points position smeared uniformly in a sphere
#define GAUSS1  3                   // positions smeared according to an independent gaussians on the 3 axis
#define GAUSS2  4                   // positions smeared according to a  gaussian distribution of distances
#define GAUSS3  5                   // positions smeared according to an uniform distribution of distances
#define GAUSS4  6                   // positions smeared according to a given functional distribution of distances
#define SMEARPOS GAUSS1             // CUBE, SPHE or GAUSSx: select which one you want to use

#define FIXEDSMEARDIST  0           // 1 to use energy independent smearing distance
#define ORDERPOINTS     0           // 1 to change order of points (check not to depend on MC order)

// Implemented geometries
#define NONVALID  -1                // 
#define NONE       0                // None
#define SHELL      1                // spherical shell
#define CBAR       2                // barrel with cylindric crystals
#define HBAR       3                // barrel with hexagonal crystals
#define BOX3       4                // barrel with 3D box
#define MXXX       5                // ball with variable number of pent. and irregular hex. (still incomplete)
#define AGATA      5                // still considered as a shell !!!!!!!!!!!!!!!

// Which metric to use for clusterization
#define DISTANGULAR   1             // using angular distance to clusterize points
#define DISTEUCLIDEAN 2             // using 3D euclidean distance di clusterize points

// Select formulation of vertex style
#define VCHISQUARE  1               // vertex calculated using chi^2 style
#define VLIKELIHOOD 2               // vertec calculated using likelihood

// Interaction mechanism of tracked clusters
#define None    0                   // Not accepted
#define Photo   1                   // Isolated point accepted as photoelectric
#define Compt   2                   // Standard tracking as sequence of Compton scattering 
#define Pair0   3                   // Pair production with no escape (still to be completed)
#define Pair1   4                   // Pair production with one escape  (not yet implemented)
#define Pair2   5                   // Pair production with two escapes (not yet implemented)

// Flags to qualify clusters
#define ToDo    1
#define Good    2
#define Bad     3
#define Removed 4

#define STATINSIDESEG  0            // 1 to produce statistics of distribution of points inside segments
#define STATINSIDEDET  0            // 1 to produce statistics of position of points inside detectors (in data file)

#define SEGBAD         0            // 1 to disable parts of the segments
//#define SEGPACK        0            // 1 to pack all points inside a segment
#define SEGPACK        1            // 1 to pack all points inside a segment (as oft ?)
#define SEGCENTER      0            // 1 to move the SEGPACKED points to the center of the segments
#define ESMEARSEG      0            // 1 to smear energy taking into account multiple hits in a segment

#define EVALL        0
#define EVPEAK       1
#define EVBACK       2
#define SELECTEVENTS EVALL            // choose which alternative

////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////// some useful macros ///////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

#define DC2TH(p1)  (RAD2DEG*acos(p1->cz))             // theta from direction cosines
#define DC2PH(p1)  (RAD2DEG*atan2(p1->cy,p1->cx))     // phi   from direction cosines

////////////////////////////////////////////////////////////////////////////////////////
///////////////// struct to describe a 3D point in its detector ////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
  double xx;
  double yy;
  double zz;
  int    nd;
  int    ns;
} coors;

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////// struct for interaction points ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
  double  ee;         // energy of point (in keV)
  double  xx;         // coordinate x  (in mm)            relative to A (0,0,0)
  double  yy;         // coordinate y  (in mm)
  double  zz;         // coordinate z  (in mm)
  double  rr;         // distance from origin of coordinate system (in mm)
  double  cx;         // direction cosine-x RESPECT TO ORIGIN
  double  cy;         // direction cosine-y
  double  cz;         // direction cosine-z
  double  s2;         // sigma^2 of position
  int     ng;         // gamma to which the point belong
  int     nd;         // its detector (particle type if <0)
  int     ss;         // segment coding in data file
  int     ns;         // segment of the point
  int     seg;        // global index of segment
  int     ind;        // index of point in string representing clusters
  int     clu;        // index of accepted-cluster for point (-1 if not yet assigned)
  char    chid[64];   // identification string: e.g. Cab ==> third gamma (C), first 2 points packed (ab)
} point;

////////////////////////////////////////////////////////////////////////////////////////
////////////// struttura che descrive il rinculo della sorgente gamma //////////////////
////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
  double  cx;         // coseno direttore lungo l'asse  x della direzione di rinculo
  double  cy;         // coseno direttore lungo l'asse  y
  double  cz;         // coseno direttore lungo l'asse  z
  double  beta;       // v/c
  double  gamma;      // 1/sqrt(1-beta**2)
} pvect;

////////////////////////////////////////////////////////////////////////////////////////
//////// la struttura che descrive i cluster da processare e quelli accettati //////////
////////////////////////////////////////////////////////////////////////////////////////

typedef struct {
  int     status;                   // ToDo, Good, Bad, Removed
  int     mechanism;                // None, Photo, Compt, Pair0, Pair1, Pair2
  int     npoints;                  // Da quanti punti e' composto
  int     numgam;                   // Numero di gamma a cui i punti appartengono
  int     fullener;                 // if numgam == 1 ==> bit1=AllEner, bit2=AllEner&FullEner
  int     firstbit;                 // Primo  bit posto in cstring
  int     lastbit;                  // Ultimo         "
  int     tag;                      // Tag a come e' stato originato
  int     childof;                  // cluster created splitting clusterspool[childof]
  int     shared;                   // Se ha punti in comune con altri clusters
  double  eclust;                   // Energia del cluster (somma delle energie dei suoi punti)
  double  egamma;                   // Energia del cluster dopo correzione doppler
  double  chibest;                  // Figura di merito del cluster
  double  size_th;                  // Apertura angolare del cluster
  double  size_dd;                  // Apertura spaziale del cluster
  double  dist_th;                  // Distanza angolare del punto piu' vicino al clusters
  double  dist_dd;                  // Distanza spaziale del punto piu' vicino al clusters)
  char    cstring[MAXEVLEN  + 2];   // La stringa di caratteri "1" e "0" che lo rappresenta
  char    ordbest[MAXPOINTS + 2];   // Dopo il tracking, ordine dei punti
  point  *ppc[MAXPOINTS];           // Dopo il tracking, puntatori (ordinati) ai punti del cluster
} clust;


 /**********************/
 /* data for root tree */
 /**********************/
// Define ntu[le/tree data structure
	typedef struct {
		int event;
		float TBeta;
		float TX1;
		float TY1;
		float TZ1;
		float TELabDopTracked;
		int TMult;
	} TreeDATA;

	extern TreeDATA mgttree;


////////////////////////////////////////////////////////////////////////////////////////
////////////////////// global function ptototypes //////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

extern int     main           (int, char **);          // contains the event loop
extern void    errexit        (const char *);                // all error conditions captured here
extern void    initevent      ();                      // called at start to initialize event specific variables
extern void    initclust      ();                      // called at start to initialize cluster specific variables
extern void    inittrack      ();                      // called at end
extern void    finitevent     ();                      // called at end
extern void    finitclust     ();                      // called at end
extern void    finittrack     ();                      // called at start to initialize tracking specific variables
extern void    nextevent      ();                      // read and preprocess data for next gamma cascade
extern void    writeheader    ();                      // riporta lo header da input a preprocessed output
extern void    writeparams    ();                      // su ofile scrive i parametri di preprocessamento
//extern void    writeevent     ();                    // write preprocessed points to "GEANT" data file; before using g++ compiler for ROOT
extern void    writeevent     (point **, int);         // write preprocessed points to "GEANT" data file; when using g++ compiler
extern point  *getptogam      (int);                   // get pointer to original gamma
extern void    setcosdir      (point *);               // set direction cosines from cartesian coordinates
extern double  costhrec       (point *);               // Lab angle respect to the recoil direction
extern double  gaussrand      ();                      // random number with gauss distribution
extern double  gaussres       (double);                // modify energy according to energy resolution
extern void    orderpoints    (point **, int);         // orders a series of points according to their ...
extern void    printchid      (char  * );              // to print the label (eg Babc) of points
extern void    distmatmake    (point **, int);         // create distance (angular, linear, ge) for preprocessed points
extern void    distmatprint   (int);                   // print distance matrix
extern void    reconstruct    ();                      // high level function to control reconstruction of prepr.points
extern void    statVtx        (point **, int);         // statistics of Vtx deviations
extern int     checkEgam      (int ig, double ee);     // bit1=1 if ee=oener; bit2=1 if ee = oegam
extern void    poolGood       ();                      // clusterizzazione di tutti i gamma originali
extern void    poolLinkedS    (int metric);            // clusterizzazione sequenziale link
extern void    poolLeaderS    (int metric);            // clusterizzazione sequenziale leader
extern void    poolLinkedC    (int metric);            // clusterizzazione consecutiva linked
extern void    poolLeaderC    (int metric);            // clusterizzazione consecutiva leaded
extern void    trackCluster   (clust *);               // prepare tracking of a cluster for Compton, Pair0 and Photo
extern void    projections    ();                      // spettri singoli dei punti e dei rivelatori
extern double  dist3Dp        (point *, point *);

extern void    beginOfRun( int number );
extern void    endOfRun();
extern void    initGaspBuffer();
extern void    initAncillary();
extern void    readAncAngles();
//int     getNumberOfAncillary();  // before using g++ compiler for ROOT
extern int     getNumberOfAncillary(int); // To use g++ compiler
extern int     lengthOfAncillary( int, int );
//unsigned short int* getAncillaryEvent(); // before using g++ compiler for ROOT
extern unsigned short int* getAncillaryEvent(int); // To use g++ compiler
extern void    eventHeader();
extern void    addGammaToEvent( clust *pcl );
extern void    addAncillaryToEvent();
extern void    sendEventToBuffer();
extern void    resetAncBuffer();
extern void    addToAncBuffer( int ndet, char *);
extern void    writeAncillaryEvent();

extern void    geominit_none  (int);
extern void    geominit_shell ();
extern void    geominit_cbar  ();
extern void    geominit_hbar  ();
extern void    geominit_box3  ();
extern void    geominit_mxxx  ();
extern void    geominit_agata ();

////////////////////////////////////////////////////////////////////////////////////////
// the various geometries must implement their specific version of the next functions //
////////////////////////////////////////////////////////////////////////////////////////

extern int    (*insidegedet)  (point *);               // check if point is inside detector
extern void   (*findsegment)  (point *);               // find in which segment the point is
extern void   (*setsegcenter) (point *);               // moves the point to the center of its segment
extern double (*gedistance)   (point *, point *);      // germanium-distance between 2 generic points

////////////////////////////////////////////////////////////////////////////////////////
///// functions related to distribution of points in detectors and segments ////////////
////////////////////////////////////////////////////////////////////////////////////////

extern void    statinsidedet  (point *);               // statistics of point positions in GEANT data file
extern void    writesegsdata  (point **, int);         // LM output of segments energies

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// Variabili  globali /////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

extern char  *ifname;                // file dati prodotto con GEANT
extern FILE  *ifp;                   // descriptor of input  file
extern char  *ofname;                // file con i punti preprocessati scritto da writeevent
extern FILE  *ofp;                   // descriptor of file to write preprocessed data

extern int    dataFileVersion;       // FMARS, FAGATA
extern int    doWriteEvs;            // enables writing preprocessed events to ofname
extern int    outputType;            // how much to write to the preprocessed data file
extern char   outputmask[9];         // description of fields present in data-file

extern int    GEOMETRY;              // geometry
extern int    ngedets;               // number of germanium detectors
extern int    nsegdet;               // numero di segmenti in un rivelatore
extern int    nsegtot;               // numero totale di segmenti (ngedets*nsegdet)

extern point  origin;                // the "source" of gammas against which events are tracked
extern pvect  recoil;                // info about source recoil               

extern point  oorigin;               // in case of smearpos original value are saved here
extern pvect  orecoil;               //  ""                

extern double enerfactor;
extern double distfactor;

extern int    doSmearSource;
extern double ssDx, ssDy, ssDz;      // smearing source position
extern double ssDth, ssDv;           // smearing source direction and velocity

extern double sposX, sposY, sposZ;   // from command line to override SOURCE satement in datafile
extern int    posForced;             // used to override the values from file header
extern double recVc, recTh, recPh;
extern int    recForced;             // used to override the values from file header

extern coors *CDet;                  // center of germanium crystals
extern coors *CSeg;                  // center of segments
extern int   *segbad;                // the disabled segments 
extern int    nbadseg;               // numero di segmenti da disabilitare

extern int    nevents;               // quanti eventi verranno analizzati
extern int    mult;                  // molteplicita' della cascata gamma
extern int    evgen;                 // evento definito da GENERATOR, auto-molteplicita'
extern int    kdetMin;               // numero minimo di rivelatori nell'evento
extern int    npevMin;
extern int    npevMax;
extern int    nsegMin;
extern int    nsegMax;
extern int    llist;                 // scritte estese sul terminale se != 0
extern double chi2Accept;            // max ChiSquare per accettare un cluster
extern double chi2Track;             // ChiSquare scaled by CHI2FUDGE used to temporarily keep clusters
extern double cluAngMin;             // angolo di clusterizzazione minimo nella procedura ricorsiva
extern double cluAngMax;             // angolo di clusterizzazione in gradi (defaults to varang)
extern double cluDistMin;            // distanza minima  di clusterizzazione in mm
extern double cluDistMax;            // distanza massima di clusterizzazione in mm
extern double distmin;               // distanza di impaccamento dei punti (packing)
extern double disterr;               // errore sulla posizione             (smearing)
extern double eminP;                 // soglia energetica (in keV) sui punti (dopo impaccamento)
extern double eminD;                 // soglia energetica (in keV) sui rivelatori (dopo impaccamento)
extern double dEgain;                // fattore di guadagno per spettri di energia
extern double fwhm;                  // risoluzione energetica a 1333 keV
extern double fwhn;                  // componente di rumore della risoluzione energetica
extern double xx_min, xx_max;
extern double yy_min, yy_max;
extern double zz_min, zz_max;
extern int    check_xyz;
extern double minegam;
extern double maxegam;
extern int    readsegs;              // data file contain segment info
extern char   segmentstring[100];    // description of segmentation set by the geominit_xxx routines
extern char   cvertexstring[100];    // description of compton scattering function

extern int    varang;        // flag per angolo di clusterizzazione variabile (settata con -cluAngMax nella riga di comando)
extern int    varmult;       // flag per molteplicita' variabile              (settata con -mult      nella riga di comando)
extern int    cluGood;       // flag per clusterizzare tutti gli originali
extern double sigmult;       // sigma della distribuzione di molteplicita' se varmult!=0
extern int    vertexFunct;   // tipo di funzione per vertice compton
extern int    vertexStyle;   // VCHISQUARE o VLIKELIHOOD

extern int    evnum;         // numero evento
extern int    evnumMC;       // numero evento (gamma) nel file dati AGATA
extern int    evmult;        // sua molteplicita'
extern int    fixmult1;      // per il caso che si voglia imporre F=1
extern int    doTracking;    // to do/skip reconstruct()
extern int    evskip1;       // per debug, i primi evskip1 eventi non sono da trattare
extern int    evskip2;       // per debug, i primi evskip2 eventi non sono da ricostruire
extern int    gamnum;        // quale gamma 
extern int    gamtot;        // numero totale di gamma trattati 
extern int    evrejected;    // numero di eventi sotto soglia per numero di rivelatori colpiti
extern int    ndataread;     // numero di punti letti da readgamma (escludendo e<0)
extern int    npacked;       // numero di punti totali dopo il preprocessamento
extern int    nbadpoints;    // numero di punti scartati per BADSEG
extern int    nmixed;        // numero di punti preprocessati mescolati da piu' gamma
extern int    noutside1;     // numero di punti originali trovati fuori dei rivelatori in readgamma
extern int    noutside2;     // numero di punti impaccati trovati fuori dei rivelatori in smearpos

extern double thmult[MAXMULT + 1];               // angolo di clusterizzazione dipendente dalla molteplicita'

extern point *ppev[MAXEVLEN];                    // puntatori ai punti rimasti dopo impaccamento e soglia energetica
extern int    npev;                              // numero di punti dopo il pre-processamento

extern int    nobufmax;                          // numero massimo di punti nei gamma originali
extern int    pgamlenmax;                        // numero massimo di punti nei gamma preprocessati
extern int    npevmax;                           // numero massimo di punti preprocessati durante l'analisi

extern int    datmult;                           // quanti gamma distinti nel file dati
extern double dategam[MAXMULT];                  // le energie dei gamma della cascata

extern double peakwidth[MAXEKEV];                // larghezza dei picchi gamma
extern int    peaklut[MAXEKEV];                  // lookup-table delle zone dei picchi

extern char   mechanism[10][10];

extern int    checkPh;                           // to allow Photoelectric   mechanism in trackCluster
extern int    checkPP;                           // to allow Pair Production mechanism in trackCluster 

extern double *distmat[MAXEVLEN];                // actual distance matrix selected by metric
extern double  cluDMin, cluDMax;                 // actual clusterization limits selected by metric
extern double  distmat_dd[MAXEVLEN][MAXEVLEN];   // matrice delle distanze spaziali
extern double  distmat_th[MAXEVLEN][MAXEVLEN];   // matrice delle distanze angolari
extern double  distmat_ge[MAXEVLEN][MAXEVLEN];   // matrice delle distanze in germanio
//extern double  distmat_xx[MAXEVLEN][MAXEVLEN];   // matrice delle distanze probabilistiche

extern char   matclust[MAXEVLEN][MAXEVLEN + 1];  // matclust e' la matrice di clusterizzazione
extern int    matnbits[MAXEVLEN];                // numero di "1" nella riga di matclust
extern double matcdist[MAXEVLEN];
extern int    matnrows;                          // quanti elementi contiene

extern clust  clusterspool[MAXCLUST];            // clusters da processare
extern int    nclupool;                          // quanti
extern int    nclupoolmax;                       // massimo valore di nclupool durante tutta l'analisi

extern clust  clustersdone[MAXCLUST];            // clusters accettati
extern int    ncludone;                          // numero di clusters accettati
extern int    ncludonemax;                       // massimo valore di ncludone durante tutta l'analisi
extern int    clugood;                           // cluster ricostruito sul picco
extern int    nclugood;                          // numero di cluster ricostruiti correttamente (picco)
extern int    nclugoodmax;                       // massimo valore di nclugood durante tutta l'analisi
extern int    npcludone;                         // numero di punti nei clusters accettati
extern int    nclumixed;

extern int    ncombined;
extern int    ncombgood;
extern int    ncombbad;
extern int    ncombnew;

////////////////////////////////////////////////////////
extern int    runNumber;                         // run number when writing out data in GASP format
extern int    doSort;                            // flag to enable/disable output in GASP format
extern char  *afname;                            // file containing the angles of the ancillary detectors
extern FILE  *afp;                               // pointer to the file

extern int     numAncil;                         // number of different kind of ancillaries
extern int    *ancOffset;                        // offsets
extern int    *ancNParam;                        // parameters
extern int    *ancNDetec;                        // number of elements composing the ancillary
extern int    *ancNSegme;                        // number of segments for each element
extern int     ancSize;
extern int     maxOffset;
extern int     minOffset;

extern double  ancBufGain;                       // gain for the ancillary detectors 
extern double *ancBuffer;                        // buffer to store the energies
extern int     ancBufLen;

extern int doAncillary;
extern int doAncAngles;
extern int doAncPixels;

extern int doPrismaEvs;

#ifdef TAPERED
extern int onlyAncil;
#endif

extern TFile *rootfile;
extern TTree *output_tree; 

#endif
