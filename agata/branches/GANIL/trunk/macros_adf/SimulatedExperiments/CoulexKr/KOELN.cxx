/*
  This codes creates data:ranc1 frames with Legnaro data format, i.e
  one UInt_t telling how many Float_t that follows

  compile with
  
  g++ -shared -o libKOELN.so KOELN.cxx
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <string>

extern "C"  {
  void KOELN(std::ostringstream &buff);
}


void KOELN(std::ostringstream &buff)

{
  static std::map<int,float> LUT_theta;
  static std::map<int,float> LUT_phi;
  if(LUT_theta.size()==0){
    std::ifstream input("NUMBERING.S2");
    int ring,sec;
    float theta,phi;
    while(input.good()){
      input >> sec >> ring >> theta >> phi;
      LUT_theta[ring]=theta;
      LUT_phi[sec]=phi;
    }
  }
  //I will sum up everything in the same segments...
  //Get rid of x,y,z 
  std::map<int,float> E,T,theta,phi;
  std::istringstream abuffer(buff.str());
  std::istringstream aline;
  buff.str("");
  buff.clear();
  std::string oneline;
  double e,x,y,z,ns;
  int seg,det,day,hour,min;
  //First make some space in buffer
  unsigned int numberoffloats=0;
  buff.write((char*)&numberoffloats,sizeof(unsigned int));
  while(abuffer.good()){
    std::getline(abuffer,oneline);
    if(abuffer.good()){
      aline.clear();
      aline.str(oneline);
      aline >> x >> y >> z >> e >> det >> seg >> day >> hour >> min >> ns;
      E[seg]+=e;
      T[seg]+=ns*e;
      theta[seg]=LUT_theta.find(seg)!=LUT_theta.end() ? LUT_theta[seg] : -1.;
      phi[seg]=LUT_phi.find(det-1000)!=LUT_phi.end() ? LUT_phi[det-1000] : -1.;
    }
  }
  std::map<int,float>::iterator itE=E.begin();
  //Fill with data floats
  for(; itE!=E.end(); ++itE){
    if(itE->second>5){
      T[itE->first]/=(itE->second>0 ? itE->second : 1);
      buff.write((char*)&(itE->second),sizeof(float));numberoffloats++;
      buff.write((char*)&(T[itE->first]),sizeof(float));numberoffloats++;
      buff.write((char*)&(theta[itE->first]),sizeof(float));numberoffloats++;
      buff.write((char*)&(phi[itE->first]),sizeof(float));numberoffloats++;
    }
  }
  //Tell how many floats that were written
  buff.seekp(0);
  buff.write((char*)&numberoffloats,sizeof(unsigned int));
}
