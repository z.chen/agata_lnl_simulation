//Some simple root script to look at the "local spectra"
//produced by the AGATA DAQ actors

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TString.h"
#include "TROOT.h"



std::ifstream *pTracefile = 0;
int NbSmp = 0;
TGraph traceexp,tracefit;
char type_[80];

#ifndef __CINT__
template <typename T> 
class datapoint {
public:
  datapoint() {;}
  ~datapoint(){;}
  T GetValue(std::istream &fs);
};

template <typename T> T datapoint<T>::GetValue(std::istream &fs)
{
  T val;
  fs.read((char*)&val,sizeof(T));
  return val;
}

template Float_t datapoint<Float_t>::GetValue(std::istream &fs);
template Short_t datapoint<Short_t>::GetValue(std::istream &fs);
template UShort_t datapoint<UShort_t>::GetValue(std::istream &fs);
template Int_t datapoint<Int_t>::GetValue(std::istream &fs);
template UInt_t datapoint<UInt_t>::GetValue(std::istream &fs);
template Double_t datapoint<Double_t>::GetValue(std::istream &fs);

#endif


void LookAtSpectrum(TString filename, UInt_t which)

{
  ifstream infil(filename.Data());
  filename.ReplaceAll("_"," ");
  filename.ReplaceAll("-"," ");
  filename.ReplaceAll("."," ");
  std::istringstream todecode(filename.Data());
  //Get histoname
  TString histname;
  if(!(todecode >> histname)) {std::cout << "no name!!!\n";return;}
  std::vector<UInt_t> dim;
  //Get dimensions
  UInt_t gp = 0;
  do{
    UInt_t tmp;
    gp = todecode.tellg();
    todecode >> tmp;
    if(!todecode.fail()){
      dim.push_back(tmp);
    }
  }while(!todecode.fail());
  todecode.clear();
  todecode.seekg(gp);
  TString type;
  todecode >> type;
  if(todecode.fail()) {std::cout << "no type!!!\n";return;}
  todecode >> histname;
  if(todecode.fail()) {std::cout << "no name!!!\n";return;}
  if(gROOT->FindObject(histname)) gROOT->FindObject(histname)->Delete();
  TH1F *histo = new TH1F(histname,histname,dim[dim.size()-1],-0.5,
			 dim[dim.size()-1]-0.5);
  //Fill histogram
  UInt_t seektohisto = dim[dim.size()-1]*which;
  if(type=="I" || type=="UI") seektohisto*=4;
  if(type=="S" || type=="US") seektohisto*=2;
  infil.seekg(seektohisto);
  for(unsigned int i=1; i<=dim[dim.size()-1]; i++){
    if(type=="I" || type=="UI"){
      char b[4];
      infil.read(b,4);
      if(type=="I"){
	histo->SetBinContent(i,*(Int_t*)b);
      } else {
	histo->SetBinContent(i,*(UInt_t*)b);
      }
    } else {
      char b[2];
      infil.read(b,2);
      if(type=="I"){
	histo->SetBinContent(i,*(Short_t*)b);
      } else {
	histo->SetBinContent(i,*(UShort_t*)b);
      }
    }
  }
  histo->Draw();
}

void LookAtMatrix(TString filename)

{
  ifstream infil(filename.Data());
  filename.ReplaceAll("_"," ");
  filename.ReplaceAll("-"," ");
  filename.ReplaceAll("."," ");
  std::istringstream todecode(filename.Data());
  //Get histoname
  TString histname;
  if(!(todecode >> histname)) {std::cout << "no name!!!\n";return;}
  std::vector<UInt_t> dim;
  //Get dimensions
  UInt_t gp = 0;
  do{
    UInt_t tmp;
    gp = todecode.tellg();
    todecode >> tmp;
    if(!todecode.fail()){
      dim.push_back(tmp);
    }
  }while(!todecode.fail());
  todecode.clear();
  todecode.seekg(gp);
  TString type;
  todecode >> type;
  if(todecode.fail()) {std::cout << "no type!!!\n";return;}
  todecode >> histname;
  if(todecode.fail()) {std::cout << "no name!!!\n";return;}
  if(gROOT->FindObject(histname)) gROOT->FindObject(histname)->Delete();
  TH2D *histo=new TH2D(histname,histname,dim[dim.size()-2],-0.5,
		       dim[dim.size()-2]-0.5,dim[dim.size()-1],-0.5,
		       dim[dim.size()-1]-0.5);
  for(unsigned int i=1; i<=dim[dim.size()-2]; i++){
    for(unsigned int j=1; j<=dim[dim.size()-2]; j++){
      if(type=="I" || type=="UI"){
	char b[4];
	infil.read(b,4);
	if(type=="I"){
	  histo->SetBinContent(i,j,*(Int_t*)b);
	} else {
	  histo->SetBinContent(i,j,*(UInt_t*)b);
	}
      } else {
	char b[2];
	infil.read(b,2);
	if(type=="I"){
	  histo->SetBinContent(i,j,*(Short_t*)b);
	} else {
	  histo->SetBinContent(i,j,*(UShort_t*)b);
	}
      }
    }
  }
  histo->Draw("col");
}

void LookAtCube(TString filename, int which)
  
{
  ifstream infil(filename.Data());
  filename.ReplaceAll("_"," ");
  filename.ReplaceAll("-"," ");
  filename.ReplaceAll("."," ");
  std::istringstream todecode(filename.Data());
  //Get histoname
  TString histname;
  if(!(todecode >> histname)) {std::cout << "no name!!!\n";return;}
  std::vector<UInt_t> dim;
  //Get dimensions
  UInt_t gp = 0;
  do{
    UInt_t tmp;
    gp = todecode.tellg();
    todecode >> tmp;
    if(!todecode.fail()){
      dim.push_back(tmp);
    }
  }while(!todecode.fail());
  todecode.clear();
  todecode.seekg(gp);
  TString type;
  todecode >> type;
  if(todecode.fail()) {std::cout << "no type!!!\n";return;}
  todecode >> histname;
  if(todecode.fail()) {std::cout << "no name!!!\n";return;}
  if(gROOT->FindObject(histname)) gROOT->FindObject(histname)->Delete();
  TH3I *histo = new TH3I(histname,histname,
			 dim[dim.size()-3],-0.5,dim[dim.size()-3]-0.5,
			 dim[dim.size()-2],-0.5,dim[dim.size()-2]-0.5,
			 dim[dim.size()-1],-0.5,dim[dim.size()-1]-0.5);
  //Fill histogram
  UInt_t seektohisto = dim[dim.size()-1]*dim[dim.size()-2]*dim[dim.size()-3]*
    which;
  if(type=="I" || type=="UI") seektohisto*=4;
  if(type=="S" || type=="US") seektohisto*=2;
  infil.seekg(seektohisto);
  for(unsigned int i=1; i<=dim[dim.size()-3]; i++){
    for(unsigned int j=1; j<=dim[dim.size()-2]; j++){
      for(unsigned int k=1; k<=dim[dim.size()-1]; k++){
	if(type=="I" || type=="UI"){
	  char b[4];
	  infil.read(b,4);
	  if(type=="I"){
	    histo->SetBinContent(k,j,i,*(Int_t*)b);
	  } else {
	    histo->SetBinContent(k,j,i,*(UInt_t*)b);
	  }
	} else {
	  char b[2];
	  infil.read(b,2);
	  if(type=="I"){
	    histo->SetBinContent(k,j,i,*(Short_t*)b);
	  } else {
	    histo->SetBinContent(k,j,i,*(UShort_t*)b);
	  }
	}
      }
      histo->Draw();
    } 
  }
}

void LoadTraceFile(TString tracesfile)

{
  while(traceexp.GetN()>0) traceexp.RemovePoint(0);
  while(tracefit.GetN()>0) tracefit.RemovePoint(0);
  if(pTracefile!=0) {
    pTracefile->close();
    delete pTracefile;
    pTracefile = 0;
  }
  pTracefile = new ifstream(tracesfile.Data());
  int nbseg,nbsmp,nbtrace;
  char path[500];
  tracesfile.ReplaceAll("_"," ");
  if(tracesfile.Contains("Psa")){
    sscanf(tracesfile.Data(),
	   "Psa  %i-2-%i-%i-%s  Traces.samp",&nbtrace,&nbseg,&nbsmp,
	   type_);
  }
  if(tracesfile.Contains("Prep")){
    sscanf(tracesfile.Data(),
	   "Prep  %i-%i-%i-%s  Traces.samp",&nbtrace,&nbseg,&nbsmp,
	   type_);
  }
  std::cout << path << " " << nbtrace << " " << nbseg << " " << nbsmp
	    << " " << type_ << std::endl;
  NbSmp = nbseg*nbsmp;
}


#ifndef __CINT__
template < typename t> void FillExp(datapoint<t> &d)

{
  Double_t val;
  for(int i=0; i<NbSmp; i++){
    val = d.GetValue(*pTracefile);
    traceexp.SetPoint(i,i,val);
  }
}

template < typename t> void FillFit(datapoint<t> &d)

{
  Double_t val;
  for(int i=0; i<NbSmp; i++){
    val = d.GetValue(*pTracefile);
    tracefit.SetPoint(i,i,val);
  }
}
#endif

void PlotNextPair()

{
#ifndef __CINT__
  traceexp.SetLineColor(1);
  tracefit.SetLineColor(2);
  if(gPad) gPad->Clear();
  if(std::string(type_)=="F"){
    datapoint<Float_t> d;
    FillExp(d);
    FillFit(d);
  }
  if(std::string(type_)=="S"){
    datapoint<Short_t> d;
    FillExp(d);
    FillFit(d);
  }
  traceexp.Draw("al");
  tracefit.Draw("l");
#endif
}


void PlotNext()

{
#ifndef __CINT__
  traceexp.SetLineColor(1);
  if(gPad) gPad->Clear();
  if(std::string(type_)=="F"){
    datapoint<Float_t> d;
    FillExp(d);
  }
  if(std::string(type_)=="S"){
    datapoint<Short_t> d;
    FillExp(d);
  }
  traceexp.Draw("al");
#endif
}
