set style line 1 lt 1 lc rgb "red" lw 2 pt 1 ps 2 
set style line 2 lt 1 lc rgb "green" lw 2 pt 1 ps 2 
set style line 3 lt 1 lc rgb "blue" lw 2 pt 1 ps 2 
set key left
set yrange [2:27]
set xrange [0:21]
set title "Full Width at Half Maximum"
set xlabel "Recoil velocity [v/c*100]"
set ylabel "FWHM at 1MeV [keV]"
  plot "fwhm.txt" using ($1):($2*2.35)\
 title "EXOGAM" w lp ls 1 
replot "fwhm.txt" using ($1):($3*2.35)\
 title "1{/Symbol p} 23.5 cm" w lp ls 2
replot "fwhm.txt" using ($1):($4*2.35)\
 title "1{/Symbol p} 13.5 cm" w lp ls 3 
set term pdf enhanced color
set output "fwhmagataweek2013.pdf"
replot
set term x11 enhanced
replot
