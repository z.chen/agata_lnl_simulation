set xlabel "Gamma-ray energy [MeV]"
set ylabel "Attenuation Coeff {/Symbol m} [1/cm]"
set log y
set log x
set xrange [0.001:30]
set yrange [0.1:70000]
plot "xsphotons.txt" using ($1):($2*5.32) title \
"http://physics.nist.gov/PhysRefData/XrayMassCoef/ElemTab/z32.html {/Symbol m}"\
 w l lw 2 lc 0
#error from d(log(I/I0))*d(I/I0)=(I0/I*1/I0*dI/I0) = 1/sqrt(I)/I0 
replot "TransmittionAndEnergyLoss.dat"\
 using (0.001*$1):($2>0 ? -log($2/$3)/(0.001):1/0):(1/sqrt($2)/$3/(0.001)) title\
 "Geant4.9.6 attenuation coeff" w yerrorbars ps 3 pt 1 lw 3
replot "TransmittionAndEnergyLoss_g41007.dat"\
 using (0.001*$1):($2>0 ? -log($2/$3)/(0.001):1/0):(1/sqrt($2)/$3/(0.001)) title\
 "Geant4.10.07 attenuation coeff" w yerrorbars ps 2 pt 2 lw 2
#replot "TransmittionAndEnergyLoss_g492p04.dat"\
# using (0.001*$1):($2>0 ? -log($2/$3)/(0.001):1/0):(1/sqrt($2)/$3/(0.001)) title\
# "Geant4.9.2p04 attenuation coeff" w yerrorbars ps 4 pt 1 lw 1
