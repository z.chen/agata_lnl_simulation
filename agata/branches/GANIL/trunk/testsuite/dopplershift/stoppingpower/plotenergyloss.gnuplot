set key left outside reverse spacing 1 width -15 font "Times,9"
set xlabel "E/A [MeV]" 
set ylabel "dE/dx [MeV/mg/cm^{2}]" offset 2.5
set yrange[0.3:10000]
set xrange[0.0001:10000]
set log y
set log x
plot "energylossSNdg496.dat" using ($1/32/1000):($4/1000/0.007)\
 title "^{32}S in Nd, G41007" w p lc 1
replot "energylossSNdg496SN.dat" using ($1/32/1000):($4/1000/0.007)\
 title "^{32}S in Nd, G41007 screened nuclear" w p lc 1
replot "s32ndstoppingLise.txt" using 1:4\
 title "^{32}S in Nd Lise++ JF Ziegler" w l ls 1 lc 1
#
replot "energylossSAug496.dat" using ($1/32/1000):($4/1000/0.01931)\
 title "^{32}S in Au, G41007" w p lc 2
replot "energylossSAug496SN.dat" using ($1/32/1000):($4/1000/0.01931)\
 title "^{32}S in Au, G41007 screened nuclear" w p lc 2
replot "s32austoppingLise.txt" using 1:4\
 title "^{32}S in Au Lise++ JF Ziegler" w l ls 2 lc 2
#
replot "energylossSTag496.dat" using ($1/32/1000):($4/1000/0.0166)\
 title "^{32}S in Ta, G41007" w p lc 3
replot "energylossSTag496SN.dat" using ($1/32/1000):($4/1000/0.0166)\
 title "^{32}S in Ta, G41007 screened nuclear" w p lc 3
replot "s32tastoppingLise.txt" using 1:4\
 title "^{32}S in Ta Lise++ JF Ziegler" w l ls 3 lc 3
#
replot "energylossOsNdg496.dat" using ($1/170/1000):($4/1000/0.007)\
 title "^{170}Os in Nd, G41007" w p lc 4
replot "energylossOsNdg496SN.dat" using ($1/170/1000):($4/1000/0.007)\
 title "^{170}Os in Nd, G41007 screened nuclear" w p lc 4
replot "os170ndstoppingLise.txt" using 1:4\
 title "^{170}Os in Nd Lise++ JF Ziegler" w l ls 4 lc 4
#
replot "energylossOsAug496.dat" using ($1/170/1000):($4/1000/0.01931)\
 title "^{170}Os in Au, G41007" w p lc 5
replot "energylossOsAug496SN.dat" using ($1/170/1000):($4/1000/0.01931)\
 title "^{170}Os in Au, G41007 screened nuclear" w p lc 5
replot "os170austoppingLise.txt" using 1:4\
 title "^{170}Os in Au Lise++ JF Ziegler" w l ls 5 lc 5
#
replot "energylossOsTag496.dat" using ($1/170/1000):($4/1000/0.0166)\
 title "^{170}Os in Ta, G41007" w p lc 0
replot "energylossOsTag496SN.dat" using ($1/170/1000):($4/1000/0.0166)\
 title "^{170}Os in Ta, G41007 screened nuclear" w p lc 0
replot "os170tastoppingLise.txt" using 1:4\
 title "^{170}Os in Ta Lise++ JF Ziegler" w l ls 6 lc 0
#
set terminal postscript color enhanced 
set output "stoppingpowers.ps"
replot
set terminal x11
replot