{
  gROOT->ProcessLine(".L ./../../Analyse.cxx+");
  Analyse("GammaEvents.00[012][0-9] GammaEvents.003[0-5]","energylossSAug496.dat");
  Analyse("GammaEvents.01[012][0-9] GammaEvents.013[0-5]","energylossSNdg496.dat");
  Analyse("GammaEvents.02[012][0-9] GammaEvents.023[0-5]","energylossSTag496.dat");
  Analyse("GammaEvents.03[012][0-9] GammaEvents.033[0-5]","energylossOsAug496.dat");
  Analyse("GammaEvents.04[012][0-9] GammaEvents.043[0-5]","energylossOsNdg496.dat");
  Analyse("GammaEvents.05[012][0-9] GammaEvents.053[0-5]","energylossOsTag496.dat");
}
