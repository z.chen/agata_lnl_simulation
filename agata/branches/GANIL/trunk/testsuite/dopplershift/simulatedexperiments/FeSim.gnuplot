#micro/ps
set key right inside spacing 1.5
set fit errorvariables
set xlabel "Distance [{/Symbol m}m]"
set ylabel "Counts in peaks"
set format y "%5.f"
v = 33.5
filenames = "FeIntensties_5000_events.txt FeIntensties_50000_events.txt\
 FeIntensties_100000_events.txt FeIntensties_500000_events.txt\
 FeIntensties_1000000_events.txt"
i=1
do for [filename in filenames] { 
plot filename using ($1):($2+$4):(($3*$3+$5*$5)**.5)\
 title "Sum of 2^+" lc rgb "black" w yerrorbars
fN(x)=N
fit fN(x) filename using ($1):($2+$4):(($3*$3+$5*$5)**.5) via N
#replot fN(x)
replot filename using ($1):($4):($5) title "2^+{/Symbol \256}0^+ slow"\
 lc rgb "red" w yerrorbars						      
replot filename using ($1):($8):($9) title "4^+{/Symbol \256}2^+ slow"\
 lc rgb "blue"  w yerrorbars
l2=log(2)/5.1
l4=log(2)/8.8
RelEffN4=1./1.2
Feeding4=1.6/2.2
Feeding2=1./2.2
N20 = N
N2(x) = N*Feeding4*(l4*exp(l4*x/v)-l2*exp(l2*x/v))*exp(-(l4+l2)*x/v)/(l4-l2)+\
N*(1-Feeding4)*exp(-l2*x/v)
N4(x) = RelEffN4*N*Feeding4*exp(-l4*x/v)
Ntot(x) = (x<=700)?N2(x):N4(x-700)
catcommand = sprintf("< cat %s %s",filename,filename)
fit Ntot(x) catcommand using\
 ($1+($0<11?0:700)):($0<11?$4:$8):($0<11?$5:$9)\
 via l2,l4,Feeding4
#fit N2(x) filename using ($1):($4):($5) via l2
#(N*Feeding2*exp(-l2*x/v)+N*Feeding4*l4/(l4-l2)*(exp(-l2*x/v)-exp(-l4*x/v)))
replot N2(x) title "Fitted decay curve 2^{+}" lc rgb "dark-orange"
replot N4(x) title "Fitted decay curve 4^{+}" lc rgb "green"
set title\
 sprintf("Fitted %2.2f < T_{1/2} < %2.2f Error %2.2f \%",\
.69/(l2+l2_err),.69/(l2-l2_err),100*(.69/(l2-l2_err)-.69/(l2+l2_err))\
/((.69/(l2+l2_err)+.69/(l2-l2_err))))
replot
set terminal pdf enhanced font "Times,9" lw 2
set output sprintf("TauPrec%i.pdf",i)
replot
set terminal X11 enhanced
replot
i=i+1
}