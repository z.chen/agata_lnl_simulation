#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>

#include "TH1.h"
#include "TH2.h"
#include "TString.h"
#include "TFile.h"
#include "TRandom.h"
#include "TMath.h"

#define _NODEBUG_

Double_t getE(Double_t &e,bool addres)

{
  if(addres) return gRandom->Gaus(e,sqrt(1.3+1./150*e)/2.36);
  else return e;
}

void SortOrgamSim(TString outputfilename="orgamsort.root", bool addres=false)

{
  double v_c=0.0;
  bool reacheddata = false;
  TFile outputfile(outputfilename.Data(),"RECREATE");
  //  ifstream inputfile(inputfilename.Data());
  std::map<int,TH1F*> histomap,anticomptonhistomap;
  std::map<int,double> detenergies;
  std::map<int,double> dettheta;
  std::map<int,double> dettimes;
  std::map<double,TH1F*> rings,anticomptonrings;
  std::map<double,int> ringid;
  std::map<int,TH2F*> histo2dmap;
  TH2F et("et","et",1024,-0.005,10.235,1024,-1,2047);
  //Read header, make spectra ->
  //1D
  //1 per det no Anticompton
  //1 per det with Anticompton
  //1 per ring no Anticompton
  //1 per ring with Anticompton
  //2D
  //1 per ring combination with Anticompton
  char linebuffer[2048];
  istringstream aninputline(linebuffer);
  ostringstream histonames;
  string codewords;
  double e;int detnb;
  unsigned int evtnb=0;
  //  while(inputfile.good()){
  while(!std::cin.eof()){
    // inputfile.getline(linebuffer,2048);
    std::cin.getline(linebuffer,2048);
#ifndef _NODEBUG_
    std::cout << linebuffer << std::endl;
#endif
    aninputline.str(linebuffer);
    aninputline.clear();
    if(aninputline>>codewords){
      if(codewords=="ORGAM"){
	std::cout << codewords << std::endl;
	int nbdetorgam;
	aninputline >> codewords >> nbdetorgam;
	for(int i=0; i<nbdetorgam; i++){
	  // inputfile.getline(linebuffer,2048);
	  std::cin.getline(linebuffer,2048);
	  aninputline.str(linebuffer);
	  aninputline.clear();
	  double theta,phi;
	  aninputline >> detnb >> theta >> phi;
	  dettheta[detnb]=theta;
	  detenergies[detnb]=0;
	  dettimes[detnb]=0;
	  detenergies[detnb+300]=0;
	  dettimes[detnb+300]=0;
	  //Check if we got this detector or not
	  if(histomap[detnb]==0){
	    histonames.str("");
	    histonames << "OrgamDet_" << detnb;
	    histomap[detnb]=new TH1F(histonames.str().c_str(),
				     histonames.str().c_str(),
				     4096,-0.25,2047.75);
	    histonames.str("");
	    histonames << "OrgamDetComptonSuppressed_" << detnb;
	    anticomptonhistomap[detnb]=new TH1F(histonames.str().c_str(),
						histonames.str().c_str(),
						4096,-0.25,2047.75);
	    anticomptonhistomap[detnb]->SetLineColor(2);
	  }
	  if(rings[theta]==0){
	    histonames.str("");
	    histonames << "Ring_" << rings.size();
	    rings[theta]=new TH1F(histonames.str().c_str(),
				  histonames.str().c_str(),
				  4096,-0.25,2047.75);
	    histonames.str("");
	    histonames << "RingComptonSuppressed_" << rings.size();
	    anticomptonrings[theta]=new TH1F(histonames.str().c_str(),
					     histonames.str().c_str(),
					     4096,-0.25,2047.75);
	    anticomptonrings[theta]->SetLineColor(2);
	    ringid[theta]=rings.size()-1;
	  }
	}
	//Here we make the 2D histos
	std::map<double,int>::iterator itrid = ringid.begin();
	std::map<double,int>::iterator itrid2 = ringid.begin();
	for(;itrid!=ringid.end(); ++itrid){
	  itrid2 = ringid.begin();
	  for(;itrid2!=ringid.end(); ++itrid2){
	    int histoid = itrid->second*ringid.size()+itrid2->second;
	    histonames.str("");
	    histonames << "Ring_" << itrid->second << "_Ring_" 
		       << itrid2->second << "_ID_" << histoid;
	    histo2dmap[histoid] = new TH2F(histonames.str().c_str(),
					   histonames.str().c_str(),
					   4096,-0.25,2047.75,
					   4096,-0.25,2047.75);
	  }
	}
      }
      if(codewords=="-100"){//new event
	std::map<int,double>::iterator ite = detenergies.begin();
	for(;ite!=detenergies.end() && evtnb>0; ++ite){
	  //Fill histos
	  if(ite->second>0 && ite->first<300 /*we only use BGOs to supress*/) {
#ifndef _NODEBUG_
	    std::cout << "------------->  " << 
	      ite->first << " " << ite->second << std::endl;
#endif
	    double gamma = 1./sqrt(1-v_c*v_c);
	    Double_t e1=getE(ite->second,addres)*
	      (gamma*(1-v_c*cos(TMath::DegToRad()*dettheta[ite->first])));
	    histomap[ite->first]->Fill(e1);
	    rings[dettheta[ite->first]]->Fill(e1);
	    if(detenergies[ite->first+300]==0){//no gamma in sheild
	      anticomptonhistomap[ite->first]->Fill(e1);
	      anticomptonrings[dettheta[ite->first]]->Fill(e1);
	    }
	    et.Fill(dettimes[ite->first],e1);
	    std::map<int,double>::iterator ite2 = ite;
	    ++ite2;
	    for(;ite2!=detenergies.end(); ++ite2){
#ifndef _NODEBUG_
	      std::cout << "------------->  " << 
		ite2->first << " " << ite2->second << std::endl;
#endif
	      if((ite2->second>0 && ite2->first<300) /*we only use BGOs to supress*/
		 &&
		 (detenergies[ite->first+300]==0 && 
		  detenergies[ite2->first+300]==0)/*here o suppressed gammas*/) {
		int histoid = ringid[dettheta[ite->first]]*ringid.size()+
		  ringid[dettheta[ite2->first]];
		double e2 = getE(ite2->second,addres)*
		  (gamma*(1-v_c*cos(TMath::DegToRad()*dettheta[ite->first])));
		histo2dmap[histoid]->Fill(e1,e2);
	      }
	    }
	  }
	  ite->second=0;
	}
	evtnb++;
#ifdef _NODEBUG_
	if(evtnb%10000==0){
//	  std::cout << evtnb << "\r";
//	  std::cout.flush();
	}
#endif
      }
      detnb = atoi(codewords.c_str());
      if(detnb>=0){
	aninputline >> e;
	if(reacheddata)	detenergies[detnb]+=e;
#ifndef _NODEBUG_
	if(reacheddata) std::cout << "<--> " << detnb << " " << e << "\n";
#endif
	aninputline >> e >> e >> e >>e >> e;
	if(reacheddata) dettimes[detnb]=e;
      }
      if(codewords=="$") reacheddata = true;
    }
  }
  std::cout << std::endl;
  outputfile.Write();
}
