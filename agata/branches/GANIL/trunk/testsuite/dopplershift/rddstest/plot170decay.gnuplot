#to integrate
delta=1.62
integral_f2(x) = (x>0)?int1a2(x,x/ceil(x/delta)):-int1b2(x,-x/ceil(-x/delta))
int1a2(x,d) = (x<=d*.1) ? 0 : (int1a2(x-d,d)+(f2(x-d)+4*f2(x-d*.5)+f2(x))*d/6.)
int1b2(x,d) = (x>=-d*.1) ? 0 : (int1b2(x+d,d)+(f2(x+d)+4*f2(x+d*.5)+f2(x))*d/6.)
#
integral_f4(x) = (x>0)?int1a4(x,x/ceil(x/delta)):-int1b4(x,-x/ceil(-x/delta))
int1a4(x,d) = (x<=d*.1) ? 0 : (int1a4(x-d,d)+(f4(x-d)+4*f4(x-d*.5)+f4(x))*d/6.)
int1b4(x,d) = (x>=-d*.1) ? 0 : (int1b4(x+d,d)+(f4(x+d)+4*f4(x+d*.5)+f4(x))*d/6.)
#
integral_f6(x) = (x>0)?int1a6(x,x/ceil(x/delta)):-int1b6(x,-x/ceil(-x/delta))
int1a6(x,d) = (x<=d*.1) ? 0 : (int1a6(x-d,d)+(f6(x-d)+4*f6(x-d*.5)+f6(x))*d/6.)
int1b6(x,d) = (x>=-d*.1) ? 0 : (int1b6(x+d,d)+(f6(x+d)+4*f6(x+d*.5)+f6(x))*d/6.)
#
integral_f8(x) = (x>0)?int1a8(x,x/ceil(x/delta)):-int1b8(x,-x/ceil(-x/delta))
int1a8(x,d) = (x<=d*.1) ? 0 : (int1a8(x-d,d)+(f8(x-d)+4*f8(x-d*.5)+f8(x))*d/6.)
int1b8(x,d) = (x>=-d*.1) ? 0 : (int1b8(x+d,d)+(f8(x+d)+4*f8(x+d*.5)+f8(x))*d/6.)
#number of nuclei at 8+
N=100
#1/tau  in picoseconds
l2=0.693/100.
l4=0.693/10.
l6=0.693/5.
l8=0.693/1.
f8(x)=N*exp(-l8*x)
f6(x)=l8*N/(l8-l6)*(exp(-l6*x)-exp(-l8*x))
f4(x)=l6*l8*N*(exp(-l8*x)/(l8*l8+(-l6-l4)*l8+l4*l6)-exp(-l6*x)/((l6-l4)*l8-l6*l6+l4*l6)+exp(-l4*x)/((l6-l4)*l8-l4*l6+l4*l4))
f2(x)=l4*l6*l8*N*(-exp(-l8*x)/(l8*l8*l8+(-l6-l4-l2)*l8*l8+((l4+l2)*l6+l2*l4)*l8-l2*l4*l6)+\
exp(-l6*x)/((l6*l6+(-l4-l2)*l6+l2*l4)*l8-l6*l6*l6+(l4+l2)*l6*l6-l2*l4*l6)\
-exp(-l4*x)/(((l4-l2)*l6-l4*l4+l2*l4)*l8+(l2*l4-l4*l4)*l6+l4*l4*l4-l2*l4*l4)\
+exp(-l2*x)/(((l4-l2)*l6-l2*l4+l2*l2)*l8+(l2*l2-l2*l4)*l6+l2*l2*l4-l2*l2*l2))
set log x
set xrange[.1:200]
set yrange [0:120]
set key left
set xlabel "Flight time [ps]"
set ylabel "Fraction decay in flight [%]"
#  plot f2(x) title "N_{2^{+}}(t)" w l lc 0
#replot f4(x) title "N_{4^{+}}(t)" w l lc 1
#replot f6(x) title "N_{6^{+}}(t)" w l lc 2
#replot f8(x) title "N_{8^{+}}(t)" w l lc 3
  plot integral_f2(x)*l2 title "N^{/Symbol g}_{2^{+}}(0->t)" w l ls 1 lc 0
replot integral_f4(x)*l4 title "N^{/Symbol g}_{4^{+}}(0->t)" w l ls 1 lc 1
replot integral_f6(x)*l6 title "N^{/Symbol g}_{6^{+}}(0->t)" w l ls 1 lc 2
replot integral_f8(x)*l8 title "N^{/Symbol g}_{8^{+}}(0->t)" w l ls 1 lc 3
#Results from geant4 sim
Norm=90.52
shift=0
v=0.0225*300
factor=1
replot "Os170.txt" using (factor*$1-shift/v):($2/Norm):($3/Norm) title "G4.9.6 2^{+}" w yerrorbars lc 0
replot "Os170.txt" using (factor*$1-shift/v):($4/Norm):($5/Norm) title "G4.9.6 4^{+}" w yerrorbars lc 1
replot "Os170.txt" using (factor*$1-shift/v):($6/Norm):($7/Norm) title "G4.9.6 6^{+}" w yerrorbars lc 2
replot "Os170.txt" using (factor*$1-shift/v):($8/Norm):($9/Norm) title "G4.9.6 8^{+}" w yerrorbars lc 3
#replot f2(x)+f4(x)+f6(x)+f8(x)
set terminal postscript color enhanced 
set output "rddstest.ps"
replot
set terminal x11
replot