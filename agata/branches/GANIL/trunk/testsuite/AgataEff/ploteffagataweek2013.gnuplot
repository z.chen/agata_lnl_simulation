reset
file = 'effAgataWeek2013.txt' 
set ylabel "Efficiency [%]"
set title "AGATA+EXOGAM eff"
set style data histograms 
set style histogram columnstacked 
set style fill solid border -1 
set boxwidth .7 
set xrange [-.5:5.5] 
set yrange [0:11] 
  plot newhistogram at 0 lt 1, file u 2 title col 
replot newhistogram at 1 lt 1, file u 3 title col 
replot newhistogram at 2 lt 1, file u 4 title col 
replot newhistogram at 3 lt 1, file u 5 title col 
replot newhistogram at 4 lt 1, file u 6 title col 
replot newhistogram at 5 lt 1, file u 7 title col 
set arrow 1 from 0,6.15 to 0,6.64 nohead lw 2 front
set arrow 2 from 1,7.18 to 1,8.02 nohead lw 2 front
set arrow 3 from 2,9.22 to 2,10.8 nohead lw 2 front
set arrow 4 from 3,3.14 to 3,4.26 nohead lw 2 front
set arrow 5 from 4,4.51 to 4,6.10 nohead lw 2 front
set arrow 6 from 5,7.57 to 5,10.2 nohead lw 2 front
set term pdf enhanced color linewidth 2
set output "effagataweek2013.pdf"
replot
set term x11 enhanced
replot
