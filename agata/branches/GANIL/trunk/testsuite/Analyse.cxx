//////////////////////////////
//
// Code to analyse data from geom 3 (i.e. test of physics)
// Under development 09/2012 Joa Ljungvall
//
//
///////////////////////////////


#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>

#include "TROOT.h"
#include "TSystem.h"
#include "TH1.h"
#include "TH2.h"
#include "TPad.h"
#include "TMath.h"
#include "TKey.h"
#include "TFile.h"
#include "TClass.h"

//#define _GRAPHICS_

void Analyse(std::string inputfilenameexp="GammaEvents.*",
	     std::string outputfilename = "TransmittionAndEnergyLoss.dat")

{
  std::ofstream outputfile(outputfilename.c_str());
  std::istringstream inputstring;
  std::istringstream files;
  files.str(std::string(gSystem->
			GetFromPipe(("ls "+inputfilenameexp).c_str())));
  //  string oneline;
  while(files.good()){
    std::string filename;
    if(files >> filename){
      std::ifstream inputfile(filename.c_str());
      if(!inputfile.good()) {
	std::cout << filename << " not good...\n";
	continue;
      }
      std::cout << "Processing " << filename << "\n";
      char onechar='\n';
      while(onechar!='$') inputfile >> onechar;
      std::string oneline;
      int transmitted = 0;
      int emitted = 0;
      int token;
      double emittedenergy,vx,vy,vz;
      double energy1=0,x,y,z;
      double energy0=-1; // "-1" first event token
      TH1F energyloss("energyloss","energyloss",1000,0,1);
      //      energyloss.SetBit(TH1::kCanRebin);
      energyloss.SetCanExtend(TH1::kXaxis);
      TH1F energyend("energyend","energyend",1000,0,1);
      // energyend.SetBit(TH1::kCanRebin);
      energyend.SetCanExtend(TH1::kXaxis);
      TH2F EvsAngle("EvsAngle","EvsAngle",1000,0,3.14,1000,0,1);
      //EvsAngle.SetBit(TH1::kCanRebin);
      EvsAngle.SetCanExtend(TH1::kXaxis);
//#ifdef _GRAPHICS_
//      energyloss.Draw();
//      gSystem->ProcessEvents();
//#endif
      while(inputfile.good()){
	getline(inputfile,oneline);
	inputstring.clear();
	inputstring.str(oneline);
	inputstring >> token;
	if(token<0){
	  inputstring >> emittedenergy >> vx >> vy >> vz; 
	  emitted++;
	  if(energy0>0){
	    energyloss.Fill(energy0);
	  }
	  if(energy1>0){
	    energyend.Fill(energy1);
	  }
	  energy0=0; energy1=0;
	} else {
	  if(token==1){
	    double tmp;
	    inputstring >> tmp >> x >> y >> z;
	    if(fabs(tmp-emittedenergy)<1e-20 &&
	       acos((x*vx+y*vy+z*vz)/sqrt(x*x+y*y+z*z))<1e-6){
	      transmitted++;
	    }
	    energy1+=tmp;
	    EvsAngle.Fill(acos((x*vx+y*vy+z*vz)/sqrt(x*x+y*y+z*z)),energy1);
 	  }
	  if(token==0){
	    double tmp;
	    inputstring >> tmp;
	    energy0+=tmp;
	  }
	}
      }
      outputfile << std::setw(10) << emittedenergy << " " 
		 << std::setw(10) << transmitted << std::setw(10) << emitted 
		 << " " <<  std::setw(20) << energyloss.GetMean() << " " 
		 <<  std::setw(20) << energyloss.GetRMS() << " "
		 << " " <<  std::setw(20) << energyend.GetMean() << " " 
		 <<  std::setw(20) << energyend.GetRMS() << "\n";
      outputfile.flush();
#ifdef _GRAPHICS_
      energyloss.Draw();
      gPad->SetLogy();
      gPad->Update();
      gSystem->ProcessEvents();
      gSystem->Sleep(1000);
      EvsAngle.Draw("col");
      gPad->SetLogy(0);
      gPad->Update();
      gSystem->ProcessEvents();
      gSystem->Sleep(1000);      
#endif

    }
  }
}


void AnalyseRDDS(std::string inputfilenameexp="GammaEvents.1*",
		 std::string outputfilename="RDDShistograms.root",double theta=20/*deg*/,
		 double dtheta=15/*deg*/)


{
  TFile outputfile(outputfilename.c_str(),"RECREATE");
  std::istringstream inputstring;
  std::istringstream files;
  std::ostringstream histoname;
  files.str(std::string(gSystem->
			GetFromPipe(("ls "+inputfilenameexp).c_str())));
  //  string oneline;
  int atfile=0;
  while(files.good()){
    std::string filename;
    if(files >> filename){
      std::ifstream inputfile(filename.c_str());
      if(!inputfile.good()) {
	std::cout << filename << " not good...\n";
	continue;
      }
      atfile++;
      std::cout << "Processing " << filename << "\n";
      char onechar='\n';
      while(onechar!='$') inputfile >> onechar;
      std::string oneline;
      int emitted = 0;
      int token;
      double emittedenergy,vx,vy,vz;
      double energy1=0,x,y,z;
      double energy0=-1; // "-1" first event token
      gROOT->cd();
      histoname.str("");
      histoname << "Gammaenergy_" << atfile;
      TH1F Gammaenergy(histoname.str().c_str(),histoname.str().c_str(),4000,0,2000);
      while(inputfile.good()){
	getline(inputfile,oneline);
	inputstring.clear();
	inputstring.str(oneline);
	inputstring >> token;
	if(token<0){
	  inputstring >> emittedenergy >> vx >> vy >> vz; 
	  emitted++;
	  energy0=0; energy1=0;
	} else {
	  if(token==1){
	    double tmp;
	    inputstring >> tmp >> x >> y >> z;
	    if(fabs(acos((x*vx+y*vy+z*vz)/sqrt(x*x+y*y+z*z))-
		    theta*TMath::DegToRad())<dtheta*TMath::DegToRad()){
	      Gammaenergy.Fill(tmp);
	    }
	    energy1+=tmp;
 	  }
	  if(token==0){
	    double tmp;
	    inputstring >> tmp;
	    energy0+=tmp;
	  }
	}
      }
      outputfile.cd();
      Gammaenergy.Write();
#ifdef _GRAPHICS_
      Gammaenergy.Draw();
      gPad->Update();
      gSystem->ProcessEvents();
      gSystem->Sleep(1000);
#endif
    }
  }
 
}



void IntegrateGammaI(std::string inputfilename="RDDShistograms.root",
		     std::string output="Os170.txt",double v_c=0.0225)

{
  double gates[4][2]={{291,293},{471,475},{585,589},{631,635}};
  double listofdistances[15]={10,20,30,40,50,100,200,300,400,500,600,
			      700,800,900,1000}; //microm
  TFile inputfile(inputfilename.c_str());
  TIter next(inputfile.GetListOfKeys());
  TKey *key;
  int atdistance=0;
  std::ofstream outputfile(output.c_str());
   while ((key = (TKey*)next())) {
     TClass *cl = gROOT->GetClass(key->GetClassName());
     if (!cl->InheritsFrom("TH1")) continue;
     TH1 *h = (TH1*)key->ReadObj();
     outputfile << std::setw(15) << std::setprecision(6) 
		<< listofdistances[atdistance]/(v_c*TMath::C()/1e6);
     for(int i=0; i<4; i++){
       int lbin = h->FindBin(gates[i][0]);
       int hbin = h->FindBin(gates[i][1]);
       double error;
       outputfile << std::setw(10) << h->IntegralAndError(lbin,hbin,error) 
		  << " ";
       outputfile << std::setw(10) << error << " ";
     }
     outputfile << "\n";
     atdistance++;
   }
}
