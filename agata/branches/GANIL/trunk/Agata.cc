#include "AgataDistributedParameters.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataPhysicsList.hh"
#include "AgataAnalysis.hh"
#include "AgataGeneratorAction.hh"
#include "AgataRunAction.hh"
#include "AgataEventAction.hh"
#include "AgataSteppingAction.hh"
#include "AgataGeneratorOmega.hh"
#include "AgataSteppingPrisma.hh"
#include "AgataDetectorAncillary.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIdirectory.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"
#include "Randomize.hh"
#include <time.h>
#include <dirent.h>

/*
#ifdef G4VIS_USE
#ifdef G4V47
#include "G4VisExecutive.hh"
#else
#include "AgataVisManager.hh"
#endif
#endif
*/

#if defined(G4VIS_USE_OI) || defined(G4VIS_USE_OIQT) || defined(G4VIS_USE_OPENGL) || defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM) || defined(G4VIS_USE_RAYTRACERX) || defined(G4VIS_USE)
//#ifdef AGATAvis  // AGATAvis defined in CMakeLists.txt
#include "AgataVisManager.hh"
//#else
#include "G4VisExecutive.hh"

//#endif
#endif

#include "G4UIExecutive.hh"

using namespace CLHEP;

void  decodeLine(int, char **);   // decoding of command line
//Make a RadioactiveDecayfile with IT for all known gammas in 
//photonevaporation file
void MakeDecayFile(char **);
void ChangeStateLifetime(char **);
#ifdef GASP
G4int   gtype   = 1;                // which geometry
#else
G4int   gtype   = 0;                // which geometry
#endif
G4String ancType = "1 0";           // which ancillary
// which event generator ( 0, 1 --> AgataStd; 2 --> user defined)
G4int   genType = 0;                
G4int   batch   = 0;                // points to macro name for batch mode
#ifdef NARRAY
G4bool  hadrons = true;            // physics list generates hadrons
#else
G4bool  hadrons = false;            // physics list generates hadrons
#endif
G4bool  lowEner = true;             // physics list uses low-energy interaction for gammas/electrons
G4bool  lowEnHa = true;             // physics list uses low-energy interaction for hadrons
G4bool  usePola = false;            // photon polarization is considered
                                    // NB polarization overrides LECS!
G4bool  useLECS = false;            // considers Compton profile (non-std Geant distribution!)
G4bool  useScreenedNuclear = false;
G4bool  volume  = false;            // enables geantinos for special purposes (volume and solid angle calculations)
G4String iniPath = "./";             // path of the detector configuration files

G4long seed = 0;
G4int runNo = 0;

G4bool  interno = true;
G4bool  userDef = false;
G4bool  QT = true;
G4bool  gps = false;
//G4bool  Addon = false;

G4bool  targetExuserDef = false;
G4bool  makedecayfile = false;
G4bool changestatelifetime = false;
G4bool usetestgenerator = false;
G4bool AgataDistributedParameters::resume=false;
G4String AgataDistributedParameters::resume_dir="";
G4String AgataDistributedParameters::eventinfo_file="";
G4int AgataDistributedParameters::resume_runnb=0;
G4int AgataDistributedParameters::resume_eventnb=0;
G4bool score=false;
std::vector<std::string> addons;

int main(int argc,char** argv) {

  if(argc >1)
    decodeLine(argc, argv);
  if(makedecayfile){
    MakeDecayFile(argv);
    return 0;
  }
  if(changestatelifetime){
    ChangeStateLifetime(argv);
    return 0;
  }
  if( seed )
    HepRandom::setTheSeed(seed);//changes the seed of the random engine
   
  if( runNo <= 0 )
    runNo = 0;  
    
  // try to avoid some known conflicts
  if( !lowEner ) {
    useLECS = false;
    G4cout << " ---> Standard interaction set chosen, disabling Compton profile." << G4endl;
  }
  if( usePola ) {
    useLECS = false;
    G4cout << " ---> Polarization of the gammas considered, disabling Compton profile." << G4endl;
  }
  if( userDef ) {
    genType = 2; 
    if(targetExuserDef) genType=4;
  }
  else if( interno ) {
    if(!gps){
      genType = 0;
    }else
      {
		genType = 5;
		interno = false; // set to false so that reaclist in the physicslist will not be constructed since not needed 
      }
  }   
  else {
    genType = 1;
  }
  if(gtype==3) genType=3;
  if(usetestgenerator) genType=3;

#if defined(G4VIS_USE_OI) || defined(G4VIS_USE_OIQT) || defined(G4VIS_USE_OPENGL) || defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM) || defined(G4VIS_USE_RAYTRACERX) || defined(G4VIS_USE)
  // Visualization, if you choose to have it!
  G4VisManager* visManager = NULL;
  //#ifdef AGATAvis
  if(!QT){
    visManager = new AgataVisManager;
  //#else
  }else
    {
      visManager = new G4VisExecutive;
    }
//#endif

  /*
#ifdef G4V47
  G4VisManager* visManager = new G4VisExecutive;
#else
  G4VisManager* visManager = new AgataVisManager;
#endif
  */
  visManager->Initialize();
#endif

  // The main directory for Agata UI commands
  const char *aLine;
  G4String commandName;
  G4String directoryName;
#ifdef GASP
  directoryName  = "Gasp";
#else  
#ifdef CLARA
  directoryName  = "Clara";
#else  
#ifdef POLAR
  directoryName  = "Polar";
#else  
#ifdef DEIMOS
  directoryName  = "Deimos";
#else  
#ifdef NARRAY
  directoryName  = "NArray";
#else  
  directoryName  = "Agata";
#endif  
#endif  
#endif  
#endif  
#endif
  
  G4UIdirectory* pAgataUIDirectory;
  commandName = "/" + directoryName + "/";
  pAgataUIDirectory = new G4UIdirectory(commandName);
  commandName = directoryName + " simulation control.";
  aLine = commandName.c_str();
  pAgataUIDirectory->SetGuidance(aLine);
  
  directoryName = "/" + directoryName;

  // Run manager
  G4RunManager * runManager = new G4RunManager;

 // UserInitialization classes 
  AgataPhysicsList*            pAgataPhysics  = gtype!=3 ? 
    new AgataPhysicsList(directoryName, hadrons, lowEner, lowEnHa, usePola, useLECS,useScreenedNuclear,interno,userDef) : 
    new AgataPhysicsList(directoryName, hadrons, lowEner, lowEnHa, usePola, useLECS,useScreenedNuclear,interno,userDef,true);
    //new AgataPhysicsList(directoryName, hadrons, lowEner, lowEnHa, usePola, useLECS,useScreenedNuclear,interno) : 
    //new AgataPhysicsList(directoryName, hadrons, lowEner, lowEnHa, usePola, useLECS,useScreenedNuclear,interno,true);
//  myTestPhysicsList* pAgataPhysics  = new myTestPhysicsList();
  runManager->SetUserInitialization( pAgataPhysics );

  // The on-line analysis class 
  AgataAnalysis*  pAgataAnalysis = new AgataAnalysis(directoryName);

 // UserInitialization classes
  if(score) gtype+=100; //Adds to gtype to tell make score sphere
  AgataDetectorConstruction*   pAgataDetector = 
    new AgataDetectorConstruction(gtype,ancType,iniPath,volume,directoryName,
				  score);
  //Here we look for and add Add Ons
  //We also call the "Constructor()" function that will intialize
  //things such as messangers etc. so that they are aviable...
//#ifdef useROOT  // added to avoid crashe with PrismaFP ancillary may not be needed
// if(Addon){  // if(Addon  addedd by Marc but may no longer be needed.
  std::vector<std::string>::iterator itaddons = addons.begin();
  for(; itaddons!=addons.end(); ++itaddons){
    AgataDetectorAncillary::AddAdditionalStrucutre(*itaddons);
  }
// }
//#endif
  runManager->SetUserInitialization( pAgataDetector );

  // UserAction classes 
//  AgataGeneratorAction*        pAgataGenerator   = new AgataGeneratorAction(iniPath, genType, hadrons, usePola, directoryName, Addon); //Addon added by Marc
  AgataGeneratorAction*        pAgataGenerator   = new AgataGeneratorAction(iniPath, genType, hadrons, usePola, directoryName);
  AgataRunAction*              pAgataRunAction   = new AgataRunAction( pAgataAnalysis, volume, directoryName );
  AgataEventAction*            pAgataEventAction = new AgataEventAction( pAgataAnalysis, directoryName );

  AgataSteppingPrisma *pAgataSteppingPrisma = new 
    AgataSteppingPrisma(directoryName); 
  
  if( runNo || AgataDistributedParameters::resume) {
    /*
      If we are resuming a stopped simulation we ignore the runNo from the 
      command line and set it based on what we find in the resume directory
      */
    if(AgataDistributedParameters::resume){
      //We walk thourgh the files in the resume directory to find
      //the latest run and event numbers
      DIR *dirp;
      struct dirent *dp;
      if ((dirp = opendir(AgataDistributedParameters::resume_dir.c_str())) 
	  == NULL) {
	std::cout << "Could not open resume dir " 
		  <<AgataDistributedParameters::resume_dir 
		  << " exiting\n";
        exit(-1);
      }
      char tmpfilename[4096];
      do {
        if ((dp = readdir(dirp)) != NULL) {
	  if(strncmp(dp->d_name,"run",3)==0){
	    int run;
	    long int event;
	    sscanf(dp->d_name,"run%devt%ld.rndm",&run,&event);
	    if(run>AgataDistributedParameters::resume_runnb){
	      AgataDistributedParameters::resume_runnb=run;
	    }
	    if(event>AgataDistributedParameters::resume_eventnb){
	      AgataDistributedParameters::resume_eventnb=event;
	    }
	    sprintf(tmpfilename,"InfoRun%d_Event%d.dat",
		    AgataDistributedParameters::resume_runnb,
		    AgataDistributedParameters::resume_eventnb);
  	  }
	}
      } while (dp != NULL);
      (void) closedir(dirp);
      runNo=AgataDistributedParameters::resume_runnb;
      AgataDistributedParameters::eventinfo_file=G4String(tmpfilename);
    }
    AgataGeneratorOmega *pDummyGen = new AgataGeneratorOmega();
    runManager->SetUserAction( pDummyGen );
    runManager->BeamOn(0);  // this is needed to initialize the run number
    runManager->SetRunIDCounter( runNo );
  }

  runManager->SetUserAction( pAgataGenerator );
  runManager->SetUserAction( pAgataRunAction );  
  runManager->SetUserAction( pAgataEventAction );
  if(gtype!=3) runManager->SetUserAction( pAgataSteppingPrisma);
// Uncomment if infinite loops appear
//    AgataSteppingAction*  pAgataStepping = new AgataSteppingAction();
//    runManager->SetUserAction( pAgataStepping );

  if(gtype==3){//Some extra checks are put 
    AgataSteppingAction*  pAgataStepping = new AgataSteppingAction();
    runManager->SetUserAction( pAgataStepping );
  }
  //Initialize G4 kernel
  runManager->Initialize();
      
  pAgataDetector->ShowStatus();    
  pAgataGenerator->GetStatus();    
  pAgataEventAction->ShowStatus();    
  pAgataRunAction->ShowStatus();    
  pAgataAnalysis->ShowStatus(); 
  
  //get the pointer to the User Interface manager 
  G4UImanager * UI = G4UImanager::GetUIpointer();  

  if(!batch) {                    // Define (G)UI terminal for interactive mode
     if(!QT){
       G4UIsession * session = 0;
       session = new G4UIterminal(new G4UItcsh);      
       session->SessionStart();
       delete session;
   
     }else
       {

#if defined(G4VIS_USE_OI) || defined(G4VIS_USE_OIQT) || defined(G4VIS_USE_OPENGL) || defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM) || defined(G4VIS_USE_RAYTRACERX) || defined(G4VIS_USE)
 	G4UIExecutive* session = 0;
 	session = new G4UIExecutive(argc,argv);      
 	  UI->ApplyCommand("/control/verbose 2");    
 	  UI->ApplyCommand("/run/verbose 2");    
 	  UI->ApplyCommand("/hits/verbose 0");	  
 //#ifndef AGATAgdml
 	  UI->ApplyCommand("/control/execute macros/geom180-Demo.mac");
 	  UI->ApplyCommand("/control/execute macros/visOGL.mac");
 	 //UI->ApplyCommand("/control/execute macros/agata_anc_OGL.mac");
 	  //UI->ApplyCommand("/control/execute macros/agata_OGL.mac");
 //#else
 //	UI->ApplyCommand("/control/execute macros/agataVisgdmlOGL.mac");
 //#endif

 	  session->SessionStart();
 	  delete session;
#endif
      }
  }
  else {                          // Batch mode
    G4String command = "/control/execute ";
    G4String fileName = argv[batch];
    UI->ApplyCommand(command+fileName);
  }

  delete runManager;

#if defined(G4VIS_USE_OI) || defined(G4VIS_USE_OIQT) || defined(G4VIS_USE_OPENGL) || defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM) || defined(G4VIS_USE_RAYTRACERX) || defined(G4VIS_USE)
  delete visManager;
#endif


  return 0;
}

void
decodeLine(int argc, char *argv[])
{
  int nn = 0, ok = 0;
  if (argc > 1) {
    nn = 1;
    ok = 1;
  }
  else {
    ok = 0;
  }
  while(nn < argc && ok == 1) {
    ok = 0;
#ifndef CLARA
    if(!strcmp(argv[nn], "-g")) {
      if(++nn < argc) gtype = atoi(argv[nn]);
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }
#endif
#ifndef GASP
#ifndef CLARA
#ifndef POLAR
#ifndef DEIMOS
#ifndef NARRAY
    if(!strcmp(argv[nn], "-a")) {
      G4int ij, ik, nAnc, nAnc1, *aaType, shift = 0;
      char aLine[256];
      if(++nn < argc) {
        nAnc = atoi(argv[nn]);
	nAnc1 = nAnc;
	if( nAnc <= 0 ) {
          printf("Invalid value!\n");
	  exit(-1);
	}
	aaType = new G4int[nAnc];
	for( ij=0; ij<nAnc1; ij++ ) {
	  if(++nn < argc) {
	    aaType[ij+shift] = atoi(argv[nn]);
	    for( ik=0; ik<ij+shift; ik++ ) {
	      if( aaType[ik] == aaType[ij+shift] ) {
	        printf("Warning! Skipped repeated ancillary #%d ...\n", aaType[ij+shift]);
		nAnc--;
		shift--;
		continue;
	      } 
	    }
	  }
	  else {
            printf("Not enough data!!!\n");
	    exit(-1);
	  }
	}
	sprintf(aLine, "%d", nAnc);
	ancType = G4String(aLine);
	for( ij=0; ij<nAnc; ij++ ) {
	  sprintf(aLine, " %d", aaType[ij]);
	  ancType = ancType + G4String(aLine);
	}
        //G4cout << " AncilString is " << ancType << G4endl;
	//exit(-1);
      }
      else {
        printf("Not enough data\n");
	exit(-1);
      }
      ok = 1;
    }
#endif
#endif
#endif
#endif
#endif
    if(!strcmp(argv[nn], "-b")) {
      if(++nn < argc) batch = nn;
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }    
    if(!strcmp(argv[nn], "-gps")) {
      gps = true;
      ok = 1;
	}
    if(!strcmp(argv[nn], "-n")) {
      hadrons = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-noQT")) {
      QT= false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-noLE")) {
      lowEner = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-noLowH")) {
      lowEnHa = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-p")) {
      usePola = true;
      ok = 1;
    }
#ifdef G4LECS
    if(!strcmp(argv[nn], "-C")) {
      useLECS = true;
      ok = 1;
    }
#endif    
    if(!strcmp(argv[nn], "-Vol")) {
      volume = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Ext")) {
      interno = false;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Gen")) {
      userDef = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-TargetEx")) {
      userDef = true;
      targetExuserDef = true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Test")) {
      usetestgenerator=true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Score")) {
      score=true;
      ok = 1;
    }
    if(!strcmp(argv[nn], "-Path")) {
      if(++nn < argc)  {
        char *path;
        path = (char*)calloc(strlen(argv[nn]) + 1, 1);
        strcpy(path,  argv[nn]);
        G4int length = strlen(path);
        if( path[length-1] != '/' )
          iniPath = G4String(path) + "/";
        else    
          iniPath = G4String(path);
      }
      ok = 1;
    }
    if(!strcmp(argv[nn], "-seed")) {
      seed=time(0); //returns time in seconds as an integer
      ok = 1;
    }
    if(!strcmp(argv[nn], "-run")) {
      if(++nn < argc) runNo = atoi(argv[nn]);
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
    }
#if defined G4V495 || G4V496 || G4V10
    if(!strcmp(argv[nn], "-SN")){
      useScreenedNuclear = true;
      ok = 1;
    }
#endif
    if(!strcmp(argv[nn], "-h")) {
      ok = -1;
    }
    if(!strcmp(argv[nn], "-decay") && argc==3){
      makedecayfile = true;
      ok = 1;
      return;
    }
    if(!strcmp(argv[nn], "-changetaustate") && argc==6){
      changestatelifetime=true;
      ok = 1;
      return;
    }
    if(!strcmp(argv[nn], "-resume")){
      AgataDistributedParameters::resume = true;
      if(++nn < argc){
	std::istringstream is(argv[nn]);
	is >> AgataDistributedParameters::resume_dir;
      }
      else            {printf("Not enough data\n"); exit(-1);}
      ok = 1;
      return;
    }

    if(!strcmp(argv[nn], "-AddOn")){
      //Addon=true; // added by Marc
      if(++nn<argc) addons.push_back(std::string(argv[nn]));
      else {
	std::cout << "Not enough data\n";
	exit(-1);
      }
      ok=1;
    }
    nn++;
  }

  if(ok < 1) {
    if(ok == 0 && argc > 1) printf("\nInvalid switch %s\n", argv[nn-1]);
    printf("\nUsage: %s  [-g -n -noLE -C -p -Vol -Ext -b -Path -h]\n\n", argv[0]);
#ifdef GASP
    printf("    -g   index    select GASP configuration:\n");
    printf("         1 --> Configuration I (BGO Inner Ball)\n");
    printf("         2 --> Configuration I (BGO Inner Ball) + n-Ring\n");
    printf("         3 --> Configuration II with lead collimator\n");
    printf("         4 --> Configuration II without lead collimator\n");
    printf("         5 --> Configuration II with n-Detectors and without lead collimator\n");
    printf("         6 --> Configuration II with n-Ring and without lead collimator\n");
    printf("         7 --> Configuration II with big lead collimator\n");
    printf("         8 --> Configuration I (BGO Inner Ball) + ISIS\n");
    printf("         9 --> Configuration I (BGO Inner Ball) + ISIS + n-Ring\n");
    printf("        10 --> Configuration II with big lead collimator + EUCLIDES\n");
#else        
#ifdef POLAR
    printf("    -g   index    select geometry (0=Polarimeter, 1=Clover)\n");
#else
    printf("    -g   index    select geometry (0=AGATA, 1=SHELL, 2=Simple, 3=Geom for test of Physics 4=Orgam (eurogam) 5=Galileo 6=Nuball2 HPG\n");
#endif
#endif
#ifndef GASP
#ifndef CLARA
#ifndef POLAR
#ifndef DEIMOS
#ifndef NARRAY
    printf("    -a  N_anc n_1 ... n_N    select ancillary devices\n");
    printf("         1 --> Koeln \n");
    printf("         2 --> Shell (?) \n");
    printf("         3 --> MCP (to be used with CLARA but CLARA files missing) \n");
    printf("         4 --> Euclides \n");
    printf("         5 --> ADCA (doesn't seem to do anything\n");
    printf("         6 --> Brick (block of material to mimic a dipole close by) \n");
    printf("         7 --> NWall (Problem!)\n");
    printf("         8 --> DIAMANT (Problem!)\n");
    printf("         9 --> EXOGAM \n");
    printf("        10 --> Helena (Problem!)\n");
    printf("        11 --> RFD  \n");
    printf("        12 --> NEDA (empty!)\n\n");

    printf("        16 --> CASSANDRA (Problem !)\n");
    printf("        17 --> AIDA \n");
    printf("        18 --> FATIMA \n");
    printf("        19 --> PARIS \n");
    printf("        20 --> GSI chamber (2015) \n");
    printf("        21 --> SPIDER (gdml) \n\n");

    printf("        23 --> NordBallNDet \n");
    printf("        24 --> Plastic Orsay \n");
    printf("        25 --> Miniball \n");
    printf("        26 --> HoneyComb (gdml) \n");
    printf("        27 --> Vamos Chamber (gdml) \n");
    printf("        28 --> OOPS (gdml) \n");
    printf("        29 --> GALILEO plunger \n");
    printf("        30 --> SIGMA \n");
    printf("        31 --> Diamant plung (gdml)\n");
    printf("        32 --> Diamant FTgt (gdml)\n");
    printf("        33 --> Orsay OPSA\n");
    printf("        34 --> PRISMA magnets \n");
    printf("        35 --> PRISMA FP \n");
    printf("        36 --> PRISMA LNL reaction chamber (gdml) \n\n");
    printf("        66 --> JG Neutron Device \n\n");


#endif
#endif
#endif
#endif
#endif
    printf("    -n            use also neutrons, protons, ... \n");
    printf("    -noLE         use the standard treatment of the interactions for gamma/e-/e+\n");
    printf("    -noLowH       use the standard treatment of the interactions for hadrons\n");
#ifdef G4LECS
    printf("    -C            consider the Compton profile\n");
#endif
    printf("    -p            consider the linear polarization of the photons\n");
    printf("    -Vol          enables the use of geantinos for volume and solid angle calculation\n");
    printf("    -Ext          reads the event sequence from file\n");
    printf("    -Gen          uses user-supplied event generation\n");
    printf("    -TargetEx     Targetlike/beamlike excitations (sets -Gen)\n");
    printf("    -Test         Event generator using geant4 gun commands\n");
    printf("    -Score        Write particle type,E,p when passing sphere R=5cm to file \"score\"\n");
    printf("    -b file.mac   runs in batch mode executing file.mac\n");
    printf("    -noQT         runs in terminal window\n");
    printf("    -Path <dir>   sets base directory to <dir>\n");
    printf("    -seed         uses current time as seed for random number engine\n");
    printf("    -run r        sets first run number to r\n");
#if defined G4V495 || G4V496  || G4V10
    printf("    -SN           Adds functionality for DSM and RDDS\n");
#endif
    printf("    -decay zZZ.aAAA     make radioactive decayfiles\n");
    printf("    -changetaustate decayfile newdecayfile E_state tau_state \n");
    printf("    -AddOn library_name Add some kind of det. via shared lib\n");
    printf("    -resume <dir> resume simulation using files in dir\n");
    printf("    -gps          using general particle source \n");
    printf("    -h            help, print this list\n");
    if(argc > 1 || ok < 0) exit(-1);
  }

}


bool sortphotondecays(const std::string &d1,const std::string &d2)

{
  std::istringstream is1(d1);
  std::istringstream is2(d2);
  double l1,l2;
  is1 >> l1;
  is2 >> l2;
  return (l1<l2);
}


void MakeDecayFile(char **argv) 

{
  std::cout << "Welcome to Make-a-decay-file ;)\n";
  if(!(getenv("G4RADIOACTIVEDATA") && getenv("G4LEVELGAMMADATA"))){
    std::cout << "You need to set G4RADIOACTIVEDATA and G4LEVELGAMMADATA sys. variables\n";
    return;
  }
  std::cout << "\n    Given nucleus (zZZ.aAAA) : " << argv[2] << std::endl;
  G4String nuc(argv[2]);
  G4String decayfile(getenv("G4RADIOACTIVEDATA"));
  decayfile +="/"+nuc;
  std::ifstream decays(decayfile);
  G4String photonfile(getenv("G4LEVELGAMMADATA"));
  photonfile +="/"+nuc;
  std::ifstream photons(photonfile);
  std::cout << "    Give name of new decayfile : ";
  G4String newdecayfile;
  std::cin >> newdecayfile;
  std::ofstream ndefile(newdecayfile);
  //Do we want artificial levels for feeding?
  std::cout << "    Do you want to add artificial levels for feeding(y/n) : ";
  std::string answer;
  do{
    std::cin >> answer;
  }while(!(answer=="n" || answer=="y"));
  std::string photonfilename;
  std::ofstream phfile;
  if(answer=="y"){
    std::cout << "    Give name of new photon evaporation file : ";
    std::cin >> photonfilename;
    phfile.open(photonfilename.c_str());
  }
  //First just copy the old decay file
  std::string oneline;
  while(decays.good()){
    std::getline(decays,oneline);
    if(decays.good()) ndefile << oneline << std::endl;
  }
  //Then we add all gamma levels as IT with the correct lifetime
  //First we read in all the photon decays and if asked for add the
  //aritificial levels for feeding
  std::vector<std::string> vectorofonephoton;
  std::vector<double> levels;
  while(photons.good()){
    std::getline(photons,oneline);
    if(!photons.good()) continue;
    std::istringstream is(oneline);
    double El, lifetime;
    is >> El >> lifetime >> lifetime >> lifetime >> lifetime;
    if(levels.size()>0){
      if(levels.back()!=El){
	//This means we have changed level, so if asked for we should add
	//the arificial level for last level
	if(answer=="y"){
	  is.clear();
	  is.str(vectorofonephoton.back());
	  double eal;
	  is >> eal;
	  eal+=10;
	  std::ostringstream nleveline;
	  nleveline << eal << " " << 10 << " " << 100 << " " << -2 << " "
		    << "0 0 0 0 0 0 0 0 0 0 0 0 0";
	  vectorofonephoton.push_back(nleveline.str());
	}
	levels.push_back(El);
      }
    } else {
      levels.push_back(El);
    }
    vectorofonephoton.push_back(oneline);
  }
  //Now we should sort the list of states with decays
  std::sort(vectorofonephoton.begin(),vectorofonephoton.end(),sortphotondecays);
  levels.clear();
  std::vector<std::string>::iterator itphotons = vectorofonephoton.begin();
  for(; itphotons!=vectorofonephoton.end(); ++itphotons){
    std::istringstream is(*itphotons);
    if(answer=="y"){
      phfile << *itphotons << std::endl;
    }
    double El, lifetime;
    is >> El >> lifetime >> lifetime >> lifetime >> lifetime;
    if(levels.size()>0){
      if(levels.back()!=El){
	ndefile << "P       " << std::setw(8) << El << "   " 
		<< lifetime << "\n";
	ndefile << "                         IT      " << std::setw(8) 
		<< El << "      100.\n";
	levels.push_back(El);
      }
    } else {
      levels.push_back(El);
      ndefile << "P       " << std::setw(8) << El << "   " << lifetime << "\n";
      ndefile << "                         IT      " << std::setw(8) 
	      << El << "      100.\n";
    }
  }
}


void ChangeStateLifetime(char **argv)

{
  std::ifstream oldfile(argv[2]);
  double E,Etochange,newthalf;
  if(oldfile.good()){
    std::ofstream newfile(argv[3]);
    if(newfile.good()){
      std::string firstline,secondline;
      sscanf(argv[4],"%lf",&Etochange);
      sscanf(argv[5],"%lf",&newthalf);
      newthalf*=log(2.0);
      while(oldfile.good()){
	std::getline(oldfile,firstline);
	std::getline(oldfile,secondline);
	if(oldfile.good()){
	  //Is this the level that we should change lifetime for
	  sscanf(firstline.c_str(),"P       %lf",&E);
	  if(E==Etochange){//Change this guy
	    newfile << "P       " << std::setw(8) << E << "   " 
		    << newthalf*1e-12 << "\n";
	  } else {
	    newfile << firstline << "\n";
	  }
	  newfile << secondline << "\n";
	}
      }
    } else exit(-1);
  } else exit(-1);
}
