/////////////////////////////////////////////////////////////
/// This class is used only during solid angle calculation.
/// Since geantinos do not interact, they can only be
/// "detected" by examining the step!
////////////////////////////////////////////////////////////
#ifndef AgataSteppingOmega_h
#define AgataSteppingOmega_h 1

#include "G4UserSteppingAction.hh"

using namespace std;

class AgataDetectorConstruction;

class AgataSteppingOmega : public G4UserSteppingAction
{
  public:
    AgataSteppingOmega() {};
   ~AgataSteppingOmega() {};
    
  private:
    AgataDetectorConstruction*  theDetector;

  /////////////////////////////////////////////////////////
  /// This method is needed by G4UserSteppingAction
  ////////////////////////////////////////////////////////
  public:
    void UserSteppingAction(const G4Step*);
};

#endif
