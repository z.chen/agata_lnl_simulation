////////////////////////////////////////////////////////////////////////////////////////
/// This class handles the properties of the particles which will be emitted in the
/// simulation, providing the transformations from CM to Lab. The CM energy is sampled
/// according to several possible distributions (both discrete and continuous).
////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataEmitted_h
#define AgataEmitted_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4LorentzVector.hh"
#include <vector>
#if defined G4V10
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
//#include "CLHEP/Units/GlobalSystemOfUnits.h"
//#include "CLHEP/Units/GlobalPhysicalConstants.h"
#endif

using namespace std;

class AgataEmittedMessenger;

class AgataEmitted
{
  public:
    AgataEmitted( G4String, G4String, G4String, G4bool, G4bool = false );
    AgataEmitted( G4String, G4int, G4int, G4String = "GenericIon", G4bool = false, G4bool = false );
    ~AgataEmitted();

  private:
    AgataEmittedMessenger *myMessenger;

  private:
    G4String        iniPath;               //> Directory where the spectra files are stored

  /////////////////////////////////////////////
  /// General characteristics of the particle
  /////////////////////////////////////////////
  protected:
    G4String        emittedName;            //> name
    G4double        emittedMass;            //> mass c^2
    G4double        emittedCharge;          //> charge
    G4int           emittedMassNumber;      //> mass number
    G4int           emittedChargeNumber;    //> charge number
 
  //////////////////////////////////////////////
  /// Event-by-event properties of the particle
  //////////////////////////////////////////////
  protected:
    G4double        energyCM;               //> energy (CM system)
    G4double        energyLab;              //> energy (Lab system)
    
  protected:
    G4ThreeVector   dirCM;                  //> direction (CM system)
    G4ThreeVector   dirLab;                 //> direction (Lab system)
    
  protected:
    G4ThreeVector   momentumCM;             // momentum (CM system)
    G4ThreeVector   momentumLab;            // momentum (Lab system)
    
  protected:
    G4LorentzVector fourMomentumCM;         // 4-momentum (CM system)
    G4LorentzVector fourMomentumLab;        // 4-momentum (Lab system)
   
  /////////////////////////////////////////////////////
  /// Angular range for the emission in the CM system
  /////////////////////////////////////////////////////
  protected:
    G4double        emittedThMin,    emittedThMax;       //> theta
    G4double        emittedPhMin,    emittedPhMax;       //> phi
    G4double        emittedThMinCos, emittedThMaxCos;    //> cos(theta)
    
  ////////////////////////////////////////////////////
  /// Characteristics of the energy distribution
  ///////////////////////////////////////////////////
  protected:
    G4int           emittedMult;         //> multiplicity

  private:
    G4bool          usePola;             //> true: particle can be polarized (only gammas)
  
  private:
    G4int           gunType;             //> Kind of CM spectrum:
                                         //> 0 --> monochromatic 		 
                                         //> 1 --> equally spaced gammas 	 
                                         //> 2 --> energies read from file	 
                                         //> 3 --> flat distribution		 
					 //> 4 --> energies sampled from spectrum 
                                         //> 5 --> energies read from file (weighted with the intensity)	 

  /////////////////
  /// gunType 0 ///
  /////////////////                                  
  private:
    G4double        monochrEnergyCM;     //> particle energy
    
  /////////////////
  /// gunType 1 ///
  /////////////////                                  
  private:
    G4double        bandMin;             //> lowest energy
    G4double        bandDelta;           //> energy spacing between two consecutive transitions
    G4int           bandMult;            //> multiplicity
    
  /////////////////
  /// gunType 2 ///
  /////////////////                                  
  private:
    std::vector<G4double>   fileEner;     //> energies read from file
    G4String                fileEnerName; //> name of the file
    G4int                   fileMult;     //> multiplicity
    
  /////////////////
  /// gunType 3 ///
  /////////////////                                  
  private:
    G4double                flatMin;      //> lowest energy
    G4double                flatMax;      //> highest energy

  /////////////////
  /// gunType 4 ///
  /////////////////                                  
  private:
    G4String                fileSpecName;   //> name of the file
    std::vector<G4double>   fileSpecDistrib;//> distribution of probability
    
  /////////////////
  /// gunType 5 ///
  /////////////////                                  
  private:
    G4String                fileEnerIName;//> name of the file
    std::vector<G4double>   fileInte;     //> energies read from file
   
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  /// Polarization: partially polarized transitions are modelled as incoherent
  /// superposition of two polarized beams (of orthogonal polarization)
  /// 1st polarization state (polarValue ==  1): cross product of gamma direction and beam direction
  /// 2nd polarization state (polarValue == -1): cross product of gamma direction and 1st polarization state
  /// polarizationAxis is used in case recoil velocity is zero
  /// In case of partially polarized transitions, the unpolarized fraction is an incoherent sum
  /// of the two states.
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  private:
    std::vector<G4double>        polarValue;              
    std::vector<G4double>        cosPsi;            
    std::vector<G4double>        sinPsi;            
    std::vector<G4ThreeVector>   stokesPar;            
    std::vector<G4bool>          considerPolarization;    //> polarization is considered when the flag is true
    G4ThreeVector                polarizationAxis;
    G4ThreeVector                emittedPolarization;	  //> polarization vector in the Lab system
    G4ThreeVector                emittedStokesParam;	  //> Stokes parameters

  private:  
    std::vector<G4double>         emittedTau;             //> lifetime of the transitions
    
  /////////////////////////
  /// private methods
  ////////////////////////
    
  private:
    void            InitData();                      //> Initialization of class members
    
  private:
    void            ResetEmitted( G4int, G4int );    //> When emitted is a GenericIon, sets Z, A

  ///////////////////////////////////////////////////////////////
  /// Samples energy and direction of the emitted particle
  //////////////////////////////////////////////////////////////
  private:
    void            EmitEnergyCM( G4int );
    void            EmitDirCM();                

  ///////////////////////////////////////////
  /// Transformations from CM to Lab frame
  //////////////////////////////////////////
  private:
    void            TransformCMToLab( G4ThreeVector );
    void            TransformCMToLab( G4ThreeVector, G4ThreeVector );
    
  /////////////////////////////////////////////////////////////////////
  /// Useful methods to calculate relativistically several quantities 
  /////////////////////////////////////////////////////////////////////
  private:
    G4double        BetaFromMomentum     ( G4double, G4double );
    G4double        MomentumFromEnergy   ( G4double, G4double );
    G4double        EnergyFromMomentum   ( G4double, G4ThreeVector );
    G4double        EnergyFromMomentum   ( G4double, G4double );
    G4ThreeVector   VelocityFromMomentum ( G4double, G4ThreeVector );
    G4ThreeVector   MomentumFromVelocity ( G4double, G4ThreeVector );
    G4LorentzVector FourMomentum         ( G4double, G4ThreeVector );
    
  ///////////////////////////////////////////////////////////
  /// Methods to read the energy distributions from file
  ///////////////////////////////////////////////////////////
  private:
    G4int           ReadFileEner   ( G4String );   //> gunType 2
    G4int           InitSpectrumCM ( G4String );   //> gunType 4
    G4int           ReadFileEnerI  ( G4String );   //> gunType 5
    
  private:
    G4double        SampleSpectrum ( );            //> chooses particle energy according to
                                                   //> the CM distribution

  ////////////////////////////////////////////////////////////  
  /// Calculates the polarization vector in the Lab system
  ///////////////////////////////////////////////////////////
  private:
    //> used by InternalEmission
    G4ThreeVector CalculatePolarizationLab ( G4int,    G4ThreeVector, G4ThreeVector );
    //> used by ExternalEmission
    G4ThreeVector getPolarizationLab       ( G4ThreeVector, G4ThreeVector, G4ThreeVector );

  //////////////////////
  /// Public methods ///
  //////////////////////
        
  /////////////////////////////////////////////////////////////////////////////
  /// Main method to sample energy and direction of the next emitted particle
  /////////////////////////////////////////////////////////////////////////////
  public:
    void Emit ( G4int, G4ThreeVector );

  public:
    void GetStatus    ();
    void PrintToFile  ( std::ofstream &outFileLMD, G4double=1.*keV );
    
  //////////////////////////////////////////////////////////////////////////////
  /// Methods to choose CM spectrum, multiplicity and lifetime of the emitted
  /// particles
  //////////////////////////////////////////////////////////////////////////////
  public:
    void SetGun          ( G4int );
    void SetMultiplicity ( G4int );
    void SetEmittedTau   ( G4double );
    
  //////////////////////////////////////////////////////////////////////////////
  /// Methods to set the parameters used in the various possible CM spectra
  //////////////////////////////////////////////////////////////////////////////
  public:
    void SetMonochromaticEnergy ( G4double );
    void SetBand                ( G4double, G4double, G4int );
    void SetFlat                ( G4double, G4double );
    void SetFileEnerName        ( G4String );
    void SetFileEnerIName       ( G4String );
    void SetFileSpecName        ( G4String );
    void SetEmittedFileName     ( G4String );
    
  /////////////////////////////////////////////////////////////////////////////
  /// Methods to set energy and direction of the emitted particle (computed
  /// elsewhere)
  ////////////////////////////////////////////////////////////////////////////
  public:
    void SetEnergyCM            ( G4double );
    void SetDirCM               ( G4ThreeVector );
    
  /////////////////////////////////////////////////////////////////////////////
  /// Methods to set the angular emission range
  ////////////////////////////////////////////////////////////////////////////
  public:
    void SetEmittedTh           ( G4double, G4double );
    void SetEmittedPh           ( G4double, G4double );

  //////////////////////////////////////////////////////////////////////////////
  /// Methods used by the ExternalEmission class (transformation CM-->Lab)
  /////////////////////////////////////////////////////////////////////////////
  public:
    void  setVelocityCM  ( G4double, G4ThreeVector ); 
    void  setVelocityCM  ( G4int,    G4ThreeVector ); 
    void  setVelocityCM  ( G4double, G4ThreeVector, G4ThreeVector ); 
    void  setVelocityCM  ( G4double, G4ThreeVector, G4double, G4double, G4double ); 
    void  setVelocityCM  ( G4int,    G4ThreeVector, G4double, G4double, G4double ); 
    void  setVelocityCM  ( G4double, G4ThreeVector, G4ThreeVector, G4double, G4double, G4double ); 
    void  setVelocityLab ( G4double, G4ThreeVector );
    void  setVelocityLab ( G4double, G4ThreeVector, G4double, G4double, G4double );
    
  ////////////////////////////////////////////////
  /// Method to set the polarization parameters
  ////////////////////////////////////////////////
  public:
    void SetPolarizationAxis ( G4ThreeVector );
    void SetPolarizationValue   ( G4double );
    void SetPolarizationState   ( G4double );
    
  /////////////////////////////////////////
  /// Method to set the polarization 
  /// through the Stokes parameters
  /////////////////////////////////////////
  public:
    void SetStokes           ( G4ThreeVector );

  ////////////////////////////////////////////////////////
  /// Methods to retrieve the relevant information     ///
  ////////////////////////////////////////////////////////  
  public:
    G4double GetEmittedTau ( G4int );   //> transition lifetime
    G4bool   IsPolarized   ( G4int );   //> true: particle is polarized

  ////////////////////////////////////////////////
  /// General characteristics of the particle
  ///////////////////////////////////////////////  
  public:
    inline G4double GetMass()                   { return emittedMass;         };
    inline G4int    GetChargeNumber()           { return emittedChargeNumber; };
    inline G4int    GetMassNumber()             { return emittedMassNumber;   };
    
  public:
    inline G4String GetEmittedName()            { return emittedName;         }; 
    
  public:
    inline G4int    GetMultiplicity()           { return emittedMult;         };

  ///////////////////////////////////
  /// Event-by-event information
  //////////////////////////////////
  public:
    inline G4double GetEnergyCM()               { return energyCM;            }; 
    inline G4double GetEnergyLab()              { return energyLab;           }; 

  public:
    inline G4ThreeVector GetDirCM()             { return dirCM;               }; 
    inline G4ThreeVector GetDirLab()            { return dirLab;              };
    
  public:
    inline G4ThreeVector GetMomentumCM()        { return momentumCM;          };  
    inline G4ThreeVector GetMomentumLab()       { return momentumLab;         };  

  public:
    inline G4ThreeVector GetPolarizationLab()   { return emittedPolarization; };  
    inline G4ThreeVector GetStokesParam()       { return emittedStokesParam;  };  
    
  public:
    inline G4LorentzVector GetFourMomentumCM()  { return fourMomentumCM;      };
    inline G4LorentzVector GetFourMomentumLab() { return fourMomentumLab;     };

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithAString;
class G4UIcmdWith3Vector;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;

class AgataEmittedMessenger: public G4UImessenger
{
  public:
    AgataEmittedMessenger(AgataEmitted*, G4String, G4bool, G4String);
   ~AgataEmittedMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    G4bool                     usePola;
    AgataEmitted*              myTarget;
    G4UIdirectory*             myDirectory;

    G4UIcmdWithAnInteger*      SetGunCmd;
    G4UIcmdWithADouble*        SetMonochromaticCmd;
    G4UIcmdWithAString*        SetBandCmd;
    G4UIcmdWithAString*        SetFlatCmd;
    G4UIcmdWithAString*        SetFileEnerNameCmd;
    G4UIcmdWithAString*        SetFileEnerINameCmd;
    G4UIcmdWithAString*        SetFileSpecNameCmd;
    G4UIcmdWithAnInteger*      SetMultiplicityCmd;
    G4UIcmdWithADouble*        SetTauCmd;
    G4UIcmdWithADouble*        SetPolarCmd;
    G4UIcmdWithADouble*        SetPolarStateCmd;
    G4UIcmdWithAString*        SetEmittedThCmd;
    G4UIcmdWithAString*        SetEmittedPhCmd;
    G4UIcmdWithAString*        SetAxisCmd;
    G4UIcmdWith3Vector*        SetPolAxisCmd;
    G4UIcmdWith3Vector*        SetStokesCmd;

};


#endif
