////////////////////////////////////////////////////////////////////////////////////////////////////////
/// This class, inheriting from G4UserEventAction, handles the printout of event number at beginning
/// of event. Its main task is to format the information which should be sent to the output file.
/// In case the on-line analysis is enabled, the same information is sent to the on-line analysis.
///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef AgataEventAction_h
#define AgataEventAction_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4UserEventAction.hh"

using namespace std;

class G4Event;
class AgataAnalysis;
class AgataRunAction;
class AgataGeneratorAction;
class AgataDetectorConstruction;
class AgataEventActionMessenger;

class AgataEventAction : public G4UserEventAction
{
  public:
    AgataEventAction(AgataAnalysis*,G4String);
   ~AgataEventAction();

  private:
    AgataAnalysis*               theAnalysis;
    AgataDetectorConstruction*   theDetector;
    AgataGeneratorAction*        theGenerator;
    AgataRunAction*              theRun;
    AgataEventActionMessenger*   myMessenger;

  private:
    G4int    eventID;      //> event number         
    G4int    interval;     //> interval for event number printout        

  private:
    G4bool   writeAll;     //> false: header of events without interactions within the active parts
                           //>        of the geometry will be skipped
    G4bool   drawRays;     //> true: particle tracks will be sketched (if visualization is enabled)
    G4bool   drawHits;     //> true: particle hits will be sketched (if visualization is enabled)

  ///////////////////////////////////////////
  /// Information about the output file
  //////////////////////////////////////////
  private:
    G4String outMask;        //> bit mask telling which parameters are actually written out
    G4bool   writeData[9];   //> the same bit mask translated into boolean variables
    G4bool   pulseShape;     //> true: data will be written out in PULSE SHAPE format
                             //> false (default): data will be written out in TRACKING format
  private:
    G4bool   mgsFormat;      //> true: if also pulseShape is true, data will be written out with MGS reference frame
    G4double packDist;       //> interaction points closer than packDist will be packed together
    G4double unitLength;     //> unit length for the output file
    G4double unitEnergy;     //> unit energy for the output file

  private:
    char     *aLine;         //> buffer line where the information is formatted before sending to the output file

  //////////////////////
  /// Private methods
  /////////////////////
  
  //////////////////////////////////////////////////////////////////////////////////////////
  /// Prepare the next line to be sent to the output file according to the output_mask
  //////////////////////////////////////////////////////////////////////////////////////////
  private:
    G4String PrepareLine( G4int, G4double, G4ThreeVector, G4ThreeVector, G4int, G4double, G4int );

  /////////////////////////
  /// Public methods
  /////////////////////////

  //////////////////////////////////////////////////////
  /// Begin/end of event (needed by G4UserEventAction)
  //////////////////////////////////////////////////////
  public:
    void BeginOfEventAction (const G4Event*);
    void EndOfEventAction   (const G4Event*);

  ////////////////////////////
  /// Begin of run
  ////////////////////////////
  public:
    void BeginOfRun();

  public:
    void ShowStatus  ();
    void WriteHeader ( std::ofstream &outFileLMD );

  /////////////////////////////////////////////////////////////////////////////
  /// This method is used by AgataExternalEmission, so that it does not need
  /// to look for the pointer to the AgataAnalysis class
  /////////////////////////////////////////////////////////////////////////////
  public:
    void FlushAnalysis ();

  /////////////////////////////////
  /// Methods for the messenger
  ////////////////////////////////
  public:
    void SetWriteAll        ( G4bool   );
    void SetDrawRays        ( G4bool   );
    void SetDrawHits        ( G4bool   );

  public:
    void SetPackDist        ( G4double );
    void SetUnitLength      ( G4double );
    void SetUnitEnergy      ( G4double );

  public:
    void SetInterval        ( G4int    );

  ////////////////////////////////////////////////////////////////
  //// Methods selecting the information sent to the output file
  ////////////////////////////////////////////////////////////////
  public:
    void SetOutMask         ( G4String );
    void SetEnableMgsFormat ( G4bool );
    void SetEnableNDet      ( G4bool );
    void SetEnableEner      ( G4bool );
    void SetEnablePosA      ( G4bool );
    void SetEnablePosI      ( G4bool );
    void SetEnablePosM      ( G4bool );
    void SetEnableNSeg      ( G4bool );
    void SetEnableTime      ( G4bool );
    void SetEnableInte      ( G4bool );

  ///////////////////////////////////////////////////////////////////////////
  //// Methods selecting the information sent to the output file (shortcuts)
  ///////////////////////////////////////////////////////////////////////////
  public:
    void DataForTracking    ();
    void DataForPulseShape  ();

  public:
    inline G4double GetUnitLength () { return unitLength; };
    inline G4double GetUnitEnergy () { return unitEnergy; };

  public:
    inline G4int    GetEventID    () { return eventID;    };
    inline G4int    GetInterval   () { return interval;   };
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;

class AgataEventActionMessenger: public G4UImessenger
{
  public:
    AgataEventActionMessenger(AgataEventAction*,G4String);
   ~AgataEventActionMessenger();
    
  private:
    AgataEventAction*          myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             myFileDirectory;
    G4UIcmdWithABool*          EnableAllCmd;
    G4UIcmdWithABool*          DisableAllCmd;
    G4UIcmdWithABool*          EnableRaysCmd;
    G4UIcmdWithABool*          DisableRaysCmd;
    G4UIcmdWithABool*          EnableHitsCmd;
    G4UIcmdWithABool*          DisableHitsCmd;
    G4UIcmdWithADouble*        SetPackDistCmd;
    G4UIcmdWithoutParameter*   StatusCmd;   
    G4UIcmdWithADouble*        SetUnitLengthCmd;
    G4UIcmdWithADouble*        SetUnitEnergyCmd;
    G4UIcmdWithAnInteger*      SetIntervalCmd;
    G4UIcmdWithAString*        SetOutMaskCmd;
    G4UIcmdWithABool*          EnableMgsFormatCmd;
    G4UIcmdWithABool*          EnableNDetCmd;
    G4UIcmdWithABool*          EnableEnerCmd;
    G4UIcmdWithABool*          EnablePosACmd;
    G4UIcmdWithABool*          EnableNSegCmd;
    G4UIcmdWithABool*          EnablePosICmd;
    G4UIcmdWithABool*          EnablePosMCmd;
    G4UIcmdWithABool*          EnableTimeCmd;
    G4UIcmdWithABool*          EnableInteCmd;
    G4UIcmdWithABool*          DisableMgsFormatCmd;
    G4UIcmdWithABool*          DisableNDetCmd;
    G4UIcmdWithABool*          DisableEnerCmd;
    G4UIcmdWithABool*          DisablePosACmd;
    G4UIcmdWithABool*          DisableNSegCmd;
    G4UIcmdWithABool*          DisablePosICmd;
    G4UIcmdWithABool*          DisablePosMCmd;
    G4UIcmdWithABool*          DisableTimeCmd;
    G4UIcmdWithABool*          DisableInteCmd;
    G4UIcmdWithoutParameter*   TrackDataCmd;   
    G4UIcmdWithoutParameter*   PSADataCmd;   
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};
#endif

    
