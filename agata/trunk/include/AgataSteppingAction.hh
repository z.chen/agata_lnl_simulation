/////////////////////////////////////////////////////////////
/// This class is normally NOT used. It might come handy in
/// case of infinite loops. It is used also after a solid
/// angle calculation has been performed (but it just
/// performs no action!)
////////////////////////////////////////////////////////////
#ifndef AgataSteppingAction_h
#define AgataSteppingAction_h 1

#include "G4UserSteppingAction.hh"

using namespace std;

class AgataSteppingAction : public G4UserSteppingAction
{
  public:
    AgataSteppingAction() {};
   ~AgataSteppingAction() {};

  /////////////////////////////////////////////////////////
  /// This method is needed by G4UserSteppingAction
  ////////////////////////////////////////////////////////
  public:
    void UserSteppingAction(const G4Step*);
};

#endif
