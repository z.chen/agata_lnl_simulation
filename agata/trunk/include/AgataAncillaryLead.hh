#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryLead_h
#define AgataAncillaryLead_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryLeadMessenger;

class AgataAncillaryLead : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryLead(G4String);
    ~AgataAncillaryLead();

  private:
    AgataAncillaryLeadMessenger* myMessenger;
  private:
    G4ThreeVector ColCentre;
    G4ThreeVector ColCentreNew;
    G4RotationMatrix ColRotMat;
    G4RotationMatrix ColRotMatNew;
  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName;
    G4Material  *matLead;
    G4String     matelHName;
    G4Material  *matelH;
  
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

};

#endif

#endif
