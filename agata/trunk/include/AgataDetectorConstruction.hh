#ifndef AgataDetectorConstruction_h
#define AgataDetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#ifdef G4V10
#include "G4SystemOfUnits.hh"
//#include "CLHEP/Units/GlobalSystemOfUnits.h"
#endif

using namespace std;

class G4Material;
class CConvexPolyhedron;
class G4Box;
class G4Tubs;
class G4Polycone;
class G4Sphere;
class G4IntersectionSolid;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4VisAttributes;
class AgataSensitiveDetector;
class AgataDetectorConstructionMessenger;

///////////////////////////////////////////////////////////////////////
/// This abstract class provides members and methods to count
/// the number of detectors and clusters which have been placed.
///////////////////////////////////////////////////////////////////////
class AgataDetectorConstructed
{
  public:
     AgataDetectorConstructed() {};
    ~AgataDetectorConstructed() {};
  
  protected:
    G4int     nDets;    //> number of detectors
    G4int     nClus;    //> number of clusters
    G4int     iGMin;    //> minimum index for detectors
    G4int     iCMin;    //> minimum index for clusters
    G4int     iGMax;    //> maximum index for detectors
    G4int     iCMax;    //> maximum index for clusters
  
  protected:
    G4int     maxSec;    //> maximum number of sectors
    G4int     maxSli;    //> maximum number of slices
      
  protected:
    G4bool    readOut;  //> true: a segmentation have been defined  
  
  public:
    inline G4int  GetNumberOfDetectors()    { return nDets;             };
    inline G4int  GetMinDetectorIndex()     { return iGMin;             };
    inline G4int  GetMaxDetectorIndex()     { return iGMax;             };
  
  public:
    inline G4int  GetNumberOfClusters()     { return nClus;             };
    inline G4int  GetMinClusterIndex()      { return iCMin;             };
    inline G4int  GetMaxClusterIndex()      { return iCMax;             };
  
  public:
    inline G4int  GetNumberOfSectors ()     { return maxSec;            };
    inline G4int  GetNumberOfSlices  ()     { return maxSli;            };

  public:
    inline G4bool GetReadOut()              { return readOut;           };
  
  ////////////////////////////////////////////////////////////////////////////
  /// Pure virtual methods which must be implemented
  ////////////////////////////////////////////////////////////////////////////
  public:
    virtual void  Placement        ()                                               = 0;
    virtual void  WriteHeader      ( std::ofstream &outFileLMD, G4double = 1.*mm )  = 0;
    virtual void  ShowStatus       ()                                               = 0;
    virtual G4int GetSegmentNumber ( G4int, G4int, G4ThreeVector )                  = 0;
    virtual G4int GetCrystalType   ( G4int )                                        = 0;
};

#ifdef GASP
class AgataDetectorGasp;
#else
#ifdef CLARA
class AgataDetectorClara;
#else
#ifdef POLAR
class AgataDetectorPolar;
class AgataDetectorClover;
#else
#ifdef EUCLIDES
class AgataDetectorEuclides;
#else
class AgataDetectorArray;
#ifndef DEIMOS
class AgataDetectorShell;
class AgataDetectorSimple;
#endif
#endif
#endif  
#endif
#endif

///////////////////////////////////////////////////////////////
/// This class handles the construction of the geometry.
/// It provides the material definition, plus the construction
/// of the experimental hall ("world"), of a target and of a
/// reaction chamber. The actual detector placement is done
/// by instantiating other classes.
///////////////////////////////////////////////////////////////
class AgataDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    AgataDetectorConstruction(G4int, G4int,    G4String, G4bool, G4String);
    AgataDetectorConstruction(G4int, G4String, G4String, G4bool, G4String);
  // following Added for gdml:
    AgataDetectorConstruction(G4VPhysicalVolume*, G4int, G4String, G4String, G4bool, G4String);
    //{
     //hallPhys = setWorld;
    //}

   ~AgataDetectorConstruction();
   
  public:
    /////////////////////////////////////////////////////////////////////
    /// This method is required (defined in G4VUserDetectorConstruction)
    /////////////////////////////////////////////////////////////////////
    G4VPhysicalVolume* Construct();

  protected:
    G4int ancillaryOffset;    //> offset to handle the sensitive detector objects for ancillaries
    
  private:
    AgataDetectorConstructionMessenger* myMessenger;  // pointer to the Messenger

  private:
    /////////////////////////////////
    /// Experimental hall
    /////////////////////////////////
    G4Box                  *hallBox;
    G4LogicalVolume        *hallLog;
    G4VPhysicalVolume      *hallPhys;

  private:
    ///////////////////////////////////////
    /// Pointers to the classes handling
    /// the germanium detector placement
    ///////////////////////////////////////
#ifdef GASP
    AgataDetectorGasp*        newGasp;
#else
#ifdef CLARA
    AgataDetectorClara*       newClara;
#else
#ifdef POLAR
    AgataDetectorPolar*       newPolar;
    AgataDetectorClover*      newClover;
#else
#ifdef EUCLIDES
    AgataDetectorEuclides*    newEuclides;
#else
    AgataDetectorArray*       newArray;
#ifndef DEIMOS
    AgataDetectorShell*       newShell;
    AgataDetectorSimple*      newSimple;
#endif
#endif  
#endif    
#endif    
#endif    

  private:
    ////////////////////////////////////////
    /// Pointer where the class which has 
    /// been instantiated is cast
    ////////////////////////////////////////
    AgataDetectorConstructed* theConstructed;

  private:
    //////////////////////////////////////
    /// Sensitive detector for germanium
    /// detectors (and other detectors)
    /////////////////////////////////////
    AgataSensitiveDetector    *geSD;
    
  private:
    /////////////////////////////////////
    /// Pointers to the materials
    /////////////////////////////////////
    G4Material             *matWorld;
    G4Material             *matTarget;
    G4Material             *matChamber;

  private:
    /////////////////////////////////////
    /// Names of the materials
    /////////////////////////////////////
    G4String                matWorldName;
    G4String                matTargetName;
    G4String                matChamberName;

  private:
    G4int                   geomType;   //> which geometry (0=AGATA, 1=shell, 2=simple)
    G4String                ancType;    //> which ancillary (only for AGATA)

  private:
    G4String                iniPath;    //> directory where the data files are stored

  private:
    G4bool                  volume;     //> true: enables the parts about volume calculation
                                        //> of the germanium detectors
    G4bool                  calcOmega;  //> true: solid angle covered by the germanium detectors
                                        //> is calculated
    
  private:
    G4int*                  numHits;    //> buffer used during volume calculation

  private:
    //////////////////////////////////////////////
    /// Dimensions of the scattering chamber
    /////////////////////////////////////////////
    G4double                outRadius;     
    G4double                wallThickness; 
    G4double                pipeRadius;    

  private:
    //////////////////////////////////////////////
    /// Rotation applied to the scattering chamber
    /////////////////////////////////////////////
    G4double                thetaChamb;     
    G4double                phiChamb; 
    
  private:
    //////////////////////////////////////////////
    /// Dimensions and position of the target
    /////////////////////////////////////////////
    G4ThreeVector           targetPosition;
    G4ThreeVector           targetSize;
    G4double                targetThickness; //> in mg/cm2
  
  ///////////////////////////////////////////////
  /// Lookup table of offsets used by ancillary
  /// detectors
  //////////////////////////////////////////////  
  private:
    std::vector<G4int>      offsetLut;
    
    
  //////////////////////////////////////////////
  /// Command directory name
  /////////////////////////////////////////////
    G4String directoryName;    

////////////////////////////////////////////////////////////////////////////  
/// Private methods							      
////////////////////////////////////////////////////////////////////////////  
  private:
    void InitData (G4int, G4String, G4String, G4bool, G4String);

  private:
    ////////////////////////////////////////////////
    /// Definition of the materials
    ///////////////////////////////////////////////
    void DefineMaterials();
    void DefineAdditionalMaterials();

  private:
    ////////////////////////////////////////////////
    /// Construction of the experimental hall,
    /// target and scattering chamber
    ///////////////////////////////////////////////
    void ConstructHall();     
    void ConstructTarget();  
    void ConstructChamber();  

//////////////////////////////////////////////////////////////////////////// 
/// Public methods							     
//////////////////////////////////////////////////////////////////////////// 
    
  public:
    ////////////////////////////////////////////////////////////
    /// Writes out information to the list-mode file
    ///////////////////////////////////////////////////////////
    void   WriteHeader  ( std::ofstream &outFileLMD, G4double=1.*mm );
    
  public:
    ///////////////////////////////////////////////////////////
    /// Methods performing volume and solid angle calculation
    /// (with Monte Carlo methods)
    //////////////////////////////////////////////////////////
    void   IncrementHits        ( G4int index );
    void   CalculateGeVolume    ( G4int numberOfEvent, G4ThreeVector size );
    void   CalculateGeSolidAngle( G4int numberOfEvent );
    
  public:
    /////////////////////////////////////////////////////////////////////////
    /// This method calculates the effective distance in germanium between
    /// two points. Materials different than germanium are normalized through
    /// their radiation length.
    /////////////////////////////////////////////////////////////////////////
    G4double DistanceInGe ( G4ThreeVector, G4ThreeVector, G4int );
    
  public:
    /////////////////////////////////////////////////////////////////////////
    /// These methods return the number of segment and the crystal type
    /// (presently meaningful only for the AGATA geometry).
    /////////////////////////////////////////////////////////////////////////
    G4int  GetSegmentNumber ( G4int, G4int, G4ThreeVector ); 
    G4int  GetCrystalType   ( G4int ); 

  ////////////////////////////////////////////////////////////////////////////
  /// Public methods to perform actions interactively through the messenger
  /// class.
  ///////////////////////////////////////////////////////////////////////////
  public:
    ///////////////////////////////////////////////////////////
    /// Characteristics of the target
    ///////////////////////////////////////////////////////////
    void SetTargetSize     ( G4ThreeVector );
    void SetTargetPosition ( G4ThreeVector );
    void SetTargMate       ( G4String );

  public:
    ///////////////////////////////////////////////////////////
    /// Characteristics of the scattering chamber
    ///////////////////////////////////////////////////////////
    void SetChamMate       ( G4String );
    void SetWallThickness  ( G4double );
    void SetChamberRadius  ( G4double );
    void SetPipeRadius     ( G4double );
    void SetThetaChamb     ( G4double );
    void SetPhiChamb       ( G4double );

  /////////////////////////////////////////////////////////////
  /// Other methods for the messenger
  //////////////////////////////////////////////////////////////
  public:
    ////////////////////////////////////////////////////////
    /// This method should be execute to commit each change
    /// of geometry.
    ///////////////////////////////////////////////////////
    void UpdateGeometry    ();

  public:
    void ShowStatus        ();
    void ShowCommonStatus  ();
    void GetDistInGe       ( G4ThreeVector, G4ThreeVector, G4int );

  ////////////////////////////////////////////////////////////////////////
  /// This method returns the next available offset to be used by the
  /// ancillary detectors
  ///////////////////////////////////////////////////////////////////////
  public:
    G4int GetAncillaryOffset();
    
  //////////////////////////////////////////////////////////////////////
  /// Methods to set/get elements of the lookup table
  /////////////////////////////////////////////////////////////////////
  public:
    G4int GetOffset     ( G4int );
    void  SetOffset     ( G4int, G4int );
    void  CopyOffset    ( std::vector<G4int>& );
    void  AddOffset     ( G4int ); 
    void  ResetOffset   ();
    void  PrintOffset   ();
    G4int HowManyOffset ();
    
  /////////////////////////////////////////////////////////////////
  /// Inline methods
  ////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////////////////////////////
  /// These methods return the information handled by the AgataDetectorConstructed
  /// object.
  ///////////////////////////////////////////////////////////////////////////////////
  public:
    inline G4int  GetNumberOfDetectors ()    { return  theConstructed->GetNumberOfDetectors(); };
    inline G4int  GetMinDetectorIndex  ()    { return  theConstructed->GetMinDetectorIndex();  };
    inline G4int  GetMaxDetectorIndex  ()    { return  theConstructed->GetMaxDetectorIndex();  };
    inline G4int  GetNumberOfClusters  ()    { return  theConstructed->GetNumberOfClusters();  };
    inline G4int  GetMinClusterIndex   ()    { return  theConstructed->GetMinClusterIndex();   };
    inline G4int  GetMaxClusterIndex   ()    { return  theConstructed->GetMaxClusterIndex();   };
    inline G4int  GetNumberOfSectors   ()    { return  theConstructed->GetNumberOfSectors();   };
    inline G4int  GetNumberOfSlices    ()    { return  theConstructed->GetNumberOfSlices ();   };
    inline G4bool GetReadOut           ()    { return  theConstructed->GetReadOut();	       };

#ifdef GASP
  public:
           G4int  GetNumberOfNeutron   ();
           G4int  GetMaxNeutronIndex   ();

  public:
           G4int  GetNumberOfBgo       ();
           G4int  GetMaxBgoIndex       ();

  public:
           G4int  GetNumberOfSi        ();
           G4int  GetMaxSiIndex        ();
           G4bool IsSiSegmented        ();
#endif
#ifdef CLARA
  public:
           G4int  GetNumberOfMcp       ();
           G4int  GetMaxMcpIndex       ();
#endif
    
  //////////////////////////////////////////////////////////////////////////////////
  /// These methods return useful pointers
  //////////////////////////////////////////////////////////////////////////////////
  public:
    inline G4VPhysicalVolume*       HallPhys          () { return hallPhys;  };
    inline G4LogicalVolume*         HallLog           () { return hallLog;   };
    inline AgataSensitiveDetector*  GeSD              () { return geSD;      };
#ifdef GASP    
   inline AgataDetectorGasp*       GetGaspDetector   () { return newGasp;   };
#else
#ifdef CLARA
    inline AgataDetectorClara*      GetClaraDetector  () { return newClara;  };
#else
#ifdef POLAR
    inline AgataDetectorPolar*      GetPolarDetector  () { return newPolar;  };
    inline AgataDetectorClover*     GetCloverDetector () { return newClover; };
#else
#ifdef EUCLIDES
    inline void SetCalcOmega( G4bool value ) { calcOmega = value; };
#else
    inline AgataDetectorArray*      GetDetectorArray  () { return newArray;  };
#ifndef DEIMOS
    inline AgataDetectorShell*      GetDetectorShell  () { return newShell;  };
    inline AgataDetectorSimple*     GetDetectorSimple () { return newSimple; };
#endif
#endif
#endif
#endif
#endif

  ////////////////////////////////////////////////////////////////////////
  /// These methods return the characteristics of the target
  ///////////////////////////////////////////////////////////////////////
  public:
    inline G4ThreeVector GetTargetPosition () { return targetPosition; };
    inline G4ThreeVector GetTargetSize     () { return targetSize;     };
    
  ////////////////////////////////////////////////////////////////////////
  /// This method is needed to notify AgataEventAction whether a solid
  /// angle calculation is being performed or not
  ///////////////////////////////////////////////////////////////////////
  public:
    inline G4bool        CalcOmega         () { return calcOmega;      };  
    

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithAnInteger;

class AgataDetectorConstructionMessenger: public G4UImessenger
{
  public:
    AgataDetectorConstructionMessenger(AgataDetectorConstruction*, G4bool, G4String);
   ~AgataDetectorConstructionMessenger();
        
  private:
    AgataDetectorConstruction* myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             myADirectory;
    G4UIcmdWithoutParameter*   UpdateCmd;   
    G4UIcmdWithoutParameter*   StatusCmd;   
    G4UIcmdWithAString*        TarMatCmd;
    G4UIcmdWithAString*        ChamMatCmd;
    G4UIcmdWithADouble*        ChamRadiusCmd;
    G4UIcmdWithADouble*        PipeRadiusCmd;
    G4UIcmdWithAString*        RotateChamberCmd;
    G4UIcmdWithADouble*        WalThickCmd;
    G4UIcmdWith3Vector*        SetSizeCmd;
    G4UIcmdWith3Vector*        SetPositionCmd;
    G4UIcmdWithAString*        CalcDistGeCmd;
    G4UIcmdWithAString*        CalcVolCmd;
    G4UIcmdWithAnInteger*      CalcOmegaCmd;
    
  private:
    G4bool volume;

  public:
    void SetNewValue(G4UIcommand*, G4String);
            
};


#endif
