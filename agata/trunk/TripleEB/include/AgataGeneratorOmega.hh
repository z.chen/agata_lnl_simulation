//////////////////////////////////////////////////////////////
/// This class is used only for solid angle calculation.
/// Geantinos are emitted from the origin with an isotropical
/// distribution.
///
//////////////////////////////////////////////////////////////

#ifndef AgataGeneratorOmega_h
#define AgataGeneratorOmega_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"

using namespace std;

class G4ParticleGun;
class G4Event;
class AgataDetectorConstruction;

class AgataGeneratorOmega : public G4VUserPrimaryGeneratorAction
{
  public:
    AgataGeneratorOmega();    
   ~AgataGeneratorOmega();

  private:
    G4ParticleGun*              particleGun;
    
  public:
    ////////////////////////////////////////////////////////////
    /// This method is needed by G4VUserPrimaryGeneratorAction
    ////////////////////////////////////////////////////////////
    void GeneratePrimaries(G4Event*);

};

#endif
