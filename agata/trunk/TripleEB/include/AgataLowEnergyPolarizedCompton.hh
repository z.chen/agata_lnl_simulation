//////////////////////////////////////////////////////////////////////////////////////
/// This class handles the polarized Compton scattering in a similar way as the
/// "low-energy" electromagnetic processes. It looks like the default treatment
/// provided by the Geant4 distribution is not correct!!!
/// Written and mantained by Dino Bazzacco.
//////////////////////////////////////////////////////////////////////////////////////

#ifndef G4LOWENERGYPOLARIZEDCOMPTON_H
#define G4LOWENERGYPOLARIZEDCOMPTON_H 1

#include "globals.hh"
#include "G4VDiscreteProcess.hh"

class G4Track;
class G4Step;
class G4ParticleDefinition;
class G4VParticleChange;
class G4VEMDataSet;
class G4VCrossSectionHandler;
class G4VRangeTest;

class AgataLowEnergyPolarizedCompton : public  G4VDiscreteProcess
{  
public:  
  
  AgataLowEnergyPolarizedCompton(const G4String& processName = "polarLowEnCompt");
  
  ~AgataLowEnergyPolarizedCompton();

  G4bool IsApplicable(const G4ParticleDefinition& definition);
  
  void BuildPhysicsTable(const G4ParticleDefinition& photon);
  G4VParticleChange* PostStepDoIt(const G4Track& aTrack, const G4Step& aStep);
  
  
  // For testing purpose only
  G4double DumpMeanFreePath(const G4Track& aTrack, 
			    G4double previousStepSize, 
			    G4ForceCondition* condition) 
  { return GetMeanFreePath(aTrack, previousStepSize, condition); }

protected:
  
  G4double GetMeanFreePath(const G4Track& aTrack, 
			   G4double previousStepSize, 
			   G4ForceCondition* condition);

private:

  // Hide copy constructor and assignment operator as private 

  AgataLowEnergyPolarizedCompton& operator=(const AgataLowEnergyPolarizedCompton& right);
  AgataLowEnergyPolarizedCompton(const AgataLowEnergyPolarizedCompton& );
  
  G4double lowEnergyLimit;  // low energy limit  applied to the process
  G4double highEnergyLimit; // high energy limit applied to the process
  
  G4VEMDataSet* meanFreePathTable;
  G4VEMDataSet* scatterFunctionData;

  G4VCrossSectionHandler* crossSectionHandler;
  G4VRangeTest* rangeTest;

  const G4double intrinsicLowEnergyLimit; // intrinsic validity range
  const G4double intrinsicHighEnergyLimit;

public: 
  void FirstThetaSet(G4double egamma, G4double theta)
    { fixTh = true; valEg = egamma; valTh = theta; }
  void FirstThetaSet(G4double egamma, G4double thetaMin, G4double /*thetaMax*/)
    { fixTh = true; valEg = egamma; valTh = thetaMin; }	// to be completed
  void FirstThetaReset()
    { fixTh = false; }

private:
  bool      fixTh;
  G4double  valEg;
  G4double  valTh;

};

#endif
 







