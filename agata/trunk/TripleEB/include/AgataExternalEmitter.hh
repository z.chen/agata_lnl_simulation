///////////////////////////////////////////////////////////////////////////////////
/// This class, inheriting from AgataEmitter, handles the position and the
/// velocity of the source for the case of a nuclear reaction. A nucleus with
/// charge zBeam, mass number aBeam is fired with energy eBeam on a nucleus with
/// charge aTarg, mass number aTarg. The position and velocity of the source
/// are calculated from such information, including the conservation of
/// energy and momentum following particle emission.
///////////////////////////////////////////////////////////////////////////////////

#ifndef AgataExternalEmitter_h
#define AgataExternalEmitter_h 1

#include "AgataEmitter.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"

using namespace std;

class AgataExternalEmitterMessenger;

class AgataExternalEmitter : protected AgataEmitter
{
  public:
    AgataExternalEmitter( G4String );
    AgataExternalEmitter( G4int, G4int, G4int, G4int, G4String );
    AgataExternalEmitter( G4int, G4int, G4int, G4int, G4double, G4String );
    ~AgataExternalEmitter();
   
  private:
    AgataExternalEmitterMessenger* myMessenger; 
    
  ////////////////////
  /// Beam nucleus ///
  ////////////////////
  private:
    G4int         aBeam;            //> mass number
    G4int         zBeam;            //> charge number
    G4double      mBeam;            //> mass
    G4double      eBeam;            //> energy
    G4double      sigmaEBeamFrac;   //> fractional dispersion of the beam energy

  //////////////////////
  /// Target nucleus ///
  //////////////////////
  private:
    G4int         aTarg;             //> mass number
    G4int         zTarg;	     //> charge number
    G4double      mTarg;  	     //> mass

  //////////////////////////
  /// "Compound" nucleus ///
  //////////////////////////
  private:
    G4int         aTota;             //> mass number    
    G4int         zTota;	     //> charge number  
    G4double      mTota;  	     //> mass	        

  //////////////////////////
  /// "Residual" nucleus ///
  //////////////////////////
  private:
    G4int         emitterMassNum;    //> mass number    
    G4int         emitterAtomNum;    //> charge number  
    G4double      emitterMass;       //> mass	        
    G4ThreeVector emitterMomentum;   //> momentum
    G4double      emitterEnergy;     //> energy

  /////////////////////////
  /// Private methods
  /////////////////////////
  
  private:
    void          InitReaction       ();  //> retrieves masses of beam, target and "compound" nucleus
    void          InitBeta           ();  //> calculates source velocity from the reaction
    void          UpdateTotalNucleus ();  //> retrieves the mass of the "compound" (total) nucleus
    
  ////////////////////////
  /// Service methods 
  ///////////////////////
  private:
    G4double      BetaFromMomentum     ( G4double, G4double );
    G4double      MomentumFromEnergy   ( G4double, G4double );
    G4double      EnergyFromMomentum   ( G4double, G4ThreeVector );
    G4double      EnergyFromMomentum   ( G4double, G4double );
    G4ThreeVector VelocityFromMomentum ( G4double, G4ThreeVector );
    G4ThreeVector MomentumFromVelocity ( G4double, G4ThreeVector );

  private:
    void          setTotalNucleus      ( G4int, G4int );   //> sets Z, A of the "compound" nucleus
 
  ///////////////////////
  /// Public methods 
  //////////////////////
  
  ////////////////////////
  /// Begin/end of run
  ///////////////////////
  public:
    void          BeginOfRun  ();  
    void          EndOfRun    ();  

  ////////////////////////
  /// Begin/end of event
  ///////////////////////
  public:
    void          BeginOfEvent();  
    void          EndOfEvent  ();  

  ///////////////////////////////////////////////////////////////////////////////
  /// RestartEmitter: source position and velocity at the beginning of an event
  ///////////////////////////////////////////////////////////////////////////////
  public:
    void          RestartEmitter ( G4int, G4int, G4double, G4ThreeVector, G4ThreeVector ); 
    void          RestartEmitter ( G4int, G4int, G4double, G4ThreeVector );		   
    void          RestartEmitter ( G4int, G4int, G4double );				   
    void          RestartEmitter ( G4int, G4int );					   
    void          RestartEmitter ();							   
  
  ///////////////////////////////////////////////////////////////////////////////////////////
  /// UpdateEmitter: recalculates source position and velocity following particle emission
  ///////////////////////////////////////////////////////////////////////////////////////////
  public:
    void          UpdateEmitter( G4int, G4int  );
    void          UpdateEmitter( G4int, G4int, G4ThreeVector );

  public:
    void          SetEmitterPosition ( G4ThreeVector );
    
  public:
    void          GetStatus          ( G4int );
    void          PrintToFile        ( G4int, std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV );

  //////////////////////////////////
  /// Methods for the messenger
  //////////////////////////////////
  public:
    void          SetBeamNucleus     ( G4int, G4int );
    void          SetTargetNucleus   ( G4int, G4int );
    void          SetBeamEnergy      ( G4double );
    void          SetEnerSigma       ( G4double );
    
  ///////////////////////////
  /// Inline "get" methods
  ///////////////////////////
  public:
    inline G4int         GetEmitterMassNum()  { return emitterMassNum;  };
    inline G4int         GetEmitterAtomNum()  { return emitterAtomNum;  };
    inline G4ThreeVector GetEmitterMomentum() { return emitterMomentum; };
    inline G4double      GetEmitterMass()     { return emitterMass;     };
    inline G4double      GetEmitterEnergy()   { return emitterEnergy;   };
    
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWithAString;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcmdWithoutParameter;

class AgataExternalEmitterMessenger: public G4UImessenger
{
  public:
    AgataExternalEmitterMessenger(AgataExternalEmitter*,G4String);
   ~AgataExternalEmitterMessenger();
    
  private:
    AgataExternalEmitter*      myTarget;

  private:
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAnInteger*      SetSourceCmd;
    G4UIcmdWithADouble*        SetBetaCmd;
    G4UIcmdWithADouble*        SetEnergyCmd;
    G4UIcmdWithADouble*        SetEnerSigmaCmd;
    G4UIcmdWithADouble*        SetAngleCmd;
    G4UIcmdWith3Vector*        SetPositionCmd;
    G4UIcmdWith3Vector*        SetRecoilCmd;
    G4UIcmdWithAString*        SetThPhiCmd;
    G4UIcmdWithAString*        SetTargetNucleusCmd;
    G4UIcmdWithAString*        SetBeamNucleusCmd;
    G4UIcmdWithABool*          GaussAngleCmd;
    G4UIcmdWithABool*          UnifoAngleCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};



#endif
