20/05/2008 Fixed remaining problems with latest ancillary detectors (Aida, Helena, Cassandra)
	   Revised numbering to cope (about time!) with actual Demonstrator

30/04/2009 Fixed problem with /Agata/file/info/disableAll behaviour
           Fixed problem with latest ancillary detectors (Aida, Helena, Cassandra)
	   Implemented asymmetric guard ring for "planar" detectors
	   Revised on-line analysis with possibility of writing out
	   both spectra and events in reduced Gasp format.

08/04/2009 Fixed problem with phi range in AgataEmitted
           Revised definition of several materials used for neutron detection
	   Modified construction of the array to include "planar" and Clover detectors
	   (handled with the same AgataDetectorArray class).
	   Revised geometry files/detector numbering, compatible with
	   actual Demonstrator installation in LNL: Demonstrator
	   detectors are placed backwards. Fixes problem with phi angle.
	   Added implementation for Aida and Cassandra ancillary detectors
	   (ids number 17 and 16). Gaspard takes id#15.
	   Removed several #ifdef constructs, manu name distributed by main function
	   as argument in class construction.
	   Fixed bug in Physics List preventing use of standard EM interactions.
	   Added possibility to use separately standard EM/low-energy EM for
	   gammas/hadrons.

26/06/2008 Added a few materials for ancillary detectors
           New geometry files/detector numbering, compatible with
	   actual Demonstrator installation in LNL: Demonstrator
	   detectors are placed backwards.

11/02/2008 Added implementation for Diamant and revised treatment
           for RFD (both provided by Grzegorz Jaworski,
	   tatrofil@slcj.uw.edu.pl)

21/12/2007 Fixed bug with AgataAncillaryShell.

22/11/2007 Fixed bug with uninitialized numAncSd variable in most
           of the implementations of specific ancillary detectors.
	   Added BaF2 optional material. Minor revision of the
	   names of the compilation options. Revised numbering
	   for the ancillary detectors:
	   
        	       id        offset
	   Koeln       1           1000
	   Shell       2 (default) 2000
	   Mcp         3           3000
	   Euclides    4           4000, 5000
	   Brick       6           6000 
	   NWall       7           7000
	   DIAMANT     8           8000         (future implementation)
	   EXOGAM      9           9000         (future implementation)
	   Helena     10          10000
	   RFD        11          11000
	   Trace      12          12000, 13000  (future implementation)
	   CUP        14          14000         (future implementation)

	   
24/09/2007 General revision to remove warnings related to unused
           variables and/or to "C" I/O style. G4LECS treatment of
	   low-energy Compton scattering is now optional (need to
	   add the corresponding source files to the distribution).
	   The "standard" Geant4 libraries can be used as they are,
	   save for known (solved) bugs:
	   - geant v 4.7.0: bug with G4IntersectionSolid (solved
	     in geant v.4.7.1)
	   - geant v.4.9.0: bug with G4BoundingBox3D (solved
	     in geant v.4.9.0.p01) 

26/07/2007 Minor change concerning input of polarization via Stokes parameters
           (consistency is now checked)
23/07/2007 Code now compiles and run with CLHEP 2.X; minor changes were
           needed with Agata.cc, AgataEmitted.cc, AgataAncillaryNeutronWall.cc
16/07/2007 Code now compiles and run successfully with geant v. 4.9.0
04/07/2007 Minor changes to AgataVisManager to cope with geant v.4.9.0.
           Code compiles but does not work (bugs with geant4?)
28/02/2007 Modified a few files to cope with new external project. No changes
           made to the bulk code.
05/01/2007 Discovered that mgs reference frame was changed. Modified MGS_TRASL accordingly
           (AgataEventAction.cc).
21/11/2006 Modified slightly the construction of the target. Its size is
           now given in mg/cm2.
10/11/2006 Modified the construction of the scattering chamber
03/11/2006 Modified slightly the format of the input event file
31/10/2006 Small changes to AgataDetectorAncillary to cope with Clara and Gasp simulations
26/10/2006 Added a couple of extra materials for the ancillary detectors.
           Symmeuler.list has now the target-detector distance used in Koeln
	   last year.
12/10/2006 Fixed bug with the ISIS geometry. Modified also AgataDetectorAncillary accordingly.
