#include "AgataPhysicsList.hh"

#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4LossTableManager.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4ios.hh"


AgataPhysicsList::AgataPhysicsList(G4String name, G4bool hadr, G4bool lowE, G4bool lowEH, G4bool polar, G4bool lecs ):  G4VUserPhysicsList()
{
  hadrons = hadr;
  lowEner = lowE;
  lowEnHa = lowEH;
  usePola = polar;
  useLECS = lecs;

  G4LossTableManager::Instance();
  
  defaultCutValue = 0.1*mm;
  cutForGamma     = defaultCutValue;
  cutForElectron  = defaultCutValue;
  cutForPositron  = defaultCutValue;
  cutForProton    = defaultCutValue;
  cutForNeutron   = defaultCutValue;
  
  SetVerboseLevel(1);

  myMessenger = new AgataPhysicsListMessenger(this, name, hadrons);
}

AgataPhysicsList::~AgataPhysicsList(){
  delete myMessenger;
}

////////////////////////////////////////////////////////////////
// In this method, static member functions are called
// for all particles which are used.
// This ensures that objects of these particle types will be
// created in the program.
////////////////////////////////////////////////////////////////
#ifdef G4V48
#include "G4BosonConstructor.hh"
#include "G4LeptonConstructor.hh"
#include "G4MesonConstructor.hh"
#include "G4BosonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"
#include "G4ShortLivedConstructor.hh"
void AgataPhysicsList::ConstructParticle()
{
  G4BosonConstructor  pBosonConstructor;
  pBosonConstructor.ConstructParticle();

  G4LeptonConstructor pLeptonConstructor;
  pLeptonConstructor.ConstructParticle();

  G4MesonConstructor pMesonConstructor;
  pMesonConstructor.ConstructParticle();

  G4BaryonConstructor pBaryonConstructor;
  pBaryonConstructor.ConstructParticle();

  G4IonConstructor pIonConstructor;
  pIonConstructor.ConstructParticle();

  G4ShortLivedConstructor pShortLivedConstructor;
  pShortLivedConstructor.ConstructParticle();  
  
  G4Geantino::GeantinoDefinition();
}
#else
void AgataPhysicsList::ConstructParticle()
{
  G4Gamma   ::GammaDefinition();
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();

  if(hadrons) {
    G4Neutron ::NeutronDefinition();
    G4AntiNeutron::AntiNeutronDefinition();
    G4Proton  ::ProtonDefinition();
    G4AntiProton::AntiProtonDefinition();
    //  Ions
    G4Deuteron::DeuteronDefinition();
    G4Triton::TritonDefinition();
    G4He3::He3Definition();
    G4Alpha::AlphaDefinition();
    G4GenericIon::GenericIonDefinition();
  }
  G4Geantino::GeantinoDefinition();
}
#endif

void AgataPhysicsList::ConstructProcess()
{
  AddTransportation();
  ConstructEMStandard();
  ConstructGeneral();
  if(hadrons) {
    ConstructHadronsEMStandard();
    ConstructHadronsEMStopp();
    ConstructHadronsElastic();
    ConstructHadronsInelastic();
  }
  ConstructAdditionalProcesses();  
}

//////////////////////////////////////////////////////////
/// Inclusion of header files for the classes describing
/// the required processes.
///////////////////////////////////////////////////////
///
/// gamma
///////////////////////////////////////////////////////
/// standard
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4PhotoElectricEffect.hh"
#include "AgataPolarizedComptonScattering.hh"
/// low energy
#include "G4LowEnergyCompton.hh"
#include "AgataLowEnergyPolarizedCompton.hh"
#ifdef G4V47
#include "G4LowEnergyPolarizedRayleigh.hh"
#endif
#include "G4LowEnergyGammaConversion.hh"
#include "G4LowEnergyPhotoElectric.hh"
#include "G4LowEnergyRayleigh.hh"
/// lecs (Compton profile)
#ifdef G4LECS
#include "G4LECSCompton.hh"
#include "G4LECSRayleigh.hh"
#endif

//////////////////////////////////////////
/// all charged particles
/////////////////////////////////////////
#include "G4eMultipleScattering.hh"
#include "G4hMultipleScattering.hh"
#include "G4MuMultipleScattering.hh"
///////////////////////////////////////
/// e- e+
//////////////////////////////////////
/// standard
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"
/// low energy
#include "G4LowEnergyBremsstrahlung.hh"
#include "G4LowEnergyIonisation.hh"

///////////////////////////////////////////////////////////////////////////
/// This method registers the electromagnetic processes for gamma, e-, e+
///////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructEMStandard()
{
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
    G4String particleType = particle->GetParticleType();
    if (particleName == "gamma") { //> gamma
      G4VProcess* theGammaConversion;
      G4VProcess* thePhotoElectricEffect;
      G4VProcess* theComptonScattering;
      G4VProcess* theRayleighScattering = NULL;
      if( !lowEner ) {
        G4cout << " Using standard EM interactions for photons" << G4endl;
        theGammaConversion     = new G4GammaConversion;
        thePhotoElectricEffect = new G4PhotoElectricEffect;
	if( usePola )
          theComptonScattering   = new AgataPolarizedComptonScattering;
	else
          theComptonScattering   = new G4ComptonScattering;
      }
      else {
        G4cout << " Using low-energy EM interactions for photons" << G4endl;
        theGammaConversion     = new G4LowEnergyGammaConversion;
        thePhotoElectricEffect = new G4LowEnergyPhotoElectric;
        // NB polarization overrides LECS (polarization -->no LECS )
        if( !usePola ) {
          if( useLECS ) {
#ifdef G4LECS
            G4cout << " Using G4LECS package" << G4endl;
            theComptonScattering   = new G4LECSCompton;
            theRayleighScattering  = new G4LECSRayleigh;
#else
            theComptonScattering   = new G4LowEnergyCompton;
            theRayleighScattering  = new G4LowEnergyRayleigh;
#endif	    
          }  
          else { 
            theComptonScattering   = new G4LowEnergyCompton;
            theRayleighScattering  = new G4LowEnergyRayleigh;
          }   
        }    
        else {
          G4cout << " Using polarized photons" << G4endl;
          theComptonScattering   = new AgataLowEnergyPolarizedCompton;
#ifdef G4V47
          theRayleighScattering  = new G4LowEnergyPolarizedRayleigh;
#else
          theRayleighScattering  = new G4LowEnergyRayleigh;
#endif
        }
      }
      // add processes
      pmanager->AddDiscreteProcess(theGammaConversion);
      pmanager->AddDiscreteProcess(theComptonScattering);      
      pmanager->AddDiscreteProcess(thePhotoElectricEffect);
      if( lowEner )
        pmanager->AddDiscreteProcess(theRayleighScattering);
    } 
    else if (particleName == "e-") { //> electron
      G4VProcess* theeminusMultipleScattering = new G4eMultipleScattering;
      G4VProcess* theeminusIonisation;        
      G4VProcess* theeminusBremsstrahlung;    
      
      if( !lowEner ) {
        theeminusIonisation         = new G4eIonisation;
        theeminusBremsstrahlung     = new G4eBremsstrahlung;
      }
      else {
        theeminusIonisation         = new G4LowEnergyIonisation;
        theeminusBremsstrahlung     = new G4LowEnergyBremsstrahlung;
      }
      // add processes
      pmanager->AddProcess(theeminusMultipleScattering);
      pmanager->AddProcess(theeminusIonisation);
      pmanager->AddProcess(theeminusBremsstrahlung);
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeminusIonisation,         idxAlongStep,2);
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeminusMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(theeminusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeminusBremsstrahlung,     idxPostStep,3);

    } 
    else if (particleName == "e+") {  //> positron
      G4VProcess* theeplusMultipleScattering = new G4eMultipleScattering;
      G4VProcess* theeplusAnnihilation       = new G4eplusAnnihilation;
      G4VProcess* theeplusIonisation         = new G4eIonisation;    
      G4VProcess* theeplusBremsstrahlung     = new G4eBremsstrahlung;
      // add processes
      pmanager->AddProcess(theeplusMultipleScattering);
      pmanager->AddProcess(theeplusIonisation);
      pmanager->AddProcess(theeplusBremsstrahlung);
      pmanager->AddProcess(theeplusAnnihilation);
      // set ordering for AtRestDoIt
      pmanager->SetProcessOrderingToFirst(theeplusAnnihilation, idxAtRest);
      // set ordering for AlongStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxAlongStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxAlongStep,2);
      // set ordering for PostStepDoIt
      pmanager->SetProcessOrdering(theeplusMultipleScattering, idxPostStep,1);
      pmanager->SetProcessOrdering(theeplusIonisation,         idxPostStep,2);
      pmanager->SetProcessOrdering(theeplusBremsstrahlung,     idxPostStep,3);
      pmanager->SetProcessOrdering(theeplusAnnihilation,       idxPostStep,4);
    }
  }
}

////////////////////////////////////////////////////////////////////////////
/// This method registers multiple scattering processes for charged hadrons
////////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructHadronsEMStandard()
{
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    if( particle->GetPDGCharge() == 0. ) continue;

    if( particle->GetParticleName() == "e-" ) continue; //> multiple scattering for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" ) continue;

    G4ProcessManager* pmanager = particle->GetProcessManager();

    G4hMultipleScattering* aMultipleScattering = new G4hMultipleScattering;
    pmanager->AddProcess(aMultipleScattering, -1, 1, 1);
  }
}

#include "G4hIonisation.hh"
#include "G4ionIonisation.hh"

// alpha and GenericIon and deuterons, triton, He3:
#include "G4hLowEnergyIonisation.hh"
#include "G4EnergyLossTables.hh"
// hLowEnergyIonisation uses Ziegler 1988 as the default

//////////////////////////////////////////////////////////////////////////
/// This method registers stopping power EM processes for hadrons
//////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructHadronsEMStopp()
{

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    
    if( particle->GetPDGCharge() == 0. ) continue;

    if( particle->GetParticleName() == "e-" ) continue; // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" ) continue;
    
    G4ProcessManager* pmanager = particle->GetProcessManager();

    // low energy: all hadrons use G4hLowEnergyIonisation
    // but the call to SetElectronicStoppingPowerModel depends
    // on the particle type
    if( lowEnHa ) {
      G4cout << " Using low-energy EM interactions for hadrons" << G4endl;
      G4hLowEnergyIonisation* ahadronLowEIon = new G4hLowEnergyIonisation;
      pmanager->AddProcess(ahadronLowEIon, -1, 2, 2 );
      ahadronLowEIon->SetNuclearStoppingOn();
      if (particle->GetParticleName() == "proton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Proton::ProtonDefinition(),         "ICRU_R49p");    
      else if (particle->GetParticleName() == "antiproton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4AntiProton::AntiProtonDefinition(), "ICRU_R49p");    
      else if (particle->GetParticleName() == "deuteron")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Deuteron::DeuteronDefinition(),     "ICRU_R49p");    
      else if (particle->GetParticleName() == "triton")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Triton::TritonDefinition(),         "ICRU_R49p");    
      else if (particle->GetParticleName() == "He3")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4He3::He3Definition(),               "ICRU_R49p");    
      else if (particle->GetParticleName() == "alpha")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4Alpha::AlphaDefinition(),           "ICRU_R49p");    
      else if (particle->GetParticleName() == "GenericIon")  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");    
      else if (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");
      else if ((!particle->IsShortLived()) && (particle->GetPDGCharge() != 0.0))  
        ahadronLowEIon->SetElectronicStoppingPowerModel(G4GenericIon::GenericIonDefinition(), "ICRU_R49p");
        
      ahadronLowEIon->SetNuclearStoppingPowerModel("ICRU_R49");      
    }
    else {
      G4cout << " Using standard EM interactions for hadrons" << G4endl;
      // standard EM: proton, deuteron and triton use G4hIonisation
      // the other ions use G4ionIonisation
      if (particle->GetParticleName() == "alpha"      ||
          particle->GetParticleName() == "He3"        ||
          particle->GetParticleName() == "GenericIon" || 
         (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)  ) {
        G4ionIonisation* aIonIonisation = new G4ionIonisation; 
        pmanager->AddProcess(aIonIonisation, -1, 2, 2);
      }
 // 31/03/09: apparently there is some problem with the stopping powers for
 //           other particles (the  !particle->IsShortLived()  condition)
 //           Is that a real issue for us? Unless we go to energies opening pion production or similar ...   
/*      else if ( particle->GetParticleName() == "proton"   ||
                particle->GetParticleName() == "deuteron" ||
                particle->GetParticleName() == "triton"   ||
	       !particle->IsShortLived()  ) {
        G4hIonisation* ahadronIonisation = new G4hIonisation;
        pmanager->AddProcess(ahadronIonisation, -1, 2, 2);
      }*/
      else if ( particle->GetParticleName() == "proton"   ||
                particle->GetParticleName() == "deuteron" ||
                particle->GetParticleName() == "triton" ) {
        G4hIonisation* ahadronIonisation = new G4hIonisation;
        pmanager->AddProcess(ahadronIonisation, -1, 2, 2);
      }
    }
  }
}

/////////////////////////////////////////////////////////////
/// Hadronic processes
////////////////////////////////////////////////////////////// 
/// Elastic processes:
#include "G4HadronElasticProcess.hh"

// Inelastic processes:
#include "G4ProtonInelasticProcess.hh"
#include "G4AntiProtonInelasticProcess.hh"
#include "G4NeutronInelasticProcess.hh"
#include "G4AntiNeutronInelasticProcess.hh"
#include "G4DeuteronInelasticProcess.hh"
#include "G4TritonInelasticProcess.hh"
#include "G4AlphaInelasticProcess.hh"

// Low-energy Models: < 20GeV
#include "G4LElastic.hh"
#include "G4LEProtonInelastic.hh"
#include "G4LEAntiProtonInelastic.hh"
#include "G4LENeutronInelastic.hh"
#include "G4LEAntiNeutronInelastic.hh"
#include "G4LEDeuteronInelastic.hh"
#include "G4LETritonInelastic.hh"
#include "G4LEAlphaInelastic.hh"

// High-energy Models: >20 GeV
#include "G4HEProtonInelastic.hh"
#include "G4HEAntiProtonInelastic.hh"
#include "G4HENeutronInelastic.hh"
#include "G4HEAntiNeutronInelastic.hh"

// Neutron high-precision models: <20 MeV
#include "G4NeutronHPElastic.hh"
#include "G4NeutronHPElasticData.hh"
#include "G4NeutronHPCapture.hh"
#include "G4NeutronHPCaptureData.hh"
#include "G4NeutronHPInelastic.hh"
#include "G4NeutronHPInelasticData.hh"
#include "G4LCapture.hh"

// Stopping processes
#include "G4AntiProtonAnnihilationAtRest.hh"
#include "G4AntiNeutronAnnihilationAtRest.hh"

// Decays 
#include "G4Decay.hh"
#include "G4RadioactiveDecay.hh"
#include "G4IonTable.hh"
#include "G4Ions.hh"

//////////////////////////////////////////////////////////////
/// This method registers the elastic scattering processes
/// for hadrons
//////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructHadronsElastic()
{
  G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
  G4LElastic* theElasticModel = new G4LElastic;
  theElasticProcess->RegisterMe(theElasticModel);

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();

    if( particle->GetParticleName() == "gamma" ) continue;
    if( particle->GetParticleName() == "e-" )    continue; // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" )    continue;

    G4ProcessManager* pManager = particle->GetProcessManager();

    if( particle->GetParticleName() == "neutron" ) {
      // elastic scattering						         
      G4HadronElasticProcess* theNeutronElasticProcess = new G4HadronElasticProcess;					         
      G4LElastic* theElasticModel1 = new G4LElastic;			         
      G4NeutronHPElastic * theElasticNeutron = new G4NeutronHPElastic;           
      theNeutronElasticProcess->RegisterMe(theElasticModel1);		         
      theElasticModel1->SetMinEnergy(19*MeV);				         
      theNeutronElasticProcess->RegisterMe(theElasticNeutron);  	         
      G4NeutronHPElasticData * theNeutronData = new G4NeutronHPElasticData;	 
      theNeutronElasticProcess->AddDataSet(theNeutronData);		         
      pManager->AddDiscreteProcess(theNeutronElasticProcess);		         
    }
//    else if ( !particle->IsShortLived() ) { 
    else if ( particle->GetParticleName() == "anti_neutron" ||
              particle->GetParticleName() == "proton"       ||
              particle->GetParticleName() == "anti_proton"  ||
              particle->GetParticleName() == "deuteron"     ||
              particle->GetParticleName() == "triton"       ||
              particle->GetParticleName() == "alpha"        || 
              particle->GetParticleName() == "He3"          ||
              particle->GetParticleName() == "GenericIon"   || 
             (particle->GetParticleType() == "nucleus" && particle->GetPDGCharge() != 0.)) {
      pManager->AddDiscreteProcess(theElasticProcess);
    }
  }
}

//////////////////////////////////////////////////////////////
/// This method registers the inelastic scattering processes
/// for hadrons
//////////////////////////////////////////////////////////////
#include "G4HadronCaptureProcess.hh"
void AgataPhysicsList::ConstructHadronsInelastic()
{

  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();

    if( particle->GetParticleName() == "gamma" ) continue;
    if( particle->GetParticleName() == "e-" )    continue; // processes for e+/e- has been already registered
    if( particle->GetParticleName() == "e+" )    continue;

    G4ProcessManager* pmanager = particle->GetProcessManager();

    if(particle->GetParticleName() == "proton") {
      G4ProtonInelasticProcess* theInelasticProcess = new G4ProtonInelasticProcess("inelastic");
      G4LEProtonInelastic* theLEInelasticModel = new G4LEProtonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEProtonInelastic* theHEInelasticModel = new G4HEProtonInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    } 
    else if(particle->GetParticleName() == "anti_proton") {
      G4AntiProtonInelasticProcess* theInelasticProcess = new G4AntiProtonInelasticProcess("inelastic");
      G4LEAntiProtonInelastic* theLEInelasticModel = new G4LEAntiProtonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEAntiProtonInelastic* theHEInelasticModel = new G4HEAntiProtonInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    } 
    else if(particle->GetParticleName() == "neutron") {
      // inelastic scattering
      G4NeutronInelasticProcess* theInelasticProcess = new G4NeutronInelasticProcess("inelastic");
      G4LENeutronInelastic* theInelasticModel = new G4LENeutronInelastic;
      theInelasticModel->SetMinEnergy(19*MeV);
      theInelasticProcess->RegisterMe(theInelasticModel);
      G4NeutronHPInelastic * theLENeutronInelasticModel = new G4NeutronHPInelastic;
      theInelasticProcess->RegisterMe(theLENeutronInelasticModel);
      G4NeutronHPInelasticData * theNeutronData1 = new G4NeutronHPInelasticData;
      theInelasticProcess->AddDataSet(theNeutronData1);
      pmanager->AddDiscreteProcess(theInelasticProcess);

      // capture
      G4HadronCaptureProcess* theCaptureProcess = new G4HadronCaptureProcess;
      G4LCapture* theCaptureModel = new G4LCapture;
      theCaptureModel->SetMinEnergy(19*MeV);
      theCaptureProcess->RegisterMe(theCaptureModel);
      G4NeutronHPCapture * theLENeutronCaptureModel = new G4NeutronHPCapture;
      theCaptureProcess->RegisterMe(theLENeutronCaptureModel);
      G4NeutronHPCaptureData * theNeutronData3 = new G4NeutronHPCaptureData;
      theCaptureProcess->AddDataSet(theNeutronData3);
      pmanager->AddDiscreteProcess(theCaptureProcess);
    } 
    else if(particle->GetParticleName() == "anti_neutron") {
      G4AntiNeutronInelasticProcess* theInelasticProcess = new G4AntiNeutronInelasticProcess("inelastic");
      G4LEAntiNeutronInelastic* theLEInelasticModel = new G4LEAntiNeutronInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      G4HEAntiNeutronInelastic* theHEInelasticModel = new G4HEAntiNeutronInelastic;
      theInelasticProcess->RegisterMe(theHEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "deuteron") {
      G4DeuteronInelasticProcess* theInelasticProcess = new G4DeuteronInelasticProcess("inelastic");
      G4LEDeuteronInelastic* theLEInelasticModel = new G4LEDeuteronInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "triton") {
      G4TritonInelasticProcess* theInelasticProcess = new G4TritonInelasticProcess("inelastic");
      G4LETritonInelastic* theLEInelasticModel = new G4LETritonInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);

    } 
    else if(particle->GetParticleName() == "alpha") {
      G4AlphaInelasticProcess* theInelasticProcess = new G4AlphaInelasticProcess("inelastic");
      G4LEAlphaInelastic* theLEInelasticModel = new G4LEAlphaInelastic;
      theInelasticProcess->RegisterMe(theLEInelasticModel);
      pmanager->AddDiscreteProcess(theInelasticProcess);
    }
  }
}


//////////////////////////////////////////////////////////////
/// This method registers the decay processes
/// for hadrons
//////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructGeneral()
{

  // Add Decay Process
  G4Decay* theDecayProcess = new G4Decay();
  theParticleIterator->reset();
  while( (*theParticleIterator)() ) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();

    if (theDecayProcess->IsApplicable(*particle) && !particle->IsShortLived()) { 
      pmanager ->AddProcess(theDecayProcess);
      pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
      pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////
/// In case other processes are needed to plug-in another event generator,
/// they should be registered with this method
//////////////////////////////////////////////////////////////////////////////
void AgataPhysicsList::ConstructAdditionalProcesses()
{}

void AgataPhysicsList::SetCuts()
{
  SetCutsWithDefault();
  // set cut values for gamma at first and for e- second and next for e+,
  // because some processes for e+/e- need cut values for gamma 
  SetCutValue(cutForGamma,    "gamma");
  G4cout << " ----> Cut for gamma is " << cutForGamma/mm << " mm" << G4endl;
  SetCutValue(cutForElectron, "e-");
  G4cout << " ----> Cut for electron is " << cutForElectron/mm << " mm" << G4endl;
  SetCutValue(cutForPositron, "e+");
  G4cout << " ----> Cut for positron is " << cutForPositron/mm << " mm" << G4endl;
  SetCutValue(cutForNeutron,  "neutron");
  G4cout << " ----> Cut for neutron is " << cutForNeutron/mm << " mm" << G4endl;
  SetCutValue(cutForNeutron,  "anti_neutron");
  G4cout << " ----> Cut for antineutron is " << cutForNeutron/mm << " mm" << G4endl;
  
  SetCutValue(cutForProton,   "proton");      
  G4cout << " ----> Cut for proton is " << cutForProton/mm << " mm" << G4endl;
  SetCutValue(cutForProton,   "anti_proton"); 
  G4cout << " ----> Cut for antiproton is " << cutForProton/mm << " mm" << G4endl;
  
  if (verboseLevel>0)
    DumpCutValuesTable();
}

void AgataPhysicsList::SetGammaCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForGamma/mm << " mm" << G4endl;
  }
  else {
    cutForGamma = cut * mm;
    SetCutValue(cutForGamma,    "gamma");
    G4cout << " ----> Cut for gamma has been set to " << cutForGamma/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetElectronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForElectron/mm << " mm" << G4endl;
  }
  else {
    cutForElectron = cut * mm;
    SetCutValue(cutForElectron, "e-");
    G4cout << " ----> Cut for electron has been set to " << cutForElectron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetPositronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForPositron/mm << " mm" << G4endl;
  }
  else {
    cutForPositron = cut * mm;
    SetCutValue(cutForPositron, "e+");
    G4cout << " ----> Cut for positron has been set to " << cutForPositron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetNeutronCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForNeutron/mm << " mm" << G4endl;
  }
  else {
    cutForNeutron = cut * mm;
    SetCutValue(cutForNeutron,  "neutron");
    SetCutValue(cutForNeutron,  "anti_neutron");
    G4cout << " ----> Cut for neutron has been set to " << cutForNeutron/mm << " mm" << G4endl;
  }
}

void AgataPhysicsList::SetProtonCut( G4double cut )
{
  if( cut < 0. ) {
    G4cout << " ----> Invalid value, keeping previous one (" << cutForProton/mm << " mm" << G4endl;
  }
  else {
    cutForProton = cut * mm;
    SetCutValue(cutForProton,   "proton");      
    SetCutValue(cutForProton,   "anti_proton"); 
    G4cout << " ----> Cut for proton has been set to " << cutForProton/mm << " mm" << G4endl;
  }
}


/////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"

AgataPhysicsListMessenger::AgataPhysicsListMessenger(AgataPhysicsList* pTarget, G4String name, G4bool had)
:myTarget(pTarget)
{ 
  hadrons = had;

  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/physics/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of physics list parameters.");

  commandName = directoryName + "gammaCut";
  aLine = commandName.c_str();
  SetGammaCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetGammaCutCmd->SetGuidance("Sets cut for gammas.");
  SetGammaCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetGammaCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "electronCut";
  aLine = commandName.c_str();
  SetElectronCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetElectronCutCmd->SetGuidance("Sets cut for electrons.");
  SetElectronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetElectronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "positronCut";
  aLine = commandName.c_str();
  SetPositronCutCmd = new G4UIcmdWithADouble(aLine, this);
  SetPositronCutCmd->SetGuidance("Sets cut for positrons.");
  SetPositronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
  SetPositronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  if( hadrons ) {
    commandName = directoryName + "neutronCut";
    aLine = commandName.c_str();
    SetNeutronCutCmd = new G4UIcmdWithADouble(aLine, this);
    SetNeutronCutCmd->SetGuidance("Sets cut for neutrons.");
    SetNeutronCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
    SetNeutronCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName + "protonCut";
    aLine = commandName.c_str();
    SetProtonCutCmd = new G4UIcmdWithADouble(aLine, this);
    SetProtonCutCmd->SetGuidance("Sets cut for protons.");
    SetProtonCutCmd->SetGuidance("Required parameters: 1 double (cut distance in mm).");
    SetProtonCutCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
}

AgataPhysicsListMessenger::~AgataPhysicsListMessenger()
{

  delete myDirectory;
  delete SetGammaCutCmd;
  delete SetElectronCutCmd;
  delete SetPositronCutCmd;
  if(hadrons) {
    delete SetNeutronCutCmd;
    delete SetProtonCutCmd;
  }  
}

void AgataPhysicsListMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetGammaCutCmd ) {
    myTarget->SetGammaCut(SetGammaCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetElectronCutCmd ) {
    myTarget->SetElectronCut(SetElectronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositronCutCmd ) {
    myTarget->SetPositronCut(SetPositronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetNeutronCutCmd ) {
    myTarget->SetNeutronCut(SetNeutronCutCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetProtonCutCmd ) {
    myTarget->SetProtonCut(SetProtonCutCmd->GetNewDoubleValue(newValue));
  }
}    
