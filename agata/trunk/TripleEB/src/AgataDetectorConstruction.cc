#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#ifdef GASP
#include "AgataDetectorGasp.hh"
#else
#ifdef CLARA
#include "AgataDetectorClara.hh"
#else
#ifdef POLAR
#include "AgataDetectorPolar.hh"
#include "AgataDetectorClover.hh"
#else
#ifdef EUCLIDES
#include "AgataDetectorEuclides.hh"
#else
#include "AgataDetectorArray.hh"
#ifndef DEIMOS
#include "AgataDetectorShell.hh"
#include "AgataDetectorSimple.hh"
#endif
#endif
#endif
#endif
#endif

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4VPhysicalVolume.hh"

using namespace std;

AgataDetectorConstruction::AgataDetectorConstruction( G4int gtype, G4int atype, G4String path, G4bool vol, G4String name )
{
  char aLine[128];
  printf(aLine, "1 %d", atype);
  this->InitData( gtype, G4String(aLine), path, vol, name ); 
}


AgataDetectorConstruction::AgataDetectorConstruction( G4int gtype, G4String atype, G4String path, G4bool vol, G4String name )
{
  this->InitData( gtype, atype, path, vol, name ); 
}

void AgataDetectorConstruction::InitData( G4int gtype, G4String atype, G4String path, G4bool vol, G4String name )
{
  geomType = gtype;
  ancType  = atype;
  volume   = vol;

  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  calcOmega = false;

#ifdef GASP
  newGasp        = NULL;
#else
#ifdef CLARA
  newClara       = NULL;
#else  
#ifdef POLAR
  newPolar       = NULL;
  newClover      = NULL;
#else  
#ifdef EUCLIDES
  newEuclides    = NULL;
#else
#ifdef DEIMOS
  newArray       = NULL;
#else  
  newArray       = NULL;
  newShell       = NULL;
  newSimple      = NULL;
#endif
#endif
#endif
#endif  
#endif  
  
  theConstructed = NULL;
  
  geSD           = NULL;

  matWorld       = NULL;
  matTarget      = NULL;
  matChamber     = NULL;

  matWorldName   = "Vacuum";
  matTargetName  = "Vacuum";
  matChamberName = "Vacuum";
  
  outRadius      = 10.0*cm; 
  wallThickness  =  2.0*mm; 
  pipeRadius     =  1.8*cm; 
  
  thetaChamb     = 0.;
  phiChamb       = 0.;
  
  targetSize.set( 2.0 *cm, 2.0 *cm, 1.0 *mm ); // full lengths
  targetThickness = 1.0*mg/cm2; 
  targetPosition.set( 0., 0., 0.);
  
  ancillaryOffset = 0;
  
  directoryName = name;
  
  myMessenger = new AgataDetectorConstructionMessenger(this, volume, name);
}


AgataDetectorConstruction::~AgataDetectorConstruction()
{
   delete  myMessenger;
   
   if( volume ) 
     delete [] numHits;          
}

G4VPhysicalVolume* AgataDetectorConstruction::Construct()
{
  DefineMaterials();

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !geSD ) {
    geSD = new AgataSensitiveDetector( directoryName, "/ge/all", "GeCollection" );
    SDman->AddNewDetector( geSD );
  }

  ConstructHall();

#ifdef GASP
  newGasp = new AgataDetectorGasp( geomType, iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newGasp;
#else
#ifdef CLARA
  newClara = new AgataDetectorClara(iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newClara;
#else
#ifdef EUCLIDES
  newEuclides = new AgataDetectorEuclides(iniPath, directoryName);
  theConstructed = (AgataDetectorConstructed*)newEuclides;
#else  
#ifdef POLAR
  switch( geomType )
  {
    case 0:
      newPolar = new AgataDetectorPolar(iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newPolar;
      break;
    case 1:
      newClover = new AgataDetectorClover(iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newClover;
      break;
  }      
#else
#ifdef DEIMOS
  newArray       = new AgataDetectorArray( ancType, iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newArray;
#else
  switch( geomType )
  {
    case 0:
      newArray       = new AgataDetectorArray( ancType, iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newArray;
      break;
    case 1:
      newShell       = new AgataDetectorShell(directoryName);
      theConstructed = (AgataDetectorConstructed*)newShell;
      break;  
    case 2:
      newSimple      = new AgataDetectorSimple(directoryName);
      theConstructed = (AgataDetectorConstructed*)newSimple;
      break;
    default:
      break;  
  }
#endif
#endif
#endif
#endif
#endif
  theConstructed->Placement();
  
  if( volume )
    numHits = new G4int[theConstructed->GetMaxDetectorIndex()+1];
  
  return hallPhys;
  
}

//////////////////////////////////////////////////////////////////////
/// In this method, the materials used by "fancy" ancillary detectors
/// are defined. By default most of the materials are not loaded to 
/// save time at initialization. Notice that also the G4Elements already
/// defined in DefineMaterials() must be redefined here. This method is
/// used only if the ANCIL compilation flag is uncommented.
///////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::DefineAdditionalMaterials()
{
#ifdef ANCIL
  G4double a, z, density;
  G4double fractionmass;
  G4String name, symbol;
  G4int    nelements, natoms, nprot, nnucl;
  G4int    ncomponents;

  std::vector<G4Element*>  myElements;    // save pointers here to avoid
  std::vector<G4Material*> myMaterials;   // warnings of unused variables

  G4Element* elO  = new G4Element(name="Oxigen",   symbol="O",  z=8.,  a= 15.9994 *g/mole);
  myElements.push_back(elO);

  G4Element* elH  = new G4Element(name="Hydrogen", symbol="H",  z=1.,  a= 1.007940*g/mole);
  myElements.push_back(elH);

  G4Element* elC  = new G4Element(name="Carbon",   symbol="C",  z=6.,  a= 12.00000*g/mole);
  myElements.push_back(elC);

  G4Element* elSi = new G4Element(name="Silicon",  symbol="Si", z=14., a= 28.0855*g/mole);
  myElements.push_back(elSi);

  G4Element* elN  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a=14.01*g/mole);
  myElements.push_back(elN);

  // Germanium isotopes
  G4Isotope* Ge70 = new G4Isotope(name="Ge70", nprot=32, nnucl=70, a=69.9242*g/mole);
  G4Isotope* Ge72 = new G4Isotope(name="Ge72", nprot=32, nnucl=72, a=71.9221*g/mole);
  G4Isotope* Ge73 = new G4Isotope(name="Ge73", nprot=32, nnucl=73, a=72.9235*g/mole);
  G4Isotope* Ge74 = new G4Isotope(name="Ge74", nprot=32, nnucl=74, a=73.9212*g/mole);
  G4Isotope* Ge76 = new G4Isotope(name="Ge76", nprot=32, nnucl=76, a=75.9214*g/mole);
  // germanium defined via its isotopes
  G4Element* elGe = new G4Element(name="Germanium",symbol="Ge", nelements=5);
  elGe->AddIsotope(Ge70, 0.2123);
  elGe->AddIsotope(Ge72, 0.2766);
  elGe->AddIsotope(Ge73, 0.0773);
  elGe->AddIsotope(Ge74, 0.3594);
  elGe->AddIsotope(Ge76, 0.0744);
  myElements.push_back(elGe);

  G4Element* elNa = new G4Element(name="Sodium",   symbol="Na", z=11., a= 22.98977*g/mole);
  myElements.push_back(elNa);

  G4Element* elFe = new G4Element(name="Iron",     symbol="Fe", z=26.,  a= 55.845*g/mole);
  myElements.push_back(elFe);
  
  G4Element* elI  = new G4Element(name="Iodine",   symbol="I" , z=53., a=126.90477*g/mole);
  myElements.push_back(elI);

  G4Element* elCs  = new G4Element(name="Cesium",   symbol="Cs" , z=55., a=132.90545*g/mole);
  myElements.push_back(elCs);

  G4Element* elPb = new G4Element(name="Lead",     symbol="Pb", z=82., a=207.2    *g/mole);
  myElements.push_back(elPb);

  G4Element* elBi = new G4Element(name="Bismuth",  symbol="Bi", z=83., a=208.98038*g/mole);
  myElements.push_back(elBi);
  
  G4Element* elLa  = new G4Element(name="Lantanum",  symbol="La", z=57., a=138.9055*g/mole);
  myElements.push_back(elLa);

  G4Element* elBr  = new G4Element(name="Bromine", symbol="Br", z=35., a=79.904*g/mole);
  myElements.push_back(elBr);

  G4Element* elBa  = new G4Element(name="Barium", symbol="Ba",  z=56., a=137.327*g/mole);
  myElements.push_back(elBa);

  G4Element* elF  = new G4Element(name="Fluorine", symbol="F",  z=9., a=18.9984032*g/mole);
  myElements.push_back(elF);

  G4Element* elCe  = new G4Element(name="Cerium", symbol="Ce",  z=58., a=140.115*g/mole);
  myElements.push_back(elBr);
  
  // hydrogen isotopes
  G4Isotope* H2 = new G4Isotope("H2", nprot=1., nnucl=2, a=2.014101*g/mole);
  G4Isotope* H1 = new G4Isotope("H1", nprot=1., nnucl=1, a=1.007940*g/mole);
  G4Element* elD = new G4Element("Deuterium",symbol="D",nelements=2);
  elD->AddIsotope(H2, 99.1228*perCent);  // % Abundance
  elD->AddIsotope(H1,  0.8772*perCent);  // % Abundance
  myElements.push_back(elD);
  

  G4Material* W  = new G4Material(name="Tungsten",  z=74., a=183.850  *g/mole, density=19.3*g/cm3);
  myMaterials.push_back(W);

// Add EnD to BC-537 with D:C ratio 0.99. Get proper ratio of atoms if you
// say there are 99 enD atoms for every 100 Carbon atoms
// Use same definition for Carbon (natural) as given above.
  G4Material* BC537 = new G4Material("BC537", density = 0.954*g/cm3, nelements=2);
  BC537->AddElement(elC, natoms=100);    // Data Sheet - D:C = 0.99
  BC537->AddElement(elD, natoms= 99);
  myMaterials.push_back(BC537);
  
  G4Material* BC501A = new G4Material(name="BC501A", density=0.874*g/cm3, nelements=2);
  BC501A->AddElement(elC, 90.779*perCent); // Note here: 90.779% by weight
  BC501A->AddElement(elH,  9.221*perCent); // Note here:  9.221% by weight
  myMaterials.push_back(BC501A);

  G4Material* BaF2 = new G4Material(name="BaF2", density=4.89*g/cm3, nelements=2);
  BaF2->AddElement(elBa, natoms=1);
  BaF2->AddElement(elF,  natoms=2);
  myMaterials.push_back(BaF2);

  G4Material* Upilex = new G4Material(name="Upilex", density=1.470*g/cm3, ncomponents=4);
  Upilex->AddElement(elH, natoms=6);
  Upilex->AddElement(elO, natoms=4);
  Upilex->AddElement(elC, natoms=16);
  Upilex->AddElement(elN, natoms=2);
  myMaterials.push_back(Upilex);
  
  G4Material* Quarz = new G4Material(name="Quarz", density=2.66*g/cm3, nelements=2);
  Quarz->AddElement(elSi, natoms=1);
  Quarz->AddElement(elO,  natoms=2);
  myMaterials.push_back(Quarz);
  
  G4Material* Epoxy = new G4Material(name="Epoxy", density=1.470*g/cm3, ncomponents=3);
  Epoxy->AddElement(elH, natoms=18);
  Epoxy->AddElement(elO, natoms= 4);
  Epoxy->AddElement(elC, natoms=21);
  myMaterials.push_back(Epoxy);
  
  G4Material* Cu = new G4Material(name="Copper", z=29., a=63.546*g/mole, density=8.920*g/cm3);
  myMaterials.push_back(Cu);
  
  G4Material* FR4 = new G4Material(name="FR4", density=1.670*g/cm3, ncomponents=3);
  FR4->AddMaterial(Quarz, fractionmass=70.*perCent);
  FR4->AddMaterial(Epoxy, fractionmass=22.*perCent);
  FR4->AddMaterial(Cu,    fractionmass= 8.*perCent);
  myMaterials.push_back(FR4);

  G4Material* Pb = new G4Material(name="Lead",      z=82., a=207.2    *g/mole, density=11.342 *g/cm3);
  myMaterials.push_back(Pb);
  
  G4Material* Fe = new G4Material(name="Iron",      z=26., a=55.845   *g/mole, density=7.874*g/cm3);
  myMaterials.push_back(Fe);
  
  G4Material* Steel = new G4Material(name="Steel", density=7.86*g/cm3, nelements=2);
  Steel->AddElement(elFe, fractionmass=98.5*perCent);
  Steel->AddElement(elC,  fractionmass= 1.5*perCent);
  myMaterials.push_back(Steel);

  G4Material* NaI = new G4Material(name="NaI", density=3.67*g/cm3, nelements=2);
  NaI->AddElement(elNa, natoms=1);
  NaI->AddElement(elI , natoms=1);
  myMaterials.push_back(NaI);

  G4Material* CsI = new G4Material(name="CsI", density=4.51*g/cm3, nelements=2);
  CsI->AddElement(elCs, natoms=1);
  CsI->AddElement(elI , natoms=1);
  myMaterials.push_back(CsI);

  G4Material* BGO = new G4Material(name="BGO", density=7.13*g/cm3, nelements=3);
  BGO->AddElement(elBi, natoms=4);
  BGO->AddElement(elGe, natoms=3);
  BGO->AddElement(elO , natoms=12);
  myMaterials.push_back(BGO);
  
  G4Material* plastic = new G4Material(name="Plastic", density=0.900*g/cm3, nelements=2);
  plastic->AddElement(elC, natoms=3);
  plastic->AddElement(elH, natoms=6);
  myMaterials.push_back(plastic);

  //NWall liquid scintillator
  G4Material* liqSci = new G4Material(name="liqSci", density=0.870*g/cm3, nelements=2);
  liqSci->AddElement(elC,natoms= 8);
  liqSci->AddElement(elH,natoms=10);
  myMaterials.push_back(liqSci);

  //material for RFD
  G4Material* Mylar = new G4Material("Mylar", density= 1.397*g/cm3, ncomponents=3);
  Mylar->AddElement(elC, natoms=10);
  Mylar->AddElement(elH, natoms= 8);
  Mylar->AddElement(elO, natoms= 4);
  myMaterials.push_back(Mylar);
  
  //materials for Diamant:
  G4Material* Ta = new G4Material(name="Tantalum", z=73.,a=178.945929*g/mole, density=16.6*g/cm3);
  myMaterials.push_back(Ta);
  
  G4Material* Delrin = new G4Material(name="Delrin", density=1.415*g/cm3, nelements=3);
  Delrin->AddElement(elC, natoms=1);
  Delrin->AddElement(elO, natoms=1);
  Delrin->AddElement(elH, natoms=2);
  myMaterials.push_back(Delrin);
  
  //materials for TRACE
  G4Material* kapton  = new G4Material(name="Kapton", density=1.42*g/cm3, nelements= 4);
  kapton->AddElement(elH, natoms=10);
  kapton->AddElement(elC, natoms=22);
  kapton->AddElement(elN, natoms=2);
  kapton->AddElement(elO, natoms=5);
  myMaterials.push_back(kapton);
  
  G4Material* PCB  = new G4Material(name="Vetronite", density=2.*g/cm3, nelements= 2);//vetro epossidico
  PCB->AddElement(elSi, natoms=1);
  PCB->AddElement(elO , natoms=2); 
  myMaterials.push_back(PCB);
  
  G4Material* LaBr3  = new G4Material(name="LaBr3", density=5.29*g/cm3, nelements= 3);
  LaBr3->AddElement(elLa, natoms= 7);
  LaBr3->AddElement(elBr, natoms=21); 
  LaBr3->AddElement(elCe, natoms= 1); 
  myMaterials.push_back(LaBr3);

  /// new materials for NArray 04/06/2008
  G4Material* Paraffin = new G4Material(name="Paraffin", density=0.9*g/cm3, ncomponents=2);
  Paraffin->AddElement(elH, natoms=52);
  Paraffin->AddElement(elC, natoms=25);
  myMaterials.push_back(Paraffin);

  G4Material* Polypropylene = new G4Material(name="Polypropylene", density=0.855*g/cm3, ncomponents=2);
  Polypropylene->AddElement(elH, natoms=6);
  Polypropylene->AddElement(elC, natoms=3);
  myMaterials.push_back(Polypropylene);


#endif  
}

void AgataDetectorConstruction::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  outFileLMD << "GEOMETRY" << G4endl;
  theConstructed->WriteHeader(outFileLMD,unit);
  outFileLMD << "ENDGEOMETRY" << G4endl;
}

void AgataDetectorConstruction::ShowStatus()
{
  theConstructed->ShowStatus();
  ShowCommonStatus();
}

G4int AgataDetectorConstruction::GetCrystalType( G4int detNum )
{
  return theConstructed->GetCrystalType(detNum);
}

G4int AgataDetectorConstruction::GetSegmentNumber( G4int offset, G4int nGe, G4ThreeVector position )
{
  return theConstructed->GetSegmentNumber(offset, nGe, position);
}

G4int AgataDetectorConstruction::GetAncillaryOffset()
{
  ancillaryOffset += 1000;
  return ancillaryOffset;
}

/////////////////////////////////////////////////////////////////////
/// This method is mandatory to commit any change in the geometry
/////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::UpdateGeometry()
{
  ConstructHall();
  theConstructed->Placement();
  G4RunManager* runManager = G4RunManager::GetRunManager();
  runManager->DefineWorldVolume( hallPhys );
  
  ShowStatus();
}

#ifdef GASP

G4int AgataDetectorConstruction::GetNumberOfNeutron()
{
  return newGasp->GetNumberOfNeutron();
}

G4int AgataDetectorConstruction::GetMaxNeutronIndex()
{
  return newGasp->GetMaxNeutronIndex();
}

G4int AgataDetectorConstruction::GetNumberOfBgo()
{
  return newGasp->GetNumberOfBgo();
}

G4int AgataDetectorConstruction::GetMaxBgoIndex()
{
  return newGasp->GetMaxBgoIndex();
}

G4int AgataDetectorConstruction::GetNumberOfSi()
{
  return newGasp->GetNumberOfSi();
}

G4int AgataDetectorConstruction::GetMaxSiIndex()
{
  return newGasp->GetMaxSiIndex();
}

G4bool AgataDetectorConstruction::IsSiSegmented()
{
  return newGasp->IsSiSegmented();
}
#endif

#ifdef CLARA
G4int AgataDetectorConstruction::GetNumberOfMcp()
{
  return newClara->GetNumberOfMcp();
}

G4int AgataDetectorConstruction::GetMaxMcpIndex()
{
  return newClara->GetMaxMcpIndex();
}
#endif

#include "AgataDetectorConstruction.icc"

