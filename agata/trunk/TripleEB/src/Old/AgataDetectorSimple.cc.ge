#include "AgataDetectorSimple.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataDetectorSimple::AgataDetectorSimple()
{
  distSingleGe = 0.;
  thSingleGe   = 0.;
  phSingleGe   = 0.;
  
  matCryst  = NULL;
  matName   = "Germanium";
  
  readOut   = false;
  
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;

  myMessenger = new AgataDetectorSimpleMessenger(this);
}

AgataDetectorSimple::~AgataDetectorSimple()
{
  delete myMessenger;
}

G4int AgataDetectorSimple::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matCryst = ptMaterial;
    G4String nome = matCryst->GetName();
    G4cout << "\n ----> The detector material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  return 0;  
}


void AgataDetectorSimple::Placement()
{
  if( FindMaterials() ) return;
  
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  //////////////////////////////////////////////////
  // a single crystal closed-end
  //////////////////////////////////////////////////

  char sName[40];
  
  G4double InnRadGe[5];
  InnRadGe[0] =   0.;
  InnRadGe[1] =   0.;
  InnRadGe[2] =   5.*mm;
  InnRadGe[3] =   5.*mm;
  InnRadGe[4] =   5.*mm;
  
  G4double OutRadGe[5];
  OutRadGe[0] =  33.5521656958*mm;
  OutRadGe[1] =  36.19707040643551*mm;
  OutRadGe[2] =  36.19707040643551*mm;
  OutRadGe[3] =  38.*mm;
  OutRadGe[4] =  38.*mm;
  
  G4double zSliceGe[5];
  zSliceGe[0] =   0.*mm;
  zSliceGe[1] =  15.*mm;
  zSliceGe[2] =  15.*mm;
  zSliceGe[3] =  25.224921826*mm;
  zSliceGe[4] =  82.*mm;
  
  sprintf(sName, "gePcone%2.2d", 0);
  G4Polycone *pCoax  = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 5, zSliceGe, InnRadGe, OutRadGe );

  sprintf(sName, "geDetCapsL%2.2d", 0);
  G4LogicalVolume *pDetL  = new G4LogicalVolume( pCoax, matCryst, G4String(sName), 0, 0, 0 );

  G4VisAttributes *pDetVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  pDetL->SetVisAttributes( pDetVA );

  // Sensitive Detector
  pDetL->SetSensitiveDetector( theDetector->GeSD() );

  // position
  G4double psSingleGe = 0;  // not needed!!!

  G4RotationMatrix rm;
  rm.rotateZ(psSingleGe);
  rm.rotateY(thSingleGe);
  rm.rotateZ(phSingleGe);

  sprintf(sName, "geDetP%3.3d", 1);
  new G4PVPlacement(G4Transform3D(rm, rm( G4ThreeVector(0., 0., distSingleGe) )), 
                    G4String(sName), pDetL, theDetector->HallPhys(), false, 0);
  nDets++;
  nClus++;                  
  G4cout << " ----> Placed a single germanium crystal" << G4endl;
}

void AgataDetectorSimple::WriteHeader(std::ofstream &outFileLMD)
{
  outFileLMD << "CBAR" << G4endl;
}

void AgataDetectorSimple::ShowStatus()
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " Placed a single germanium crystal" << G4endl;
  G4cout << " Distance from the origin " << distSingleGe/cm << " cm" << G4endl;
  G4cout << " Angular position theta, phi " << thSingleGe/deg << ", "
                                           << phSingleGe/deg << " degrees" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////// methods for the messenger
/////////////////////////////////////////////////////////////////////////
void AgataDetectorSimple::SetDistSingleGe( G4double dist )
{
  if( dist < 0. ) {
    distSingleGe = 0.;
    G4cout << " Warning! Single Ge distance from origin set to zero " << G4endl;
  }
  else {
    distSingleGe = dist * mm;
    G4cout << " ----> Single Ge distance from origin is " << distSingleGe/cm << " cm" << G4endl;
  }
}

void AgataDetectorSimple::SetThSingleGe( G4double angle )
{
  thSingleGe = angle;
  G4cout << " ----> Single Ge will be placed at theta = " << thSingleGe/deg << " degrees" << G4endl;
}  

void AgataDetectorSimple::SetPhSingleGe( G4double angle )
{
  phSingleGe = angle;
  G4cout << " ----> Single Ge will be placed at phi   = " << phSingleGe/deg << " degrees" << G4endl;
}  

////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"

AgataDetectorSimpleMessenger::AgataDetectorSimpleMessenger(AgataDetectorSimple* pTarget)
:myTarget(pTarget)
{ 

  SetDistGeCmd = new G4UIcmdWithADouble("/Agata/detector/distGe",this);  
  SetDistGeCmd->SetGuidance("Define distance from the origin of the single Ge detector.");
  SetDistGeCmd->SetGuidance("Required parameters: 1 double (distance in mm)");
  SetDistGeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetGePositionCmd = new G4UIcmdWithAString("/Agata/detector/positionGe",this);  
  SetGePositionCmd->SetGuidance("Define position (theta, phi) of the single Ge detector.");
  SetGePositionCmd->SetGuidance("Required parameters: 2 double (theta, phi in degrees).");
  SetGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataDetectorSimpleMessenger::~AgataDetectorSimpleMessenger()
{

  delete SetGePositionCmd;
  delete SetDistGeCmd;
}

void AgataDetectorSimpleMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetGePositionCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetThSingleGe( e1*deg );
    myTarget->SetPhSingleGe( e2*deg );
  }
  if( command == SetDistGeCmd ) {
    myTarget->SetDistSingleGe(SetDistGeCmd->GetNewDoubleValue(newValue));
  }
  
}
  

