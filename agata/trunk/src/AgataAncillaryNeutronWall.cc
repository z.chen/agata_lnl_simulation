#ifdef ANCIL
// Simulation of NWall detector using GEANT package
// Code "assembled" by Joa
//
// Source file for det. geometry
//
//   ******STRENGTH AND HONOR******     
#include "AgataAncillaryNeutronWall.hh"
#include "AgataSensitiveDetector.hh"
#include <math.h>
#include "G4SDManager.hh"

using namespace std;


void AgataAncillaryNeutronWall::fixcorners(G4ThreeVector corners[8])


{

  /*This is an uggly hack... by loosing some precision the corners match*/
  for (int i=0; i<8; i++)
    {
      float xtemp = corners[i].x();
      float ytemp = corners[i].y();
      float ztemp = corners[i].z();
      corners[i](0) = xtemp;
      corners[i](1) = ytemp;
      corners[i](2) = ztemp;
    }



  /*First make sure that trap will be centered in z direction*/
  G4double zerror = corners[0].z()+corners[4].z();
  if (zerror > 0)
    corners[4](2) -=zerror;
  else
    corners[0](2) -=zerror;

  G4cout << "[0]z + [4]z =  " << corners[0].z()+corners[4].z() << "\n";
  /*The make sure all z-values are correct*/
  zerror = corners[0].z()-corners[1].z();
  corners[1](2) +=zerror;
  G4cout << "[0]z - [1]z =  " << corners[0].z()-corners[1].z() << "\n";
  zerror = corners[0].z()-corners[2].z();
  corners[2](2) +=zerror;
  G4cout << "[0]z - [2]z =  " << corners[0].z()-corners[2].z() << "\n";
  zerror = corners[0].z()-corners[3].z();
  corners[3](2) +=zerror;
  G4cout << "[0]z - [3]z =  " << corners[0].z()-corners[3].z() << "\n";
  zerror = corners[4].z()-corners[5].z();
  corners[5](2) +=zerror;
  G4cout << "[4]z - [5]z =  " << corners[4].z()-corners[5].z() << "\n";
  zerror = corners[4].z()-corners[6].z();
  corners[6](2) +=zerror;
  G4cout << "[4]z - [6]z =  " << corners[4].z()-corners[6].z() << "\n";
  zerror = corners[4].z()-corners[7].z();
  corners[7](2) +=zerror;
  G4cout << "[4]z - [7]z =  " << corners[4].z()-corners[7].z() << "\n";
  /*Now take care of y*/
  G4double yerror = (corners[0].y()+corners[2].y()+
		     corners[4].y()+corners[6].y());
  corners[0](1)-=yerror/4;
  corners[2](1)-=yerror/4;
  corners[4](1)-=yerror/4;
  corners[6](1)-=yerror/4;
  G4cout << "[0]y + [2]y + [4]y + [6]y = " 
       << (corners[0].y()+corners[2].y()+corners[4].y()+corners[6].y()) 
       << "\n";
  yerror = corners[0].y()-corners[1].y();
  corners[1](1)+=yerror;
  G4cout << "[0]y - [1]y = " << corners[0].y()-corners[1].y() << "\n";
  yerror = corners[2].y()-corners[3].y();
  corners[3](1)+=yerror;
  G4cout << "[2]y - [3]y = " << corners[2].y()-corners[3].y() << "\n";
  yerror = corners[4].y()-corners[5].y();
  corners[5](1)+=yerror;
  G4cout << "[4]y - [5]y = " << corners[4].y()-corners[5].y() << "\n";
  yerror = corners[6].y()-corners[7].y();
  corners[7](1)+=yerror;
  G4cout << "[6]y - [7]y = " << corners[6].y()-corners[7].y() << "\n";
  /*Finally x*/
  G4double xerror = 
    corners[0].x() + corners[1].x() + corners[4].x() + corners[5].x() + 
    corners[2].x() + corners[3].x() + corners[6].x() + corners[7].x();
  corners[0](0) -=xerror/4;
  corners[2](0) -=xerror/4;
  corners[4](0) -=xerror/4;
  corners[6](0) -=xerror/4;
  G4cout << "[0]x + ... + 7[x] = " << corners[0].x() + corners[1].x() + 
    corners[4].x() + corners[5].x() + 
    corners[2].x() + corners[3].x() + corners[6].x() + corners[7].x()
       << "\n";
  
}



AgataAncillaryNeutronWall::AgataAncillaryNeutronWall(G4String path, G4String name)
{
  NWallTrans = G4ThreeVector(0,0,0);
  nwallangle[0] = 0;
  nwallangle[1] = 0;
  nwallangle[2] = 0;

  iniPath = path;
  dirName = name;

  Hexagon1_inside_height = 147.12*mm;
  Distance_From_Focus_To_Backplane  = 510*mm + 194.12*mm;

  matNWallName = "liqSci";
  matNWall = NULL;
  matAluminiumName = "Aluminium";
  matAluminium = NULL;
  matVacuumName = "Vacuum";
  matVacuum = NULL;

  ancSD = NULL;
  
  ancName   = G4String("NWALL");
  ancOffset = 7000;
  
  numAncSd = 0;

  myMessenger = new AgataAncillaryNeutronWallMessenger(this,name);
  G4cout << "AgataAncillaryNeutronWall constructed...\n";
}

AgataAncillaryNeutronWall::~AgataAncillaryNeutronWall()
{
  ;
}
 
G4int AgataAncillaryNeutronWall::FindMaterials()

{
  G4Material* ptMaterial = G4Material::GetMaterial(matNWallName);
  if (ptMaterial)
    {
      matNWall = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matNWall->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matNWallName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }  

  ptMaterial = G4Material::GetMaterial(matAluminiumName);
  if (ptMaterial)
    {
      matAluminium = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matAluminium->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matAluminiumName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }  

  ptMaterial = G4Material::GetMaterial(matVacuumName);
  if (ptMaterial)
    {
      matVacuum = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matVacuum->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << matVacuumName << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }  
  return 0;
}

void AgataAncillaryNeutronWall::GetDetectorConstruction()
{

  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}


void AgataAncillaryNeutronWall::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 1;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/all", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }
}

void AgataAncillaryNeutronWall::Placement()

{

  PlaceDetector();  
  return;

}


void AgataAncillaryNeutronWall::PlaceDetector()

{
  G4cout << "Construct the Neutron Wall...\n";
  G4VisAttributes *Vac, *invisible, *Alu, *Wall;
  Vac = new G4VisAttributes(G4Colour(0.0,0.0,0.0));
  Vac -> SetVisibility(true);
  Vac -> SetDaughtersInvisible(false);
  Alu = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
  Alu -> SetVisibility(true);
  Alu -> SetDaughtersInvisible(false);
  invisible = new G4VisAttributes(G4Colour(0.0,0.0,0.0));
  invisible -> SetVisibility(false);
  Wall = new G4VisAttributes(G4Color(0,1,0,0));
  Wall -> SetVisibility(true);


  //Lab
  //General comments... none so far
  //Visibility Attributes... so the visibility of different logical volumes 
  //can be turned on and off
  //Detector Parameters and Constants, distance refer to outer dimension at
  //bottom if nothing else specified


  NWallRot.rotateX(nwallangle[0]);
  NWallRot.rotateY(nwallangle[1]);
  NWallRot.rotateZ(nwallangle[2]);
  G4Rotate3D nwallrot(NWallRot);

  
  
  //Hexagon Type 1 dim.
  //Read form drawings

  const G4double Original_Hexagon1_Center_to_Side[6] = 
    //Also anticlockwisw, side i between angles i and i+1 
  {167.88*mm,
   167.88*mm,
   167.89*mm,
   167.88*mm,
   167.88*mm,
   167.88*mm};
  
  const G4double Original_Hexagon1_Half_Length = 194.12/2.0*mm;  
  
  const G4double Hexagon1_Top_Thickness = 2*mm; //Top against target!
  
  const G4double Hexagon1_Side_Thickness = 2.5*mm;
  
  const G4double Hexagon1_Container_Divider_thickness = 2*mm;
  
  const G4double Hexagon1_Bottom_Wall_Thickness = 9.73*mm;

  const G4double Hexagon1_Bottom_Wall_Depth = 25.0*mm;

  const G4double Original_Distance_From_Focus_To_Backplane = 510*mm+//OBS 510!
    Original_Hexagon1_Half_Length*2;
  
  G4double Hexagon1_Half_Length = (Hexagon1_inside_height + 
				   Hexagon1_Top_Thickness +
				   45*mm)/2;
  const G4double Hexagon1_Side_To_xyplane_Angles[6] = 
    //The ...Angles[i] corresponds to angle to rotate something on side i
    //around z-axis -deg (phi)
  {28.13*deg,
   326.27*deg,
   270*deg,
   213.73*deg,
   151.87*deg,
   90*deg};

  //Angles to center axis of each of the fifteen detectors
  //...tor[DetectorNr][0=theta,1=phi,2=detrotZ]
  const G4double Angle_To_Detector[15][3] = {{21.21*deg +
					      asin(50.0/510.0*sin(21.21*deg)),
					      0,0},
					     {21.21*deg +
					      asin(50.0/510.0*sin(21.21*deg)),
					      72*deg,67.46*deg},
					     {21.21*deg +
					      asin(50.0/510.0*sin(21.21*deg))
					      ,144*deg,134.92*deg},
					     {21.21*deg +
					      asin(50.0/510.0*sin(21.21*deg))
					      ,216*deg,202.38*deg},
					     {21.21*deg +
					      asin(50/510.0*sin(21.21*deg)),
					      288*deg,269.84*deg},
					     {46.02*deg+
					      asin(50/510.0*sin(46.02*deg))
					      ,0,118.13*deg},
					     {39.06*deg+
					      asin(50/510.0*sin(39.06*deg))
					      ,36*deg,-149.065*deg},
					     {46.02*deg +
					      asin(50/510.0*sin(46.02*deg))
					      ,72*deg,118.13*deg},
					     {39.06*deg +
					      asin(50/510.0*sin(39.06*deg))
					      ,108*deg,-149.065*deg},
					     {46.02*deg +
					      asin(50/510.0*sin(46.02*deg))
					      ,144*deg,118.13*deg},
					     {39.06*deg +
					      asin(50/510.0*sin(39.06*deg))
					      ,180*deg,-149.065*deg},
					     {46.02*deg +
					      asin(50/510.0*sin(46.02*deg))
					      ,216*deg,118.13*deg},
					     {39.06*deg +
					      asin(50/510.0*sin(39.06*deg))
					      ,252*deg,-149.065*deg},
					     {46.02*deg +
					      asin(50/510.0*sin(46.02*deg))
					      ,288*deg,118.13*deg},
					     {39.06*deg +
					      asin(50/510.0*sin(39.06*deg))
					      ,324*deg,-149.065*deg}};
  
  const G4double Hexagon1_Bottom_Thickness = 2*Hexagon1_Half_Length - 
    Hexagon1_Top_Thickness - Hexagon1_inside_height;



   
  //Values calculated with above constants

  //First the (x,y,z) of the bottomcorners of H1 and H2

  G4ThreeVector Bottom_Corners[6];
  G4double t;
  int i;

  for (i=0; i<6; i++)
    {
      if (abs(cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))>0.000001)
      {
	t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	  (Original_Hexagon1_Center_to_Side[(i+1)%6]* 
	   sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	   1/tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])*
	   (Original_Hexagon1_Center_to_Side[i]* 
	    cos(Hexagon1_Side_To_xyplane_Angles[i])-
	    Original_Hexagon1_Center_to_Side[(i+1)%6]* 
	    cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))-
	   Original_Hexagon1_Center_to_Side[i]* 
	   sin(Hexagon1_Side_To_xyplane_Angles[i]))/
	  (1-tan(Hexagon1_Side_To_xyplane_Angles[i])/
	   tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]));
	Bottom_Corners[(i+1)%6](1) = Original_Hexagon1_Center_to_Side[i]*
	  sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	  cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	Bottom_Corners[(i+1)%6](0) = Original_Hexagon1_Center_to_Side[i]*
	  cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	  sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	Bottom_Corners[(i+1)%6](2) = Original_Distance_From_Focus_To_Backplane;
      }
      else
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Original_Hexagon1_Center_to_Side[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     Original_Hexagon1_Center_to_Side[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]));
	  Bottom_Corners[(i+1)%6](1) = Original_Hexagon1_Center_to_Side[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners[(i+1)%6](0) = Original_Hexagon1_Center_to_Side[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners[(i+1)%6](2) = 
	    Original_Distance_From_Focus_To_Backplane;
	}
    }

  //Now the theta angles of the planes, i.e. the angle to the z-axis
  //Correspondes to the scalar product with z unit vector

  G4ThreeVector z,n[6];
  G4double Hexagon1_Side_Taper_Angle[6];
  z(0)=z(1)=0;
  z(2)=1;

  G4double Corner_vs_z[6];
  for (i=0; i<6; i++)
    Corner_vs_z[i] = acos(z.dot(Bottom_Corners[i].unit()));


  for (i=0; i<6; i++)
    {
      n[i] = Bottom_Corners[(i+1)%6].cross(Bottom_Corners[i]);
      Hexagon1_Side_Taper_Angle[i] = 90*deg - acos(z.dot(n[i].unit()));
    }



  //Fix bottom corner in case radius different from NWall

  for (i=0; i<6; i++)
    {
      t = (Distance_From_Focus_To_Backplane - 
	   Original_Distance_From_Focus_To_Backplane) / 
	Bottom_Corners[i].unit()(2);
      Bottom_Corners[i] += Bottom_Corners[i].unit()*t;
    }
  

  //Fix Hexagon1_Center_to_Side in case radius different from NWall

  G4double Hexagon1_Center_to_Side[6];
  G4cout << "Hexagon1_Center_to_Side - Original_Hexagon1_Center_to_Side:\n";
  for (i=0; i<6; i++)
    {
      G4ThreeVector temp;
      temp = Bottom_Corners[i]-Bottom_Corners[(i+1)%6];
      temp(2)=0;
      G4double a = temp.mag();
      temp = Bottom_Corners[i];
      temp(2)=0;
      G4double b = temp.mag();
      temp = Bottom_Corners[(i+1)%6];
      temp(2)=0;
      G4double c = temp.mag();
      G4double p = (a+b+c)/2;
      Hexagon1_Center_to_Side[i] = 2*sqrt(p*(p-a)*(p-b)*(p-c))/a; 
      G4cout << Hexagon1_Center_to_Side[i] - Original_Hexagon1_Center_to_Side[i]
	   << "\n";
    }

  //Now the Top_Corners of Hexagon1

  G4ThreeVector Top_Corners[6];

  for (i=0; i<6; i++)
    {
      t = 2*Hexagon1_Half_Length / Bottom_Corners[i].unit()(2);
      Top_Corners[i] = Bottom_Corners[i] - Bottom_Corners[i].unit()*t;
    }


  //Now coord. for the other 36 corners needed
  //To calculate these cornes we use the thickness of the walls and how far
  //from the center the outside of the wall should be on this z-coord. and
  //then as for the outside corners.
 
  
  G4double Hexagon1_Center_to_Side_Bottom_Inside[6];
  G4double Hexagon1_Center_to_Side_Sci_Bottom[6];
  G4double Hexagon1_Center_to_Side_Sci_Bottom_Outside[6];
  
  
  for (i=0; i<6; i++)
    {
      Hexagon1_Center_to_Side_Bottom_Inside[i] =
	Hexagon1_Center_to_Side[i] - Hexagon1_Bottom_Wall_Thickness;
      Hexagon1_Center_to_Side_Sci_Bottom_Outside[i] =
	Hexagon1_Center_to_Side[i] + 
	(2*Hexagon1_Half_Length - Hexagon1_inside_height -
	 Hexagon1_Top_Thickness) * tan(-Hexagon1_Side_Taper_Angle[i]);
      Hexagon1_Center_to_Side_Sci_Bottom[i] = 
	Hexagon1_Center_to_Side[i] +
	(2*Hexagon1_Half_Length - Hexagon1_inside_height -
	 Hexagon1_Top_Thickness) * tan(-Hexagon1_Side_Taper_Angle[i]) -
	Hexagon1_Side_Thickness;
    }

  G4ThreeVector Sci_Bottom_Corners[6];
  G4ThreeVector Sci_Bottom_Corners_Outside[6];
  G4ThreeVector Bottom_Corners_Inside[6];

  for (i=0; i<6; i++)
    {
      //First bottom corners inside
      if (abs(cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))>0.000001)
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Bottom_Inside[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     1/tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])*
	     (Hexagon1_Center_to_Side_Bottom_Inside[i]* 
	      cos(Hexagon1_Side_To_xyplane_Angles[i])-
	      Hexagon1_Center_to_Side_Bottom_Inside[(i+1)%6]* 
	      cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))-
	   Hexagon1_Center_to_Side_Bottom_Inside[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]))/
	    (1-tan(Hexagon1_Side_To_xyplane_Angles[i])/
	     tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]));
	  Bottom_Corners_Inside[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Bottom_Inside[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners_Inside[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Bottom_Inside[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners_Inside[(i+1)%6](2) = Distance_From_Focus_To_Backplane;
	}
      else
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Bottom_Inside[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     Hexagon1_Center_to_Side_Bottom_Inside[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]));
	  Bottom_Corners_Inside[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Bottom_Inside[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners_Inside[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Bottom_Inside[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Bottom_Corners_Inside[(i+1)%6](2) = Distance_From_Focus_To_Backplane;
	}
      //Then sci bottom corners inside
      if (abs(cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))>0.000001)
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Sci_Bottom[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     1/tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])*
	     (Hexagon1_Center_to_Side_Sci_Bottom[i]* 
	      cos(Hexagon1_Side_To_xyplane_Angles[i])-
	      Hexagon1_Center_to_Side_Sci_Bottom[(i+1)%6]* 
	    cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))-
	     Hexagon1_Center_to_Side_Sci_Bottom[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]))/
	    (1-tan(Hexagon1_Side_To_xyplane_Angles[i])/
	     tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]));
	  Sci_Bottom_Corners[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Sci_Bottom[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Sci_Bottom[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	Sci_Bottom_Corners[(i+1)%6](2) = Distance_From_Focus_To_Backplane - 
	  (2*Hexagon1_Half_Length - Hexagon1_Top_Thickness - 
	   Hexagon1_inside_height);
	}
      else
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Sci_Bottom[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     Hexagon1_Center_to_Side_Sci_Bottom[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]));
	  Sci_Bottom_Corners[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Sci_Bottom[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Sci_Bottom[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners[(i+1)%6](2) = Distance_From_Focus_To_Backplane - 
	    (2*Hexagon1_Half_Length - Hexagon1_Top_Thickness - 
	     Hexagon1_inside_height);
	}
      //Then sci coord outside
      if (abs(cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))>0.000001)
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Sci_Bottom_Outside[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     1/tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])*
	     (Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]* 
	      cos(Hexagon1_Side_To_xyplane_Angles[i])-
	      Hexagon1_Center_to_Side_Sci_Bottom_Outside[(i+1)%6]* 
	      cos(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]))-
	     Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]))/
	    (1-tan(Hexagon1_Side_To_xyplane_Angles[i])/
	     tan(Hexagon1_Side_To_xyplane_Angles[(i+1)%6]));
	  Sci_Bottom_Corners_Outside[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners_Outside[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners_Outside[(i+1)%6](2) = 
	    Distance_From_Focus_To_Backplane - 
	    (2*Hexagon1_Half_Length - Hexagon1_Top_Thickness - 
	     Hexagon1_inside_height);
	}
      else
	{
	  t = -1/cos(Hexagon1_Side_To_xyplane_Angles[i])*
	    (Hexagon1_Center_to_Side_Sci_Bottom_Outside[(i+1)%6]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[(i+1)%6])-
	     Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]* 
	     sin(Hexagon1_Side_To_xyplane_Angles[i]));
	  Sci_Bottom_Corners_Outside[(i+1)%6](1) = 
	    Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]*
	    sin(Hexagon1_Side_To_xyplane_Angles[i]) -
	    cos(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners_Outside[(i+1)%6](0) = 
	    Hexagon1_Center_to_Side_Sci_Bottom_Outside[i]*
	    cos(Hexagon1_Side_To_xyplane_Angles[i]) +
	    sin(Hexagon1_Side_To_xyplane_Angles[i])*t;
	  Sci_Bottom_Corners_Outside[(i+1)%6](2) = 
	    Distance_From_Focus_To_Backplane - 
	    (2*Hexagon1_Half_Length - Hexagon1_Top_Thickness - 
	     Hexagon1_inside_height);
	}
      
    }
  
  //Then corners closer to top
  
  G4ThreeVector Sci_Top_Corners[6];
  G4ThreeVector Sci_Top_Corners_Outside[6];
  G4ThreeVector Top_Corners_Inside[6];
  for (i=0; i<6; i++)
    {
      // First top corners inside
      t = Hexagon1_Bottom_Wall_Depth / Bottom_Corners[i].unit()(2);
      Top_Corners_Inside[i] = 
	Bottom_Corners_Inside[i] - Bottom_Corners[i].unit()*t;
      // Then sci top corners inside      
      t = Hexagon1_inside_height / Bottom_Corners[i].unit()(2); 
      Sci_Top_Corners[i] = Sci_Bottom_Corners[i] - Bottom_Corners[i].unit()*t;
      // Then the sci top corners outside
      t = Hexagon1_inside_height / Bottom_Corners[i].unit()(2); 
      Sci_Top_Corners_Outside[i] = Sci_Bottom_Corners_Outside[i] - 
	Bottom_Corners[i].unit()*t;
    }

  //All 'corners' together, for my clarity
  //  G4ThreeVector Bottom_Corners[6];  
  //  G4ThreeVector Top_Corners[6];  
  //  G4ThreeVector Sci_Bottom_Corners[6];
  //  G4ThreeVector Sci_Top_Corners[6];


  //  G4ThreeVector Sci_Bottom_Corners_Outside[6];
  //  G4ThreeVector Bottom_Corners_Inside[6];

  //  G4ThreeVector Sci_Top_Corners_Outside[6];
  //  G4ThreeVector Top_Corners_Inside[6];
  //Total of 48 corners, thats all we need!

  //Now the twelve angles in the outer corners of the six 'triangel shapes'
  //that builds up the hexagon, counting anticlockwise
 
  G4double Hexagon1_Triag_Angles[12];
  
  //Temps used in this calculation

  G4ThreeVector zerozeroz;
  zerozeroz(0) = zerozeroz(1) = 0;
  zerozeroz(2) = Bottom_Corners[0](2);
  int j;

  //End of these temps
  for (i=0; i<12; i++)
    Hexagon1_Triag_Angles[i] = 5.0;

  for (i=0; i<12; i+=2)
    {
      //Calculate correct -1<j<6 integer
      j = i/2;
      Hexagon1_Triag_Angles[i] = 
	acos((Bottom_Corners[j]-zerozeroz).
	     dot(Bottom_Corners[j] - Bottom_Corners[(j+1)%6])/
	     ((Bottom_Corners[j]-zerozeroz).mag()*
	      (Bottom_Corners[j] - Bottom_Corners[(j+1)%6]).mag()));
      Hexagon1_Triag_Angles[i+1] =
	acos((Bottom_Corners[(j+1)%6]-zerozeroz).
	      dot(Bottom_Corners[(j+1)%6] - Bottom_Corners[j])/
	     ((Bottom_Corners[(j+1)%6]-zerozeroz).mag()*
	      (Bottom_Corners[j] - Bottom_Corners[(j+1)%6]).mag()));
    }


  //G4cout << "Bottom corners:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Bottom_Corners[i] << "\n";
  
  //G4cout << "Bottom corners inside:\n";
  // for (int i=0; i<6; i++)
  //  G4cout << Bottom_Corners_Inside[i] << "\n";
  
  //G4cout << "Sci Bottom corners:\n";
  //for (int i=0; i<6; i++)
  // G4cout << Sci_Bottom_Corners[i] << "\n";
  
  //G4cout << "Sci. Bottom corners Outside:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Sci_Bottom_Corners_Outside[i] << "\n";

  //G4cout << "Sci. Top corners Outside:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Sci_Top_Corners_Outside[i] << "\n";

  //G4cout << "Sci. Top corners:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Sci_Top_Corners[i] << "\n";
  
  //G4cout << "Top corners:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Top_Corners[i] << "\n";

  //G4cout << "Top corners inside:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Top_Corners_Inside[i] << "\n";

  //G4cout << "Side taper angles:\n";
  //for (int i=0; i<6; i++)
  //  G4cout << Hexagon1_Side_Taper_Angle[i]/deg << "\n";
  
  //G4cout << "Hexagon1_Triag_Angles:\n";
  //for (int i=0; i<12; i++)
  //  G4cout << Hexagon1_Triag_Angles[i]/deg << "\n";
  
  
  
  //Now the coordinates of aluminumwalls inside scintillator volume
  //..lls[0=bottom,1=top][0..3=corner]. Corners anticlockwise
  //staring on anticlockwise side on wall.

  G4ThreeVector one;
  


  G4ThreeVector Hexagon1_Coor_Inner_Walls[2][4];
  
  //Wall 0 in Hexagons of type 1
  one(0) = Hexagon1_Container_Divider_thickness/2.0;
  one(1) = one(2) = 0;
  one.rotateZ(-90*deg + Hexagon1_Side_To_xyplane_Angles[0]);
  Hexagon1_Coor_Inner_Walls[0][0] = 0.5*(Sci_Bottom_Corners[0] +
					    Sci_Bottom_Corners[1])  +
    one;
  Hexagon1_Coor_Inner_Walls[0][3] = 0.5*(Sci_Bottom_Corners[0] +
					    Sci_Bottom_Corners[1]) - 
    one;
  Hexagon1_Coor_Inner_Walls[0][1] = 
    one;
  Hexagon1_Coor_Inner_Walls[0][2] = 
    -one;
  Hexagon1_Coor_Inner_Walls[0][1](2) =
    Hexagon1_Coor_Inner_Walls[0][2](2) =
    Sci_Bottom_Corners[0](2);

  Hexagon1_Coor_Inner_Walls[1][0] = 0.5*(Sci_Top_Corners[0] +
					    Sci_Top_Corners[1]) +
    one;
  Hexagon1_Coor_Inner_Walls[1][3] = 0.5*(Sci_Top_Corners[0] +
					    Sci_Top_Corners[1]) - 
    one;
  Hexagon1_Coor_Inner_Walls[1][1] = 
    one;
  Hexagon1_Coor_Inner_Walls[1][2] = 
    -one;
  Hexagon1_Coor_Inner_Walls[1][1](2) =
    Hexagon1_Coor_Inner_Walls[1][2](2) =
    Sci_Top_Corners[0](2);
  

  //Hexagon Type 1 ends
  

  //Hexagon Type 2 dim.
  //If same for H1 and H2, values for H1 used
  
  //Now the coordinates of aluminumwalls inside scintillator volume
  //..lls[0=bottom,1=top][0..3=corner]. Corners anticlockwise
  //staring on anticlockwise side on wall.

  G4ThreeVector oneH2;
  


  G4ThreeVector Hexagon2_Coor_Inner_Walls[2][4];
  
  //Wall 0 in Hexagons of type 2
  oneH2(0) = Hexagon1_Container_Divider_thickness/2.0;
  oneH2(1) = oneH2(2) = 0;
  oneH2.rotateZ(-92.8*deg);
  Hexagon2_Coor_Inner_Walls[0][0] = Sci_Bottom_Corners[1]  +
    oneH2;
  Hexagon2_Coor_Inner_Walls[0][3] = Sci_Bottom_Corners[1] - 
    oneH2;
  Hexagon2_Coor_Inner_Walls[0][1] = 
    oneH2;
  Hexagon2_Coor_Inner_Walls[0][2] = 
    -oneH2;
  Hexagon2_Coor_Inner_Walls[0][1](2) =
    Hexagon2_Coor_Inner_Walls[0][2](2) =
    Sci_Bottom_Corners[0](2);

  Hexagon2_Coor_Inner_Walls[1][0] = Sci_Top_Corners[1] +
    oneH2;
  Hexagon2_Coor_Inner_Walls[1][3] = Sci_Top_Corners[1] - 
    oneH2;
  Hexagon2_Coor_Inner_Walls[1][1] = 
    oneH2;
  Hexagon2_Coor_Inner_Walls[1][2] = 
    -oneH2;
  Hexagon2_Coor_Inner_Walls[1][1](2) =
    Hexagon2_Coor_Inner_Walls[1][2](2) =
    Sci_Top_Corners[0](2);
  

  //Hexagon Type 2 ends
  

  //Pentagon dim.

  const G4double tanangle = 305.477*mm/2/(510*mm+194.12*mm);
  const G4double Pentagon_Bottom = sin(54*deg)*
    tanangle*Distance_From_Focus_To_Backplane;
  const G4double Pentagon_Top = sin(54*deg)*
    tanangle*(Distance_From_Focus_To_Backplane-2*Hexagon1_Half_Length);
  const G4double Pentagon_Half_Length = Hexagon1_Half_Length; 
  const G4double Pentagon_Sci_Bottom = Pentagon_Top+
    (Pentagon_Bottom-Pentagon_Top)*
    (Hexagon1_Top_Thickness+Hexagon1_inside_height)
    /(2*Pentagon_Half_Length) - Hexagon1_Side_Thickness;
  const G4double Pentagon_Sci_Top = Pentagon_Top+
    (Pentagon_Bottom-Pentagon_Top)*
    Hexagon1_Top_Thickness
    /(2*Pentagon_Half_Length) - Hexagon1_Side_Thickness;
  const G4double Pentagon_Vac_Bottom = 
    Pentagon_Bottom - Hexagon1_Bottom_Wall_Thickness;
  const G4double Pentagon_Vac_Top = Pentagon_Vac_Bottom - 
    (Pentagon_Bottom-Pentagon_Top)*
    Hexagon1_Bottom_Wall_Depth
    /(2*Pentagon_Half_Length);
  


  
  

  //Pentagon ends
  
       
  //////////////////////////////////////////////////////////////////
  //Pentagon detctor                                              //
  //////////////////////////////////////////////////////////////////

  G4Polyhedra *PHousing,*PScintillator, *PVacume;
  G4double Hradius[2], Sciradius[2], Vacradius[2], Hzs[2], Scizs[2], Vaczs[2],
    zeros[2] = {0,0};

  Hradius[0] = Pentagon_Bottom;
  Hradius[1] = Pentagon_Top;
  Hzs[0] = Pentagon_Half_Length;
  Hzs[1] = -Pentagon_Half_Length;
  
  PHousing = new G4Polyhedra("PHousing",0,2*M_PI,5,2,Hzs,zeros,Hradius);
  
  G4LogicalVolume *LPHousing;
  LPHousing = new G4LogicalVolume(PHousing,matAluminium,"LPHousing");
 
  Sciradius[0] = Pentagon_Sci_Bottom;
  Sciradius[1] = Pentagon_Sci_Top;
  Scizs[0] = Hexagon1_inside_height/2;
  Scizs[1] = -Hexagon1_inside_height/2;
  
  PScintillator = new G4Polyhedra("PScintillator",0,2*M_PI,5,2,Scizs,zeros,
				  Sciradius);
  
  G4LogicalVolume *LPScintillator;
  LPScintillator = new G4LogicalVolume(PScintillator,matNWall,"LPScintillator");
  LPScintillator->SetSensitiveDetector(ancSD);
  Vacradius[0] = Pentagon_Vac_Bottom;
  Vacradius[1] = Pentagon_Vac_Top;
  Vaczs[0] = Hexagon1_Bottom_Wall_Depth/2;
  Vaczs[1] = -Hexagon1_Bottom_Wall_Depth/2;
  
  PVacume = new G4Polyhedra("PVacume",0,2*M_PI,5,2,Vaczs,zeros,
				  Vacradius);
  
  G4LogicalVolume *LPVacume;
  LPVacume = new G4LogicalVolume(PVacume,matVacuum,"LPVacume");

  
  G4Trap *PInWalls;
  G4ThreeVector PInWallsCoor[8];

  PInWallsCoor[0](0) = -1;
  PInWallsCoor[0](1) = Pentagon_Sci_Bottom;
  PInWallsCoor[0](2) = -Hexagon1_inside_height/2;
  PInWallsCoor[1](0) = 1;
  PInWallsCoor[1](1) = Pentagon_Sci_Bottom;
  PInWallsCoor[1](2) = -Hexagon1_inside_height/2;
  PInWallsCoor[2](0) = -1;
  PInWallsCoor[2](1) = 0;
  PInWallsCoor[2](2) = -Hexagon1_inside_height/2;
  PInWallsCoor[3](0) = 1;
  PInWallsCoor[3](1) = 0;
  PInWallsCoor[3](2) = -Hexagon1_inside_height/2;
  PInWallsCoor[4](0) = -1;
  PInWallsCoor[4](1) = Pentagon_Sci_Top;
  PInWallsCoor[4](2) = Hexagon1_inside_height/2;
  PInWallsCoor[5](0) = 1;
  PInWallsCoor[5](1) = Pentagon_Sci_Top;
  PInWallsCoor[5](2) = Hexagon1_inside_height/2;
  PInWallsCoor[6](0) = -1;
  PInWallsCoor[6](1) = 0;
  PInWallsCoor[6](2) = Hexagon1_inside_height/2;
  PInWallsCoor[7](0) = 1;
  PInWallsCoor[7](1) = 0;
  PInWallsCoor[7](2) = Hexagon1_inside_height/2;

  G4double Py_correction = (PInWallsCoor[0].y() + PInWallsCoor[2].y() + 
			    PInWallsCoor[4].y() + PInWallsCoor[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      PInWallsCoor[i](1)  = PInWallsCoor[i](1) - Py_correction;
    }

  fixcorners(PInWallsCoor);
  
  PInWalls = new 
    G4Trap("PPInWalls",PInWallsCoor);

  G4LogicalVolume *LPInWalls;
  LPInWalls = new G4LogicalVolume(PInWalls,matAluminium,"LPInWalls");
  
  PentRot.rotateZ(-18*deg);
  PentZRot = G4Rotate3D(-18*deg,G4ThreeVector(0,0,1));
  PentTrans(1) = PentTrans(0) = 0;
  PentTrans(2) = Distance_From_Focus_To_Backplane*cos(Angle_To_Detector[0][0])
    - Pentagon_Half_Length +
    Hexagon1_Center_to_Side[2]*sin(Angle_To_Detector[0][0]);

  PentTrans += NWallTrans;

  G4VPhysicalVolume *PPHousing, *PPscintillator; 
  //G4VPhysicalVolume *PPVacume,*PPInWall0, *PPInWall1, *PPInWall2, *PPInWall3, *PPInWall4;

  PentTransform = nwallrot*G4Translate3D(PentTrans)*PentZRot;

  PPHousing = new G4PVPlacement(PentTransform,
				"Pentagon",LPHousing,theDetector->HallPhys(),false,0);

  
  G4ThreeVector PentTrans_Sci_In_Housing; 

  PentTrans_Sci_In_Housing(0) = PentTrans_Sci_In_Housing(1) = 0;
  PentTrans_Sci_In_Housing(2) = - Pentagon_Half_Length +
    Scizs[0] + Hexagon1_Top_Thickness;
  G4Transform3D SciPentTrans;

  SciPentTrans = G4Translate3D(PentTrans_Sci_In_Housing);

  PPscintillator = new G4PVPlacement(SciPentTrans,
				     "PentagonPSci",LPScintillator,
				     PPHousing,false,0);
 

  G4ThreeVector InWall0T;

  InWall0T(0) = InWall0T(2) = 0;
  InWall0T(1) = -0.5 * (PInWallsCoor[2](1) + PInWallsCoor[6](1));

  G4Transform3D InWallTrans0;
  
  InWall0T.rotateZ(-18*deg);
  InWallTrans0 = G4RotateY3D(180*deg)*
    G4Translate3D(InWall0T)*
    G4RotateZ3D(-18*deg);
  /*PPInWall0 =*/ new G4PVPlacement(InWallTrans0,"PentagonPInWall0",LPInWalls,
				PPscintillator,false,0);
 
  G4ThreeVector InWall1T;

  InWall1T(0) = InWall1T(2) = 0;
  InWall1T(1) = -0.5 * (PInWallsCoor[2](1) + PInWallsCoor[6](1));

  G4Transform3D InWallTrans1;
  
  InWall1T.rotateZ(-90*deg);
  InWallTrans1 = G4RotateY3D(180*deg)*
    G4Translate3D(InWall1T)*
    G4RotateZ3D(-90*deg);
  /*PPInWall1 =*/ new G4PVPlacement(InWallTrans1,"PentagonPInWall1",LPInWalls,
				PPscintillator,false,0);
 
  G4ThreeVector InWall2T;

  InWall2T(0) = InWall2T(2) = 0;
  InWall2T(1) = -0.5 * (PInWallsCoor[2](1) + PInWallsCoor[6](1));

  G4Transform3D InWallTrans2;
  
  InWall2T.rotateZ(-162*deg);
  InWallTrans2 = G4RotateY3D(180*deg)*
    G4Translate3D(InWall2T)*
    G4RotateZ3D(-162*deg);
  /*PPInWall2 =*/ new G4PVPlacement(InWallTrans2,"PentagonPInWall2",LPInWalls,
				PPscintillator,false,0);

  G4ThreeVector InWall3T;
  
  InWall3T(0) = InWall3T(2) = 0;
  InWall3T(1) = -0.5 * (PInWallsCoor[2](1) + PInWallsCoor[6](1));

  G4Transform3D InWallTrans3;
  
  InWall3T.rotateZ(-234*deg);
  InWallTrans3 = G4RotateY3D(180*deg)*
    G4Translate3D(InWall3T)*
    G4RotateZ3D(-234*deg);
  /*PPInWall3 =*/ new G4PVPlacement(InWallTrans3,"PentagonPInWall3",LPInWalls,
				PPscintillator,false,0);
 

  G4ThreeVector InWall4T;

  InWall4T(0) = InWall4T(2) = 0;
  InWall4T(1) = -0.5 * (PInWallsCoor[2](1) + PInWallsCoor[6](1));

  G4Transform3D InWallTrans4;
  
  InWall4T.rotateZ(-306*deg);
  InWallTrans4 = G4RotateY3D(180*deg)*
    G4Translate3D(InWall4T)*
    G4RotateZ3D(-306*deg);
  /*PPInWall4 =*/ new G4PVPlacement(InWallTrans4,"PentagonPInWall4",LPInWalls,
				PPscintillator,false,0);
 
 
  
  G4ThreeVector PentTrans_Vac_In_Housing; 
  
  PentTrans_Vac_In_Housing(0) = PentTrans_Vac_In_Housing(1) = 0;
  PentTrans_Vac_In_Housing(2) = Pentagon_Half_Length -
    Vaczs[0];
  G4Transform3D VacPentTrans;

  VacPentTrans = G4Translate3D(PentTrans_Vac_In_Housing);

  /*PPVacume=*/ new G4PVPlacement(VacPentTrans,
			      "PentagonPVac",LPVacume,
			      PPHousing,false,0);
  
  LPHousing -> SetVisAttributes(Alu);
  LPScintillator -> SetVisAttributes(Wall);
  LPVacume -> SetVisAttributes(Vac);
  LPInWalls -> SetVisAttributes(Alu);
  
  
  //////////////////////////////////////////////////////////////////
  //Some var. used...                                             //
  //////////////////////////////////////////////////////////////////
  
  G4double  y_correction=0,z_correction=0;
 
  
  //////////////////////////////////////////////////////////////////
  //H10 detector                                                  //
  //////////////////////////////////////////////////////////////////


  G4Trap *H10Sci_Half1_4, *H10Sci_Half4_1;

  G4ThreeVector H10SciH1_4Vertices[8];

  H10SciH1_4Vertices[4] = Sci_Bottom_Corners[1];
  H10SciH1_4Vertices[5] = Sci_Bottom_Corners[4];
  H10SciH1_4Vertices[7] = Sci_Bottom_Corners[3];
  H10SciH1_4Vertices[6] = Sci_Bottom_Corners[2];
  H10SciH1_4Vertices[0] = Sci_Top_Corners[1];
  H10SciH1_4Vertices[1] = Sci_Top_Corners[4];
  H10SciH1_4Vertices[3] = Sci_Top_Corners[3];
  H10SciH1_4Vertices[2] = Sci_Top_Corners[2];

  
  y_correction = (H10SciH1_4Vertices[0].y() + H10SciH1_4Vertices[2].y() + 
  		  H10SciH1_4Vertices[4].y() + H10SciH1_4Vertices[6].y())/4.0;
  z_correction = Distance_From_Focus_To_Backplane  - Hexagon1_Bottom_Thickness
    - Hexagon1_inside_height/2;

  //  G4cout << "Sci. corners for solid\n";
  for (i=0; i<8; i++)
    {
      H10SciH1_4Vertices[i](1)  = H10SciH1_4Vertices[i](1) - y_correction;
      H10SciH1_4Vertices[i](2)  = H10SciH1_4Vertices[i](2) - z_correction;
      //  G4cout << H10SciH1_4Vertices[i] << "\n";
    }
  

  fixcorners(H10SciH1_4Vertices);

  H10Sci_Half1_4 = new 
    G4Trap("Sci_Half1_4",H10SciH1_4Vertices);
 
  G4ThreeVector H10SciH4_1Vertices[8];
  
  H10SciH4_1Vertices[4] = Sci_Bottom_Corners[4];
  H10SciH4_1Vertices[5] = Sci_Bottom_Corners[1];
  H10SciH4_1Vertices[7] = Sci_Bottom_Corners[0];
  H10SciH4_1Vertices[6] = Sci_Bottom_Corners[5];
  H10SciH4_1Vertices[0] = Sci_Top_Corners[4];
  H10SciH4_1Vertices[1] = Sci_Top_Corners[1];
  H10SciH4_1Vertices[3] = Sci_Top_Corners[0];
  H10SciH4_1Vertices[2] = Sci_Top_Corners[5];
  
  y_correction = (H10SciH4_1Vertices[0].y() + H10SciH4_1Vertices[2].y() + 
		  H10SciH4_1Vertices[4].y() + H10SciH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H10SciH4_1Vertices[i](1)  = H10SciH4_1Vertices[i](1) - y_correction;
      H10SciH4_1Vertices[i](2)  = H10SciH4_1Vertices[i](2) - z_correction;
    }

  fixcorners(H10SciH4_1Vertices);
  
  H10Sci_Half4_1 = new 
    G4Trap("Sci_Half4_1",H10SciH4_1Vertices);
  
  G4UnionSolid *H10scintillator;

  G4ThreeVector H10UnionTrans;
  G4RotationMatrix H10UnionRot;
  H10UnionRot.rotateZ(0*deg);
  
  H10UnionTrans(1)= ((Sci_Bottom_Corners[1](1)-Sci_Bottom_Corners[2](1))/2+
		     (Sci_Top_Corners[1](1)-Sci_Top_Corners[2](1))/2)/2 +
    ((Sci_Bottom_Corners[0](1)-Sci_Bottom_Corners[1](1))/2+
     (Sci_Top_Corners[0](1)-Sci_Top_Corners[1](1))/2)/2;


  H10scintillator = new G4UnionSolid("H10scintillator",H10Sci_Half1_4,
				     H10Sci_Half4_1,
				     G4Transform3D(H10UnionRot,H10UnionTrans));

  G4LogicalVolume *LH10scintillator;

  LH10scintillator = new G4LogicalVolume(H10scintillator, matNWall, "H10Sci");
  LH10scintillator->SetSensitiveDetector(ancSD);
  /////////////////////////////////////////////////////////////////////////
  //Container splitting walls                                            //
  /////////////////////////////////////////////////////////////////////////
  G4Trap *H10InWall0;
  
  G4ThreeVector H10InWall0Vertices[8];

  H10InWall0Vertices[4] = Hexagon1_Coor_Inner_Walls[0][2];
  H10InWall0Vertices[5] = Hexagon1_Coor_Inner_Walls[0][1];
  H10InWall0Vertices[7] = Hexagon1_Coor_Inner_Walls[0][0];
  H10InWall0Vertices[6] = Hexagon1_Coor_Inner_Walls[0][3];
  H10InWall0Vertices[0] = Hexagon1_Coor_Inner_Walls[1][2];
  H10InWall0Vertices[1] = Hexagon1_Coor_Inner_Walls[1][1];
  H10InWall0Vertices[3] = Hexagon1_Coor_Inner_Walls[1][0];
  H10InWall0Vertices[2] = Hexagon1_Coor_Inner_Walls[1][3];
  
  for (i=0; i<8; i++)
    {
      H10InWall0Vertices[i].rotateZ(90*deg-Hexagon1_Side_To_xyplane_Angles[0]);
    }

  y_correction = (H10InWall0Vertices[0].y() + H10InWall0Vertices[2].y() + 
		  H10InWall0Vertices[4].y() + H10InWall0Vertices[6].y())/4;
  z_correction = Distance_From_Focus_To_Backplane  - Hexagon1_Bottom_Thickness
    - Hexagon1_inside_height/2;

 
   for (i=0; i<8; i++)
    {
      H10InWall0Vertices[i](1)  = H10InWall0Vertices[i](1) - 
	y_correction;
      H10InWall0Vertices[i](2)  = H10InWall0Vertices[i](2) - 
	z_correction;
    }
  
   fixcorners(H10InWall0Vertices);
   
   H10InWall0 = new 
    G4Trap("InWall0",H10InWall0Vertices);


  G4LogicalVolume *LH10InWall0;
  
  LH10InWall0 = new G4LogicalVolume(H10InWall0,matAluminium,"H10InWall0");
  
  /////////////////////////////////////////////////////////////////////////
  //Top vacume                                                           //
  /////////////////////////////////////////////////////////////////////////
  G4Trap *H10Vac_Half1_4, *H10Vac_Half4_1;

  G4ThreeVector H10VacH1_4Vertices[8];

  H10VacH1_4Vertices[4] = Bottom_Corners_Inside[1];
  H10VacH1_4Vertices[5] = Bottom_Corners_Inside[4];
  H10VacH1_4Vertices[7] = Bottom_Corners_Inside[3];
  H10VacH1_4Vertices[6] = Bottom_Corners_Inside[2];
  H10VacH1_4Vertices[0] = Top_Corners_Inside[1];
  H10VacH1_4Vertices[1] = Top_Corners_Inside[4];
  H10VacH1_4Vertices[3] = Top_Corners_Inside[3];
  H10VacH1_4Vertices[2] = Top_Corners_Inside[2];
  
  y_correction = (H10VacH1_4Vertices[0].y() + H10VacH1_4Vertices[2].y() + 
		  H10VacH1_4Vertices[4].y() + H10VacH1_4Vertices[6].y())/4;
  z_correction = Distance_From_Focus_To_Backplane  -
    Hexagon1_Bottom_Wall_Depth/2;

 
   for (i=0; i<8; i++)
    {
      H10VacH1_4Vertices[i](1)  = H10VacH1_4Vertices[i](1) - y_correction;
      H10VacH1_4Vertices[i](2)  = H10VacH1_4Vertices[i](2) - z_correction;
    }
  

   fixcorners(H10VacH1_4Vertices);
   
  H10Vac_Half1_4 = new 
    G4Trap("Sci_Half1_4",H10VacH1_4Vertices);
  
  G4ThreeVector H10VacH4_1Vertices[8];
  
  H10VacH4_1Vertices[4] = Bottom_Corners_Inside[4];
  H10VacH4_1Vertices[5] = Bottom_Corners_Inside[1];
  H10VacH4_1Vertices[7] = Bottom_Corners_Inside[0];
  H10VacH4_1Vertices[6] = Bottom_Corners_Inside[5];
  H10VacH4_1Vertices[0] = Top_Corners_Inside[4];
  H10VacH4_1Vertices[1] = Top_Corners_Inside[1];
  H10VacH4_1Vertices[3] = Top_Corners_Inside[0];
  H10VacH4_1Vertices[2] = Top_Corners_Inside[5];
  
  y_correction = (H10VacH4_1Vertices[0].y() + H10VacH4_1Vertices[2].y() + 
		  H10VacH4_1Vertices[4].y() + H10VacH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H10VacH4_1Vertices[i](1)  = H10VacH4_1Vertices[i](1) - y_correction;
      H10VacH4_1Vertices[i](2)  = H10VacH4_1Vertices[i](2) - z_correction;
    }

  fixcorners(H10VacH4_1Vertices);
  
  H10Vac_Half4_1 = new 
    G4Trap("Sci_Half4_1",H10VacH4_1Vertices);
  
  G4UnionSolid *H10Vacume;

  G4ThreeVector H10UnionTransV;
  G4RotationMatrix H10UnionRotV;
  H10UnionRotV.rotateZ(0*deg);
  
  H10UnionTransV(1)= ((Bottom_Corners_Inside[1](1)-Bottom_Corners_Inside[2](1))/2+
		     (Top_Corners_Inside[1](1)-Top_Corners_Inside[2](1))/2)/2 +
    ((Bottom_Corners_Inside[0](1)-Bottom_Corners_Inside[1](1))/2+
     (Top_Corners_Inside[0](1)-Top_Corners_Inside[1](1))/2)/2;


  H10Vacume = new G4UnionSolid("H10Vacume",H10Vac_Half1_4,
				     H10Vac_Half4_1,
				     G4Transform3D(H10UnionRotV,
						   H10UnionTransV));

  G4LogicalVolume *LH10Vacume;

  LH10Vacume = new G4LogicalVolume(H10Vacume, matVacuum, "H10Vac");

  

  ///////////////////////////////////////////////////////////////
  //Then the housing                                           //
  ///////////////////////////////////////////////////////////////

  G4Trap *H10Housing_Half1_4, *H10Housing_Half4_1;



  G4ThreeVector H10HH1_4Vertices[8];

  H10HH1_4Vertices[4] = Bottom_Corners[1];
  H10HH1_4Vertices[5] = Bottom_Corners[4];
  H10HH1_4Vertices[7] = Bottom_Corners[3];
  H10HH1_4Vertices[6] = Bottom_Corners[2];
  H10HH1_4Vertices[0] = Top_Corners[1];
  H10HH1_4Vertices[1] = Top_Corners[4];
  H10HH1_4Vertices[3] = Top_Corners[3];
  H10HH1_4Vertices[2] = Top_Corners[2];
  
  y_correction = (H10HH1_4Vertices[0].y() + H10HH1_4Vertices[2].y() + 
		  H10HH1_4Vertices[4].y() + H10HH1_4Vertices[6].y())/4;
  z_correction = (-Hexagon1_Half_Length + Distance_From_Focus_To_Backplane);
   
  for (i=0; i<8; i++)
    {
      H10HH1_4Vertices[i](1)  = H10HH1_4Vertices[i](1) - y_correction;
      H10HH1_4Vertices[i](2)  = H10HH1_4Vertices[i](2) - z_correction;
    }

  //Vector from origo of detector to where linje from focus point exits

  G4ThreeVector H10HexOrigo;
  H10HexOrigo(0) = 0;
  H10HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H10HexOrigo(2) = Hexagon1_Half_Length;
  
  

  fixcorners(H10HH1_4Vertices);


  H10Housing_Half1_4 = new 
    G4Trap("Housing_Half1_4",H10HH1_4Vertices); 
  
  G4ThreeVector H10HH4_1Vertices[8];
  
  H10HH4_1Vertices[4] = Bottom_Corners[4];
  H10HH4_1Vertices[5] = Bottom_Corners[1];
  H10HH4_1Vertices[7] = Bottom_Corners[0];
  H10HH4_1Vertices[6] = Bottom_Corners[5];
  H10HH4_1Vertices[0] = Top_Corners[4];
  H10HH4_1Vertices[1] = Top_Corners[1];
  H10HH4_1Vertices[3] = Top_Corners[0];
  H10HH4_1Vertices[2] = Top_Corners[5];
  
  y_correction = (H10HH4_1Vertices[0].y() + H10HH4_1Vertices[2].y() + 
		  H10HH4_1Vertices[4].y() + H10HH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H10HH4_1Vertices[i](1)  = H10HH4_1Vertices[i](1) - y_correction;
      H10HH4_1Vertices[i](2)  = H10HH4_1Vertices[i](2) - z_correction;
    }
  
  fixcorners(H10HH4_1Vertices);

  H10Housing_Half4_1 = new 
    G4Trap("Housing_Half4_1",H10HH4_1Vertices);
	      
  G4UnionSolid *H10Housing;

  G4ThreeVector H10UnionTransH;
  G4RotationMatrix H10UnionRotH;
  H10UnionRotH.rotateZ(0*deg);
  
  H10UnionTransH(1)= ((Bottom_Corners[1](1)-Bottom_Corners[2](1))/2+
		  (Top_Corners[1](1)-Top_Corners[2](1))/2)/2 +
    ((Bottom_Corners[0](1)-Bottom_Corners[1](1))/2+
     (Top_Corners[0](1)-Top_Corners[1](1))/2)/2;// - 0.001*mm;


  H10Housing = 
    new G4UnionSolid("Housing",H10Housing_Half1_4,H10Housing_Half4_1,
		     G4Transform3D(H10UnionRotH,H10UnionTransH));

  G4LogicalVolume *LH10Housing;

  LH10Housing = new G4LogicalVolume(H10Housing, matAluminium, "Alu");
  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  G4VPhysicalVolume *H10;

  G4ThreeVector H10Trans;
  G4RotationMatrix H10Rot;

  H10Rot.rotateX(-Angle_To_Detector[0][0]);
  H10Rot.rotateZ(Angle_To_Detector[0][1]);
  H10Rot.rotateY(0);

  
  H10Trans(1) = H10Trans(0) = 0;
  H10Trans(2) = Distance_From_Focus_To_Backplane;
  H10Trans = H10Trans.transform(H10Rot) - H10HexOrigo.transform(H10Rot);
  H10Trans += NWallTrans;
  H10Transform = nwallrot*G4Translate3D(H10Trans)*G4Rotate3D(H10Rot);  

  H10 = new G4PVPlacement(H10Transform,
			  "H10",LH10Housing,theDetector->HallPhys(),false,
				    10);
  

  //Adding Sci. volume
  G4ThreeVector H10Trans_Sci_In_Housing;
  G4RotationMatrix H10Rot_Sci_In_Housing;

  H10Trans_Sci_In_Housing(2) = 
   - (Hexagon1_Bottom_Thickness - Hexagon1_Top_Thickness)/2;
  H10Trans_Sci_In_Housing(1) = - (H10HH4_1Vertices[0].y() - 
				  H10SciH4_1Vertices[0].y());
   

  G4VPhysicalVolume *PH10scintillator;
  PH10scintillator = new G4PVPlacement(G4Transform3D(H10Rot_Sci_In_Housing,
						     H10Trans_Sci_In_Housing),
				       "H10PSci",LH10scintillator,H10,false,0);


  //Adding walls in scintillator
  G4ThreeVector aTranslH10InWall0;
  //Vector from origo of Scintillator volume to "detector origo"
  //used for all walls!
  aTranslH10InWall0(0) = 0;
  aTranslH10InWall0(1) = (Hexagon1_Center_to_Side_Sci_Bottom[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_inside_height/2) +
    (H10SciH1_4Vertices[2](1)+H10SciH1_4Vertices[6](1))/2;
  aTranslH10InWall0(2) = 0;
 

  //Wall one 


  //G4VPhysicalVolume *PH10InWall0;
  G4Transform3D TransformH10InWall0;
  G4ThreeVector bTranslH10InWall0;
  
  bTranslH10InWall0(2) = 0;
  bTranslH10InWall0(0) = 0;
  bTranslH10InWall0(1) = -H10InWall0Vertices[4].y();
  bTranslH10InWall0.rotateZ(-90*deg+Hexagon1_Side_To_xyplane_Angles[0]);
  TransformH10InWall0 = G4Translate3D(bTranslH10InWall0)*
    G4Translate3D(aTranslH10InWall0)*
    G4RotateZ3D(-90*deg+Hexagon1_Side_To_xyplane_Angles[0]);
  /*PH10InWall0 =*/ new G4PVPlacement(TransformH10InWall0, "H10PInWall0",
				  LH10InWall0,PH10scintillator,false,0);


  //Wall two


  //G4VPhysicalVolume *PH10InWall1;
  G4Transform3D TransformH10InWall1;
  G4ThreeVector bTranslH10InWall1;
  
  bTranslH10InWall1(2) = 0;
  bTranslH10InWall1(0) = 0;
  bTranslH10InWall1(1) = -H10InWall0Vertices[4].y();
  bTranslH10InWall1.rotateZ(-90*deg+Hexagon1_Side_To_xyplane_Angles[2]);
  TransformH10InWall1 = G4Translate3D(bTranslH10InWall1)*
    G4Translate3D(aTranslH10InWall0)*
    G4RotateZ3D(-90*deg+Hexagon1_Side_To_xyplane_Angles[2]);
  /*PH10InWall1 =*/ new G4PVPlacement(TransformH10InWall1, "H10PInWall1",
				  LH10InWall0,PH10scintillator,false,0);


  //Wall three


  //G4VPhysicalVolume *PH10InWall2;
  G4Transform3D TransformH10InWall2;
  G4ThreeVector bTranslH10InWall2;
  
  bTranslH10InWall2(2) = 0;
  bTranslH10InWall2(0) = 0;
  bTranslH10InWall2(1) = -H10InWall0Vertices[4].y();
  bTranslH10InWall2.rotateZ(-90*deg+Hexagon1_Side_To_xyplane_Angles[4]);
  TransformH10InWall2 = G4Translate3D(bTranslH10InWall2)*
    G4Translate3D(aTranslH10InWall0)*
    G4RotateZ3D(-90*deg+Hexagon1_Side_To_xyplane_Angles[4]);
  /*PH10InWall2 =*/ new G4PVPlacement(TransformH10InWall2, "H10PInWall2",
				  LH10InWall0,PH10scintillator,false,0);


  //Adding vacume volume
  G4ThreeVector H10Trans_Vac_In_Housing;
  G4RotationMatrix H10Rot_Vac_In_Housing;
  
  H10Trans_Vac_In_Housing(2) = 
    (Hexagon1_Half_Length - Hexagon1_Bottom_Wall_Depth/2);
  H10Trans_Vac_In_Housing(1) = - (H10HH4_1Vertices[0].y() - 
				  H10VacH4_1Vertices[0].y());
   

  /*G4VPhysicalVolume *PH10Vacume;
  PH10Vacume =*/ new G4PVPlacement(G4Transform3D(H10Rot_Vac_In_Housing,
					       H10Trans_Vac_In_Housing),
				       "H10PVac",LH10Vacume,H10,false,0);
  
  
  LH10Housing -> SetVisAttributes(Alu);
  LH10scintillator -> SetVisAttributes(Wall);
  LH10Vacume -> SetVisAttributes(Vac);
  LH10InWall0 -> SetVisAttributes(Alu);
  //////////////////////////////////////////////////////////////////
  //H11 detector                                                  //
  //////////////////////////////////////////////////////////////////


  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H11;

  G4ThreeVector H11Trans;
  G4RotationMatrix H11Rot, H11bRot;

  G4ThreeVector H11HexOrigo;
  H11HexOrigo(0) = 0;
  H11HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H11HexOrigo(2) = Hexagon1_Half_Length;

  H11Rot.rotateX(-Angle_To_Detector[1][0]);
  H11Rot.rotateZ(Angle_To_Detector[1][1]);
  H11Rot.rotateY(0);

  H11Trans(1) = H11Trans(0) = 0;
  H11Trans(2) = Distance_From_Focus_To_Backplane;
  
  
  H11Trans =  H11Trans.transform(H11Rot) - H11HexOrigo.transform(H11Rot) + 
    NWallTrans;
    
  H11Transform = nwallrot*G4Translate3D(H11Trans)*G4Rotate3D(H11Rot);
  /*H11 =*/ new G4PVPlacement(H11Transform,
			  "H11",LH10Housing,theDetector->HallPhys(),false,
				    11);
  

  //////////////////////////////////////////////////////////////////
  //H12 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H12;

  G4ThreeVector H12Trans;
  G4RotationMatrix H12Rot;
  
  G4ThreeVector H12HexOrigo;
  H12HexOrigo(0) = 0;
  H12HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		    tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H12HexOrigo(2) = Hexagon1_Half_Length;
  

  H12Rot.rotateX(-Angle_To_Detector[2][0]);
  H12Rot.rotateZ(Angle_To_Detector[2][1]);
  H12Rot.rotateY(0);
  
  H12Trans(1) = H12Trans(0) = 0;
  H12Trans(2) = Distance_From_Focus_To_Backplane;
  H12Trans = H12Trans.transform(H12Rot) - H12HexOrigo.transform(H12Rot) +
    NWallTrans;
  
 H12Transform = nwallrot*G4Translate3D(H12Trans)*G4Rotate3D(H12Rot);
  
  /*H12 =*/ new G4PVPlacement(H12Transform,
			  "H12",LH10Housing,theDetector->HallPhys(),false,
				    12);
  
 
  //////////////////////////////////////////////////////////////////
  //H13 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H13;

  G4ThreeVector H13Trans;
  G4RotationMatrix H13Rot;

  G4ThreeVector H13HexOrigo;
  H13HexOrigo(0) = 0;
  H13HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H13HexOrigo(2) = Hexagon1_Half_Length;
 

  H13Rot.rotateX(-Angle_To_Detector[3][0]);
  H13Rot.rotateZ(Angle_To_Detector[3][1]);
  H13Rot.rotateY(0);

  H13Trans(1) = H13Trans(0) = 0;
  H13Trans(2) = Distance_From_Focus_To_Backplane;
  H13Trans = H13Trans.transform(H13Rot) - H13HexOrigo.transform(H13Rot) + 
    NWallTrans;
    
  H13Transform = nwallrot*G4Translate3D(H13Trans)*G4Rotate3D(H13Rot);
  /*H13 =*/ new G4PVPlacement(H13Transform,
			  "H13",LH10Housing,theDetector->HallPhys(),false,
				    13);
  

  //////////////////////////////////////////////////////////////////
  //H14 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H14;

  G4ThreeVector H14Trans;
  G4RotationMatrix H14Rot;

  G4ThreeVector H14HexOrigo;
  H14HexOrigo(0) = 0;
  H14HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H14HexOrigo(2) = Hexagon1_Half_Length;
 

  H14Rot.rotateX(-Angle_To_Detector[4][0]);
  H14Rot.rotateZ(Angle_To_Detector[4][1]);
  H14Rot.rotateY(0);

  H14Trans(1) = H14Trans(0) = 0;
  H14Trans(2) = Distance_From_Focus_To_Backplane;
  H14Trans = H14Trans.transform(H14Rot) - H14HexOrigo.transform(H14Rot) + 
    NWallTrans;
  
  H14Transform = nwallrot*G4Translate3D(H14Trans)*G4Rotate3D(H14Rot);

  /*H14 =*/ new G4PVPlacement(H14Transform,
			  "H14",LH10Housing,theDetector->HallPhys(),false,
				    14);

  //////////////////////////////////////////////////////////////////
  //H15 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H15;

  G4ThreeVector H15Trans;
  G4RotationMatrix H15Rot;
  

  G4ThreeVector H15HexOrigo;
  H15HexOrigo(0) = 0;
  H15HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H15HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H15Rot.rotateX(-Angle_To_Detector[5][0]);
  H15Rot.rotateZ(Angle_To_Detector[5][1]);
  H15Rot.rotateY(0);

  H15ZRot = G4Rotate3D(Angle_To_Detector[5][2], G4ThreeVector(0,0,1));
  H15HexOrigo.rotateZ(Angle_To_Detector[5][2]);
  
  H15Trans(1) = H15Trans(0) = 0;
  H15Trans(2) = Distance_From_Focus_To_Backplane;
  H15Trans = H15Trans.transform(H15Rot) - H15HexOrigo.transform(H15Rot) + 
    NWallTrans;
  
  
  H15Transform = nwallrot*G4Transform3D(H15Rot,H15Trans)*H15ZRot;
    

  /*H15 =*/ new G4PVPlacement(H15Transform,"H15",LH10Housing,theDetector->HallPhys(),false,15);

  //////////////////////////////////////////////////////////////////
  //H26 detector                                                  //
  //////////////////////////////////////////////////////////////////


  G4Trap *H26Sci_Half1_4, *H26Sci_Half4_1;

  G4ThreeVector H26SciH1_4Vertices[8];

  H26SciH1_4Vertices[4] = Sci_Bottom_Corners[1];
  H26SciH1_4Vertices[5] = Sci_Bottom_Corners[4];
  H26SciH1_4Vertices[7] = Sci_Bottom_Corners[3];
  H26SciH1_4Vertices[6] = Sci_Bottom_Corners[2];
  H26SciH1_4Vertices[0] = Sci_Top_Corners[1];
  H26SciH1_4Vertices[1] = Sci_Top_Corners[4];
  H26SciH1_4Vertices[3] = Sci_Top_Corners[3];
  H26SciH1_4Vertices[2] = Sci_Top_Corners[2];
  
  y_correction = (H26SciH1_4Vertices[0].y() + H26SciH1_4Vertices[2].y() + 
		  H26SciH1_4Vertices[4].y() + H26SciH1_4Vertices[6].y())/4;
  z_correction = Distance_From_Focus_To_Backplane  - Hexagon1_Bottom_Thickness
    - Hexagon1_inside_height/2;

 
   for (i=0; i<8; i++)
    {
      H26SciH1_4Vertices[i](1)  = H26SciH1_4Vertices[i](1) - y_correction;
      H26SciH1_4Vertices[i](2)  = H26SciH1_4Vertices[i](2) - z_correction;
    }

   fixcorners(H26SciH1_4Vertices);

  H26Sci_Half1_4 = new 
    G4Trap("Sci_Half1_4",H26SciH1_4Vertices);
  

  G4ThreeVector H26SciH4_1Vertices[8];
  
  H26SciH4_1Vertices[4] = Sci_Bottom_Corners[4];
  H26SciH4_1Vertices[5] = Sci_Bottom_Corners[1];
  H26SciH4_1Vertices[7] = Sci_Bottom_Corners[0];
  H26SciH4_1Vertices[6] = Sci_Bottom_Corners[5];
  H26SciH4_1Vertices[0] = Sci_Top_Corners[4];
  H26SciH4_1Vertices[1] = Sci_Top_Corners[1];
  H26SciH4_1Vertices[3] = Sci_Top_Corners[0];
  H26SciH4_1Vertices[2] = Sci_Top_Corners[5];
  
  y_correction = (H26SciH4_1Vertices[0].y() + H26SciH4_1Vertices[2].y() + 
		  H26SciH4_1Vertices[4].y() + H26SciH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H26SciH4_1Vertices[i](1)  = H26SciH4_1Vertices[i](1) - y_correction;
      H26SciH4_1Vertices[i](2)  = H26SciH4_1Vertices[i](2) - z_correction;
    }
  
  fixcorners(H26SciH4_1Vertices);

  H26Sci_Half4_1 = new 
    G4Trap("Sci_Half4_1",H26SciH4_1Vertices);
  
  G4UnionSolid *H26scintillator;

  G4ThreeVector H26UnionTrans;
  G4RotationMatrix H26UnionRot;
  H26UnionRot.rotateZ(0*deg);
  
  H26UnionTrans(1)= ((Sci_Bottom_Corners[1](1)-Sci_Bottom_Corners[2](1))/2+
		     (Sci_Top_Corners[1](1)-Sci_Top_Corners[2](1))/2)/2 +
    ((Sci_Bottom_Corners[0](1)-Sci_Bottom_Corners[1](1))/2+
     (Sci_Top_Corners[0](1)-Sci_Top_Corners[1](1))/2)/2;// - 0.001*mm;


  H26scintillator = new G4UnionSolid("H26scintillator",H26Sci_Half1_4,
				     H26Sci_Half4_1,
				     G4Transform3D(H26UnionRot,H26UnionTrans));

  G4LogicalVolume *LH26scintillator;

  LH26scintillator = new G4LogicalVolume(H26scintillator, matNWall, "H26Sci");
  LH26scintillator->SetSensitiveDetector(ancSD);
  /////////////////////////////////////////////////////////////////////////
  //Container splitting walls                                            //
  /////////////////////////////////////////////////////////////////////////
  G4Trap *H26InWall0;
  
  G4ThreeVector H26InWall0Vertices[8];

  H26InWall0Vertices[4] = Hexagon2_Coor_Inner_Walls[0][2];
  H26InWall0Vertices[5] = Hexagon2_Coor_Inner_Walls[0][1];
  H26InWall0Vertices[7] = Hexagon2_Coor_Inner_Walls[0][0];
  H26InWall0Vertices[6] = Hexagon2_Coor_Inner_Walls[0][3];
  H26InWall0Vertices[0] = Hexagon2_Coor_Inner_Walls[1][2];
  H26InWall0Vertices[1] = Hexagon2_Coor_Inner_Walls[1][1];
  H26InWall0Vertices[3] = Hexagon2_Coor_Inner_Walls[1][0];
  H26InWall0Vertices[2] = Hexagon2_Coor_Inner_Walls[1][3];
  
  for (i=0; i<8; i++)
    {
      H26InWall0Vertices[i].rotateZ(92.8*deg);
    }

  y_correction = (H26InWall0Vertices[0].y() + H26InWall0Vertices[2].y() + 
		  H26InWall0Vertices[4].y() + H26InWall0Vertices[6].y())/4;
  z_correction = Distance_From_Focus_To_Backplane  - Hexagon1_Bottom_Thickness
    - Hexagon1_inside_height/2;

 
   for (i=0; i<8; i++)
    {
      H26InWall0Vertices[i](1)  = H26InWall0Vertices[i](1) - 
	y_correction;
      H26InWall0Vertices[i](2)  = H26InWall0Vertices[i](2) - 
	z_correction;
    } 

   fixcorners(H26InWall0Vertices);

  H26InWall0 = new 
    G4Trap("InWall0",H26InWall0Vertices);

  G4LogicalVolume *LH26InWall0;
  
  LH26InWall0 = new G4LogicalVolume(H26InWall0,matAluminium,"H26InWall0");
  
  /////////////////////////////////////////////////////////////////////////
  //Top vacume                                                           //
  /////////////////////////////////////////////////////////////////////////
  G4Trap *H26Vac_Half1_4, *H26Vac_Half4_1;

  G4ThreeVector H26VacH1_4Vertices[8];

  H26VacH1_4Vertices[4] = Bottom_Corners_Inside[1];
  H26VacH1_4Vertices[5] = Bottom_Corners_Inside[4];
  H26VacH1_4Vertices[7] = Bottom_Corners_Inside[3];
  H26VacH1_4Vertices[6] = Bottom_Corners_Inside[2];
  H26VacH1_4Vertices[0] = Top_Corners_Inside[1];
  H26VacH1_4Vertices[1] = Top_Corners_Inside[4];
  H26VacH1_4Vertices[3] = Top_Corners_Inside[3];
  H26VacH1_4Vertices[2] = Top_Corners_Inside[2];
  
  y_correction = (H26VacH1_4Vertices[0].y() + H26VacH1_4Vertices[2].y() + 
		  H26VacH1_4Vertices[4].y() + H26VacH1_4Vertices[6].y())/4;
  z_correction = Distance_From_Focus_To_Backplane  -
    Hexagon1_Bottom_Wall_Depth/2;

 
   for (i=0; i<8; i++)
    {
      H26VacH1_4Vertices[i](1)  = H26VacH1_4Vertices[i](1) - y_correction;
      H26VacH1_4Vertices[i](2)  = H26VacH1_4Vertices[i](2) - z_correction;
    }

   fixcorners(H26VacH1_4Vertices);



  H26Vac_Half1_4 = new 
    G4Trap("Sci_Half1_4",H26VacH1_4Vertices);
  

  G4ThreeVector H26VacH4_1Vertices[8];
  
  H26VacH4_1Vertices[4] = Bottom_Corners_Inside[4];
  H26VacH4_1Vertices[5] = Bottom_Corners_Inside[1];
  H26VacH4_1Vertices[7] = Bottom_Corners_Inside[0];
  H26VacH4_1Vertices[6] = Bottom_Corners_Inside[5];
  H26VacH4_1Vertices[0] = Top_Corners_Inside[4];
  H26VacH4_1Vertices[1] = Top_Corners_Inside[1];
  H26VacH4_1Vertices[3] = Top_Corners_Inside[0];
  H26VacH4_1Vertices[2] = Top_Corners_Inside[5];
  
  y_correction = (H26VacH4_1Vertices[0].y() + H26VacH4_1Vertices[2].y() + 
		  H26VacH4_1Vertices[4].y() + H26VacH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H26VacH4_1Vertices[i](1)  = H26VacH4_1Vertices[i](1) - y_correction;
      H26VacH4_1Vertices[i](2)  = H26VacH4_1Vertices[i](2) - z_correction;
    }

  fixcorners(H26VacH4_1Vertices);
  
  H26Vac_Half4_1 = new 
    G4Trap("Sci_Half4_1",H26VacH4_1Vertices);
  
  G4UnionSolid *H26Vacume;

  G4ThreeVector H26UnionTransV;
  G4RotationMatrix H26UnionRotV;
  H26UnionRotV.rotateZ(0*deg);
  
  H26UnionTransV(1)= ((Bottom_Corners_Inside[1](1)-Bottom_Corners_Inside[2](1))/2+
		     (Top_Corners_Inside[1](1)-Top_Corners_Inside[2](1))/2)/2 +
    ((Bottom_Corners_Inside[0](1)-Bottom_Corners_Inside[1](1))/2+
     (Top_Corners_Inside[0](1)-Top_Corners_Inside[1](1))/2)/2;// - 0.001*mm;


  H26Vacume = new G4UnionSolid("H26Vacume",H26Vac_Half1_4,
				     H26Vac_Half4_1,
				     G4Transform3D(H26UnionRotV,
						   H26UnionTransV));

  G4LogicalVolume *LH26Vacume;

  LH26Vacume = new G4LogicalVolume(H26Vacume, matVacuum, "H26Vac");




  ///////////////////////////////////////////////////////////////
  //Then the housing                                           //
  ///////////////////////////////////////////////////////////////

  G4Trap *H26Housing_Half1_4, *H26Housing_Half4_1;



  G4ThreeVector H26HH1_4Vertices[8];

  H26HH1_4Vertices[4] = Bottom_Corners[1];
  H26HH1_4Vertices[5] = Bottom_Corners[4];
  H26HH1_4Vertices[7] = Bottom_Corners[3];
  H26HH1_4Vertices[6] = Bottom_Corners[2];
  H26HH1_4Vertices[0] = Top_Corners[1];
  H26HH1_4Vertices[1] = Top_Corners[4];
  H26HH1_4Vertices[3] = Top_Corners[3];
  H26HH1_4Vertices[2] = Top_Corners[2];
  
  y_correction = (H26HH1_4Vertices[0].y() + H26HH1_4Vertices[2].y() + 
		  H26HH1_4Vertices[4].y() + H26HH1_4Vertices[6].y())/4;
  z_correction = (-Hexagon1_Half_Length + Distance_From_Focus_To_Backplane);
   
  for (i=0; i<8; i++)
    {
      H26HH1_4Vertices[i](1)  = H26HH1_4Vertices[i](1) - y_correction;
      H26HH1_4Vertices[i](2)  = H26HH1_4Vertices[i](2) - z_correction;
    }
  
  
  fixcorners(H26HH1_4Vertices);

  H26Housing_Half1_4 = new 
    G4Trap("Housing_Half1_4",H26HH1_4Vertices); 
  
  G4ThreeVector H26HH4_1Vertices[8];
  
  H26HH4_1Vertices[4] = Bottom_Corners[4];
  H26HH4_1Vertices[5] = Bottom_Corners[1];
  H26HH4_1Vertices[7] = Bottom_Corners[0];
  H26HH4_1Vertices[6] = Bottom_Corners[5];
  H26HH4_1Vertices[0] = Top_Corners[4];
  H26HH4_1Vertices[1] = Top_Corners[1];
  H26HH4_1Vertices[3] = Top_Corners[0];
  H26HH4_1Vertices[2] = Top_Corners[5];
  
  y_correction = (H26HH4_1Vertices[0].y() + H26HH4_1Vertices[2].y() + 
		  H26HH4_1Vertices[4].y() + H26HH4_1Vertices[6].y())/4;
  
  for (i=0; i<8; i++)
    {
      H26HH4_1Vertices[i](1)  = H26HH4_1Vertices[i](1) - y_correction;
      H26HH4_1Vertices[i](2)  = H26HH4_1Vertices[i](2) - z_correction;
    }

  fixcorners(H26HH4_1Vertices);

  H26Housing_Half4_1 = new 
    G4Trap("Housing_Half4_1",H26HH4_1Vertices);
	      

  G4UnionSolid *H26Housing;

  G4ThreeVector H26UnionTransH;
  G4RotationMatrix H26UnionRotH;
  H26UnionRotH.rotateZ(0*deg);
  
  H26UnionTransH(1)= ((Bottom_Corners[1](1)-Bottom_Corners[2](1))/2+
		  (Top_Corners[1](1)-Top_Corners[2](1))/2)/2 +
    ((Bottom_Corners[0](1)-Bottom_Corners[1](1))/2+
     (Top_Corners[0](1)-Top_Corners[1](1))/2)/2;// - 0.001*mm;


  H26Housing = 
    new G4UnionSolid("Housing",H26Housing_Half1_4,H26Housing_Half4_1,
		     G4Transform3D(H26UnionRotH,H26UnionTransH));

  G4LogicalVolume *LH26Housing;

  LH26Housing = new G4LogicalVolume(H26Housing, matAluminium, "Alu");
  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  G4VPhysicalVolume *H26=NULL;

  G4ThreeVector H26Trans;
  G4RotationMatrix H26Rot;
  

  G4ThreeVector H26HexOrigo;
  H26HexOrigo(0) = 0;
  H26HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H26HH1_4Vertices[2](1)+H26HH1_4Vertices[6](1))/2;
  H26HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H26Rot.rotateX(-Angle_To_Detector[6][0]);
  H26Rot.rotateZ(Angle_To_Detector[6][1]);
  H26Rot.rotateY(0);

  H26ZRot = G4Rotate3D(Angle_To_Detector[6][2], G4ThreeVector(0,0,1));
  H26HexOrigo.rotateZ(Angle_To_Detector[6][2]);
  
  H26Trans(1) = H26Trans(0) = 0;
  H26Trans(2) = Distance_From_Focus_To_Backplane;
  H26Trans = H26Trans.transform(H26Rot) - H26HexOrigo.transform(H26Rot) + 
    NWallTrans;
  
  
  H26Transform = nwallrot*G4Transform3D(H26Rot,H26Trans)*H26ZRot;
    

  /*H26 =*/ new G4PVPlacement(H26Transform,"H26",LH26Housing,theDetector->HallPhys(),false,26);
  
  G4ThreeVector H26Trans_Sci_In_Housing;
  G4RotationMatrix H26Rot_Sci_In_Housing;

  H26Trans_Sci_In_Housing(2) = 
   - (Hexagon1_Bottom_Thickness - Hexagon1_Top_Thickness)/2;
  H26Trans_Sci_In_Housing(1) = - (H26HH4_1Vertices[0].y() - 
				  H26SciH4_1Vertices[0].y());
   

  G4VPhysicalVolume *PH26scintillator;
  PH26scintillator = new G4PVPlacement(G4Transform3D(H26Rot_Sci_In_Housing,
						     H26Trans_Sci_In_Housing),
				       "H26PSci",LH26scintillator,H26,false,0);
  
  //Adding walls in scintillator
  G4ThreeVector aTranslH26InWall0;
  //Vector from origo of Scintillator volume to "detector origo"
  //used for all walls!
  aTranslH26InWall0(0) = 0;
  aTranslH26InWall0(1) = (Hexagon1_Center_to_Side_Sci_Bottom[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_inside_height/2) +
    (H26SciH1_4Vertices[2](1)+H26SciH1_4Vertices[6](1))/2;
  aTranslH26InWall0(2) = 0;
 

  //Wall one 


  //G4VPhysicalVolume *PH26InWall0;
  G4Transform3D TransformH26InWall0;
  G4ThreeVector bTranslH26InWall0;
  
  bTranslH26InWall0(2) = 0;
  bTranslH26InWall0(0) = 0;
  bTranslH26InWall0(1) = -H26InWall0Vertices[4].y();
  bTranslH26InWall0.rotateZ(-92.8*deg);
  TransformH26InWall0 = G4Translate3D(bTranslH26InWall0)*
    G4Translate3D(aTranslH26InWall0)*
    G4RotateZ3D(-92.8*deg);
  /*PH26InWall0 = */ new G4PVPlacement(TransformH26InWall0, "H26PInWall0",
				  LH26InWall0,PH26scintillator,false,0);


  //Wall two


  //G4VPhysicalVolume *PH26InWall1;
  G4Transform3D TransformH26InWall1;
  G4ThreeVector bTranslH26InWall1;
  
  bTranslH26InWall1(2) = 0;
  bTranslH26InWall1(0) = 0;
  bTranslH26InWall1(1) = -H26InWall0Vertices[4].y();
  bTranslH26InWall1.rotateZ(-208.14*deg);
  TransformH26InWall1 = G4Translate3D(bTranslH26InWall1)*
    G4Translate3D(aTranslH26InWall0)*
    G4RotateZ3D(-208.14*deg);
  /*PH26InWall1 =*/ new G4PVPlacement(TransformH26InWall1, "H26PInWall1",
				  LH26InWall0,PH26scintillator,false,0);


  //Wall three


  //G4VPhysicalVolume *PH26InWall2;
  G4Transform3D TransformH26InWall2;
  G4ThreeVector bTranslH26InWall2;
  
  bTranslH26InWall2(2) = 0;
  bTranslH26InWall2(0) = 0;
  bTranslH26InWall2(1) = -H26InWall0Vertices[4].y();
  bTranslH26InWall2.rotateZ(-329.06*deg);
  TransformH26InWall2 = G4Translate3D(bTranslH26InWall2)*
    G4Translate3D(aTranslH26InWall0)*
    G4RotateZ3D(-329.06*deg);
  /*PH26InWall2 =*/ new G4PVPlacement(TransformH26InWall2, "H26PInWall2",
				  LH26InWall0,PH26scintillator,false,0);


  //Adding vacume volume
  G4ThreeVector H26Trans_Vac_In_Housing;
  G4RotationMatrix H26Rot_Vac_In_Housing;
  
  H26Trans_Vac_In_Housing(2) = 
    (Hexagon1_Half_Length - Hexagon1_Bottom_Wall_Depth/2);
  H26Trans_Vac_In_Housing(1) = - (H26HH4_1Vertices[0].y() - 
				  H26VacH4_1Vertices[0].y());
   

  /*G4VPhysicalVolume *PH26Vacume;
  PH26Vacume =*/ new G4PVPlacement(G4Transform3D(H26Rot_Vac_In_Housing,
					       H26Trans_Vac_In_Housing),
				       "H26PVac",LH26Vacume,H26,false,0);

  LH26Housing -> SetVisAttributes(Alu);
  LH26scintillator -> SetVisAttributes(Wall);
  LH26Vacume -> SetVisAttributes(Vac);
  LH26InWall0 -> SetVisAttributes(Alu);

  
  //////////////////////////////////////////////////////////////////
  //H17 detector                                                  //
  //////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H17;

  G4ThreeVector H17Trans;
  G4RotationMatrix H17Rot;
  

  G4ThreeVector H17HexOrigo;
  H17HexOrigo(0) = 0;
  H17HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H17HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H17Rot.rotateX(-Angle_To_Detector[7][0]);
  H17Rot.rotateZ(Angle_To_Detector[7][1]);
  H17Rot.rotateY(0);

  H17ZRot = G4Rotate3D(Angle_To_Detector[7][2], G4ThreeVector(0,0,1));
  H17HexOrigo.rotateZ(Angle_To_Detector[7][2]);
  
  H17Trans(1) = H17Trans(0) = 0;
  H17Trans(2) = Distance_From_Focus_To_Backplane;
  H17Trans = H17Trans.transform(H17Rot) - H17HexOrigo.transform(H17Rot) + 
    NWallTrans;
  
  
  H17Transform = nwallrot*G4Transform3D(H17Rot,H17Trans)*H17ZRot;
    

  /*H17 =*/ new G4PVPlacement(H17Transform,"H17",LH10Housing,theDetector->HallPhys(),false,17);
  

  //////////////////////////////////////////////////////////////////
  //H28 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H28;

  G4ThreeVector H28Trans;
  G4RotationMatrix H28Rot;
  

  G4ThreeVector H28HexOrigo;
  H28HexOrigo(0) = 0;
  H28HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H26HH1_4Vertices[2](1)+H26HH1_4Vertices[6](1))/2;
  H28HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H28Rot.rotateX(-Angle_To_Detector[8][0]);
  H28Rot.rotateZ(Angle_To_Detector[8][1]);
  H28Rot.rotateY(0);

  H28ZRot = G4Rotate3D(Angle_To_Detector[8][2], G4ThreeVector(0,0,1));
  H28HexOrigo.rotateZ(Angle_To_Detector[8][2]);
  
  H28Trans(1) = H28Trans(0) = 0;
  H28Trans(2) = Distance_From_Focus_To_Backplane;
  H28Trans = H28Trans.transform(H28Rot) - H28HexOrigo.transform(H28Rot) + 
    NWallTrans;
  
  
  H28Transform = nwallrot*G4Transform3D(H28Rot,H28Trans)*H28ZRot;
    

  /*H28 =*/ new G4PVPlacement(H28Transform,"H28",LH26Housing,theDetector->HallPhys(),false,28);
  

  //////////////////////////////////////////////////////////////////
  //H19 detector                                                  //
  //////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H19;

  G4ThreeVector H19Trans;
  G4RotationMatrix H19Rot;
  

  G4ThreeVector H19HexOrigo;
  H19HexOrigo(0) = 0;
  H19HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H19HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H19Rot.rotateX(-Angle_To_Detector[9][0]);
  H19Rot.rotateZ(Angle_To_Detector[9][1]);
  H19Rot.rotateY(0);

  H19ZRot = G4Rotate3D(Angle_To_Detector[9][2], G4ThreeVector(0,0,1));
  H19HexOrigo.rotateZ(Angle_To_Detector[9][2]);
  
  H19Trans(1) = H19Trans(0) = 0;
  H19Trans(2) = Distance_From_Focus_To_Backplane;
  H19Trans = H19Trans.transform(H19Rot) - H19HexOrigo.transform(H19Rot) + 
    NWallTrans;
  
  
  H19Transform = nwallrot*G4Transform3D(H19Rot,H19Trans)*H19ZRot;
    

  /*H19 =*/ new G4PVPlacement(H19Transform,"H19",LH10Housing,theDetector->HallPhys(),false,19);
  
 
  //////////////////////////////////////////////////////////////////
  //H210 detector                                                  //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H210;

  G4ThreeVector H210Trans;
  G4RotationMatrix H210Rot;
  

  G4ThreeVector H210HexOrigo;
  H210HexOrigo(0) = 0;
  H210HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H26HH1_4Vertices[2](1)+H26HH1_4Vertices[6](1))/2;
  H210HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H210Rot.rotateX(-Angle_To_Detector[10][0]);
  H210Rot.rotateZ(Angle_To_Detector[10][1]);
  H210Rot.rotateY(0);

  H210ZRot = G4Rotate3D(Angle_To_Detector[10][2], G4ThreeVector(0,0,1));
  H210HexOrigo.rotateZ(Angle_To_Detector[10][2]);
  
  H210Trans(1) = H210Trans(0) = 0;
  H210Trans(2) = Distance_From_Focus_To_Backplane;
  H210Trans = H210Trans.transform(H210Rot) - H210HexOrigo.transform(H210Rot) + 
    NWallTrans;
  
  
  H210Transform = nwallrot*G4Transform3D(H210Rot,H210Trans)*H210ZRot;
    

  /*H210 =*/ new G4PVPlacement(H210Transform,"H210",LH26Housing,theDetector->HallPhys(),false,210);
  

  //////////////////////////////////////////////////////////////////
  //H111 detector                                                 //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H111;

  G4ThreeVector H111Trans;
  G4RotationMatrix H111Rot;
  

  G4ThreeVector H111HexOrigo;
  H111HexOrigo(0) = 0;
  H111HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H111HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H111Rot.rotateX(-Angle_To_Detector[11][0]);
  H111Rot.rotateZ(Angle_To_Detector[11][1]);
  H111Rot.rotateY(0);

  H111ZRot = G4Rotate3D(Angle_To_Detector[11][2], G4ThreeVector(0,0,1));
  H111HexOrigo.rotateZ(Angle_To_Detector[11][2]);
  
  H111Trans(1) = H111Trans(0) = 0;
  H111Trans(2) = Distance_From_Focus_To_Backplane;
  H111Trans = H111Trans.transform(H111Rot) - H111HexOrigo.transform(H111Rot) + 
    NWallTrans;
  
  
  H111Transform = nwallrot*G4Transform3D(H111Rot,H111Trans)*H111ZRot;
    

  /*H111 =*/ new G4PVPlacement(H111Transform,"H111",LH10Housing,theDetector->HallPhys(),false,111);
  

  //////////////////////////////////////////////////////////////////
  //H212 detector                                                 //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H212;

  G4ThreeVector H212Trans;
  G4RotationMatrix H212Rot;
  

  G4ThreeVector H212HexOrigo;
  H212HexOrigo(0) = 0;
  H212HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H26HH1_4Vertices[2](1)+H26HH1_4Vertices[6](1))/2;
  H212HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H212Rot.rotateX(-Angle_To_Detector[12][0]);
  H212Rot.rotateZ(Angle_To_Detector[12][1]);
  H212Rot.rotateY(0);

  H212ZRot = G4Rotate3D(Angle_To_Detector[12][2], G4ThreeVector(0,0,1));
  H212HexOrigo.rotateZ(Angle_To_Detector[12][2]);
  
  H212Trans(1) = H212Trans(0) = 0;
  H212Trans(2) = Distance_From_Focus_To_Backplane;
  H212Trans = H212Trans.transform(H212Rot) - H212HexOrigo.transform(H212Rot) + 
    NWallTrans;
  
  
  H212Transform = nwallrot*G4Transform3D(H212Rot,H212Trans)*H212ZRot;
    

  /*H212 =*/ new G4PVPlacement(H212Transform,"H212",LH26Housing,theDetector->HallPhys(),false,212);
  

  //////////////////////////////////////////////////////////////////
  //H113 detector                                                 //
  //////////////////////////////////////////////////////////////////

  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H113;

  G4ThreeVector H113Trans;
  G4RotationMatrix H113Rot;
  

  G4ThreeVector H113HexOrigo;
  H113HexOrigo(0) = 0;
  H113HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H10HH1_4Vertices[2](1)+H10HH1_4Vertices[6](1))/2;
  H113HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H113Rot.rotateX(-Angle_To_Detector[13][0]);
  H113Rot.rotateZ(Angle_To_Detector[13][1]);
  H113Rot.rotateY(0);

  H113ZRot = G4Rotate3D(Angle_To_Detector[13][2], G4ThreeVector(0,0,1));
  H113HexOrigo.rotateZ(Angle_To_Detector[13][2]);
  
  H113Trans(1) = H113Trans(0) = 0;
  H113Trans(2) = Distance_From_Focus_To_Backplane;
  H113Trans = H113Trans.transform(H113Rot) - H113HexOrigo.transform(H113Rot) + 
    NWallTrans;
  
  
  H113Transform = nwallrot*G4Transform3D(H113Rot,H113Trans)*H113ZRot;
    

  /*H113 =*/ new G4PVPlacement(H113Transform,"H113",LH10Housing,theDetector->HallPhys(),false,113);

  //////////////////////////////////////////////////////////////////
  //H214 detector                                                 //
  //////////////////////////////////////////////////////////////////
  
  ////////////////////////////////////////////////////////////
  //Now put them together                                   //
  ////////////////////////////////////////////////////////////

  //G4VPhysicalVolume *H214;

  G4ThreeVector H214Trans;
  G4RotationMatrix H214Rot;
  

  G4ThreeVector H214HexOrigo;
  H214HexOrigo(0) = 0;
  H214HexOrigo(1) = (Hexagon1_Center_to_Side[2] - 
		 tan(Hexagon1_Side_Taper_Angle[2])*Hexagon1_Half_Length) +
    (H26HH1_4Vertices[2](1)+H26HH1_4Vertices[6](1))/2;
  H214HexOrigo(2) = Hexagon1_Half_Length;
 
  
  H214Rot.rotateX(-Angle_To_Detector[14][0]);
  H214Rot.rotateZ(Angle_To_Detector[14][1]);
  H214Rot.rotateY(0);

  H214ZRot = G4Rotate3D(Angle_To_Detector[14][2], G4ThreeVector(0,0,1));
  H214HexOrigo.rotateZ(Angle_To_Detector[14][2]);
  
  H214Trans(1) = H214Trans(0) = 0;
  H214Trans(2) = Distance_From_Focus_To_Backplane;
  H214Trans = H214Trans.transform(H214Rot) - H214HexOrigo.transform(H214Rot) + 
    NWallTrans;
  
  
  H214Transform = nwallrot*G4Transform3D(H214Rot,H214Trans)*H214ZRot;
    

  /*H214 =*/ new G4PVPlacement(H214Transform,"H214",
			   LH26Housing,theDetector->HallPhys(),false,214);
  return;
}


int AgataAncillaryNeutronWall::PentSeg(G4ThreeVector position)

{
  G4ThreeVector yhat;
  yhat(1) = 1;
  position(2) = 0;
  G4double angle = acos(yhat.dot(position.unit()));
  if (fabs(angle) <= 36*deg) return 0;
  if (fabs(angle) > 36*deg && fabs(angle) < 108*deg) 
    {
      if (position(0) >= 0) return 1;
      else
	return 4;
    }
  if (fabs(angle) >= 108*deg && fabs(angle) <= 180*deg) 
    {
      if (position(0) >= 0) return 2;
      else
	return 3;
    }
  return -1;
}


int AgataAncillaryNeutronWall::H2Seg(G4ThreeVector position)
 
{
  G4ThreeVector yhat;
  yhat(1) = 1;
  position(2) = 0;
  G4double angle = acos(yhat.dot(position.unit()));
  if (angle >= 92.8*deg && position(0) >= 0) return 5;
  if (angle >= 151.86*deg && position(0) <= 0) return 5;
  if (angle < 151.86*deg && angle > 30.94*deg && position(0) < 0) return 6;
  return 7;
}



int AgataAncillaryNeutronWall::H1Seg(G4ThreeVector position)

{
  G4ThreeVector yhat;
  yhat(1) = 1;
  position(2) = 0;
  G4double angle = acos(yhat.dot(position.unit()));
  if (angle >= 61.87*deg && position(0) > 0) return 5;
  if (angle > 61.87*deg && position(0) < 0) return 6;
  return 7;
}


G4int AgataAncillaryNeutronWall::ConvertSegmentNumber(G4int segment)

{
  if (segment==0) return 45;
  if (segment==1) return 49;
  if (segment==2) return 48;
  if (segment==3) return 47;
  if (segment==4) return 46;
  if (segment==5) return 35;
  if (segment==6) return 36;
  if (segment==7) return 25;
  if (segment==8) return 37;
  if (segment==9) return 38;
  if (segment==10) return 27;
  if (segment==11) return 39;
  if (segment==12) return 40;
  if (segment==13) return 29;
  if (segment==14) return 41;
  if (segment==15) return 42;
  if (segment==16) return 31;
  if (segment==17) return 43;
  if (segment==18) return 44;
  if (segment==19) return 33;
  if (segment==20) return 1;
  if (segment==21) return 0;
  if (segment==22) return 2;
  if (segment==23) return 4;
  if (segment==24) return 3;
  if (segment==25) return 26;
  if (segment==26) return 6;
  if (segment==27) return 5;
  if (segment==28) return 7;
  if (segment==29) return 9;
  if (segment==30) return 8;
  if (segment==31) return 28;
  if (segment==32) return 11;
  if (segment==33) return 10;
  if (segment==34) return 12;
  if (segment==35) return 14;
  if (segment==36) return 13;
  if (segment==37) return 30;
  if (segment==38) return 16;
  if (segment==39) return 15;
  if (segment==40) return 17;
  if (segment==41) return 19;
  if (segment==42) return 18;
  if (segment==43) return 32;
  if (segment==44) return 21;
  if (segment==45) return 20;
  if (segment==46) return 22;
  if (segment==47) return 24;
  if (segment==48) return 23;
  if (segment==49) return 34;
  return -1;
}


G4int AgataAncillaryNeutronWall::GetSegmentNumber(G4int /*offset*/, G4int nGe, 
					 G4ThreeVector position)


{
  G4int det = nGe;

  //////////////

  switch (det)
    {
    case 0:
      return       ConvertSegmentNumber(PentSeg(position));
    case 10:
      return       ConvertSegmentNumber(H1Seg(position));
    case 11:
      return       ConvertSegmentNumber(3 + H1Seg(position));
    case 12:
      return       ConvertSegmentNumber(6 + H1Seg(position));
    case 13:
      return       ConvertSegmentNumber(9 + H1Seg(position));
    case 14:
      return       ConvertSegmentNumber(12 + H1Seg(position));
    case 15:
      return       ConvertSegmentNumber(15 + H1Seg(position));
    case 26:
      return       ConvertSegmentNumber(18 + H2Seg(position));
    case 17:
      return       ConvertSegmentNumber(21 + H1Seg(position));
    case 28:
      return       ConvertSegmentNumber(24 + H2Seg(position));
    case 19:
      return       ConvertSegmentNumber(27 + H1Seg(position));
    case 210:
      return       ConvertSegmentNumber(30 + H2Seg(position));
    case 111:
      return       ConvertSegmentNumber(33 + H1Seg(position));
    case 212:
      return       ConvertSegmentNumber(36 + H2Seg(position));
    case 113:
      return       ConvertSegmentNumber(39 + H1Seg(position));
    case 214:
      return       ConvertSegmentNumber(42 + H2Seg(position));
    }
  /////////////




  return 0;
}


////////////////////////////////////////////////////////
///// Methods for the Messanger
//////////////////////////////////////////////////////

void AgataAncillaryNeutronWall::SetTranslatation(G4ThreeVector pos)

{
  NWallTrans = pos;
}


void AgataAncillaryNeutronWall::SetRotation(G4ThreeVector rot)

{
  nwallangle[0] = rot.x();
  nwallangle[1] = rot.y();
  nwallangle[2] = rot.z();
}


void AgataAncillaryNeutronWall::SetDistanceToFocus(G4double dist)

{
  Distance_From_Focus_To_Backplane = dist + 194.12*mm;
}


void AgataAncillaryNeutronWall::SetDepthOfLiquid(G4double depth)

{
  Hexagon1_inside_height = depth;
}


//////////////////////////////////
// The Messenger
/////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"



AgataAncillaryNeutronWallMessenger::AgataAncillaryNeutronWallMessenger(AgataAncillaryNeutronWall* NWallDet, G4String /*name*/):NWallDetector(NWallDet)
{
  NWallDetDir = new G4UIdirectory("/Agata/detector/ancillary/NWall/");
  NWallDetDir -> SetGuidance("Control of NWall detector construction.");


    NWallTranslation = new G4UIcmdWith3Vector("/Agata/detector/ancillary/NWall/SetTranslation",this);
  NWallTranslation -> SetGuidance("Set focus pos for NWall");
  NWallTranslation -> AvailableForStates(G4State_Idle);
  
  NWallRotation = new G4UIcmdWith3Vector("/Agata/detector/ancillary/NWall/SetRotation",this);
  NWallRotation -> SetGuidance("Set orientation for NWall");
  NWallRotation -> AvailableForStates(G4State_Idle);
  

  DistanceToFocus = new 
    G4UIcmdWithADoubleAndUnit("/Agata/detector/ancillary/NWall/DistanceToFocus",this);
  DistanceToFocus -> 
    SetGuidance("Distance from NWall to focus.");
  DistanceToFocus -> SetParameterName("Size",false);
  DistanceToFocus -> SetRange("Size>0.");
  DistanceToFocus -> SetUnitCategory("Length");
  DistanceToFocus -> AvailableForStates(G4State_Idle);

  DepthOfLiquid = new 
    G4UIcmdWithADoubleAndUnit("/Agata/detector/ancillary/NWall/DepthOfLiquid",this);
  DepthOfLiquid -> SetGuidance("Depth Of Scint. Liquid");
  DepthOfLiquid -> SetParameterName("Size",false);
  DepthOfLiquid -> SetRange("Size>0.");
  DepthOfLiquid -> SetUnitCategory("Length");
  DepthOfLiquid -> AvailableForStates(G4State_Idle);
  

}

AgataAncillaryNeutronWallMessenger::~AgataAncillaryNeutronWallMessenger()
{
  delete PlasticRadius;
  delete NWallDetDir;
}

void AgataAncillaryNeutronWallMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if(command==DistanceToFocus)
    {
      NWallDetector -> SetDistanceToFocus(DistanceToFocus -> 
					  GetNewDoubleValue(newValue));
    }
  if(command==DepthOfLiquid)
    {
      NWallDetector -> SetDepthOfLiquid(DepthOfLiquid -> 
					GetNewDoubleValue(newValue));
    }
  if(command==DistanceToFocus)
    {
      NWallDetector -> SetDistanceToFocus(DistanceToFocus -> 
					  GetNewDoubleValue(newValue));
    }

  
  
  if(command==NWallTranslation)
    {
      NWallDetector -> SetTranslatation(NWallTranslation->
				   GetNew3VectorValue(newValue));
    }
  if(command==NWallRotation)
    {
      NWallDetector -> SetRotation(NWallRotation->
				   GetNew3VectorValue(newValue));
    }
}

#endif








