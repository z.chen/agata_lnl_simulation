#include "CGaspBuffer.hh"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>

#include "G4Types.hh"
#include "G4String.hh"
#include "G4ios.hh"

#define BUFLENGTH 8192

using namespace std;


CGaspBuffer::CGaspBuffer()
{
  pData = new unsigned short int[BUFLENGTH];
  ResetBuffer();
  cout << " Buffer allocated and ready." << endl;
  
  currentRecord  = 0;
  runNumber      = 0;
  bufferPosition = 0;
  maxFileSize    = 2040109465; // 95% of 2GB
  fileNumber     = 0;
}

CGaspBuffer::~CGaspBuffer()
{
  if(pData) delete [] pData;
}

void CGaspBuffer::ResetBuffer()
{
  memset( pData, 0, BUFLENGTH*sizeof(short int) );
}

void CGaspBuffer::Reset()
{
  ResetBuffer();
}

void CGaspBuffer::RecordHeader()
{
  //G4cout << " New header " << G4endl;
  int size = 16;
  unsigned short int* header = new unsigned short int[size];
  memset( header, 0, size*sizeof(short int) );
  
  header[1] = (unsigned short int)(currentRecord >> 8);
  header[2] = (unsigned short int) runNumber;
  if( firstHeader ) {
    header[3] = (unsigned short int)0x4748;	// reduced GASP format
    firstHeader = false;
  }  
  else
    header[3] = (unsigned short int)0x4758;     // reduced GASP format
    
  
  memcpy( pData, header, size*sizeof(short int) );
  //for( int ii=0; ii<size; ii++ )
  //  pData[ii] = header[ii];
    
  bufferPosition = size;  
}

void CGaspBuffer::BeginOfRun( int number )
{
  SetRunNumber( number+1 );
  fileNumber = 0;
  firstHeader = true;
  
  OpenFile();
  RecordHeader();
  //RecordHeader();
  FlushBuffer();
}

void CGaspBuffer::OpenFile()
{
  char specname[32];
  
  if( fileNumber > 0 )
    sprintf(specname, "Run.%4.4d.%2.2d", runNumber, fileNumber);
  else  
    sprintf(specname, "Run.%4.4d", runNumber);
    
  fout.open(specname, ios::out | ios::binary);
  if(!fout.is_open()) {
    cout << " Could not open output file (" << specname << ")! " << endl;
    return;
  }
  else {
    cout << specname << " opened." << endl;
    fileNumber++;
  }      
}

void CGaspBuffer::CloseFile()
{
  fout.close();
  cout << " Run file closed." << endl;
}

void CGaspBuffer::SplitFile()
{
  CloseFile();
  OpenFile();
}

void CGaspBuffer::EndOfRun()
{
  FlushBuffer();
  CloseFile();
}

void CGaspBuffer::FlushBuffer()
{
  if( ( sizeof(short int)*BUFLENGTH + (unsigned int)GetFileSize() ) >= (unsigned int)maxFileSize )
    SplitFile();
  
  fout.write((const char *)pData, sizeof(short int)*BUFLENGTH);
  memset( pData, 0, BUFLENGTH*sizeof(short int) );
  currentRecord++;
  //cout << " Flushed buffer ... " << currentBuffer << endl;
  RecordHeader();

}

void CGaspBuffer::AddToBuffer( unsigned short int* event, int size )
{
  if( (bufferPosition + size) >= BUFLENGTH )
    FlushBuffer();
    
  //cout << " currentBuffer is " << currentBuffer << " " << bufferPosition << endl;  
  
  //for( int ii=0; ii<size; ii++ )
  //  pData[bufferPosition+ii] = event[ii];
  
  memcpy( pData+bufferPosition, event, size*sizeof(short int) );
  bufferPosition += size;
  
}

void CGaspBuffer::SetMaxFileSize( G4int size )
{
  if( size > 0 )
    maxFileSize = size;
}
