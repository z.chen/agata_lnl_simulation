#include "AgataAnalysis.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataGeneratorAction.hh"
#include "AgataEventAction.hh"
#include "AgataPhysicsList.hh"
#include "AgataHitDetector.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include <cstdio>
#include <cmath>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "CLHEP/Random/RandomEngine.h"
#include "Randomize.hh"

#include "CGaspBuffer.hh"
#include "CSpec1D.hh"
// uncomment if matrices are needed!
//#include "CSpec2D.hh"

using namespace std;


AgataAnalysis::AgataAnalysis(G4String name)
{

  // buffers
  geAccu    = NULL;
  gtAccu    = NULL;
  anAccu    = NULL;
  agAccu    = NULL;
  cluAccu   = NULL;
  det2clust = NULL;
  
  aLine       = new char[100];
  
  position    = 0;
  runNumber   = 0;
  
  nDetectors  = 0;
  nClusters   = 0;
  nInstances  = 0;
  nAncil      = 0;
  

  maxFileSize = 2136997888; // 95% of 2GB
  fileNumber  = 0;
  
  // Energy resolution modelled as FWHM = sqrt( A + B E )
  CalculateResolution( 122.*keV, 1.1*keV,       // preset standard values for A and B
                      1333.*keV, 1.9*keV,
                      false);
  FWHMk       = 1./(sqrt(8.*log(2.)));  // sigma = FWHM/2.355
  
  // buffer
  gaspBuffer    = NULL;
  gaspEvent     = NULL;
  // ge energy and time calibration
  gf1           = 0.25*keV;
  gf2           = 0.1*ns;
  fThreshold    = 1.0*keV;
  ancillaryOnly = 0;
  
  
  // spectra
  specLength        = 4096;        //> length of spectra
  offset            = 0.;          //> offset for spectra
  gain              = 1.;          //> gain   for spectra

  // default: disable analysis; if enabled, writes file in GASP format
  enabledAnalysis       = false;
  packEvent             = false;       // don't pack multievent-gammas
  makeSpectra           = false;
  makeBuffer            = true;
  asciiSpectra          = false;       //> default: binary spectra
  enabledResolution     = false;       // energy resolution on/off

  myMessenger = new AgataAnalysisMessenger(this, name);
}

AgataAnalysis::~AgataAnalysis()
{
  spec1.clear();
  // uncomment if matrices are needed!
  //spec2.clear();

  if( geAccu    ) delete [] geAccu;  // ge energy
  if( gtAccu    ) delete [] gtAccu;  // ge time
  if( cluAccu   ) delete [] cluAccu; // cluster energy
  if( det2clust ) delete [] det2clust;
  if( anAccu    ) delete [] anAccu;  // ancillary detectors
  if( agAccu    ) delete [] agAccu;  // ancillary detectors --> good or not
  
  if( gaspEvent ) delete [] gaspEvent;
  
  delete [] aLine;
}

//////////////////////////////////////////////////////////////
//// General "service" methods ///////////////////////////////
//////////////////////////////////////////////////////////////
void AgataAnalysis::AnalysisStart(G4int run)
{
  if(!enabledAnalysis)
    return;
    
  if( (!makeSpectra) && (!makeBuffer) ) {
    G4cout << " ---> Warning! Nothing to do, disabling analysis ... " << G4endl;
    this->EnableAnalysis(false);
    return;
  } 

  G4RunManager * runManager = G4RunManager::GetRunManager();

  theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theEvent     = (AgataEventAction*)          runManager->GetUserEventAction();
  
  if( (theGenerator->GetBeginOfEventTag() != "") && (!packEvent) )
    SetPackEvent( true );

  if(packEvent && theGenerator->GetCascadeMult() > 1)
    scaleEtot = 0.1;
  else if(packEvent && (theGenerator->GetBeginOfEventTag() != "") ) 
    scaleEtot = 0.1;
  else
    scaleEtot = 1.;
  
  // gain factor for ancillary detectors
  if( packEvent )
    gf3 = 10.*keV; // writes out at 100 ch/MeV
  else
    gf3 =  5.*keV;

  runNumber   = run;
  
  nDetectors  = theDetector->GetMaxDetectorIndex() + 1;          // safe allocation: this number was already calculated leaving some margin
  nClusters   = theDetector->GetMaxClusterIndex()  + 1;    
  
  nAncil      = theDetector->HowManyOffset(); 
  nInstances  = 0;                                               // how many different ancillary detectors
  
  
  /////////////////////
  // several buffers
  ////////////////////
  if( geAccu    ) delete [] geAccu;  // ge energy
  if( gtAccu    ) delete [] gtAccu;  // ge time
  if( cluAccu   ) delete [] cluAccu; // cluster energy
  if( det2clust ) delete [] det2clust;
  if( anAccu    ) delete [] anAccu;  // ancillary detectors
  if( agAccu    ) delete [] agAccu;  // ancillary detectors --> good or not

  parLut.clear();
  ancLut.clear();
  minLut.clear();
  
  // germanium detectors
  geAccu    = new G4double [nDetectors];
  memset(geAccu,   0, nDetectors*sizeof(G4double));
  gtAccu    = new G4double [nDetectors];
  memset(gtAccu,   0, nDetectors*sizeof(G4double));
  
  det2clust = new G4int [nDetectors];
  memset(det2clust,0, nDetectors*sizeof(G4int));
  
  minGeAccu = nDetectors;
  maxGeAccu = 0;

  cluAccu   = new G4double [nClusters];
    
  // ancillary detectors: they have been created only when nAncil > 1
  // this will most likely crash with fixed offsets (which are not recommended anyway ...)
  if( nAncil > 1 ) {
    anAccu   = new G4double[nDetectors*nAncil];
    memset(anAccu,   0, nDetectors*nAncil*sizeof(G4double));
    minAnAccu = nDetectors*nAncil;
    maxAnAccu = 0;
    
    G4int ii;
    // easy task: lookup table offset ==> ancillary (copy what is stored into DetectorConstruction)
    ancLut.resize(nAncil);
    for( ii=1; ii<nAncil; ii++ )
      ancLut[ii] = theDetector->GetOffset(ii);
      
    // lookup table ancillary ==> how many parameters?
    parLut.resize(nAncil);
    minLut.resize(nAncil);
    for( ii=0; ii<nAncil; ii++ ) {
      parLut[ii] = 0;
      minLut[ii] = nAncil;
    }  
    
    for( ii=1; ii<nAncil; ii++ ) {
      parLut[theDetector->GetOffset(ii)]++;  // counts how many times the given ancillary is called (how many offsets ==> how many parameters)
      if( ii < minLut[theDetector->GetOffset(ii)] )
        minLut[theDetector->GetOffset(ii)] = ii;    // first offset corresponding to a given ancillary
    }  
      
    for( ii=0; ii<nAncil; ii++ ) {
      if( parLut[ii] > 0 ) 
        nInstances++;    // counts how many ancillaries have at least a parameter
    }
    
    // flag: ancillary fired or not
    agAccu   = new G4int[nDetectors*nInstances];
    memset(agAccu,   0, nDetectors*nInstances*sizeof(G4int));
    minAgAccu = nDetectors*nInstances;
    maxAgAccu = 0;
  }
  
  /////////////////////
  /// Create buffer
  ////////////////////
  fileNumber = 0;
  
  if( makeBuffer ) {
    if( !gaspBuffer )                               
      gaspBuffer = new CGaspBuffer();               
    gaspBuffer->Reset();                            
    gaspBuffer->BeginOfRun( runNumber );            
    gaspBuffer->SetMaxFileSize( maxFileSize );      
    if( !gaspEvent )                                
      gaspEvent = new unsigned short int[2048];      
  }

  /////////////////////
  /// Create spectra
  ////////////////////
  if( makeSpectra ) {
    spec1.clear();
    // uncomment if matrices are needed!
    //spec2.clear();

    spec1.push_back(new CSpec1D(specLength, 1));           //> total spectrum
    spec1.push_back(new CSpec1D(specLength, nDetectors));  //> spectrum of detectors
    spec1.push_back(new CSpec1D(specLength, nClusters));   //> spectrum of clusters
    spec1.push_back(new CSpec1D(specLength, 1));           //> sum of spectrum of detectors

    // uncomment if matrices are needed!
    // spec2.push_back(new CSpec2D(256, 256, 1));
  }
}

void AgataAnalysis::AnalysisEnd( G4String workPath )
{
  if(!enabledAnalysis)
    return;
    
  if( makeBuffer )
    gaspBuffer->EndOfRun();
    
  if( makeSpectra ) 
    FlushSpectra( workPath );

}

void AgataAnalysis::EndOfEvent()
{
  if(!enabledAnalysis)
    return;

  if( packEvent && !theGenerator->IsEndOfEvent() )
    return;

  ApplyResolution();
  WriteGaspEvent();
  IncrementSpectra();
  ResetBuffers();
  return;
}

void AgataAnalysis::ResetBuffers()
{
  if( nDetectors ) {
    memset(geAccu,   0, nDetectors*sizeof(G4double));
    memset(gtAccu,   0, nDetectors*sizeof(G4double));
    memset(cluAccu,  0, nClusters *sizeof(G4double));

    minGeAccu = nDetectors;                           //> we have erased detAccu in the loop
    maxGeAccu = 0;                                    //> so we prepare it for the next event
  }
  if( nInstances ) {
    memset(anAccu,   0, nDetectors*nAncil    *sizeof(G4double));
    memset(agAccu,   0, nDetectors*nInstances*sizeof(G4int));

    minAgAccu = nDetectors*nInstances;
    maxAgAccu = 0;
  }
}

G4double AgataAnalysis::EnergyResolution(double value)
{
  // resolution modelled as FWHM = sqrt( A + B E )
  // FWHMk = 1/2.355
  G4double dummy = G4RandGauss::shoot( value, FWHMk*sqrt( FWHMa + FWHMb*value ) );
  if( dummy < 0. ) 
    dummy = 0.;
  return dummy;
}

void AgataAnalysis::ApplyResolution()
{
  if(!enabledAnalysis)
    return;
  
  if( !enabledResolution )
    return;

  if( minGeAccu > maxGeAccu )
    return;
  
  for( G4int ii=minGeAccu; ii<=maxGeAccu; ii++ ) {
    if( geAccu[ii] ) {
      geAccu[ii] = EnergyResolution(geAccu[ii]);
      if( geAccu[ii] < 0. )
        geAccu[ii] = 0.;
    }
  }  	
}



////////////////////////////////////////////////////////////
/////// Treatment of the hits  /////////////////////////////
//////  Get hit object and dispatch relevant information ///
////////////////////////////////////////////////////////////
void AgataAnalysis::AddHit( AgataHitDetector* theHit )
{
  if(!enabledAnalysis)
    return;
  AddEnergy   ( theHit->GetEdep(), theHit->GetDetNb(), theHit->GetCluNb() );
  AddTime     ( theHit->GetTime(), theHit->GetEdep(), theHit->GetDetNb() );
}

void AgataAnalysis::AddEnergy( G4double energy, G4int detnum, G4int clunum )
{
  if( detnum < 1000 )
    AddGermaniumEnergy( energy, detnum, clunum );
  else 
    AddAncillaryEnergy( energy, detnum );
}

void AgataAnalysis::AddGermaniumEnergy( G4double energy, G4int detnum, G4int clunum )
{
  if(detnum >= 0 && detnum < nDetectors) {
    geAccu[detnum] += energy;
    if(detnum < minGeAccu) minGeAccu = detnum;
    if(detnum > maxGeAccu) maxGeAccu = detnum;
    det2clust[detnum] = clunum;
  }
}

void AgataAnalysis::AddAncillaryEnergy( G4double energy, G4int detcode )
{
  G4int detnum = detcode%1000;
  
  if(detnum >= 0 && detnum < nDetectors) {
    G4int ofset = detcode/1000;
    G4int whichAnc = ancLut[ofset];

    anAccu[detnum + ofset*nDetectors]  += energy;
    agAccu[detnum + whichAnc*nDetectors] = 1;  // it fired!

    if(detnum < minAnAccu) minAnAccu = detnum;
    if(detnum > maxAnAccu) maxAnAccu = detnum;
  }
}

void AgataAnalysis::AddTime( G4double time, G4double ener, G4int detnum )
{
  if( detnum < 1000 )
    AddGermaniumTime ( time, ener, detnum );  
  else 
    return;    // no treatment for ancillary "time" provided!!!
}

void AgataAnalysis::AddGermaniumTime( G4double time, G4double ener, G4int detnum )
{
  if( ener == 0. ) return;
  
  if(detnum >= 0 && detnum < nDetectors) {
    if( !gtAccu[detnum] )
      gtAccu[detnum] = time;
    else
      gtAccu[detnum] = ( ener * time + ( geAccu[detnum] - ener ) * gtAccu[detnum] ) /  geAccu[detnum];

    if(detnum < minGeAccu) minGeAccu = detnum;
    if(detnum > maxGeAccu) maxGeAccu = detnum;
  }
}


///////////////////////////////////////////////
///// Management of spectra ///////////////////
///////////////////////////////////////////////
void AgataAnalysis::IncrementSpectra()
{
  if( !makeSpectra ) return;

  G4double edepTot      = 0.;
  G4int minClu = nClusters;
  G4int maxClu = 0;
  G4int clunum;
  G4int ii;
  G4int nd = 0;
  G4double edummy;
  for(ii=minGeAccu; ii<=maxGeAccu; ii++) {
    edummy = geAccu[ii]/keV;
    if( edummy > 0. ) {
      clunum = det2clust[ii];
      edummy = offset + gain * edummy;
      if(edummy) {
        spec1[1]->incr((G4int)edummy, ii);          // the energy of the detectors
        spec1[3]->incr((G4int)edummy, 0);           // the energy of the detectors
        edepTot += edummy;
        nd++;
      }
      if(clunum >= 0 && clunum < nClusters) {
        cluAccu[clunum] += edummy;
        if(clunum < minClu) minClu = clunum;
        if(clunum > maxClu) maxClu = clunum;
      }
    }
  }

  if (edepTot) {
    spec1[0]->incr((G4int)(edepTot*scaleEtot), 0);   //> the total energy of the array
  }

  if(minClu > maxClu)
    return;

  G4int nc = 0;
  for(ii = minClu; ii <= maxClu; ii++) {
    edummy   = cluAccu[ii];
    if(edummy) {
      spec1[2]->incr((G4int)edummy, ii);             //> the energy of the clusters
      nc++;
    }  
  }

//spec2[0]->incr((int)(edepTot*0.1), nd);
}

void AgataAnalysis::FlushSpectra( G4String workPath )
{
  if( !makeSpectra ) return;
  ////////////////////////////////////////////
  /// Write out spectra (packed GASP format)
  ////////////////////////////////////////////
  G4int size = spec1.size();
  G4int nn;
  for(nn = 0; nn <size; nn++) {
    if(spec1[nn]) {
      sprintf(aLine, "%sG%2.2d.%4.4d", workPath.c_str(), nn, runNumber);
      if( asciiSpectra )
        spec1[nn]->writeA(aLine);
      else  
        spec1[nn]->write(aLine);
    }
  }

  // uncomment if matrices are needed!
  /*
  size = spec2.size();
  for(nn = 0; nn < size; nn++) {
    if(spec2[nn]) {
      sprintf(aLine, "M%2.2d.%4.4d", nn, runNumber);
      spec2[nn]->write(aLine);
    }
  }
  */
}


/////////////////////////////////////////////////////////////
//// Management of the list-mode event (GASP format) ////////
/////////////////////////////////////////////////////////////
void AgataAnalysis::WriteGaspEvent()
{
  if( !makeBuffer ) return;

  memset( gaspEvent, 0, 2048*sizeof(short int) );
  
  unsigned short int nGd=0, nAncTot=0;
  
  unsigned short int* nAnc = new unsigned short int[nInstances];
  memset( nAnc, 0, nInstances*sizeof(unsigned short int));
  
  // Calculates event length
  G4int ii, jj, kk;
  G4int evLength = 1;       //   Ge  (2 par: energy, time) 
  if( nInstances ) 
    evLength += nInstances; // + ancillary detectors
      //G4cout << " WGE " << minAnAccu << " " << maxAnAccu << G4endl;

  if( nDetectors ) {
    for( ii=minGeAccu; ii<=maxGeAccu; ii++ ) {
      if( geAccu[ii] > fThreshold ) {
        evLength += 3;            // number, ge energy, ge time
        nGd++;
      }  
    }
  }
  if( nInstances ) {
    for( jj=0; jj<nInstances; jj++ ) {          // cycle on ancillary detectors
    
      
      //G4cout << " WGE " << minAnAccu << " " << maxAnAccu << G4endl;
      for( ii=minAnAccu; ii<=maxAnAccu; ii++ ) {  // cycle on detector number
        
	if( agAccu[ii+nDetectors*jj] > 0 ) {
            evLength += (parLut[jj]+1);     // ndet + x parameters
            nAncTot++;
	    nAnc[jj]++;
	}  
      }
    }
  }

  //G4cout << " EoE " << nInstances << " " << nGd << " " << nAncTot << G4endl;
  if( !(nGd+ancillaryOnly*nAncTot) )
    return;
  
  unsigned short int length = evLength;
  position = 0;
  
  if( evLength ) {  // begin of event tag
    gaspEvent[position++] = (unsigned short int)0xf000+length;
    gaspEvent[position++] = nGd;
    
    if( nAncTot ) {
      for( jj=0; jj<nInstances; jj++ )
        gaspEvent[position++] = nAnc[jj];
    }
    //if( nAncTot ) {
   //   for( jj=0; jj<nInstances; jj++ )
    //    G4cout << nAnc[jj] << G4endl;
    //}
  }
  
  
  if( nGd ) {
    for( G4int ng=minGeAccu; ng<=maxGeAccu; ng++ ) {
      if( geAccu[ng] > fThreshold  ) {
        gaspEvent[position++] = (unsigned short int)ng;
        gaspEvent[position++] = (unsigned short int)floor(geAccu[ng]/gf1);
        gaspEvent[position++] = (unsigned short int)floor(gtAccu[ng]/gf2);
      }  
    }
  }
  
  if( nAncTot ) {
    for( jj=0; jj<nInstances; jj++ ) {
      //G4cout << "   " << nAnc[jj] << " " << minAnAccu << " " << maxAnAccu << G4endl;
      if( nAnc[jj] > 0 ) {
        for( G4int na=minAnAccu; na<=maxAnAccu; na++ ) {
	  if( agAccu[na+nDetectors*jj] > 0 ) {
            gaspEvent[position++] = (unsigned short int)na;
	    for( kk=0; kk<parLut[jj]; kk++ ) {
	      gaspEvent[position++] = (unsigned short int)floor(anAccu[na+(kk+minLut[jj])*nDetectors]/gf3);
	    }
	  } 
	}
      }
    }
  }

  gaspBuffer->AddToBuffer( gaspEvent, position );
}



/////////////////////////////////////////////////////////////////////////
// Methods for the Messenger
/////////////////////////////////////////////////////////////////////////

void AgataAnalysis::EnableAnalysis( G4bool enable )
{
  enabledAnalysis = enable;

  if(enabledAnalysis)
    G4cout << " ----> On-line analysis enabled!" << G4endl;
  else
    G4cout << " ----> On-line analysis disabled!" << G4endl;
}

void AgataAnalysis::EnableSpectra( G4bool enable )
{
  makeSpectra = enable;

  if(makeSpectra)
    G4cout << " ----> On-line spectra enabled!" << G4endl;
  else
    G4cout << " ----> On-line spectra disabled!" << G4endl;
}

void AgataAnalysis::EnableBuffer( G4bool enable )
{
  makeBuffer = enable;

  if(makeBuffer)
    G4cout << " ----> Events will be written out in REDUCED GASP format!" << G4endl;
  else
    G4cout << " ----> Events will not be written out in REDUCED GASP format!" << G4endl;
}

void AgataAnalysis::EnableResolution(G4bool enable )
{
  enabledResolution = enable;

  if(enabledResolution)
    G4cout << " ----> Energy resolution will be considered!" << G4endl;
  else
    G4cout << " ----> Energy resolution will not be considered!" << G4endl;
}

void AgataAnalysis::CalculateResolution( G4double e1, G4double f1, G4double e2, G4double f2, G4bool print)
{
  if( e1 != e2 )
  {
    FWHMb = ( pow(f1, 2.)-pow(f2, 2.) )/( e1-e2 );
    FWHMa = pow(f1, 2.) - FWHMb*e1;
    if(!print) return;
    sprintf(aLine, " FWHM = %8.3f keV   at  %8.3f keV \n FWHM = %8.3f keV   at  %8.3f keV", f1, e1, f2, e2);
    G4cout << aLine << G4endl;
    sprintf(aLine, " a = %f\n b = %f", FWHMa, FWHMb);
    G4cout << aLine << G4endl;
  }
}

void AgataAnalysis::SetPackEvent( G4bool value )
{
  packEvent = value;
  if(packEvent)
    G4cout << " ----> Packing gammas of cascade is enabled" << G4endl;
  else
    G4cout << " ----> Packing gammas of cascade is disabled" << G4endl;
}

void AgataAnalysis::SetAncillaryOnly( G4bool value )
{
  if( value )
    ancillaryOnly = 1;
  else
    ancillaryOnly = 0;  
  
  if( value )
    G4cout << " ----> Writing out ancillary data also when no germanium is firing" << G4endl;
  else  
    G4cout << " ----> Writing out ancillary data only when at least a germanium is firing" << G4endl;
}

void AgataAnalysis::ShowStatus()
{
  if( !enabledAnalysis )
    return;
  
  G4cout << G4endl;
  if(makeSpectra) {
    G4cout << " On-line spectra enabled" << G4endl;
    if(asciiSpectra)
      G4cout << " Spectra written in ASCII format." << G4endl;
    else   
      G4cout << " Spectra written in BINARY format." << G4endl;
    
    G4cout << " Spectra with " << specLength << " channels"  << G4endl;    

    sprintf(aLine, " Offset = %f\n Gain   = %f",offset, gain);
    G4cout << aLine << G4endl;
  }
  else
    G4cout << " On-line spectra disabled" << G4endl; 

  if(makeBuffer) {
    G4cout << " Writing out events in reduced GASP format at " << gf1/keV << " keV/ch." << G4endl;
  }
  else {
    G4cout << " File in reduced GASP format will NOT be produced." << G4endl;
  }

  if(enabledResolution) { 
    G4cout << " Energy resolution will be applied" << G4endl;
    sprintf(aLine, " Energy resolution coefficients  a = %f   b = %f", FWHMa, FWHMb);
    G4cout << aLine << G4endl;
  }  
  else
    G4cout << " Energy resolution will not be considered" << G4endl; 

  if(packEvent)
    G4cout << " Packing gammas of cascade is enabled" << G4endl;
  else
    G4cout << " Packing gammas of cascade is disabled" << G4endl;

  if(enabledAnalysis)
    G4cout << " On-line analysis enabled" << G4endl;
  else
    G4cout << " On-line analysis disabled" << G4endl;

}

void AgataAnalysis::SetMaxFileSize( G4int size )
{
  if( size < 0 ) {
    G4cout << " ----> Invalid value, keeping previous limit ("
           << maxFileSize/1048576 << " MB)" << G4endl;
  }
  else if( size > 2048 ) {  // 2GB
    G4cout << " ----> Value out of range, resetting to maximum value." << G4endl;
    maxFileSize = 2040109465; // 95% of 2GB
  }
  else {
    maxFileSize = (G4int)( 0.95 * size * 1048576 );
    G4cout << " ----> Maximum file size has been set to " << size << " MB." << G4endl;
  }
}

void AgataAnalysis::SetEnerGain( G4double value )
{
  if( value > 0. )
    gf1 = value * keV;
  else
    gf1 = 1.0 * keV;
  G4cout << " ----> Energy Gain = " << gf1/keV << " keV/ch" << G4endl;    
}

void AgataAnalysis::SetTimeGain( G4double value )
{
  if( value > 0. )
    gf2 = value * ns;
  else
    gf2 = 1.0 * ns;
  G4cout << " ----> Time Gain = " << gf2/ns << " ns/ch" << G4endl;    
}

void AgataAnalysis::SetFThreshold( G4double value )
{
  if( value > 1. )
    fThreshold = value * keV;
  else
    fThreshold = 1.0 * keV;
  G4cout << " ----> Threshold for output to file = " << fThreshold/keV << " keV" << G4endl;    
}

void AgataAnalysis::SetOffset( G4double value )
{
  offset = value;
  G4cout << " ----> Offset = " << offset << G4endl;
}

void AgataAnalysis::SetGain( G4double value )
{
  if( value > 0. )
    gain = value;
  else
    gain = 1.0;
  G4cout << " ----> Gain = " << gain << G4endl;    
}

void AgataAnalysis::SetAsciiSpectra( G4bool value )
{
  asciiSpectra = value;
  if(asciiSpectra)
    G4cout << " ----> Spectra will be written in ASCII format." << G4endl;
  else
    G4cout << " ----> Spectra will be written in BINARY format." << G4endl;
}

void AgataAnalysis::SetSpecLength( G4int value )
{
  if( value > 0 )
    specLength = value * 1024;
  else
    specLength = 1024;
  G4cout << " ----> Spectra with " << specLength << " channels"  << G4endl;    
}

/////////////////////////////////////////////////////////////////////////
// The Messenger

#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAnInteger.hh"

#include "G4UIcmdWithoutParameter.hh"

AgataAnalysisMessenger::AgataAnalysisMessenger(AgataAnalysis* pTarget, G4String name)
:myTarget(pTarget)
{
  const char *aLine;
  G4String commandName;
  G4String directoryName = name + "/analysis/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of on-line analysis.");
///////////////////////////////////////////////////////////////////////
//// General commands /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enable";
  aLine = commandName.c_str();
  EnableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  EnableAnalysisCmd->SetGuidance("Activate the on-line analysis");
  EnableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  EnableAnalysisCmd->SetDefaultValue(true);
  EnableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disable";
  aLine = commandName.c_str();
  DisableAnalysisCmd = new G4UIcmdWithABool(aLine, this);
  DisableAnalysisCmd->SetGuidance("Deactivate the on-line analysis");
  DisableAnalysisCmd->SetParameterName("enabledAnalysis",true);
  DisableAnalysisCmd->SetDefaultValue(false);
  DisableAnalysisCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionDisable";
  aLine = commandName.c_str();
  DisableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  DisableResolutionCmd->SetGuidance("Disable application of energy resolution");
  DisableResolutionCmd->SetParameterName("enabledResolution",true);
  DisableResolutionCmd->SetDefaultValue(false);
  DisableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "resolutionEnable";
  aLine = commandName.c_str();
  EnableResolutionCmd = new G4UIcmdWithABool(aLine, this);
  EnableResolutionCmd->SetGuidance("Enable application of energy resolution");
  EnableResolutionCmd->SetParameterName("enabledResolution",true);
  EnableResolutionCmd->SetDefaultValue(true);
  EnableResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "calculateResolution";
  aLine = commandName.c_str();
  CalculateResolutionCmd = new G4UIcmdWithAString(aLine, this);
  CalculateResolutionCmd->SetGuidance("Calculate parameters of the energy resolution");
  CalculateResolutionCmd->SetGuidance("starting from FWHM values at two energies");
  CalculateResolutionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "packGammas";
  aLine = commandName.c_str();
  PackEventCmd = new G4UIcmdWithABool(aLine, this);
  PackEventCmd->SetGuidance("Activate packing the gammas of a cascade");
  PackEventCmd->SetParameterName("packEvent",true);
  PackEventCmd->SetDefaultValue(true);
  PackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "unpackGammas";
  aLine = commandName.c_str();
  UnpackEventCmd = new G4UIcmdWithABool(aLine, this);
  UnpackEventCmd->SetGuidance("Deactivate packing the gammas of a cascade");
  UnpackEventCmd->SetParameterName("packEvent",true);
  UnpackEventCmd->SetDefaultValue(false);
  UnpackEventCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fileSize";
  aLine = commandName.c_str();
  SetFileSizeCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetFileSizeCmd->SetGuidance("Set maximum size for the list-mode file.");
  SetFileSizeCmd->SetGuidance("Required parameters: 1 integer (file size in MB).");
  SetFileSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

///////////////////////////////////////////////////////////////////////
//// Spectra          /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enableSpectra";
  aLine = commandName.c_str();
  EnableSpectraCmd = new G4UIcmdWithABool(aLine, this);
  EnableSpectraCmd->SetGuidance("Activate the on-line spectra");
  EnableSpectraCmd->SetParameterName("makeSpectra",true);
  EnableSpectraCmd->SetDefaultValue(true);
  EnableSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableSpectra";
  aLine = commandName.c_str();
  DisableSpectraCmd = new G4UIcmdWithABool(aLine, this);
  DisableSpectraCmd->SetGuidance("Deactivate the on-line spectra");
  DisableSpectraCmd->SetParameterName("makeSpectra",true);
  DisableSpectraCmd->SetDefaultValue(false);
  DisableSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "asciiSpectra";
  aLine = commandName.c_str();
  SetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  SetAsciiSpectraCmd->SetGuidance("Enables ASCII writing in the on-line analysis.");
  SetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  SetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  SetAsciiSpectraCmd->SetDefaultValue(true);
  SetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "binarySpectra";
  aLine = commandName.c_str();
  UnsetAsciiSpectraCmd = new G4UIcmdWithABool(aLine, this);
  UnsetAsciiSpectraCmd->SetGuidance("Enables BINARY writing in the on-line analysis.");
  UnsetAsciiSpectraCmd->SetGuidance("Required parameters: none.");
  UnsetAsciiSpectraCmd->SetParameterName("asciiSpectra",true);
  UnsetAsciiSpectraCmd->SetDefaultValue(false);
  UnsetAsciiSpectraCmd->AvailableForStates(G4State_PreInit,G4State_Idle);  

  commandName = directoryName + "specLength";
  aLine = commandName.c_str();
  SetSpecLengthCmd = new G4UIcmdWithAnInteger(aLine, this);  
  SetSpecLengthCmd->SetGuidance("Define number of channels of spectra");
  SetSpecLengthCmd->SetGuidance("Required parameters: 1 integer (spectra length as a multiple of 1024 channels)");
  SetSpecLengthCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "specOffset";
  aLine = commandName.c_str();
  SetOffsetCmd = new G4UIcmdWithADouble(aLine, this);  
  SetOffsetCmd->SetGuidance("Define offset of the energy calibration for the on-line spectra.");
  SetOffsetCmd->SetGuidance("Required parameters: 1 double.");
  SetOffsetCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "specGain";
  aLine = commandName.c_str();
  SetGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetGainCmd->SetGuidance("Define gain of the energy calibration for the on-line spectra.");
  SetGainCmd->SetGuidance("Required parameters: 1 double.");
  SetGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

///////////////////////////////////////////////////////////////////////
//// Buffer           /////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
  commandName = directoryName + "enableBuffer";
  aLine = commandName.c_str();
  EnableBufferCmd = new G4UIcmdWithABool(aLine, this);
  EnableBufferCmd->SetGuidance("Activate the on-line analysis");
  EnableBufferCmd->SetParameterName("makeBuffer",true);
  EnableBufferCmd->SetDefaultValue(true);
  EnableBufferCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableBuffer";
  aLine = commandName.c_str();
  DisableBufferCmd = new G4UIcmdWithABool(aLine, this);
  DisableBufferCmd->SetGuidance("Deactivate the on-line analysis");
  DisableBufferCmd->SetParameterName("makeBuffer",true);
  DisableBufferCmd->SetDefaultValue(false);
  DisableBufferCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ancillaryOnly";
  aLine = commandName.c_str();
  AncillaryOnlyCmd = new G4UIcmdWithABool(aLine, this);
  AncillaryOnlyCmd->SetGuidance("Activate writing out of ancillary data for events without germanium firing");
  AncillaryOnlyCmd->SetParameterName("ancillaryOnly",true);
  AncillaryOnlyCmd->SetDefaultValue(true);
  AncillaryOnlyCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "ancillaryCoinc";
  aLine = commandName.c_str();
  AncillaryCoincCmd = new G4UIcmdWithABool(aLine, this);
  AncillaryCoincCmd->SetGuidance("Deactivate writing out of ancillary data for events without germanium firing");
  AncillaryCoincCmd->SetParameterName("ancillaryOnly",true);
  AncillaryCoincCmd->SetDefaultValue(false);
  AncillaryCoincCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "egain";
  aLine = commandName.c_str();
  SetEnerGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetEnerGainCmd->SetGuidance("Define gain of the energy calibration (keV/ch).");
  SetEnerGainCmd->SetGuidance("Required parameters: 1 double.");
  SetEnerGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "tgain";
  aLine = commandName.c_str();
  SetTimeGainCmd = new G4UIcmdWithADouble(aLine, this);  
  SetTimeGainCmd->SetGuidance("Define gain of the time calibration (ns/ch).");
  SetTimeGainCmd->SetGuidance("Required parameters: 1 double.");
  SetTimeGainCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fThreshold";
  aLine = commandName.c_str();
  SetFThresholdCmd = new G4UIcmdWithADouble(aLine, this);  
  SetFThresholdCmd->SetGuidance("Define threshold for output to file (keV).");
  SetFThresholdCmd->SetGuidance("Required parameters: 1 double.");
  SetFThresholdCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

}

AgataAnalysisMessenger::~AgataAnalysisMessenger()
{
  delete myDirectory;
  delete EnableAnalysisCmd;
  delete DisableAnalysisCmd;
  delete EnableResolutionCmd;
  delete DisableResolutionCmd;
  delete CalculateResolutionCmd;
  delete PackEventCmd;
  delete UnpackEventCmd;
  delete SetFileSizeCmd;
  delete StatusCmd;

  delete EnableSpectraCmd;
  delete DisableSpectraCmd;
  delete SetAsciiSpectraCmd;
  delete UnsetAsciiSpectraCmd;
  delete SetSpecLengthCmd;
  delete SetOffsetCmd;
  delete SetGainCmd;
  
  delete EnableBufferCmd;
  delete DisableBufferCmd;
  delete AncillaryOnlyCmd;
  delete AncillaryCoincCmd;
  delete SetEnerGainCmd;
  delete SetTimeGainCmd;
  delete SetFThresholdCmd;
}

void AgataAnalysisMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{

  if( command == EnableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableAnalysisCmd ) {
    myTarget->EnableAnalysis( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableResolutionCmd ) {
    myTarget->EnableResolution( EnableResolutionCmd->GetNewBoolValue(newValue)  );
  }
  if( command == DisableResolutionCmd ) {
    myTarget->EnableResolution( DisableResolutionCmd->GetNewBoolValue(newValue)  );
  }
  if( command == CalculateResolutionCmd ) { 
    G4double e1, e2, f1, f2;
    sscanf( newValue, " %lf %lf %lf %lf ", &e1, &f1, &e2, &f2 );
    myTarget->CalculateResolution(e1*keV, f1*keV, e2*keV, f2*keV );
  }
  if( command == PackEventCmd ) {
    myTarget->SetPackEvent( PackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnpackEventCmd ) {
    myTarget->SetPackEvent( UnpackEventCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetFileSizeCmd ) {
    myTarget->SetMaxFileSize( SetFileSizeCmd->GetNewIntValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus( );
  }
  
  if( command == EnableSpectraCmd ) {
    myTarget->EnableSpectra( EnableSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableSpectraCmd ) {
    myTarget->EnableSpectra( EnableAnalysisCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( SetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == UnsetAsciiSpectraCmd ) {
    myTarget->SetAsciiSpectra( UnsetAsciiSpectraCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetSpecLengthCmd ) {
    myTarget->SetSpecLength(SetSpecLengthCmd->GetNewIntValue(newValue));
  }
  if( command == SetOffsetCmd ) {
    myTarget->SetOffset(SetOffsetCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetGainCmd ) {
    myTarget->SetGain(SetGainCmd->GetNewDoubleValue(newValue));
  }

  if( command == EnableBufferCmd ) {
    myTarget->EnableBuffer( EnableBufferCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableBufferCmd ) {
    myTarget->EnableBuffer( EnableBufferCmd->GetNewBoolValue(newValue) );
  }
  if( command == AncillaryOnlyCmd ) {
    myTarget->SetAncillaryOnly( AncillaryOnlyCmd->GetNewBoolValue(newValue) );
  }
  if( command == AncillaryCoincCmd ) {
    myTarget->SetAncillaryOnly( AncillaryCoincCmd->GetNewBoolValue(newValue) );
  }
  if( command == SetEnerGainCmd ) {
    myTarget->SetEnerGain(SetEnerGainCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetTimeGainCmd ) {
    myTarget->SetTimeGain(SetTimeGainCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetFThresholdCmd ) {
    myTarget->SetFThreshold(SetFThresholdCmd->GetNewDoubleValue(newValue));
  }

}
