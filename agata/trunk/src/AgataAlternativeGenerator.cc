#include "AgataAlternativeGenerator.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"


AgataAlternativeGenerator::AgataAlternativeGenerator( )
{

  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName = "gamma";
  particleGun->SetParticleDefinition(particleTable->FindParticle(particleName));
  particleGun->SetParticleEnergy(1.0*MeV);
  particleGun->SetParticlePosition(G4ThreeVector());
  
}

AgataAlternativeGenerator::~AgataAlternativeGenerator()
{
  delete particleGun;
}

void AgataAlternativeGenerator::GeneratePrimaries(G4Event* anEvent)
{ 
  G4double costheta = 2.0 * G4UniformRand() - 1.0;
  G4double sintheta = sqrt( 1 - costheta * costheta );
  G4double phi      = 360. * G4UniformRand() * deg;
  
  G4ThreeVector direction;
  direction.set( sintheta*cos(phi), sintheta*sin(phi), costheta );

  ///////////////////////////////////////////////
  // finally, call particle gun 
  ///////////////////////////////////////////////////
  particleGun->SetParticleMomentumDirection( direction );
  particleGun->GeneratePrimaryVertex(anEvent);

}

G4String AgataAlternativeGenerator::GetParticleHeader( const G4Event* evt, G4double /* unitLength */, G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", -partType, epart/unitEnergy, direction.x(), direction.y(), direction.z(), evt->GetEventID());
  return G4String(aLine);

}
