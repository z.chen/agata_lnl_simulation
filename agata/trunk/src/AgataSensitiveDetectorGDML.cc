#include "AgataSensitiveDetectorGDML.hh"
#include "AgataDetectorConstructionGDML.hh"

#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4EventManager.hh"
#include "G4ios.hh"
#include <string>
#include <cmath>

#ifdef G4V10
using namespace CLHEP;
#endif


#define TOLERANCE (0.0001*mm)
#define TOLERANEN (0.001*keV)

AgataSensitiveDetectorGDML::AgataSensitiveDetectorGDML(G4String directory, G4String name, G4String HCname, 
         G4int offs, G4int dep, G4bool createMenu )
:G4VSensitiveDetector(name)
{
  InitData( HCname, offs, dep, directory );
  if( createMenu )
    myMessenger = new AgataSensitiveDetectorMessengerGDML(this,directory);
}

AgataSensitiveDetectorGDML::AgataSensitiveDetectorGDML(G4String directory, G4String name, G4String HCname, G4bool createMenu)
:G4VSensitiveDetector(name)
{
  G4int offs = 0;
  G4int dep  = 0;
  InitData( HCname, offs, dep, directory );
  if( createMenu )
    myMessenger = new AgataSensitiveDetectorMessengerGDML(this,directory);
}

AgataSensitiveDetectorGDML::~AgataSensitiveDetectorGDML()
{
  if( myMessenger )
    delete myMessenger;
}

void AgataSensitiveDetectorGDML::InitData( G4String HCname, G4int offs, G4int dep, G4String /*directory*/ )
{
  HCID            = -1;
  collectionName.insert(HCname);
  
  offset          = offs;
  depth           = dep;
  
  trackMethod     = 0;    
  
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstructionGDML*) runManager->GetUserDetectorConstruction();
  
  myMessenger     = NULL;
}

void AgataSensitiveDetectorGDML::Initialize(G4HCofThisEvent* HCE)
{
  if( !HCE ) return;
  theHits     = new AgataHitDetectorCollection(SensitiveDetectorName,collectionName[0]);
  //EventIsGood = true;
  annihilationHappened = false; 
  residualEnergy = 0.;
  
  	cout << "Initialisation of SensitiveDetector class  done" << endl;
  
}

G4bool AgataSensitiveDetectorGDML::ProcessHits(G4Step* aStep,G4TouchableHistory* /*ROhist*/)
{
  G4double edep = 0.;
  G4double additionalEnergy = 0.;
  G4String ChiEStato;
  G4String ilProcesso;
  G4String ilGeneratore = " ";
  G4int padre;
  
  switch (trackMethod)
  {
    ///////////////////////////////////////////////////////////////////
    //// Idea: primary gamma is followed
    //// store the position where an e+ annihilated
    //// accept only secondary gammas coming from that position!!!
    ///////////////////////////////////////////////////////////////////
    case 1:
      /// Consider only gammas and positrons
      ChiEStato = aStep->GetTrack()->GetDefinition()->GetParticleName();
      ilProcesso = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
      //G4cout << ChiEStato << " interacted through a " << ilProcesso << G4endl;
      /// positrons: just store the position!!!
      if( ChiEStato == "e+" && ilProcesso == "annihil" ) {
	positionOfAnnihilation = aStep->GetPostStepPoint()->GetPosition();
	/// some positron decays in flight, should correct for that ...
	///////////////residualEnergy = -1.*(aStep->GetDeltaEnergy());
	residualEnergy = aStep->GetTrack()->GetKineticEnergy();
	annihilationHappened = true;
	//G4cout << " e+ annihilated at " << positionOfAnnihilation/mm << G4endl; 
	//G4cout << " e+ residual energy is " << residualEnergy/keV << G4endl;
	if( residualEnergy ) {
          //G4cout << ChiEStato << " interacted through a " << ilProcesso << G4endl;
	  //G4cout << " e+ residual energy is " << residualEnergy/keV << G4endl;
	  G4int ii = theHits->entries();
	  //G4cout << " number of entries is " << ii << G4endl;
	  if( ii ) {
	    //G4cout << " Deposited energy was " << (*theHits)[ii-1]->GetEdep()/keV << G4endl;
	    edep = (*theHits)[ii-1]->GetEdep() - residualEnergy;
	    if( edep <= 0. ) edep = 0.;
	      (*theHits)[ii-1]->SetEdep(edep); 
	      //G4cout << " Deposited energy now is " << (*theHits)[ii-1]->GetEdep()/keV << G4endl;
	      residualEnergy = 0.;
	  }
	}
      }
      if( ChiEStato != "gamma" ) return false;
   

      ///////////////////////////////////////////////
      ///////// Treatment of secondary gammas
      ///////////////////////////////////////////////
      padre = aStep->GetTrack()->GetParentID();
      if( padre ) {
	////////////////////////////////////////////////////////////////////////
	//// Should not count very-low-energy gammas from photoelectric
	///  or bremsstrahlung
	////////////////////////////////////////////////////////////////////////
	if( aStep->GetTrack()->GetCreatorProcess() )
          ilGeneratore = aStep->GetTrack()->GetCreatorProcess()->GetProcessName();
	if( ilGeneratore != "annihil" )
	  return false;
	////////////////////////////////////////////////////////////////////////
	//// Only secondary gammas from annihilation vertexes!!!
	////////////////////////////////////////////////////////////////////////
	if( annihilationHappened ) {
          G4ThreeVector originOfTrack = aStep->GetTrack()->GetVertexPosition();
	  //G4cout << " originOfTrack is " << originOfTrack/mm << G4endl;
	  if( (positionOfAnnihilation-originOfTrack).mag() > TOLERANCE ) return false;
	}
      }
      ///////////////////////////////////////////////////////////////////
      /////// Should take care of in-flight annihilations!
      //////////////////////////////////////////////////////////////////
      if( ilProcesso == "conv" || ilProcesso == "LowEnConversion" ) {
        additionalEnergy = residualEnergy;
	//if( additionalEnergy )
	  //G4cout << " additionalEnergy is " << additionalEnergy << G4endl;
      }  
      
      if( ilProcesso != "conv" && ilProcesso != "LowEnConversion" )
        edep  = -1.*(aStep->GetDeltaEnergy());
      else {      // pair production
        edep  =  -1.*(aStep->GetDeltaEnergy()) - 2. * electron_mass_c2 - additionalEnergy;  // E_gamma - 2 m_e
	if( edep < 0. ) edep = 0.;
      }	
      if(edep==0.)
        return false;
      break;

    /////////////////////////////////////////////////////////////
    /// default trackMethod: the electron tracks are followed
    /////////////////////////////////////////////////////////////
    default:
      edep = aStep->GetTotalEnergyDeposit();
      if(edep==0.)
        return false;
      // emulate pulse height defect in case of energy deposition by heavy ions
      // as suggested by Joa Ljungvall
#ifdef PULSE_DEFECT      	
      // heavy ion ==> particle mass larger than alpha particle
      //G4cout << " Tracked " << aStep->GetTrack()->GetDefinition()->GetParticleName() << G4endl;
      //G4cout << " Mass is " << aStep->GetTrack()->GetDefinition()->GetPDGMass()/MeV << G4endl;
      if( aStep->GetTrack()->GetDefinition()->GetPDGMass() > 4.* proton_mass_c2 )
        edep = 0.210 * pow( edep, 1.099 );
#ifdef FLAGGED_NEUTRON
      G4RunManager* runManager = G4RunManager::GetRunManager();
      const G4Event* evt = runManager->GetCurrentEvent();
//      G4cout << " Primary is " << evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() << G4endl;
      if( evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetParticleName() == "neutron" )
        edep = -edep;
#endif	
#endif	
  }
  
  ///////////////////////////////////////////////////////////////////////////////////
  /// Process name (and number via a LUT) which generated the track is retrieved.
  /// In the case of secondary particles, the process which generated the secondary
  /// particle is actually retrieved.
  ///////////////////////////////////////////////////////////////////////////////////
  padre = aStep->GetTrack()->GetParentID();
  if( padre > 0 ) {
    const G4VProcess* ChiHaGenerato = aStep->GetTrack()->GetCreatorProcess();
    if( ChiHaGenerato )
      ilProcesso = ChiHaGenerato->GetProcessName();
    else
      ilProcesso = "NotAProcess";
  }
  else {
    ilProcesso = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
  }
  G4int interNum = GetProcessNum( ilProcesso );
  
  ////////////////////////////////////////////////////////////
  /// Direction (in the Lab system) of the primary particle
  ////////////////////////////////////////////////////////////
  G4ThreeVector direction = aStep->GetPreStepPoint()->GetMomentumDirection();

  ////////////////////////////////////////////////////////////
  /// Position (in the Lab system) of the interaction point
  ////////////////////////////////////////////////////////////
  G4ThreeVector position = aStep->GetPostStepPoint()->GetPosition();
  
  ////////////////////////////////////////////////////////////
  /// Time at which the interaction point was generated,
  /// counted from the moment the primary particle was
  /// shoot.
  ////////////////////////////////////////////////////////////
  G4double time  = aStep->GetPostStepPoint()->GetGlobalTime();
//  G4double time  = aStep->GetPostStepPoint()->GetGlobalTime() - aStep->GetPostStepPoint()->GetLoperTime();
  
  ////////////////////////////////////////////////////////////
  /// Name of the volume in which the interaction took place
  ////////////////////////////////////////////////////////////
  G4String nomevolume = aStep->GetPreStepPoint()->GetPhysicalVolume()->GetName();
//  G4cout << nomevolume << G4endl;
  
  //////////////////////
  /// Detector number
  //////////////////////
  G4int detCode = aStep->GetPreStepPoint()->GetTouchable()->GetReplicaNumber(depth);

  //if( offset ) G4cout << " offset " << offset << " " << detCode << G4endl;
  
  //if( (detCode%1000) == 1 )
  //  G4cout << nomevolume << G4endl;

  ////////////////////////////////////////////////////////////////////
  /// Position of the interaction point in the solid reference frame
  ///////////////////////////////////////////////////////////////////
  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
  G4VPhysicalVolume* topVolume = theTouchable->GetVolume(depth);
  
  G4ThreeVector frameTrans = topVolume->GetFrameTranslation();
                                  
  const G4RotationMatrix* rot = topVolume->GetFrameRotation();
                                  
  G4RotationMatrix frameRot;
  if( rot )
    frameRot = *rot;
  
  G4ThreeVector posSol = frameRot( position );
  posSol += frameRot( frameTrans );
    
  //////////////////////
  /// Segment number
  //////////////////////
  G4int segCode = 0;
  //segCode = theDetector->GetSegmentNumber( offset, detCode, posSol );  // ToDo: 
  
  //G4cout << " Ho trovato qualcosa? " << offset+ (detCode%1000) << " " << segCode << " " << edep/keV << G4endl;
  /////////////////////////////////////////////////////
  /// Stores the information in the "hit" structure
  ////////////////////////////////////////////////////
  AgataHitDetector* newHit = new AgataHitDetector();
  
  newHit->SetTrackID( aStep->GetTrack()->GetTrackID() );  //> trackID
  newHit->SetDetNb( offset+ (detCode%1000) );             //> the detector number here includes the offset
                                                          //>  so that ancillaries can be distinguished from
							  //>  germanium detectors
  newHit->SetCluNb( (detCode/1000) );                     //> cluster number 
  newHit->SetSegNb( segCode );                            //> segment number 
  newHit->SetEdep( edep );                                //> energy release
  newHit->SetTime( time );                                //> time of the interaction (starting from the t0=0 reference)
  newHit->SetPos( position );                             //> position of the interaction
  newHit->SetRelPos( posSol );                            //> position of the interaction (relative to the solid)
  newHit->SetDir( direction );                            //> position of the interaction
  newHit->SetName( nomevolume );                          //> volume name
  newHit->SetInterNb( interNum );                         //> interaction

  theHits->insert( newHit );

  return true;
}

#include "AgataRunAction.hh"
void AgataSensitiveDetectorGDML::EndOfEvent(G4HCofThisEvent* HCE)
{
  G4int i;
  
  if(HCID<0) { 
    HCID = G4SDManager::GetSDMpointer()->GetCollectionID(collectionName[0]); 
  }
  HCE->AddHitsCollection( HCID, theHits );
  
  G4RunManager* runManager = G4RunManager::GetRunManager();
  const G4Event* evt = runManager->GetCurrentEvent();
  G4int evNum = evt->GetEventID();

  //////////////////////////////////////////////////////////////////////////////////////
  /// The characteristics of the primary particle are retrieved for printout on screen
  ///  in case of high verbosity levels
  //////////////////////////////////////////////////////////////////////////////////////
  G4double enpartlab = evt->GetPrimaryVertex()->GetPrimary()->GetMomentum().mag();
  G4double masspart  = evt->GetPrimaryVertex()->GetPrimary()->GetG4code()->GetPDGMass();
  if( masspart > 0. )    // massive particles --> kinetic energy (relativistic)
    enpartlab  = sqrt( masspart * masspart + enpartlab  * enpartlab ) - masspart;

  G4int nHits = theHits->entries();

  G4cout.precision(3);
  G4cout.setf(ios::fixed);
  if(verboseLevel>0) {
    G4cout << " Ev# "   << std::setw(4) << evNum << "  "
           << "  Ein  " << std::setw(8) << enpartlab/keV  << " keV" << G4endl;
  }

  if(nHits) {
    G4double etot = 0.;
    G4int    detNumber;
    G4int    cluNumber;
    for(i = 0; i < nHits; i++)  {
      detNumber = (*theHits)[i]->GetDetNb();
      cluNumber = (*theHits)[i]->GetCluNb();
      etot     += (*theHits)[i]->GetEdep();
      if(verboseLevel>0) {
        G4cout << " Ev# "   << std::setw(4) << evNum << "  "
                            << std::setw(2) << i << " " 
                            << std::setw(2) << detNumber << " "
                            << std::setw(2) << cluNumber;

        (*theHits)[i]->Print();
      }
    }
    // << " Ev#" << evNum << " Etot is " << etot/keV << " and epartlab is " << enpartlab/keV << G4endl;
    if( etot > (enpartlab+TOLERANEN) && trackMethod ) {
      AgataRunAction* theRun  = (AgataRunAction*) runManager->GetUserRunAction();
      theRun->IncrementErrors();
      ///////////////////////////////////////////////////////////////
      /// Uncomment next line if you like event-by-event warning ///
      //////////////////////////////////////////////////////////////
      
      //G4cout << " Warning! Ev# " << evNum << " Etot is " << etot/keV << " and enpartlab is " << enpartlab/keV << " keV" << G4endl;
      
      //for(i = 0; i < nHits; i++)  {
      //  G4cout << " Hit " << (*theHits)[i]->GetEdep()/keV << " " << (*theHits)[i]->GetPos()/mm << G4endl;;
      //}
      
      // seems like there is no .clear() available ...
      delete theHits;
      theHits     = new AgataHitDetectorCollection(SensitiveDetectorName,collectionName[0]);
    }
    if(verboseLevel>0) {
      G4cout << std::setw(50) << "TotEdep ";
      G4cout.setf(ios::fixed);
      G4cout << std::setw(7) << etot/keV << " keV" << G4endl;
    }
  }
  annihilationHappened = false; 
  positionOfAnnihilation = G4ThreeVector();
  residualEnergy = 0.; 
}  

void AgataSensitiveDetectorGDML::DeleteHits()
{
  delete theHits;
}

void AgataSensitiveDetectorGDML::SetDepth( G4int dep )
{
  if( dep >= 0 )
    depth = dep;
  else
    depth = 0;
    
  G4cout << " --> Sensitive Detector depth has been set to " << depth << G4endl; 
}

////////////////////////////////
// Methods for the Messenger
////////////////////////////////
void AgataSensitiveDetectorGDML::SetMethod( G4int method )
{
  if( method > 0 )
    trackMethod = 1;
  else
    trackMethod = 0;  
  switch( trackMethod )
  {
    case 0:
      G4cout << " ----> Performing standard tracking" << G4endl;
      break;
    case 1:
      G4cout << " ----> Considering only gammas (including pair production)" << G4endl;
      break;
    default:
      G4cout << " ----> Performing standard tracking" << G4endl;
      break;
  }
}

void AgataSensitiveDetectorGDML::ShowStatus()
{
  switch( trackMethod )
  {
    case 0:
      G4cout << " Performing standard tracking" << G4endl;
      break;
    case 1:
      G4cout << " Considering only gammas (including pair production)" << G4endl;
      break;
    default:
      G4cout << " Performing standard tracking" << G4endl;
      break;
  }
}

G4int AgataSensitiveDetectorGDML::GetProcessNum( G4String nome )
{
  /////////////////////////////////////////////////////////////
  //> LUT for the processes which generated a step
  //> 1 ---> Compton
  //> 2 ---> photoelectric
  //> 3 ---> pair production
  //> 4 ---> Rayleigh
  //> 5 ---> transportation
  //> 99 --> other process (not a gamma interaction directly)
  ////////////////////////////////////////////////////////////
  
  if( nome=="compt" )
    return 1;
  if( nome=="LowEnCompton" )
    return 1;
  if( nome=="G4LECSCompton" )
    return 1;
  if( nome=="polarCompt" )
    return 1;
  if( nome=="polarLowEnCompt" )
    return 1;
  
  if( nome=="phot" )
    return 2;
  if( nome=="LowEnPhotoElec" )
    return 2;

  if( nome=="conv" )
    return 3;
  if( nome=="LowEnConversion" )
    return 3;

  if( nome=="LowEnRayleigh" )
    return 4;
  if( nome=="G4LECSRayleigh" )
    return 4;

  if( nome=="Transportation" )
    return 5;

  return 99;
}


///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithoutParameter.hh"

AgataSensitiveDetectorMessengerGDML::AgataSensitiveDetectorMessengerGDML( AgataSensitiveDetectorGDML* pTarget, G4String name )
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/tracking/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of tracking method.");
    
  commandName = directoryName + "method";
  aLine = commandName.c_str();
  SetMethodCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetMethodCmd->SetGuidance("Define method for tracking.");
  SetMethodCmd->SetGuidance("  0--> Standard tracking (default)");
  SetMethodCmd->SetGuidance("  1--> Standard tracking (events with primary gammas only)");
  SetMethodCmd->SetGuidance("  2--> Gammas only (including pair production)");
  SetMethodCmd->SetGuidance("  3--> Gammas and neutrons (including pair production and inelastic scattering)");
  SetMethodCmd->SetGuidance("Required parameters: 1 integer (0--3).");
  SetMethodCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters");
  StatusCmd->SetGuidance("Required parameters: none.");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataSensitiveDetectorMessengerGDML::~AgataSensitiveDetectorMessengerGDML()
{
  delete myDirectory;
  delete StatusCmd;
  delete SetMethodCmd;
}

void AgataSensitiveDetectorMessengerGDML::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetMethodCmd ) {
    myTarget->SetMethod(SetMethodCmd->GetNewIntValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus();
  }
}

