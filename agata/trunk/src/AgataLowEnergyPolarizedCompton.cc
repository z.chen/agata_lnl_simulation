#include "AgataLowEnergyPolarizedCompton.hh"
#include "Randomize.hh"
#include "G4ParticleDefinition.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4ForceCondition.hh"
#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4DynamicParticle.hh"
#include "G4VParticleChange.hh"
#include "G4ThreeVector.hh"
#include "G4VCrossSectionHandler.hh"
#include "G4CrossSectionHandler.hh"
#include "G4VEMDataSet.hh"
#include "G4CompositeEMDataSet.hh"
#include "G4VDataSetAlgorithm.hh"
#include "G4LogLogInterpolation.hh"

#ifndef G4V10
#include "G4VRangeTest.hh"
#include "G4RangeTest.hh"
#endif

#include "G4MaterialCutsCouple.hh"

// constructor

AgataLowEnergyPolarizedCompton::AgataLowEnergyPolarizedCompton(const G4String& processName)
  : G4VDiscreteProcess(processName),
    lowEnergyLimit (250*eV),              // initialization
    highEnergyLimit(100*GeV),
    intrinsicLowEnergyLimit(10*eV),
    intrinsicHighEnergyLimit(100*GeV)
{
  if (lowEnergyLimit < intrinsicLowEnergyLimit ||
      highEnergyLimit > intrinsicHighEnergyLimit)
    {
#if defined G4V495  || G4V496 || G4V10
      G4Exception("AgataLowEnergyPolarizedCompton::AgataLowEnergyPolarizedCompton", "error1", FatalException , "energy outside intrinsic process validity range" );
#else
      G4Exception("AgataLowEnergyPolarizedCompton::AgataLowEnergyPolarizedCompton - energy outside intrinsic process validity range");
#endif 
   }

  crossSectionHandler = new G4CrossSectionHandler;


  G4VDataSetAlgorithm* scatterInterpolation = new G4LogLogInterpolation;
  G4String scatterFile = "comp/ce-sf-";
#if defined G4V48 || G4V494 || G4V495 || G4V496 || G4V10
  scatterFunctionData = new G4CompositeEMDataSet(scatterInterpolation,1.,1.);
  scatterFunctionData->LoadData(scatterFile);
#else
  scatterFunctionData = new G4CompositeEMDataSet(scatterFile,scatterInterpolation,1.,1.);
#endif
  meanFreePathTable = 0;

#ifndef G4V10
  rangeTest = new G4RangeTest;
#endif

   if (verboseLevel > 0)
     {
       G4cout << GetProcessName() << " is created " << G4endl
	      << "Energy range: "
	      << lowEnergyLimit / keV << " keV - "
	      << highEnergyLimit / GeV << " GeV"
	      << G4endl;
     }
}

// destructor

AgataLowEnergyPolarizedCompton::~AgataLowEnergyPolarizedCompton()
{
  delete meanFreePathTable;
  delete crossSectionHandler;
  delete scatterFunctionData;
#ifndef G4V10
  delete rangeTest;
#endif
}


void AgataLowEnergyPolarizedCompton::BuildPhysicsTable(const G4ParticleDefinition& )
{

  crossSectionHandler->Clear();
  G4String crossSectionFile = "comp/ce-cs-";
  crossSectionHandler->LoadData(crossSectionFile);
  delete meanFreePathTable;
  meanFreePathTable = crossSectionHandler->BuildMeanFreePathForMaterials();
}

// GEANT4 internal units
// Note : Effects due to binding of atomic electrons are neglected.

G4VParticleChange* AgataLowEnergyPolarizedCompton::
                   PostStepDoIt(const G4Track& aTrack, const G4Step&  aStep)
{
  aParticleChange.Initialize(aTrack);

  const G4DynamicParticle* incidentPhoton = aTrack.GetDynamicParticle();

  G4double gammaEnergy0 = incidentPhoton->GetKineticEnergy();
  if(gammaEnergy0 <= lowEnergyLimit) {
    aParticleChange.ProposeTrackStatus(fStopAndKill);
    aParticleChange.ProposeEnergy(0.);
    aParticleChange.ProposeLocalEnergyDeposit(gammaEnergy0);
    return G4VDiscreteProcess::PostStepDoIt(aTrack,aStep);
  }

  G4double E0_m = gammaEnergy0 / electron_mass_c2 ;

  G4double epsilon, cosTheta, sinTheta, sinThetaSq, greject ;

// Select randomly one element in the current material
  const G4MaterialCutsCouple* couple = aTrack.GetMaterialCutsCouple();
  G4int  Z = crossSectionHandler->SelectRandomAtom(couple, gammaEnergy0);

/////////////////////////
// Theta determination // 
/////////////////////////

  if(fixTh && std::abs(gammaEnergy0-valEg) < 0.001) {
// Force first scattering at a fixed angle as requested by FirstThetaSet
    cosTheta   = cos(valTh);
    epsilon    = 1./(1+E0_m*(1-cosTheta));
    sinThetaSq = 1. - cosTheta*cosTheta;
  }

  else {
// Sample the rate of the scattered gamma according to Klein - Nishina formula.
// The random number techniques of Butcher & Messel are used (Nuc Phys 20(1960),15),
// which are faster (at least at high gamma energy) than directly sampling cosTheta

    G4double epsilonSq, oneMcosTheta;
    G4double epsilon0   = 1./(1. + 2.*E0_m);
    G4double epsilon0Sq = epsilon0*epsilon0;
    G4double alpha1     = -std::log(epsilon0);
    G4double alpha2     = 0.5*(1.- epsilon0Sq);
    G4double selector   = alpha1/(alpha1+alpha2);
    G4double wlGamma    = h_Planck*c_light/gammaEnergy0;

    do {
      if ( selector > G4UniformRand() ) {
        epsilon   = std::exp(-alpha1*G4UniformRand());  
        epsilonSq = epsilon*epsilon; 
      }
      else {
        epsilonSq = epsilon0Sq + (1.- epsilon0Sq)*G4UniformRand();
        epsilon   = std::sqrt(epsilonSq);
      }

      oneMcosTheta = (1.- epsilon)/(epsilon*E0_m);
      sinThetaSq   = oneMcosTheta*(2.-oneMcosTheta);

      G4double x =  std::sqrt(oneMcosTheta/2.) / (wlGamma/cm);;
      G4double scatteringFunction = scatterFunctionData->FindValue(x,Z-1);
      greject = (1. - epsilon*sinThetaSq/(1.+ epsilonSq))*scatteringFunction;

    }
    while(greject < G4UniformRand()*Z);

    cosTheta = 1. - oneMcosTheta;

  }

  G4double gammaEnergy1 = epsilon*gammaEnergy0;
  G4double ggg          = epsilon + 1./epsilon;

  sinTheta = std::sqrt(std::abs(sinThetaSq)); // true only beacause sin(Theta)>=0 for Theta=[0:Pi]

///////////////////////  
// Phi determination //
///////////////////////  
  
// For a gamma fully polarized along x (as we are treating here) the ph dependence of the cross section is
// w(ph) ~ E1/E0 + E0/E1 - 2*sin(th)**2*cos(ph)**2
// This maximizes at ph = pi/2  and can be expressed as 1 - factor*cos(ph)**2, easy to sample

  G4double fff = 2*sinThetaSq/ggg;
  G4double Phi, cosPhi;
  do {
    Phi     = twopi*G4UniformRand();
    cosPhi  = std::cos(Phi);
    greject = 1. - fff*cosPhi*cosPhi;
  }
  while (greject < G4UniformRand());
  
  G4double sinPhi   = std::sin(Phi);

//////////////////////////////////////////////
// Consider now directions and polarization //
//////////////////////////////////////////////
 
  G4ParticleMomentum gammaDirection0    = incidentPhoton->GetMomentumDirection();
  G4ThreeVector      gammaPolarization0 = incidentPhoton->GetPolarization().unit();
  G4double           D0dotP0            = gammaDirection0.dot(gammaPolarization0);
 
// If the gamma is unpolarized, set a random polarization orthogomal to gammaDirection0:
// select at random a direction (cos(ps), sin(ps), 0) in the xy plane and 
// move it to the frame of gammaDirection0 by the Euler rotation Rz(phi)Ry(theta).
// Same thing is done if gammaPolarization0 is parallel to gammaDirection0...
// This is probably overkill because the framework should ensure consistency...
// In the same spirit explicitely force gammaPolarization0 to be orthogonal to gammaDirection0.
 
  if ( gammaPolarization0.mag2() < 1.e-12  ||  std::abs(std::abs(D0dotP0)-1.) <  1.e-9 ) {
    G4double th = gammaDirection0.theta();
    G4double ph = gammaDirection0.phi();
    G4double ps = twopi*G4UniformRand();
    G4double costh = std::cos(th), sinth = std::sin(th);
    G4double cosph = std::cos(ph), sinph = std::sin(ph);
    G4double cosps = std::cos(ps), sinps = std::sin(ps);
    gammaPolarization0.set(cosps*costh*cosph - sinps*sinph,
                           cosps*costh*sinph + sinps*cosph,
                          -cosps*sinth);
  }
  else {
    if(std::abs(D0dotP0) > 1.e-9) {
      gammaPolarization0 = gammaPolarization0 - D0dotP0 * gammaDirection0;
      gammaPolarization0 = gammaPolarization0.unit();
    }
  }

// The cartesian coordinate system for our vector calculations

  G4ThreeVector dirX = gammaPolarization0;    // x is gammaPolarization0
  G4ThreeVector dirZ = gammaDirection0;       // z is gammaDirection0
  G4ThreeVector dirY = dirZ.cross(dirX);      // y is normal to both
  
// In this frame the new direction is expressed in the standard way as

  G4ThreeVector gammaDirection1 = dirX * cosPhi*sinTheta
                                + dirY * sinPhi*sinTheta
                                + dirZ * cosTheta;
  
// For a fully polarized gamma, the polarization of the scattered gamma is in the "polarization plane"
// (Dir1 cross Pol0).   As it is also in the plane normal to the scattered direction, it given by the
// cross product of the normals to the two planes (Dir1 cross Pol0) cross Dir1. With a little algebra we get:

  G4ThreeVector gammaPolarization1 = dirX * (1-cosPhi*cosPhi*sinThetaSq)
                                   - dirY * sinPhi*cosPhi*sinThetaSq
                                   - dirZ * cosPhi*sinTheta*cosTheta;

// New polarization vector must be normalized to unit length

  gammaPolarization1 = gammaPolarization1.unit();

// Treat explicitely the case gammaDirection1 = dirX (for which gammaPolarization1 is undefinded)
// by setting it to dirY.  This is as good as any other direction in the (dirY,dirZ) plane because,
// being in this case PolarizationDegree == 0, it will be turned to dirZ 50% of the times

  if(gammaPolarization1.mag2() < 1.e-12)
     gammaPolarization1 = dirY;
  
// The total polarization P of the scattered gamma is given by the Stokes parameters formalism as:

  G4double vvv = 2.*sinThetaSq*cosPhi*cosPhi;
  G4double PolarizationDegree = (2. - vvv) / (ggg - vvv);
  
// The depolarization effect of the scattering is obtained by a pi/2 rotation of a fraction (1-P)/2

  if(G4UniformRand() > 0.5*(1+PolarizationDegree))
    gammaPolarization1 = gammaDirection1.cross(gammaPolarization1);
  
// Just to check
// G4double D1dotP1 = gammaDirection1.dot(gammaPolarization1);
// G4double modDir1 = gammaDirection1.mag2();
// G4double modPol1 = gammaPolarization1.mag2();

//////////////////////////////////////////////////////
// Update G4VParticleChange for the scattered gamma //
//////////////////////////////////////////////////////

  if (gammaEnergy1 > 0.) {
    aParticleChange.ProposeEnergy( gammaEnergy1 ) ;
    aParticleChange.ProposeMomentumDirection( gammaDirection1 );
    aParticleChange.ProposePolarization( gammaPolarization1 );
  }
  else {
    aParticleChange.ProposeEnergy(0.) ;
    aParticleChange.ProposeTrackStatus(fStopAndKill);
  }

//////////////////////////////////////////
// Kinematics of the scattered electron //
//////////////////////////////////////////

  G4double ElecKineEnergy = gammaEnergy0 - gammaEnergy1 ;

// Generate the electron only if with large enough range w.r.t. cuts and safety

 
#ifndef G4V10
 G4double safety = aStep.GetPostStepPoint()->GetSafety();

  if (rangeTest->Escape(G4Electron::Electron(),couple,ElecKineEnergy,safety)) {
    G4double ElecMomentum = std::sqrt(ElecKineEnergy*(ElecKineEnergy+2.*electron_mass_c2));
    G4ThreeVector ElecDirection((gammaEnergy0 * gammaDirection0 - gammaEnergy1 * gammaDirection1) * (1./ElecMomentum));
    G4DynamicParticle* electron = new G4DynamicParticle (G4Electron::Electron(),ElecDirection.unit(),ElecKineEnergy) ;
    aParticleChange.SetNumberOfSecondaries(1);
    aParticleChange.AddSecondary(electron);
    aParticleChange.ProposeLocalEnergyDeposit(0.); 
  }
  else {
    aParticleChange.SetNumberOfSecondaries(0);
    aParticleChange.ProposeLocalEnergyDeposit(ElecKineEnergy);
  }
#else
    aParticleChange.SetNumberOfSecondaries(0);
    aParticleChange.ProposeLocalEnergyDeposit(ElecKineEnergy);
#endif
  
  return G4VDiscreteProcess::PostStepDoIt( aTrack, aStep);
  
}


G4bool AgataLowEnergyPolarizedCompton::IsApplicable(const G4ParticleDefinition& particle)
{
  return ( &particle == G4Gamma::Gamma() ); 
}


G4double AgataLowEnergyPolarizedCompton::GetMeanFreePath(const G4Track& track,
						      G4double,
						      G4ForceCondition*)
{
  const G4DynamicParticle* photon = track.GetDynamicParticle();
  G4double energy = photon->GetKineticEnergy();
  const G4MaterialCutsCouple* couple = track.GetMaterialCutsCouple();
  size_t materialIndex = couple->GetIndex();
  G4double meanFreePath;
  if (energy > highEnergyLimit) meanFreePath = meanFreePathTable->FindValue(highEnergyLimit,materialIndex);
  else if (energy < lowEnergyLimit) meanFreePath = DBL_MAX;
  else meanFreePath = meanFreePathTable->FindValue(energy,materialIndex);
  return meanFreePath;
}














