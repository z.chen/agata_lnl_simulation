#ifdef ANCIL
#include "AgataAncillaryLead.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryLead::AgataAncillaryLead(G4String path)
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  matLead    = NULL;
  matName     = "Lead";
  matelH    = NULL;
  matelHName     = "Vacuum";
  
  ancSD       = NULL;
  ancName     = G4String("LeadCollimator");
  ancOffset   = 20000;
  
  numAncSd = 0;
}

AgataAncillaryLead::~AgataAncillaryLead()
{}

G4int AgataAncillaryLead::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matLead  = ptMaterial;
    G4String nome = matLead ->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  /*G4Material* */ ptMaterial = G4Material::GetMaterial(matelHName);
    matelH  = ptMaterial;
//    G4String nome = matelH ->GetName();
//    G4cout << "\n----> The ancillary material is "
//        << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}


void AgataAncillaryLead::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}

void AgataAncillaryLead::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
    G4cout << " The detector LeadCollimator is sensitive detector. It has fixed offset" << G4endl;
#endif
  G4SDManager* SDman = G4SDManager::GetSDMpointer();

  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( "/anc/all", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
    G4cout << " The detector LeadCollimator is sensitive detector. It has no fixed offset" << G4endl;

}
}

void AgataAncillaryLead::Placement()
{
  ///////////////////////
  /// construct the  LeadCollimator
  ///////////////////////
  G4bool ifvis  = false;
  G4double col_inn_rad = 0.10*cm;
  G4double col_thick =   9.90*cm;
  G4double col_out_rad = col_inn_rad+col_thick;
  G4double col_half_len = 6.*cm;
  G4double col_shift_len =  .2*cm;
  
  ColRotMat.set(0,0,0);  
  ColCentre = G4ThreeVector(0.*cm, 0.*cm, col_half_len+col_shift_len ); 
  ColRotMatNew.set(0,0,0);  
  ColRotMatNew.rotateZ(0.0*deg);  
  ColRotMatNew.rotateY(12.424198*deg);  
  ColRotMatNew.rotateZ(-2.941715*deg);  
  ColCentreNew = G4ThreeVector(0.*cm, 0.*cm, 0.*cm); 

  G4Tubs       *SolidTube = new G4Tubs("Vac",                        
                 col_inn_rad,
                 col_out_rad,
                 col_half_len,
                 0.*deg,
                 360.*deg);
  G4Tubs       *ShiftedSolidTube = new G4Tubs("Lead",                        
                 col_inn_rad,
                 col_out_rad,
                 col_half_len+ col_shift_len,
                 0.*deg,
                 360.*deg);
  
  G4LogicalVolume *logicTube = new G4LogicalVolume(SolidTube, matLead, G4String("LeadTube"), 0, 0, 0 );
  G4LogicalVolume *logicShiftedTube = new G4LogicalVolume(ShiftedSolidTube, matelH , G4String("ShiftedLeadTube"), 0, 0, 0 );
//  logicShiftedTube->SetSensitiveDetector(ancSD);
  new G4PVPlacement(G4Transform3D(ColRotMatNew,ColCentreNew), G4String("VacTube"), logicShiftedTube, theDetector->HallPhys(), false, 0 );
  logicTube->SetSensitiveDetector(ancSD);
  new G4PVPlacement(G4Transform3D(ColRotMat,ColCentre), logicTube, G4String("LeadTube"), logicShiftedTube, false, 0 );
  
  // Vis Attributes
  G4VisAttributes *pVA1 = new G4VisAttributes( );
  pVA1->SetVisibility (ifvis);
  logicShiftedTube->SetVisAttributes( pVA1 );
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  logicTube->SetVisAttributes( pVA );

  return;
}


void AgataAncillaryLead::ShowStatus()
{
  G4cout << " ANCILLARY LeadCollimator has been constructed." << G4endl;
  G4cout << " Ancillary LeadCollimator material is " << matLead->GetName() << G4endl;
}

void AgataAncillaryLead::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
