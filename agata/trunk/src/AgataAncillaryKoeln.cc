#include "AgataAncillaryKoeln.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryKoeln::AgataAncillaryKoeln(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with "real" implementations of this class
  //G4String iniPath = path;
  iniPath = path;
  dirName = name;

  // annular detector size
  innRad = 10.*mm;
  outRad = 35.*mm;
  passTh =  1.*mm;
  thickn = 0.3*mm;
  
  // segmentation
  nSect  = 16;
  dPhi   = 360.*deg/nSect;
  
  nRing  = 12;
  dRad   = (outRad - innRad - passTh)/nRing;
  
  positionD = G4ThreeVector(0., 0., -35.*mm);
  positionA = G4ThreeVector(0., 0., -30.*mm);
  positionT = G4ThreeVector();
  
  // absorber
  innRaA = 10.*mm;
  outRaA = 35.*mm;
  thickA = 0.3*mm;
  
  // target
  sizeT  = G4ThreeVector(1.0*cm, 1.0*cm, 0.005*mm);
  
  // materials
  matAnnul = NULL;
  matDNam  = "Silicon";
  
  numAncSd = 0;
  
  matAbso  = NULL;
  matANam  = "Vacuum";
  
  matTarg  = NULL;
  matTNam  = "Vacuum";
  
  ancSD       = NULL;
  
  ancName   = G4String("KOELN");
  ancOffset = 1000;
  
  // target rotation
  psi   = 0.;
  theta = 0.;
  phi   = 0.;
  
  writeSegments      = true;
  numbeFile          = iniPath + "NUMBERING.S2";

  myMessenger = new AgataAncillaryKoelnMessenger(this,name);

}

AgataAncillaryKoeln::~AgataAncillaryKoeln()
{

  delete myMessenger;

}

G4int AgataAncillaryKoeln::FindMaterials()
{
  // search the materials by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matDNam);
  if (ptMaterial) {
    matAnnul = ptMaterial;
    G4cout << "\n----> The ancillary detector material is "
          << matAnnul->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matDNam << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }  
  ptMaterial = G4Material::GetMaterial(matANam);
  if (ptMaterial) {
    matAbso = ptMaterial;
    G4cout << "\n----> The absorber material is "
          << matAbso->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matANam << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }
  ptMaterial = G4Material::GetMaterial(matTNam);
  if (ptMaterial) {
    matTarg = ptMaterial;
    G4cout << "\n----> The target material is "
          << matTarg->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matTNam << G4endl;
    G4cout << " Could not build the ancillary! " << G4endl;
    return 1;
  }
  return 0;
}

void AgataAncillaryKoeln::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( "/anc/all", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
}

void AgataAncillaryKoeln::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryKoeln::Placement()
{
  G4RotationMatrix rm;
  rm.set(0,0,0);

  PlaceDetector();
  
  if( matANam != "Vacuum" )
    PlaceAbsorber();
    
  if( matTNam != "Vacuum" )
    PlaceTarget();  

  return;
}

void AgataAncillaryKoeln::PlaceDetector()
{
  char sName[50];
  
  G4double innR = 0.;
  G4double outR = 0.;
  
  G4int nR = 0;
  G4int nS = 0;
  
  G4int index = 0;
  
  G4RotationMatrix rm;
  rm.set(0,0,0);
  
  FILE *s2number=NULL;
  if( writeSegments ) {
    if( (s2number = fopen(numbeFile, "w")) == NULL) {
      G4cout << "\nCould not open " << numbeFile
             << ", will not write angles for the segments!" << G4endl;
    }
    writeSegments = !writeSegments;
  }

  // passive area of the "outer" detector
  G4Tubs*            pAnnS = new G4Tubs(G4String("AnnS"), innRad, outRad, thickn/2., 0., 360.*deg);
  G4LogicalVolume*   pAnnL = new G4LogicalVolume( pAnnS, matAnnul, G4String("AnnL"), 0, 0, 0 );
  new G4PVPlacement(G4Transform3D(rm, positionD), "AnnP", pAnnL, theDetector->HallPhys(), false, 0 );
  
  dRad   = (outRad - innRad - passTh)/nRing;
  dPhi   = 360.*deg/nSect;
  
  G4Tubs*            pSegS = NULL;
  G4LogicalVolume*   pSegL = NULL;
  // G4VPhysicalVolume* pSegP = NULL;
  
  
  G4VisAttributes* segVA[3];
  segVA[0] = new G4VisAttributes(G4Colour(1.0,0.0,0.0));
  segVA[1] = new G4VisAttributes(G4Colour(0.0,1.0,0.0));
  segVA[2] = new G4VisAttributes(G4Colour(0.0,0.0,1.0));

  //G4cout << G4endl << "Center of segments:" << G4endl << "  #nR  #nS	theta	     phi" << G4endl;
  G4double mediumr;
  G4ThreeVector centerOfSegment;
  //G4double theta, phi; 
  //G4int prec = G4cout.precision(4);
  //G4cout.setf(ios::fixed);

  for( nR=0; nR<nRing; nR++ ) {
    innR = innRad + passTh + nR * dRad;
    outR = innR + dRad;
    sprintf( sName, "SegS%2.2d", nR );
    pSegS = new G4Tubs(G4String(sName), innR, outR, thickn/2., 0., dPhi );
    sprintf( sName, "SegL%2.2d", nR );
    pSegL = new G4LogicalVolume( pSegS, matAnnul, G4String(sName), 0, 0, 0 );
    pSegL->SetSensitiveDetector( ancSD );
    pSegL->SetVisAttributes(segVA[nR%3]);
    
    index = 0;
    for( nS=0; nS<nSect; nS++ ) {
      rm.set(0,0,0);
      rm.rotateZ(nS*dPhi);
			
      mediumr = (innR + outR) / 2.;
      centerOfSegment = positionD + mediumr * G4ThreeVector(cos((0.5+nS)*dPhi), sin((0.5+nS)*dPhi), 0.);
      theta = centerOfSegment.theta();
      phi   = centerOfSegment.phi();
      if( phi < 0. )
        phi  += 360.*deg;

      //G4cout << std::setw( 5) << nR 
      //       << std::setw( 5) << index
      //       << std::setw(10) << theta/deg
      //       << std::setw(10) << phi/deg << G4endl;
      
//      fprintf(s2number," %4d %3d %9.4lf %9.4lf\n", index, nR, theta/deg, phi/deg);
      fprintf(s2number," %4d %3d %9.4f %9.4f\n", index, nR, theta/deg, phi/deg);

      sprintf(sName, "SegP%4.4d", index);
      /*pSegP = */new G4PVPlacement(G4Transform3D(rm, G4ThreeVector()), pSegL, G4String(sName), pAnnL, false, index++);
    }
  }
  //G4cout << G4endl << G4endl;
  //G4cout.unsetf(ios::fixed);
  //G4cout.precision(prec);
}

void AgataAncillaryKoeln::PlaceAbsorber()
{
  G4RotationMatrix rm;
  rm.set(0,0,0);
  
  // absorber
  G4Tubs*            pAbsS = new G4Tubs("AbsS", innRaA, outRaA, thickA/2., 0., 360.*deg);
  G4LogicalVolume*   pAbsL = new G4LogicalVolume( pAbsS, matAbso, "AbsL", 0, 0, 0 );
  new G4PVPlacement(G4Transform3D(rm, positionA), "AbsP", pAbsL, theDetector->HallPhys(), false, 0 );
  
  G4VisAttributes* AbsVA = new G4VisAttributes(G4Colour(0.0,1.0,0.0));
  pAbsL->SetVisAttributes(AbsVA);
}

void AgataAncillaryKoeln::PlaceTarget()
{
  G4RotationMatrix rm;
  rm.set(0,0,0);
  rm.rotateZ(psi);
  rm.rotateY(theta);
  rm.rotateZ(phi);
  
  // target
  G4Box*             pTarS = new G4Box("TarS", sizeT.x()/2.,  sizeT.y()/2., sizeT.z()/2.);
  G4LogicalVolume*   pTarL = new G4LogicalVolume( pTarS, matTarg, "TarL", 0, 0, 0 );
  new G4PVPlacement(G4Transform3D(rm, positionT), "TarP", pTarL, theDetector->HallPhys(), false, 0 );
  
  G4VisAttributes* AbsVA = new G4VisAttributes(G4Colour(0.0,0.0,1.0));
  pTarL->SetVisAttributes(AbsVA);
}

G4int AgataAncillaryKoeln::GetSegmentNumber( G4int /*offset*/, G4int /*nGe*/, G4ThreeVector position )
{
  G4double proje = sqrt( position.x()*position.x() + position.y()*position.y() ) - innRad - passTh;
  
  return ((G4int)floor(proje/dRad));
}


/////////////////////////////////
/// methods for the messenger
/////////////////////////////////

// detector: size, position, segmentation ...
void AgataAncillaryKoeln::SetNSect( G4int number )
{
  if( number < 0. ) {
    nSect = 1;
    G4cout << "Warning! Only 1 sector considered. " << G4endl;
  }
  else if( number > 1000 ) {
    nSect = 1000;
    G4cout << "Warning! Only 1000 sectors considered. " << G4endl;
  }
  else {
    nSect = number;
    G4cout << " ----> Number of sectors is " << nSect << G4endl;
  }
  dPhi = 360.*deg/nSect;
}

void AgataAncillaryKoeln::SetNRing( G4int number )
{
  if( number < 0. ) {
    nRing = 1;
    G4cout << "Warning! Only 1 radial slice considered. " << G4endl;
  }
  else if( number > 100 ) {
    nRing = 100;
    G4cout << "Warning! Only 100 radial slices considered. " << G4endl;
  }
  else {
    nRing = number;
    G4cout << " ----> Number of radial slices is " << nRing << G4endl;
  }
  dRad   = (outRad - innRad - passTh)/nRing;
}

void AgataAncillaryKoeln::SetAnnulRmin( G4double radius )
{
  if( radius < 0. ) {
    innRad = 0.;
    G4cout << "Warning! Inner radius set to zero " << G4endl;
  }
  else {
    innRad = radius * mm;
    G4cout << " ----> Inner radius is " << innRad/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetAnnulRmax( G4double radius )
{
  if( radius < 0. ) {
    outRad = 0.;
    G4cout << "Warning! Outer radius set to zero " << G4endl;
  }
  else {
    outRad = radius * mm;
    G4cout << " ----> Outer radius is " << outRad/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetThickn( G4double value )
{
  if( value < 0. ) {
    thickn = 0.;
    G4cout << "Warning! Detector thickness set to zero " << G4endl;
  }
  else {
    thickn = value * mm;
    G4cout << " ----> Detector thickness is " << thickn/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetPassTh( G4double value )
{
  if( value < 0. ) {
    passTh = 0.;
    G4cout << "Warning! Passivated area thickness set to zero " << G4endl;
  }
  else {
    passTh = value * mm;
    G4cout << " ----> Passivated area thickness is " << passTh/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetPositionD( G4ThreeVector pos )
{
  positionD = pos * mm;
  
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Detector position is (" 
           << std::setw(8) << positionD.x()/mm
           << std::setw(8) << positionD.y()/mm
           << std::setw(8) << positionD.z()/mm << ") mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

// absorber: size, material, position
void AgataAncillaryKoeln::SetAbsoMate( G4String name )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( name );
  
  if( ptMaterial ) {
    matANam = name;
    G4cout << " ----> Absorber material has been set to " << matANam << G4endl;
  }
  else {
    G4cout << " Could not change material, keeping previous material ("
           << matAbso->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryKoeln::SetAbsoRmin( G4double radius )
{
  if( radius < 0. ) {
    innRaA = 0.;
    G4cout << "Warning! Inner absorber radius set to zero " << G4endl;
  }
  else {
    innRaA = radius * mm;
    G4cout << " ----> Inner absorber radius is " << innRaA/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetAbsoRmax( G4double radius )
{
  if( radius < 0. ) {
    outRaA = 0.;
    G4cout << "Warning! Outer absorber radius set to zero " << G4endl;
  }
  else {
    outRaA = radius * mm;
    G4cout << " ----> Outer absorber radius is " << outRaA/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetThickA( G4double value )
{
  if( value < 0. ) {
    thickA = 0.;
    G4cout << "Warning! Absorber thickness set to zero " << G4endl;
  }
  else {
    thickA = value * mm;
    G4cout << " ----> Absorber thickness is " << thickA/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetPositionA( G4ThreeVector pos )
{
  positionA = pos * mm;
  
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Absorber position is (" 
           << std::setw(8) << positionA.x()/mm
           << std::setw(8) << positionA.y()/mm
           << std::setw(8) << positionA.z()/mm << ") mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}


// target: size, material, position
void AgataAncillaryKoeln::SetTargMate( G4String name )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial( name );
  
  if( ptMaterial ) {
    matTNam = name;
    G4cout << " ----> Target material has been set to " << matTNam << G4endl;
  }
  else {
    G4cout << " Could not change material, keeping previous material ("
           << matTarg->GetName() << ")" << G4endl; 
  }
}

void AgataAncillaryKoeln::SetTargXSize( G4double value )
{
  if( value < 0. ) {
    sizeT.setX(0.);
    G4cout << "Warning! Target x dimension set to zero. " << G4endl;
  }
  else {
    sizeT.setX(value * mm);
    G4cout << " ----> Target x dimension is " << sizeT.x()/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetTargYSize( G4double value )
{
  if( value < 0. ) {
    sizeT.setY(0.);
    G4cout << "Warning! Target y dimension set to zero. " << G4endl;
  }
  else {
    sizeT.setY(value * mm);
    G4cout << " ----> Target y dimension is " << sizeT.y()/mm << " mm" << G4endl;
  }
}

void AgataAncillaryKoeln::SetThickT( G4double value )
{
  if( value <= 0. ) {
    sizeT.setZ(0.);
    G4cout << "Warning! Target thickness set to zero " << G4endl;
    return;
  }
  
  if( matTNam == "Vacuum" ) {
    G4cout << " Warning! Target material should be set before setting its thickness!" << G4endl;
    return;
  }
  
  G4Material* ptMaterial = G4Material::GetMaterial(matTNam);
  if(!ptMaterial) {
    G4cout << " Material " << matTNam << " has not been defined, cannot build the target!" << G4endl;
    G4cout << " Please choose an existing material according to the following material table: " << G4endl;
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
    return;
  }
  
  G4double dens = ptMaterial->GetDensity();
  sizeT.setZ((value * mg/cm2)/dens);
  G4cout << " ----> Target thickness is " << sizeT.z()*dens/(mg/cm2) << " mg/cm2" << G4endl;
}

void AgataAncillaryKoeln::SetRotationT( G4ThreeVector angles )
{
  psi = angles.x() * deg;
  if( psi > 360.*deg ){ 
    psi = 0.;
    G4cout << " Warning! Psi angle set to zero." << G4endl;
  }  
  theta = angles.y() * deg;
  if( theta > 180.*deg ){ 
    theta = 0.;
    G4cout << " Warning! Theta angle set to zero." << G4endl;
  }  
  phi = angles.z() * deg;
  if( phi > 360.*deg ){ 
    phi = 0.;
    G4cout << " Warning! Phi angle set to zero." << G4endl;
  }  
  G4cout << " --> Target rotation angles have been set to " <<
            psi/deg << ", " << theta/deg << ", " << phi/deg << " degrees." << G4endl;
}

void AgataAncillaryKoeln::SetPositionT( G4ThreeVector pos )
{
  positionT = pos * mm;
  
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Target position is (" 
           << std::setw(8) << positionT.x()/mm
           << std::setw(8) << positionT.y()/mm
           << std::setw(8) << positionT.z()/mm << ") mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataAncillaryKoeln::ShowStatus()
{
  G4int prec = G4cout.precision(4);
  G4cout << " Placed annular detector with inner radius " << innRad/mm << " mm, " 
         << "outer radius " << outRad/mm << " mm and thickness " << thickn/mm << " mm" << G4endl;
  G4cout << " Passivated area thickness is " << passTh/mm << " mm" << G4endl;
  G4cout << " Considered " << nSect << " sectors and " << nRing << " radial slices" << G4endl;
  G4cout.setf(ios::fixed);
  G4cout << " Detector position is ("      
           << std::setw(10) << positionD.x()/mm
           << std::setw(10) << positionD.y()/mm
           << std::setw(10) << positionD.z()/mm << ") mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
  
  if( matANam != "Vacuum" ) {
    G4cout << " Placed absorber with inner radius " << innRaA/mm << " mm, " 
           << "outer radius " << outRaA/mm << " mm and thickness " << thickA/mm << " mm" << G4endl;
    G4cout.setf(ios::fixed);
    G4cout << " Absorber position is ("      
             << std::setw(10) << positionA.x()/mm
             << std::setw(10) << positionA.y()/mm
             << std::setw(10) << positionA.z()/mm << ") mm" << G4endl;
    G4cout.unsetf(ios::fixed);
    G4cout.precision(prec);
  }
  
  if( matTNam != "Vacuum" ) {
    G4cout << " Placed target with x-size " << sizeT.x()/mm << " mm, " 
           << "and y-size " << sizeT.y()/mm << " mm and thickness " 
           << sizeT.z()*matTarg->GetDensity()/(mg/cm2) << " mg/cm2" << G4endl;
    G4cout.setf(ios::fixed);
    G4cout << " Target position is ("      
             << std::setw(10) << positionT.x()/mm
             << std::setw(10) << positionT.y()/mm
             << std::setw(10) << positionT.z()/mm << ") mm" << G4endl;
    G4cout.unsetf(ios::fixed);
    G4cout.precision(prec);
    G4cout << " Euler angles for target placement are "
           << psi/deg << ", " << theta/deg << ", " << phi/deg
           << " degrees/" << G4endl;
  }
}

void AgataAncillaryKoeln::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

void AgataAncillaryKoeln::SetWriteSegments( G4bool value )
{
  writeSegments = value;
  if( writeSegments )
    G4cout << " ----> The segment angles will be written out." << G4endl;
  else  
    G4cout << " ----> The segment angles will not be written out." << G4endl;
}

/////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"

AgataAncillaryKoelnMessenger::AgataAncillaryKoelnMessenger(AgataAncillaryKoeln* pTarget,G4String /*name*/)
:myTarget(pTarget)
{ 
  
  myDirectory = new G4UIdirectory("/Agata/detector/ancillary/Koeln/");
  myDirectory->SetGuidance("Control of S2 detector construction.");

  SetAnnulSizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/detSize", this);  
  SetAnnulSizeCmd->SetGuidance("Define size of the ancillary annular detector.");
  SetAnnulSizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetAnnulSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetThicknCmd = new G4UIcmdWithADouble("/Agata/detector/ancillary/Koeln/detThick", this);  
  SetThicknCmd->SetGuidance("Define thickness of the ancillary annular detector.");
  SetThicknCmd->SetGuidance("Required parameters: 1 double (thickness in mm).");
  SetThicknCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetPassThCmd = new G4UIcmdWithADouble("/Agata/detector/ancillary/Koeln/detPassThick", this);  
  SetPassThCmd->SetGuidance("Define thickness of the passivated area of the ancillary annular detector.");
  SetPassThCmd->SetGuidance("Required parameters: 1 double (thickness in mm).");
  SetPassThCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetSegmentsCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/detSegments", this);  
  SetSegmentsCmd->SetGuidance("Define segmentation of the ancillary annular detector.");
  SetSegmentsCmd->SetGuidance("Required parameters: 2 int (nSectors, nRings).");
  SetSegmentsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetPositionDCmd = new G4UIcmdWith3Vector("/Agata/detector/ancillary/Koeln/detPosition", this);  
  SetPositionDCmd->SetGuidance("Define position of the ancillary annular detector.");
  SetPositionDCmd->SetGuidance("Required parameters: 3 double (position coordinates in mm).");
  SetPositionDCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetAbsoSizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/absoSize", this);  
  SetAbsoSizeCmd->SetGuidance("Define size of the absorber.");
  SetAbsoSizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetAbsoSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetAbsoMateCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/absoMate", this);  
  SetAbsoMateCmd->SetGuidance("Define material of the absorber.");
  SetAbsoMateCmd->SetGuidance("Required parameters: 1 string (material name).");
  SetAbsoMateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetThickACmd = new G4UIcmdWithADouble("/Agata/detector/ancillary/Koeln/absoThick", this);  
  SetThickACmd->SetGuidance("Define thickness of the absorber.");
  SetThickACmd->SetGuidance("Required parameters: 1 double (thickness in mm).");
  SetThickACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetPositionACmd = new G4UIcmdWith3Vector("/Agata/detector/ancillary/Koeln/absoPosition", this);  
  SetPositionACmd->SetGuidance("Define position of the absorber.");
  SetPositionACmd->SetGuidance("Required parameters: 3 double (position coordinates in mm).");
  SetPositionACmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetTargSizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/targSize", this);  
  SetTargSizeCmd->SetGuidance("Define size of the target.");
  SetTargSizeCmd->SetGuidance("Required parameters: 2 double (x, y size in mm).");
  SetTargSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetTargMateCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/Koeln/targMate", this);  
  SetTargMateCmd->SetGuidance("Define material of the target.");
  SetTargMateCmd->SetGuidance("Required parameters: 1 string (material name).");
  SetTargMateCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetThickTCmd = new G4UIcmdWithADouble("/Agata/detector/ancillary/Koeln/targThick", this);  
  SetThickTCmd->SetGuidance("Define thickness of the target.");
  SetThickTCmd->SetGuidance("Required parameters: 1 double (thickness in mg/cm2).");
  SetThickTCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetRotationTCmd = new G4UIcmdWith3Vector("/Agata/detector/ancillary/Koeln/targRotation", this);  
  SetRotationTCmd->SetGuidance("Define rotation angles of the target.");
  SetRotationTCmd->SetGuidance("Required parameters: 3 double (Euler angles in degrees).");
  SetRotationTCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetPositionTCmd = new G4UIcmdWith3Vector("/Agata/detector/ancillary/Koeln/targPosition", this);  
  SetPositionTCmd->SetGuidance("Define position of the target.");
  SetPositionTCmd->SetGuidance("Required parameters: 3 double (position coordinates in mm).");
  SetPositionTCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  EnableSegmentsCmd = new G4UIcmdWithABool("/Agata/detector/ancillary/Koeln/enableSegments", this);
  EnableSegmentsCmd->SetGuidance("Writes out segment angles.");
  EnableSegmentsCmd->SetGuidance("Required parameters: none.");
  EnableSegmentsCmd->SetParameterName("writeSegments",true);
  EnableSegmentsCmd->SetDefaultValue(true);
  EnableSegmentsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  DisableSegmentsCmd = new G4UIcmdWithABool("/Agata/detector/ancillary/Koeln/disableSegments", this);
  DisableSegmentsCmd->SetGuidance("Do not write out segment angles.");
  DisableSegmentsCmd->SetGuidance("Required parameters: none.");
  DisableSegmentsCmd->SetParameterName("writeSegments",true);
  DisableSegmentsCmd->SetDefaultValue(false);
  DisableSegmentsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAncillaryKoelnMessenger::~AgataAncillaryKoelnMessenger()
{
  delete  myDirectory;
  delete  SetAnnulSizeCmd;
  delete  SetSegmentsCmd;
  delete  SetThicknCmd;
  delete  SetPassThCmd;
  delete  SetPositionDCmd;
  delete  SetAbsoMateCmd;
  delete  SetAbsoSizeCmd;
  delete  SetThickACmd;
  delete  SetPositionACmd;
  delete  SetTargMateCmd;
  delete  SetTargSizeCmd;
  delete  SetThickTCmd;
  delete  SetRotationTCmd;
  delete  SetPositionTCmd;
  delete  EnableSegmentsCmd;
  delete  DisableSegmentsCmd;
}

void AgataAncillaryKoelnMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetAnnulSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetAnnulRmin( e1 );
    myTarget->SetAnnulRmax( e2 );
  }
  if( command == SetSegmentsCmd ) {
    G4int n1, n2;
    sscanf( newValue, "%d %d", &n1, &n2);
    myTarget->SetNSect( n1 );
    myTarget->SetNRing( n2 );
  }
  if( command == SetThicknCmd ) {
    myTarget->SetThickn(SetThicknCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPassThCmd ) {
    myTarget->SetPassTh(SetPassThCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositionDCmd ) {
    myTarget->SetPositionD(SetPositionDCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetAbsoMateCmd ) {
    myTarget->SetAbsoMate(newValue);
  }
  if( command == SetAbsoSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetAbsoRmin( e1 );
    myTarget->SetAbsoRmax( e2 );
  }
  if( command == SetThickACmd ) {
    myTarget->SetThickA(SetThickACmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositionACmd ) {
    myTarget->SetPositionA(SetPositionACmd->GetNew3VectorValue(newValue));
  }
  if( command == SetTargMateCmd ) {
    myTarget->SetTargMate(newValue);
  }
  if( command == SetTargSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetTargXSize( e1 );
    myTarget->SetTargYSize( e2 );
  }
  if( command == SetThickTCmd ) {
    myTarget->SetThickT(SetThickTCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetPositionTCmd ) {
    myTarget->SetPositionT(SetPositionTCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetRotationTCmd ) {
    myTarget->SetPositionT(SetPositionTCmd->GetNew3VectorValue(newValue));
  }
  if( command == EnableSegmentsCmd ) {
    myTarget->SetWriteSegments( EnableSegmentsCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableSegmentsCmd ) {
    myTarget->SetWriteSegments( DisableSegmentsCmd->GetNewBoolValue(newValue) );
  }
  
}    
