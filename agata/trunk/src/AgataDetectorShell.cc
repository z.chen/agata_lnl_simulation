#include "AgataDetectorShell.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataDetectorShell::AgataDetectorShell(G4String name)
{
  shellRmin      = 15.0*cm;
  shellRmax      = 24.0*cm;
  
  matShell       = NULL;
  matName        = "Germanium";
  
  readOut        = false;
    
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;

  myMessenger    = new AgataDetectorShellMessenger(this,name);
}

AgataDetectorShell::~AgataDetectorShell()
{
  delete myMessenger;
}

G4int AgataDetectorShell::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4cout << "\n ----> The shell material is "
          << matShell->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the Shell! " << G4endl;
    return 1;
  } 
  return 0; 
}


void AgataDetectorShell::Placement()
{
  if( FindMaterials() ) return;

  G4RunManager* runManager               = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  
  //////////////////////////////////////////////////
  // a germanium shell
  //////////////////////////////////////////////////
  G4Sphere*        solidShell = new G4Sphere( "shell", shellRmin, shellRmax, 0.*deg, 360.*deg, 0., 180.*deg);
  G4LogicalVolume* logicShell = new G4LogicalVolume( solidShell, matShell, "Shell", 0, 0, 0 );
  new G4PVPlacement(0, G4ThreeVector(), "Shell", logicShell, theDetector->HallPhys(), false, 0 );

  nDets++;
  nClus++;                  

  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  logicShell->SetVisAttributes( pVA );

  // Sensitive Detector
  logicShell->SetSensitiveDetector( theDetector->GeSD() );

  G4cout << "Shell radius " << shellRmin << " -- " << shellRmax << G4endl;
  G4cout << "Shell material is " << matShell->GetName() << G4endl;
}

void AgataDetectorShell::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  outFileLMD << "SHELL" << G4endl;
  outFileLMD << shellRmin/unit << "  "  << shellRmax/unit  << G4endl;

}

void AgataDetectorShell::ShowStatus()
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " Shell radius " << shellRmin/cm << " -- " << shellRmax/cm << " cm" << G4endl;
  G4cout << " Shell material is " << matShell->GetName() << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

///////////////////////////////////////////////////////////////////
/// methods for the messenger
////////////////////////////////////////////////////////////////////

void AgataDetectorShell::SetShellRmin( G4double radius )
{
  if( radius < 0. ) {
    shellRmin = 0.;
    G4cout << " Warning! Inner shell radius set to zero " << G4endl;
  }
  else {
    shellRmin = radius;
    G4cout << " ----> Inner shell radius is " << shellRmin/cm << " cm" << G4endl;
  }
}

void AgataDetectorShell::SetShellRmax( G4double radius )
{
  if( radius < 0. ) {
    shellRmax = 0.;
    G4cout << " Warning! Outer shell radius set to zero " << G4endl;
  }
  else {
    shellRmax = radius;
    G4cout << " ----> Outer shell radius is " << shellRmax/cm << " cm" << G4endl;
  }
}

void AgataDetectorShell::SetShellMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matName = materialName;
    G4cout << " ----> Shell material has been set to " << matName << G4endl;
  }
  else {
    G4cout << " Material not found! " << G4endl;
    G4cout << " ----> Keeping previous material ("
           << matShell->GetName() << ")" << G4endl;
  }
}

/////////////////////////////////////////////////////////////////////////
// The Messenger

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

AgataDetectorShellMessenger::AgataDetectorShellMessenger(AgataDetectorShell* pTarget,G4String /*name*/)
:myTarget(pTarget)
{ 

  SetShellSizeCmd = new G4UIcmdWithAString("/Agata/detector/size",this);  
  SetShellSizeCmd->SetGuidance("Define size of the Germanium shell.");
  SetShellSizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetShellSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  ShellMatCmd = new G4UIcmdWithAString("/Agata/detector/shellMaterial",this);
  ShellMatCmd->SetGuidance("Select Material of the shell.");
  ShellMatCmd->SetGuidance("Required parameters: 1 string.");
  ShellMatCmd->SetParameterName("choice",false);
  ShellMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataDetectorShellMessenger::~AgataDetectorShellMessenger()
{

  delete ShellMatCmd;
  delete SetShellSizeCmd;
}

void AgataDetectorShellMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetShellSizeCmd ) {
    G4double e1, e2;
    sscanf( newValue, "%lf %lf", &e1, &e2);
    myTarget->SetShellRmin( e1*mm );
    myTarget->SetShellRmax( e2*mm );
  }
  if( command == ShellMatCmd ) {
    myTarget->SetShellMate(newValue);
  }
}    
