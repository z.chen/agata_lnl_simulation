#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iomanip>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <stdint.h>
using std::setw;
using namespace std;

int main( int argc, char** argv ) 
{
	 ifstream readFile("../AGATASmod.gdml");

    //char *readout = "<position name=posRef_1 x=\"-18.1589\" y=\"41.9311\" z=\"-0.3713\"/>" ;
	string s1;
	//s1=readout;
    char *readout;
 
 // String to look fo in the file
	char *search1 ="z=\"";
	char *search2 ="\"/>";
	const char *c;
	
	string s2(search1);
	
 	size_t  Posfound1=0;
 	size_t  Posfound2=0;
 	
 	double Zin, Zout;

	int cond=0; 	
 
	//Create an output file:
	char outputFile[512] = "../AGATASmodShifted.gdml";
	ofstream outFile(outputFile);

	if( readFile.is_open())
	{
		while(!readFile.eof()){
		
		getline(readFile,s1);

		//cout <<"s1:" << s1 << endl;
		//cout <<"s1:" << s1.size()  << endl;

		if(s1.size()>2){
			string ss1(s1,2,9);
			//cout << "ss1=" <<  ss1 <<endl;
			if(ss1 == "<physvol>")cond=1;  // one need to have reached this position in the file to start the correction 
		}

		if(cond==0) outFile << s1 << endl; // just copy the line s1 in output file
      
		if (cond==1) // if cond==1, one can start to look for the string s2 and s3 within s1
		{

				if(s1.size()>8)
				{	string ss1(s1,2,9);
					cout << "ss1=" <<  ss1 << " s1size=" << s1.size() <<endl;

					if(ss1 != "	<positio")
					{
						outFile << s1 << endl; // just copy the line s1 in output file
					}else
					{
						Posfound1 = s1.find(s2);
 
						if(Posfound1<s1.size()){  // if one has found s2 in the line: 
							string s3(search2);
							Posfound2=s1.find(s3);
							cout << "Pos. found: " << Posfound2 << endl;
							
	
							if(Posfound2<=s1.size()){ // if on has found s3 in the line:
								string s4(s1, Posfound1+3, Posfound2-(Posfound1+3) );
								cout << "s4: " << s4 << endl;
								const char * c= s4.c_str();

								Zin= atof(c); // 
								Zout=Zin+230+52.; // add a shif of 230 mm in Z + 45mm as crystal are 9 cm long
								cout << "Zout =" << Zout << endl;
								
								string s5(s1, 0, Posfound1+3);
								string s6(s1, Posfound2,s1.size());
																
								outFile << s5 << Zout << s6 << endl;
							}
							
							
							
						}
					}
				}else // ie if( s1.size<8)
				{
					outFile << s1 << endl; // just copy the line s1 in output file
				}	
		}
	}		
	
     
       /*if(readout == search){
           //Replace it with 'replace'
           // How can I do that Here.
          // Some code that find the position of pointer and replace it.
        }else
        {
			outFile << readout << endl;
		}*/
        
   } 
  
outFile.close();


return 0;  
}


